package com.feedstoa.rf.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(ServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private FetchRequestServiceProxy fetchRequestService = null;
    private SecondaryFetchServiceProxy secondaryFetchService = null;
    private ChannelSourceServiceProxy channelSourceService = null;
    private ChannelFeedServiceProxy channelFeedService = null;
    private FeedItemServiceProxy feedItemService = null;
    private FeedContentServiceProxy feedContentService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    private ServiceProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ServiceProxyFactoryHolder
    {
        private static final ServiceProxyFactory INSTANCE = new ServiceProxyFactory();
    }

    // Singleton method
    public static ServiceProxyFactory getInstance()
    {
        return ServiceProxyFactoryHolder.INSTANCE;
    }

    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new UserServiceProxy();
        }
        return userService;
    }

    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceProxy();
        }
        return fetchRequestService;
    }

    public SecondaryFetchServiceProxy getSecondaryFetchServiceProxy()
    {
        if(secondaryFetchService == null) {
            secondaryFetchService = new SecondaryFetchServiceProxy();
        }
        return secondaryFetchService;
    }

    public ChannelSourceServiceProxy getChannelSourceServiceProxy()
    {
        if(channelSourceService == null) {
            channelSourceService = new ChannelSourceServiceProxy();
        }
        return channelSourceService;
    }

    public ChannelFeedServiceProxy getChannelFeedServiceProxy()
    {
        if(channelFeedService == null) {
            channelFeedService = new ChannelFeedServiceProxy();
        }
        return channelFeedService;
    }

    public FeedItemServiceProxy getFeedItemServiceProxy()
    {
        if(feedItemService == null) {
            feedItemService = new FeedItemServiceProxy();
        }
        return feedItemService;
    }

    public FeedContentServiceProxy getFeedContentServiceProxy()
    {
        if(feedContentService == null) {
            feedContentService = new FeedContentServiceProxy();
        }
        return feedContentService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceProxy();
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceProxy();
        }
        return fiveTenService;
    }

}
