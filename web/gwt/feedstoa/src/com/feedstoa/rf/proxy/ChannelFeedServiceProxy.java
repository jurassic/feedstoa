package com.feedstoa.rf.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.NotImplementedException;
import com.feedstoa.ws.core.StatusCode;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.KeyListStub;
import com.feedstoa.ws.stub.CloudStructStub;
import com.feedstoa.ws.stub.CloudStructListStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.CategoryStructListStub;
import com.feedstoa.ws.stub.ImageStructStub;
import com.feedstoa.ws.stub.ImageStructListStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.stub.UriStructListStub;
import com.feedstoa.ws.stub.UserStructStub;
import com.feedstoa.ws.stub.UserStructListStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructListStub;
import com.feedstoa.ws.stub.TextInputStructStub;
import com.feedstoa.ws.stub.TextInputStructListStub;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.stub.ChannelFeedListStub;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.util.StringUtil;
import com.feedstoa.rf.auth.TwoLeggedOAuthClientUtil;

import com.feedstoa.rf.config.Config;


public class ChannelFeedServiceProxy extends AbstractBaseServiceProxy implements ChannelFeedService
{
    private static final Logger log = Logger.getLogger(ChannelFeedServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_CHANNELFEED = "channelFeeds";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public ChannelFeedServiceProxy()
    {
        initCache();
    }


    protected WebResource getChannelFeedWebResource()
    {
        return getWebResource(RESOURCE_CHANNELFEED);
    }
    protected WebResource getChannelFeedWebResource(String path)
    {
        return getWebResource(RESOURCE_CHANNELFEED, path);
    }
    protected WebResource getChannelFeedWebResourceByGuid(String guid)
    {
        return getChannelFeedWebResource(guid);
    }


    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        ChannelFeed bean = null;

        String key = getResourcePath(RESOURCE_CHANNELFEED, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getChannelFeedWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ChannelFeedStub stub = clientResponse.getEntity(ChannelFeedStub.class);
                bean = MarshalHelper.convertChannelFeedToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ChannelFeed bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        ChannelFeed bean = getChannelFeed(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("channelSource")) {
            return bean.getChannelSource();
        } else if(field.equals("channelCode")) {
            return bean.getChannelCode();
        } else if(field.equals("previousVersion")) {
            return bean.getPreviousVersion();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("fetchUrl")) {
            return bean.getFetchUrl();
        } else if(field.equals("feedServiceUrl")) {
            return bean.getFeedServiceUrl();
        } else if(field.equals("feedUrl")) {
            return bean.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return bean.getFeedFormat();
        } else if(field.equals("maxItemCount")) {
            return bean.getMaxItemCount();
        } else if(field.equals("feedCategory")) {
            return bean.getFeedCategory();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("subtitle")) {
            return bean.getSubtitle();
        } else if(field.equals("link")) {
            return bean.getLink();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("copyright")) {
            return bean.getCopyright();
        } else if(field.equals("managingEditor")) {
            return bean.getManagingEditor();
        } else if(field.equals("webMaster")) {
            return bean.getWebMaster();
        } else if(field.equals("contributor")) {
            return bean.getContributor();
        } else if(field.equals("pubDate")) {
            return bean.getPubDate();
        } else if(field.equals("lastBuildDate")) {
            return bean.getLastBuildDate();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("generator")) {
            return bean.getGenerator();
        } else if(field.equals("docs")) {
            return bean.getDocs();
        } else if(field.equals("cloud")) {
            return bean.getCloud();
        } else if(field.equals("ttl")) {
            return bean.getTtl();
        } else if(field.equals("logo")) {
            return bean.getLogo();
        } else if(field.equals("icon")) {
            return bean.getIcon();
        } else if(field.equals("rating")) {
            return bean.getRating();
        } else if(field.equals("textInput")) {
            return bean.getTextInput();
        } else if(field.equals("skipHours")) {
            return bean.getSkipHours();
        } else if(field.equals("skipDays")) {
            return bean.getSkipDays();
        } else if(field.equals("outputText")) {
            return bean.getOutputText();
        } else if(field.equals("outputHash")) {
            return bean.getOutputHash();
        } else if(field.equals("feedContent")) {
            return bean.getFeedContent();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("lastBuildTime")) {
            return bean.getLastBuildTime();
        } else if(field.equals("publishedTime")) {
            return bean.getPublishedTime();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        throw new NotImplementedException("To be implemented.");
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return getAllChannelFeeds(null, null, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<ChannelFeed> list = null;
    	
        WebResource webResource = getChannelFeedWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ChannelFeedListStub stub = clientResponse.getEntity(ChannelFeedListStub.class);
                list = MarshalHelper.convertChannelFeedListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ChannelFeed list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;
    	
        WebResource webResource = getChannelFeedWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ChannelFeed key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<ChannelFeed> list = null;
    	
//        ClientResponse clientResponse = getChannelFeedWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getChannelFeedWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ChannelFeedListStub stub = clientResponse.getEntity(ChannelFeedListStub.class);
                list = MarshalHelper.convertChannelFeedListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ChannelFeed list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;

    	WebResource webResource = getChannelFeedWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ChannelFeed key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getChannelFeedWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ChannelFeed count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        ChannelFeedBean bean = new ChannelFeedBean(null, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, MarshalHelper.convertUriStructToBean(link), description, language, copyright, MarshalHelper.convertUserStructToBean(managingEditor), MarshalHelper.convertUserStructToBean(webMaster), contributor, pubDate, lastBuildDate, category, generator, docs, MarshalHelper.convertCloudStructToBean(cloud), ttl, MarshalHelper.convertImageStructToBean(logo), MarshalHelper.convertImageStructToBean(icon), rating, MarshalHelper.convertTextInputStructToBean(textInput), skipHours, skipDays, outputText, outputHash, feedContent, status, note, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return createChannelFeed(bean);        
    }

    @Override
    public String createChannelFeed(ChannelFeed bean) throws BaseException
    {
        String guid = null;
        ChannelFeedStub stub = MarshalHelper.convertChannelFeedToStub(bean);
        WebResource webResource = getChannelFeedWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New ChannelFeed guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New ChannelFeed resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public ChannelFeed constructChannelFeed(ChannelFeed bean) throws BaseException
    {
        ChannelFeedStub stub = MarshalHelper.convertChannelFeedToStub(bean);
        WebResource webResource = getChannelFeedWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(ChannelFeedStub.class);
                bean = MarshalHelper.convertChannelFeedToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New ChannelFeed bean = " + bean);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New ChannelFeed resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelFeed guid is invalid.");
        	throw new BaseException("ChannelFeed guid is invalid.");
        }
        ChannelFeedBean bean = new ChannelFeedBean(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, MarshalHelper.convertUriStructToBean(link), description, language, copyright, MarshalHelper.convertUserStructToBean(managingEditor), MarshalHelper.convertUserStructToBean(webMaster), contributor, pubDate, lastBuildDate, category, generator, docs, MarshalHelper.convertCloudStructToBean(cloud), ttl, MarshalHelper.convertImageStructToBean(logo), MarshalHelper.convertImageStructToBean(icon), rating, MarshalHelper.convertTextInputStructToBean(textInput), skipHours, skipDays, outputText, outputHash, feedContent, status, note, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return updateChannelFeed(bean);        
    }

    @Override
    public Boolean updateChannelFeed(ChannelFeed bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelFeed object is invalid.");
        	throw new BaseException("ChannelFeed object is invalid.");
        }
        ChannelFeedStub stub = MarshalHelper.convertChannelFeedToStub(bean);

        WebResource webResource = getChannelFeedWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        //ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).put(ClientResponse.class, stub);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public ChannelFeed refreshChannelFeed(ChannelFeed bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelFeed object is invalid.");
        	throw new BaseException("ChannelFeed object is invalid.");
        }
        ChannelFeedStub stub = MarshalHelper.convertChannelFeedToStub(bean);

        WebResource webResource = getChannelFeedWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                stub = clientResponse.getEntity(ChannelFeedStub.class);
                bean = MarshalHelper.convertChannelFeedToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully refreshed the ChannelFeed bean = " + bean);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelFeed guid is invalid.");
        	throw new BaseException("ChannelFeed guid is invalid.");
        }

        WebResource webResource = getChannelFeedWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deleteChannelFeed(ChannelFeed bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteChannelFeed(guid);
    }

    @Override
    public Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getChannelFeedWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exception if the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        // Order of the mime types ???
        ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted ChannelFeeds: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_CHANNELFEED);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    {
        log.finer("BEGIN");

        if(channelFeeds == null) {
            log.log(Level.WARNING, "createChannelFeeds() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = channelFeeds.size();
        if(size == 0) {
            log.log(Level.WARNING, "createChannelFeeds() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ChannelFeed channelFeed : channelFeeds) {
            String guid = createChannelFeed(channelFeed);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createChannelFeeds() failed for at least one channelFeed. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    //{
    //}

}
