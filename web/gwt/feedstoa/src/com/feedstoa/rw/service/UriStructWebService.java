package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UriStructWebService // implements UriStructService
{
    private static final Logger log = Logger.getLogger(UriStructWebService.class.getName());
     
    public static UriStructJsBean convertUriStructToJsBean(UriStruct uriStruct)
    {
        UriStructJsBean jsBean = null;
        if(uriStruct != null) {
            jsBean = new UriStructJsBean();
            jsBean.setUuid(uriStruct.getUuid());
            jsBean.setHref(uriStruct.getHref());
            jsBean.setRel(uriStruct.getRel());
            jsBean.setType(uriStruct.getType());
            jsBean.setLabel(uriStruct.getLabel());
        }
        return jsBean;
    }

    public static UriStruct convertUriStructJsBeanToBean(UriStructJsBean jsBean)
    {
        UriStructBean uriStruct = null;
        if(jsBean != null) {
            uriStruct = new UriStructBean();
            uriStruct.setUuid(jsBean.getUuid());
            uriStruct.setHref(jsBean.getHref());
            uriStruct.setRel(jsBean.getRel());
            uriStruct.setType(jsBean.getType());
            uriStruct.setLabel(jsBean.getLabel());
        }
        return uriStruct;
    }

}
