package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.EnclosureStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.RssItemJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RssItemWebService // implements RssItemService
{
    private static final Logger log = Logger.getLogger(RssItemWebService.class.getName());
     
    public static RssItemJsBean convertRssItemToJsBean(RssItem rssItem)
    {
        RssItemJsBean jsBean = null;
        if(rssItem != null) {
            jsBean = new RssItemJsBean();
            jsBean.setTitle(rssItem.getTitle());
            jsBean.setLink(rssItem.getLink());
            jsBean.setDescription(rssItem.getDescription());
            jsBean.setAuthor(rssItem.getAuthor());
            List<CategoryStructJsBean> categoryJsBeans = new ArrayList<CategoryStructJsBean>();
            List<CategoryStruct> categoryBeans = rssItem.getCategory();
            if(categoryBeans != null) {
                for(CategoryStruct categoryStruct : categoryBeans) {
                    CategoryStructJsBean jB = CategoryStructWebService.convertCategoryStructToJsBean(categoryStruct);
                    categoryJsBeans.add(jB); 
                }
            }
            jsBean.setCategory(categoryJsBeans);
            jsBean.setComments(rssItem.getComments());
            jsBean.setEnclosure(EnclosureStructWebService.convertEnclosureStructToJsBean(rssItem.getEnclosure()));
            jsBean.setGuid(rssItem.getGuid());
            jsBean.setPubDate(rssItem.getPubDate());
            jsBean.setSource(UriStructWebService.convertUriStructToJsBean(rssItem.getSource()));
        }
        return jsBean;
    }

    public static RssItem convertRssItemJsBeanToBean(RssItemJsBean jsBean)
    {
        RssItemBean rssItem = null;
        if(jsBean != null) {
            rssItem = new RssItemBean();
            rssItem.setTitle(jsBean.getTitle());
            rssItem.setLink(jsBean.getLink());
            rssItem.setDescription(jsBean.getDescription());
            rssItem.setAuthor(jsBean.getAuthor());
            List<CategoryStruct> categoryBeans = new ArrayList<CategoryStruct>();
            List<CategoryStructJsBean> categoryJsBeans = jsBean.getCategory();
            if(categoryJsBeans != null) {
                for(CategoryStructJsBean categoryStruct : categoryJsBeans) {
                    CategoryStruct b = CategoryStructWebService.convertCategoryStructJsBeanToBean(categoryStruct);
                    categoryBeans.add(b); 
                }
            }
            rssItem.setCategory(categoryBeans);
            rssItem.setComments(jsBean.getComments());
            rssItem.setEnclosure(EnclosureStructWebService.convertEnclosureStructJsBeanToBean(jsBean.getEnclosure()));
            rssItem.setGuid(jsBean.getGuid());
            rssItem.setPubDate(jsBean.getPubDate());
            rssItem.setSource(UriStructWebService.convertUriStructJsBeanToBean(jsBean.getSource()));
        }
        return rssItem;
    }

}
