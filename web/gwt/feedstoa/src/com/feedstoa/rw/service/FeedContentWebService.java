package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.FeedContentJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FeedContentWebService // implements FeedContentService
{
    private static final Logger log = Logger.getLogger(FeedContentWebService.class.getName());
     
    // Af service interface.
    private FeedContentService mService = null;

    public FeedContentWebService()
    {
        this(ServiceProxyFactory.getInstance().getFeedContentServiceProxy());
    }
    public FeedContentWebService(FeedContentService service)
    {
        mService = service;
    }
    
    private FeedContentService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getFeedContentServiceProxy();
        }
        return mService;
    }
    
    
    public FeedContentJsBean getFeedContent(String guid) throws WebException
    {
        try {
            FeedContent feedContent = getService().getFeedContent(guid);
            FeedContentJsBean bean = convertFeedContentToJsBean(feedContent);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getFeedContent(String guid, String field) throws WebException
    {
        try {
            return getService().getFeedContent(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FeedContentJsBean> getFeedContents(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FeedContentJsBean> jsBeans = new ArrayList<FeedContentJsBean>();
            List<FeedContent> feedContents = getService().getFeedContents(guids);
            if(feedContents != null) {
                for(FeedContent feedContent : feedContents) {
                    jsBeans.add(convertFeedContentToJsBean(feedContent));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FeedContentJsBean> getAllFeedContents() throws WebException
    {
        return getAllFeedContents(null, null, null);
    }

    // @Deprecated
    public List<FeedContentJsBean> getAllFeedContents(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFeedContents(ordering, offset, count, null);
    }

    public List<FeedContentJsBean> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<FeedContentJsBean> jsBeans = new ArrayList<FeedContentJsBean>();
            List<FeedContent> feedContents = getService().getAllFeedContents(ordering, offset, count, forwardCursor);
            if(feedContents != null) {
                for(FeedContent feedContent : feedContents) {
                    jsBeans.add(convertFeedContentToJsBean(feedContent));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFeedContentKeys(ordering, offset, count, null);
    }

    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllFeedContentKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<FeedContentJsBean> findFeedContents(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findFeedContents(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<FeedContentJsBean> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<FeedContentJsBean> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<FeedContentJsBean> jsBeans = new ArrayList<FeedContentJsBean>();
            List<FeedContent> feedContents = getService().findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(feedContents != null) {
                for(FeedContent feedContent : feedContents) {
                    jsBeans.add(convertFeedContentToJsBean(feedContent));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFeedContent(String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws WebException
    {
        try {
            return getService().createFeedContent(user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFeedContent(FeedContentJsBean jsBean) throws WebException
    {
        try {
            FeedContent feedContent = convertFeedContentJsBeanToBean(jsBean);
            return getService().createFeedContent(feedContent);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FeedContentJsBean constructFeedContent(FeedContentJsBean jsBean) throws WebException
    {
        try {
            FeedContent feedContent = convertFeedContentJsBeanToBean(jsBean);
            feedContent = getService().constructFeedContent(feedContent);
            jsBean = convertFeedContentToJsBean(feedContent);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateFeedContent(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws WebException
    {
        try {
            return getService().updateFeedContent(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateFeedContent(FeedContentJsBean jsBean) throws WebException
    {
        try {
            FeedContent feedContent = convertFeedContentJsBeanToBean(jsBean);
            return getService().updateFeedContent(feedContent);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FeedContentJsBean refreshFeedContent(FeedContentJsBean jsBean) throws WebException
    {
        try {
            FeedContent feedContent = convertFeedContentJsBeanToBean(jsBean);
            feedContent = getService().refreshFeedContent(feedContent);
            jsBean = convertFeedContentToJsBean(feedContent);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFeedContent(String guid) throws WebException
    {
        try {
            return getService().deleteFeedContent(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFeedContent(FeedContentJsBean jsBean) throws WebException
    {
        try {
            FeedContent feedContent = convertFeedContentJsBeanToBean(jsBean);
            return getService().deleteFeedContent(feedContent);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteFeedContents(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteFeedContents(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static FeedContentJsBean convertFeedContentToJsBean(FeedContent feedContent)
    {
        FeedContentJsBean jsBean = null;
        if(feedContent != null) {
            jsBean = new FeedContentJsBean();
            jsBean.setGuid(feedContent.getGuid());
            jsBean.setUser(feedContent.getUser());
            jsBean.setChannelSource(feedContent.getChannelSource());
            jsBean.setChannelFeed(feedContent.getChannelFeed());
            jsBean.setChannelVersion(feedContent.getChannelVersion());
            jsBean.setFeedUrl(feedContent.getFeedUrl());
            jsBean.setFeedFormat(feedContent.getFeedFormat());
            jsBean.setContent(feedContent.getContent());
            jsBean.setContentHash(feedContent.getContentHash());
            jsBean.setStatus(feedContent.getStatus());
            jsBean.setNote(feedContent.getNote());
            jsBean.setLastBuildDate(feedContent.getLastBuildDate());
            jsBean.setLastBuildTime(feedContent.getLastBuildTime());
            jsBean.setCreatedTime(feedContent.getCreatedTime());
            jsBean.setModifiedTime(feedContent.getModifiedTime());
        }
        return jsBean;
    }

    public static FeedContent convertFeedContentJsBeanToBean(FeedContentJsBean jsBean)
    {
        FeedContentBean feedContent = null;
        if(jsBean != null) {
            feedContent = new FeedContentBean();
            feedContent.setGuid(jsBean.getGuid());
            feedContent.setUser(jsBean.getUser());
            feedContent.setChannelSource(jsBean.getChannelSource());
            feedContent.setChannelFeed(jsBean.getChannelFeed());
            feedContent.setChannelVersion(jsBean.getChannelVersion());
            feedContent.setFeedUrl(jsBean.getFeedUrl());
            feedContent.setFeedFormat(jsBean.getFeedFormat());
            feedContent.setContent(jsBean.getContent());
            feedContent.setContentHash(jsBean.getContentHash());
            feedContent.setStatus(jsBean.getStatus());
            feedContent.setNote(jsBean.getNote());
            feedContent.setLastBuildDate(jsBean.getLastBuildDate());
            feedContent.setLastBuildTime(jsBean.getLastBuildTime());
            feedContent.setCreatedTime(jsBean.getCreatedTime());
            feedContent.setModifiedTime(jsBean.getModifiedTime());
        }
        return feedContent;
    }

}
