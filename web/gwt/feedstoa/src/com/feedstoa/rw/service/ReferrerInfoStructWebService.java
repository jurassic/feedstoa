package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ReferrerInfoStructWebService // implements ReferrerInfoStructService
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructWebService.class.getName());
     
    public static ReferrerInfoStructJsBean convertReferrerInfoStructToJsBean(ReferrerInfoStruct referrerInfoStruct)
    {
        ReferrerInfoStructJsBean jsBean = null;
        if(referrerInfoStruct != null) {
            jsBean = new ReferrerInfoStructJsBean();
            jsBean.setReferer(referrerInfoStruct.getReferer());
            jsBean.setUserAgent(referrerInfoStruct.getUserAgent());
            jsBean.setLanguage(referrerInfoStruct.getLanguage());
            jsBean.setHostname(referrerInfoStruct.getHostname());
            jsBean.setIpAddress(referrerInfoStruct.getIpAddress());
            jsBean.setNote(referrerInfoStruct.getNote());
        }
        return jsBean;
    }

    public static ReferrerInfoStruct convertReferrerInfoStructJsBeanToBean(ReferrerInfoStructJsBean jsBean)
    {
        ReferrerInfoStructBean referrerInfoStruct = null;
        if(jsBean != null) {
            referrerInfoStruct = new ReferrerInfoStructBean();
            referrerInfoStruct.setReferer(jsBean.getReferer());
            referrerInfoStruct.setUserAgent(jsBean.getUserAgent());
            referrerInfoStruct.setLanguage(jsBean.getLanguage());
            referrerInfoStruct.setHostname(jsBean.getHostname());
            referrerInfoStruct.setIpAddress(jsBean.getIpAddress());
            referrerInfoStruct.setNote(jsBean.getNote());
        }
        return referrerInfoStruct;
    }

}
