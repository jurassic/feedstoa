package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.af.bean.RssChannelBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.ImageStructJsBean;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.fe.bean.RssItemJsBean;
import com.feedstoa.fe.bean.RssChannelJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RssChannelWebService // implements RssChannelService
{
    private static final Logger log = Logger.getLogger(RssChannelWebService.class.getName());
     
    public static RssChannelJsBean convertRssChannelToJsBean(RssChannel rssChannel)
    {
        RssChannelJsBean jsBean = null;
        if(rssChannel != null) {
            jsBean = new RssChannelJsBean();
            jsBean.setTitle(rssChannel.getTitle());
            jsBean.setLink(rssChannel.getLink());
            jsBean.setDescription(rssChannel.getDescription());
            jsBean.setLanguage(rssChannel.getLanguage());
            jsBean.setCopyright(rssChannel.getCopyright());
            jsBean.setManagingEditor(rssChannel.getManagingEditor());
            jsBean.setWebMaster(rssChannel.getWebMaster());
            jsBean.setPubDate(rssChannel.getPubDate());
            jsBean.setLastBuildDate(rssChannel.getLastBuildDate());
            List<CategoryStructJsBean> categoryJsBeans = new ArrayList<CategoryStructJsBean>();
            List<CategoryStruct> categoryBeans = rssChannel.getCategory();
            if(categoryBeans != null) {
                for(CategoryStruct categoryStruct : categoryBeans) {
                    CategoryStructJsBean jB = CategoryStructWebService.convertCategoryStructToJsBean(categoryStruct);
                    categoryJsBeans.add(jB); 
                }
            }
            jsBean.setCategory(categoryJsBeans);
            jsBean.setGenerator(rssChannel.getGenerator());
            jsBean.setDocs(rssChannel.getDocs());
            jsBean.setCloud(CloudStructWebService.convertCloudStructToJsBean(rssChannel.getCloud()));
            jsBean.setTtl(rssChannel.getTtl());
            jsBean.setImage(ImageStructWebService.convertImageStructToJsBean(rssChannel.getImage()));
            jsBean.setRating(rssChannel.getRating());
            jsBean.setTextInput(TextInputStructWebService.convertTextInputStructToJsBean(rssChannel.getTextInput()));
            jsBean.setSkipHours(rssChannel.getSkipHours());
            jsBean.setSkipDays(rssChannel.getSkipDays());
            List<RssItemJsBean> itemJsBeans = new ArrayList<RssItemJsBean>();
            List<RssItem> itemBeans = rssChannel.getItem();
            if(itemBeans != null) {
                for(RssItem rssItem : itemBeans) {
                    RssItemJsBean jB = RssItemWebService.convertRssItemToJsBean(rssItem);
                    itemJsBeans.add(jB); 
                }
            }
            jsBean.setItem(itemJsBeans);
        }
        return jsBean;
    }

    public static RssChannel convertRssChannelJsBeanToBean(RssChannelJsBean jsBean)
    {
        RssChannelBean rssChannel = null;
        if(jsBean != null) {
            rssChannel = new RssChannelBean();
            rssChannel.setTitle(jsBean.getTitle());
            rssChannel.setLink(jsBean.getLink());
            rssChannel.setDescription(jsBean.getDescription());
            rssChannel.setLanguage(jsBean.getLanguage());
            rssChannel.setCopyright(jsBean.getCopyright());
            rssChannel.setManagingEditor(jsBean.getManagingEditor());
            rssChannel.setWebMaster(jsBean.getWebMaster());
            rssChannel.setPubDate(jsBean.getPubDate());
            rssChannel.setLastBuildDate(jsBean.getLastBuildDate());
            List<CategoryStruct> categoryBeans = new ArrayList<CategoryStruct>();
            List<CategoryStructJsBean> categoryJsBeans = jsBean.getCategory();
            if(categoryJsBeans != null) {
                for(CategoryStructJsBean categoryStruct : categoryJsBeans) {
                    CategoryStruct b = CategoryStructWebService.convertCategoryStructJsBeanToBean(categoryStruct);
                    categoryBeans.add(b); 
                }
            }
            rssChannel.setCategory(categoryBeans);
            rssChannel.setGenerator(jsBean.getGenerator());
            rssChannel.setDocs(jsBean.getDocs());
            rssChannel.setCloud(CloudStructWebService.convertCloudStructJsBeanToBean(jsBean.getCloud()));
            rssChannel.setTtl(jsBean.getTtl());
            rssChannel.setImage(ImageStructWebService.convertImageStructJsBeanToBean(jsBean.getImage()));
            rssChannel.setRating(jsBean.getRating());
            rssChannel.setTextInput(TextInputStructWebService.convertTextInputStructJsBeanToBean(jsBean.getTextInput()));
            rssChannel.setSkipHours(jsBean.getSkipHours());
            rssChannel.setSkipDays(jsBean.getSkipDays());
            List<RssItem> itemBeans = new ArrayList<RssItem>();
            List<RssItemJsBean> itemJsBeans = jsBean.getItem();
            if(itemJsBeans != null) {
                for(RssItemJsBean rssItem : itemJsBeans) {
                    RssItem b = RssItemWebService.convertRssItemJsBeanToBean(rssItem);
                    itemBeans.add(b); 
                }
            }
            rssChannel.setItem(itemBeans);
        }
        return rssChannel;
    }

}
