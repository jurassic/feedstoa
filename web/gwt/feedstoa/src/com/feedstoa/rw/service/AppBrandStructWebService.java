package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.AppBrandStruct;
import com.feedstoa.af.bean.AppBrandStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.AppBrandStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppBrandStructWebService // implements AppBrandStructService
{
    private static final Logger log = Logger.getLogger(AppBrandStructWebService.class.getName());
     
    public static AppBrandStructJsBean convertAppBrandStructToJsBean(AppBrandStruct appBrandStruct)
    {
        AppBrandStructJsBean jsBean = null;
        if(appBrandStruct != null) {
            jsBean = new AppBrandStructJsBean();
            jsBean.setBrand(appBrandStruct.getBrand());
            jsBean.setName(appBrandStruct.getName());
            jsBean.setDescription(appBrandStruct.getDescription());
        }
        return jsBean;
    }

    public static AppBrandStruct convertAppBrandStructJsBeanToBean(AppBrandStructJsBean jsBean)
    {
        AppBrandStructBean appBrandStruct = null;
        if(jsBean != null) {
            appBrandStruct = new AppBrandStructBean();
            appBrandStruct.setBrand(jsBean.getBrand());
            appBrandStruct.setName(jsBean.getName());
            appBrandStruct.setDescription(jsBean.getDescription());
        }
        return appBrandStruct;
    }

}
