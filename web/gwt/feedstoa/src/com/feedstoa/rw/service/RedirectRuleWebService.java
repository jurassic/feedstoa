package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.RedirectRule;
import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.RedirectRuleJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RedirectRuleWebService // implements RedirectRuleService
{
    private static final Logger log = Logger.getLogger(RedirectRuleWebService.class.getName());
     
    public static RedirectRuleJsBean convertRedirectRuleToJsBean(RedirectRule redirectRule)
    {
        RedirectRuleJsBean jsBean = null;
        if(redirectRule != null) {
            jsBean = new RedirectRuleJsBean();
            jsBean.setRedirectType(redirectRule.getRedirectType());
            jsBean.setPrecedence(redirectRule.getPrecedence());
            jsBean.setSourceDomain(redirectRule.getSourceDomain());
            jsBean.setSourcePath(redirectRule.getSourcePath());
            jsBean.setTargetDomain(redirectRule.getTargetDomain());
            jsBean.setTargetPath(redirectRule.getTargetPath());
            jsBean.setNote(redirectRule.getNote());
            jsBean.setStatus(redirectRule.getStatus());
        }
        return jsBean;
    }

    public static RedirectRule convertRedirectRuleJsBeanToBean(RedirectRuleJsBean jsBean)
    {
        RedirectRuleBean redirectRule = null;
        if(jsBean != null) {
            redirectRule = new RedirectRuleBean();
            redirectRule.setRedirectType(jsBean.getRedirectType());
            redirectRule.setPrecedence(jsBean.getPrecedence());
            redirectRule.setSourceDomain(jsBean.getSourceDomain());
            redirectRule.setSourcePath(jsBean.getSourcePath());
            redirectRule.setTargetDomain(jsBean.getTargetDomain());
            redirectRule.setTargetPath(jsBean.getTargetPath());
            redirectRule.setNote(jsBean.getNote());
            redirectRule.setStatus(jsBean.getStatus());
        }
        return redirectRule;
    }

}
