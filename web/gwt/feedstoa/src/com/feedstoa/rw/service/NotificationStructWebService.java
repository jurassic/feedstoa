package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.NotificationStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class NotificationStructWebService // implements NotificationStructService
{
    private static final Logger log = Logger.getLogger(NotificationStructWebService.class.getName());
     
    public static NotificationStructJsBean convertNotificationStructToJsBean(NotificationStruct notificationStruct)
    {
        NotificationStructJsBean jsBean = null;
        if(notificationStruct != null) {
            jsBean = new NotificationStructJsBean();
            jsBean.setPreferredMode(notificationStruct.getPreferredMode());
            jsBean.setMobileNumber(notificationStruct.getMobileNumber());
            jsBean.setEmailAddress(notificationStruct.getEmailAddress());
            jsBean.setTwitterUsername(notificationStruct.getTwitterUsername());
            jsBean.setFacebookId(notificationStruct.getFacebookId());
            jsBean.setLinkedinId(notificationStruct.getLinkedinId());
            jsBean.setNote(notificationStruct.getNote());
        }
        return jsBean;
    }

    public static NotificationStruct convertNotificationStructJsBeanToBean(NotificationStructJsBean jsBean)
    {
        NotificationStructBean notificationStruct = null;
        if(jsBean != null) {
            notificationStruct = new NotificationStructBean();
            notificationStruct.setPreferredMode(jsBean.getPreferredMode());
            notificationStruct.setMobileNumber(jsBean.getMobileNumber());
            notificationStruct.setEmailAddress(jsBean.getEmailAddress());
            notificationStruct.setTwitterUsername(jsBean.getTwitterUsername());
            notificationStruct.setFacebookId(jsBean.getFacebookId());
            notificationStruct.setLinkedinId(jsBean.getLinkedinId());
            notificationStruct.setNote(jsBean.getNote());
        }
        return notificationStruct;
    }

}
