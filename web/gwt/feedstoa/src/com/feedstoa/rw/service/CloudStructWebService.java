package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CloudStructWebService // implements CloudStructService
{
    private static final Logger log = Logger.getLogger(CloudStructWebService.class.getName());
     
    public static CloudStructJsBean convertCloudStructToJsBean(CloudStruct cloudStruct)
    {
        CloudStructJsBean jsBean = null;
        if(cloudStruct != null) {
            jsBean = new CloudStructJsBean();
            jsBean.setDomain(cloudStruct.getDomain());
            jsBean.setPort(cloudStruct.getPort());
            jsBean.setPath(cloudStruct.getPath());
            jsBean.setRegisterProcedure(cloudStruct.getRegisterProcedure());
            jsBean.setProtocol(cloudStruct.getProtocol());
        }
        return jsBean;
    }

    public static CloudStruct convertCloudStructJsBeanToBean(CloudStructJsBean jsBean)
    {
        CloudStructBean cloudStruct = null;
        if(jsBean != null) {
            cloudStruct = new CloudStructBean();
            cloudStruct.setDomain(jsBean.getDomain());
            cloudStruct.setPort(jsBean.getPort());
            cloudStruct.setPath(jsBean.getPath());
            cloudStruct.setRegisterProcedure(jsBean.getRegisterProcedure());
            cloudStruct.setProtocol(jsBean.getProtocol());
        }
        return cloudStruct;
    }

}
