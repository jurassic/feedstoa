package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserStructWebService // implements UserStructService
{
    private static final Logger log = Logger.getLogger(UserStructWebService.class.getName());
     
    public static UserStructJsBean convertUserStructToJsBean(UserStruct userStruct)
    {
        UserStructJsBean jsBean = null;
        if(userStruct != null) {
            jsBean = new UserStructJsBean();
            jsBean.setUuid(userStruct.getUuid());
            jsBean.setName(userStruct.getName());
            jsBean.setEmail(userStruct.getEmail());
            jsBean.setUrl(userStruct.getUrl());
        }
        return jsBean;
    }

    public static UserStruct convertUserStructJsBeanToBean(UserStructJsBean jsBean)
    {
        UserStructBean userStruct = null;
        if(jsBean != null) {
            userStruct = new UserStructBean();
            userStruct.setUuid(jsBean.getUuid());
            userStruct.setName(jsBean.getName());
            userStruct.setEmail(jsBean.getEmail());
            userStruct.setUrl(jsBean.getUrl());
        }
        return userStruct;
    }

}
