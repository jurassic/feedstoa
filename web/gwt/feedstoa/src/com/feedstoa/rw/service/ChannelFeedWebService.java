package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.ImageStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.fe.bean.ChannelFeedJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ChannelFeedWebService // implements ChannelFeedService
{
    private static final Logger log = Logger.getLogger(ChannelFeedWebService.class.getName());
     
    // Af service interface.
    private ChannelFeedService mService = null;

    public ChannelFeedWebService()
    {
        this(ServiceProxyFactory.getInstance().getChannelFeedServiceProxy());
    }
    public ChannelFeedWebService(ChannelFeedService service)
    {
        mService = service;
    }
    
    private ChannelFeedService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getChannelFeedServiceProxy();
        }
        return mService;
    }
    
    
    public ChannelFeedJsBean getChannelFeed(String guid) throws WebException
    {
        try {
            ChannelFeed channelFeed = getService().getChannelFeed(guid);
            ChannelFeedJsBean bean = convertChannelFeedToJsBean(channelFeed);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getChannelFeed(String guid, String field) throws WebException
    {
        try {
            return getService().getChannelFeed(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ChannelFeedJsBean> getChannelFeeds(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ChannelFeedJsBean> jsBeans = new ArrayList<ChannelFeedJsBean>();
            List<ChannelFeed> channelFeeds = getService().getChannelFeeds(guids);
            if(channelFeeds != null) {
                for(ChannelFeed channelFeed : channelFeeds) {
                    jsBeans.add(convertChannelFeedToJsBean(channelFeed));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ChannelFeedJsBean> getAllChannelFeeds() throws WebException
    {
        return getAllChannelFeeds(null, null, null);
    }

    // @Deprecated
    public List<ChannelFeedJsBean> getAllChannelFeeds(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    public List<ChannelFeedJsBean> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ChannelFeedJsBean> jsBeans = new ArrayList<ChannelFeedJsBean>();
            List<ChannelFeed> channelFeeds = getService().getAllChannelFeeds(ordering, offset, count, forwardCursor);
            if(channelFeeds != null) {
                for(ChannelFeed channelFeed : channelFeeds) {
                    jsBeans.add(convertChannelFeedToJsBean(channelFeed));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllChannelFeedKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<ChannelFeedJsBean> findChannelFeeds(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findChannelFeeds(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<ChannelFeedJsBean> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<ChannelFeedJsBean> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ChannelFeedJsBean> jsBeans = new ArrayList<ChannelFeedJsBean>();
            List<ChannelFeed> channelFeeds = getService().findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(channelFeeds != null) {
                for(ChannelFeed channelFeed : channelFeeds) {
                    jsBeans.add(convertChannelFeedToJsBean(channelFeed));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructJsBean link, String description, String language, String copyright, UserStructJsBean managingEditor, UserStructJsBean webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean logo, ImageStructJsBean icon, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().createChannelFeed(user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, UriStructWebService.convertUriStructJsBeanToBean(link), description, language, copyright, UserStructWebService.convertUserStructJsBeanToBean(managingEditor), UserStructWebService.convertUserStructJsBeanToBean(webMaster), contributor, pubDate, lastBuildDate, category, generator, docs, CloudStructWebService.convertCloudStructJsBeanToBean(cloud), ttl, ImageStructWebService.convertImageStructJsBeanToBean(logo), ImageStructWebService.convertImageStructJsBeanToBean(icon), rating, TextInputStructWebService.convertTextInputStructJsBeanToBean(textInput), skipHours, skipDays, outputText, outputHash, feedContent, status, note, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createChannelFeed(ChannelFeedJsBean jsBean) throws WebException
    {
        try {
            ChannelFeed channelFeed = convertChannelFeedJsBeanToBean(jsBean);
            return getService().createChannelFeed(channelFeed);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ChannelFeedJsBean constructChannelFeed(ChannelFeedJsBean jsBean) throws WebException
    {
        try {
            ChannelFeed channelFeed = convertChannelFeedJsBeanToBean(jsBean);
            channelFeed = getService().constructChannelFeed(channelFeed);
            jsBean = convertChannelFeedToJsBean(channelFeed);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructJsBean link, String description, String language, String copyright, UserStructJsBean managingEditor, UserStructJsBean webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean logo, ImageStructJsBean icon, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().updateChannelFeed(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, UriStructWebService.convertUriStructJsBeanToBean(link), description, language, copyright, UserStructWebService.convertUserStructJsBeanToBean(managingEditor), UserStructWebService.convertUserStructJsBeanToBean(webMaster), contributor, pubDate, lastBuildDate, category, generator, docs, CloudStructWebService.convertCloudStructJsBeanToBean(cloud), ttl, ImageStructWebService.convertImageStructJsBeanToBean(logo), ImageStructWebService.convertImageStructJsBeanToBean(icon), rating, TextInputStructWebService.convertTextInputStructJsBeanToBean(textInput), skipHours, skipDays, outputText, outputHash, feedContent, status, note, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateChannelFeed(ChannelFeedJsBean jsBean) throws WebException
    {
        try {
            ChannelFeed channelFeed = convertChannelFeedJsBeanToBean(jsBean);
            return getService().updateChannelFeed(channelFeed);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ChannelFeedJsBean refreshChannelFeed(ChannelFeedJsBean jsBean) throws WebException
    {
        try {
            ChannelFeed channelFeed = convertChannelFeedJsBeanToBean(jsBean);
            channelFeed = getService().refreshChannelFeed(channelFeed);
            jsBean = convertChannelFeedToJsBean(channelFeed);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteChannelFeed(String guid) throws WebException
    {
        try {
            return getService().deleteChannelFeed(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteChannelFeed(ChannelFeedJsBean jsBean) throws WebException
    {
        try {
            ChannelFeed channelFeed = convertChannelFeedJsBeanToBean(jsBean);
            return getService().deleteChannelFeed(channelFeed);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteChannelFeeds(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteChannelFeeds(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ChannelFeedJsBean convertChannelFeedToJsBean(ChannelFeed channelFeed)
    {
        ChannelFeedJsBean jsBean = null;
        if(channelFeed != null) {
            jsBean = new ChannelFeedJsBean();
            jsBean.setGuid(channelFeed.getGuid());
            jsBean.setUser(channelFeed.getUser());
            jsBean.setChannelSource(channelFeed.getChannelSource());
            jsBean.setChannelCode(channelFeed.getChannelCode());
            jsBean.setPreviousVersion(channelFeed.getPreviousVersion());
            jsBean.setFetchRequest(channelFeed.getFetchRequest());
            jsBean.setFetchUrl(channelFeed.getFetchUrl());
            jsBean.setFeedServiceUrl(channelFeed.getFeedServiceUrl());
            jsBean.setFeedUrl(channelFeed.getFeedUrl());
            jsBean.setFeedFormat(channelFeed.getFeedFormat());
            jsBean.setMaxItemCount(channelFeed.getMaxItemCount());
            jsBean.setFeedCategory(channelFeed.getFeedCategory());
            jsBean.setTitle(channelFeed.getTitle());
            jsBean.setSubtitle(channelFeed.getSubtitle());
            jsBean.setLink(UriStructWebService.convertUriStructToJsBean(channelFeed.getLink()));
            jsBean.setDescription(channelFeed.getDescription());
            jsBean.setLanguage(channelFeed.getLanguage());
            jsBean.setCopyright(channelFeed.getCopyright());
            jsBean.setManagingEditor(UserStructWebService.convertUserStructToJsBean(channelFeed.getManagingEditor()));
            jsBean.setWebMaster(UserStructWebService.convertUserStructToJsBean(channelFeed.getWebMaster()));
            List<UserStructJsBean> contributorJsBeans = new ArrayList<UserStructJsBean>();
            List<UserStruct> contributorBeans = channelFeed.getContributor();
            if(contributorBeans != null) {
                for(UserStruct userStruct : contributorBeans) {
                    UserStructJsBean jB = UserStructWebService.convertUserStructToJsBean(userStruct);
                    contributorJsBeans.add(jB); 
                }
            }
            jsBean.setContributor(contributorJsBeans);
            jsBean.setPubDate(channelFeed.getPubDate());
            jsBean.setLastBuildDate(channelFeed.getLastBuildDate());
            List<CategoryStructJsBean> categoryJsBeans = new ArrayList<CategoryStructJsBean>();
            List<CategoryStruct> categoryBeans = channelFeed.getCategory();
            if(categoryBeans != null) {
                for(CategoryStruct categoryStruct : categoryBeans) {
                    CategoryStructJsBean jB = CategoryStructWebService.convertCategoryStructToJsBean(categoryStruct);
                    categoryJsBeans.add(jB); 
                }
            }
            jsBean.setCategory(categoryJsBeans);
            jsBean.setGenerator(channelFeed.getGenerator());
            jsBean.setDocs(channelFeed.getDocs());
            jsBean.setCloud(CloudStructWebService.convertCloudStructToJsBean(channelFeed.getCloud()));
            jsBean.setTtl(channelFeed.getTtl());
            jsBean.setLogo(ImageStructWebService.convertImageStructToJsBean(channelFeed.getLogo()));
            jsBean.setIcon(ImageStructWebService.convertImageStructToJsBean(channelFeed.getIcon()));
            jsBean.setRating(channelFeed.getRating());
            jsBean.setTextInput(TextInputStructWebService.convertTextInputStructToJsBean(channelFeed.getTextInput()));
            jsBean.setSkipHours(channelFeed.getSkipHours());
            jsBean.setSkipDays(channelFeed.getSkipDays());
            jsBean.setOutputText(channelFeed.getOutputText());
            jsBean.setOutputHash(channelFeed.getOutputHash());
            jsBean.setFeedContent(channelFeed.getFeedContent());
            jsBean.setStatus(channelFeed.getStatus());
            jsBean.setNote(channelFeed.getNote());
            jsBean.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructToJsBean(channelFeed.getReferrerInfo()));
            jsBean.setLastBuildTime(channelFeed.getLastBuildTime());
            jsBean.setPublishedTime(channelFeed.getPublishedTime());
            jsBean.setExpirationTime(channelFeed.getExpirationTime());
            jsBean.setLastUpdatedTime(channelFeed.getLastUpdatedTime());
            jsBean.setCreatedTime(channelFeed.getCreatedTime());
            jsBean.setModifiedTime(channelFeed.getModifiedTime());
        }
        return jsBean;
    }

    public static ChannelFeed convertChannelFeedJsBeanToBean(ChannelFeedJsBean jsBean)
    {
        ChannelFeedBean channelFeed = null;
        if(jsBean != null) {
            channelFeed = new ChannelFeedBean();
            channelFeed.setGuid(jsBean.getGuid());
            channelFeed.setUser(jsBean.getUser());
            channelFeed.setChannelSource(jsBean.getChannelSource());
            channelFeed.setChannelCode(jsBean.getChannelCode());
            channelFeed.setPreviousVersion(jsBean.getPreviousVersion());
            channelFeed.setFetchRequest(jsBean.getFetchRequest());
            channelFeed.setFetchUrl(jsBean.getFetchUrl());
            channelFeed.setFeedServiceUrl(jsBean.getFeedServiceUrl());
            channelFeed.setFeedUrl(jsBean.getFeedUrl());
            channelFeed.setFeedFormat(jsBean.getFeedFormat());
            channelFeed.setMaxItemCount(jsBean.getMaxItemCount());
            channelFeed.setFeedCategory(jsBean.getFeedCategory());
            channelFeed.setTitle(jsBean.getTitle());
            channelFeed.setSubtitle(jsBean.getSubtitle());
            channelFeed.setLink(UriStructWebService.convertUriStructJsBeanToBean(jsBean.getLink()));
            channelFeed.setDescription(jsBean.getDescription());
            channelFeed.setLanguage(jsBean.getLanguage());
            channelFeed.setCopyright(jsBean.getCopyright());
            channelFeed.setManagingEditor(UserStructWebService.convertUserStructJsBeanToBean(jsBean.getManagingEditor()));
            channelFeed.setWebMaster(UserStructWebService.convertUserStructJsBeanToBean(jsBean.getWebMaster()));
            List<UserStruct> contributorBeans = new ArrayList<UserStruct>();
            List<UserStructJsBean> contributorJsBeans = jsBean.getContributor();
            if(contributorJsBeans != null) {
                for(UserStructJsBean userStruct : contributorJsBeans) {
                    UserStruct b = UserStructWebService.convertUserStructJsBeanToBean(userStruct);
                    contributorBeans.add(b); 
                }
            }
            channelFeed.setContributor(contributorBeans);
            channelFeed.setPubDate(jsBean.getPubDate());
            channelFeed.setLastBuildDate(jsBean.getLastBuildDate());
            List<CategoryStruct> categoryBeans = new ArrayList<CategoryStruct>();
            List<CategoryStructJsBean> categoryJsBeans = jsBean.getCategory();
            if(categoryJsBeans != null) {
                for(CategoryStructJsBean categoryStruct : categoryJsBeans) {
                    CategoryStruct b = CategoryStructWebService.convertCategoryStructJsBeanToBean(categoryStruct);
                    categoryBeans.add(b); 
                }
            }
            channelFeed.setCategory(categoryBeans);
            channelFeed.setGenerator(jsBean.getGenerator());
            channelFeed.setDocs(jsBean.getDocs());
            channelFeed.setCloud(CloudStructWebService.convertCloudStructJsBeanToBean(jsBean.getCloud()));
            channelFeed.setTtl(jsBean.getTtl());
            channelFeed.setLogo(ImageStructWebService.convertImageStructJsBeanToBean(jsBean.getLogo()));
            channelFeed.setIcon(ImageStructWebService.convertImageStructJsBeanToBean(jsBean.getIcon()));
            channelFeed.setRating(jsBean.getRating());
            channelFeed.setTextInput(TextInputStructWebService.convertTextInputStructJsBeanToBean(jsBean.getTextInput()));
            channelFeed.setSkipHours(jsBean.getSkipHours());
            channelFeed.setSkipDays(jsBean.getSkipDays());
            channelFeed.setOutputText(jsBean.getOutputText());
            channelFeed.setOutputHash(jsBean.getOutputHash());
            channelFeed.setFeedContent(jsBean.getFeedContent());
            channelFeed.setStatus(jsBean.getStatus());
            channelFeed.setNote(jsBean.getNote());
            channelFeed.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            channelFeed.setLastBuildTime(jsBean.getLastBuildTime());
            channelFeed.setPublishedTime(jsBean.getPublishedTime());
            channelFeed.setExpirationTime(jsBean.getExpirationTime());
            channelFeed.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            channelFeed.setCreatedTime(jsBean.getCreatedTime());
            channelFeed.setModifiedTime(jsBean.getModifiedTime());
        }
        return channelFeed;
    }

}
