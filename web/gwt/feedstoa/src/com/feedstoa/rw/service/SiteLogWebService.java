package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.SiteLog;
import com.feedstoa.af.bean.SiteLogBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.SiteLogJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SiteLogWebService // implements SiteLogService
{
    private static final Logger log = Logger.getLogger(SiteLogWebService.class.getName());
     
    public static SiteLogJsBean convertSiteLogToJsBean(SiteLog siteLog)
    {
        SiteLogJsBean jsBean = null;
        if(siteLog != null) {
            jsBean = new SiteLogJsBean();
            jsBean.setUuid(siteLog.getUuid());
            jsBean.setTitle(siteLog.getTitle());
            jsBean.setPubDate(siteLog.getPubDate());
            jsBean.setTag(siteLog.getTag());
            jsBean.setContent(siteLog.getContent());
            jsBean.setFormat(siteLog.getFormat());
            jsBean.setNote(siteLog.getNote());
            jsBean.setStatus(siteLog.getStatus());
        }
        return jsBean;
    }

    public static SiteLog convertSiteLogJsBeanToBean(SiteLogJsBean jsBean)
    {
        SiteLogBean siteLog = null;
        if(jsBean != null) {
            siteLog = new SiteLogBean();
            siteLog.setUuid(jsBean.getUuid());
            siteLog.setTitle(jsBean.getTitle());
            siteLog.setPubDate(jsBean.getPubDate());
            siteLog.setTag(jsBean.getTag());
            siteLog.setContent(jsBean.getContent());
            siteLog.setFormat(jsBean.getFormat());
            siteLog.setNote(jsBean.getNote());
            siteLog.setStatus(jsBean.getStatus());
        }
        return siteLog;
    }

}
