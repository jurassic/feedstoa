package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TextInputStructWebService // implements TextInputStructService
{
    private static final Logger log = Logger.getLogger(TextInputStructWebService.class.getName());
     
    public static TextInputStructJsBean convertTextInputStructToJsBean(TextInputStruct textInputStruct)
    {
        TextInputStructJsBean jsBean = null;
        if(textInputStruct != null) {
            jsBean = new TextInputStructJsBean();
            jsBean.setTitle(textInputStruct.getTitle());
            jsBean.setName(textInputStruct.getName());
            jsBean.setLink(textInputStruct.getLink());
            jsBean.setDescription(textInputStruct.getDescription());
        }
        return jsBean;
    }

    public static TextInputStruct convertTextInputStructJsBeanToBean(TextInputStructJsBean jsBean)
    {
        TextInputStructBean textInputStruct = null;
        if(jsBean != null) {
            textInputStruct = new TextInputStructBean();
            textInputStruct.setTitle(jsBean.getTitle());
            textInputStruct.setName(jsBean.getName());
            textInputStruct.setLink(jsBean.getLink());
            textInputStruct.setDescription(jsBean.getDescription());
        }
        return textInputStruct;
    }

}
