package com.feedstoa.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.ChannelSourceJsBean;
import com.feedstoa.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ChannelSourceWebService // implements ChannelSourceService
{
    private static final Logger log = Logger.getLogger(ChannelSourceWebService.class.getName());
     
    // Af service interface.
    private ChannelSourceService mService = null;

    public ChannelSourceWebService()
    {
        this(ServiceProxyFactory.getInstance().getChannelSourceServiceProxy());
    }
    public ChannelSourceWebService(ChannelSourceService service)
    {
        mService = service;
    }
    
    private ChannelSourceService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getChannelSourceServiceProxy();
        }
        return mService;
    }
    
    
    public ChannelSourceJsBean getChannelSource(String guid) throws WebException
    {
        try {
            ChannelSource channelSource = getService().getChannelSource(guid);
            ChannelSourceJsBean bean = convertChannelSourceToJsBean(channelSource);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getChannelSource(String guid, String field) throws WebException
    {
        try {
            return getService().getChannelSource(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ChannelSourceJsBean> getChannelSources(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ChannelSourceJsBean> jsBeans = new ArrayList<ChannelSourceJsBean>();
            List<ChannelSource> channelSources = getService().getChannelSources(guids);
            if(channelSources != null) {
                for(ChannelSource channelSource : channelSources) {
                    jsBeans.add(convertChannelSourceToJsBean(channelSource));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ChannelSourceJsBean> getAllChannelSources() throws WebException
    {
        return getAllChannelSources(null, null, null);
    }

    // @Deprecated
    public List<ChannelSourceJsBean> getAllChannelSources(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllChannelSources(ordering, offset, count, null);
    }

    public List<ChannelSourceJsBean> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ChannelSourceJsBean> jsBeans = new ArrayList<ChannelSourceJsBean>();
            List<ChannelSource> channelSources = getService().getAllChannelSources(ordering, offset, count, forwardCursor);
            if(channelSources != null) {
                for(ChannelSource channelSource : channelSources) {
                    jsBeans.add(convertChannelSourceToJsBean(channelSource));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllChannelSourceKeys(ordering, offset, count, null);
    }

    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllChannelSourceKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<ChannelSourceJsBean> findChannelSources(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findChannelSources(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<ChannelSourceJsBean> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<ChannelSourceJsBean> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ChannelSourceJsBean> jsBeans = new ArrayList<ChannelSourceJsBean>();
            List<ChannelSource> channelSources = getService().findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(channelSources != null) {
                for(ChannelSource channelSource : channelSources) {
                    jsBeans.add(convertChannelSourceToJsBean(channelSource));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createChannelSource(String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws WebException
    {
        try {
            return getService().createChannelSource(user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createChannelSource(ChannelSourceJsBean jsBean) throws WebException
    {
        try {
            ChannelSource channelSource = convertChannelSourceJsBeanToBean(jsBean);
            return getService().createChannelSource(channelSource);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ChannelSourceJsBean constructChannelSource(ChannelSourceJsBean jsBean) throws WebException
    {
        try {
            ChannelSource channelSource = convertChannelSourceJsBeanToBean(jsBean);
            channelSource = getService().constructChannelSource(channelSource);
            jsBean = convertChannelSourceToJsBean(channelSource);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateChannelSource(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws WebException
    {
        try {
            return getService().updateChannelSource(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateChannelSource(ChannelSourceJsBean jsBean) throws WebException
    {
        try {
            ChannelSource channelSource = convertChannelSourceJsBeanToBean(jsBean);
            return getService().updateChannelSource(channelSource);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ChannelSourceJsBean refreshChannelSource(ChannelSourceJsBean jsBean) throws WebException
    {
        try {
            ChannelSource channelSource = convertChannelSourceJsBeanToBean(jsBean);
            channelSource = getService().refreshChannelSource(channelSource);
            jsBean = convertChannelSourceToJsBean(channelSource);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteChannelSource(String guid) throws WebException
    {
        try {
            return getService().deleteChannelSource(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteChannelSource(ChannelSourceJsBean jsBean) throws WebException
    {
        try {
            ChannelSource channelSource = convertChannelSourceJsBeanToBean(jsBean);
            return getService().deleteChannelSource(channelSource);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteChannelSources(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteChannelSources(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ChannelSourceJsBean convertChannelSourceToJsBean(ChannelSource channelSource)
    {
        ChannelSourceJsBean jsBean = null;
        if(channelSource != null) {
            jsBean = new ChannelSourceJsBean();
            jsBean.setGuid(channelSource.getGuid());
            jsBean.setUser(channelSource.getUser());
            jsBean.setServiceName(channelSource.getServiceName());
            jsBean.setServiceUrl(channelSource.getServiceUrl());
            jsBean.setFeedUrl(channelSource.getFeedUrl());
            jsBean.setFeedFormat(channelSource.getFeedFormat());
            jsBean.setChannelTitle(channelSource.getChannelTitle());
            jsBean.setChannelDescription(channelSource.getChannelDescription());
            jsBean.setChannelCategory(channelSource.getChannelCategory());
            jsBean.setChannelUrl(channelSource.getChannelUrl());
            jsBean.setChannelLogo(channelSource.getChannelLogo());
            jsBean.setStatus(channelSource.getStatus());
            jsBean.setNote(channelSource.getNote());
            jsBean.setCreatedTime(channelSource.getCreatedTime());
            jsBean.setModifiedTime(channelSource.getModifiedTime());
        }
        return jsBean;
    }

    public static ChannelSource convertChannelSourceJsBeanToBean(ChannelSourceJsBean jsBean)
    {
        ChannelSourceBean channelSource = null;
        if(jsBean != null) {
            channelSource = new ChannelSourceBean();
            channelSource.setGuid(jsBean.getGuid());
            channelSource.setUser(jsBean.getUser());
            channelSource.setServiceName(jsBean.getServiceName());
            channelSource.setServiceUrl(jsBean.getServiceUrl());
            channelSource.setFeedUrl(jsBean.getFeedUrl());
            channelSource.setFeedFormat(jsBean.getFeedFormat());
            channelSource.setChannelTitle(jsBean.getChannelTitle());
            channelSource.setChannelDescription(jsBean.getChannelDescription());
            channelSource.setChannelCategory(jsBean.getChannelCategory());
            channelSource.setChannelUrl(jsBean.getChannelUrl());
            channelSource.setChannelLogo(jsBean.getChannelLogo());
            channelSource.setStatus(jsBean.getStatus());
            channelSource.setNote(jsBean.getNote());
            channelSource.setCreatedTime(jsBean.getCreatedTime());
            channelSource.setModifiedTime(jsBean.getModifiedTime());
        }
        return channelSource;
    }

}
