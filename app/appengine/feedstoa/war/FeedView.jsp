<%@ page import="com.feedstoa.ws.core.*, com.feedstoa.af.util.*, com.feedstoa.af.auth.*, com.feedstoa.af.auth.user.*, com.feedstoa.fe.*, com.feedstoa.fe.core.*, com.feedstoa.fe.bean.*, com.feedstoa.fe.util.*, com.feedstoa.wa.service.*, com.feedstoa.common.*, com.feedstoa.util.*, com.feedstoa.app.util.*, com.feedstoa.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError.jsp" 
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
// ...
String paramUrl = QueryParamUtil.getParamUrl(request);
String pageUrl = "";
String infoMsg = null;
String errorMsg = null;
if(paramUrl != null && !paramUrl.isEmpty()) {
    if(paramUrl != null && URLUtil.isValidUrl(paramUrl)) {
        pageUrl = paramUrl;
    } else {
        errorMsg = "Invalid URL specified. Input URL = " + paramUrl;
    }
} else {
    infoMsg = "Please specify the Web page URL in the query input box in the top navbar above, including the \"http://\" prefix. "
            + "Here's an example: <a href=\"/feedview?url=http%3A%2F%2Fwww.moma.org%2Fexplore%2Fcollection%2Findex\">http://www.moma.org/explore/collection/index</a>.";
}
// ...
%><%
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
String nickName = "";
String logoutUrl = null;
String comebackUrl = null;
String encodedComebackUrl = null;
if(authUser != null) {
    nickName = authUser.getNickname(); 
    logoutUrl = authUserService.createLogoutURL(request.getRequestURI());
} else {
    comebackUrl = request.getRequestURI();
    try {
        encodedComebackUrl = java.net.URLEncoder.encode(comebackUrl, "UTF-8");
    } catch(Exception e) {
        // ignore
    }
    if(encodedComebackUrl == null) {
        encodedComebackUrl = "";  // ???
    }
}
%><%
//[4] Initial validation
//URL "/granted/list/<guid>" ???
String targetUser = URLHelper.getInstance().getGuidFromPathInfo(request.getPathInfo());
//check first if guid is a valid GUID....
if(targetUser != null && !targetUser.isEmpty()) {   // TBD: validation...
// TBD: Check if guid == userId ????
// ....
} else {
// error
// bail out...
// Or, just user userId
targetUser = userId;  // ???
// ...
}
%><%
// ...
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title>Feed Stoa - Web Page Feed View</title>
    <meta name="author" content="Aery Software">
    <meta name="description" content="Feed Stoa service.">
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="shortcut icon" href="/img/favicon-feedstoa.ico" />
    <link rel="icon" href="/img/favicon-feedstoa.ico" type="image/x-icon" />

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/bootstrap/blueicicle/bootstrap-2.0.css">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/jquery-ui-1.8.20/pepper-grinder/jquery-ui-1.8.20.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-mobile.css" media="only screen and (max-device-width: 480px)"/>
<!-- 
    <link href="/css/bootstrap.responsive-2.0.css" rel="stylesheet">
-->

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://www.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=h0alKXyg4pUisu3N5jppJA&v=1" type="text/javascript"></script>
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/home">Feed Stoa (Beta)</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="/feedview">Feed View</a></li>
            </ul>
            <form class="navbar-search" method="GET" action="/feedview">
              <input id="input_topmenu_pageurl" type="text" class="search-query" placeholder="Feed URL" name="url" value="<%=pageUrl%>"></input>
            </form>
            <p class="navbar-text pull-right">
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+feedstoa+com?subject=Re: Feed Stoa" title="Email us"><i class="icon-envelope icon-white"></i></a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>


    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">


      <div id="images_list_header">


      <div style="text-align: center;">
      <h2>Feed View</h2>
      </div>

      <div>
<%
if(errorMsg != null) {
%>
<div class="alert alert-error alert-block">
  <a class="close" data-dismiss="alert" href="#">×</a>
  <h4 class="alert-heading">Warning!</h4>
  <p><%=errorMsg%></p>
</div>
<%
}
%>    
<%
if(infoMsg != null) {
%>
<div class="alert alert-info alert-block">
  <a class="close" data-dismiss="alert" href="#">×</a>
  <h4 class="alert-heading">How to Fetch Feed View</h4>
  <p><%=infoMsg%></p>
</div>
<%
}
%>    
      </div>
      
      </div>


      <div id="images_body" class="images_list_body">

      <div id="images_list_main" class="images_list_main">


<%
if(pageUrl != null && !pageUrl.isEmpty()) {
ChannelFeedJsBean channelFeedBean = FeedViewHelper.getInstance().getChannelFeed(pageUrl, 60*60*2);
if(channelFeedBean == null) {
%>
<div class="alert alert-error alert-block">
  <a class="close" data-dismiss="alert" href="#">×</a>
  <h4 class="alert-heading">Error!</h4>
  <p>Failed to fetch the Web page: <%=pageUrl%>.</p>
</div>
<%
} else {
String webPageTitle = channelFeedBean.getFeedUrl();
if(webPageTitle == null) {
    webPageTitle = "";
}
%>

      <div style="text-align: center;">
      <h4><%=webPageTitle%>&nbsp;<a href="<%=pageUrl%>" title="Visit the website"><i class="icon-fire"></i></a></h4>
      </div>

<%
//java.util.List<FeedItemJsBean> beans = ImageListHelper.getInstance().getImageList(pageUrl);
java.util.List<FeedItemJsBean> beans = new java.util.ArrayList<FeedItemJsBean>();  // channelFeedBean.getFeedItems();
if(beans == null) {
    // error
%>
<div class="alert alert-error alert-block">
  <a class="close" data-dismiss="alert" href="#">×</a>
  <h4 class="alert-heading">Error!</h4>
  <p>Failed to fetch the image list for the given Web page: <%=pageUrl%>.</p>
</div>
<%
} else if(beans.isEmpty()) {
    // no images found
%>
<div class="alert alert-success alert-block">
  <a class="close" data-dismiss="alert" href="#">×</a>
  <h4 class="alert-heading">Alert!</h4>
  <p>No images were found for the given Web page: <%=pageUrl%>.</p>
</div>
<%
} else {
    // display images
%>
<div style="text-align: center;">
<ul class="thumbnails">
<%
    int counter = 0;
    for(FeedItemJsBean img : beans) {
       String iUuid = ""; // img.getUuid();
       String iSrc = "";  // img.getSrc();
       String iAlt = "";  // img.getAlt();
%>
  <li class="span5">
    <div class="thumbnail">
      <img src="<%=iSrc%>" alt="<%=iAlt%>">
      <h5>Image: <%=iUuid%></h5>
      <p><%=iAlt%></p>
    </div>
  </li>
<%
       if(counter++ >= 100) {  // TBD: Arbitrary cutoff. -> Use pagination???
           break;
       }
    }
%>
</ul>
</div>
<%
}
}
}
%>

      </div>

      </div>

      </div>  <!--   Hero unit  -->


      <hr>

      <footer>
        <p>
        &copy; Feed Stoa 2012&nbsp; 
        <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a>
        <span class="pull-right">
<!-- 
              <a href="/about">About</a> | 
 -->
        <a href="/contact">Contact</a> | <a href="http://blog.feedstoa.com/">Blog</a>
        </span>
        </p>
      </footer>


    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;">
      <div id="status_message">
        <span id="span_status_message">(Status)</span>
      </div>
    </div>
    

    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.7.2.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.8.20.custom.min.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
	<script defer src="/js/plugins.js"></script>
    <script defer src="/js/script.js"></script>
    <script type="text/javascript" src="/js/bean/imagestructjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/webpagejsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/glosshelper-1.0.js"></script>
    <!-- end scripts-->



    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
        var glossHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "Feed Stoa";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "Feed Stoa";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "Feed Stoa";
    	var subject = "Interesting site: Feed Stoa";
    	var defaultMessage = "Hi,\n\nFeed Stoa, http://www.feedstoa.com/, is a Web page image list service. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);

    	glossHelper = new webstoa.GlossHelper();
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>


    <script>
    $(function() {
        $("#anchor_topmenu_tweet").click(function() {
            if(DEBUG_ENABLED) console.log("Tweet button clicked."); 

            if(!tweetHelper) {
                tweetHelper = new webstoa.TweetHelper();
            }
        	if(tweetHelper) {
	            var tweetTitle = 'Feed Stoa Message';
	            var tweetMessage = 'Feed Stoa, http://www.feedstoa.com/, is a Web page image list service. Please check it out.';
	            tweetHelper.tweet(tweetTitle, tweetMessage);
        	} else {
        		// ????
        	}
		    return false;
        });
    });
    </script>


    <script>
    $("#topmenu_nav_view").click(function(e) {
        if(DEBUG_ENABLED) console.log("topmenu_nav_view anchor clicked."); 
   		e.preventDefault();
        $("#topmenu_nav_view").fadeOut(450).fadeIn(950);
        return false;
    });
    </script>


    <script>
    // Event handlers
    $(function() {    	
    	$("#feedstoa_questionmark_link").click(function() {
    	    if(DEBUG_ENABLED) console.log("Question Mark link clicked.");

    	    var memoGuid = "8a47042f-86eb-4399-803b-9409a5623f3f";
    	    glossHelper.showMemo(memoGuid);

    		return false;
    	});
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>

    <script>
    // temporary
    function replaceURLWithHTMLLinks(text) {
        if(text) {
            var exp = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            //var exp = /\(?\bhttp:\/\/[-A-Za-z0-9+&@#\/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]/ig;
            return text.replace(exp,"<a rel='nofollow' href='$1'>$1</a>"); 
        }
        return '';    // ???
    }
    </script>

    <script>
    // "Init"
    $(function() {
    	// TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/oceansunset.jpg)');
        //$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");

        // TBD: Location...
    	var params = {x: 530, y: 75, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(21889, 12);  // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    </script>


    <script>
    // Event handlers for the list...
    $(function() {

        $("button.button_images_list_view").live('click', function() {
            if(DEBUG_ENABLED) console.log("View memo button clicked.");
            //window.alert('Not implemented yet.');
            //window.alert('target: ' + event.target.id);

            //var guid =  event.target.id.substring(12);  // Note: this does not work on IE....
            var guid =  $(this).attr('id').substring(12);  // prefix: "button_view_"
            //window.alert('guid: ' + guid);

            var viewPageUrl = "/view/" + guid;
            window.location = viewPageUrl; 
        });

    	
    });
    </script>




<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

    </body>
</html>