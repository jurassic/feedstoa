//////////////////////////////////////////////////////////
// <script src="/js/bean/categorystructjsbean-1.0.js"></script>
// Last modified time: 1377391580483.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.CategoryStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var domain;
    var label;
    var title;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getLabel = function() { return label; };
    this.setLabel = function(value) { label = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.CategoryStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(label !== undefined && label != null) {
        o.setLabel(label);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(label !== undefined && label != null) {
        jsonObj.label = label;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(label) {
        str += "\"label\":\"" + label + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"label\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "domain:" + domain + ", ";
      str += "label:" + label + ", ";
      str += "title:" + title + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.CategoryStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.CategoryStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.label !== undefined && obj.label != null) {
    o.setLabel(obj.label);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
    
  return o;
};

feedstoa.wa.bean.CategoryStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.CategoryStructJsBean.create(jsonObj);
  return obj;
};
