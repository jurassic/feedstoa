//////////////////////////////////////////////////////////
// <script src="/js/bean/rsschanneljsbean-1.0.js"></script>
// Last modified time: 1377391580551.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.RssChannelJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var title;
    var link;
    var description;
    var language;
    var copyright;
    var managingEditor;
    var webMaster;
    var pubDate;
    var lastBuildDate;
    var category;
    var generator;
    var docs;
    var cloud;
    var ttl;
    var image;
    var rating;
    var textInput;
    var skipHours;
    var skipDays;
    var item;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getLink = function() { return link; };
    this.setLink = function(value) { link = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getLanguage = function() { return language; };
    this.setLanguage = function(value) { language = value; };
    this.getCopyright = function() { return copyright; };
    this.setCopyright = function(value) { copyright = value; };
    this.getManagingEditor = function() { return managingEditor; };
    this.setManagingEditor = function(value) { managingEditor = value; };
    this.getWebMaster = function() { return webMaster; };
    this.setWebMaster = function(value) { webMaster = value; };
    this.getPubDate = function() { return pubDate; };
    this.setPubDate = function(value) { pubDate = value; };
    this.getLastBuildDate = function() { return lastBuildDate; };
    this.setLastBuildDate = function(value) { lastBuildDate = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getGenerator = function() { return generator; };
    this.setGenerator = function(value) { generator = value; };
    this.getDocs = function() { return docs; };
    this.setDocs = function(value) { docs = value; };
    this.getCloud = function() { return cloud; };
    this.setCloud = function(value) { cloud = value; };
    this.getTtl = function() { return ttl; };
    this.setTtl = function(value) { ttl = value; };
    this.getImage = function() { return image; };
    this.setImage = function(value) { image = value; };
    this.getRating = function() { return rating; };
    this.setRating = function(value) { rating = value; };
    this.getTextInput = function() { return textInput; };
    this.setTextInput = function(value) { textInput = value; };
    this.getSkipHours = function() { return skipHours; };
    this.setSkipHours = function(value) { skipHours = value; };
    this.getSkipDays = function() { return skipDays; };
    this.setSkipDays = function(value) { skipDays = value; };
    this.getItem = function() { return item; };
    this.setItem = function(value) { item = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.RssChannelJsBean();

      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(link !== undefined && link != null) {
        o.setLink(link);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(language !== undefined && language != null) {
        o.setLanguage(language);
      }
      if(copyright !== undefined && copyright != null) {
        o.setCopyright(copyright);
      }
      if(managingEditor !== undefined && managingEditor != null) {
        o.setManagingEditor(managingEditor);
      }
      if(webMaster !== undefined && webMaster != null) {
        o.setWebMaster(webMaster);
      }
      if(pubDate !== undefined && pubDate != null) {
        o.setPubDate(pubDate);
      }
      if(lastBuildDate !== undefined && lastBuildDate != null) {
        o.setLastBuildDate(lastBuildDate);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(generator !== undefined && generator != null) {
        o.setGenerator(generator);
      }
      if(docs !== undefined && docs != null) {
        o.setDocs(docs);
      }
      //o.setCloud(cloud.clone());
      if(cloud !== undefined && cloud != null) {
        o.setCloud(cloud);
      }
      if(ttl !== undefined && ttl != null) {
        o.setTtl(ttl);
      }
      //o.setImage(image.clone());
      if(image !== undefined && image != null) {
        o.setImage(image);
      }
      if(rating !== undefined && rating != null) {
        o.setRating(rating);
      }
      //o.setTextInput(textInput.clone());
      if(textInput !== undefined && textInput != null) {
        o.setTextInput(textInput);
      }
      if(skipHours !== undefined && skipHours != null) {
        o.setSkipHours(skipHours);
      }
      if(skipDays !== undefined && skipDays != null) {
        o.setSkipDays(skipDays);
      }
      if(item !== undefined && item != null) {
        o.setItem(item);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(link !== undefined && link != null) {
        jsonObj.link = link;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(language !== undefined && language != null) {
        jsonObj.language = language;
      } // Otherwise ignore...
      if(copyright !== undefined && copyright != null) {
        jsonObj.copyright = copyright;
      } // Otherwise ignore...
      if(managingEditor !== undefined && managingEditor != null) {
        jsonObj.managingEditor = managingEditor;
      } // Otherwise ignore...
      if(webMaster !== undefined && webMaster != null) {
        jsonObj.webMaster = webMaster;
      } // Otherwise ignore...
      if(pubDate !== undefined && pubDate != null) {
        jsonObj.pubDate = pubDate;
      } // Otherwise ignore...
      if(lastBuildDate !== undefined && lastBuildDate != null) {
        jsonObj.lastBuildDate = lastBuildDate;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(generator !== undefined && generator != null) {
        jsonObj.generator = generator;
      } // Otherwise ignore...
      if(docs !== undefined && docs != null) {
        jsonObj.docs = docs;
      } // Otherwise ignore...
      if(cloud !== undefined && cloud != null) {
        jsonObj.cloud = cloud;
      } // Otherwise ignore...
      if(ttl !== undefined && ttl != null) {
        jsonObj.ttl = ttl;
      } // Otherwise ignore...
      if(image !== undefined && image != null) {
        jsonObj.image = image;
      } // Otherwise ignore...
      if(rating !== undefined && rating != null) {
        jsonObj.rating = rating;
      } // Otherwise ignore...
      if(textInput !== undefined && textInput != null) {
        jsonObj.textInput = textInput;
      } // Otherwise ignore...
      if(skipHours !== undefined && skipHours != null) {
        jsonObj.skipHours = skipHours;
      } // Otherwise ignore...
      if(skipDays !== undefined && skipDays != null) {
        jsonObj.skipDays = skipDays;
      } // Otherwise ignore...
      if(item !== undefined && item != null) {
        jsonObj.item = item;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(link) {
        str += "\"link\":\"" + link + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"link\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(language) {
        str += "\"language\":\"" + language + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"language\":null, ";
      }
      if(copyright) {
        str += "\"copyright\":\"" + copyright + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"copyright\":null, ";
      }
      if(managingEditor) {
        str += "\"managingEditor\":\"" + managingEditor + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managingEditor\":null, ";
      }
      if(webMaster) {
        str += "\"webMaster\":\"" + webMaster + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"webMaster\":null, ";
      }
      if(pubDate) {
        str += "\"pubDate\":\"" + pubDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pubDate\":null, ";
      }
      if(lastBuildDate) {
        str += "\"lastBuildDate\":\"" + lastBuildDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastBuildDate\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(generator) {
        str += "\"generator\":\"" + generator + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"generator\":null, ";
      }
      if(docs) {
        str += "\"docs\":\"" + docs + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"docs\":null, ";
      }
      str += "\"cloud\":" + cloud.toJsonString() + ", ";
      if(ttl) {
        str += "\"ttl\":" + ttl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ttl\":null, ";
      }
      str += "\"image\":" + image.toJsonString() + ", ";
      if(rating) {
        str += "\"rating\":\"" + rating + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"rating\":null, ";
      }
      str += "\"textInput\":" + textInput.toJsonString() + ", ";
      if(skipHours) {
        str += "\"skipHours\":\"" + skipHours + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"skipHours\":null, ";
      }
      if(skipDays) {
        str += "\"skipDays\":\"" + skipDays + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"skipDays\":null, ";
      }
      if(item) {
        str += "\"item\":\"" + item + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"item\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "title:" + title + ", ";
      str += "link:" + link + ", ";
      str += "description:" + description + ", ";
      str += "language:" + language + ", ";
      str += "copyright:" + copyright + ", ";
      str += "managingEditor:" + managingEditor + ", ";
      str += "webMaster:" + webMaster + ", ";
      str += "pubDate:" + pubDate + ", ";
      str += "lastBuildDate:" + lastBuildDate + ", ";
      str += "category:" + category + ", ";
      str += "generator:" + generator + ", ";
      str += "docs:" + docs + ", ";
      str += "cloud:" + cloud + ", ";
      str += "ttl:" + ttl + ", ";
      str += "image:" + image + ", ";
      str += "rating:" + rating + ", ";
      str += "textInput:" + textInput + ", ";
      str += "skipHours:" + skipHours + ", ";
      str += "skipDays:" + skipDays + ", ";
      str += "item:" + item + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.RssChannelJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.RssChannelJsBean();

  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.link !== undefined && obj.link != null) {
    o.setLink(obj.link);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.language !== undefined && obj.language != null) {
    o.setLanguage(obj.language);
  }
  if(obj.copyright !== undefined && obj.copyright != null) {
    o.setCopyright(obj.copyright);
  }
  if(obj.managingEditor !== undefined && obj.managingEditor != null) {
    o.setManagingEditor(obj.managingEditor);
  }
  if(obj.webMaster !== undefined && obj.webMaster != null) {
    o.setWebMaster(obj.webMaster);
  }
  if(obj.pubDate !== undefined && obj.pubDate != null) {
    o.setPubDate(obj.pubDate);
  }
  if(obj.lastBuildDate !== undefined && obj.lastBuildDate != null) {
    o.setLastBuildDate(obj.lastBuildDate);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.generator !== undefined && obj.generator != null) {
    o.setGenerator(obj.generator);
  }
  if(obj.docs !== undefined && obj.docs != null) {
    o.setDocs(obj.docs);
  }
  if(obj.cloud !== undefined && obj.cloud != null) {
    o.setCloud(obj.cloud);
  }
  if(obj.ttl !== undefined && obj.ttl != null) {
    o.setTtl(obj.ttl);
  }
  if(obj.image !== undefined && obj.image != null) {
    o.setImage(obj.image);
  }
  if(obj.rating !== undefined && obj.rating != null) {
    o.setRating(obj.rating);
  }
  if(obj.textInput !== undefined && obj.textInput != null) {
    o.setTextInput(obj.textInput);
  }
  if(obj.skipHours !== undefined && obj.skipHours != null) {
    o.setSkipHours(obj.skipHours);
  }
  if(obj.skipDays !== undefined && obj.skipDays != null) {
    o.setSkipDays(obj.skipDays);
  }
  if(obj.item !== undefined && obj.item != null) {
    o.setItem(obj.item);
  }
    
  return o;
};

feedstoa.wa.bean.RssChannelJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.RssChannelJsBean.create(jsonObj);
  return obj;
};
