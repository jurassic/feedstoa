//////////////////////////////////////////////////////////
// <script src="/js/bean/uristructjsbean-1.0.js"></script>
// Last modified time: 1377391580461.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.UriStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var href;
    var rel;
    var type;
    var label;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getHref = function() { return href; };
    this.setHref = function(value) { href = value; };
    this.getRel = function() { return rel; };
    this.setRel = function(value) { rel = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getLabel = function() { return label; };
    this.setLabel = function(value) { label = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.UriStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(href !== undefined && href != null) {
        o.setHref(href);
      }
      if(rel !== undefined && rel != null) {
        o.setRel(rel);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(label !== undefined && label != null) {
        o.setLabel(label);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(href !== undefined && href != null) {
        jsonObj.href = href;
      } // Otherwise ignore...
      if(rel !== undefined && rel != null) {
        jsonObj.rel = rel;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(label !== undefined && label != null) {
        jsonObj.label = label;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(href) {
        str += "\"href\":\"" + href + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"href\":null, ";
      }
      if(rel) {
        str += "\"rel\":\"" + rel + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"rel\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(label) {
        str += "\"label\":\"" + label + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"label\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "href:" + href + ", ";
      str += "rel:" + rel + ", ";
      str += "type:" + type + ", ";
      str += "label:" + label + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.UriStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.UriStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.href !== undefined && obj.href != null) {
    o.setHref(obj.href);
  }
  if(obj.rel !== undefined && obj.rel != null) {
    o.setRel(obj.rel);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.label !== undefined && obj.label != null) {
    o.setLabel(obj.label);
  }
    
  return o;
};

feedstoa.wa.bean.UriStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.UriStructJsBean.create(jsonObj);
  return obj;
};
