//////////////////////////////////////////////////////////
// <script src="/js/bean/fetchrequestjsbean-1.0.js"></script>
// Last modified time: 1377391580400.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.FetchRequestJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var ownerUser;
    var userAcl;
    var user;
    var title;
    var description;
    var fetchUrl;
    var feedUrl;
    var channelFeed;
    var reuseChannel;
    var maxItemCount;
    var note;
    var status;
    var originFetch;
    var outputFormat;
    var fetchStatus;
    var result;
    var feedCategory;
    var multipleFeedEnabled;
    var deferred;
    var alert;
    var notificationPref;
    var referrerInfo;
    var refreshInterval;
    var refreshExpressions;
    var refreshTimeZone;
    var currentRefreshCount;
    var maxRefreshCount;
    var refreshExpirationTime;
    var nextRefreshTime;
    var lastUpdatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getOwnerUser = function() { return ownerUser; };
    this.setOwnerUser = function(value) { ownerUser = value; };
    this.getUserAcl = function() { return userAcl; };
    this.setUserAcl = function(value) { userAcl = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getFetchUrl = function() { return fetchUrl; };
    this.setFetchUrl = function(value) { fetchUrl = value; };
    this.getFeedUrl = function() { return feedUrl; };
    this.setFeedUrl = function(value) { feedUrl = value; };
    this.getChannelFeed = function() { return channelFeed; };
    this.setChannelFeed = function(value) { channelFeed = value; };
    this.getReuseChannel = function() { return reuseChannel; };
    this.setReuseChannel = function(value) { reuseChannel = value; };
    this.getMaxItemCount = function() { return maxItemCount; };
    this.setMaxItemCount = function(value) { maxItemCount = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getOriginFetch = function() { return originFetch; };
    this.setOriginFetch = function(value) { originFetch = value; };
    this.getOutputFormat = function() { return outputFormat; };
    this.setOutputFormat = function(value) { outputFormat = value; };
    this.getFetchStatus = function() { return fetchStatus; };
    this.setFetchStatus = function(value) { fetchStatus = value; };
    this.getResult = function() { return result; };
    this.setResult = function(value) { result = value; };
    this.getFeedCategory = function() { return feedCategory; };
    this.setFeedCategory = function(value) { feedCategory = value; };
    this.getMultipleFeedEnabled = function() { return multipleFeedEnabled; };
    this.setMultipleFeedEnabled = function(value) { multipleFeedEnabled = value; };
    this.getDeferred = function() { return deferred; };
    this.setDeferred = function(value) { deferred = value; };
    this.getAlert = function() { return alert; };
    this.setAlert = function(value) { alert = value; };
    this.getNotificationPref = function() { return notificationPref; };
    this.setNotificationPref = function(value) { notificationPref = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getRefreshInterval = function() { return refreshInterval; };
    this.setRefreshInterval = function(value) { refreshInterval = value; };
    this.getRefreshExpressions = function() { return refreshExpressions; };
    this.setRefreshExpressions = function(value) { refreshExpressions = value; };
    this.getRefreshTimeZone = function() { return refreshTimeZone; };
    this.setRefreshTimeZone = function(value) { refreshTimeZone = value; };
    this.getCurrentRefreshCount = function() { return currentRefreshCount; };
    this.setCurrentRefreshCount = function(value) { currentRefreshCount = value; };
    this.getMaxRefreshCount = function() { return maxRefreshCount; };
    this.setMaxRefreshCount = function(value) { maxRefreshCount = value; };
    this.getRefreshExpirationTime = function() { return refreshExpirationTime; };
    this.setRefreshExpirationTime = function(value) { refreshExpirationTime = value; };
    this.getNextRefreshTime = function() { return nextRefreshTime; };
    this.setNextRefreshTime = function(value) { nextRefreshTime = value; };
    this.getLastUpdatedTime = function() { return lastUpdatedTime; };
    this.setLastUpdatedTime = function(value) { lastUpdatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.FetchRequestJsBean();

      o.setGuid(generateUuid());
      if(managerApp !== undefined && managerApp != null) {
        o.setManagerApp(managerApp);
      }
      if(appAcl !== undefined && appAcl != null) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp !== undefined && gaeApp != null) {
        o.setGaeApp(gaeApp);
      }
      if(ownerUser !== undefined && ownerUser != null) {
        o.setOwnerUser(ownerUser);
      }
      if(userAcl !== undefined && userAcl != null) {
        o.setUserAcl(userAcl);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(fetchUrl !== undefined && fetchUrl != null) {
        o.setFetchUrl(fetchUrl);
      }
      if(feedUrl !== undefined && feedUrl != null) {
        o.setFeedUrl(feedUrl);
      }
      if(channelFeed !== undefined && channelFeed != null) {
        o.setChannelFeed(channelFeed);
      }
      if(reuseChannel !== undefined && reuseChannel != null) {
        o.setReuseChannel(reuseChannel);
      }
      if(maxItemCount !== undefined && maxItemCount != null) {
        o.setMaxItemCount(maxItemCount);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(originFetch !== undefined && originFetch != null) {
        o.setOriginFetch(originFetch);
      }
      if(outputFormat !== undefined && outputFormat != null) {
        o.setOutputFormat(outputFormat);
      }
      if(fetchStatus !== undefined && fetchStatus != null) {
        o.setFetchStatus(fetchStatus);
      }
      if(result !== undefined && result != null) {
        o.setResult(result);
      }
      if(feedCategory !== undefined && feedCategory != null) {
        o.setFeedCategory(feedCategory);
      }
      if(multipleFeedEnabled !== undefined && multipleFeedEnabled != null) {
        o.setMultipleFeedEnabled(multipleFeedEnabled);
      }
      if(deferred !== undefined && deferred != null) {
        o.setDeferred(deferred);
      }
      if(alert !== undefined && alert != null) {
        o.setAlert(alert);
      }
      //o.setNotificationPref(notificationPref.clone());
      if(notificationPref !== undefined && notificationPref != null) {
        o.setNotificationPref(notificationPref);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(refreshInterval !== undefined && refreshInterval != null) {
        o.setRefreshInterval(refreshInterval);
      }
      if(refreshExpressions !== undefined && refreshExpressions != null) {
        o.setRefreshExpressions(refreshExpressions);
      }
      if(refreshTimeZone !== undefined && refreshTimeZone != null) {
        o.setRefreshTimeZone(refreshTimeZone);
      }
      if(currentRefreshCount !== undefined && currentRefreshCount != null) {
        o.setCurrentRefreshCount(currentRefreshCount);
      }
      if(maxRefreshCount !== undefined && maxRefreshCount != null) {
        o.setMaxRefreshCount(maxRefreshCount);
      }
      if(refreshExpirationTime !== undefined && refreshExpirationTime != null) {
        o.setRefreshExpirationTime(refreshExpirationTime);
      }
      if(nextRefreshTime !== undefined && nextRefreshTime != null) {
        o.setNextRefreshTime(nextRefreshTime);
      }
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        o.setLastUpdatedTime(lastUpdatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp !== undefined && managerApp != null) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl !== undefined && appAcl != null) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp !== undefined && gaeApp != null) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(ownerUser !== undefined && ownerUser != null) {
        jsonObj.ownerUser = ownerUser;
      } // Otherwise ignore...
      if(userAcl !== undefined && userAcl != null) {
        jsonObj.userAcl = userAcl;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(fetchUrl !== undefined && fetchUrl != null) {
        jsonObj.fetchUrl = fetchUrl;
      } // Otherwise ignore...
      if(feedUrl !== undefined && feedUrl != null) {
        jsonObj.feedUrl = feedUrl;
      } // Otherwise ignore...
      if(channelFeed !== undefined && channelFeed != null) {
        jsonObj.channelFeed = channelFeed;
      } // Otherwise ignore...
      if(reuseChannel !== undefined && reuseChannel != null) {
        jsonObj.reuseChannel = reuseChannel;
      } // Otherwise ignore...
      if(maxItemCount !== undefined && maxItemCount != null) {
        jsonObj.maxItemCount = maxItemCount;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(originFetch !== undefined && originFetch != null) {
        jsonObj.originFetch = originFetch;
      } // Otherwise ignore...
      if(outputFormat !== undefined && outputFormat != null) {
        jsonObj.outputFormat = outputFormat;
      } // Otherwise ignore...
      if(fetchStatus !== undefined && fetchStatus != null) {
        jsonObj.fetchStatus = fetchStatus;
      } // Otherwise ignore...
      if(result !== undefined && result != null) {
        jsonObj.result = result;
      } // Otherwise ignore...
      if(feedCategory !== undefined && feedCategory != null) {
        jsonObj.feedCategory = feedCategory;
      } // Otherwise ignore...
      if(multipleFeedEnabled !== undefined && multipleFeedEnabled != null) {
        jsonObj.multipleFeedEnabled = multipleFeedEnabled;
      } // Otherwise ignore...
      if(deferred !== undefined && deferred != null) {
        jsonObj.deferred = deferred;
      } // Otherwise ignore...
      if(alert !== undefined && alert != null) {
        jsonObj.alert = alert;
      } // Otherwise ignore...
      if(notificationPref !== undefined && notificationPref != null) {
        jsonObj.notificationPref = notificationPref;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(refreshInterval !== undefined && refreshInterval != null) {
        jsonObj.refreshInterval = refreshInterval;
      } // Otherwise ignore...
      if(refreshExpressions !== undefined && refreshExpressions != null) {
        jsonObj.refreshExpressions = refreshExpressions;
      } // Otherwise ignore...
      if(refreshTimeZone !== undefined && refreshTimeZone != null) {
        jsonObj.refreshTimeZone = refreshTimeZone;
      } // Otherwise ignore...
      if(currentRefreshCount !== undefined && currentRefreshCount != null) {
        jsonObj.currentRefreshCount = currentRefreshCount;
      } // Otherwise ignore...
      if(maxRefreshCount !== undefined && maxRefreshCount != null) {
        jsonObj.maxRefreshCount = maxRefreshCount;
      } // Otherwise ignore...
      if(refreshExpirationTime !== undefined && refreshExpirationTime != null) {
        jsonObj.refreshExpirationTime = refreshExpirationTime;
      } // Otherwise ignore...
      if(nextRefreshTime !== undefined && nextRefreshTime != null) {
        jsonObj.nextRefreshTime = nextRefreshTime;
      } // Otherwise ignore...
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        jsonObj.lastUpdatedTime = lastUpdatedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(ownerUser) {
        str += "\"ownerUser\":\"" + ownerUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ownerUser\":null, ";
      }
      if(userAcl) {
        str += "\"userAcl\":" + userAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAcl\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(fetchUrl) {
        str += "\"fetchUrl\":\"" + fetchUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchUrl\":null, ";
      }
      if(feedUrl) {
        str += "\"feedUrl\":\"" + feedUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedUrl\":null, ";
      }
      if(channelFeed) {
        str += "\"channelFeed\":\"" + channelFeed + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelFeed\":null, ";
      }
      if(reuseChannel) {
        str += "\"reuseChannel\":" + reuseChannel + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reuseChannel\":null, ";
      }
      if(maxItemCount) {
        str += "\"maxItemCount\":" + maxItemCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"maxItemCount\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(originFetch) {
        str += "\"originFetch\":\"" + originFetch + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"originFetch\":null, ";
      }
      if(outputFormat) {
        str += "\"outputFormat\":\"" + outputFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputFormat\":null, ";
      }
      if(fetchStatus) {
        str += "\"fetchStatus\":" + fetchStatus + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchStatus\":null, ";
      }
      if(result) {
        str += "\"result\":\"" + result + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"result\":null, ";
      }
      if(feedCategory) {
        str += "\"feedCategory\":\"" + feedCategory + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedCategory\":null, ";
      }
      if(multipleFeedEnabled) {
        str += "\"multipleFeedEnabled\":" + multipleFeedEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"multipleFeedEnabled\":null, ";
      }
      if(deferred) {
        str += "\"deferred\":" + deferred + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"deferred\":null, ";
      }
      if(alert) {
        str += "\"alert\":" + alert + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"alert\":null, ";
      }
      str += "\"notificationPref\":" + notificationPref.toJsonString() + ", ";
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(refreshInterval) {
        str += "\"refreshInterval\":" + refreshInterval + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshInterval\":null, ";
      }
      if(refreshExpressions) {
        str += "\"refreshExpressions\":\"" + refreshExpressions + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshExpressions\":null, ";
      }
      if(refreshTimeZone) {
        str += "\"refreshTimeZone\":\"" + refreshTimeZone + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshTimeZone\":null, ";
      }
      if(currentRefreshCount) {
        str += "\"currentRefreshCount\":" + currentRefreshCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"currentRefreshCount\":null, ";
      }
      if(maxRefreshCount) {
        str += "\"maxRefreshCount\":" + maxRefreshCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"maxRefreshCount\":null, ";
      }
      if(refreshExpirationTime) {
        str += "\"refreshExpirationTime\":" + refreshExpirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshExpirationTime\":null, ";
      }
      if(nextRefreshTime) {
        str += "\"nextRefreshTime\":" + nextRefreshTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextRefreshTime\":null, ";
      }
      if(lastUpdatedTime) {
        str += "\"lastUpdatedTime\":" + lastUpdatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastUpdatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "ownerUser:" + ownerUser + ", ";
      str += "userAcl:" + userAcl + ", ";
      str += "user:" + user + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "fetchUrl:" + fetchUrl + ", ";
      str += "feedUrl:" + feedUrl + ", ";
      str += "channelFeed:" + channelFeed + ", ";
      str += "reuseChannel:" + reuseChannel + ", ";
      str += "maxItemCount:" + maxItemCount + ", ";
      str += "note:" + note + ", ";
      str += "status:" + status + ", ";
      str += "originFetch:" + originFetch + ", ";
      str += "outputFormat:" + outputFormat + ", ";
      str += "fetchStatus:" + fetchStatus + ", ";
      str += "result:" + result + ", ";
      str += "feedCategory:" + feedCategory + ", ";
      str += "multipleFeedEnabled:" + multipleFeedEnabled + ", ";
      str += "deferred:" + deferred + ", ";
      str += "alert:" + alert + ", ";
      str += "notificationPref:" + notificationPref + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "refreshInterval:" + refreshInterval + ", ";
      str += "refreshExpressions:" + refreshExpressions + ", ";
      str += "refreshTimeZone:" + refreshTimeZone + ", ";
      str += "currentRefreshCount:" + currentRefreshCount + ", ";
      str += "maxRefreshCount:" + maxRefreshCount + ", ";
      str += "refreshExpirationTime:" + refreshExpirationTime + ", ";
      str += "nextRefreshTime:" + nextRefreshTime + ", ";
      str += "lastUpdatedTime:" + lastUpdatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.FetchRequestJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.FetchRequestJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp !== undefined && obj.managerApp != null) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl !== undefined && obj.appAcl != null) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp !== undefined && obj.gaeApp != null) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.ownerUser !== undefined && obj.ownerUser != null) {
    o.setOwnerUser(obj.ownerUser);
  }
  if(obj.userAcl !== undefined && obj.userAcl != null) {
    o.setUserAcl(obj.userAcl);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.fetchUrl !== undefined && obj.fetchUrl != null) {
    o.setFetchUrl(obj.fetchUrl);
  }
  if(obj.feedUrl !== undefined && obj.feedUrl != null) {
    o.setFeedUrl(obj.feedUrl);
  }
  if(obj.channelFeed !== undefined && obj.channelFeed != null) {
    o.setChannelFeed(obj.channelFeed);
  }
  if(obj.reuseChannel !== undefined && obj.reuseChannel != null) {
    o.setReuseChannel(obj.reuseChannel);
  }
  if(obj.maxItemCount !== undefined && obj.maxItemCount != null) {
    o.setMaxItemCount(obj.maxItemCount);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.originFetch !== undefined && obj.originFetch != null) {
    o.setOriginFetch(obj.originFetch);
  }
  if(obj.outputFormat !== undefined && obj.outputFormat != null) {
    o.setOutputFormat(obj.outputFormat);
  }
  if(obj.fetchStatus !== undefined && obj.fetchStatus != null) {
    o.setFetchStatus(obj.fetchStatus);
  }
  if(obj.result !== undefined && obj.result != null) {
    o.setResult(obj.result);
  }
  if(obj.feedCategory !== undefined && obj.feedCategory != null) {
    o.setFeedCategory(obj.feedCategory);
  }
  if(obj.multipleFeedEnabled !== undefined && obj.multipleFeedEnabled != null) {
    o.setMultipleFeedEnabled(obj.multipleFeedEnabled);
  }
  if(obj.deferred !== undefined && obj.deferred != null) {
    o.setDeferred(obj.deferred);
  }
  if(obj.alert !== undefined && obj.alert != null) {
    o.setAlert(obj.alert);
  }
  if(obj.notificationPref !== undefined && obj.notificationPref != null) {
    o.setNotificationPref(obj.notificationPref);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.refreshInterval !== undefined && obj.refreshInterval != null) {
    o.setRefreshInterval(obj.refreshInterval);
  }
  if(obj.refreshExpressions !== undefined && obj.refreshExpressions != null) {
    o.setRefreshExpressions(obj.refreshExpressions);
  }
  if(obj.refreshTimeZone !== undefined && obj.refreshTimeZone != null) {
    o.setRefreshTimeZone(obj.refreshTimeZone);
  }
  if(obj.currentRefreshCount !== undefined && obj.currentRefreshCount != null) {
    o.setCurrentRefreshCount(obj.currentRefreshCount);
  }
  if(obj.maxRefreshCount !== undefined && obj.maxRefreshCount != null) {
    o.setMaxRefreshCount(obj.maxRefreshCount);
  }
  if(obj.refreshExpirationTime !== undefined && obj.refreshExpirationTime != null) {
    o.setRefreshExpirationTime(obj.refreshExpirationTime);
  }
  if(obj.nextRefreshTime !== undefined && obj.nextRefreshTime != null) {
    o.setNextRefreshTime(obj.nextRefreshTime);
  }
  if(obj.lastUpdatedTime !== undefined && obj.lastUpdatedTime != null) {
    o.setLastUpdatedTime(obj.lastUpdatedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

feedstoa.wa.bean.FetchRequestJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.FetchRequestJsBean.create(jsonObj);
  return obj;
};
