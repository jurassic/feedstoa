//////////////////////////////////////////////////////////
// <script src="/js/bean/cloudstructjsbean-1.0.js"></script>
// Last modified time: 1377391580491.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.CloudStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var domain;
    var port;
    var path;
    var registerProcedure;
    var protocol;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getPort = function() { return port; };
    this.setPort = function(value) { port = value; };
    this.getPath = function() { return path; };
    this.setPath = function(value) { path = value; };
    this.getRegisterProcedure = function() { return registerProcedure; };
    this.setRegisterProcedure = function(value) { registerProcedure = value; };
    this.getProtocol = function() { return protocol; };
    this.setProtocol = function(value) { protocol = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.CloudStructJsBean();

      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(port !== undefined && port != null) {
        o.setPort(port);
      }
      if(path !== undefined && path != null) {
        o.setPath(path);
      }
      if(registerProcedure !== undefined && registerProcedure != null) {
        o.setRegisterProcedure(registerProcedure);
      }
      if(protocol !== undefined && protocol != null) {
        o.setProtocol(protocol);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(port !== undefined && port != null) {
        jsonObj.port = port;
      } // Otherwise ignore...
      if(path !== undefined && path != null) {
        jsonObj.path = path;
      } // Otherwise ignore...
      if(registerProcedure !== undefined && registerProcedure != null) {
        jsonObj.registerProcedure = registerProcedure;
      } // Otherwise ignore...
      if(protocol !== undefined && protocol != null) {
        jsonObj.protocol = protocol;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(port) {
        str += "\"port\":" + port + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"port\":null, ";
      }
      if(path) {
        str += "\"path\":\"" + path + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"path\":null, ";
      }
      if(registerProcedure) {
        str += "\"registerProcedure\":\"" + registerProcedure + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"registerProcedure\":null, ";
      }
      if(protocol) {
        str += "\"protocol\":\"" + protocol + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"protocol\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "domain:" + domain + ", ";
      str += "port:" + port + ", ";
      str += "path:" + path + ", ";
      str += "registerProcedure:" + registerProcedure + ", ";
      str += "protocol:" + protocol + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.CloudStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.CloudStructJsBean();

  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.port !== undefined && obj.port != null) {
    o.setPort(obj.port);
  }
  if(obj.path !== undefined && obj.path != null) {
    o.setPath(obj.path);
  }
  if(obj.registerProcedure !== undefined && obj.registerProcedure != null) {
    o.setRegisterProcedure(obj.registerProcedure);
  }
  if(obj.protocol !== undefined && obj.protocol != null) {
    o.setProtocol(obj.protocol);
  }
    
  return o;
};

feedstoa.wa.bean.CloudStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.CloudStructJsBean.create(jsonObj);
  return obj;
};
