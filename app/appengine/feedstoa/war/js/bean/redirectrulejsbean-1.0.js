//////////////////////////////////////////////////////////
// <script src="/js/bean/redirectrulejsbean-1.0.js"></script>
// Last modified time: 1377391580602.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.RedirectRuleJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var redirectType;
    var precedence;
    var sourceDomain;
    var sourcePath;
    var targetDomain;
    var targetPath;
    var note;
    var status;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getRedirectType = function() { return redirectType; };
    this.setRedirectType = function(value) { redirectType = value; };
    this.getPrecedence = function() { return precedence; };
    this.setPrecedence = function(value) { precedence = value; };
    this.getSourceDomain = function() { return sourceDomain; };
    this.setSourceDomain = function(value) { sourceDomain = value; };
    this.getSourcePath = function() { return sourcePath; };
    this.setSourcePath = function(value) { sourcePath = value; };
    this.getTargetDomain = function() { return targetDomain; };
    this.setTargetDomain = function(value) { targetDomain = value; };
    this.getTargetPath = function() { return targetPath; };
    this.setTargetPath = function(value) { targetPath = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.RedirectRuleJsBean();

      if(redirectType !== undefined && redirectType != null) {
        o.setRedirectType(redirectType);
      }
      if(precedence !== undefined && precedence != null) {
        o.setPrecedence(precedence);
      }
      if(sourceDomain !== undefined && sourceDomain != null) {
        o.setSourceDomain(sourceDomain);
      }
      if(sourcePath !== undefined && sourcePath != null) {
        o.setSourcePath(sourcePath);
      }
      if(targetDomain !== undefined && targetDomain != null) {
        o.setTargetDomain(targetDomain);
      }
      if(targetPath !== undefined && targetPath != null) {
        o.setTargetPath(targetPath);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(redirectType !== undefined && redirectType != null) {
        jsonObj.redirectType = redirectType;
      } // Otherwise ignore...
      if(precedence !== undefined && precedence != null) {
        jsonObj.precedence = precedence;
      } // Otherwise ignore...
      if(sourceDomain !== undefined && sourceDomain != null) {
        jsonObj.sourceDomain = sourceDomain;
      } // Otherwise ignore...
      if(sourcePath !== undefined && sourcePath != null) {
        jsonObj.sourcePath = sourcePath;
      } // Otherwise ignore...
      if(targetDomain !== undefined && targetDomain != null) {
        jsonObj.targetDomain = targetDomain;
      } // Otherwise ignore...
      if(targetPath !== undefined && targetPath != null) {
        jsonObj.targetPath = targetPath;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(redirectType) {
        str += "\"redirectType\":\"" + redirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirectType\":null, ";
      }
      if(precedence) {
        str += "\"precedence\":" + precedence + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"precedence\":null, ";
      }
      if(sourceDomain) {
        str += "\"sourceDomain\":\"" + sourceDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sourceDomain\":null, ";
      }
      if(sourcePath) {
        str += "\"sourcePath\":\"" + sourcePath + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sourcePath\":null, ";
      }
      if(targetDomain) {
        str += "\"targetDomain\":\"" + targetDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"targetDomain\":null, ";
      }
      if(targetPath) {
        str += "\"targetPath\":\"" + targetPath + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"targetPath\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "redirectType:" + redirectType + ", ";
      str += "precedence:" + precedence + ", ";
      str += "sourceDomain:" + sourceDomain + ", ";
      str += "sourcePath:" + sourcePath + ", ";
      str += "targetDomain:" + targetDomain + ", ";
      str += "targetPath:" + targetPath + ", ";
      str += "note:" + note + ", ";
      str += "status:" + status + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.RedirectRuleJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.RedirectRuleJsBean();

  if(obj.redirectType !== undefined && obj.redirectType != null) {
    o.setRedirectType(obj.redirectType);
  }
  if(obj.precedence !== undefined && obj.precedence != null) {
    o.setPrecedence(obj.precedence);
  }
  if(obj.sourceDomain !== undefined && obj.sourceDomain != null) {
    o.setSourceDomain(obj.sourceDomain);
  }
  if(obj.sourcePath !== undefined && obj.sourcePath != null) {
    o.setSourcePath(obj.sourcePath);
  }
  if(obj.targetDomain !== undefined && obj.targetDomain != null) {
    o.setTargetDomain(obj.targetDomain);
  }
  if(obj.targetPath !== undefined && obj.targetPath != null) {
    o.setTargetPath(obj.targetPath);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
    
  return o;
};

feedstoa.wa.bean.RedirectRuleJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.RedirectRuleJsBean.create(jsonObj);
  return obj;
};
