//////////////////////////////////////////////////////////
// <script src="/js/bean/rssitemjsbean-1.0.js"></script>
// Last modified time: 1377391580541.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.RssItemJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var title;
    var link;
    var description;
    var author;
    var category;
    var comments;
    var enclosure;
    var guid = generateUuid();
    var pubDate;
    var source;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getLink = function() { return link; };
    this.setLink = function(value) { link = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getAuthor = function() { return author; };
    this.setAuthor = function(value) { author = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getComments = function() { return comments; };
    this.setComments = function(value) { comments = value; };
    this.getEnclosure = function() { return enclosure; };
    this.setEnclosure = function(value) { enclosure = value; };
    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getPubDate = function() { return pubDate; };
    this.setPubDate = function(value) { pubDate = value; };
    this.getSource = function() { return source; };
    this.setSource = function(value) { source = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.RssItemJsBean();

      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(link !== undefined && link != null) {
        o.setLink(link);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(author !== undefined && author != null) {
        o.setAuthor(author);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(comments !== undefined && comments != null) {
        o.setComments(comments);
      }
      //o.setEnclosure(enclosure.clone());
      if(enclosure !== undefined && enclosure != null) {
        o.setEnclosure(enclosure);
      }
      o.setGuid(generateUuid());
      if(pubDate !== undefined && pubDate != null) {
        o.setPubDate(pubDate);
      }
      //o.setSource(source.clone());
      if(source !== undefined && source != null) {
        o.setSource(source);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(link !== undefined && link != null) {
        jsonObj.link = link;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(author !== undefined && author != null) {
        jsonObj.author = author;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(comments !== undefined && comments != null) {
        jsonObj.comments = comments;
      } // Otherwise ignore...
      if(enclosure !== undefined && enclosure != null) {
        jsonObj.enclosure = enclosure;
      } // Otherwise ignore...
      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(pubDate !== undefined && pubDate != null) {
        jsonObj.pubDate = pubDate;
      } // Otherwise ignore...
      if(source !== undefined && source != null) {
        jsonObj.source = source;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(link) {
        str += "\"link\":\"" + link + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"link\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(author) {
        str += "\"author\":\"" + author + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"author\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(comments) {
        str += "\"comments\":\"" + comments + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"comments\":null, ";
      }
      str += "\"enclosure\":" + enclosure.toJsonString() + ", ";
      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(pubDate) {
        str += "\"pubDate\":\"" + pubDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pubDate\":null, ";
      }
      str += "\"source\":" + source.toJsonString() + ", ";

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "title:" + title + ", ";
      str += "link:" + link + ", ";
      str += "description:" + description + ", ";
      str += "author:" + author + ", ";
      str += "category:" + category + ", ";
      str += "comments:" + comments + ", ";
      str += "enclosure:" + enclosure + ", ";
      str += "guid:" + guid + ", ";
      str += "pubDate:" + pubDate + ", ";
      str += "source:" + source + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.RssItemJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.RssItemJsBean();

  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.link !== undefined && obj.link != null) {
    o.setLink(obj.link);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.author !== undefined && obj.author != null) {
    o.setAuthor(obj.author);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.comments !== undefined && obj.comments != null) {
    o.setComments(obj.comments);
  }
  if(obj.enclosure !== undefined && obj.enclosure != null) {
    o.setEnclosure(obj.enclosure);
  }
  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.pubDate !== undefined && obj.pubDate != null) {
    o.setPubDate(obj.pubDate);
  }
  if(obj.source !== undefined && obj.source != null) {
    o.setSource(obj.source);
  }
    
  return o;
};

feedstoa.wa.bean.RssItemJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.RssItemJsBean.create(jsonObj);
  return obj;
};
