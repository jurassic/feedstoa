//////////////////////////////////////////////////////////
// <script src="/js/bean/notificationstructjsbean-1.0.js"></script>
// Last modified time: 1377391580355.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.NotificationStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var preferredMode;
    var mobileNumber;
    var emailAddress;
    var twitterUsername;
    var facebookId;
    var linkedinId;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getPreferredMode = function() { return preferredMode; };
    this.setPreferredMode = function(value) { preferredMode = value; };
    this.getMobileNumber = function() { return mobileNumber; };
    this.setMobileNumber = function(value) { mobileNumber = value; };
    this.getEmailAddress = function() { return emailAddress; };
    this.setEmailAddress = function(value) { emailAddress = value; };
    this.getTwitterUsername = function() { return twitterUsername; };
    this.setTwitterUsername = function(value) { twitterUsername = value; };
    this.getFacebookId = function() { return facebookId; };
    this.setFacebookId = function(value) { facebookId = value; };
    this.getLinkedinId = function() { return linkedinId; };
    this.setLinkedinId = function(value) { linkedinId = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.NotificationStructJsBean();

      if(preferredMode !== undefined && preferredMode != null) {
        o.setPreferredMode(preferredMode);
      }
      if(mobileNumber !== undefined && mobileNumber != null) {
        o.setMobileNumber(mobileNumber);
      }
      if(emailAddress !== undefined && emailAddress != null) {
        o.setEmailAddress(emailAddress);
      }
      if(twitterUsername !== undefined && twitterUsername != null) {
        o.setTwitterUsername(twitterUsername);
      }
      if(facebookId !== undefined && facebookId != null) {
        o.setFacebookId(facebookId);
      }
      if(linkedinId !== undefined && linkedinId != null) {
        o.setLinkedinId(linkedinId);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(preferredMode !== undefined && preferredMode != null) {
        jsonObj.preferredMode = preferredMode;
      } // Otherwise ignore...
      if(mobileNumber !== undefined && mobileNumber != null) {
        jsonObj.mobileNumber = mobileNumber;
      } // Otherwise ignore...
      if(emailAddress !== undefined && emailAddress != null) {
        jsonObj.emailAddress = emailAddress;
      } // Otherwise ignore...
      if(twitterUsername !== undefined && twitterUsername != null) {
        jsonObj.twitterUsername = twitterUsername;
      } // Otherwise ignore...
      if(facebookId !== undefined && facebookId != null) {
        jsonObj.facebookId = facebookId;
      } // Otherwise ignore...
      if(linkedinId !== undefined && linkedinId != null) {
        jsonObj.linkedinId = linkedinId;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(preferredMode) {
        str += "\"preferredMode\":\"" + preferredMode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"preferredMode\":null, ";
      }
      if(mobileNumber) {
        str += "\"mobileNumber\":\"" + mobileNumber + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"mobileNumber\":null, ";
      }
      if(emailAddress) {
        str += "\"emailAddress\":\"" + emailAddress + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"emailAddress\":null, ";
      }
      if(twitterUsername) {
        str += "\"twitterUsername\":\"" + twitterUsername + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"twitterUsername\":null, ";
      }
      if(facebookId) {
        str += "\"facebookId\":" + facebookId + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"facebookId\":null, ";
      }
      if(linkedinId) {
        str += "\"linkedinId\":\"" + linkedinId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"linkedinId\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "preferredMode:" + preferredMode + ", ";
      str += "mobileNumber:" + mobileNumber + ", ";
      str += "emailAddress:" + emailAddress + ", ";
      str += "twitterUsername:" + twitterUsername + ", ";
      str += "facebookId:" + facebookId + ", ";
      str += "linkedinId:" + linkedinId + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.NotificationStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.NotificationStructJsBean();

  if(obj.preferredMode !== undefined && obj.preferredMode != null) {
    o.setPreferredMode(obj.preferredMode);
  }
  if(obj.mobileNumber !== undefined && obj.mobileNumber != null) {
    o.setMobileNumber(obj.mobileNumber);
  }
  if(obj.emailAddress !== undefined && obj.emailAddress != null) {
    o.setEmailAddress(obj.emailAddress);
  }
  if(obj.twitterUsername !== undefined && obj.twitterUsername != null) {
    o.setTwitterUsername(obj.twitterUsername);
  }
  if(obj.facebookId !== undefined && obj.facebookId != null) {
    o.setFacebookId(obj.facebookId);
  }
  if(obj.linkedinId !== undefined && obj.linkedinId != null) {
    o.setLinkedinId(obj.linkedinId);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

feedstoa.wa.bean.NotificationStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.NotificationStructJsBean.create(jsonObj);
  return obj;
};
