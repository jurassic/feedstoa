//////////////////////////////////////////////////////////
// <script src="/js/bean/feeditemjsbean-1.0.js"></script>
// Last modified time: 1377391580580.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.FeedItemJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var fetchRequest;
    var fetchUrl;
    var feedUrl;
    var channelSource;
    var channelFeed;
    var feedFormat;
    var title;
    var link;
    var summary;
    var content;
    var author;
    var contributor;
    var category;
    var comments;
    var enclosure;
    var id;
    var pubDate;
    var source;
    var feedContent;
    var status;
    var note;
    var referrerInfo;
    var publishedTime;
    var lastUpdatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getFetchRequest = function() { return fetchRequest; };
    this.setFetchRequest = function(value) { fetchRequest = value; };
    this.getFetchUrl = function() { return fetchUrl; };
    this.setFetchUrl = function(value) { fetchUrl = value; };
    this.getFeedUrl = function() { return feedUrl; };
    this.setFeedUrl = function(value) { feedUrl = value; };
    this.getChannelSource = function() { return channelSource; };
    this.setChannelSource = function(value) { channelSource = value; };
    this.getChannelFeed = function() { return channelFeed; };
    this.setChannelFeed = function(value) { channelFeed = value; };
    this.getFeedFormat = function() { return feedFormat; };
    this.setFeedFormat = function(value) { feedFormat = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getLink = function() { return link; };
    this.setLink = function(value) { link = value; };
    this.getSummary = function() { return summary; };
    this.setSummary = function(value) { summary = value; };
    this.getContent = function() { return content; };
    this.setContent = function(value) { content = value; };
    this.getAuthor = function() { return author; };
    this.setAuthor = function(value) { author = value; };
    this.getContributor = function() { return contributor; };
    this.setContributor = function(value) { contributor = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getComments = function() { return comments; };
    this.setComments = function(value) { comments = value; };
    this.getEnclosure = function() { return enclosure; };
    this.setEnclosure = function(value) { enclosure = value; };
    this.getId = function() { return id; };
    this.setId = function(value) { id = value; };
    this.getPubDate = function() { return pubDate; };
    this.setPubDate = function(value) { pubDate = value; };
    this.getSource = function() { return source; };
    this.setSource = function(value) { source = value; };
    this.getFeedContent = function() { return feedContent; };
    this.setFeedContent = function(value) { feedContent = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getPublishedTime = function() { return publishedTime; };
    this.setPublishedTime = function(value) { publishedTime = value; };
    this.getLastUpdatedTime = function() { return lastUpdatedTime; };
    this.setLastUpdatedTime = function(value) { lastUpdatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.FeedItemJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(fetchRequest !== undefined && fetchRequest != null) {
        o.setFetchRequest(fetchRequest);
      }
      if(fetchUrl !== undefined && fetchUrl != null) {
        o.setFetchUrl(fetchUrl);
      }
      if(feedUrl !== undefined && feedUrl != null) {
        o.setFeedUrl(feedUrl);
      }
      if(channelSource !== undefined && channelSource != null) {
        o.setChannelSource(channelSource);
      }
      if(channelFeed !== undefined && channelFeed != null) {
        o.setChannelFeed(channelFeed);
      }
      if(feedFormat !== undefined && feedFormat != null) {
        o.setFeedFormat(feedFormat);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      //o.setLink(link.clone());
      if(link !== undefined && link != null) {
        o.setLink(link);
      }
      if(summary !== undefined && summary != null) {
        o.setSummary(summary);
      }
      if(content !== undefined && content != null) {
        o.setContent(content);
      }
      //o.setAuthor(author.clone());
      if(author !== undefined && author != null) {
        o.setAuthor(author);
      }
      if(contributor !== undefined && contributor != null) {
        o.setContributor(contributor);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(comments !== undefined && comments != null) {
        o.setComments(comments);
      }
      //o.setEnclosure(enclosure.clone());
      if(enclosure !== undefined && enclosure != null) {
        o.setEnclosure(enclosure);
      }
      if(id !== undefined && id != null) {
        o.setId(id);
      }
      if(pubDate !== undefined && pubDate != null) {
        o.setPubDate(pubDate);
      }
      //o.setSource(source.clone());
      if(source !== undefined && source != null) {
        o.setSource(source);
      }
      if(feedContent !== undefined && feedContent != null) {
        o.setFeedContent(feedContent);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(publishedTime !== undefined && publishedTime != null) {
        o.setPublishedTime(publishedTime);
      }
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        o.setLastUpdatedTime(lastUpdatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(fetchRequest !== undefined && fetchRequest != null) {
        jsonObj.fetchRequest = fetchRequest;
      } // Otherwise ignore...
      if(fetchUrl !== undefined && fetchUrl != null) {
        jsonObj.fetchUrl = fetchUrl;
      } // Otherwise ignore...
      if(feedUrl !== undefined && feedUrl != null) {
        jsonObj.feedUrl = feedUrl;
      } // Otherwise ignore...
      if(channelSource !== undefined && channelSource != null) {
        jsonObj.channelSource = channelSource;
      } // Otherwise ignore...
      if(channelFeed !== undefined && channelFeed != null) {
        jsonObj.channelFeed = channelFeed;
      } // Otherwise ignore...
      if(feedFormat !== undefined && feedFormat != null) {
        jsonObj.feedFormat = feedFormat;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(link !== undefined && link != null) {
        jsonObj.link = link;
      } // Otherwise ignore...
      if(summary !== undefined && summary != null) {
        jsonObj.summary = summary;
      } // Otherwise ignore...
      if(content !== undefined && content != null) {
        jsonObj.content = content;
      } // Otherwise ignore...
      if(author !== undefined && author != null) {
        jsonObj.author = author;
      } // Otherwise ignore...
      if(contributor !== undefined && contributor != null) {
        jsonObj.contributor = contributor;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(comments !== undefined && comments != null) {
        jsonObj.comments = comments;
      } // Otherwise ignore...
      if(enclosure !== undefined && enclosure != null) {
        jsonObj.enclosure = enclosure;
      } // Otherwise ignore...
      if(id !== undefined && id != null) {
        jsonObj.id = id;
      } // Otherwise ignore...
      if(pubDate !== undefined && pubDate != null) {
        jsonObj.pubDate = pubDate;
      } // Otherwise ignore...
      if(source !== undefined && source != null) {
        jsonObj.source = source;
      } // Otherwise ignore...
      if(feedContent !== undefined && feedContent != null) {
        jsonObj.feedContent = feedContent;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(publishedTime !== undefined && publishedTime != null) {
        jsonObj.publishedTime = publishedTime;
      } // Otherwise ignore...
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        jsonObj.lastUpdatedTime = lastUpdatedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(fetchRequest) {
        str += "\"fetchRequest\":\"" + fetchRequest + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchRequest\":null, ";
      }
      if(fetchUrl) {
        str += "\"fetchUrl\":\"" + fetchUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchUrl\":null, ";
      }
      if(feedUrl) {
        str += "\"feedUrl\":\"" + feedUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedUrl\":null, ";
      }
      if(channelSource) {
        str += "\"channelSource\":\"" + channelSource + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelSource\":null, ";
      }
      if(channelFeed) {
        str += "\"channelFeed\":\"" + channelFeed + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelFeed\":null, ";
      }
      if(feedFormat) {
        str += "\"feedFormat\":\"" + feedFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedFormat\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      str += "\"link\":" + link.toJsonString() + ", ";
      if(summary) {
        str += "\"summary\":\"" + summary + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"summary\":null, ";
      }
      if(content) {
        str += "\"content\":\"" + content + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"content\":null, ";
      }
      str += "\"author\":" + author.toJsonString() + ", ";
      if(contributor) {
        str += "\"contributor\":\"" + contributor + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contributor\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(comments) {
        str += "\"comments\":\"" + comments + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"comments\":null, ";
      }
      str += "\"enclosure\":" + enclosure.toJsonString() + ", ";
      if(id) {
        str += "\"id\":\"" + id + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"id\":null, ";
      }
      if(pubDate) {
        str += "\"pubDate\":\"" + pubDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pubDate\":null, ";
      }
      str += "\"source\":" + source.toJsonString() + ", ";
      if(feedContent) {
        str += "\"feedContent\":\"" + feedContent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedContent\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(publishedTime) {
        str += "\"publishedTime\":" + publishedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"publishedTime\":null, ";
      }
      if(lastUpdatedTime) {
        str += "\"lastUpdatedTime\":" + lastUpdatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastUpdatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "fetchRequest:" + fetchRequest + ", ";
      str += "fetchUrl:" + fetchUrl + ", ";
      str += "feedUrl:" + feedUrl + ", ";
      str += "channelSource:" + channelSource + ", ";
      str += "channelFeed:" + channelFeed + ", ";
      str += "feedFormat:" + feedFormat + ", ";
      str += "title:" + title + ", ";
      str += "link:" + link + ", ";
      str += "summary:" + summary + ", ";
      str += "content:" + content + ", ";
      str += "author:" + author + ", ";
      str += "contributor:" + contributor + ", ";
      str += "category:" + category + ", ";
      str += "comments:" + comments + ", ";
      str += "enclosure:" + enclosure + ", ";
      str += "id:" + id + ", ";
      str += "pubDate:" + pubDate + ", ";
      str += "source:" + source + ", ";
      str += "feedContent:" + feedContent + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "publishedTime:" + publishedTime + ", ";
      str += "lastUpdatedTime:" + lastUpdatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.FeedItemJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.FeedItemJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.fetchRequest !== undefined && obj.fetchRequest != null) {
    o.setFetchRequest(obj.fetchRequest);
  }
  if(obj.fetchUrl !== undefined && obj.fetchUrl != null) {
    o.setFetchUrl(obj.fetchUrl);
  }
  if(obj.feedUrl !== undefined && obj.feedUrl != null) {
    o.setFeedUrl(obj.feedUrl);
  }
  if(obj.channelSource !== undefined && obj.channelSource != null) {
    o.setChannelSource(obj.channelSource);
  }
  if(obj.channelFeed !== undefined && obj.channelFeed != null) {
    o.setChannelFeed(obj.channelFeed);
  }
  if(obj.feedFormat !== undefined && obj.feedFormat != null) {
    o.setFeedFormat(obj.feedFormat);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.link !== undefined && obj.link != null) {
    o.setLink(obj.link);
  }
  if(obj.summary !== undefined && obj.summary != null) {
    o.setSummary(obj.summary);
  }
  if(obj.content !== undefined && obj.content != null) {
    o.setContent(obj.content);
  }
  if(obj.author !== undefined && obj.author != null) {
    o.setAuthor(obj.author);
  }
  if(obj.contributor !== undefined && obj.contributor != null) {
    o.setContributor(obj.contributor);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.comments !== undefined && obj.comments != null) {
    o.setComments(obj.comments);
  }
  if(obj.enclosure !== undefined && obj.enclosure != null) {
    o.setEnclosure(obj.enclosure);
  }
  if(obj.id !== undefined && obj.id != null) {
    o.setId(obj.id);
  }
  if(obj.pubDate !== undefined && obj.pubDate != null) {
    o.setPubDate(obj.pubDate);
  }
  if(obj.source !== undefined && obj.source != null) {
    o.setSource(obj.source);
  }
  if(obj.feedContent !== undefined && obj.feedContent != null) {
    o.setFeedContent(obj.feedContent);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.publishedTime !== undefined && obj.publishedTime != null) {
    o.setPublishedTime(obj.publishedTime);
  }
  if(obj.lastUpdatedTime !== undefined && obj.lastUpdatedTime != null) {
    o.setLastUpdatedTime(obj.lastUpdatedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

feedstoa.wa.bean.FeedItemJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.FeedItemJsBean.create(jsonObj);
  return obj;
};
