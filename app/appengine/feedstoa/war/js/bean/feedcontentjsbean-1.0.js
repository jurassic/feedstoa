//////////////////////////////////////////////////////////
// <script src="/js/bean/feedcontentjsbean-1.0.js"></script>
// Last modified time: 1377391580593.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.FeedContentJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var channelSource;
    var channelFeed;
    var channelVersion;
    var feedUrl;
    var feedFormat;
    var content;
    var contentHash;
    var status;
    var note;
    var lastBuildDate;
    var lastBuildTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getChannelSource = function() { return channelSource; };
    this.setChannelSource = function(value) { channelSource = value; };
    this.getChannelFeed = function() { return channelFeed; };
    this.setChannelFeed = function(value) { channelFeed = value; };
    this.getChannelVersion = function() { return channelVersion; };
    this.setChannelVersion = function(value) { channelVersion = value; };
    this.getFeedUrl = function() { return feedUrl; };
    this.setFeedUrl = function(value) { feedUrl = value; };
    this.getFeedFormat = function() { return feedFormat; };
    this.setFeedFormat = function(value) { feedFormat = value; };
    this.getContent = function() { return content; };
    this.setContent = function(value) { content = value; };
    this.getContentHash = function() { return contentHash; };
    this.setContentHash = function(value) { contentHash = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getLastBuildDate = function() { return lastBuildDate; };
    this.setLastBuildDate = function(value) { lastBuildDate = value; };
    this.getLastBuildTime = function() { return lastBuildTime; };
    this.setLastBuildTime = function(value) { lastBuildTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.FeedContentJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(channelSource !== undefined && channelSource != null) {
        o.setChannelSource(channelSource);
      }
      if(channelFeed !== undefined && channelFeed != null) {
        o.setChannelFeed(channelFeed);
      }
      if(channelVersion !== undefined && channelVersion != null) {
        o.setChannelVersion(channelVersion);
      }
      if(feedUrl !== undefined && feedUrl != null) {
        o.setFeedUrl(feedUrl);
      }
      if(feedFormat !== undefined && feedFormat != null) {
        o.setFeedFormat(feedFormat);
      }
      if(content !== undefined && content != null) {
        o.setContent(content);
      }
      if(contentHash !== undefined && contentHash != null) {
        o.setContentHash(contentHash);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(lastBuildDate !== undefined && lastBuildDate != null) {
        o.setLastBuildDate(lastBuildDate);
      }
      if(lastBuildTime !== undefined && lastBuildTime != null) {
        o.setLastBuildTime(lastBuildTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(channelSource !== undefined && channelSource != null) {
        jsonObj.channelSource = channelSource;
      } // Otherwise ignore...
      if(channelFeed !== undefined && channelFeed != null) {
        jsonObj.channelFeed = channelFeed;
      } // Otherwise ignore...
      if(channelVersion !== undefined && channelVersion != null) {
        jsonObj.channelVersion = channelVersion;
      } // Otherwise ignore...
      if(feedUrl !== undefined && feedUrl != null) {
        jsonObj.feedUrl = feedUrl;
      } // Otherwise ignore...
      if(feedFormat !== undefined && feedFormat != null) {
        jsonObj.feedFormat = feedFormat;
      } // Otherwise ignore...
      if(content !== undefined && content != null) {
        jsonObj.content = content;
      } // Otherwise ignore...
      if(contentHash !== undefined && contentHash != null) {
        jsonObj.contentHash = contentHash;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(lastBuildDate !== undefined && lastBuildDate != null) {
        jsonObj.lastBuildDate = lastBuildDate;
      } // Otherwise ignore...
      if(lastBuildTime !== undefined && lastBuildTime != null) {
        jsonObj.lastBuildTime = lastBuildTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(channelSource) {
        str += "\"channelSource\":\"" + channelSource + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelSource\":null, ";
      }
      if(channelFeed) {
        str += "\"channelFeed\":\"" + channelFeed + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelFeed\":null, ";
      }
      if(channelVersion) {
        str += "\"channelVersion\":\"" + channelVersion + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelVersion\":null, ";
      }
      if(feedUrl) {
        str += "\"feedUrl\":\"" + feedUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedUrl\":null, ";
      }
      if(feedFormat) {
        str += "\"feedFormat\":\"" + feedFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedFormat\":null, ";
      }
      if(content) {
        str += "\"content\":\"" + content + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"content\":null, ";
      }
      if(contentHash) {
        str += "\"contentHash\":\"" + contentHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contentHash\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(lastBuildDate) {
        str += "\"lastBuildDate\":\"" + lastBuildDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastBuildDate\":null, ";
      }
      if(lastBuildTime) {
        str += "\"lastBuildTime\":" + lastBuildTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastBuildTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "channelSource:" + channelSource + ", ";
      str += "channelFeed:" + channelFeed + ", ";
      str += "channelVersion:" + channelVersion + ", ";
      str += "feedUrl:" + feedUrl + ", ";
      str += "feedFormat:" + feedFormat + ", ";
      str += "content:" + content + ", ";
      str += "contentHash:" + contentHash + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "lastBuildDate:" + lastBuildDate + ", ";
      str += "lastBuildTime:" + lastBuildTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.FeedContentJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.FeedContentJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.channelSource !== undefined && obj.channelSource != null) {
    o.setChannelSource(obj.channelSource);
  }
  if(obj.channelFeed !== undefined && obj.channelFeed != null) {
    o.setChannelFeed(obj.channelFeed);
  }
  if(obj.channelVersion !== undefined && obj.channelVersion != null) {
    o.setChannelVersion(obj.channelVersion);
  }
  if(obj.feedUrl !== undefined && obj.feedUrl != null) {
    o.setFeedUrl(obj.feedUrl);
  }
  if(obj.feedFormat !== undefined && obj.feedFormat != null) {
    o.setFeedFormat(obj.feedFormat);
  }
  if(obj.content !== undefined && obj.content != null) {
    o.setContent(obj.content);
  }
  if(obj.contentHash !== undefined && obj.contentHash != null) {
    o.setContentHash(obj.contentHash);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.lastBuildDate !== undefined && obj.lastBuildDate != null) {
    o.setLastBuildDate(obj.lastBuildDate);
  }
  if(obj.lastBuildTime !== undefined && obj.lastBuildTime != null) {
    o.setLastBuildTime(obj.lastBuildTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

feedstoa.wa.bean.FeedContentJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.FeedContentJsBean.create(jsonObj);
  return obj;
};
