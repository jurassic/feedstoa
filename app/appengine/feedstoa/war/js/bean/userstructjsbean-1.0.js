//////////////////////////////////////////////////////////
// <script src="/js/bean/userstructjsbean-1.0.js"></script>
// Last modified time: 1377391580469.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.UserStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var name;
    var email;
    var url;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getEmail = function() { return email; };
    this.setEmail = function(value) { email = value; };
    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.UserStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(email !== undefined && email != null) {
        o.setEmail(email);
      }
      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(email !== undefined && email != null) {
        jsonObj.email = email;
      } // Otherwise ignore...
      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(email) {
        str += "\"email\":\"" + email + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"email\":null, ";
      }
      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "name:" + name + ", ";
      str += "email:" + email + ", ";
      str += "url:" + url + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.UserStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.UserStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.email !== undefined && obj.email != null) {
    o.setEmail(obj.email);
  }
  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
    
  return o;
};

feedstoa.wa.bean.UserStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.UserStructJsBean.create(jsonObj);
  return obj;
};
