//////////////////////////////////////////////////////////
// <script src="/js/bean/channelsourcejsbean-1.0.js"></script>
// Last modified time: 1377391580451.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.ChannelSourceJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var serviceName;
    var serviceUrl;
    var feedUrl;
    var feedFormat;
    var channelTitle;
    var channelDescription;
    var channelCategory;
    var channelUrl;
    var channelLogo;
    var status;
    var note;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getServiceName = function() { return serviceName; };
    this.setServiceName = function(value) { serviceName = value; };
    this.getServiceUrl = function() { return serviceUrl; };
    this.setServiceUrl = function(value) { serviceUrl = value; };
    this.getFeedUrl = function() { return feedUrl; };
    this.setFeedUrl = function(value) { feedUrl = value; };
    this.getFeedFormat = function() { return feedFormat; };
    this.setFeedFormat = function(value) { feedFormat = value; };
    this.getChannelTitle = function() { return channelTitle; };
    this.setChannelTitle = function(value) { channelTitle = value; };
    this.getChannelDescription = function() { return channelDescription; };
    this.setChannelDescription = function(value) { channelDescription = value; };
    this.getChannelCategory = function() { return channelCategory; };
    this.setChannelCategory = function(value) { channelCategory = value; };
    this.getChannelUrl = function() { return channelUrl; };
    this.setChannelUrl = function(value) { channelUrl = value; };
    this.getChannelLogo = function() { return channelLogo; };
    this.setChannelLogo = function(value) { channelLogo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.ChannelSourceJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(serviceName !== undefined && serviceName != null) {
        o.setServiceName(serviceName);
      }
      if(serviceUrl !== undefined && serviceUrl != null) {
        o.setServiceUrl(serviceUrl);
      }
      if(feedUrl !== undefined && feedUrl != null) {
        o.setFeedUrl(feedUrl);
      }
      if(feedFormat !== undefined && feedFormat != null) {
        o.setFeedFormat(feedFormat);
      }
      if(channelTitle !== undefined && channelTitle != null) {
        o.setChannelTitle(channelTitle);
      }
      if(channelDescription !== undefined && channelDescription != null) {
        o.setChannelDescription(channelDescription);
      }
      if(channelCategory !== undefined && channelCategory != null) {
        o.setChannelCategory(channelCategory);
      }
      if(channelUrl !== undefined && channelUrl != null) {
        o.setChannelUrl(channelUrl);
      }
      if(channelLogo !== undefined && channelLogo != null) {
        o.setChannelLogo(channelLogo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(serviceName !== undefined && serviceName != null) {
        jsonObj.serviceName = serviceName;
      } // Otherwise ignore...
      if(serviceUrl !== undefined && serviceUrl != null) {
        jsonObj.serviceUrl = serviceUrl;
      } // Otherwise ignore...
      if(feedUrl !== undefined && feedUrl != null) {
        jsonObj.feedUrl = feedUrl;
      } // Otherwise ignore...
      if(feedFormat !== undefined && feedFormat != null) {
        jsonObj.feedFormat = feedFormat;
      } // Otherwise ignore...
      if(channelTitle !== undefined && channelTitle != null) {
        jsonObj.channelTitle = channelTitle;
      } // Otherwise ignore...
      if(channelDescription !== undefined && channelDescription != null) {
        jsonObj.channelDescription = channelDescription;
      } // Otherwise ignore...
      if(channelCategory !== undefined && channelCategory != null) {
        jsonObj.channelCategory = channelCategory;
      } // Otherwise ignore...
      if(channelUrl !== undefined && channelUrl != null) {
        jsonObj.channelUrl = channelUrl;
      } // Otherwise ignore...
      if(channelLogo !== undefined && channelLogo != null) {
        jsonObj.channelLogo = channelLogo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(serviceName) {
        str += "\"serviceName\":\"" + serviceName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceName\":null, ";
      }
      if(serviceUrl) {
        str += "\"serviceUrl\":\"" + serviceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"serviceUrl\":null, ";
      }
      if(feedUrl) {
        str += "\"feedUrl\":\"" + feedUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedUrl\":null, ";
      }
      if(feedFormat) {
        str += "\"feedFormat\":\"" + feedFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedFormat\":null, ";
      }
      if(channelTitle) {
        str += "\"channelTitle\":\"" + channelTitle + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelTitle\":null, ";
      }
      if(channelDescription) {
        str += "\"channelDescription\":\"" + channelDescription + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelDescription\":null, ";
      }
      if(channelCategory) {
        str += "\"channelCategory\":\"" + channelCategory + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelCategory\":null, ";
      }
      if(channelUrl) {
        str += "\"channelUrl\":\"" + channelUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelUrl\":null, ";
      }
      if(channelLogo) {
        str += "\"channelLogo\":\"" + channelLogo + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelLogo\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "serviceName:" + serviceName + ", ";
      str += "serviceUrl:" + serviceUrl + ", ";
      str += "feedUrl:" + feedUrl + ", ";
      str += "feedFormat:" + feedFormat + ", ";
      str += "channelTitle:" + channelTitle + ", ";
      str += "channelDescription:" + channelDescription + ", ";
      str += "channelCategory:" + channelCategory + ", ";
      str += "channelUrl:" + channelUrl + ", ";
      str += "channelLogo:" + channelLogo + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.ChannelSourceJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.ChannelSourceJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.serviceName !== undefined && obj.serviceName != null) {
    o.setServiceName(obj.serviceName);
  }
  if(obj.serviceUrl !== undefined && obj.serviceUrl != null) {
    o.setServiceUrl(obj.serviceUrl);
  }
  if(obj.feedUrl !== undefined && obj.feedUrl != null) {
    o.setFeedUrl(obj.feedUrl);
  }
  if(obj.feedFormat !== undefined && obj.feedFormat != null) {
    o.setFeedFormat(obj.feedFormat);
  }
  if(obj.channelTitle !== undefined && obj.channelTitle != null) {
    o.setChannelTitle(obj.channelTitle);
  }
  if(obj.channelDescription !== undefined && obj.channelDescription != null) {
    o.setChannelDescription(obj.channelDescription);
  }
  if(obj.channelCategory !== undefined && obj.channelCategory != null) {
    o.setChannelCategory(obj.channelCategory);
  }
  if(obj.channelUrl !== undefined && obj.channelUrl != null) {
    o.setChannelUrl(obj.channelUrl);
  }
  if(obj.channelLogo !== undefined && obj.channelLogo != null) {
    o.setChannelLogo(obj.channelLogo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

feedstoa.wa.bean.ChannelSourceJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.ChannelSourceJsBean.create(jsonObj);
  return obj;
};
