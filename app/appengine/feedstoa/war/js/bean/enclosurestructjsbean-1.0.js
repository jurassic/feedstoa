//////////////////////////////////////////////////////////
// <script src="/js/bean/enclosurestructjsbean-1.0.js"></script>
// Last modified time: 1377391580515.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.EnclosureStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var url;
    var length;
    var type;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };
    this.getLength = function() { return length; };
    this.setLength = function(value) { length = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.EnclosureStructJsBean();

      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
      if(length !== undefined && length != null) {
        o.setLength(length);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...
      if(length !== undefined && length != null) {
        jsonObj.length = length;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }
      if(length) {
        str += "\"length\":" + length + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"length\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "url:" + url + ", ";
      str += "length:" + length + ", ";
      str += "type:" + type + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.EnclosureStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.EnclosureStructJsBean();

  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
  if(obj.length !== undefined && obj.length != null) {
    o.setLength(obj.length);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
    
  return o;
};

feedstoa.wa.bean.EnclosureStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.EnclosureStructJsBean.create(jsonObj);
  return obj;
};
