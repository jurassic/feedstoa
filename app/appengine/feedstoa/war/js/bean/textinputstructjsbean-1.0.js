//////////////////////////////////////////////////////////
// <script src="/js/bean/textinputstructjsbean-1.0.js"></script>
// Last modified time: 1377391580507.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.TextInputStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var title;
    var name;
    var link;
    var description;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getLink = function() { return link; };
    this.setLink = function(value) { link = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.TextInputStructJsBean();

      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(link !== undefined && link != null) {
        o.setLink(link);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(link !== undefined && link != null) {
        jsonObj.link = link;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(link) {
        str += "\"link\":\"" + link + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"link\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "title:" + title + ", ";
      str += "name:" + name + ", ";
      str += "link:" + link + ", ";
      str += "description:" + description + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.TextInputStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.TextInputStructJsBean();

  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.link !== undefined && obj.link != null) {
    o.setLink(obj.link);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
    
  return o;
};

feedstoa.wa.bean.TextInputStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.TextInputStructJsBean.create(jsonObj);
  return obj;
};
