//////////////////////////////////////////////////////////
// <script src="/js/bean/secondaryfetchjsbean-1.0.js"></script>
// Last modified time: 1377391580414.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.SecondaryFetchJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var managerApp;
    var appAcl;
    var gaeApp;
    var ownerUser;
    var userAcl;
    var user;
    var title;
    var description;
    var fetchUrl;
    var feedUrl;
    var channelFeed;
    var reuseChannel;
    var maxItemCount;
    var note;
    var status;
    var fetchRequest;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getManagerApp = function() { return managerApp; };
    this.setManagerApp = function(value) { managerApp = value; };
    this.getAppAcl = function() { return appAcl; };
    this.setAppAcl = function(value) { appAcl = value; };
    this.getGaeApp = function() { return gaeApp; };
    this.setGaeApp = function(value) { gaeApp = value; };
    this.getOwnerUser = function() { return ownerUser; };
    this.setOwnerUser = function(value) { ownerUser = value; };
    this.getUserAcl = function() { return userAcl; };
    this.setUserAcl = function(value) { userAcl = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getFetchUrl = function() { return fetchUrl; };
    this.setFetchUrl = function(value) { fetchUrl = value; };
    this.getFeedUrl = function() { return feedUrl; };
    this.setFeedUrl = function(value) { feedUrl = value; };
    this.getChannelFeed = function() { return channelFeed; };
    this.setChannelFeed = function(value) { channelFeed = value; };
    this.getReuseChannel = function() { return reuseChannel; };
    this.setReuseChannel = function(value) { reuseChannel = value; };
    this.getMaxItemCount = function() { return maxItemCount; };
    this.setMaxItemCount = function(value) { maxItemCount = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getFetchRequest = function() { return fetchRequest; };
    this.setFetchRequest = function(value) { fetchRequest = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.SecondaryFetchJsBean();

      o.setGuid(generateUuid());
      if(managerApp !== undefined && managerApp != null) {
        o.setManagerApp(managerApp);
      }
      if(appAcl !== undefined && appAcl != null) {
        o.setAppAcl(appAcl);
      }
      //o.setGaeApp(gaeApp.clone());
      if(gaeApp !== undefined && gaeApp != null) {
        o.setGaeApp(gaeApp);
      }
      if(ownerUser !== undefined && ownerUser != null) {
        o.setOwnerUser(ownerUser);
      }
      if(userAcl !== undefined && userAcl != null) {
        o.setUserAcl(userAcl);
      }
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(fetchUrl !== undefined && fetchUrl != null) {
        o.setFetchUrl(fetchUrl);
      }
      if(feedUrl !== undefined && feedUrl != null) {
        o.setFeedUrl(feedUrl);
      }
      if(channelFeed !== undefined && channelFeed != null) {
        o.setChannelFeed(channelFeed);
      }
      if(reuseChannel !== undefined && reuseChannel != null) {
        o.setReuseChannel(reuseChannel);
      }
      if(maxItemCount !== undefined && maxItemCount != null) {
        o.setMaxItemCount(maxItemCount);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(fetchRequest !== undefined && fetchRequest != null) {
        o.setFetchRequest(fetchRequest);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(managerApp !== undefined && managerApp != null) {
        jsonObj.managerApp = managerApp;
      } // Otherwise ignore...
      if(appAcl !== undefined && appAcl != null) {
        jsonObj.appAcl = appAcl;
      } // Otherwise ignore...
      if(gaeApp !== undefined && gaeApp != null) {
        jsonObj.gaeApp = gaeApp;
      } // Otherwise ignore...
      if(ownerUser !== undefined && ownerUser != null) {
        jsonObj.ownerUser = ownerUser;
      } // Otherwise ignore...
      if(userAcl !== undefined && userAcl != null) {
        jsonObj.userAcl = userAcl;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(fetchUrl !== undefined && fetchUrl != null) {
        jsonObj.fetchUrl = fetchUrl;
      } // Otherwise ignore...
      if(feedUrl !== undefined && feedUrl != null) {
        jsonObj.feedUrl = feedUrl;
      } // Otherwise ignore...
      if(channelFeed !== undefined && channelFeed != null) {
        jsonObj.channelFeed = channelFeed;
      } // Otherwise ignore...
      if(reuseChannel !== undefined && reuseChannel != null) {
        jsonObj.reuseChannel = reuseChannel;
      } // Otherwise ignore...
      if(maxItemCount !== undefined && maxItemCount != null) {
        jsonObj.maxItemCount = maxItemCount;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(fetchRequest !== undefined && fetchRequest != null) {
        jsonObj.fetchRequest = fetchRequest;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(managerApp) {
        str += "\"managerApp\":\"" + managerApp + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"managerApp\":null, ";
      }
      if(appAcl) {
        str += "\"appAcl\":" + appAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"appAcl\":null, ";
      }
      str += "\"gaeApp\":" + gaeApp.toJsonString() + ", ";
      if(ownerUser) {
        str += "\"ownerUser\":\"" + ownerUser + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ownerUser\":null, ";
      }
      if(userAcl) {
        str += "\"userAcl\":" + userAcl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAcl\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(fetchUrl) {
        str += "\"fetchUrl\":\"" + fetchUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchUrl\":null, ";
      }
      if(feedUrl) {
        str += "\"feedUrl\":\"" + feedUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedUrl\":null, ";
      }
      if(channelFeed) {
        str += "\"channelFeed\":\"" + channelFeed + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelFeed\":null, ";
      }
      if(reuseChannel) {
        str += "\"reuseChannel\":" + reuseChannel + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reuseChannel\":null, ";
      }
      if(maxItemCount) {
        str += "\"maxItemCount\":" + maxItemCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"maxItemCount\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(fetchRequest) {
        str += "\"fetchRequest\":\"" + fetchRequest + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchRequest\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "managerApp:" + managerApp + ", ";
      str += "appAcl:" + appAcl + ", ";
      str += "gaeApp:" + gaeApp + ", ";
      str += "ownerUser:" + ownerUser + ", ";
      str += "userAcl:" + userAcl + ", ";
      str += "user:" + user + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "fetchUrl:" + fetchUrl + ", ";
      str += "feedUrl:" + feedUrl + ", ";
      str += "channelFeed:" + channelFeed + ", ";
      str += "reuseChannel:" + reuseChannel + ", ";
      str += "maxItemCount:" + maxItemCount + ", ";
      str += "note:" + note + ", ";
      str += "status:" + status + ", ";
      str += "fetchRequest:" + fetchRequest + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.SecondaryFetchJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.SecondaryFetchJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.managerApp !== undefined && obj.managerApp != null) {
    o.setManagerApp(obj.managerApp);
  }
  if(obj.appAcl !== undefined && obj.appAcl != null) {
    o.setAppAcl(obj.appAcl);
  }
  if(obj.gaeApp !== undefined && obj.gaeApp != null) {
    o.setGaeApp(obj.gaeApp);
  }
  if(obj.ownerUser !== undefined && obj.ownerUser != null) {
    o.setOwnerUser(obj.ownerUser);
  }
  if(obj.userAcl !== undefined && obj.userAcl != null) {
    o.setUserAcl(obj.userAcl);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.fetchUrl !== undefined && obj.fetchUrl != null) {
    o.setFetchUrl(obj.fetchUrl);
  }
  if(obj.feedUrl !== undefined && obj.feedUrl != null) {
    o.setFeedUrl(obj.feedUrl);
  }
  if(obj.channelFeed !== undefined && obj.channelFeed != null) {
    o.setChannelFeed(obj.channelFeed);
  }
  if(obj.reuseChannel !== undefined && obj.reuseChannel != null) {
    o.setReuseChannel(obj.reuseChannel);
  }
  if(obj.maxItemCount !== undefined && obj.maxItemCount != null) {
    o.setMaxItemCount(obj.maxItemCount);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.fetchRequest !== undefined && obj.fetchRequest != null) {
    o.setFetchRequest(obj.fetchRequest);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

feedstoa.wa.bean.SecondaryFetchJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.SecondaryFetchJsBean.create(jsonObj);
  return obj;
};
