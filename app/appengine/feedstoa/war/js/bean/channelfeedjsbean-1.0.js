//////////////////////////////////////////////////////////
// <script src="/js/bean/channelfeedjsbean-1.0.js"></script>
// Last modified time: 1377391580565.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.ChannelFeedJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var channelSource;
    var channelCode;
    var previousVersion;
    var fetchRequest;
    var fetchUrl;
    var feedServiceUrl;
    var feedUrl;
    var feedFormat;
    var maxItemCount;
    var feedCategory;
    var title;
    var subtitle;
    var link;
    var description;
    var language;
    var copyright;
    var managingEditor;
    var webMaster;
    var contributor;
    var pubDate;
    var lastBuildDate;
    var category;
    var generator;
    var docs;
    var cloud;
    var ttl;
    var logo;
    var icon;
    var rating;
    var textInput;
    var skipHours;
    var skipDays;
    var outputText;
    var outputHash;
    var feedContent;
    var status;
    var note;
    var referrerInfo;
    var lastBuildTime;
    var publishedTime;
    var expirationTime;
    var lastUpdatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getChannelSource = function() { return channelSource; };
    this.setChannelSource = function(value) { channelSource = value; };
    this.getChannelCode = function() { return channelCode; };
    this.setChannelCode = function(value) { channelCode = value; };
    this.getPreviousVersion = function() { return previousVersion; };
    this.setPreviousVersion = function(value) { previousVersion = value; };
    this.getFetchRequest = function() { return fetchRequest; };
    this.setFetchRequest = function(value) { fetchRequest = value; };
    this.getFetchUrl = function() { return fetchUrl; };
    this.setFetchUrl = function(value) { fetchUrl = value; };
    this.getFeedServiceUrl = function() { return feedServiceUrl; };
    this.setFeedServiceUrl = function(value) { feedServiceUrl = value; };
    this.getFeedUrl = function() { return feedUrl; };
    this.setFeedUrl = function(value) { feedUrl = value; };
    this.getFeedFormat = function() { return feedFormat; };
    this.setFeedFormat = function(value) { feedFormat = value; };
    this.getMaxItemCount = function() { return maxItemCount; };
    this.setMaxItemCount = function(value) { maxItemCount = value; };
    this.getFeedCategory = function() { return feedCategory; };
    this.setFeedCategory = function(value) { feedCategory = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getSubtitle = function() { return subtitle; };
    this.setSubtitle = function(value) { subtitle = value; };
    this.getLink = function() { return link; };
    this.setLink = function(value) { link = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getLanguage = function() { return language; };
    this.setLanguage = function(value) { language = value; };
    this.getCopyright = function() { return copyright; };
    this.setCopyright = function(value) { copyright = value; };
    this.getManagingEditor = function() { return managingEditor; };
    this.setManagingEditor = function(value) { managingEditor = value; };
    this.getWebMaster = function() { return webMaster; };
    this.setWebMaster = function(value) { webMaster = value; };
    this.getContributor = function() { return contributor; };
    this.setContributor = function(value) { contributor = value; };
    this.getPubDate = function() { return pubDate; };
    this.setPubDate = function(value) { pubDate = value; };
    this.getLastBuildDate = function() { return lastBuildDate; };
    this.setLastBuildDate = function(value) { lastBuildDate = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getGenerator = function() { return generator; };
    this.setGenerator = function(value) { generator = value; };
    this.getDocs = function() { return docs; };
    this.setDocs = function(value) { docs = value; };
    this.getCloud = function() { return cloud; };
    this.setCloud = function(value) { cloud = value; };
    this.getTtl = function() { return ttl; };
    this.setTtl = function(value) { ttl = value; };
    this.getLogo = function() { return logo; };
    this.setLogo = function(value) { logo = value; };
    this.getIcon = function() { return icon; };
    this.setIcon = function(value) { icon = value; };
    this.getRating = function() { return rating; };
    this.setRating = function(value) { rating = value; };
    this.getTextInput = function() { return textInput; };
    this.setTextInput = function(value) { textInput = value; };
    this.getSkipHours = function() { return skipHours; };
    this.setSkipHours = function(value) { skipHours = value; };
    this.getSkipDays = function() { return skipDays; };
    this.setSkipDays = function(value) { skipDays = value; };
    this.getOutputText = function() { return outputText; };
    this.setOutputText = function(value) { outputText = value; };
    this.getOutputHash = function() { return outputHash; };
    this.setOutputHash = function(value) { outputHash = value; };
    this.getFeedContent = function() { return feedContent; };
    this.setFeedContent = function(value) { feedContent = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getLastBuildTime = function() { return lastBuildTime; };
    this.setLastBuildTime = function(value) { lastBuildTime = value; };
    this.getPublishedTime = function() { return publishedTime; };
    this.setPublishedTime = function(value) { publishedTime = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getLastUpdatedTime = function() { return lastUpdatedTime; };
    this.setLastUpdatedTime = function(value) { lastUpdatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.ChannelFeedJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(channelSource !== undefined && channelSource != null) {
        o.setChannelSource(channelSource);
      }
      if(channelCode !== undefined && channelCode != null) {
        o.setChannelCode(channelCode);
      }
      if(previousVersion !== undefined && previousVersion != null) {
        o.setPreviousVersion(previousVersion);
      }
      if(fetchRequest !== undefined && fetchRequest != null) {
        o.setFetchRequest(fetchRequest);
      }
      if(fetchUrl !== undefined && fetchUrl != null) {
        o.setFetchUrl(fetchUrl);
      }
      if(feedServiceUrl !== undefined && feedServiceUrl != null) {
        o.setFeedServiceUrl(feedServiceUrl);
      }
      if(feedUrl !== undefined && feedUrl != null) {
        o.setFeedUrl(feedUrl);
      }
      if(feedFormat !== undefined && feedFormat != null) {
        o.setFeedFormat(feedFormat);
      }
      if(maxItemCount !== undefined && maxItemCount != null) {
        o.setMaxItemCount(maxItemCount);
      }
      if(feedCategory !== undefined && feedCategory != null) {
        o.setFeedCategory(feedCategory);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(subtitle !== undefined && subtitle != null) {
        o.setSubtitle(subtitle);
      }
      //o.setLink(link.clone());
      if(link !== undefined && link != null) {
        o.setLink(link);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(language !== undefined && language != null) {
        o.setLanguage(language);
      }
      if(copyright !== undefined && copyright != null) {
        o.setCopyright(copyright);
      }
      //o.setManagingEditor(managingEditor.clone());
      if(managingEditor !== undefined && managingEditor != null) {
        o.setManagingEditor(managingEditor);
      }
      //o.setWebMaster(webMaster.clone());
      if(webMaster !== undefined && webMaster != null) {
        o.setWebMaster(webMaster);
      }
      if(contributor !== undefined && contributor != null) {
        o.setContributor(contributor);
      }
      if(pubDate !== undefined && pubDate != null) {
        o.setPubDate(pubDate);
      }
      if(lastBuildDate !== undefined && lastBuildDate != null) {
        o.setLastBuildDate(lastBuildDate);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(generator !== undefined && generator != null) {
        o.setGenerator(generator);
      }
      if(docs !== undefined && docs != null) {
        o.setDocs(docs);
      }
      //o.setCloud(cloud.clone());
      if(cloud !== undefined && cloud != null) {
        o.setCloud(cloud);
      }
      if(ttl !== undefined && ttl != null) {
        o.setTtl(ttl);
      }
      //o.setLogo(logo.clone());
      if(logo !== undefined && logo != null) {
        o.setLogo(logo);
      }
      //o.setIcon(icon.clone());
      if(icon !== undefined && icon != null) {
        o.setIcon(icon);
      }
      if(rating !== undefined && rating != null) {
        o.setRating(rating);
      }
      //o.setTextInput(textInput.clone());
      if(textInput !== undefined && textInput != null) {
        o.setTextInput(textInput);
      }
      if(skipHours !== undefined && skipHours != null) {
        o.setSkipHours(skipHours);
      }
      if(skipDays !== undefined && skipDays != null) {
        o.setSkipDays(skipDays);
      }
      if(outputText !== undefined && outputText != null) {
        o.setOutputText(outputText);
      }
      if(outputHash !== undefined && outputHash != null) {
        o.setOutputHash(outputHash);
      }
      if(feedContent !== undefined && feedContent != null) {
        o.setFeedContent(feedContent);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(lastBuildTime !== undefined && lastBuildTime != null) {
        o.setLastBuildTime(lastBuildTime);
      }
      if(publishedTime !== undefined && publishedTime != null) {
        o.setPublishedTime(publishedTime);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        o.setLastUpdatedTime(lastUpdatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(channelSource !== undefined && channelSource != null) {
        jsonObj.channelSource = channelSource;
      } // Otherwise ignore...
      if(channelCode !== undefined && channelCode != null) {
        jsonObj.channelCode = channelCode;
      } // Otherwise ignore...
      if(previousVersion !== undefined && previousVersion != null) {
        jsonObj.previousVersion = previousVersion;
      } // Otherwise ignore...
      if(fetchRequest !== undefined && fetchRequest != null) {
        jsonObj.fetchRequest = fetchRequest;
      } // Otherwise ignore...
      if(fetchUrl !== undefined && fetchUrl != null) {
        jsonObj.fetchUrl = fetchUrl;
      } // Otherwise ignore...
      if(feedServiceUrl !== undefined && feedServiceUrl != null) {
        jsonObj.feedServiceUrl = feedServiceUrl;
      } // Otherwise ignore...
      if(feedUrl !== undefined && feedUrl != null) {
        jsonObj.feedUrl = feedUrl;
      } // Otherwise ignore...
      if(feedFormat !== undefined && feedFormat != null) {
        jsonObj.feedFormat = feedFormat;
      } // Otherwise ignore...
      if(maxItemCount !== undefined && maxItemCount != null) {
        jsonObj.maxItemCount = maxItemCount;
      } // Otherwise ignore...
      if(feedCategory !== undefined && feedCategory != null) {
        jsonObj.feedCategory = feedCategory;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(subtitle !== undefined && subtitle != null) {
        jsonObj.subtitle = subtitle;
      } // Otherwise ignore...
      if(link !== undefined && link != null) {
        jsonObj.link = link;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(language !== undefined && language != null) {
        jsonObj.language = language;
      } // Otherwise ignore...
      if(copyright !== undefined && copyright != null) {
        jsonObj.copyright = copyright;
      } // Otherwise ignore...
      if(managingEditor !== undefined && managingEditor != null) {
        jsonObj.managingEditor = managingEditor;
      } // Otherwise ignore...
      if(webMaster !== undefined && webMaster != null) {
        jsonObj.webMaster = webMaster;
      } // Otherwise ignore...
      if(contributor !== undefined && contributor != null) {
        jsonObj.contributor = contributor;
      } // Otherwise ignore...
      if(pubDate !== undefined && pubDate != null) {
        jsonObj.pubDate = pubDate;
      } // Otherwise ignore...
      if(lastBuildDate !== undefined && lastBuildDate != null) {
        jsonObj.lastBuildDate = lastBuildDate;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(generator !== undefined && generator != null) {
        jsonObj.generator = generator;
      } // Otherwise ignore...
      if(docs !== undefined && docs != null) {
        jsonObj.docs = docs;
      } // Otherwise ignore...
      if(cloud !== undefined && cloud != null) {
        jsonObj.cloud = cloud;
      } // Otherwise ignore...
      if(ttl !== undefined && ttl != null) {
        jsonObj.ttl = ttl;
      } // Otherwise ignore...
      if(logo !== undefined && logo != null) {
        jsonObj.logo = logo;
      } // Otherwise ignore...
      if(icon !== undefined && icon != null) {
        jsonObj.icon = icon;
      } // Otherwise ignore...
      if(rating !== undefined && rating != null) {
        jsonObj.rating = rating;
      } // Otherwise ignore...
      if(textInput !== undefined && textInput != null) {
        jsonObj.textInput = textInput;
      } // Otherwise ignore...
      if(skipHours !== undefined && skipHours != null) {
        jsonObj.skipHours = skipHours;
      } // Otherwise ignore...
      if(skipDays !== undefined && skipDays != null) {
        jsonObj.skipDays = skipDays;
      } // Otherwise ignore...
      if(outputText !== undefined && outputText != null) {
        jsonObj.outputText = outputText;
      } // Otherwise ignore...
      if(outputHash !== undefined && outputHash != null) {
        jsonObj.outputHash = outputHash;
      } // Otherwise ignore...
      if(feedContent !== undefined && feedContent != null) {
        jsonObj.feedContent = feedContent;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(lastBuildTime !== undefined && lastBuildTime != null) {
        jsonObj.lastBuildTime = lastBuildTime;
      } // Otherwise ignore...
      if(publishedTime !== undefined && publishedTime != null) {
        jsonObj.publishedTime = publishedTime;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        jsonObj.lastUpdatedTime = lastUpdatedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(channelSource) {
        str += "\"channelSource\":\"" + channelSource + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelSource\":null, ";
      }
      if(channelCode) {
        str += "\"channelCode\":\"" + channelCode + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"channelCode\":null, ";
      }
      if(previousVersion) {
        str += "\"previousVersion\":\"" + previousVersion + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"previousVersion\":null, ";
      }
      if(fetchRequest) {
        str += "\"fetchRequest\":\"" + fetchRequest + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchRequest\":null, ";
      }
      if(fetchUrl) {
        str += "\"fetchUrl\":\"" + fetchUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchUrl\":null, ";
      }
      if(feedServiceUrl) {
        str += "\"feedServiceUrl\":\"" + feedServiceUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedServiceUrl\":null, ";
      }
      if(feedUrl) {
        str += "\"feedUrl\":\"" + feedUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedUrl\":null, ";
      }
      if(feedFormat) {
        str += "\"feedFormat\":\"" + feedFormat + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedFormat\":null, ";
      }
      if(maxItemCount) {
        str += "\"maxItemCount\":" + maxItemCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"maxItemCount\":null, ";
      }
      if(feedCategory) {
        str += "\"feedCategory\":\"" + feedCategory + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedCategory\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(subtitle) {
        str += "\"subtitle\":\"" + subtitle + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"subtitle\":null, ";
      }
      str += "\"link\":" + link.toJsonString() + ", ";
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(language) {
        str += "\"language\":\"" + language + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"language\":null, ";
      }
      if(copyright) {
        str += "\"copyright\":\"" + copyright + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"copyright\":null, ";
      }
      str += "\"managingEditor\":" + managingEditor.toJsonString() + ", ";
      str += "\"webMaster\":" + webMaster.toJsonString() + ", ";
      if(contributor) {
        str += "\"contributor\":\"" + contributor + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contributor\":null, ";
      }
      if(pubDate) {
        str += "\"pubDate\":\"" + pubDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pubDate\":null, ";
      }
      if(lastBuildDate) {
        str += "\"lastBuildDate\":\"" + lastBuildDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastBuildDate\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(generator) {
        str += "\"generator\":\"" + generator + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"generator\":null, ";
      }
      if(docs) {
        str += "\"docs\":\"" + docs + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"docs\":null, ";
      }
      str += "\"cloud\":" + cloud.toJsonString() + ", ";
      if(ttl) {
        str += "\"ttl\":" + ttl + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ttl\":null, ";
      }
      str += "\"logo\":" + logo.toJsonString() + ", ";
      str += "\"icon\":" + icon.toJsonString() + ", ";
      if(rating) {
        str += "\"rating\":\"" + rating + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"rating\":null, ";
      }
      str += "\"textInput\":" + textInput.toJsonString() + ", ";
      if(skipHours) {
        str += "\"skipHours\":\"" + skipHours + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"skipHours\":null, ";
      }
      if(skipDays) {
        str += "\"skipDays\":\"" + skipDays + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"skipDays\":null, ";
      }
      if(outputText) {
        str += "\"outputText\":\"" + outputText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputText\":null, ";
      }
      if(outputHash) {
        str += "\"outputHash\":\"" + outputHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"outputHash\":null, ";
      }
      if(feedContent) {
        str += "\"feedContent\":\"" + feedContent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"feedContent\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(lastBuildTime) {
        str += "\"lastBuildTime\":" + lastBuildTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastBuildTime\":null, ";
      }
      if(publishedTime) {
        str += "\"publishedTime\":" + publishedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"publishedTime\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(lastUpdatedTime) {
        str += "\"lastUpdatedTime\":" + lastUpdatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastUpdatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "channelSource:" + channelSource + ", ";
      str += "channelCode:" + channelCode + ", ";
      str += "previousVersion:" + previousVersion + ", ";
      str += "fetchRequest:" + fetchRequest + ", ";
      str += "fetchUrl:" + fetchUrl + ", ";
      str += "feedServiceUrl:" + feedServiceUrl + ", ";
      str += "feedUrl:" + feedUrl + ", ";
      str += "feedFormat:" + feedFormat + ", ";
      str += "maxItemCount:" + maxItemCount + ", ";
      str += "feedCategory:" + feedCategory + ", ";
      str += "title:" + title + ", ";
      str += "subtitle:" + subtitle + ", ";
      str += "link:" + link + ", ";
      str += "description:" + description + ", ";
      str += "language:" + language + ", ";
      str += "copyright:" + copyright + ", ";
      str += "managingEditor:" + managingEditor + ", ";
      str += "webMaster:" + webMaster + ", ";
      str += "contributor:" + contributor + ", ";
      str += "pubDate:" + pubDate + ", ";
      str += "lastBuildDate:" + lastBuildDate + ", ";
      str += "category:" + category + ", ";
      str += "generator:" + generator + ", ";
      str += "docs:" + docs + ", ";
      str += "cloud:" + cloud + ", ";
      str += "ttl:" + ttl + ", ";
      str += "logo:" + logo + ", ";
      str += "icon:" + icon + ", ";
      str += "rating:" + rating + ", ";
      str += "textInput:" + textInput + ", ";
      str += "skipHours:" + skipHours + ", ";
      str += "skipDays:" + skipDays + ", ";
      str += "outputText:" + outputText + ", ";
      str += "outputHash:" + outputHash + ", ";
      str += "feedContent:" + feedContent + ", ";
      str += "status:" + status + ", ";
      str += "note:" + note + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "lastBuildTime:" + lastBuildTime + ", ";
      str += "publishedTime:" + publishedTime + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "lastUpdatedTime:" + lastUpdatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.ChannelFeedJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.ChannelFeedJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.channelSource !== undefined && obj.channelSource != null) {
    o.setChannelSource(obj.channelSource);
  }
  if(obj.channelCode !== undefined && obj.channelCode != null) {
    o.setChannelCode(obj.channelCode);
  }
  if(obj.previousVersion !== undefined && obj.previousVersion != null) {
    o.setPreviousVersion(obj.previousVersion);
  }
  if(obj.fetchRequest !== undefined && obj.fetchRequest != null) {
    o.setFetchRequest(obj.fetchRequest);
  }
  if(obj.fetchUrl !== undefined && obj.fetchUrl != null) {
    o.setFetchUrl(obj.fetchUrl);
  }
  if(obj.feedServiceUrl !== undefined && obj.feedServiceUrl != null) {
    o.setFeedServiceUrl(obj.feedServiceUrl);
  }
  if(obj.feedUrl !== undefined && obj.feedUrl != null) {
    o.setFeedUrl(obj.feedUrl);
  }
  if(obj.feedFormat !== undefined && obj.feedFormat != null) {
    o.setFeedFormat(obj.feedFormat);
  }
  if(obj.maxItemCount !== undefined && obj.maxItemCount != null) {
    o.setMaxItemCount(obj.maxItemCount);
  }
  if(obj.feedCategory !== undefined && obj.feedCategory != null) {
    o.setFeedCategory(obj.feedCategory);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.subtitle !== undefined && obj.subtitle != null) {
    o.setSubtitle(obj.subtitle);
  }
  if(obj.link !== undefined && obj.link != null) {
    o.setLink(obj.link);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.language !== undefined && obj.language != null) {
    o.setLanguage(obj.language);
  }
  if(obj.copyright !== undefined && obj.copyright != null) {
    o.setCopyright(obj.copyright);
  }
  if(obj.managingEditor !== undefined && obj.managingEditor != null) {
    o.setManagingEditor(obj.managingEditor);
  }
  if(obj.webMaster !== undefined && obj.webMaster != null) {
    o.setWebMaster(obj.webMaster);
  }
  if(obj.contributor !== undefined && obj.contributor != null) {
    o.setContributor(obj.contributor);
  }
  if(obj.pubDate !== undefined && obj.pubDate != null) {
    o.setPubDate(obj.pubDate);
  }
  if(obj.lastBuildDate !== undefined && obj.lastBuildDate != null) {
    o.setLastBuildDate(obj.lastBuildDate);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.generator !== undefined && obj.generator != null) {
    o.setGenerator(obj.generator);
  }
  if(obj.docs !== undefined && obj.docs != null) {
    o.setDocs(obj.docs);
  }
  if(obj.cloud !== undefined && obj.cloud != null) {
    o.setCloud(obj.cloud);
  }
  if(obj.ttl !== undefined && obj.ttl != null) {
    o.setTtl(obj.ttl);
  }
  if(obj.logo !== undefined && obj.logo != null) {
    o.setLogo(obj.logo);
  }
  if(obj.icon !== undefined && obj.icon != null) {
    o.setIcon(obj.icon);
  }
  if(obj.rating !== undefined && obj.rating != null) {
    o.setRating(obj.rating);
  }
  if(obj.textInput !== undefined && obj.textInput != null) {
    o.setTextInput(obj.textInput);
  }
  if(obj.skipHours !== undefined && obj.skipHours != null) {
    o.setSkipHours(obj.skipHours);
  }
  if(obj.skipDays !== undefined && obj.skipDays != null) {
    o.setSkipDays(obj.skipDays);
  }
  if(obj.outputText !== undefined && obj.outputText != null) {
    o.setOutputText(obj.outputText);
  }
  if(obj.outputHash !== undefined && obj.outputHash != null) {
    o.setOutputHash(obj.outputHash);
  }
  if(obj.feedContent !== undefined && obj.feedContent != null) {
    o.setFeedContent(obj.feedContent);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.lastBuildTime !== undefined && obj.lastBuildTime != null) {
    o.setLastBuildTime(obj.lastBuildTime);
  }
  if(obj.publishedTime !== undefined && obj.publishedTime != null) {
    o.setPublishedTime(obj.publishedTime);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.lastUpdatedTime !== undefined && obj.lastUpdatedTime != null) {
    o.setLastUpdatedTime(obj.lastUpdatedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

feedstoa.wa.bean.ChannelFeedJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.ChannelFeedJsBean.create(jsonObj);
  return obj;
};
