//////////////////////////////////////////////////////////
// <script src="/js/bean/imagestructjsbean-1.0.js"></script>
// Last modified time: 1377391580498.
//////////////////////////////////////////////////////////

var feedstoa = feedstoa || {};
feedstoa.wa = feedstoa.wa || {};
feedstoa.wa.bean = feedstoa.wa.bean || {};
feedstoa.wa.bean.ImageStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var url;
    var title;
    var link;
    var width;
    var height;
    var description;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getLink = function() { return link; };
    this.setLink = function(value) { link = value; };
    this.getWidth = function() { return width; };
    this.setWidth = function(value) { width = value; };
    this.getHeight = function() { return height; };
    this.setHeight = function(value) { height = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new feedstoa.wa.bean.ImageStructJsBean();

      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(link !== undefined && link != null) {
        o.setLink(link);
      }
      if(width !== undefined && width != null) {
        o.setWidth(width);
      }
      if(height !== undefined && height != null) {
        o.setHeight(height);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(link !== undefined && link != null) {
        jsonObj.link = link;
      } // Otherwise ignore...
      if(width !== undefined && width != null) {
        jsonObj.width = width;
      } // Otherwise ignore...
      if(height !== undefined && height != null) {
        jsonObj.height = height;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(link) {
        str += "\"link\":\"" + link + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"link\":null, ";
      }
      if(width) {
        str += "\"width\":" + width + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"width\":null, ";
      }
      if(height) {
        str += "\"height\":" + height + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"height\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "url:" + url + ", ";
      str += "title:" + title + ", ";
      str += "link:" + link + ", ";
      str += "width:" + width + ", ";
      str += "height:" + height + ", ";
      str += "description:" + description + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

feedstoa.wa.bean.ImageStructJsBean.create = function(obj) {
  var o = new feedstoa.wa.bean.ImageStructJsBean();

  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.link !== undefined && obj.link != null) {
    o.setLink(obj.link);
  }
  if(obj.width !== undefined && obj.width != null) {
    o.setWidth(obj.width);
  }
  if(obj.height !== undefined && obj.height != null) {
    o.setHeight(obj.height);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
    
  return o;
};

feedstoa.wa.bean.ImageStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = feedstoa.wa.bean.ImageStructJsBean.create(jsonObj);
  return obj;
};
