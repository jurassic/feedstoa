<%@ page import="com.feedstoa.ws.core.*, com.feedstoa.af.auth.*, com.feedstoa.fe.*, com.feedstoa.fe.core.*, com.feedstoa.fe.bean.*, com.feedstoa.fe.util.*, com.feedstoa.wa.service.*, com.feedstoa.util.*, com.feedstoa.helper.*" 
%><%@ page contentType="application/xml; charset=UTF-8" 
%><%
String requestUrl = request.getRequestURL().toString();
%><%
String rssContent = null;
FeedContentJsBean feedContent = FeedContentHelper.getInstance().getFeedContentByFeedUrl(requestUrl);
if(feedContent != null) {
	rssContent = feedContent.getContent();
}
if(rssContent != null && !rssContent.isEmpty()) {
%><%=rssContent%>
<%}%>
