<%@ page import="com.feedstoa.fe.*, com.feedstoa.fe.bean.*, com.feedstoa.util.*, com.feedstoa.helper.*" %>
<%@ page isErrorPage="true" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Page Not Found :(</title> 

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/bootstrap/blueicicle/bootstrap-2.0.css">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/jquery-ui-1.8.20/pepper-grinder/jquery-ui-1.8.20.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-website.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-mobile.css" media="only screen and (max-device-width: 480px)"/>
  </head>

  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="/home">Feed Stoa</a>
          <div class="nav-collapse">
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">
        <h1>404</h1>
		<p>
Page not found!
<br/>
-- Back to <a href="/home">Home</a>.
		</p>
      </div>

      <hr>

      <footer>
        <p>&copy; Feed Stoa 2012</p>
      </footer>
    </div> <!-- /container -->


<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
