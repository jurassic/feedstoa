<%@ page import="com.feedstoa.ws.core.*, com.feedstoa.af.util.*, com.feedstoa.af.auth.*, com.feedstoa.af.auth.user.*, com.feedstoa.fe.*, com.feedstoa.fe.core.*, com.feedstoa.fe.bean.*, com.feedstoa.fe.util.*, com.feedstoa.wa.service.*, com.feedstoa.util.*,  com.feedstoa.app.util.*, com.feedstoa.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%@ page errorPage="/error/UnknownError.jsp" 
%><%
// {1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String contextPath = request.getContextPath();
String servletPath = request.getServletPath();
String pathInfo = request.getPathInfo();
String queryString = request.getQueryString();
//...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
%><%
AuthUserService authUserService = AuthUserServiceFactory.getAuthUserService();
AuthUser authUser = authUserService.getCurrentUser(); // or req.getUserPrincipal()
String nickName = "";
String descriptionutUrl = null;
String comebackUrl = null;
String encodedComebackUrl = null;
if(authUser != null) {
    nickName = authUser.getNickname(); 
    descriptionutUrl = authUserService.createLogoutURL(request.getRequestURI());
} else {
    comebackUrl = request.getRequestURI();
    try {
        encodedComebackUrl = java.net.URLEncoder.encode(comebackUrl, "UTF-8");
    } catch(Exception e) {
        // ignore
    }
    if(encodedComebackUrl == null) {
        encodedComebackUrl = "";  // ???
    }
}
%><%
// [3] Local "global" variables.
//String editState = null;     // "state_new", "state_edit", "state_read" (?), "state_unknown" == null.
ChannelFeedJsBean feedBean = null;

// [4] Initial validation
// URL "/feed/edit/<guid>" ???
String guid = URLHelper.getInstance().getGuidFromPathInfo(request.getPathInfo());
// check first if guid is a valid GUID....
if(guid != null && !guid.isEmpty()) {   // TBD: validation...
    try {
        feedBean = new ChannelFeedWebService().getChannelFeed(guid);
    } catch(Exception e) {
        // ignore...
    }
	if(feedBean != null) {
	    //editState = "state_edit";
	    String feedUser = feedBean.getUser();
	    Long createdTime = feedBean.getCreatedTime();    // This cannot be null
	    long cutoffTime = new java.util.Date().getTime() - 12 * 3600 * 1000L;   // Arbitrary. 12 hours ago.
		if((feedUser != null && !feedUser.equals(userId)) || (createdTime != null && createdTime < cutoffTime)) {
		    // If the user is not the author,
		    // or, the feed is "too old", 
	    	// then make this feed non-editable!!!
	    	// Or, just forward it to "/channel/edit" ???
	    	// temporary
	    	response.sendRedirect(servletPath);
		}
	} else {
	    // guid is not null, but feedBean is not found...
	    // TBD: Show error message? Or, just redirect to the feed create page???
	    // temporary
    	//response.sendRedirect(servletPath);
	    
	    // Instead...
	    // Create a new bean with the specified guid...
	    // In some cases, this can be very useful.
	    // ....
	    
	    // Allow this only when the guid is valid...
	    if(GUID.isValid(guid)) {
	    	// Go ahead and us this guid..
	    } else {
	    	// Error
	    	response.sendRedirect(servletPath);
	    }
	}
} else {
    //editState = "state_new";
}

// [5] JSP/Javascript variables..
String feedFeedUrl = "";
String feedPubDate = "";
String feedTitle = "";
String feedDescription = "";
String feedLink = "";
String feedLinkUuid = "";
String managingEditor = "";
String editorEmail = "";
String editorName = "";
// ....
long feedCreatedTime = 0L;
long feedModifiedTime = 0L;
// ...
String channelCode = "";
String channelSource = "";
String channelCopyright = "";
Integer maxItemCount = 0;    // Zero is an invalid value...
if(feedBean != null) {
    feedFeedUrl = (feedBean.getFeedUrl() != null) ? feedBean.getFeedUrl() : feedFeedUrl;
    feedPubDate = (feedBean.getPubDate() != null) ? feedBean.getPubDate() : feedPubDate;

    feedDescription = (feedBean.getDescription() != null) ? feedBean.getDescription() : feedDescription;
    feedTitle = (feedBean.getTitle() != null) ? feedBean.getTitle() : feedTitle;
    
//    java.util.List<UriStructJsBean> links = feedBean.getLink();
//    if(links != null && !links.isEmpty()) {
//    	feedLink = links.get(0).getHref();
//    	if(feedLink == null) {
//    		feedLink = "";
//    	}
//    	// ???
//    	feedLinkUuid = links.get(0).getUuid();
//    	// ????
//    }
    UriStructJsBean link = feedBean.getLink();
    if(link != null) {
    	feedLink = link.getHref();
    	if(feedLink == null) {
    		feedLink = "";
    	}
    	// ???
    	feedLinkUuid = link.getUuid();
    	// ????
    }
    
    UserStructJsBean editorUser = feedBean.getManagingEditor();
    if(editorUser != null) {
    	editorEmail = editorUser.getEmail();
	    if(editorEmail == null) {
	    	editorEmail = "";     // ???
	    }
	    managingEditor = editorEmail;
	    editorName = editorUser.getName();
	    if(editorName != null && !editorName.isEmpty()) {
	    	managingEditor += " (" + editorName + ")";
	    } else {
	    	editorName = "";
	    }
    }

    feedCreatedTime = (feedBean.getCreatedTime() != null) ? feedBean.getCreatedTime() : feedCreatedTime;
    feedModifiedTime = (feedBean.getModifiedTime() != null) ? feedBean.getModifiedTime() : feedModifiedTime;

    maxItemCount = (feedBean.getMaxItemCount() != null) ? feedBean.getMaxItemCount() : maxItemCount;
    channelCode = (feedBean.getChannelCode() != null) ? feedBean.getChannelCode() : channelCode;
    channelSource = (feedBean.getChannelSource() != null) ? feedBean.getChannelSource() : channelSource;
    channelCopyright = (feedBean.getCopyright() != null) ? feedBean.getCopyright() : channelCopyright;
}

// .....
%><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <title>Channel Feed Editor | <%=brandDisplayName%></title>
    <meta name="author" content="Aery Software" />
    <meta name="description" content="<%=brandDisplayName%>. Feed made easy." />

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/bootstrap/blueicicle/bootstrap-2.0.css">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/jquery-ui-1.8.20/pepper-grinder/jquery-ui-1.8.20.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa.css"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/feedstoa-mobile.css" media="only screen and (max-device-width: 480px)"/>
    <!-- end CSS-->

<%
if(BrandingHelper.getInstance().isBrandFeedStoa()) {
%>
	<link rel="shortcut icon" href="/img/favicon-feedstoa.ico" />
	<link rel="icon" href="/img/favicon-feedstoa.ico" type="image/x-icon" />
<%
} else if(BrandingHelper.getInstance().isBrandFeedJoin()) {
%>
	<link rel="shortcut icon" href="/img/favicon-feedstoa.ico" />
	<link rel="icon" href="/img/favicon-feedstoa.ico" type="image/x-icon" />
<%
} else {
%>
	<link rel="shortcut icon" href="/img/favicon-feedstoa.ico" />
	<link rel="icon" href="/img/favicon-feedstoa.ico" type="image/x-icon" />
<%
}
%>

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://www.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=h0alKXyg4pUisu3N5jppJA&v=1" type="text/javascript"></script>

  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/home"><%=brandDisplayName%> (beta)</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a id="topmenu_nav_edit"href="/channel/edit" title="Create or edit blog channel feed">Edit</a></li>
            </ul>
<%
if(DebugUtil.isDevelFeatureEnabled(application)) {
%>
            <form class="navbar-search" method="GET" action="/feedview">
              <input id="input_topmenu_pageurl" type="text" class="search-query" placeholder="Feed URL" name="url" value=""></input>
            </form>
<%
}
%>
            <p class="navbar-text pull-right">
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+feedstoa+com?subject=Re: <%=brandDisplayName%>" title="Email us"><i class="icon-envelope icon-white"></i></a>
              &nbsp;
<%
if(DebugUtil.isDevelFeatureEnabled(application)) {
%>
<%
if(authUser != null) {
%>
              <a id="scribblememo_descriptionut_anchor" href="<%=descriptionutUrl%>" title="Logout from <%=brandDisplayName%>"><button class="btn-info btn-mini" id="scribblememo_descriptionut_button"><i class="icon-off icon-white"></i> Logout</button></a>
<%
} else {
%>
              <a id="scribblememo_login_anchor" href="/auth?comebackUrl=<%=encodedComebackUrl%>" title="Login to <%=brandDisplayName%>"><button class="btn-info btn-mini" id="scribblememo_login_button"><i class="icon-user icon-white"></i> Login</button></a>
<%
}
%>
<%
}
%>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="main" class="hero-unit">

      <form class="form-inline" id="form_feed_edit" method="POST" action="" accept-charset="UTF-8">

      <div id="feed_body">
      <fieldset name="fieldset_feed_body">
        <div id="feed_body_top">
        <table>
          <tr>
          <td>
            <label for="input_feed_feedurl" id="input_feed_feedurl_label" title="Channel feed URL">Feed URL:</label>
          </td>
          <td>
            <div id="feed_feedurl">
              <input type="text" name="input_feed_feedurl" id="input_feed_feedurl" readonly="readonly" value="<%=feedFeedUrl%>"></input>
            </div>
          </td>
          <td>
            &nbsp;
          </td>
          </tr>

          <tr>
          <td>
            <label for="input_feed_channelcode" id="input_feed_channelcode_label" title="Channel feed website channelcode">Channel Code:</label>
          </td>
          <td>
            <div id="feed_channelcode">
              <input type="text" name="input_feed_channelcode" id="input_feed_channelcode" value="<%=channelCode%>"></input>
            </div>
          </td>
          <td>
            &nbsp;
          </td>
          </tr>

          <tr>
          <td>
            <label for="input_feed_link" id="input_feed_link_label" title="Channel feed website link">Website Link:</label>
          </td>
          <td>
            <div id="feed_link">
              <input type="text" name="input_feed_link" id="input_feed_link" value="<%=feedLink%>"></input>
            </div>
          </td>
          <td>
            &nbsp;
          </td>
          </tr>

          <tr>
          <td>
            <label for="input_feed_editor" id="input_feed_editor_label" title="Channel feed website editor">Managing&nbsp;Editor:</label>
          </td>
          <td colspan="2">
            <div id="feed_editor">
              <input type="text" name="input_feed_editor_email" id="input_feed_editor_email" value="<%=editorEmail%>" placeholder="Email"></input>&nbsp;
              <input type="text" name="input_feed_editor_name" id="input_feed_editor_name" value="<%=editorName%>" placeholder="Name"></input>
            </div>
          </td>
          </tr>

          <tr>
          <td>
            <label for="input_feed_maxitemcount" id="input_feed_maxitemcount_label" title="Channel feed website maxitemcount">Max Item Count:</label>
          </td>
          <td>
            <div id="feed_maxitemcount">
              <input type="text" name="input_feed_maxitemcount" id="input_feed_maxitemcount" value="<%=maxItemCount%>"></input>
            </div>
          </td>
          <td>
            &nbsp;
          </td>
          </tr>

          <tr>
          <td>
            <label for="input_feed_copyright" id="input_feed_copyright_label" title="Channel feed website copyright">Copyright Statement:</label>
          </td>
          <td>
            <div id="feed_copyright">
              <textarea name="input_feed_copyright" id="input_feed_copyright" cols="75" rows="2"><%=channelCopyright%></textarea>
            </div>
          </td>
          <td>
            &nbsp;
          </td>
          </tr>

          <tr>
          <td>
            <label for="input_feed_title" id="input_feed_title_label" title="Title is required. Should be less than 100 characters.">Title:</label>
            <br/><br/>
            <span id="description_char_counter">500</span><br/>
          </td>
          <td>
            <div id="feed_title">
              <textarea name="input_feed_title" id="input_feed_title" cols="75" rows="2"><%=feedTitle%></textarea>
            </div>
          </td>
          <td>
            <span id="title_char_counter">100</span>
            <br/>
            <button type="button" class="btn btn-primary" name="button_feed_save" id="button_feed_save" title="Save/Update the channel" style="width:90px">Save</button> 
          </td>
          </tr>
        </table>
        </div>
        <div id="feed_body_main" class="feed_body_main_content">
          <div id="feed_feed_textarea">
              <textarea name="input_feed_description" id="input_feed_description" cols="105" rows="10"><%=feedDescription%></textarea>
          </div>
        </div>
      </fieldset>
      </div>

      </form>
      </div>  <!--   Hero unit  -->

      <hr>

      <footer>
        <p>
        &copy; <%=brandDisplayName%> 2012&nbsp; 
        <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a>
        <span class="pull-right">
<!--
            <a href="/about" title="About <%=brandDisplayName%>">About</a> |
-->
           <a href="/contact" title="Contact information">Contact</a> |
           <a href="http://blog.<%=appBrand%>.com/" title="<%=brandDisplayName%> blog">Blog</a>
        </span>
        </p>
      </footer>

    </div> <!--! end of #container -->



    <div id="div_status_floater" style="display: none;">
      <div id="status_message">
        <span id="span_status_message">(Status)</span>
      </div>
    </div>
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.7.2.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.8.20.custom.min.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery-ui-timepicker-addon.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.adapter.jquery.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/history.js/uncompressed/history.html4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/uuidutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/uristructjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/userstructjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/channelfeedjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/feeditemjsbean-1.0.js"></script>
	<script defer type="text/javascript" src="/js/plugins.js"></script>
    <script defer type="text/javascript" src="/js/script.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/fiveten-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <!-- end scripts-->


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var feedname = "Initial pre-feed trial";
    	var pageid = "contact";
    	signupHelper = new webstoa.SignupHelper(servicename, feedname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "feededit";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, <%=topLevelUrl%>, is a RSS feed helper service. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_tweet").click(function() {
            if(DEBUG_ENABLED) console.log("Tweet button clicked."); 

            if(!tweetHelper) {
                tweetHelper = new webstoa.TweetHelper();
            }
        	if(tweetHelper) {
	            var tweetTitle = '<%=brandDisplayName%>';
	            var tweetMessage = '<%=brandDisplayName%>, <%=topLevelUrl%>, is a RSS feed helper service. Check it out.';
	            tweetHelper.tweet(tweetTitle, tweetMessage);
        	} else {
        		// ????
        	}
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		$("#div_status_floater").textInOut(msg, type, time);
    }    
    function countDescriptionChar() {
    	var max = 500; 
    	var value = $('#input_feed_description').val();
        var len = value.length;
    	var remaining = (max - len) >= 0 ? (max - len) : 0; 
        if (len > max) {
         	$('#input_feed_description').val(value.substring(0, max));
        } else {
         	//$('#input_feed_description').val(value);
        }
        $('#description_char_counter').text(remaining);
    }
    function countChannelTitleChar() {
    	var max = 100;    // TBD
    	var value = $('#input_feed_title').val();
        var len = value.length;
    	var remaining = (max - len) >= 0 ? (max - len) : 0; 
        if (len > max) {
         	$('#input_feed_title').val(value.substring(0, max));
        } else {
         	//$('#input_feed_title').val(value);
        }
        $('#title_char_counter').text(remaining);
    }
    </script>

    <script>
    function disableSaveButton(delay)
    {
    	$("#button_feed_save").attr('disabled','disabled');
    	var milli;
    	if(delay) {
    		milli = delay;
    	} else {
    		milli = 7500;  // ???
    	}
    	setTimeout(resetAndEnableSaveButton, milli);
    }
    function enableSaveButton()
    {
    	setTimeout(resetAndEnableSaveButton, 1250);  // ???
    }
    function resetAndEnableSaveButton()
    {
    	$("#button_feed_save").removeAttr('disabled');
    }
    </script>

    <script>
    // "Init"
    $(function() {
<%
if(feedBean == null) {
%>
        $("#input_feed_title").focus().select();
<%
} else {
%>
        $("#input_feed_title").focus();
<%
}
%>
        // TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();
    
        // Background image test.
        //$("#container").css("background-image", 'url(/img/bg/snowycabin.jpg)');
        //$(document.body).css("background", "grey");

        // TBD: Location...
    	var params = {x: 430, y: 65, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(23771, 8);   // ???
    	//fiveTenTimer.start();
    });
    $(function() {
    	countChannelTitleChar();
        $("#input_feed_title").keyup(function(event) {
			countChannelTitleChar();
        });
    	countDescriptionChar();
        $("#input_feed_description").keyup(function(event) {
			countDescriptionChar();
        });
    });
    </script>

    <script>
    // App-related vars.
    var userGuid;
    var feedJsBean;
    var editState = 'state_new';
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
<%
if(feedBean == null) {
%>
    // Create a new guid with the specified guid, if any.
    feedJsBean = new feedstoa.wa.bean.ChannelFeedJsBean();
    if(userGuid) {
    	feedJsBean.setUser(userGuid);
    }
<%
if(guid != null && !guid.isEmpty()) {
%>
    feedJsBean.setGuid('<%=guid%>');
<%
}
%>
    // ....
<%
} else {
%>
    editState = 'state_edit';
    var feedJsObjectStr = '<%=feedBean.toEscapedJsonStringForJavascript()%>';
    if(DEBUG_ENABLED) console.log("feedJsObjectStr = " + feedJsObjectStr);
    feedJsBean = feedstoa.wa.bean.ChannelFeedJsBean.fromJSON(feedJsObjectStr);
<%
}
%>

    if(DEBUG_ENABLED) console.log("editState = " + editState);
    if(DEBUG_ENABLED) console.log("feedJsBean = " + feedJsBean.toString());
    </script>

    <script>
    // Additional Init based on editState.
    $(function() {
      if(editState == 'state_new') {
        $("#button_feed_save").text("Save");
        $("#input_feed_channelcode").removeAttr('readonly');
        //$("#input_feed_feedurl").removeAttr('readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "white");
      } else if(editState == 'state_edit') {
        $("#button_feed_save").text("Update");
    	$("#input_feed_channelcode").attr('readonly','readonly');
    	$("#input_feed_feedurl").attr('readonly','readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "ivory");
      } else {
    	// 'state_read'  // TBD...
        $("#button_feed_save").hide();
        $("#input_feed_channelcode").removeAttr('readonly');
        //$("#input_feed_feedurl").removeAttr('readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "gray");
    	// TBD: make title/content readonly...
      }
    });
    </script>


    <script>
    // Ajax form handlers.
  $(function() {
    $("#button_feed_save").click(function() {
      // TBD:
      // Disable the button here
      // and enable again (some delay) after ajax call returns
      //     or it times out (because the call has failed, etc.).
      
    
      //if(DEBUG_ENABLED) console.log("Save button clicked.");
      updateStatus('Saving the feed...', 'info', 5550);
      disableSaveButton();
    	
      saveChannelFeedJsBean();

    });
  });
  </script>

  <script>
  var saveChannelFeedJsBean = function() {

      // validate and process form here      
      var feedLink = $("#input_feed_link").val().trim();
      if(DEBUG_ENABLED) console.log("feedLink = " + feedLink);
      if(feedLink == "") {
    	  if(DEBUG_ENABLED) console.log("feedLink is empty");
          updateStatus('Channel website link cannot be empty.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      var title = $("#input_feed_title").val().trim();
      if(DEBUG_ENABLED) console.log("title = " + title);
      if(title == "") {
          if(DEBUG_ENABLED) console.log("title is empty");
          updateStatus('Title cannot be empty.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      var description = $("#input_feed_description").val().trim();
      if(DEBUG_ENABLED) console.log("description = " + description);
      if(description == "") {
          if(DEBUG_ENABLED) console.log("description is empty");
          updateStatus('Description cannot be empty.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      var channelCode = $("#input_feed_channelcode").val().trim();
      if(DEBUG_ENABLED) console.log("channelCode = " + channelCode);
      var maxItemCount = $("#input_feed_maxitemcount").val().trim();
      if(maxItemCount == '') {
    	  maxItemCount = 0;  // ???
      }
      if(DEBUG_ENABLED) console.log("maxItemCount = " + maxItemCount);

      var channelCopyright = $("#input_feed_copyright").val().trim();
      if(DEBUG_ENABLED) console.log("channelCopyright = " + channelCopyright);

      var editorEmail = $("#input_feed_editor_email").val().trim();
      if(DEBUG_ENABLED) console.log("editorEmail = " + editorEmail);
      var editorName = $("#input_feed_editor_name").val().trim();
      if(DEBUG_ENABLED) console.log("editorName = " + editorName);

      // etc.

      
      //var channelSource = $("#input_feed_info_channelsource").val().trim();
      //if(DEBUG_ENABLED) console.log("channelSource = " + channelSource);
      
      // Update the beans with input values

      if(userGuid) {   
         //feedJsBean.setUser(userGuid);  // This should not be necessary..
      }
      var feedLinkStruct = new feedstoa.wa.bean.UriStructJsBean();
      feedLinkStruct.setHref(feedLink);
<%if(feedLinkUuid == null || feedLinkUuid.isEmpty()) {%>
      var feedLinkUuid = generateUuid();
<%} else {%>
      var feedLinkUuid = '<%=feedLinkUuid%>';
<%}%>
      feedLinkStruct.setUuid(feedLinkUuid);  // ???
//      var feedLinks = [];
//      feedLinks[0] = feedLinkStruct;
//      feedJsBean.setLink(feedLinks);
      feedJsBean.setLink(feedLinkStruct);
      feedJsBean.setTitle(title);
      feedJsBean.setDescription(description);
      feedJsBean.setChannelCode(channelCode);
      feedJsBean.setMaxItemCount(maxItemCount);
      feedJsBean.setCopyright(channelCopyright);
      var editorStruct = new feedstoa.wa.bean.UserStructJsBean();
      editorStruct.setEmail(editorEmail);
      editorStruct.setName(editorName);
      feedJsBean.setManagingEditor(editorStruct);
      // ...

      if(editState != 'state_new') {
           feedJsBean.setFeedServiceUrl('<%=topLevelUrl%>');   // ???
   	  }
      // ...

      // Update the modifiedTime field ....
   	  if(editState != 'state_new') {
   		  var modifiedTime = (new Date()).getTime();
          feedJsBean.setModifiedTime(modifiedTime);
   	  }

      // Payload...
      var feedJsonStr = JSON.stringify(feedJsBean);
      if(DEBUG_ENABLED) console.log("feedJsonStr = " + feedJsonStr);
      var payload = "{\"channelFeed\":" + feedJsonStr;
      // TBD...
      payload += "}";
      if(DEBUG_ENABLED) console.log("payload = " + payload);
      
      
      // Web service settings.
      var feedPostEndPoint = "<%=topLevelUrl%>" + "ajax/channelfeed";  // TBD: ...
      if(DEBUG_ENABLED) console.log("feedPostEndPoint =" + feedPostEndPoint);
      var httpMethod;
      var webServiceUrl;
      if(editState == 'state_new') {
    	  httpMethod = "POST";
    	  webServiceUrl = feedPostEndPoint;
	  } else if(editState == 'state_edit') {
    	  httpMethod = "PUT";
    	  webServiceUrl = feedPostEndPoint + "/" + feedJsBean.getGuid();
      } else {
    	  // ???
   		  if(DEBUG_ENABLED) console.log("Error. Invalid editState = " + editState);
          updateStatus('Error occurred.', 'error', 5550);
          enableSaveButton();
    	  return false;
      }

      if(httpMethod) {
        $.ajax({
    	    type: httpMethod,
    	    url: webServiceUrl,
    	    data: payload,
    	    dataType: "json",
    	    contentType: "application/json; charset=UTF-8",
    	    success: function(data, textStatus) {
    	      // TBD
    	      if(DEBUG_ENABLED) console.log("feedJsBean successfully saved. textStatus = " + textStatus);
    	      if(DEBUG_ENABLED) console.log("data = " + data);
    	      if(DEBUG_ENABLED) console.log(data);

    	      // History
    	      // Do this only if the current editState == 'state_new'
    	      if(editState == 'state_new') {
	    	      var History = window.History;
	    	      if(History) {
	   	    	    	// TBD: Need to html escape????
	    				var htmlPageTitle = '<%=brandDisplayName%> - ' + feedJsBean.getTitle();
	    	            // TBD: This does not work on HTML4 browsers (e.g., IE)
				        History.pushState({state:1}, htmlPageTitle, "<%=servletPath%>/" + feedJsBean.getGuid());
	    	      }
    	      }
    	      
    	      // If it is currently editState == 'state_new', change it to 'state_edit'.
    	      // If it is currently editState == 'state_edit', that's ok too.
    	      editState = 'state_edit';

    	      // Parse data...
    	      if(data) {   // POST only..
	    	      // Replace/Update the JsBeans ...

	    	      var feedObj = data["channelFeed"];
	    	      if(DEBUG_ENABLED) console.log("feedObj = " + feedObj);
	    	      if(DEBUG_ENABLED) console.log(feedObj);
	    	      feedJsBean = feedstoa.wa.bean.ChannelFeedJsBean.create(feedObj);
	    	      if(DEBUG_ENABLED) console.log("New feedJsBean = " + feedJsBean);
	    	      if(DEBUG_ENABLED) console.log(feedJsBean);
	    
	    	      // Delay this just a bit so that the server has time to actually save it (Note: we are using async call for saving).
	    	      // This helps when a new feed is saved. 
	    	      // TBD: In both save/update, various buttons should be disabled before the ajax call
	    	      //   and they should be re-enabled after the call successuflly returns or after certain preset time.
	    	      window.setTimeout(refreshFormFields, 1250);
	              updateStatus('Channel feed successfully saved.', 'info', 5550);
    	      } else {
    	    	  // ignore
    	    	  // PUT currently returns "OK" only.
    	    	  // TBD: Change the ajax server side code so that it returns the updated data.
    	    	  //      Based on the returned/updated data, some fields (e.g., shortUrl) may need to be updated....
	              updateStatus('Channel feed successfully updated.', 'info', 5550);
    	      } 
    	      enableSaveButton();
    	    },
    	    error: function(req, textStatus) {
          	    if(DEBUG_ENABLED) console.log('Failed to save the feed. status = ' + textStatus);
    	    	updateStatus('Failed to save the feed. status = ' + textStatus, 'error', 5550);
    	    	enableSaveButton();
    	    }
	   	  });
      } else {
    	  // ???
    	  enableSaveButton();
      }

  };
  </script>

  <script>
  // Refresh input fields...
  var refreshFormFields = function() {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");

	if(editState == 'state_new') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);

        $("#button_feed_save").text("Save");
        $("#input_feed_channelcode").removeAttr('readonly');
        //$("#input_feed_feedurl").removeAttr('readonly');
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "white");

		// Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%>';
        $('title').text(htmlPageTitle);
	} else if(editState == 'state_edit') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);

		var htmlPageTitle = '<%=brandDisplayName%>';
		if(feedJsBean) {
			var title = feedJsBean.getTitle();
			htmlPageTitle += ' - ' + title;    // TBD: Need to html escape????

		    // TBD:
			var feedUrl = feedJsBean.getFeedUrl();
			$("#input_feed_feedurl").val(feedUrl);				
			// Refresh other fields????
			
			// TBD: Update the feed title???
		    // Is that necessary?
			
	        $("#button_feed_save").text("Update");
	        $("#input_feed_channelcode").attr('readonly','readonly');
	    	$("#input_feed_feedurl").attr('readonly','readonly');
	    	//$(document.body).css("background", "silver");
	    	//$("#container").css("background", "ivory");
		} else {
			// ???
		}	

		// Refresh the title...
        $('title').text(htmlPageTitle);
	} else if(editState == 'state_read') {
		if(DEBUG_ENABLED) console.log("editState = " + editState);
		
		// ???
        $("#button_feed_save").hide();
    	//$(document.body).css("background", "silver");
    	//$("#container").css("background", "gray");

		// Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%> (ReadOnly)';
        $('title').text(htmlPageTitle);
	} else {
		if(DEBUG_ENABLED) console.log("invalid state: editState = " + editState);

		// ????
		// Refresh the title...
		var htmlPageTitle = '<%=brandDisplayName%>';
        $('title').text(htmlPageTitle);
	}
  };
  </script>

<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
      <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
      <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->

  </body>
</html>