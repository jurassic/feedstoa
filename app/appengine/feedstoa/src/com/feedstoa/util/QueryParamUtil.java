package com.feedstoa.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

public class QueryParamUtil
{
    private static final Logger log = Logger.getLogger(QueryParamUtil.class.getName());

    private QueryParamUtil() {}

    //public static final String PARAM_PAGEURL = "pageurl";
    public static final String PARAM_URL = "url";

    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_COUNT = "count";
    public static final String PARAM_PAGESIZE = "pagesize";
    public static final String PARAM_ORDERING = "ordering";    // "field [asc/desc]"

    

    public static String getParamUrl(HttpServletRequest request)
    {
        String paramUrl = null;
        if(request != null) {
            paramUrl = request.getParameter(PARAM_URL);
        }
        return paramUrl;
    }

    
    public static Long getParamOffset(HttpServletRequest request)
    {
        Long paramOffset = null;
        if(request != null) {
            String strOffset = request.getParameter(PARAM_OFFSET);
            if(strOffset != null) {
                try {
                    paramOffset = Long.parseLong(strOffset);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
        }
        return paramOffset;
    }

    public static Integer getParamCount(HttpServletRequest request)
    {
        Integer paramCount = null;
        if(request != null) {
            String strCount = request.getParameter(PARAM_COUNT);
            if(strCount != null) {
                try {
                    paramCount = Integer.parseInt(strCount);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
        }
        return paramCount;
    }

    public static Integer getParamPageSize(HttpServletRequest request)
    {
        Integer paramPageSize = null;
        if(request != null) {
            String strPageSize = request.getParameter(PARAM_PAGESIZE);
            if(strPageSize != null) {
                try {
                    paramPageSize = Integer.parseInt(strPageSize);
                } catch(NumberFormatException e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strPageSize, e);
                }
            }
        }
        return paramPageSize;
    }

    public static String getParamOrdering(HttpServletRequest request)
    {
        String paramOrdering = null;
        if(request != null) {
            paramOrdering = request.getParameter(PARAM_ORDERING);
        }
        return paramOrdering;
    }

    
    // TBD:
    
//    public static Map<String, String> parseQueryString(HttpServletRequest request)
//    {
//        Map<String, String> paramMap = new HashMap<String, String>();
//        if(request != null) {
//           // TBD ...            
//        }
//        return paramMap;
//    }

}
