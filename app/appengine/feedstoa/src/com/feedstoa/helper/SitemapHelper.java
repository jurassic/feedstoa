package com.feedstoa.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.FetchRequestJsBean;
import com.feedstoa.wa.service.FetchRequestWebService;
import com.feedstoa.wa.service.UserWebService;


public class SitemapHelper
{
    private static final Logger log = Logger.getLogger(SitemapHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;   // Sitemap.xml max count...
    private static final int DEFAULT_MAX_MEMO_COUNT = 2500;
    // temporary

    private UserWebService userWebService = null;
    private FetchRequestWebService fetchRequestWebService = null;
    // ...

    private SitemapHelper() {}

    // Initialization-on-demand holder.
    private static final class SitemapHelperHolder
    {
        private static final SitemapHelper INSTANCE = new SitemapHelper();
    }

    // Singleton method
    public static SitemapHelper getInstance()
    {
        return SitemapHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private FetchRequestWebService getFetchRequestWebService()
    {
        if(fetchRequestWebService == null) {
            fetchRequestWebService = new FetchRequestWebService();
        }
        return fetchRequestWebService;
    }

    public List<FetchRequestJsBean> findRecentFetchRequests()
    {
        return findRecentFetchRequests(DEFAULT_MAX_MEMO_COUNT);
    }

    public List<FetchRequestJsBean> findRecentFetchRequests(int maxCount)
    {
        List<FetchRequestJsBean> fetchRequests = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = maxCount;   // TBD: Validation??? ( maxCount < MAX_MEMO_COUNT ???? )
            fetchRequests = getFetchRequestWebService().findFetchRequests(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find fetchRequests.", e);
            return null;
        }

        return fetchRequests;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
