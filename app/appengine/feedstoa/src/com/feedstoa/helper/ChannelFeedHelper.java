package com.feedstoa.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.common.RefreshStatus;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.ChannelFeedJsBean;
import com.feedstoa.wa.service.ChannelFeedWebService;
import com.feedstoa.wa.service.UserWebService;


public class ChannelFeedHelper
{
    private static final Logger log = Logger.getLogger(ChannelFeedHelper.class.getName());

    private ChannelFeedHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private ChannelFeedWebService channelFeedWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private ChannelFeedWebService getChannelFeedService()
    {
        if(channelFeedWebService == null) {
            channelFeedWebService = new ChannelFeedWebService();
        }
        return channelFeedWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ChannelFeedHelperHolder
    {
        private static final ChannelFeedHelper INSTANCE = new ChannelFeedHelper();
    }

    // Singleton method
    public static ChannelFeedHelper getInstance()
    {
        return ChannelFeedHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public ChannelFeedJsBean getChannelFeed(String guid) 
    {
        ChannelFeedJsBean bean = null;
        try {
            bean = getChannelFeedService().getChannelFeed(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public ChannelFeedJsBean getChannelFeedByTargetUrl(String targetUrl) 
    {
        ChannelFeedJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<ChannelFeedJsBean> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }

    

    public List<String> getChannelFeedKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getChannelFeedService().findChannelFeedKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<ChannelFeedJsBean> getChannelFeedsForRefreshProcessing(Integer maxCount)
    {
        List<ChannelFeedJsBean> channelFeeds = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return channelFeeds;
    }


}
