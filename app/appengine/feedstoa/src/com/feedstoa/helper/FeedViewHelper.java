package com.feedstoa.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.ChannelFeedJsBean;
import com.feedstoa.wa.service.ChannelFeedWebService;
import com.feedstoa.wa.service.FeedItemWebService;
import com.feedstoa.wa.service.UserWebService;


public class FeedViewHelper
{
    private static final Logger log = Logger.getLogger(FeedViewHelper.class.getName());
    
    // temporary
    private static final int MAX_IMAGE_COUNT = 5000;
    private static final int DEFAULT_MAX_IMAGE_COUNT = 60;
    // temporary

    private UserWebService userWebService = null;
    private ChannelFeedWebService channelFeedWebService = null;
    private FeedItemWebService feedItemWebService = null;
    // ...

    private FeedViewHelper() {}

    // Initialization-on-demand holder.
    private static final class FeedViewHelperHolder
    {
        private static final FeedViewHelper INSTANCE = new FeedViewHelper();
    }

    // Singleton method
    public static FeedViewHelper getInstance()
    {
        return FeedViewHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ChannelFeedWebService getChannelFeedWebService()
    {
        if(channelFeedWebService == null) {
            channelFeedWebService = new ChannelFeedWebService();
        }
        return channelFeedWebService;
    }
    private FeedItemWebService getFeedItemWebService()
    {
        if(feedItemWebService == null) {
            feedItemWebService = new FeedItemWebService();
        }
        return feedItemWebService;
    }

    
    
    // (1) useCacheAge == 0 or < 0 => Do not use "cache"
    // (2) useCacheAge == null     => Always use "cache"
    // (3) useCacheAge == X > 0    => if cache data found within now-X*1000L, use it. Otherwise, fetch it again...
    public ChannelFeedJsBean getChannelFeed(String targetUrl, Integer useCacheAge)
    {
        // TBD:
        // Check the datastore, and if webpage is found for the given targetUrl, within the last X hours, ...
        // just return the most recent result
        if(useCacheAge == null || useCacheAge > 0) {
            ChannelFeedJsBean existingChannelFeed = findChannelFeedByFeedUrl(targetUrl, useCacheAge);
            if(existingChannelFeed != null) {
                if(log.isLoggable(Level.FINE)) log.fine(">>>>> Found an existing Web Page: " + existingChannelFeed);
                return existingChannelFeed;
            } else {
                // continue
            }
        } else {
            // Do not use "cache".
        }
        // ....

        ChannelFeedJsBean channelFeed = null;

        channelFeed = new ChannelFeedJsBean();
        channelFeed.setFeedUrl(targetUrl);
        // ...
        
        
        // temporary
        if(channelFeed != null) {
            try {
                getChannelFeedWebService().createChannelFeed(channelFeed);
            } catch (WebException e) {
                // ignore
                log.log(Level.WARNING, "Failed to save the Web page bean.", e);
            }
        }
        // temporary

        return channelFeed;
    }
    
  
    
    // age in seconds..
    public ChannelFeedJsBean findChannelFeedByFeedUrl(String targetPage, Integer age)
    {
        ChannelFeedJsBean channelFeed = null;
        try {
            String filter = "targetPage=='" + targetPage + "'";
            if(age != null && age > 0) {
                long now = System.currentTimeMillis();
                long cutoff = now - (age * 1000L);
                filter += " && createdTime > " + cutoff;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<ChannelFeedJsBean> beans = getChannelFeedWebService().findChannelFeeds(filter, ordering, null, null, null, null, offset, count);
            if(beans == null || beans.isEmpty()) {
                if(log.isLoggable(Level.INFO)) log.info("No ChannelFeed meeting the criteria has been found. targetPage = " + targetPage);
            } else {
                channelFeed = beans.get(0);
            }
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find channelFeeds.", e);
            //return null;
        }
        return channelFeed;
    }

    
    
    
    
    
    
    // TBD
    
    
    public List<ChannelFeedJsBean> findRecentChannelFeeds()
    {
        return findRecentChannelFeeds(DEFAULT_MAX_IMAGE_COUNT);
    }

    // TBD....
    // TBD: filter -> Target site and/or pingUrl ???
    public List<ChannelFeedJsBean> findRecentChannelFeeds(int maxCount)
    {
        List<ChannelFeedJsBean> channelFeeds = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = maxCount;   // TBD: Validation??? ( maxCount < MAX_IMAGE_COUNT ???? )
            channelFeeds = getChannelFeedWebService().findChannelFeeds(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find channelFeeds.", e);
            return null;
        }

        return channelFeeds;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
