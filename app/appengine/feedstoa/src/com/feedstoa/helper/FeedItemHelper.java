package com.feedstoa.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.common.RefreshStatus;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.FeedItemJsBean;
import com.feedstoa.wa.service.ChannelFeedWebService;
import com.feedstoa.wa.service.FeedItemWebService;
import com.feedstoa.wa.service.UserWebService;


public class FeedItemHelper
{
    private static final Logger log = Logger.getLogger(FeedItemHelper.class.getName());

    private FeedItemHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private ChannelFeedWebService channelFeedWebService = null;
    private FeedItemWebService feedItemWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private ChannelFeedWebService getChannelFeedService()
    {
        if(channelFeedWebService == null) {
            channelFeedWebService = new ChannelFeedWebService();
        }
        return channelFeedWebService;
    }
    private FeedItemWebService getFeedItemService()
    {
        if(feedItemWebService == null) {
            feedItemWebService = new FeedItemWebService();
        }
        return feedItemWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FeedItemHelperHolder
    {
        private static final FeedItemHelper INSTANCE = new FeedItemHelper();
    }

    // Singleton method
    public static FeedItemHelper getInstance()
    {
        return FeedItemHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public FeedItemJsBean getFeedItem(String guid) 
    {
        FeedItemJsBean bean = null;
        try {
            bean = getFeedItemService().getFeedItem(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public FeedItemJsBean getFeedItemByTargetUrl(String targetUrl) 
    {
        FeedItemJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<FeedItemJsBean> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }

    
    
    // ????
    // This has been copied from FetchRequest...
//
//    public List<String> getFeedItemKeysForRefreshProcessing(Integer maxCount)
//    {
//        List<String> keys = null;
//        long now = System.currentTimeMillis();
//        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
//        String ordering = "nextRefreshTime asc";
//        try {
//            keys = getFeedItemService().findFeedItemKeys(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (WebException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
//        }
//
//        return keys;
//    }
//    
//    public List<FeedItemJsBean> getFeedItemsForRefreshProcessing(Integer maxCount)
//    {
//        List<FeedItemJsBean> feedItems = null;
//        long now = System.currentTimeMillis();
//        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
//        String ordering = "nextRefreshTime asc";
//        try {
//            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (WebException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
//        }
//
//        return feedItems;
//    }
//

}
