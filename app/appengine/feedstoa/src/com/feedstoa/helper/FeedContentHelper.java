package com.feedstoa.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.common.RefreshStatus;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.FeedContentJsBean;
import com.feedstoa.wa.service.FeedContentWebService;
import com.feedstoa.wa.service.UserWebService;


public class FeedContentHelper
{
    private static final Logger log = Logger.getLogger(FeedContentHelper.class.getName());

    private FeedContentHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private FeedContentWebService feedContentWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private FeedContentWebService getFeedContentService()
    {
        if(feedContentWebService == null) {
            feedContentWebService = new FeedContentWebService();
        }
        return feedContentWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FeedContentHelperHolder
    {
        private static final FeedContentHelper INSTANCE = new FeedContentHelper();
    }

    // Singleton method
    public static FeedContentHelper getInstance()
    {
        return FeedContentHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public FeedContentJsBean getFeedContent(String guid) 
    {
        FeedContentJsBean bean = null;
        try {
            bean = getFeedContentService().getFeedContent(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    /*
    public FeedContentJsBean getFeedContentByFeedUrl(String feedUrl) 
    {
    	log.fine("BEGIN: getFeedContentByFeedUrl() feedUrl = " + feedUrl);

    	FeedContentJsBean bean = null;
        try {
            String filter = "feedUrl=='" + feedUrl + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<FeedContentJsBean> beans = getFeedContentService().findFeedContents(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
            	bean = beans.get(0);
                log.fine("FeedContent retrieved for feedUrl = " + feedUrl);

                // The "content" field (and, possibly others with defaultFetch==f) are not retrieved by "find" methods...
                // Need to fetch the whole bean through its guid....
                String guid = bean.getGuid();
                try {
	                FeedContentJsBean fullBean = getFeedContentService().getFeedContent(guid);
	                if(fullBean != null) {
	                	bean = fullBean;
	                } else {
	                	// ???
	                    log.info("FeedContent not found for guid = " + guid);            	
	                }
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }                
            } else {
                log.info("FeedContent not found for feedUrl = " + feedUrl);            	
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for feedUrl = " + feedUrl, e);
        }

    	log.fine("END: getFeedContentByFeedUrl()");
    	return bean;
    }

     */

    public FeedContentJsBean getFeedContentByFeedUrl(String feedUrl) 
    {
    	if(log.isLoggable(Level.FINE)) log.fine("BEGIN: getFeedContentByFeedUrl() feedUrl = " + feedUrl);

        // The "content" field (and, possibly others with defaultFetch==f) are not retrieved by "find" methods...
        // Need to fetch the whole bean through its guid/key....
    	FeedContentJsBean bean = null;
        try {
            String filter = "feedUrl=='" + feedUrl + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<String> keys = getFeedContentService().findFeedContentKeys(filter, ordering, null, null, null, null, offset, count);
            if(keys != null && !keys.isEmpty()) {
            	String key = keys.get(0);
                if(log.isLoggable(Level.FINE)) log.fine("FeedContent retrieved: key = " + key + ", for feedUrl = " + feedUrl);
                try {
	                bean = getFeedContentService().getFeedContent(key);
	                if(bean == null) {
	                	// ???
	                    if(log.isLoggable(Level.INFO)) log.info("FeedContent not found for key = " + key);	
	                }
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for key = " + key, e);
                }                
            } else {
                if(log.isLoggable(Level.INFO)) log.info("FeedContent not found for feedUrl = " + feedUrl);            	
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for feedUrl = " + feedUrl, e);
        }

    	log.fine("END: getFeedContentByFeedUrl()");
    	return bean;
    }


}
