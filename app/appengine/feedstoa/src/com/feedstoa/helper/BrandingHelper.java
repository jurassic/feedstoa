package com.feedstoa.helper;

import java.util.logging.Logger;

import com.feedstoa.app.util.ConfigUtil;
import com.feedstoa.common.AppBrand;


// temporary
public class BrandingHelper
{
    private static final Logger log = Logger.getLogger(BrandingHelper.class.getName());

    private String mBrand = null;
    private BrandingHelper() 
    {
        mBrand = initBrand();
    }

    // Initialization-on-demand holder.
    private static final class BrandingHelperHolder
    {
        private static final BrandingHelper INSTANCE = new BrandingHelper();
    }

    // Singleton method
    public static BrandingHelper getInstance()
    {
        return BrandingHelperHolder.INSTANCE;
    }

    private String initBrand()
    {
        String brand = ConfigUtil.getApplicationBrand();
        if(!AppBrand.isValidBrand(brand)) {
            brand = AppBrand.getDefaultBrand();
        }
        return brand;
    }

    
    public String getAppBrand()
    { 
        return mBrand;
    }
    
    public String getBrandDisplayName()
    { 
        return AppBrand.getDisplayName(mBrand);
    }

    public boolean isBrandFeedStoa()
    {
        return AppBrand.BRAND_FEEDSTOA.equals(mBrand);
    }

    public boolean isBrandFeedJoin()
    {
        return AppBrand.BRAND_FEEDJOIN.equals(mBrand);
    }


}
