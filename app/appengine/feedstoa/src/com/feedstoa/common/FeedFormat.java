package com.feedstoa.common;

import java.util.logging.Logger;


// TBD: ....
public class FeedFormat
{
    private static final Logger log = Logger.getLogger(FeedFormat.class.getName());

    // Static methods only.
    private FeedFormat() {}

    // TBD
    public static final String FORMAT_RSS2 = "RSS 2.0"; 
    public static final String FORMAT_RSS1 = "RSS 1.0"; 
    public static final String FORMAT_ATOM1 = "Atom 1.0"; 
    // ...
    
}
