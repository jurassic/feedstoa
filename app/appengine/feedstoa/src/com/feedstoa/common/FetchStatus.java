package com.feedstoa.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

// TBD: ....
public class FetchStatus
{
    private static final Logger log = Logger.getLogger(FetchStatus.class.getName());

    // Static methods only.
    private FetchStatus() {}

    // TBD
    public static final int STATUS_UNKNOWN = -1;          // Use a big number such as 1000 ???
    public static final int STATUS_PAUSED = 0;            // ???
    public static final int STATUS_REQUESTED = 1;         // Currently, not being used.
    public static final int STATUS_CREATED = 2;           // FetchRequest obj has been created. (not necessarily persisted)
    public static final int STATUS_SCHEDULED = 4;         // ???
    public static final int STATUS_QUEUED = 8;            // ??? Processed at least once before, but will be re-processed through "refresh" cron...
    public static final int STATUS_PROCESSING = 16;       // Before the scraping attempt.   (For error handling....)  ????
    public static final int STATUS_FAILED = 32;           // after the scraping attempt. failed???
    public static final int STATUS_PROCESSED = 64;        // after the scraping attempt. success???
    // etc.
    //...
    
}
