package com.feedstoa.common;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// Poor man's branding...
public class AppBrand
{
    private static final Logger log = Logger.getLogger(AppBrand.class.getName());

    private AppBrand() {}


    // TBD:
    public static final String BRAND_UNKNOWN = "unknown";  // ???
    public static final String BRAND_FEEDSTOA = "feedstoa";
    public static final String BRAND_FEEDJOIN = "feedjoin";
    // ...
    
    
    // Brand key -> App display name
    private static final Map<String, String> sBrandMap = new HashMap<String, String>();
    static {
        sBrandMap.put(BRAND_FEEDSTOA, "FeedStoa");
        sBrandMap.put(BRAND_FEEDJOIN, "Feed Join");
    }
    
    
    // temporary
    public static String getDefaultBrand()
    {
        return BRAND_FEEDSTOA;
    }
    
    // temporary
    public static boolean isValidBrand(String brand)
    {
        return sBrandMap.containsKey(brand);
    }
    
    // temporary
    public static String getDisplayName(String brand)
    {
        if(isValidBrand(brand)) {
            return sBrandMap.get(brand);
        } else {
            // ????
            return "";
        }
    }

    // temporary
    public static String getBrandDescription(String brand)
    {
        if(BRAND_FEEDSTOA.equals(brand)) {
            return "FeedStoa, a feed helper Web service";
        } else if(BRAND_FEEDJOIN.equals(brand)) {
            return "Feed Join is an onlin feed aggregate application";
        } else {
            // ????
            return "";
        }
    }

}
