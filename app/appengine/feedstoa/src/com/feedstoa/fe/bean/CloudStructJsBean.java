package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudStructJsBean implements Serializable, Cloneable  //, CloudStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CloudStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String domain;
    private Integer port;
    private String path;
    private String registerProcedure;
    private String protocol;

    // Ctors.
    public CloudStructJsBean()
    {
        //this((String) null);
    }
    public CloudStructJsBean(String domain, Integer port, String path, String registerProcedure, String protocol)
    {
        this.domain = domain;
        this.port = port;
        this.path = path;
        this.registerProcedure = registerProcedure;
        this.protocol = protocol;
    }
    public CloudStructJsBean(CloudStructJsBean bean)
    {
        if(bean != null) {
            setDomain(bean.getDomain());
            setPort(bean.getPort());
            setPath(bean.getPath());
            setRegisterProcedure(bean.getRegisterProcedure());
            setProtocol(bean.getProtocol());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static CloudStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        CloudStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(CloudStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, CloudStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public Integer getPort()
    {
        return this.port;
    }
    public void setPort(Integer port)
    {
        this.port = port;
    }

    public String getPath()
    {
        return this.path;
    }
    public void setPath(String path)
    {
        this.path = path;
    }

    public String getRegisterProcedure()
    {
        return this.registerProcedure;
    }
    public void setRegisterProcedure(String registerProcedure)
    {
        this.registerProcedure = registerProcedure;
    }

    public String getProtocol()
    {
        return this.protocol;
    }
    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPort() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegisterProcedure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProtocol() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("domain:null, ");
        sb.append("port:0, ");
        sb.append("path:null, ");
        sb.append("registerProcedure:null, ");
        sb.append("protocol:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("domain:");
        if(this.getDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDomain()).append("\", ");
        }
        sb.append("port:" + this.getPort()).append(", ");
        sb.append("path:");
        if(this.getPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPath()).append("\", ");
        }
        sb.append("registerProcedure:");
        if(this.getRegisterProcedure() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRegisterProcedure()).append("\", ");
        }
        sb.append("protocol:");
        if(this.getProtocol() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getProtocol()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getDomain() != null) {
            sb.append("\"domain\":").append("\"").append(this.getDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"domain\":").append("null, ");
        }
        if(this.getPort() != null) {
            sb.append("\"port\":").append("").append(this.getPort()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"port\":").append("null, ");
        }
        if(this.getPath() != null) {
            sb.append("\"path\":").append("\"").append(this.getPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"path\":").append("null, ");
        }
        if(this.getRegisterProcedure() != null) {
            sb.append("\"registerProcedure\":").append("\"").append(this.getRegisterProcedure()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"registerProcedure\":").append("null, ");
        }
        if(this.getProtocol() != null) {
            sb.append("\"protocol\":").append("\"").append(this.getProtocol()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"protocol\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("domain = " + this.domain).append(";");
        sb.append("port = " + this.port).append(";");
        sb.append("path = " + this.path).append(";");
        sb.append("registerProcedure = " + this.registerProcedure).append(";");
        sb.append("protocol = " + this.protocol).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        CloudStructJsBean cloned = new CloudStructJsBean();
        cloned.setDomain(this.getDomain());   
        cloned.setPort(this.getPort());   
        cloned.setPath(this.getPath());   
        cloned.setRegisterProcedure(this.getRegisterProcedure());   
        cloned.setProtocol(this.getProtocol());   
        return cloned;
    }

}
