package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationStructJsBean implements Serializable, Cloneable  //, NotificationStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NotificationStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String preferredMode;
    private String mobileNumber;
    private String emailAddress;
    private String twitterUsername;
    private Long facebookId;
    private String linkedinId;
    private String note;

    // Ctors.
    public NotificationStructJsBean()
    {
        //this((String) null);
    }
    public NotificationStructJsBean(String preferredMode, String mobileNumber, String emailAddress, String twitterUsername, Long facebookId, String linkedinId, String note)
    {
        this.preferredMode = preferredMode;
        this.mobileNumber = mobileNumber;
        this.emailAddress = emailAddress;
        this.twitterUsername = twitterUsername;
        this.facebookId = facebookId;
        this.linkedinId = linkedinId;
        this.note = note;
    }
    public NotificationStructJsBean(NotificationStructJsBean bean)
    {
        if(bean != null) {
            setPreferredMode(bean.getPreferredMode());
            setMobileNumber(bean.getMobileNumber());
            setEmailAddress(bean.getEmailAddress());
            setTwitterUsername(bean.getTwitterUsername());
            setFacebookId(bean.getFacebookId());
            setLinkedinId(bean.getLinkedinId());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static NotificationStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        NotificationStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(NotificationStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, NotificationStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getPreferredMode()
    {
        return this.preferredMode;
    }
    public void setPreferredMode(String preferredMode)
    {
        this.preferredMode = preferredMode;
    }

    public String getMobileNumber()
    {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getTwitterUsername()
    {
        return this.twitterUsername;
    }
    public void setTwitterUsername(String twitterUsername)
    {
        this.twitterUsername = twitterUsername;
    }

    public Long getFacebookId()
    {
        return this.facebookId;
    }
    public void setFacebookId(Long facebookId)
    {
        this.facebookId = facebookId;
    }

    public String getLinkedinId()
    {
        return this.linkedinId;
    }
    public void setLinkedinId(String linkedinId)
    {
        this.linkedinId = linkedinId;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPreferredMode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMobileNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterUsername() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLinkedinId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("preferredMode:null, ");
        sb.append("mobileNumber:null, ");
        sb.append("emailAddress:null, ");
        sb.append("twitterUsername:null, ");
        sb.append("facebookId:0, ");
        sb.append("linkedinId:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("preferredMode:");
        if(this.getPreferredMode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPreferredMode()).append("\", ");
        }
        sb.append("mobileNumber:");
        if(this.getMobileNumber() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMobileNumber()).append("\", ");
        }
        sb.append("emailAddress:");
        if(this.getEmailAddress() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEmailAddress()).append("\", ");
        }
        sb.append("twitterUsername:");
        if(this.getTwitterUsername() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTwitterUsername()).append("\", ");
        }
        sb.append("facebookId:" + this.getFacebookId()).append(", ");
        sb.append("linkedinId:");
        if(this.getLinkedinId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLinkedinId()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getPreferredMode() != null) {
            sb.append("\"preferredMode\":").append("\"").append(this.getPreferredMode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"preferredMode\":").append("null, ");
        }
        if(this.getMobileNumber() != null) {
            sb.append("\"mobileNumber\":").append("\"").append(this.getMobileNumber()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"mobileNumber\":").append("null, ");
        }
        if(this.getEmailAddress() != null) {
            sb.append("\"emailAddress\":").append("\"").append(this.getEmailAddress()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"emailAddress\":").append("null, ");
        }
        if(this.getTwitterUsername() != null) {
            sb.append("\"twitterUsername\":").append("\"").append(this.getTwitterUsername()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"twitterUsername\":").append("null, ");
        }
        if(this.getFacebookId() != null) {
            sb.append("\"facebookId\":").append("").append(this.getFacebookId()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"facebookId\":").append("null, ");
        }
        if(this.getLinkedinId() != null) {
            sb.append("\"linkedinId\":").append("\"").append(this.getLinkedinId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"linkedinId\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("preferredMode = " + this.preferredMode).append(";");
        sb.append("mobileNumber = " + this.mobileNumber).append(";");
        sb.append("emailAddress = " + this.emailAddress).append(";");
        sb.append("twitterUsername = " + this.twitterUsername).append(";");
        sb.append("facebookId = " + this.facebookId).append(";");
        sb.append("linkedinId = " + this.linkedinId).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        NotificationStructJsBean cloned = new NotificationStructJsBean();
        cloned.setPreferredMode(this.getPreferredMode());   
        cloned.setMobileNumber(this.getMobileNumber());   
        cloned.setEmailAddress(this.getEmailAddress());   
        cloned.setTwitterUsername(this.getTwitterUsername());   
        cloned.setFacebookId(this.getFacebookId());   
        cloned.setLinkedinId(this.getLinkedinId());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
