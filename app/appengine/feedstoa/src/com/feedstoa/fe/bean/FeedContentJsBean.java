package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedContentJsBean implements Serializable, Cloneable  //, FeedContent
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedContentJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String channelSource;
    private String channelFeed;
    private String channelVersion;
    private String feedUrl;
    private String feedFormat;
    private String content;
    private String contentHash;
    private String status;
    private String note;
    private String lastBuildDate;
    private Long lastBuildTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FeedContentJsBean()
    {
        //this((String) null);
    }
    public FeedContentJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FeedContentJsBean(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime)
    {
        this(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime, null, null);
    }
    public FeedContentJsBean(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.channelSource = channelSource;
        this.channelFeed = channelFeed;
        this.channelVersion = channelVersion;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.content = content;
        this.contentHash = contentHash;
        this.status = status;
        this.note = note;
        this.lastBuildDate = lastBuildDate;
        this.lastBuildTime = lastBuildTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FeedContentJsBean(FeedContentJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setChannelSource(bean.getChannelSource());
            setChannelFeed(bean.getChannelFeed());
            setChannelVersion(bean.getChannelVersion());
            setFeedUrl(bean.getFeedUrl());
            setFeedFormat(bean.getFeedFormat());
            setContent(bean.getContent());
            setContentHash(bean.getContentHash());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setLastBuildDate(bean.getLastBuildDate());
            setLastBuildTime(bean.getLastBuildTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FeedContentJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FeedContentJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FeedContentJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FeedContentJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    public String getChannelVersion()
    {
        return this.channelVersion;
    }
    public void setChannelVersion(String channelVersion)
    {
        this.channelVersion = channelVersion;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContentHash()
    {
        return this.contentHash;
    }
    public void setContentHash(String contentHash)
    {
        this.contentHash = contentHash;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    public Long getLastBuildTime()
    {
        return this.lastBuildTime;
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        this.lastBuildTime = lastBuildTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("channelSource:null, ");
        sb.append("channelFeed:null, ");
        sb.append("channelVersion:null, ");
        sb.append("feedUrl:null, ");
        sb.append("feedFormat:null, ");
        sb.append("content:null, ");
        sb.append("contentHash:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("lastBuildDate:null, ");
        sb.append("lastBuildTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("channelSource:");
        if(this.getChannelSource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelSource()).append("\", ");
        }
        sb.append("channelFeed:");
        if(this.getChannelFeed() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelFeed()).append("\", ");
        }
        sb.append("channelVersion:");
        if(this.getChannelVersion() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelVersion()).append("\", ");
        }
        sb.append("feedUrl:");
        if(this.getFeedUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedUrl()).append("\", ");
        }
        sb.append("feedFormat:");
        if(this.getFeedFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedFormat()).append("\", ");
        }
        sb.append("content:");
        if(this.getContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContent()).append("\", ");
        }
        sb.append("contentHash:");
        if(this.getContentHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContentHash()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("lastBuildDate:");
        if(this.getLastBuildDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastBuildDate()).append("\", ");
        }
        sb.append("lastBuildTime:" + this.getLastBuildTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getChannelSource() != null) {
            sb.append("\"channelSource\":").append("\"").append(this.getChannelSource()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelSource\":").append("null, ");
        }
        if(this.getChannelFeed() != null) {
            sb.append("\"channelFeed\":").append("\"").append(this.getChannelFeed()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelFeed\":").append("null, ");
        }
        if(this.getChannelVersion() != null) {
            sb.append("\"channelVersion\":").append("\"").append(this.getChannelVersion()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelVersion\":").append("null, ");
        }
        if(this.getFeedUrl() != null) {
            sb.append("\"feedUrl\":").append("\"").append(this.getFeedUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedUrl\":").append("null, ");
        }
        if(this.getFeedFormat() != null) {
            sb.append("\"feedFormat\":").append("\"").append(this.getFeedFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedFormat\":").append("null, ");
        }
        if(this.getContent() != null) {
            sb.append("\"content\":").append("\"").append(this.getContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"content\":").append("null, ");
        }
        if(this.getContentHash() != null) {
            sb.append("\"contentHash\":").append("\"").append(this.getContentHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentHash\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getLastBuildDate() != null) {
            sb.append("\"lastBuildDate\":").append("\"").append(this.getLastBuildDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastBuildDate\":").append("null, ");
        }
        if(this.getLastBuildTime() != null) {
            sb.append("\"lastBuildTime\":").append("").append(this.getLastBuildTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastBuildTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("channelSource = " + this.channelSource).append(";");
        sb.append("channelFeed = " + this.channelFeed).append(";");
        sb.append("channelVersion = " + this.channelVersion).append(";");
        sb.append("feedUrl = " + this.feedUrl).append(";");
        sb.append("feedFormat = " + this.feedFormat).append(";");
        sb.append("content = " + this.content).append(";");
        sb.append("contentHash = " + this.contentHash).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("lastBuildDate = " + this.lastBuildDate).append(";");
        sb.append("lastBuildTime = " + this.lastBuildTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FeedContentJsBean cloned = new FeedContentJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setChannelSource(this.getChannelSource());   
        cloned.setChannelFeed(this.getChannelFeed());   
        cloned.setChannelVersion(this.getChannelVersion());   
        cloned.setFeedUrl(this.getFeedUrl());   
        cloned.setFeedFormat(this.getFeedFormat());   
        cloned.setContent(this.getContent());   
        cloned.setContentHash(this.getContentHash());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setLastBuildDate(this.getLastBuildDate());   
        cloned.setLastBuildTime(this.getLastBuildTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
