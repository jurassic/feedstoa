package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FetchBaseJsBean implements Serializable  //, FetchBase
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchBaseJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructJsBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String title;
    private String description;
    private String fetchUrl;
    private String feedUrl;
    private String channelFeed;
    private Boolean reuseChannel;
    private Integer maxItemCount;
    private String note;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FetchBaseJsBean()
    {
        //this((String) null);
    }
    public FetchBaseJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchBaseJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, null, null);
    }
    public FetchBaseJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.title = title;
        this.description = description;
        this.fetchUrl = fetchUrl;
        this.feedUrl = feedUrl;
        this.channelFeed = channelFeed;
        this.reuseChannel = reuseChannel;
        this.maxItemCount = maxItemCount;
        this.note = note;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FetchBaseJsBean(FetchBaseJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setManagerApp(bean.getManagerApp());
            setAppAcl(bean.getAppAcl());
            setGaeApp(bean.getGaeApp());
            setOwnerUser(bean.getOwnerUser());
            setUserAcl(bean.getUserAcl());
            setUser(bean.getUser());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setFetchUrl(bean.getFetchUrl());
            setFeedUrl(bean.getFeedUrl());
            setChannelFeed(bean.getChannelFeed());
            setReuseChannel(bean.isReuseChannel());
            setMaxItemCount(bean.getMaxItemCount());
            setNote(bean.getNote());
            setStatus(bean.getStatus());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FetchBaseJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FetchBaseJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FetchBaseJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FetchBaseJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getManagerApp()
    {
        return this.managerApp;
    }
    public void setManagerApp(String managerApp)
    {
        this.managerApp = managerApp;
    }

    public Long getAppAcl()
    {
        return this.appAcl;
    }
    public void setAppAcl(Long appAcl)
    {
        this.appAcl = appAcl;
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return this.gaeApp;
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        this.gaeApp = gaeApp;
    }

    public String getOwnerUser()
    {
        return this.ownerUser;
    }
    public void setOwnerUser(String ownerUser)
    {
        this.ownerUser = ownerUser;
    }

    public Long getUserAcl()
    {
        return this.userAcl;
    }
    public void setUserAcl(Long userAcl)
    {
        this.userAcl = userAcl;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    public Boolean isReuseChannel()
    {
        return this.reuseChannel;
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        this.reuseChannel = reuseChannel;
    }

    public Integer getMaxItemCount()
    {
        return this.maxItemCount;
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        this.maxItemCount = maxItemCount;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("ownerUser:null, ");
        sb.append("userAcl:0, ");
        sb.append("user:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("fetchUrl:null, ");
        sb.append("feedUrl:null, ");
        sb.append("channelFeed:null, ");
        sb.append("reuseChannel:false, ");
        sb.append("maxItemCount:0, ");
        sb.append("note:null, ");
        sb.append("status:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("ownerUser:");
        if(this.getOwnerUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwnerUser()).append("\", ");
        }
        sb.append("userAcl:" + this.getUserAcl()).append(", ");
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("fetchUrl:");
        if(this.getFetchUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchUrl()).append("\", ");
        }
        sb.append("feedUrl:");
        if(this.getFeedUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedUrl()).append("\", ");
        }
        sb.append("channelFeed:");
        if(this.getChannelFeed() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelFeed()).append("\", ");
        }
        sb.append("reuseChannel:" + this.isReuseChannel()).append(", ");
        sb.append("maxItemCount:" + this.getMaxItemCount()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getOwnerUser() != null) {
            sb.append("\"ownerUser\":").append("\"").append(this.getOwnerUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ownerUser\":").append("null, ");
        }
        if(this.getUserAcl() != null) {
            sb.append("\"userAcl\":").append("").append(this.getUserAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAcl\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getFetchUrl() != null) {
            sb.append("\"fetchUrl\":").append("\"").append(this.getFetchUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchUrl\":").append("null, ");
        }
        if(this.getFeedUrl() != null) {
            sb.append("\"feedUrl\":").append("\"").append(this.getFeedUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedUrl\":").append("null, ");
        }
        if(this.getChannelFeed() != null) {
            sb.append("\"channelFeed\":").append("\"").append(this.getChannelFeed()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelFeed\":").append("null, ");
        }
        if(this.isReuseChannel() != null) {
            sb.append("\"reuseChannel\":").append("").append(this.isReuseChannel()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"reuseChannel\":").append("null, ");
        }
        if(this.getMaxItemCount() != null) {
            sb.append("\"maxItemCount\":").append("").append(this.getMaxItemCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"maxItemCount\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("managerApp = " + this.managerApp).append(";");
        sb.append("appAcl = " + this.appAcl).append(";");
        sb.append("gaeApp = " + this.gaeApp).append(";");
        sb.append("ownerUser = " + this.ownerUser).append(";");
        sb.append("userAcl = " + this.userAcl).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("fetchUrl = " + this.fetchUrl).append(";");
        sb.append("feedUrl = " + this.feedUrl).append(";");
        sb.append("channelFeed = " + this.channelFeed).append(";");
        sb.append("reuseChannel = " + this.reuseChannel).append(";");
        sb.append("maxItemCount = " + this.maxItemCount).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
