package com.feedstoa.fe.bean.mock;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.fe.Validateable;
import com.feedstoa.fe.core.StringEscapeUtil;
import com.feedstoa.fe.bean.NotificationStructJsBean;
import com.feedstoa.fe.bean.GaeAppStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.FetchRequestJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchRequestMockBean extends FetchRequestJsBean implements Serializable, Cloneable, Validateable  //, FetchRequest
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestMockBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public FetchRequestMockBean()
    {
        super();
    }
    public FetchRequestMockBean(String guid)
    {
       super(guid);
    }
    public FetchRequestMockBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
    }
    public FetchRequestMockBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime, createdTime, modifiedTime);
    }
    public FetchRequestMockBean(FetchRequestJsBean bean)
    {
        super(bean);
    }

    public static FetchRequestMockBean fromJsonString(String jsonStr)
    {
        FetchRequestMockBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, FetchRequestMockBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getOriginFetch() == null) {
//            addError("originFetch", "originFetch is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputFormat() == null) {
//            addError("outputFormat", "outputFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFetchStatus() == null) {
//            addError("fetchStatus", "fetchStatus is null");
//            allOK = false;
//        }
//        // TBD
//        if(getResult() == null) {
//            addError("result", "result is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedCategory() == null) {
//            addError("feedCategory", "feedCategory is null");
//            allOK = false;
//        }
//        // TBD
//        if(isMultipleFeedEnabled() == null) {
//            addError("multipleFeedEnabled", "multipleFeedEnabled is null");
//            allOK = false;
//        }
//        // TBD
//        if(isDeferred() == null) {
//            addError("deferred", "deferred is null");
//            allOK = false;
//        }
//        // TBD
//        if(isAlert() == null) {
//            addError("alert", "alert is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNotificationPref() == null) {
//            addError("notificationPref", "notificationPref is null");
//            allOK = false;
//        } else {
//            NotificationStructJsBean notificationPref = getNotificationPref();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getReferrerInfo() == null) {
//            addError("referrerInfo", "referrerInfo is null");
//            allOK = false;
//        } else {
//            ReferrerInfoStructJsBean referrerInfo = getReferrerInfo();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getRefreshInterval() == null) {
//            addError("refreshInterval", "refreshInterval is null");
//            allOK = false;
//        }
//        // TBD
//        if(getRefreshExpressions() == null) {
//            addError("refreshExpressions", "refreshExpressions is null");
//            allOK = false;
//        }
//        // TBD
//        if(getRefreshTimeZone() == null) {
//            addError("refreshTimeZone", "refreshTimeZone is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCurrentRefreshCount() == null) {
//            addError("currentRefreshCount", "currentRefreshCount is null");
//            allOK = false;
//        }
//        // TBD
//        if(getMaxRefreshCount() == null) {
//            addError("maxRefreshCount", "maxRefreshCount is null");
//            allOK = false;
//        }
//        // TBD
//        if(getRefreshExpirationTime() == null) {
//            addError("refreshExpirationTime", "refreshExpirationTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNextRefreshTime() == null) {
//            addError("nextRefreshTime", "nextRefreshTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastUpdatedTime() == null) {
//            addError("lastUpdatedTime", "lastUpdatedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FetchRequestMockBean cloned = new FetchRequestMockBean((FetchRequestJsBean) super.clone());
        return cloned;
    }

}
