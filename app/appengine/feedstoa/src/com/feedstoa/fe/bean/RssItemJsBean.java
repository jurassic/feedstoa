package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RssItemJsBean implements Serializable, Cloneable  //, RssItem
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssItemJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String title;
    private String link;
    private String description;
    private String author;
    private List<CategoryStructJsBean> category;
    private String comments;
    private EnclosureStructJsBean enclosure;
    private String guid;
    private String pubDate;
    private UriStructJsBean source;

    // Ctors.
    public RssItemJsBean()
    {
        //this((String) null);
    }
    public RssItemJsBean(String title, String link, String description, String author, List<CategoryStructJsBean> category, String comments, EnclosureStructJsBean enclosure, String guid, String pubDate, UriStructJsBean source)
    {
        this.title = title;
        this.link = link;
        this.description = description;
        this.author = author;
        setCategory(getCategory());
        this.comments = comments;
        this.enclosure = enclosure;
        this.guid = guid;
        this.pubDate = pubDate;
        this.source = source;
    }
    public RssItemJsBean(RssItemJsBean bean)
    {
        if(bean != null) {
            setTitle(bean.getTitle());
            setLink(bean.getLink());
            setDescription(bean.getDescription());
            setAuthor(bean.getAuthor());
            setCategory(bean.getCategory());
            setComments(bean.getComments());
            setEnclosure(bean.getEnclosure());
            setGuid(bean.getGuid());
            setPubDate(bean.getPubDate());
            setSource(bean.getSource());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RssItemJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RssItemJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RssItemJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RssItemJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAuthor()
    {
        return this.author;
    }
    public void setAuthor(String author)
    {
        this.author = author;
    }

    public List<CategoryStructJsBean> getCategory()
    {  
        return this.category;
    }
    public void setCategory(List<CategoryStructJsBean> category)
    {
        this.category = category;
    }

    public String getComments()
    {
        return this.comments;
    }
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public EnclosureStructJsBean getEnclosure()
    {  
        return this.enclosure;
    }
    public void setEnclosure(EnclosureStructJsBean enclosure)
    {
        this.enclosure = enclosure;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public UriStructJsBean getSource()
    {  
        return this.source;
    }
    public void setSource(UriStructJsBean source)
    {
        this.source = source;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAuthor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getComments() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEnclosure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGuid() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("title:null, ");
        sb.append("link:null, ");
        sb.append("description:null, ");
        sb.append("author:null, ");
        sb.append("category:null, ");
        sb.append("comments:null, ");
        sb.append("enclosure:{}, ");
        sb.append("guid:null, ");
        sb.append("pubDate:null, ");
        sb.append("source:{}, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("link:");
        if(this.getLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLink()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("author:");
        if(this.getAuthor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAuthor()).append("\", ");
        }
        sb.append("category:");
        if(this.getCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCategory()).append("\", ");
        }
        sb.append("comments:");
        if(this.getComments() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getComments()).append("\", ");
        }
        sb.append("enclosure:");
        if(this.getEnclosure() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEnclosure()).append("\", ");
        }
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("pubDate:");
        if(this.getPubDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPubDate()).append("\", ");
        }
        sb.append("source:");
        if(this.getSource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getLink() != null) {
            sb.append("\"link\":").append("\"").append(this.getLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"link\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getAuthor() != null) {
            sb.append("\"author\":").append("\"").append(this.getAuthor()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"author\":").append("null, ");
        }
        if(this.getCategory() != null) {
            sb.append("\"category\":").append("\"").append(this.getCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"category\":").append("null, ");
        }
        if(this.getComments() != null) {
            sb.append("\"comments\":").append("\"").append(this.getComments()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"comments\":").append("null, ");
        }
        sb.append("\"enclosure\":").append(this.enclosure.toJsonString()).append(", ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getPubDate() != null) {
            sb.append("\"pubDate\":").append("\"").append(this.getPubDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pubDate\":").append("null, ");
        }
        sb.append("\"source\":").append(this.source.toJsonString()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("title = " + this.title).append(";");
        sb.append("link = " + this.link).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("author = " + this.author).append(";");
        sb.append("category = " + this.category).append(";");
        sb.append("comments = " + this.comments).append(";");
        sb.append("enclosure = " + this.enclosure).append(";");
        sb.append("guid = " + this.guid).append(";");
        sb.append("pubDate = " + this.pubDate).append(";");
        sb.append("source = " + this.source).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RssItemJsBean cloned = new RssItemJsBean();
        cloned.setTitle(this.getTitle());   
        cloned.setLink(this.getLink());   
        cloned.setDescription(this.getDescription());   
        cloned.setAuthor(this.getAuthor());   
        cloned.setCategory(this.getCategory());   
        cloned.setComments(this.getComments());   
        cloned.setEnclosure( (EnclosureStructJsBean) this.getEnclosure().clone() );
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setPubDate(this.getPubDate());   
        cloned.setSource( (UriStructJsBean) this.getSource().clone() );
        return cloned;
    }

}
