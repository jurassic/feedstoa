package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RssChannelJsBean implements Serializable, Cloneable  //, RssChannel
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssChannelJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String title;
    private String link;
    private String description;
    private String language;
    private String copyright;
    private String managingEditor;
    private String webMaster;
    private String pubDate;
    private String lastBuildDate;
    private List<CategoryStructJsBean> category;
    private String generator;
    private String docs;
    private CloudStructJsBean cloud;
    private Integer ttl;
    private ImageStructJsBean image;
    private String rating;
    private TextInputStructJsBean textInput;
    private List<Integer> skipHours;
    private List<String> skipDays;
    private List<RssItemJsBean> item;

    // Ctors.
    public RssChannelJsBean()
    {
        //this((String) null);
    }
    public RssChannelJsBean(String title, String link, String description, String language, String copyright, String managingEditor, String webMaster, String pubDate, String lastBuildDate, List<CategoryStructJsBean> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean image, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, List<RssItemJsBean> item)
    {
        this.title = title;
        this.link = link;
        this.description = description;
        this.language = language;
        this.copyright = copyright;
        this.managingEditor = managingEditor;
        this.webMaster = webMaster;
        this.pubDate = pubDate;
        this.lastBuildDate = lastBuildDate;
        setCategory(getCategory());
        this.generator = generator;
        this.docs = docs;
        this.cloud = cloud;
        this.ttl = ttl;
        this.image = image;
        this.rating = rating;
        this.textInput = textInput;
        this.skipHours = skipHours;
        this.skipDays = skipDays;
        setItem(getItem());
    }
    public RssChannelJsBean(RssChannelJsBean bean)
    {
        if(bean != null) {
            setTitle(bean.getTitle());
            setLink(bean.getLink());
            setDescription(bean.getDescription());
            setLanguage(bean.getLanguage());
            setCopyright(bean.getCopyright());
            setManagingEditor(bean.getManagingEditor());
            setWebMaster(bean.getWebMaster());
            setPubDate(bean.getPubDate());
            setLastBuildDate(bean.getLastBuildDate());
            setCategory(bean.getCategory());
            setGenerator(bean.getGenerator());
            setDocs(bean.getDocs());
            setCloud(bean.getCloud());
            setTtl(bean.getTtl());
            setImage(bean.getImage());
            setRating(bean.getRating());
            setTextInput(bean.getTextInput());
            setSkipHours(bean.getSkipHours());
            setSkipDays(bean.getSkipDays());
            setItem(bean.getItem());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RssChannelJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RssChannelJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RssChannelJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RssChannelJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getCopyright()
    {
        return this.copyright;
    }
    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    public String getManagingEditor()
    {
        return this.managingEditor;
    }
    public void setManagingEditor(String managingEditor)
    {
        this.managingEditor = managingEditor;
    }

    public String getWebMaster()
    {
        return this.webMaster;
    }
    public void setWebMaster(String webMaster)
    {
        this.webMaster = webMaster;
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    public List<CategoryStructJsBean> getCategory()
    {  
        return this.category;
    }
    public void setCategory(List<CategoryStructJsBean> category)
    {
        this.category = category;
    }

    public String getGenerator()
    {
        return this.generator;
    }
    public void setGenerator(String generator)
    {
        this.generator = generator;
    }

    public String getDocs()
    {
        return this.docs;
    }
    public void setDocs(String docs)
    {
        this.docs = docs;
    }

    public CloudStructJsBean getCloud()
    {  
        return this.cloud;
    }
    public void setCloud(CloudStructJsBean cloud)
    {
        this.cloud = cloud;
    }

    public Integer getTtl()
    {
        return this.ttl;
    }
    public void setTtl(Integer ttl)
    {
        this.ttl = ttl;
    }

    public ImageStructJsBean getImage()
    {  
        return this.image;
    }
    public void setImage(ImageStructJsBean image)
    {
        this.image = image;
    }

    public String getRating()
    {
        return this.rating;
    }
    public void setRating(String rating)
    {
        this.rating = rating;
    }

    public TextInputStructJsBean getTextInput()
    {  
        return this.textInput;
    }
    public void setTextInput(TextInputStructJsBean textInput)
    {
        this.textInput = textInput;
    }

    public List<Integer> getSkipHours()
    {
        return this.skipHours;
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        this.skipHours = skipHours;
    }

    public List<String> getSkipDays()
    {
        return this.skipDays;
    }
    public void setSkipDays(List<String> skipDays)
    {
        this.skipDays = skipDays;
    }

    public List<RssItemJsBean> getItem()
    {  
        return this.item;
    }
    public void setItem(List<RssItemJsBean> item)
    {
        this.item = item;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCopyright() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getManagingEditor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebMaster() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastBuildDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGenerator() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDocs() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCloud() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTtl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRating() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTextInput() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipHours() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipDays() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getItem() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("title:null, ");
        sb.append("link:null, ");
        sb.append("description:null, ");
        sb.append("language:null, ");
        sb.append("copyright:null, ");
        sb.append("managingEditor:null, ");
        sb.append("webMaster:null, ");
        sb.append("pubDate:null, ");
        sb.append("lastBuildDate:null, ");
        sb.append("category:null, ");
        sb.append("generator:null, ");
        sb.append("docs:null, ");
        sb.append("cloud:{}, ");
        sb.append("ttl:0, ");
        sb.append("image:{}, ");
        sb.append("rating:null, ");
        sb.append("textInput:{}, ");
        sb.append("skipHours:null, ");
        sb.append("skipDays:null, ");
        sb.append("item:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("link:");
        if(this.getLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLink()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("copyright:");
        if(this.getCopyright() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCopyright()).append("\", ");
        }
        sb.append("managingEditor:");
        if(this.getManagingEditor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagingEditor()).append("\", ");
        }
        sb.append("webMaster:");
        if(this.getWebMaster() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getWebMaster()).append("\", ");
        }
        sb.append("pubDate:");
        if(this.getPubDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPubDate()).append("\", ");
        }
        sb.append("lastBuildDate:");
        if(this.getLastBuildDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastBuildDate()).append("\", ");
        }
        sb.append("category:");
        if(this.getCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCategory()).append("\", ");
        }
        sb.append("generator:");
        if(this.getGenerator() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGenerator()).append("\", ");
        }
        sb.append("docs:");
        if(this.getDocs() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDocs()).append("\", ");
        }
        sb.append("cloud:");
        if(this.getCloud() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCloud()).append("\", ");
        }
        sb.append("ttl:" + this.getTtl()).append(", ");
        sb.append("image:");
        if(this.getImage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage()).append("\", ");
        }
        sb.append("rating:");
        if(this.getRating() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRating()).append("\", ");
        }
        sb.append("textInput:");
        if(this.getTextInput() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTextInput()).append("\", ");
        }
        sb.append("skipHours:");
        if(this.getSkipHours() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSkipHours()).append("\", ");
        }
        sb.append("skipDays:");
        if(this.getSkipDays() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSkipDays()).append("\", ");
        }
        sb.append("item:");
        if(this.getItem() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getItem()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getLink() != null) {
            sb.append("\"link\":").append("\"").append(this.getLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"link\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getCopyright() != null) {
            sb.append("\"copyright\":").append("\"").append(this.getCopyright()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"copyright\":").append("null, ");
        }
        if(this.getManagingEditor() != null) {
            sb.append("\"managingEditor\":").append("\"").append(this.getManagingEditor()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managingEditor\":").append("null, ");
        }
        if(this.getWebMaster() != null) {
            sb.append("\"webMaster\":").append("\"").append(this.getWebMaster()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"webMaster\":").append("null, ");
        }
        if(this.getPubDate() != null) {
            sb.append("\"pubDate\":").append("\"").append(this.getPubDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pubDate\":").append("null, ");
        }
        if(this.getLastBuildDate() != null) {
            sb.append("\"lastBuildDate\":").append("\"").append(this.getLastBuildDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastBuildDate\":").append("null, ");
        }
        if(this.getCategory() != null) {
            sb.append("\"category\":").append("\"").append(this.getCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"category\":").append("null, ");
        }
        if(this.getGenerator() != null) {
            sb.append("\"generator\":").append("\"").append(this.getGenerator()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"generator\":").append("null, ");
        }
        if(this.getDocs() != null) {
            sb.append("\"docs\":").append("\"").append(this.getDocs()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"docs\":").append("null, ");
        }
        sb.append("\"cloud\":").append(this.cloud.toJsonString()).append(", ");
        if(this.getTtl() != null) {
            sb.append("\"ttl\":").append("").append(this.getTtl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ttl\":").append("null, ");
        }
        sb.append("\"image\":").append(this.image.toJsonString()).append(", ");
        if(this.getRating() != null) {
            sb.append("\"rating\":").append("\"").append(this.getRating()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"rating\":").append("null, ");
        }
        sb.append("\"textInput\":").append(this.textInput.toJsonString()).append(", ");
        if(this.getSkipHours() != null) {
            sb.append("\"skipHours\":").append("\"").append(this.getSkipHours()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"skipHours\":").append("null, ");
        }
        if(this.getSkipDays() != null) {
            sb.append("\"skipDays\":").append("\"").append(this.getSkipDays()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"skipDays\":").append("null, ");
        }
        if(this.getItem() != null) {
            sb.append("\"item\":").append("\"").append(this.getItem()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"item\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("title = " + this.title).append(";");
        sb.append("link = " + this.link).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("language = " + this.language).append(";");
        sb.append("copyright = " + this.copyright).append(";");
        sb.append("managingEditor = " + this.managingEditor).append(";");
        sb.append("webMaster = " + this.webMaster).append(";");
        sb.append("pubDate = " + this.pubDate).append(";");
        sb.append("lastBuildDate = " + this.lastBuildDate).append(";");
        sb.append("category = " + this.category).append(";");
        sb.append("generator = " + this.generator).append(";");
        sb.append("docs = " + this.docs).append(";");
        sb.append("cloud = " + this.cloud).append(";");
        sb.append("ttl = " + this.ttl).append(";");
        sb.append("image = " + this.image).append(";");
        sb.append("rating = " + this.rating).append(";");
        sb.append("textInput = " + this.textInput).append(";");
        sb.append("skipHours = " + this.skipHours).append(";");
        sb.append("skipDays = " + this.skipDays).append(";");
        sb.append("item = " + this.item).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RssChannelJsBean cloned = new RssChannelJsBean();
        cloned.setTitle(this.getTitle());   
        cloned.setLink(this.getLink());   
        cloned.setDescription(this.getDescription());   
        cloned.setLanguage(this.getLanguage());   
        cloned.setCopyright(this.getCopyright());   
        cloned.setManagingEditor(this.getManagingEditor());   
        cloned.setWebMaster(this.getWebMaster());   
        cloned.setPubDate(this.getPubDate());   
        cloned.setLastBuildDate(this.getLastBuildDate());   
        cloned.setCategory(this.getCategory());   
        cloned.setGenerator(this.getGenerator());   
        cloned.setDocs(this.getDocs());   
        cloned.setCloud( (CloudStructJsBean) this.getCloud().clone() );
        cloned.setTtl(this.getTtl());   
        cloned.setImage( (ImageStructJsBean) this.getImage().clone() );
        cloned.setRating(this.getRating());   
        cloned.setTextInput( (TextInputStructJsBean) this.getTextInput().clone() );
        cloned.setSkipHours(this.getSkipHours());   
        cloned.setSkipDays(this.getSkipDays());   
        cloned.setItem(this.getItem());   
        return cloned;
    }

}
