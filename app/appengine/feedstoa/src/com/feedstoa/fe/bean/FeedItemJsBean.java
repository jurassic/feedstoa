package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedItemJsBean implements Serializable, Cloneable  //, FeedItem
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedItemJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String fetchRequest;
    private String fetchUrl;
    private String feedUrl;
    private String channelSource;
    private String channelFeed;
    private String feedFormat;
    private String title;
    private UriStructJsBean link;
    private String summary;
    private String content;
    private UserStructJsBean author;
    private List<UserStructJsBean> contributor;
    private List<CategoryStructJsBean> category;
    private String comments;
    private EnclosureStructJsBean enclosure;
    private String id;
    private String pubDate;
    private UriStructJsBean source;
    private String feedContent;
    private String status;
    private String note;
    private ReferrerInfoStructJsBean referrerInfo;
    private Long publishedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FeedItemJsBean()
    {
        //this((String) null);
    }
    public FeedItemJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FeedItemJsBean(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructJsBean link, String summary, String content, UserStructJsBean author, List<UserStructJsBean> contributor, List<CategoryStructJsBean> category, String comments, EnclosureStructJsBean enclosure, String id, String pubDate, UriStructJsBean source, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long publishedTime, Long lastUpdatedTime)
    {
        this(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime, null, null);
    }
    public FeedItemJsBean(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructJsBean link, String summary, String content, UserStructJsBean author, List<UserStructJsBean> contributor, List<CategoryStructJsBean> category, String comments, EnclosureStructJsBean enclosure, String id, String pubDate, UriStructJsBean source, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long publishedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.fetchRequest = fetchRequest;
        this.fetchUrl = fetchUrl;
        this.feedUrl = feedUrl;
        this.channelSource = channelSource;
        this.channelFeed = channelFeed;
        this.feedFormat = feedFormat;
        this.title = title;
        this.link = link;
        this.summary = summary;
        this.content = content;
        this.author = author;
        setContributor(getContributor());
        setCategory(getCategory());
        this.comments = comments;
        this.enclosure = enclosure;
        this.id = id;
        this.pubDate = pubDate;
        this.source = source;
        this.feedContent = feedContent;
        this.status = status;
        this.note = note;
        this.referrerInfo = referrerInfo;
        this.publishedTime = publishedTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FeedItemJsBean(FeedItemJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setFetchRequest(bean.getFetchRequest());
            setFetchUrl(bean.getFetchUrl());
            setFeedUrl(bean.getFeedUrl());
            setChannelSource(bean.getChannelSource());
            setChannelFeed(bean.getChannelFeed());
            setFeedFormat(bean.getFeedFormat());
            setTitle(bean.getTitle());
            setLink(bean.getLink());
            setSummary(bean.getSummary());
            setContent(bean.getContent());
            setAuthor(bean.getAuthor());
            setContributor(bean.getContributor());
            setCategory(bean.getCategory());
            setComments(bean.getComments());
            setEnclosure(bean.getEnclosure());
            setId(bean.getId());
            setPubDate(bean.getPubDate());
            setSource(bean.getSource());
            setFeedContent(bean.getFeedContent());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setReferrerInfo(bean.getReferrerInfo());
            setPublishedTime(bean.getPublishedTime());
            setLastUpdatedTime(bean.getLastUpdatedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FeedItemJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FeedItemJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FeedItemJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FeedItemJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public UriStructJsBean getLink()
    {  
        return this.link;
    }
    public void setLink(UriStructJsBean link)
    {
        this.link = link;
    }

    public String getSummary()
    {
        return this.summary;
    }
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public UserStructJsBean getAuthor()
    {  
        return this.author;
    }
    public void setAuthor(UserStructJsBean author)
    {
        this.author = author;
    }

    public List<UserStructJsBean> getContributor()
    {  
        return this.contributor;
    }
    public void setContributor(List<UserStructJsBean> contributor)
    {
        this.contributor = contributor;
    }

    public List<CategoryStructJsBean> getCategory()
    {  
        return this.category;
    }
    public void setCategory(List<CategoryStructJsBean> category)
    {
        this.category = category;
    }

    public String getComments()
    {
        return this.comments;
    }
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public EnclosureStructJsBean getEnclosure()
    {  
        return this.enclosure;
    }
    public void setEnclosure(EnclosureStructJsBean enclosure)
    {
        this.enclosure = enclosure;
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public UriStructJsBean getSource()
    {  
        return this.source;
    }
    public void setSource(UriStructJsBean source)
    {
        this.source = source;
    }

    public String getFeedContent()
    {
        return this.feedContent;
    }
    public void setFeedContent(String feedContent)
    {
        this.feedContent = feedContent;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public Long getPublishedTime()
    {
        return this.publishedTime;
    }
    public void setPublishedTime(Long publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("fetchRequest:null, ");
        sb.append("fetchUrl:null, ");
        sb.append("feedUrl:null, ");
        sb.append("channelSource:null, ");
        sb.append("channelFeed:null, ");
        sb.append("feedFormat:null, ");
        sb.append("title:null, ");
        sb.append("link:{}, ");
        sb.append("summary:null, ");
        sb.append("content:null, ");
        sb.append("author:{}, ");
        sb.append("contributor:null, ");
        sb.append("category:null, ");
        sb.append("comments:null, ");
        sb.append("enclosure:{}, ");
        sb.append("id:null, ");
        sb.append("pubDate:null, ");
        sb.append("source:{}, ");
        sb.append("feedContent:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("referrerInfo:{}, ");
        sb.append("publishedTime:0, ");
        sb.append("lastUpdatedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("fetchRequest:");
        if(this.getFetchRequest() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchRequest()).append("\", ");
        }
        sb.append("fetchUrl:");
        if(this.getFetchUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchUrl()).append("\", ");
        }
        sb.append("feedUrl:");
        if(this.getFeedUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedUrl()).append("\", ");
        }
        sb.append("channelSource:");
        if(this.getChannelSource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelSource()).append("\", ");
        }
        sb.append("channelFeed:");
        if(this.getChannelFeed() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelFeed()).append("\", ");
        }
        sb.append("feedFormat:");
        if(this.getFeedFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedFormat()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("link:");
        if(this.getLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLink()).append("\", ");
        }
        sb.append("summary:");
        if(this.getSummary() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSummary()).append("\", ");
        }
        sb.append("content:");
        if(this.getContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContent()).append("\", ");
        }
        sb.append("author:");
        if(this.getAuthor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAuthor()).append("\", ");
        }
        sb.append("contributor:");
        if(this.getContributor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContributor()).append("\", ");
        }
        sb.append("category:");
        if(this.getCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCategory()).append("\", ");
        }
        sb.append("comments:");
        if(this.getComments() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getComments()).append("\", ");
        }
        sb.append("enclosure:");
        if(this.getEnclosure() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getEnclosure()).append("\", ");
        }
        sb.append("id:");
        if(this.getId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getId()).append("\", ");
        }
        sb.append("pubDate:");
        if(this.getPubDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPubDate()).append("\", ");
        }
        sb.append("source:");
        if(this.getSource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource()).append("\", ");
        }
        sb.append("feedContent:");
        if(this.getFeedContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedContent()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("publishedTime:" + this.getPublishedTime()).append(", ");
        sb.append("lastUpdatedTime:" + this.getLastUpdatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getFetchRequest() != null) {
            sb.append("\"fetchRequest\":").append("\"").append(this.getFetchRequest()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchRequest\":").append("null, ");
        }
        if(this.getFetchUrl() != null) {
            sb.append("\"fetchUrl\":").append("\"").append(this.getFetchUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchUrl\":").append("null, ");
        }
        if(this.getFeedUrl() != null) {
            sb.append("\"feedUrl\":").append("\"").append(this.getFeedUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedUrl\":").append("null, ");
        }
        if(this.getChannelSource() != null) {
            sb.append("\"channelSource\":").append("\"").append(this.getChannelSource()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelSource\":").append("null, ");
        }
        if(this.getChannelFeed() != null) {
            sb.append("\"channelFeed\":").append("\"").append(this.getChannelFeed()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelFeed\":").append("null, ");
        }
        if(this.getFeedFormat() != null) {
            sb.append("\"feedFormat\":").append("\"").append(this.getFeedFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedFormat\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        sb.append("\"link\":").append(this.link.toJsonString()).append(", ");
        if(this.getSummary() != null) {
            sb.append("\"summary\":").append("\"").append(this.getSummary()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"summary\":").append("null, ");
        }
        if(this.getContent() != null) {
            sb.append("\"content\":").append("\"").append(this.getContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"content\":").append("null, ");
        }
        sb.append("\"author\":").append(this.author.toJsonString()).append(", ");
        if(this.getContributor() != null) {
            sb.append("\"contributor\":").append("\"").append(this.getContributor()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contributor\":").append("null, ");
        }
        if(this.getCategory() != null) {
            sb.append("\"category\":").append("\"").append(this.getCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"category\":").append("null, ");
        }
        if(this.getComments() != null) {
            sb.append("\"comments\":").append("\"").append(this.getComments()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"comments\":").append("null, ");
        }
        sb.append("\"enclosure\":").append(this.enclosure.toJsonString()).append(", ");
        if(this.getId() != null) {
            sb.append("\"id\":").append("\"").append(this.getId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"id\":").append("null, ");
        }
        if(this.getPubDate() != null) {
            sb.append("\"pubDate\":").append("\"").append(this.getPubDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pubDate\":").append("null, ");
        }
        sb.append("\"source\":").append(this.source.toJsonString()).append(", ");
        if(this.getFeedContent() != null) {
            sb.append("\"feedContent\":").append("\"").append(this.getFeedContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedContent\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getPublishedTime() != null) {
            sb.append("\"publishedTime\":").append("").append(this.getPublishedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"publishedTime\":").append("null, ");
        }
        if(this.getLastUpdatedTime() != null) {
            sb.append("\"lastUpdatedTime\":").append("").append(this.getLastUpdatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastUpdatedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("fetchRequest = " + this.fetchRequest).append(";");
        sb.append("fetchUrl = " + this.fetchUrl).append(";");
        sb.append("feedUrl = " + this.feedUrl).append(";");
        sb.append("channelSource = " + this.channelSource).append(";");
        sb.append("channelFeed = " + this.channelFeed).append(";");
        sb.append("feedFormat = " + this.feedFormat).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("link = " + this.link).append(";");
        sb.append("summary = " + this.summary).append(";");
        sb.append("content = " + this.content).append(";");
        sb.append("author = " + this.author).append(";");
        sb.append("contributor = " + this.contributor).append(";");
        sb.append("category = " + this.category).append(";");
        sb.append("comments = " + this.comments).append(";");
        sb.append("enclosure = " + this.enclosure).append(";");
        sb.append("id = " + this.id).append(";");
        sb.append("pubDate = " + this.pubDate).append(";");
        sb.append("source = " + this.source).append(";");
        sb.append("feedContent = " + this.feedContent).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("publishedTime = " + this.publishedTime).append(";");
        sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FeedItemJsBean cloned = new FeedItemJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setFetchRequest(this.getFetchRequest());   
        cloned.setFetchUrl(this.getFetchUrl());   
        cloned.setFeedUrl(this.getFeedUrl());   
        cloned.setChannelSource(this.getChannelSource());   
        cloned.setChannelFeed(this.getChannelFeed());   
        cloned.setFeedFormat(this.getFeedFormat());   
        cloned.setTitle(this.getTitle());   
        cloned.setLink( (UriStructJsBean) this.getLink().clone() );
        cloned.setSummary(this.getSummary());   
        cloned.setContent(this.getContent());   
        cloned.setAuthor( (UserStructJsBean) this.getAuthor().clone() );
        cloned.setContributor(this.getContributor());   
        cloned.setCategory(this.getCategory());   
        cloned.setComments(this.getComments());   
        cloned.setEnclosure( (EnclosureStructJsBean) this.getEnclosure().clone() );
        cloned.setId(this.getId());   
        cloned.setPubDate(this.getPubDate());   
        cloned.setSource( (UriStructJsBean) this.getSource().clone() );
        cloned.setFeedContent(this.getFeedContent());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setPublishedTime(this.getPublishedTime());   
        cloned.setLastUpdatedTime(this.getLastUpdatedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
