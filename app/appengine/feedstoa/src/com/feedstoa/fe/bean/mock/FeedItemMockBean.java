package com.feedstoa.fe.bean.mock;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.fe.Validateable;
import com.feedstoa.fe.core.StringEscapeUtil;
import com.feedstoa.fe.bean.EnclosureStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.FeedItemJsBean;


// Place holder...
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedItemMockBean extends FeedItemJsBean implements Serializable, Cloneable, Validateable  //, FeedItem
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedItemMockBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public FeedItemMockBean()
    {
        super();
    }
    public FeedItemMockBean(String guid)
    {
       super(guid);
    }
    public FeedItemMockBean(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructJsBean link, String summary, String content, UserStructJsBean author, List<UserStructJsBean> contributor, List<CategoryStructJsBean> category, String comments, EnclosureStructJsBean enclosure, String id, String pubDate, UriStructJsBean source, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long publishedTime, Long lastUpdatedTime)
    {
        super(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime);
    }
    public FeedItemMockBean(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructJsBean link, String summary, String content, UserStructJsBean author, List<UserStructJsBean> contributor, List<CategoryStructJsBean> category, String comments, EnclosureStructJsBean enclosure, String id, String pubDate, UriStructJsBean source, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long publishedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime, createdTime, modifiedTime);
    }
    public FeedItemMockBean(FeedItemJsBean bean)
    {
        super(bean);
    }

    public static FeedItemMockBean fromJsonString(String jsonStr)
    {
        FeedItemMockBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, FeedItemMockBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFetchRequest() == null) {
//            addError("fetchRequest", "fetchRequest is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFetchUrl() == null) {
//            addError("fetchUrl", "fetchUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedUrl() == null) {
//            addError("feedUrl", "feedUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getChannelSource() == null) {
//            addError("channelSource", "channelSource is null");
//            allOK = false;
//        }
//        // TBD
//        if(getChannelFeed() == null) {
//            addError("channelFeed", "channelFeed is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedFormat() == null) {
//            addError("feedFormat", "feedFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTitle() == null) {
//            addError("title", "title is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLink() == null) {
//            addError("link", "link is null");
//            allOK = false;
//        } else {
//            UriStructJsBean link = getLink();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getSummary() == null) {
//            addError("summary", "summary is null");
//            allOK = false;
//        }
//        // TBD
//        if(getContent() == null) {
//            addError("content", "content is null");
//            allOK = false;
//        }
//        // TBD
//        if(getAuthor() == null) {
//            addError("author", "author is null");
//            allOK = false;
//        } else {
//            UserStructJsBean author = getAuthor();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getContributor() == null) {
//            addError("contributor", "contributor is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCategory() == null) {
//            addError("category", "category is null");
//            allOK = false;
//        }
//        // TBD
//        if(getComments() == null) {
//            addError("comments", "comments is null");
//            allOK = false;
//        }
//        // TBD
//        if(getEnclosure() == null) {
//            addError("enclosure", "enclosure is null");
//            allOK = false;
//        } else {
//            EnclosureStructJsBean enclosure = getEnclosure();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getId() == null) {
//            addError("id", "id is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPubDate() == null) {
//            addError("pubDate", "pubDate is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSource() == null) {
//            addError("source", "source is null");
//            allOK = false;
//        } else {
//            UriStructJsBean source = getSource();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getFeedContent() == null) {
//            addError("feedContent", "feedContent is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReferrerInfo() == null) {
//            addError("referrerInfo", "referrerInfo is null");
//            allOK = false;
//        } else {
//            ReferrerInfoStructJsBean referrerInfo = getReferrerInfo();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getPublishedTime() == null) {
//            addError("publishedTime", "publishedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastUpdatedTime() == null) {
//            addError("lastUpdatedTime", "lastUpdatedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FeedItemMockBean cloned = new FeedItemMockBean((FeedItemJsBean) super.clone());
        return cloned;
    }

}
