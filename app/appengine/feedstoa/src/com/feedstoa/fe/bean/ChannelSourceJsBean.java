package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelSourceJsBean implements Serializable, Cloneable  //, ChannelSource
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelSourceJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String serviceName;
    private String serviceUrl;
    private String feedUrl;
    private String feedFormat;
    private String channelTitle;
    private String channelDescription;
    private String channelCategory;
    private String channelUrl;
    private String channelLogo;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ChannelSourceJsBean()
    {
        //this((String) null);
    }
    public ChannelSourceJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ChannelSourceJsBean(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note)
    {
        this(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note, null, null);
    }
    public ChannelSourceJsBean(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.channelTitle = channelTitle;
        this.channelDescription = channelDescription;
        this.channelCategory = channelCategory;
        this.channelUrl = channelUrl;
        this.channelLogo = channelLogo;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ChannelSourceJsBean(ChannelSourceJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setServiceName(bean.getServiceName());
            setServiceUrl(bean.getServiceUrl());
            setFeedUrl(bean.getFeedUrl());
            setFeedFormat(bean.getFeedFormat());
            setChannelTitle(bean.getChannelTitle());
            setChannelDescription(bean.getChannelDescription());
            setChannelCategory(bean.getChannelCategory());
            setChannelUrl(bean.getChannelUrl());
            setChannelLogo(bean.getChannelLogo());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ChannelSourceJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ChannelSourceJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ChannelSourceJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ChannelSourceJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public String getChannelTitle()
    {
        return this.channelTitle;
    }
    public void setChannelTitle(String channelTitle)
    {
        this.channelTitle = channelTitle;
    }

    public String getChannelDescription()
    {
        return this.channelDescription;
    }
    public void setChannelDescription(String channelDescription)
    {
        this.channelDescription = channelDescription;
    }

    public String getChannelCategory()
    {
        return this.channelCategory;
    }
    public void setChannelCategory(String channelCategory)
    {
        this.channelCategory = channelCategory;
    }

    public String getChannelUrl()
    {
        return this.channelUrl;
    }
    public void setChannelUrl(String channelUrl)
    {
        this.channelUrl = channelUrl;
    }

    public String getChannelLogo()
    {
        return this.channelLogo;
    }
    public void setChannelLogo(String channelLogo)
    {
        this.channelLogo = channelLogo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("serviceName:null, ");
        sb.append("serviceUrl:null, ");
        sb.append("feedUrl:null, ");
        sb.append("feedFormat:null, ");
        sb.append("channelTitle:null, ");
        sb.append("channelDescription:null, ");
        sb.append("channelCategory:null, ");
        sb.append("channelUrl:null, ");
        sb.append("channelLogo:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("serviceName:");
        if(this.getServiceName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceName()).append("\", ");
        }
        sb.append("serviceUrl:");
        if(this.getServiceUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getServiceUrl()).append("\", ");
        }
        sb.append("feedUrl:");
        if(this.getFeedUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedUrl()).append("\", ");
        }
        sb.append("feedFormat:");
        if(this.getFeedFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedFormat()).append("\", ");
        }
        sb.append("channelTitle:");
        if(this.getChannelTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelTitle()).append("\", ");
        }
        sb.append("channelDescription:");
        if(this.getChannelDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelDescription()).append("\", ");
        }
        sb.append("channelCategory:");
        if(this.getChannelCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelCategory()).append("\", ");
        }
        sb.append("channelUrl:");
        if(this.getChannelUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelUrl()).append("\", ");
        }
        sb.append("channelLogo:");
        if(this.getChannelLogo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelLogo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getServiceName() != null) {
            sb.append("\"serviceName\":").append("\"").append(this.getServiceName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceName\":").append("null, ");
        }
        if(this.getServiceUrl() != null) {
            sb.append("\"serviceUrl\":").append("\"").append(this.getServiceUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"serviceUrl\":").append("null, ");
        }
        if(this.getFeedUrl() != null) {
            sb.append("\"feedUrl\":").append("\"").append(this.getFeedUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedUrl\":").append("null, ");
        }
        if(this.getFeedFormat() != null) {
            sb.append("\"feedFormat\":").append("\"").append(this.getFeedFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedFormat\":").append("null, ");
        }
        if(this.getChannelTitle() != null) {
            sb.append("\"channelTitle\":").append("\"").append(this.getChannelTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelTitle\":").append("null, ");
        }
        if(this.getChannelDescription() != null) {
            sb.append("\"channelDescription\":").append("\"").append(this.getChannelDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelDescription\":").append("null, ");
        }
        if(this.getChannelCategory() != null) {
            sb.append("\"channelCategory\":").append("\"").append(this.getChannelCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelCategory\":").append("null, ");
        }
        if(this.getChannelUrl() != null) {
            sb.append("\"channelUrl\":").append("\"").append(this.getChannelUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelUrl\":").append("null, ");
        }
        if(this.getChannelLogo() != null) {
            sb.append("\"channelLogo\":").append("\"").append(this.getChannelLogo()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelLogo\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("serviceName = " + this.serviceName).append(";");
        sb.append("serviceUrl = " + this.serviceUrl).append(";");
        sb.append("feedUrl = " + this.feedUrl).append(";");
        sb.append("feedFormat = " + this.feedFormat).append(";");
        sb.append("channelTitle = " + this.channelTitle).append(";");
        sb.append("channelDescription = " + this.channelDescription).append(";");
        sb.append("channelCategory = " + this.channelCategory).append(";");
        sb.append("channelUrl = " + this.channelUrl).append(";");
        sb.append("channelLogo = " + this.channelLogo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ChannelSourceJsBean cloned = new ChannelSourceJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setServiceName(this.getServiceName());   
        cloned.setServiceUrl(this.getServiceUrl());   
        cloned.setFeedUrl(this.getFeedUrl());   
        cloned.setFeedFormat(this.getFeedFormat());   
        cloned.setChannelTitle(this.getChannelTitle());   
        cloned.setChannelDescription(this.getChannelDescription());   
        cloned.setChannelCategory(this.getChannelCategory());   
        cloned.setChannelUrl(this.getChannelUrl());   
        cloned.setChannelLogo(this.getChannelLogo());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
