package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchRequestJsBean extends FetchBaseJsBean implements Serializable, Cloneable  //, FetchRequest
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String originFetch;
    private String outputFormat;
    private Integer fetchStatus;
    private String result;
    private String feedCategory;
    private Boolean multipleFeedEnabled;
    private Boolean deferred;
    private Boolean alert;
    private NotificationStructJsBean notificationPref;
    private ReferrerInfoStructJsBean referrerInfo;
    private Integer refreshInterval;
    private List<String> refreshExpressions;
    private String refreshTimeZone;
    private Integer currentRefreshCount;
    private Integer maxRefreshCount;
    private Long refreshExpirationTime;
    private Long nextRefreshTime;
    private Long lastUpdatedTime;

    // Ctors.
    public FetchRequestJsBean()
    {
        //this((String) null);
    }
    public FetchRequestJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchRequestJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime, null, null);
    }
    public FetchRequestJsBean(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, createdTime, modifiedTime);

        this.originFetch = originFetch;
        this.outputFormat = outputFormat;
        this.fetchStatus = fetchStatus;
        this.result = result;
        this.feedCategory = feedCategory;
        this.multipleFeedEnabled = multipleFeedEnabled;
        this.deferred = deferred;
        this.alert = alert;
        this.notificationPref = notificationPref;
        this.referrerInfo = referrerInfo;
        this.refreshInterval = refreshInterval;
        this.refreshExpressions = refreshExpressions;
        this.refreshTimeZone = refreshTimeZone;
        this.currentRefreshCount = currentRefreshCount;
        this.maxRefreshCount = maxRefreshCount;
        this.refreshExpirationTime = refreshExpirationTime;
        this.nextRefreshTime = nextRefreshTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }
    public FetchRequestJsBean(FetchRequestJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setManagerApp(bean.getManagerApp());
            setAppAcl(bean.getAppAcl());
            setGaeApp(bean.getGaeApp());
            setOwnerUser(bean.getOwnerUser());
            setUserAcl(bean.getUserAcl());
            setUser(bean.getUser());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setFetchUrl(bean.getFetchUrl());
            setFeedUrl(bean.getFeedUrl());
            setChannelFeed(bean.getChannelFeed());
            setReuseChannel(bean.isReuseChannel());
            setMaxItemCount(bean.getMaxItemCount());
            setNote(bean.getNote());
            setStatus(bean.getStatus());
            setOriginFetch(bean.getOriginFetch());
            setOutputFormat(bean.getOutputFormat());
            setFetchStatus(bean.getFetchStatus());
            setResult(bean.getResult());
            setFeedCategory(bean.getFeedCategory());
            setMultipleFeedEnabled(bean.isMultipleFeedEnabled());
            setDeferred(bean.isDeferred());
            setAlert(bean.isAlert());
            setNotificationPref(bean.getNotificationPref());
            setReferrerInfo(bean.getReferrerInfo());
            setRefreshInterval(bean.getRefreshInterval());
            setRefreshExpressions(bean.getRefreshExpressions());
            setRefreshTimeZone(bean.getRefreshTimeZone());
            setCurrentRefreshCount(bean.getCurrentRefreshCount());
            setMaxRefreshCount(bean.getMaxRefreshCount());
            setRefreshExpirationTime(bean.getRefreshExpirationTime());
            setNextRefreshTime(bean.getNextRefreshTime());
            setLastUpdatedTime(bean.getLastUpdatedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FetchRequestJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FetchRequestJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FetchRequestJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FetchRequestJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getManagerApp()
    {
        return super.getManagerApp();
    }
    public void setManagerApp(String managerApp)
    {
        super.setManagerApp(managerApp);
    }

    public Long getAppAcl()
    {
        return super.getAppAcl();
    }
    public void setAppAcl(Long appAcl)
    {
        super.setAppAcl(appAcl);
    }

    public GaeAppStructJsBean getGaeApp()
    {  
        return super.getGaeApp();
    }
    public void setGaeApp(GaeAppStructJsBean gaeApp)
    {
        super.setGaeApp(gaeApp);
    }

    public String getOwnerUser()
    {
        return super.getOwnerUser();
    }
    public void setOwnerUser(String ownerUser)
    {
        super.setOwnerUser(ownerUser);
    }

    public Long getUserAcl()
    {
        return super.getUserAcl();
    }
    public void setUserAcl(Long userAcl)
    {
        super.setUserAcl(userAcl);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getFetchUrl()
    {
        return super.getFetchUrl();
    }
    public void setFetchUrl(String fetchUrl)
    {
        super.setFetchUrl(fetchUrl);
    }

    public String getFeedUrl()
    {
        return super.getFeedUrl();
    }
    public void setFeedUrl(String feedUrl)
    {
        super.setFeedUrl(feedUrl);
    }

    public String getChannelFeed()
    {
        return super.getChannelFeed();
    }
    public void setChannelFeed(String channelFeed)
    {
        super.setChannelFeed(channelFeed);
    }

    public Boolean isReuseChannel()
    {
        return super.isReuseChannel();
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        super.setReuseChannel(reuseChannel);
    }

    public Integer getMaxItemCount()
    {
        return super.getMaxItemCount();
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        super.setMaxItemCount(maxItemCount);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getOriginFetch()
    {
        return this.originFetch;
    }
    public void setOriginFetch(String originFetch)
    {
        this.originFetch = originFetch;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public Integer getFetchStatus()
    {
        return this.fetchStatus;
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        this.fetchStatus = fetchStatus;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getFeedCategory()
    {
        return this.feedCategory;
    }
    public void setFeedCategory(String feedCategory)
    {
        this.feedCategory = feedCategory;
    }

    public Boolean isMultipleFeedEnabled()
    {
        return this.multipleFeedEnabled;
    }
    public void setMultipleFeedEnabled(Boolean multipleFeedEnabled)
    {
        this.multipleFeedEnabled = multipleFeedEnabled;
    }

    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    public NotificationStructJsBean getNotificationPref()
    {  
        return this.notificationPref;
    }
    public void setNotificationPref(NotificationStructJsBean notificationPref)
    {
        this.notificationPref = notificationPref;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public Integer getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    public List<String> getRefreshExpressions()
    {
        return this.refreshExpressions;
    }
    public void setRefreshExpressions(List<String> refreshExpressions)
    {
        this.refreshExpressions = refreshExpressions;
    }

    public String getRefreshTimeZone()
    {
        return this.refreshTimeZone;
    }
    public void setRefreshTimeZone(String refreshTimeZone)
    {
        this.refreshTimeZone = refreshTimeZone;
    }

    public Integer getCurrentRefreshCount()
    {
        return this.currentRefreshCount;
    }
    public void setCurrentRefreshCount(Integer currentRefreshCount)
    {
        this.currentRefreshCount = currentRefreshCount;
    }

    public Integer getMaxRefreshCount()
    {
        return this.maxRefreshCount;
    }
    public void setMaxRefreshCount(Integer maxRefreshCount)
    {
        this.maxRefreshCount = maxRefreshCount;
    }

    public Long getRefreshExpirationTime()
    {
        return this.refreshExpirationTime;
    }
    public void setRefreshExpirationTime(Long refreshExpirationTime)
    {
        this.refreshExpirationTime = refreshExpirationTime;
    }

    public Long getNextRefreshTime()
    {
        return this.nextRefreshTime;
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        this.nextRefreshTime = nextRefreshTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("managerApp:null, ");
        sb.append("appAcl:0, ");
        sb.append("gaeApp:{}, ");
        sb.append("ownerUser:null, ");
        sb.append("userAcl:0, ");
        sb.append("user:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("fetchUrl:null, ");
        sb.append("feedUrl:null, ");
        sb.append("channelFeed:null, ");
        sb.append("reuseChannel:false, ");
        sb.append("maxItemCount:0, ");
        sb.append("note:null, ");
        sb.append("status:null, ");
        sb.append("originFetch:null, ");
        sb.append("outputFormat:null, ");
        sb.append("fetchStatus:0, ");
        sb.append("result:null, ");
        sb.append("feedCategory:null, ");
        sb.append("multipleFeedEnabled:false, ");
        sb.append("deferred:false, ");
        sb.append("alert:false, ");
        sb.append("notificationPref:{}, ");
        sb.append("referrerInfo:{}, ");
        sb.append("refreshInterval:0, ");
        sb.append("refreshExpressions:null, ");
        sb.append("refreshTimeZone:null, ");
        sb.append("currentRefreshCount:0, ");
        sb.append("maxRefreshCount:0, ");
        sb.append("refreshExpirationTime:0, ");
        sb.append("nextRefreshTime:0, ");
        sb.append("lastUpdatedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("managerApp:");
        if(this.getManagerApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagerApp()).append("\", ");
        }
        sb.append("appAcl:" + this.getAppAcl()).append(", ");
        sb.append("gaeApp:");
        if(this.getGaeApp() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGaeApp()).append("\", ");
        }
        sb.append("ownerUser:");
        if(this.getOwnerUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOwnerUser()).append("\", ");
        }
        sb.append("userAcl:" + this.getUserAcl()).append(", ");
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("fetchUrl:");
        if(this.getFetchUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchUrl()).append("\", ");
        }
        sb.append("feedUrl:");
        if(this.getFeedUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedUrl()).append("\", ");
        }
        sb.append("channelFeed:");
        if(this.getChannelFeed() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelFeed()).append("\", ");
        }
        sb.append("reuseChannel:" + this.isReuseChannel()).append(", ");
        sb.append("maxItemCount:" + this.getMaxItemCount()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("originFetch:");
        if(this.getOriginFetch() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOriginFetch()).append("\", ");
        }
        sb.append("outputFormat:");
        if(this.getOutputFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputFormat()).append("\", ");
        }
        sb.append("fetchStatus:" + this.getFetchStatus()).append(", ");
        sb.append("result:");
        if(this.getResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResult()).append("\", ");
        }
        sb.append("feedCategory:");
        if(this.getFeedCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedCategory()).append("\", ");
        }
        sb.append("multipleFeedEnabled:" + this.isMultipleFeedEnabled()).append(", ");
        sb.append("deferred:" + this.isDeferred()).append(", ");
        sb.append("alert:" + this.isAlert()).append(", ");
        sb.append("notificationPref:");
        if(this.getNotificationPref() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNotificationPref()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("refreshInterval:" + this.getRefreshInterval()).append(", ");
        sb.append("refreshExpressions:");
        if(this.getRefreshExpressions() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRefreshExpressions()).append("\", ");
        }
        sb.append("refreshTimeZone:");
        if(this.getRefreshTimeZone() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRefreshTimeZone()).append("\", ");
        }
        sb.append("currentRefreshCount:" + this.getCurrentRefreshCount()).append(", ");
        sb.append("maxRefreshCount:" + this.getMaxRefreshCount()).append(", ");
        sb.append("refreshExpirationTime:" + this.getRefreshExpirationTime()).append(", ");
        sb.append("nextRefreshTime:" + this.getNextRefreshTime()).append(", ");
        sb.append("lastUpdatedTime:" + this.getLastUpdatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getManagerApp() != null) {
            sb.append("\"managerApp\":").append("\"").append(this.getManagerApp()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"managerApp\":").append("null, ");
        }
        if(this.getAppAcl() != null) {
            sb.append("\"appAcl\":").append("").append(this.getAppAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"appAcl\":").append("null, ");
        }
        sb.append("\"gaeApp\":").append(this.gaeApp.toJsonString()).append(", ");
        if(this.getOwnerUser() != null) {
            sb.append("\"ownerUser\":").append("\"").append(this.getOwnerUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ownerUser\":").append("null, ");
        }
        if(this.getUserAcl() != null) {
            sb.append("\"userAcl\":").append("").append(this.getUserAcl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAcl\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getFetchUrl() != null) {
            sb.append("\"fetchUrl\":").append("\"").append(this.getFetchUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchUrl\":").append("null, ");
        }
        if(this.getFeedUrl() != null) {
            sb.append("\"feedUrl\":").append("\"").append(this.getFeedUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedUrl\":").append("null, ");
        }
        if(this.getChannelFeed() != null) {
            sb.append("\"channelFeed\":").append("\"").append(this.getChannelFeed()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelFeed\":").append("null, ");
        }
        if(this.isReuseChannel() != null) {
            sb.append("\"reuseChannel\":").append("").append(this.isReuseChannel()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"reuseChannel\":").append("null, ");
        }
        if(this.getMaxItemCount() != null) {
            sb.append("\"maxItemCount\":").append("").append(this.getMaxItemCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"maxItemCount\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getOriginFetch() != null) {
            sb.append("\"originFetch\":").append("\"").append(this.getOriginFetch()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"originFetch\":").append("null, ");
        }
        if(this.getOutputFormat() != null) {
            sb.append("\"outputFormat\":").append("\"").append(this.getOutputFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputFormat\":").append("null, ");
        }
        if(this.getFetchStatus() != null) {
            sb.append("\"fetchStatus\":").append("").append(this.getFetchStatus()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchStatus\":").append("null, ");
        }
        if(this.getResult() != null) {
            sb.append("\"result\":").append("\"").append(this.getResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"result\":").append("null, ");
        }
        if(this.getFeedCategory() != null) {
            sb.append("\"feedCategory\":").append("\"").append(this.getFeedCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedCategory\":").append("null, ");
        }
        if(this.isMultipleFeedEnabled() != null) {
            sb.append("\"multipleFeedEnabled\":").append("").append(this.isMultipleFeedEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"multipleFeedEnabled\":").append("null, ");
        }
        if(this.isDeferred() != null) {
            sb.append("\"deferred\":").append("").append(this.isDeferred()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"deferred\":").append("null, ");
        }
        if(this.isAlert() != null) {
            sb.append("\"alert\":").append("").append(this.isAlert()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"alert\":").append("null, ");
        }
        sb.append("\"notificationPref\":").append(this.notificationPref.toJsonString()).append(", ");
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getRefreshInterval() != null) {
            sb.append("\"refreshInterval\":").append("").append(this.getRefreshInterval()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshInterval\":").append("null, ");
        }
        if(this.getRefreshExpressions() != null) {
            sb.append("\"refreshExpressions\":").append("\"").append(this.getRefreshExpressions()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshExpressions\":").append("null, ");
        }
        if(this.getRefreshTimeZone() != null) {
            sb.append("\"refreshTimeZone\":").append("\"").append(this.getRefreshTimeZone()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshTimeZone\":").append("null, ");
        }
        if(this.getCurrentRefreshCount() != null) {
            sb.append("\"currentRefreshCount\":").append("").append(this.getCurrentRefreshCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"currentRefreshCount\":").append("null, ");
        }
        if(this.getMaxRefreshCount() != null) {
            sb.append("\"maxRefreshCount\":").append("").append(this.getMaxRefreshCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"maxRefreshCount\":").append("null, ");
        }
        if(this.getRefreshExpirationTime() != null) {
            sb.append("\"refreshExpirationTime\":").append("").append(this.getRefreshExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshExpirationTime\":").append("null, ");
        }
        if(this.getNextRefreshTime() != null) {
            sb.append("\"nextRefreshTime\":").append("").append(this.getNextRefreshTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextRefreshTime\":").append("null, ");
        }
        if(this.getLastUpdatedTime() != null) {
            sb.append("\"lastUpdatedTime\":").append("").append(this.getLastUpdatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastUpdatedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("originFetch = " + this.originFetch).append(";");
        sb.append("outputFormat = " + this.outputFormat).append(";");
        sb.append("fetchStatus = " + this.fetchStatus).append(";");
        sb.append("result = " + this.result).append(";");
        sb.append("feedCategory = " + this.feedCategory).append(";");
        sb.append("multipleFeedEnabled = " + this.multipleFeedEnabled).append(";");
        sb.append("deferred = " + this.deferred).append(";");
        sb.append("alert = " + this.alert).append(";");
        sb.append("notificationPref = " + this.notificationPref).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("refreshInterval = " + this.refreshInterval).append(";");
        sb.append("refreshExpressions = " + this.refreshExpressions).append(";");
        sb.append("refreshTimeZone = " + this.refreshTimeZone).append(";");
        sb.append("currentRefreshCount = " + this.currentRefreshCount).append(";");
        sb.append("maxRefreshCount = " + this.maxRefreshCount).append(";");
        sb.append("refreshExpirationTime = " + this.refreshExpirationTime).append(";");
        sb.append("nextRefreshTime = " + this.nextRefreshTime).append(";");
        sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FetchRequestJsBean cloned = new FetchRequestJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setManagerApp(this.getManagerApp());   
        cloned.setAppAcl(this.getAppAcl());   
        cloned.setGaeApp( (GaeAppStructJsBean) this.getGaeApp().clone() );
        cloned.setOwnerUser(this.getOwnerUser());   
        cloned.setUserAcl(this.getUserAcl());   
        cloned.setUser(this.getUser());   
        cloned.setTitle(this.getTitle());   
        cloned.setDescription(this.getDescription());   
        cloned.setFetchUrl(this.getFetchUrl());   
        cloned.setFeedUrl(this.getFeedUrl());   
        cloned.setChannelFeed(this.getChannelFeed());   
        cloned.setReuseChannel(this.isReuseChannel());   
        cloned.setMaxItemCount(this.getMaxItemCount());   
        cloned.setNote(this.getNote());   
        cloned.setStatus(this.getStatus());   
        cloned.setOriginFetch(this.getOriginFetch());   
        cloned.setOutputFormat(this.getOutputFormat());   
        cloned.setFetchStatus(this.getFetchStatus());   
        cloned.setResult(this.getResult());   
        cloned.setFeedCategory(this.getFeedCategory());   
        cloned.setMultipleFeedEnabled(this.isMultipleFeedEnabled());   
        cloned.setDeferred(this.isDeferred());   
        cloned.setAlert(this.isAlert());   
        cloned.setNotificationPref( (NotificationStructJsBean) this.getNotificationPref().clone() );
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setRefreshInterval(this.getRefreshInterval());   
        cloned.setRefreshExpressions(this.getRefreshExpressions());   
        cloned.setRefreshTimeZone(this.getRefreshTimeZone());   
        cloned.setCurrentRefreshCount(this.getCurrentRefreshCount());   
        cloned.setMaxRefreshCount(this.getMaxRefreshCount());   
        cloned.setRefreshExpirationTime(this.getRefreshExpirationTime());   
        cloned.setNextRefreshTime(this.getNextRefreshTime());   
        cloned.setLastUpdatedTime(this.getLastUpdatedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
