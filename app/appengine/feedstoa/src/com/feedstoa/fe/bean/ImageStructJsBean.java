package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageStructJsBean implements Serializable, Cloneable  //, ImageStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String url;
    private String title;
    private String link;
    private Integer width;
    private Integer height;
    private String description;

    // Ctors.
    public ImageStructJsBean()
    {
        //this((String) null);
    }
    public ImageStructJsBean(String url, String title, String link, Integer width, Integer height, String description)
    {
        this.url = url;
        this.title = title;
        this.link = link;
        this.width = width;
        this.height = height;
        this.description = description;
    }
    public ImageStructJsBean(ImageStructJsBean bean)
    {
        if(bean != null) {
            setUrl(bean.getUrl());
            setTitle(bean.getTitle());
            setLink(bean.getLink());
            setWidth(bean.getWidth());
            setHeight(bean.getHeight());
            setDescription(bean.getDescription());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ImageStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ImageStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ImageStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ImageStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
    }

    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("url:null, ");
        sb.append("title:null, ");
        sb.append("link:null, ");
        sb.append("width:0, ");
        sb.append("height:0, ");
        sb.append("description:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("url:");
        if(this.getUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrl()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("link:");
        if(this.getLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLink()).append("\", ");
        }
        sb.append("width:" + this.getWidth()).append(", ");
        sb.append("height:" + this.getHeight()).append(", ");
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUrl() != null) {
            sb.append("\"url\":").append("\"").append(this.getUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"url\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getLink() != null) {
            sb.append("\"link\":").append("\"").append(this.getLink()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"link\":").append("null, ");
        }
        if(this.getWidth() != null) {
            sb.append("\"width\":").append("").append(this.getWidth()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"width\":").append("null, ");
        }
        if(this.getHeight() != null) {
            sb.append("\"height\":").append("").append(this.getHeight()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"height\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("url = " + this.url).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("link = " + this.link).append(";");
        sb.append("width = " + this.width).append(";");
        sb.append("height = " + this.height).append(";");
        sb.append("description = " + this.description).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ImageStructJsBean cloned = new ImageStructJsBean();
        cloned.setUrl(this.getUrl());   
        cloned.setTitle(this.getTitle());   
        cloned.setLink(this.getLink());   
        cloned.setWidth(this.getWidth());   
        cloned.setHeight(this.getHeight());   
        cloned.setDescription(this.getDescription());   
        return cloned;
    }

}
