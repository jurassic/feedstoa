package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UriStructJsBean implements Serializable, Cloneable  //, UriStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UriStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String href;
    private String rel;
    private String type;
    private String label;

    // Ctors.
    public UriStructJsBean()
    {
        //this((String) null);
    }
    public UriStructJsBean(String uuid, String href, String rel, String type, String label)
    {
        this.uuid = uuid;
        this.href = href;
        this.rel = rel;
        this.type = type;
        this.label = label;
    }
    public UriStructJsBean(UriStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setHref(bean.getHref());
            setRel(bean.getRel());
            setType(bean.getType());
            setLabel(bean.getLabel());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UriStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UriStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UriStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UriStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getHref()
    {
        return this.href;
    }
    public void setHref(String href)
    {
        this.href = href;
    }

    public String getRel()
    {
        return this.rel;
    }
    public void setRel(String rel)
    {
        this.rel = rel;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getLabel()
    {
        return this.label;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLabel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("href:null, ");
        sb.append("rel:null, ");
        sb.append("type:null, ");
        sb.append("label:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("href:");
        if(this.getHref() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHref()).append("\", ");
        }
        sb.append("rel:");
        if(this.getRel() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRel()).append("\", ");
        }
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("label:");
        if(this.getLabel() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLabel()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getHref() != null) {
            sb.append("\"href\":").append("\"").append(this.getHref()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"href\":").append("null, ");
        }
        if(this.getRel() != null) {
            sb.append("\"rel\":").append("\"").append(this.getRel()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"rel\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getLabel() != null) {
            sb.append("\"label\":").append("\"").append(this.getLabel()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"label\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("href = " + this.href).append(";");
        sb.append("rel = " + this.rel).append(";");
        sb.append("type = " + this.type).append(";");
        sb.append("label = " + this.label).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UriStructJsBean cloned = new UriStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setHref(this.getHref());   
        cloned.setRel(this.getRel());   
        cloned.setType(this.getType());   
        cloned.setLabel(this.getLabel());   
        return cloned;
    }

}
