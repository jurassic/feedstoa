package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelFeedJsBean implements Serializable, Cloneable  //, ChannelFeed
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String channelSource;
    private String channelCode;
    private String previousVersion;
    private String fetchRequest;
    private String fetchUrl;
    private String feedServiceUrl;
    private String feedUrl;
    private String feedFormat;
    private Integer maxItemCount;
    private String feedCategory;
    private String title;
    private String subtitle;
    private UriStructJsBean link;
    private String description;
    private String language;
    private String copyright;
    private UserStructJsBean managingEditor;
    private UserStructJsBean webMaster;
    private List<UserStructJsBean> contributor;
    private String pubDate;
    private String lastBuildDate;
    private List<CategoryStructJsBean> category;
    private String generator;
    private String docs;
    private CloudStructJsBean cloud;
    private Integer ttl;
    private ImageStructJsBean logo;
    private ImageStructJsBean icon;
    private String rating;
    private TextInputStructJsBean textInput;
    private List<Integer> skipHours;
    private List<String> skipDays;
    private String outputText;
    private String outputHash;
    private String feedContent;
    private String status;
    private String note;
    private ReferrerInfoStructJsBean referrerInfo;
    private Long lastBuildTime;
    private Long publishedTime;
    private Long expirationTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ChannelFeedJsBean()
    {
        //this((String) null);
    }
    public ChannelFeedJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ChannelFeedJsBean(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructJsBean link, String description, String language, String copyright, UserStructJsBean managingEditor, UserStructJsBean webMaster, List<UserStructJsBean> contributor, String pubDate, String lastBuildDate, List<CategoryStructJsBean> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean logo, ImageStructJsBean icon, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime)
    {
        this(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime, null, null);
    }
    public ChannelFeedJsBean(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructJsBean link, String description, String language, String copyright, UserStructJsBean managingEditor, UserStructJsBean webMaster, List<UserStructJsBean> contributor, String pubDate, String lastBuildDate, List<CategoryStructJsBean> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean logo, ImageStructJsBean icon, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.channelSource = channelSource;
        this.channelCode = channelCode;
        this.previousVersion = previousVersion;
        this.fetchRequest = fetchRequest;
        this.fetchUrl = fetchUrl;
        this.feedServiceUrl = feedServiceUrl;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.maxItemCount = maxItemCount;
        this.feedCategory = feedCategory;
        this.title = title;
        this.subtitle = subtitle;
        this.link = link;
        this.description = description;
        this.language = language;
        this.copyright = copyright;
        this.managingEditor = managingEditor;
        this.webMaster = webMaster;
        setContributor(getContributor());
        this.pubDate = pubDate;
        this.lastBuildDate = lastBuildDate;
        setCategory(getCategory());
        this.generator = generator;
        this.docs = docs;
        this.cloud = cloud;
        this.ttl = ttl;
        this.logo = logo;
        this.icon = icon;
        this.rating = rating;
        this.textInput = textInput;
        this.skipHours = skipHours;
        this.skipDays = skipDays;
        this.outputText = outputText;
        this.outputHash = outputHash;
        this.feedContent = feedContent;
        this.status = status;
        this.note = note;
        this.referrerInfo = referrerInfo;
        this.lastBuildTime = lastBuildTime;
        this.publishedTime = publishedTime;
        this.expirationTime = expirationTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ChannelFeedJsBean(ChannelFeedJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setChannelSource(bean.getChannelSource());
            setChannelCode(bean.getChannelCode());
            setPreviousVersion(bean.getPreviousVersion());
            setFetchRequest(bean.getFetchRequest());
            setFetchUrl(bean.getFetchUrl());
            setFeedServiceUrl(bean.getFeedServiceUrl());
            setFeedUrl(bean.getFeedUrl());
            setFeedFormat(bean.getFeedFormat());
            setMaxItemCount(bean.getMaxItemCount());
            setFeedCategory(bean.getFeedCategory());
            setTitle(bean.getTitle());
            setSubtitle(bean.getSubtitle());
            setLink(bean.getLink());
            setDescription(bean.getDescription());
            setLanguage(bean.getLanguage());
            setCopyright(bean.getCopyright());
            setManagingEditor(bean.getManagingEditor());
            setWebMaster(bean.getWebMaster());
            setContributor(bean.getContributor());
            setPubDate(bean.getPubDate());
            setLastBuildDate(bean.getLastBuildDate());
            setCategory(bean.getCategory());
            setGenerator(bean.getGenerator());
            setDocs(bean.getDocs());
            setCloud(bean.getCloud());
            setTtl(bean.getTtl());
            setLogo(bean.getLogo());
            setIcon(bean.getIcon());
            setRating(bean.getRating());
            setTextInput(bean.getTextInput());
            setSkipHours(bean.getSkipHours());
            setSkipDays(bean.getSkipDays());
            setOutputText(bean.getOutputText());
            setOutputHash(bean.getOutputHash());
            setFeedContent(bean.getFeedContent());
            setStatus(bean.getStatus());
            setNote(bean.getNote());
            setReferrerInfo(bean.getReferrerInfo());
            setLastBuildTime(bean.getLastBuildTime());
            setPublishedTime(bean.getPublishedTime());
            setExpirationTime(bean.getExpirationTime());
            setLastUpdatedTime(bean.getLastUpdatedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ChannelFeedJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ChannelFeedJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ChannelFeedJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ChannelFeedJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    public String getChannelCode()
    {
        return this.channelCode;
    }
    public void setChannelCode(String channelCode)
    {
        this.channelCode = channelCode;
    }

    public String getPreviousVersion()
    {
        return this.previousVersion;
    }
    public void setPreviousVersion(String previousVersion)
    {
        this.previousVersion = previousVersion;
    }

    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    public String getFeedServiceUrl()
    {
        return this.feedServiceUrl;
    }
    public void setFeedServiceUrl(String feedServiceUrl)
    {
        this.feedServiceUrl = feedServiceUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public Integer getMaxItemCount()
    {
        return this.maxItemCount;
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        this.maxItemCount = maxItemCount;
    }

    public String getFeedCategory()
    {
        return this.feedCategory;
    }
    public void setFeedCategory(String feedCategory)
    {
        this.feedCategory = feedCategory;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSubtitle()
    {
        return this.subtitle;
    }
    public void setSubtitle(String subtitle)
    {
        this.subtitle = subtitle;
    }

    public UriStructJsBean getLink()
    {  
        return this.link;
    }
    public void setLink(UriStructJsBean link)
    {
        this.link = link;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getCopyright()
    {
        return this.copyright;
    }
    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    public UserStructJsBean getManagingEditor()
    {  
        return this.managingEditor;
    }
    public void setManagingEditor(UserStructJsBean managingEditor)
    {
        this.managingEditor = managingEditor;
    }

    public UserStructJsBean getWebMaster()
    {  
        return this.webMaster;
    }
    public void setWebMaster(UserStructJsBean webMaster)
    {
        this.webMaster = webMaster;
    }

    public List<UserStructJsBean> getContributor()
    {  
        return this.contributor;
    }
    public void setContributor(List<UserStructJsBean> contributor)
    {
        this.contributor = contributor;
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    public List<CategoryStructJsBean> getCategory()
    {  
        return this.category;
    }
    public void setCategory(List<CategoryStructJsBean> category)
    {
        this.category = category;
    }

    public String getGenerator()
    {
        return this.generator;
    }
    public void setGenerator(String generator)
    {
        this.generator = generator;
    }

    public String getDocs()
    {
        return this.docs;
    }
    public void setDocs(String docs)
    {
        this.docs = docs;
    }

    public CloudStructJsBean getCloud()
    {  
        return this.cloud;
    }
    public void setCloud(CloudStructJsBean cloud)
    {
        this.cloud = cloud;
    }

    public Integer getTtl()
    {
        return this.ttl;
    }
    public void setTtl(Integer ttl)
    {
        this.ttl = ttl;
    }

    public ImageStructJsBean getLogo()
    {  
        return this.logo;
    }
    public void setLogo(ImageStructJsBean logo)
    {
        this.logo = logo;
    }

    public ImageStructJsBean getIcon()
    {  
        return this.icon;
    }
    public void setIcon(ImageStructJsBean icon)
    {
        this.icon = icon;
    }

    public String getRating()
    {
        return this.rating;
    }
    public void setRating(String rating)
    {
        this.rating = rating;
    }

    public TextInputStructJsBean getTextInput()
    {  
        return this.textInput;
    }
    public void setTextInput(TextInputStructJsBean textInput)
    {
        this.textInput = textInput;
    }

    public List<Integer> getSkipHours()
    {
        return this.skipHours;
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        this.skipHours = skipHours;
    }

    public List<String> getSkipDays()
    {
        return this.skipDays;
    }
    public void setSkipDays(List<String> skipDays)
    {
        this.skipDays = skipDays;
    }

    public String getOutputText()
    {
        return this.outputText;
    }
    public void setOutputText(String outputText)
    {
        this.outputText = outputText;
    }

    public String getOutputHash()
    {
        return this.outputHash;
    }
    public void setOutputHash(String outputHash)
    {
        this.outputHash = outputHash;
    }

    public String getFeedContent()
    {
        return this.feedContent;
    }
    public void setFeedContent(String feedContent)
    {
        this.feedContent = feedContent;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public Long getLastBuildTime()
    {
        return this.lastBuildTime;
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        this.lastBuildTime = lastBuildTime;
    }

    public Long getPublishedTime()
    {
        return this.publishedTime;
    }
    public void setPublishedTime(Long publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("channelSource:null, ");
        sb.append("channelCode:null, ");
        sb.append("previousVersion:null, ");
        sb.append("fetchRequest:null, ");
        sb.append("fetchUrl:null, ");
        sb.append("feedServiceUrl:null, ");
        sb.append("feedUrl:null, ");
        sb.append("feedFormat:null, ");
        sb.append("maxItemCount:0, ");
        sb.append("feedCategory:null, ");
        sb.append("title:null, ");
        sb.append("subtitle:null, ");
        sb.append("link:{}, ");
        sb.append("description:null, ");
        sb.append("language:null, ");
        sb.append("copyright:null, ");
        sb.append("managingEditor:{}, ");
        sb.append("webMaster:{}, ");
        sb.append("contributor:null, ");
        sb.append("pubDate:null, ");
        sb.append("lastBuildDate:null, ");
        sb.append("category:null, ");
        sb.append("generator:null, ");
        sb.append("docs:null, ");
        sb.append("cloud:{}, ");
        sb.append("ttl:0, ");
        sb.append("logo:{}, ");
        sb.append("icon:{}, ");
        sb.append("rating:null, ");
        sb.append("textInput:{}, ");
        sb.append("skipHours:null, ");
        sb.append("skipDays:null, ");
        sb.append("outputText:null, ");
        sb.append("outputHash:null, ");
        sb.append("feedContent:null, ");
        sb.append("status:null, ");
        sb.append("note:null, ");
        sb.append("referrerInfo:{}, ");
        sb.append("lastBuildTime:0, ");
        sb.append("publishedTime:0, ");
        sb.append("expirationTime:0, ");
        sb.append("lastUpdatedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("channelSource:");
        if(this.getChannelSource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelSource()).append("\", ");
        }
        sb.append("channelCode:");
        if(this.getChannelCode() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getChannelCode()).append("\", ");
        }
        sb.append("previousVersion:");
        if(this.getPreviousVersion() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPreviousVersion()).append("\", ");
        }
        sb.append("fetchRequest:");
        if(this.getFetchRequest() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchRequest()).append("\", ");
        }
        sb.append("fetchUrl:");
        if(this.getFetchUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchUrl()).append("\", ");
        }
        sb.append("feedServiceUrl:");
        if(this.getFeedServiceUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedServiceUrl()).append("\", ");
        }
        sb.append("feedUrl:");
        if(this.getFeedUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedUrl()).append("\", ");
        }
        sb.append("feedFormat:");
        if(this.getFeedFormat() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedFormat()).append("\", ");
        }
        sb.append("maxItemCount:" + this.getMaxItemCount()).append(", ");
        sb.append("feedCategory:");
        if(this.getFeedCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedCategory()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("subtitle:");
        if(this.getSubtitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSubtitle()).append("\", ");
        }
        sb.append("link:");
        if(this.getLink() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLink()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("copyright:");
        if(this.getCopyright() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCopyright()).append("\", ");
        }
        sb.append("managingEditor:");
        if(this.getManagingEditor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getManagingEditor()).append("\", ");
        }
        sb.append("webMaster:");
        if(this.getWebMaster() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getWebMaster()).append("\", ");
        }
        sb.append("contributor:");
        if(this.getContributor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContributor()).append("\", ");
        }
        sb.append("pubDate:");
        if(this.getPubDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPubDate()).append("\", ");
        }
        sb.append("lastBuildDate:");
        if(this.getLastBuildDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastBuildDate()).append("\", ");
        }
        sb.append("category:");
        if(this.getCategory() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCategory()).append("\", ");
        }
        sb.append("generator:");
        if(this.getGenerator() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGenerator()).append("\", ");
        }
        sb.append("docs:");
        if(this.getDocs() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDocs()).append("\", ");
        }
        sb.append("cloud:");
        if(this.getCloud() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCloud()).append("\", ");
        }
        sb.append("ttl:" + this.getTtl()).append(", ");
        sb.append("logo:");
        if(this.getLogo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLogo()).append("\", ");
        }
        sb.append("icon:");
        if(this.getIcon() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getIcon()).append("\", ");
        }
        sb.append("rating:");
        if(this.getRating() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRating()).append("\", ");
        }
        sb.append("textInput:");
        if(this.getTextInput() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTextInput()).append("\", ");
        }
        sb.append("skipHours:");
        if(this.getSkipHours() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSkipHours()).append("\", ");
        }
        sb.append("skipDays:");
        if(this.getSkipDays() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSkipDays()).append("\", ");
        }
        sb.append("outputText:");
        if(this.getOutputText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputText()).append("\", ");
        }
        sb.append("outputHash:");
        if(this.getOutputHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOutputHash()).append("\", ");
        }
        sb.append("feedContent:");
        if(this.getFeedContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFeedContent()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("lastBuildTime:" + this.getLastBuildTime()).append(", ");
        sb.append("publishedTime:" + this.getPublishedTime()).append(", ");
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("lastUpdatedTime:" + this.getLastUpdatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getChannelSource() != null) {
            sb.append("\"channelSource\":").append("\"").append(this.getChannelSource()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelSource\":").append("null, ");
        }
        if(this.getChannelCode() != null) {
            sb.append("\"channelCode\":").append("\"").append(this.getChannelCode()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"channelCode\":").append("null, ");
        }
        if(this.getPreviousVersion() != null) {
            sb.append("\"previousVersion\":").append("\"").append(this.getPreviousVersion()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"previousVersion\":").append("null, ");
        }
        if(this.getFetchRequest() != null) {
            sb.append("\"fetchRequest\":").append("\"").append(this.getFetchRequest()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchRequest\":").append("null, ");
        }
        if(this.getFetchUrl() != null) {
            sb.append("\"fetchUrl\":").append("\"").append(this.getFetchUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchUrl\":").append("null, ");
        }
        if(this.getFeedServiceUrl() != null) {
            sb.append("\"feedServiceUrl\":").append("\"").append(this.getFeedServiceUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedServiceUrl\":").append("null, ");
        }
        if(this.getFeedUrl() != null) {
            sb.append("\"feedUrl\":").append("\"").append(this.getFeedUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedUrl\":").append("null, ");
        }
        if(this.getFeedFormat() != null) {
            sb.append("\"feedFormat\":").append("\"").append(this.getFeedFormat()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedFormat\":").append("null, ");
        }
        if(this.getMaxItemCount() != null) {
            sb.append("\"maxItemCount\":").append("").append(this.getMaxItemCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"maxItemCount\":").append("null, ");
        }
        if(this.getFeedCategory() != null) {
            sb.append("\"feedCategory\":").append("\"").append(this.getFeedCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedCategory\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getSubtitle() != null) {
            sb.append("\"subtitle\":").append("\"").append(this.getSubtitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"subtitle\":").append("null, ");
        }
        sb.append("\"link\":").append(this.link.toJsonString()).append(", ");
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getCopyright() != null) {
            sb.append("\"copyright\":").append("\"").append(this.getCopyright()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"copyright\":").append("null, ");
        }
        sb.append("\"managingEditor\":").append(this.managingEditor.toJsonString()).append(", ");
        sb.append("\"webMaster\":").append(this.webMaster.toJsonString()).append(", ");
        if(this.getContributor() != null) {
            sb.append("\"contributor\":").append("\"").append(this.getContributor()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contributor\":").append("null, ");
        }
        if(this.getPubDate() != null) {
            sb.append("\"pubDate\":").append("\"").append(this.getPubDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pubDate\":").append("null, ");
        }
        if(this.getLastBuildDate() != null) {
            sb.append("\"lastBuildDate\":").append("\"").append(this.getLastBuildDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastBuildDate\":").append("null, ");
        }
        if(this.getCategory() != null) {
            sb.append("\"category\":").append("\"").append(this.getCategory()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"category\":").append("null, ");
        }
        if(this.getGenerator() != null) {
            sb.append("\"generator\":").append("\"").append(this.getGenerator()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"generator\":").append("null, ");
        }
        if(this.getDocs() != null) {
            sb.append("\"docs\":").append("\"").append(this.getDocs()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"docs\":").append("null, ");
        }
        sb.append("\"cloud\":").append(this.cloud.toJsonString()).append(", ");
        if(this.getTtl() != null) {
            sb.append("\"ttl\":").append("").append(this.getTtl()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"ttl\":").append("null, ");
        }
        sb.append("\"logo\":").append(this.logo.toJsonString()).append(", ");
        sb.append("\"icon\":").append(this.icon.toJsonString()).append(", ");
        if(this.getRating() != null) {
            sb.append("\"rating\":").append("\"").append(this.getRating()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"rating\":").append("null, ");
        }
        sb.append("\"textInput\":").append(this.textInput.toJsonString()).append(", ");
        if(this.getSkipHours() != null) {
            sb.append("\"skipHours\":").append("\"").append(this.getSkipHours()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"skipHours\":").append("null, ");
        }
        if(this.getSkipDays() != null) {
            sb.append("\"skipDays\":").append("\"").append(this.getSkipDays()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"skipDays\":").append("null, ");
        }
        if(this.getOutputText() != null) {
            sb.append("\"outputText\":").append("\"").append(this.getOutputText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputText\":").append("null, ");
        }
        if(this.getOutputHash() != null) {
            sb.append("\"outputHash\":").append("\"").append(this.getOutputHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"outputHash\":").append("null, ");
        }
        if(this.getFeedContent() != null) {
            sb.append("\"feedContent\":").append("\"").append(this.getFeedContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"feedContent\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getLastBuildTime() != null) {
            sb.append("\"lastBuildTime\":").append("").append(this.getLastBuildTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastBuildTime\":").append("null, ");
        }
        if(this.getPublishedTime() != null) {
            sb.append("\"publishedTime\":").append("").append(this.getPublishedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"publishedTime\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getLastUpdatedTime() != null) {
            sb.append("\"lastUpdatedTime\":").append("").append(this.getLastUpdatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastUpdatedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("channelSource = " + this.channelSource).append(";");
        sb.append("channelCode = " + this.channelCode).append(";");
        sb.append("previousVersion = " + this.previousVersion).append(";");
        sb.append("fetchRequest = " + this.fetchRequest).append(";");
        sb.append("fetchUrl = " + this.fetchUrl).append(";");
        sb.append("feedServiceUrl = " + this.feedServiceUrl).append(";");
        sb.append("feedUrl = " + this.feedUrl).append(";");
        sb.append("feedFormat = " + this.feedFormat).append(";");
        sb.append("maxItemCount = " + this.maxItemCount).append(";");
        sb.append("feedCategory = " + this.feedCategory).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("subtitle = " + this.subtitle).append(";");
        sb.append("link = " + this.link).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("language = " + this.language).append(";");
        sb.append("copyright = " + this.copyright).append(";");
        sb.append("managingEditor = " + this.managingEditor).append(";");
        sb.append("webMaster = " + this.webMaster).append(";");
        sb.append("contributor = " + this.contributor).append(";");
        sb.append("pubDate = " + this.pubDate).append(";");
        sb.append("lastBuildDate = " + this.lastBuildDate).append(";");
        sb.append("category = " + this.category).append(";");
        sb.append("generator = " + this.generator).append(";");
        sb.append("docs = " + this.docs).append(";");
        sb.append("cloud = " + this.cloud).append(";");
        sb.append("ttl = " + this.ttl).append(";");
        sb.append("logo = " + this.logo).append(";");
        sb.append("icon = " + this.icon).append(";");
        sb.append("rating = " + this.rating).append(";");
        sb.append("textInput = " + this.textInput).append(";");
        sb.append("skipHours = " + this.skipHours).append(";");
        sb.append("skipDays = " + this.skipDays).append(";");
        sb.append("outputText = " + this.outputText).append(";");
        sb.append("outputHash = " + this.outputHash).append(";");
        sb.append("feedContent = " + this.feedContent).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("lastBuildTime = " + this.lastBuildTime).append(";");
        sb.append("publishedTime = " + this.publishedTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ChannelFeedJsBean cloned = new ChannelFeedJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setChannelSource(this.getChannelSource());   
        cloned.setChannelCode(this.getChannelCode());   
        cloned.setPreviousVersion(this.getPreviousVersion());   
        cloned.setFetchRequest(this.getFetchRequest());   
        cloned.setFetchUrl(this.getFetchUrl());   
        cloned.setFeedServiceUrl(this.getFeedServiceUrl());   
        cloned.setFeedUrl(this.getFeedUrl());   
        cloned.setFeedFormat(this.getFeedFormat());   
        cloned.setMaxItemCount(this.getMaxItemCount());   
        cloned.setFeedCategory(this.getFeedCategory());   
        cloned.setTitle(this.getTitle());   
        cloned.setSubtitle(this.getSubtitle());   
        cloned.setLink( (UriStructJsBean) this.getLink().clone() );
        cloned.setDescription(this.getDescription());   
        cloned.setLanguage(this.getLanguage());   
        cloned.setCopyright(this.getCopyright());   
        cloned.setManagingEditor( (UserStructJsBean) this.getManagingEditor().clone() );
        cloned.setWebMaster( (UserStructJsBean) this.getWebMaster().clone() );
        cloned.setContributor(this.getContributor());   
        cloned.setPubDate(this.getPubDate());   
        cloned.setLastBuildDate(this.getLastBuildDate());   
        cloned.setCategory(this.getCategory());   
        cloned.setGenerator(this.getGenerator());   
        cloned.setDocs(this.getDocs());   
        cloned.setCloud( (CloudStructJsBean) this.getCloud().clone() );
        cloned.setTtl(this.getTtl());   
        cloned.setLogo( (ImageStructJsBean) this.getLogo().clone() );
        cloned.setIcon( (ImageStructJsBean) this.getIcon().clone() );
        cloned.setRating(this.getRating());   
        cloned.setTextInput( (TextInputStructJsBean) this.getTextInput().clone() );
        cloned.setSkipHours(this.getSkipHours());   
        cloned.setSkipDays(this.getSkipDays());   
        cloned.setOutputText(this.getOutputText());   
        cloned.setOutputHash(this.getOutputHash());   
        cloned.setFeedContent(this.getFeedContent());   
        cloned.setStatus(this.getStatus());   
        cloned.setNote(this.getNote());   
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setLastBuildTime(this.getLastBuildTime());   
        cloned.setPublishedTime(this.getPublishedTime());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setLastUpdatedTime(this.getLastUpdatedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
