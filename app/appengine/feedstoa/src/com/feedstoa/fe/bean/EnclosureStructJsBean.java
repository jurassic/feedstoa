package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class EnclosureStructJsBean implements Serializable, Cloneable  //, EnclosureStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(EnclosureStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String url;
    private Integer length;
    private String type;

    // Ctors.
    public EnclosureStructJsBean()
    {
        //this((String) null);
    }
    public EnclosureStructJsBean(String url, Integer length, String type)
    {
        this.url = url;
        this.length = length;
        this.type = type;
    }
    public EnclosureStructJsBean(EnclosureStructJsBean bean)
    {
        if(bean != null) {
            setUrl(bean.getUrl());
            setLength(bean.getLength());
            setType(bean.getType());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static EnclosureStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        EnclosureStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(EnclosureStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, EnclosureStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public Integer getLength()
    {
        return this.length;
    }
    public void setLength(Integer length)
    {
        this.length = length;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLength() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("url:null, ");
        sb.append("length:0, ");
        sb.append("type:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("url:");
        if(this.getUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrl()).append("\", ");
        }
        sb.append("length:" + this.getLength()).append(", ");
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUrl() != null) {
            sb.append("\"url\":").append("\"").append(this.getUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"url\":").append("null, ");
        }
        if(this.getLength() != null) {
            sb.append("\"length\":").append("").append(this.getLength()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"length\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("url = " + this.url).append(";");
        sb.append("length = " + this.length).append(";");
        sb.append("type = " + this.type).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        EnclosureStructJsBean cloned = new EnclosureStructJsBean();
        cloned.setUrl(this.getUrl());   
        cloned.setLength(this.getLength());   
        cloned.setType(this.getType());   
        return cloned;
    }

}
