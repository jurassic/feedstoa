package com.feedstoa.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RedirectRuleJsBean implements Serializable, Cloneable  //, RedirectRule
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RedirectRuleJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String redirectType;
    private Double precedence;
    private String sourceDomain;
    private String sourcePath;
    private String targetDomain;
    private String targetPath;
    private String note;
    private String status;

    // Ctors.
    public RedirectRuleJsBean()
    {
        //this((String) null);
    }
    public RedirectRuleJsBean(String redirectType, Double precedence, String sourceDomain, String sourcePath, String targetDomain, String targetPath, String note, String status)
    {
        this.redirectType = redirectType;
        this.precedence = precedence;
        this.sourceDomain = sourceDomain;
        this.sourcePath = sourcePath;
        this.targetDomain = targetDomain;
        this.targetPath = targetPath;
        this.note = note;
        this.status = status;
    }
    public RedirectRuleJsBean(RedirectRuleJsBean bean)
    {
        if(bean != null) {
            setRedirectType(bean.getRedirectType());
            setPrecedence(bean.getPrecedence());
            setSourceDomain(bean.getSourceDomain());
            setSourcePath(bean.getSourcePath());
            setTargetDomain(bean.getTargetDomain());
            setTargetPath(bean.getTargetPath());
            setNote(bean.getNote());
            setStatus(bean.getStatus());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RedirectRuleJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RedirectRuleJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RedirectRuleJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RedirectRuleJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public Double getPrecedence()
    {
        return this.precedence;
    }
    public void setPrecedence(Double precedence)
    {
        this.precedence = precedence;
    }

    public String getSourceDomain()
    {
        return this.sourceDomain;
    }
    public void setSourceDomain(String sourceDomain)
    {
        this.sourceDomain = sourceDomain;
    }

    public String getSourcePath()
    {
        return this.sourcePath;
    }
    public void setSourcePath(String sourcePath)
    {
        this.sourcePath = sourcePath;
    }

    public String getTargetDomain()
    {
        return this.targetDomain;
    }
    public void setTargetDomain(String targetDomain)
    {
        this.targetDomain = targetDomain;
    }

    public String getTargetPath()
    {
        return this.targetPath;
    }
    public void setTargetPath(String targetPath)
    {
        this.targetPath = targetPath;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrecedence() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourceDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourcePath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("redirectType:null, ");
        sb.append("precedence:0, ");
        sb.append("sourceDomain:null, ");
        sb.append("sourcePath:null, ");
        sb.append("targetDomain:null, ");
        sb.append("targetPath:null, ");
        sb.append("note:null, ");
        sb.append("status:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("redirectType:");
        if(this.getRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectType()).append("\", ");
        }
        sb.append("precedence:" + this.getPrecedence()).append(", ");
        sb.append("sourceDomain:");
        if(this.getSourceDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSourceDomain()).append("\", ");
        }
        sb.append("sourcePath:");
        if(this.getSourcePath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSourcePath()).append("\", ");
        }
        sb.append("targetDomain:");
        if(this.getTargetDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetDomain()).append("\", ");
        }
        sb.append("targetPath:");
        if(this.getTargetPath() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetPath()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getRedirectType() != null) {
            sb.append("\"redirectType\":").append("\"").append(this.getRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectType\":").append("null, ");
        }
        if(this.getPrecedence() != null) {
            sb.append("\"precedence\":").append("").append(this.getPrecedence()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"precedence\":").append("null, ");
        }
        if(this.getSourceDomain() != null) {
            sb.append("\"sourceDomain\":").append("\"").append(this.getSourceDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sourceDomain\":").append("null, ");
        }
        if(this.getSourcePath() != null) {
            sb.append("\"sourcePath\":").append("\"").append(this.getSourcePath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sourcePath\":").append("null, ");
        }
        if(this.getTargetDomain() != null) {
            sb.append("\"targetDomain\":").append("\"").append(this.getTargetDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetDomain\":").append("null, ");
        }
        if(this.getTargetPath() != null) {
            sb.append("\"targetPath\":").append("\"").append(this.getTargetPath()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetPath\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("redirectType = " + this.redirectType).append(";");
        sb.append("precedence = " + this.precedence).append(";");
        sb.append("sourceDomain = " + this.sourceDomain).append(";");
        sb.append("sourcePath = " + this.sourcePath).append(";");
        sb.append("targetDomain = " + this.targetDomain).append(";");
        sb.append("targetPath = " + this.targetPath).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("status = " + this.status).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RedirectRuleJsBean cloned = new RedirectRuleJsBean();
        cloned.setRedirectType(this.getRedirectType());   
        cloned.setPrecedence(this.getPrecedence());   
        cloned.setSourceDomain(this.getSourceDomain());   
        cloned.setSourcePath(this.getSourcePath());   
        cloned.setTargetDomain(this.getTargetDomain());   
        cloned.setTargetPath(this.getTargetPath());   
        cloned.setNote(this.getNote());   
        cloned.setStatus(this.getStatus());   
        return cloned;
    }

}
