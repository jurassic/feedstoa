package com.feedstoa.fe.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.feedstoa.fe.bean.KeyValuePairStructJsBean;
import com.feedstoa.fe.bean.KeyValueRelationStructJsBean;
import com.feedstoa.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.feedstoa.fe.bean.GeoPointStructJsBean;
import com.feedstoa.fe.bean.GeoCoordinateStructJsBean;
import com.feedstoa.fe.bean.CellLatitudeLongitudeJsBean;
import com.feedstoa.fe.bean.StreetAddressStructJsBean;
import com.feedstoa.fe.bean.FullNameStructJsBean;
import com.feedstoa.fe.bean.UserWebsiteStructJsBean;
import com.feedstoa.fe.bean.ContactInfoStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.GaeAppStructJsBean;
import com.feedstoa.fe.bean.GaeUserStructJsBean;
import com.feedstoa.fe.bean.NotificationStructJsBean;
import com.feedstoa.fe.bean.PagerStateStructJsBean;
import com.feedstoa.fe.bean.AppBrandStructJsBean;
import com.feedstoa.fe.bean.ApiConsumerJsBean;
import com.feedstoa.fe.bean.UserJsBean;
import com.feedstoa.fe.bean.FetchRequestJsBean;
import com.feedstoa.fe.bean.SecondaryFetchJsBean;
import com.feedstoa.fe.bean.ChannelSourceJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.fe.bean.ImageStructJsBean;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.fe.bean.EnclosureStructJsBean;
import com.feedstoa.fe.bean.RssItemJsBean;
import com.feedstoa.fe.bean.RssChannelJsBean;
import com.feedstoa.fe.bean.ChannelFeedJsBean;
import com.feedstoa.fe.bean.FeedItemJsBean;
import com.feedstoa.fe.bean.FeedContentJsBean;
import com.feedstoa.fe.bean.RedirectRuleJsBean;
import com.feedstoa.fe.bean.HelpNoticeJsBean;
import com.feedstoa.fe.bean.SiteLogJsBean;
import com.feedstoa.fe.bean.ServiceInfoJsBean;
import com.feedstoa.fe.bean.FiveTenJsBean;


// Utility functions for combining/decomposing into json string segments.
public class JsonJsBeanUtil
{
    private static final Logger log = Logger.getLogger(JsonJsBeanUtil.class.getName());

    private JsonJsBeanUtil() {}

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectMapString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                JsonNode leafNode = topNode.get(name);
                if(! leafNode.isNull()) {
                    String value = leafNode.asText();
                    jsonObjectMap.put(name, value);
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: Empty node skipped. name = " + name);
                }
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> objJsonString
    public static String generateJsonObjectMapString(Map<String, String> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMapString(): jsonStr = " + jsonStr);

        return jsonStr;
    }


    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> jsBean
    public static Map<String, Object> parseJsonObjectMap(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, Object> jsonObjectMap = new HashMap<String, Object>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);
            JsonNode topNode = parser.readValueAsTree();

/*
            // TBD: Remove the loop.
            Iterator<String> fieldNames = topNode.getFieldNames();
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).asText();
                if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);

                if(name.equals("keyValuePairStruct")) {
                    KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keyValueRelationStruct")) {
                    KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalServiceApiKeyStruct")) {
                    ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoPointStruct")) {
                    GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoCoordinateStruct")) {
                    GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cellLatitudeLongitude")) {
                    CellLatitudeLongitudeJsBean bean = CellLatitudeLongitudeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("streetAddressStruct")) {
                    StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fullNameStruct")) {
                    FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userWebsiteStruct")) {
                    UserWebsiteStructJsBean bean = UserWebsiteStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("contactInfoStruct")) {
                    ContactInfoStructJsBean bean = ContactInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("referrerInfoStruct")) {
                    ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeAppStruct")) {
                    GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeUserStruct")) {
                    GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("notificationStruct")) {
                    NotificationStructJsBean bean = NotificationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pagerStateStruct")) {
                    PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("appBrandStruct")) {
                    AppBrandStructJsBean bean = AppBrandStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("apiConsumer")) {
                    ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("user")) {
                    UserJsBean bean = UserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fetchRequest")) {
                    FetchRequestJsBean bean = FetchRequestJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("secondaryFetch")) {
                    SecondaryFetchJsBean bean = SecondaryFetchJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("channelSource")) {
                    ChannelSourceJsBean bean = ChannelSourceJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("uriStruct")) {
                    UriStructJsBean bean = UriStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userStruct")) {
                    UserStructJsBean bean = UserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("categoryStruct")) {
                    CategoryStructJsBean bean = CategoryStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cloudStruct")) {
                    CloudStructJsBean bean = CloudStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("imageStruct")) {
                    ImageStructJsBean bean = ImageStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("textInputStruct")) {
                    TextInputStructJsBean bean = TextInputStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("enclosureStruct")) {
                    EnclosureStructJsBean bean = EnclosureStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("rssItem")) {
                    RssItemJsBean bean = RssItemJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("rssChannel")) {
                    RssChannelJsBean bean = RssChannelJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("channelFeed")) {
                    ChannelFeedJsBean bean = ChannelFeedJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("feedItem")) {
                    FeedItemJsBean bean = FeedItemJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("feedContent")) {
                    FeedContentJsBean bean = FeedContentJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("redirectRule")) {
                    RedirectRuleJsBean bean = RedirectRuleJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("helpNotice")) {
                    HelpNoticeJsBean bean = HelpNoticeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("siteLog")) {
                    SiteLogJsBean bean = SiteLogJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceInfo")) {
                    ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fiveTen")) {
                    FiveTenJsBean bean = FiveTenJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
            }
*/

            JsonNode objNode = null;
            String objValueStr = null;
            objNode = topNode.findValue("keyValuePairStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValuePairStruct", bean);               
            }
            objNode = topNode.findValue("keyValueRelationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValueRelationStruct", bean);               
            }
            objNode = topNode.findValue("externalServiceApiKeyStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalServiceApiKeyStruct", bean);               
            }
            objNode = topNode.findValue("geoPointStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoPointStruct", bean);               
            }
            objNode = topNode.findValue("geoCoordinateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoCoordinateStruct", bean);               
            }
            objNode = topNode.findValue("cellLatitudeLongitude");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CellLatitudeLongitudeJsBean bean = CellLatitudeLongitudeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cellLatitudeLongitude", bean);               
            }
            objNode = topNode.findValue("streetAddressStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("streetAddressStruct", bean);               
            }
            objNode = topNode.findValue("fullNameStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fullNameStruct", bean);               
            }
            objNode = topNode.findValue("userWebsiteStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserWebsiteStructJsBean bean = UserWebsiteStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userWebsiteStruct", bean);               
            }
            objNode = topNode.findValue("contactInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ContactInfoStructJsBean bean = ContactInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("contactInfoStruct", bean);               
            }
            objNode = topNode.findValue("referrerInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("referrerInfoStruct", bean);               
            }
            objNode = topNode.findValue("gaeAppStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeAppStruct", bean);               
            }
            objNode = topNode.findValue("gaeUserStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeUserStruct", bean);               
            }
            objNode = topNode.findValue("notificationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                NotificationStructJsBean bean = NotificationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("notificationStruct", bean);               
            }
            objNode = topNode.findValue("pagerStateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pagerStateStruct", bean);               
            }
            objNode = topNode.findValue("appBrandStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AppBrandStructJsBean bean = AppBrandStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("appBrandStruct", bean);               
            }
            objNode = topNode.findValue("apiConsumer");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("apiConsumer", bean);               
            }
            objNode = topNode.findValue("user");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserJsBean bean = UserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("user", bean);               
            }
            objNode = topNode.findValue("fetchRequest");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FetchRequestJsBean bean = FetchRequestJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fetchRequest", bean);               
            }
            objNode = topNode.findValue("secondaryFetch");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                SecondaryFetchJsBean bean = SecondaryFetchJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("secondaryFetch", bean);               
            }
            objNode = topNode.findValue("channelSource");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ChannelSourceJsBean bean = ChannelSourceJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("channelSource", bean);               
            }
            objNode = topNode.findValue("uriStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UriStructJsBean bean = UriStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("uriStruct", bean);               
            }
            objNode = topNode.findValue("userStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserStructJsBean bean = UserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userStruct", bean);               
            }
            objNode = topNode.findValue("categoryStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CategoryStructJsBean bean = CategoryStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("categoryStruct", bean);               
            }
            objNode = topNode.findValue("cloudStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CloudStructJsBean bean = CloudStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cloudStruct", bean);               
            }
            objNode = topNode.findValue("imageStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ImageStructJsBean bean = ImageStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("imageStruct", bean);               
            }
            objNode = topNode.findValue("textInputStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TextInputStructJsBean bean = TextInputStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("textInputStruct", bean);               
            }
            objNode = topNode.findValue("enclosureStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                EnclosureStructJsBean bean = EnclosureStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("enclosureStruct", bean);               
            }
            objNode = topNode.findValue("rssItem");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RssItemJsBean bean = RssItemJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("rssItem", bean);               
            }
            objNode = topNode.findValue("rssChannel");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RssChannelJsBean bean = RssChannelJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("rssChannel", bean);               
            }
            objNode = topNode.findValue("channelFeed");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ChannelFeedJsBean bean = ChannelFeedJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("channelFeed", bean);               
            }
            objNode = topNode.findValue("feedItem");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FeedItemJsBean bean = FeedItemJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("feedItem", bean);               
            }
            objNode = topNode.findValue("feedContent");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FeedContentJsBean bean = FeedContentJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("feedContent", bean);               
            }
            objNode = topNode.findValue("redirectRule");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RedirectRuleJsBean bean = RedirectRuleJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("redirectRule", bean);               
            }
            objNode = topNode.findValue("helpNotice");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                HelpNoticeJsBean bean = HelpNoticeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("helpNotice", bean);               
            }
            objNode = topNode.findValue("siteLog");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                SiteLogJsBean bean = SiteLogJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("siteLog", bean);               
            }
            objNode = topNode.findValue("serviceInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceInfo", bean);               
            }
            objNode = topNode.findValue("fiveTen");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FiveTenJsBean bean = FiveTenJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fiveTen", bean);               
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> jsBean
    public static String generateJsonObjectMap(Map<String, Object> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // TBD: Remove the loop.
        for(String key : jsonObjectMap.keySet()) {
            Object jsonObj = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(jsonObj == null) {
                sb.append("null");  // ????
            } else {
                if(jsonObj instanceof KeyValuePairStructJsBean) {
                    sb.append(((KeyValuePairStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeyValueRelationStructJsBean) {
                    sb.append(((KeyValueRelationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalServiceApiKeyStructJsBean) {
                    sb.append(((ExternalServiceApiKeyStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoPointStructJsBean) {
                    sb.append(((GeoPointStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoCoordinateStructJsBean) {
                    sb.append(((GeoCoordinateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CellLatitudeLongitudeJsBean) {
                    sb.append(((CellLatitudeLongitudeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof StreetAddressStructJsBean) {
                    sb.append(((StreetAddressStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FullNameStructJsBean) {
                    sb.append(((FullNameStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserWebsiteStructJsBean) {
                    sb.append(((UserWebsiteStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ContactInfoStructJsBean) {
                    sb.append(((ContactInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ReferrerInfoStructJsBean) {
                    sb.append(((ReferrerInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeAppStructJsBean) {
                    sb.append(((GaeAppStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeUserStructJsBean) {
                    sb.append(((GaeUserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof NotificationStructJsBean) {
                    sb.append(((NotificationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PagerStateStructJsBean) {
                    sb.append(((PagerStateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AppBrandStructJsBean) {
                    sb.append(((AppBrandStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ApiConsumerJsBean) {
                    sb.append(((ApiConsumerJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserJsBean) {
                    sb.append(((UserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FetchRequestJsBean) {
                    sb.append(((FetchRequestJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof SecondaryFetchJsBean) {
                    sb.append(((SecondaryFetchJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ChannelSourceJsBean) {
                    sb.append(((ChannelSourceJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UriStructJsBean) {
                    sb.append(((UriStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserStructJsBean) {
                    sb.append(((UserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CategoryStructJsBean) {
                    sb.append(((CategoryStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CloudStructJsBean) {
                    sb.append(((CloudStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ImageStructJsBean) {
                    sb.append(((ImageStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TextInputStructJsBean) {
                    sb.append(((TextInputStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof EnclosureStructJsBean) {
                    sb.append(((EnclosureStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RssItemJsBean) {
                    sb.append(((RssItemJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RssChannelJsBean) {
                    sb.append(((RssChannelJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ChannelFeedJsBean) {
                    sb.append(((ChannelFeedJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FeedItemJsBean) {
                    sb.append(((FeedItemJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FeedContentJsBean) {
                    sb.append(((FeedContentJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RedirectRuleJsBean) {
                    sb.append(((RedirectRuleJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof HelpNoticeJsBean) {
                    sb.append(((HelpNoticeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof SiteLogJsBean) {
                    sb.append(((SiteLogJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceInfoJsBean) {
                    sb.append(((ServiceInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FiveTenJsBean) {
                    sb.append(((FiveTenJsBean) jsonObj).toJsonString());
                }
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMap(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
