package com.feedstoa.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.af.service.impl.FeedContentServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class FeedContentAppService extends FeedContentServiceImpl implements FeedContentService
{
    private static final Logger log = Logger.getLogger(FeedContentAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public FeedContentAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FeedContent related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FeedContent getFeedContent(String guid) throws BaseException
    {
        return super.getFeedContent(guid);
    }

    @Override
    public Object getFeedContent(String guid, String field) throws BaseException
    {
        return super.getFeedContent(guid, field);
    }

    @Override
    public List<FeedContent> getFeedContents(List<String> guids) throws BaseException
    {
        return super.getFeedContents(guids);
    }

    @Override
    public List<FeedContent> getAllFeedContents() throws BaseException
    {
        return super.getAllFeedContents();
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllFeedContentKeys(ordering, offset, count);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFeedContents(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createFeedContent(FeedContent feedContent) throws BaseException
    {
        return super.createFeedContent(feedContent);
    }

    @Override
    public FeedContent constructFeedContent(FeedContent feedContent) throws BaseException
    {
        return super.constructFeedContent(feedContent);
    }


    @Override
    public Boolean updateFeedContent(FeedContent feedContent) throws BaseException
    {
        return super.updateFeedContent(feedContent);
    }
        
    @Override
    public FeedContent refreshFeedContent(FeedContent feedContent) throws BaseException
    {
        return super.refreshFeedContent(feedContent);
    }

    @Override
    public Boolean deleteFeedContent(String guid) throws BaseException
    {
        return super.deleteFeedContent(guid);
    }

    @Override
    public Boolean deleteFeedContent(FeedContent feedContent) throws BaseException
    {
        return super.deleteFeedContent(feedContent);
    }

    @Override
    public Integer createFeedContents(List<FeedContent> feedContents) throws BaseException
    {
        return super.createFeedContents(feedContents);
    }

    // TBD
    //@Override
    //public Boolean updateFeedContents(List<FeedContent> feedContents) throws BaseException
    //{
    //}

}
