package com.feedstoa.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.FeedItemServiceImpl;
import com.feedstoa.app.feed.rss.RssFeedProcessor;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.core.GUID;


// Updated.
public class FeedItemAppService extends FeedItemServiceImpl implements FeedItemService
{
    private static final Logger log = Logger.getLogger(FeedItemAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }

    private ChannelFeedService mChannelFeedService = null;
    private ChannelFeedService getChannelFeedService()
    {
    	if(mChannelFeedService == null) {
    		mChannelFeedService = new ChannelFeedServiceImpl();
    	}
    	return mChannelFeedService;
    }

    public FeedItemAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FeedItem related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FeedItem getFeedItem(String guid) throws BaseException
    {
        return super.getFeedItem(guid);
    }

    @Override
    public Object getFeedItem(String guid, String field) throws BaseException
    {
        return super.getFeedItem(guid, field);
    }

    @Override
    public List<FeedItem> getFeedItems(List<String> guids) throws BaseException
    {
        return super.getFeedItems(guids);
    }

    @Override
    public List<FeedItem> getAllFeedItems() throws BaseException
    {
        return super.getAllFeedItems();
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllFeedItemKeys(ordering, offset, count);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFeedItems(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }



    @Override
    public String createFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("TOP: createFeedItem()");

        // TBD:
        //feedItem = checkFeedItem(feedItem);
        feedItem = validateFeedItem(feedItem);
        feedItem = enhanceFeedItem(feedItem);
        feedItem = processFeedItem(feedItem);
        // ...

        // TBD:
//        // Permalink?
//        String permalink = feedItem.getPermalink();
//        if(permalink == null || permalink.isEmpty()) {
//        	permalink = PermalinkUtil.generateFeedItemViewPermalink(topUrl, feedItem);
//        }
      // ...

        String guid = super.createFeedItem(feedItem);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createFeedItem(): guid = " + guid);
        return guid;
    }

    @Override
    public FeedItem constructFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("TOP: constructFeedItem()");

        // TBD:
        //feedItem = checkFeedItem(feedItem);
        feedItem = validateFeedItem(feedItem);
        feedItem = enhanceFeedItem(feedItem);
        feedItem = processFeedItem(feedItem);
        // ...
        
        // TBD:
//        // Permalink?
//        String permalink = feedItem.getPermalink();
//        if(permalink == null || permalink.isEmpty()) {
//        	permalink = PermalinkUtil.generateFeedItemViewPermalink(topUrl, feedItem);
//        }
        // ...
       
                
        feedItem = super.constructFeedItem(feedItem);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructFeedItem(): feedItem = " + feedItem);
        return feedItem;
    }


    @Override
    public Boolean updateFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("TOP: updateFeedItem()");

        // TBD:
        feedItem = validateFeedItem(feedItem);
        feedItem = processFeedItem(feedItem);   // ???
        Boolean suc = super.updateFeedItem(feedItem);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createFeedItem(): suc = " + suc);
        return suc;
    }
        
    @Override
    public FeedItem refreshFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("TOP: refreshFeedItem()");

        // TBD:
        feedItem = validateFeedItem(feedItem);
        feedItem = processFeedItem(feedItem);   // ???
        feedItem = super.refreshFeedItem(feedItem);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshFeedItem(): feedItem = " + feedItem);
        return feedItem;
    }

    
    @Override
    public Boolean deleteFeedItem(String guid) throws BaseException
    {
        return super.deleteFeedItem(guid);
    }

    @Override
    public Boolean deleteFeedItem(FeedItem feedItem) throws BaseException
    {
        return super.deleteFeedItem(feedItem);
    }

    @Override
    public Integer createFeedItems(List<FeedItem> feedItems) throws BaseException
    {
        return super.createFeedItems(feedItems);
    }

    // TBD
    //@Override
    //public Boolean updateFeedItems(List<FeedItem> feedItems) throws BaseException
    //{
    //}

    

    // TBD...
    // This is sort of a "PK" check...
    // If a feedItem with the same PK (channelFeed and id) already exists in the DB during the creation,
    // Make it an update ?????
    // ...
    // Does this make sense????
    private FeedItemBean checkFeedItem(FeedItem feedItem) throws BaseException
    {
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        }
        // else ???
        
        // TBD:
        // 
        
        return bean;
    }

    // TBD...
    private FeedItemBean validateFeedItem(FeedItem feedItem) throws BaseException
    {
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        }
        // else ???
        
        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private FeedItem enhanceFeedItem(FeedItem feedItem) throws BaseException
    {        
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        }
        // else ???
        
        // TBD: ...
        String id = feedItem.getId();
        if(id == null || id.isEmpty()) {
        	id = GUID.generate();         // ????
        	bean.setId(id);
        }
                
        
        // TBD:
        // ...
        
        
        return bean;
    }        

    private FeedItem processFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("TOP: processFeedItem()");
        if(feedItem == null) {
        	return null;
        }
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        }
        // ...

        // ????
    	FeedContent feedContent = RssFeedProcessor.getInstance().constructFeedContent(feedItem);
    	if(log.isLoggable(Level.FINE)) log.fine("feedContent = " + feedContent);

    	if(feedContent != null) {
    		String contentGuid = feedContent.getGuid();
    		bean.setFeedContent(contentGuid);
    		
    		// Need to update its parent's FeedContent guid as well.....
    		// But, does this really make sense in terms of performance and what not????
    		String channelGuid = bean.getChannelFeed();
    		if(channelGuid != null) {
				try {
					ChannelFeed channelFeed = getChannelFeedService().getChannelFeed(channelGuid);
					if(channelFeed != null && channelFeed instanceof ChannelFeedBean) {
						((ChannelFeedBean) channelFeed).setFeedContent(contentGuid);
	    				Boolean suc = getChannelFeedService().updateChannelFeed(channelFeed);
	    				if(log.isLoggable(Level.INFO)) log.info("ChannelFeed has been updated: suc=" + suc + "; channelGuid=" + channelGuid + "; contentGuid=" + contentGuid);
					} else {
						log.warning("ChannelFeed not updated because object is not found: contentGuid = " + contentGuid);
					}
				} catch(BaseException e) {
					// ignore
    				log.log(Level.WARNING, "ChannelFeed has not been updated: channelGuid=" + channelGuid + "; contentGuid=" + contentGuid, e);
				}
    		} else {
    			// Can this happen???
    		}
    	} else {
    		// ????
    	}
    	// .....
    	
    	
        // ????  Bean.feedUrl is not really being used...
        String feedUrl = bean.getFeedUrl();
        if(feedUrl == null) {
        	feedUrl = feedContent.getFeedUrl();
        	if(feedUrl != null) {
        		bean.setFeedUrl(feedUrl);
        	}
        }
        // ....


        log.finer("BOTTOM: processFeedItem()");
        return bean;
    }



}
