package com.feedstoa.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.impl.FetchRequestServiceImpl;
import com.feedstoa.app.fetch.FetchProcessor;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.exception.BadRequestException;


// Updated...
public class FetchRequestAppService extends FetchRequestServiceImpl implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public FetchRequestAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        return super.getFetchRequest(guid);
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return super.getFetchRequest(guid, field);
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        return super.getFetchRequests(guids);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return super.getAllFetchRequests();
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllFetchRequestKeys(ordering, offset, count);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }




    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: createFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        fetchRequest = enhanceFetchRequest(fetchRequest);
        // ...

        
        // ????
        Integer fetchStatus = ((FetchRequestBean) fetchRequest).getFetchStatus();
        if(fetchStatus == null) {
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_CREATED);
        }
        // ...

        Boolean isDeferred = fetchRequest.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                fetchRequest = processFetchRequest(fetchRequest);
                if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: fetchRequest = " + fetchRequest);
                // ...
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            log.fine("Fetch to be procssed in a deferred mode through cron.");
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_SCHEDULED);
        }
        // ...

        
        String guid = super.createFetchRequest(fetchRequest);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createFetchRequest(): guid = " + guid);
        return guid;
    }

    @Override
    public FetchRequest constructFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: constructFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        fetchRequest = enhanceFetchRequest(fetchRequest);
        // ...

        
        // ????
        Integer fetchStatus = ((FetchRequestBean) fetchRequest).getFetchStatus();
        if(fetchStatus == null) {
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_CREATED);
        }
        // ...

        Boolean isDeferred = fetchRequest.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                fetchRequest = processFetchRequest(fetchRequest);
                if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: fetchRequest = " + fetchRequest);
                // ...
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
                // change it to deferred and try again later?
                // or, throw exception here???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            log.fine("Fetch to be procssed in a deferred mode through cron.");
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_SCHEDULED);
        }
        // ...

                
        fetchRequest = super.constructFetchRequest(fetchRequest);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructFetchRequest(): fetchRequest = " + fetchRequest);
        return fetchRequest;
    }


    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: updateFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        fetchRequest = reviseFetchRequest(fetchRequest);
        
        // TBD: Process ???
        Boolean suc = super.updateFetchRequest(fetchRequest);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createFetchRequest(): suc = " + suc);
        return suc;
    }
        
    @Override
    public FetchRequest refreshFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: refreshFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        fetchRequest = reviseFetchRequest(fetchRequest);
        
        // TBD: Process ???
        fetchRequest = super.refreshFetchRequest(fetchRequest);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshFetchRequest(): fetchRequest = " + fetchRequest);
        return fetchRequest;
    }


    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return super.deleteFetchRequest(guid);
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.deleteFetchRequest(fetchRequest);
    }

    @Override
    public Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    {
        return super.createFetchRequests(fetchRequests);
    }

    // TBD
    //@Override
    //public Boolean updateFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    //{
    //}

    
    
    
    // TBD...
    private FetchRequestBean validateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        }
        // else ???
                
        
        String fetchUrl = bean.getFetchUrl();
        //String feedUrl = bean.getFeedUrl();   // ???
        if(fetchUrl == null || fetchUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BadRequestException("Fetch Url is null or empty. Cannot proceed with fetch.");
        }
        
        // TBD: What is feedUrl???
        // In case of fetch, just use fetchUrl????
        // In case of pub, use FeedStoa permalink ?????
        String feedUrl = bean.getFeedUrl();
        if(feedUrl == null || feedUrl.isEmpty()) {
            feedUrl = fetchUrl;
            bean.setFeedUrl(feedUrl);
        }

        
        // TBD
        Long refreshExpirationTime = bean.getRefreshExpirationTime();
        if(refreshExpirationTime == null || refreshExpirationTime == 0L) {  // -1 == no expiration as well, but likely, this has been explicitly set by the caller.
            // ???
            // One month by default???? just to be safe???
            // ....
            refreshExpirationTime = System.currentTimeMillis() + 30 * 24 * 3600 * 1000L;   // 30 days...
            bean.setRefreshExpirationTime(refreshExpirationTime);
            // ????
        }
        // etc...
        
        
        
        // TBD:
        // ...
        
        
        
        return bean;
    }
    
    private FetchRequestBean enhanceFetchRequest(FetchRequest fetchRequest) throws BaseException
    {        
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        }
        // else ???
        
        
        // TBD:
        // ...
        
        
        return bean;
    }        
    
    private FetchRequestBean reviseFetchRequest(FetchRequest fetchRequest) throws BaseException
    {        
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        }
        // else ???
        
        // This is actually not necessary here
        // nextRefreshTime update is done when the fetchRequest is processed (e.g., in FetchProcessor)
//        Integer refreshInterval = bean.getRefreshInterval();   // In seconds...
//        if(refreshInterval != null && refreshInterval > 0) {
//            //Long nextRefreshTime = bean.getNextRefreshTime();
//            //Long refreshExpirationTime = bean.getRefreshExpirationTime();
//            
//            // TBD:
//            // Minimum value for refreshInterval ??? Such as 10 minutes, etc. ???
//            // ...
//
//            // Note the next refresh time is calculated based on NOW, not based on the last scheduled refresh time....
//            Long nextRefreshTime = System.currentTimeMillis() + refreshInterval * 1000L;
//            bean.setNextRefreshTime(nextRefreshTime);
//            // ....
//            
//        }        
        
        
        // TBD:
        // ...
        
        
        return bean;
    }        

    private FetchRequestBean processFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: processFetchRequest()");
        if(fetchRequest == null) {
            return null;
        }
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        }
        // ...
        
        // TBD: ...
        bean = (FetchRequestBean) FetchProcessor.getInstance().processPageFetch(bean, false);

        // .....

        log.finer("BOTTOM: processFetchRequest()");
        return bean;
    }

    
    
}
