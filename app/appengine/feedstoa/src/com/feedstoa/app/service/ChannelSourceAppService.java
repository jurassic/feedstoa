package com.feedstoa.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.af.service.impl.ChannelSourceServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ChannelSourceAppService extends ChannelSourceServiceImpl implements ChannelSourceService
{
    private static final Logger log = Logger.getLogger(ChannelSourceAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ChannelSourceAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ChannelSource related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ChannelSource getChannelSource(String guid) throws BaseException
    {
        return super.getChannelSource(guid);
    }

    @Override
    public Object getChannelSource(String guid, String field) throws BaseException
    {
        return super.getChannelSource(guid, field);
    }

    @Override
    public List<ChannelSource> getChannelSources(List<String> guids) throws BaseException
    {
        return super.getChannelSources(guids);
    }

    @Override
    public List<ChannelSource> getAllChannelSources() throws BaseException
    {
        return super.getAllChannelSources();
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllChannelSourceKeys(ordering, offset, count);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findChannelSources(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createChannelSource(ChannelSource channelSource) throws BaseException
    {
        return super.createChannelSource(channelSource);
    }

    @Override
    public ChannelSource constructChannelSource(ChannelSource channelSource) throws BaseException
    {
        return super.constructChannelSource(channelSource);
    }


    @Override
    public Boolean updateChannelSource(ChannelSource channelSource) throws BaseException
    {
        return super.updateChannelSource(channelSource);
    }
        
    @Override
    public ChannelSource refreshChannelSource(ChannelSource channelSource) throws BaseException
    {
        return super.refreshChannelSource(channelSource);
    }

    @Override
    public Boolean deleteChannelSource(String guid) throws BaseException
    {
        return super.deleteChannelSource(guid);
    }

    @Override
    public Boolean deleteChannelSource(ChannelSource channelSource) throws BaseException
    {
        return super.deleteChannelSource(channelSource);
    }

    @Override
    public Integer createChannelSources(List<ChannelSource> channelSources) throws BaseException
    {
        return super.createChannelSources(channelSources);
    }

    // TBD
    //@Override
    //public Boolean updateChannelSources(List<ChannelSource> channelSources) throws BaseException
    //{
    //}

}
