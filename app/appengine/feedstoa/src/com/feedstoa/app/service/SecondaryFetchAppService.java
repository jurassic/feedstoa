package com.feedstoa.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.impl.SecondaryFetchServiceImpl;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.exception.BadRequestException;


// Updated...
public class SecondaryFetchAppService extends SecondaryFetchServiceImpl implements SecondaryFetchService
{
    private static final Logger log = Logger.getLogger(SecondaryFetchAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public SecondaryFetchAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // SecondaryFetch related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public SecondaryFetch getSecondaryFetch(String guid) throws BaseException
    {
        return super.getSecondaryFetch(guid);
    }

    @Override
    public Object getSecondaryFetch(String guid, String field) throws BaseException
    {
        return super.getSecondaryFetch(guid, field);
    }

    @Override
    public List<SecondaryFetch> getSecondaryFetches(List<String> guids) throws BaseException
    {
        return super.getSecondaryFetches(guids);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches() throws BaseException
    {
        return super.getAllSecondaryFetches();
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllSecondaryFetchKeys(ordering, offset, count);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }



    @Override
    public String createSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("TOP: createSecondaryFetch()");

        // TBD:
        secondaryFetch = validateSecondaryFetch(secondaryFetch);
        secondaryFetch = enhanceSecondaryFetch(secondaryFetch);
        // ...

        
        String guid = super.createSecondaryFetch(secondaryFetch);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createSecondaryFetch(): guid = " + guid);
        return guid;
    }

    @Override
    public SecondaryFetch constructSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("TOP: constructSecondaryFetch()");

        // TBD:
        secondaryFetch = validateSecondaryFetch(secondaryFetch);
        secondaryFetch = enhanceSecondaryFetch(secondaryFetch);
        // ...

                
        secondaryFetch = super.constructSecondaryFetch(secondaryFetch);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructSecondaryFetch(): secondaryFetch = " + secondaryFetch);
        return secondaryFetch;
    }


    @Override
    public Boolean updateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("TOP: updateSecondaryFetch()");

        // TBD:
        secondaryFetch = validateSecondaryFetch(secondaryFetch);
        secondaryFetch = reviseSecondaryFetch(secondaryFetch);
        
        // TBD: Process ???
        Boolean suc = super.updateSecondaryFetch(secondaryFetch);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createSecondaryFetch(): suc = " + suc);
        return suc;
    }
        
    @Override
    public SecondaryFetch refreshSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("TOP: refreshSecondaryFetch()");

        // TBD:
        secondaryFetch = validateSecondaryFetch(secondaryFetch);
        secondaryFetch = reviseSecondaryFetch(secondaryFetch);
        
        // TBD: Process ???
        secondaryFetch = super.refreshSecondaryFetch(secondaryFetch);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshSecondaryFetch(): secondaryFetch = " + secondaryFetch);
        return secondaryFetch;
    }


    @Override
    public Boolean deleteSecondaryFetch(String guid) throws BaseException
    {
        return super.deleteSecondaryFetch(guid);
    }

    @Override
    public Boolean deleteSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        return super.deleteSecondaryFetch(secondaryFetch);
    }

    @Override
    public Integer createSecondaryFetches(List<SecondaryFetch> secondaryFetches) throws BaseException
    {
        return super.createSecondaryFetches(secondaryFetches);
    }

    // TBD
    //@Override
    //public Boolean updateSecondaryFetches(List<SecondaryFetch> secondaryFetches) throws BaseException
    //{
    //}

    
    
    
    // TBD...
    private SecondaryFetchBean validateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        SecondaryFetchBean bean = null;
        if(secondaryFetch instanceof SecondaryFetchBean) {
            bean = (SecondaryFetchBean) secondaryFetch;
        }
        // else ???
                
        
        String fetchRequest = bean.getFetchRequest();
        if(fetchRequest == null || fetchRequest.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BadRequestException("Fetch Request has not provided. Cannot proceed with secondary fetch.");
        }
        
        String fetchUrl = bean.getFetchUrl();
        if(fetchUrl == null || fetchUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BadRequestException("Fetch Url is null or empty. Cannot proceed with fetch.");
        }
        
        // TBD: What is feedUrl???
        // In case of fetch, just use fetchUrl????
        // In case of pub, use FeedStoa permalink ?????
        String feedUrl = bean.getFeedUrl();
        if(feedUrl == null || feedUrl.isEmpty()) {
            feedUrl = fetchUrl;
            bean.setFeedUrl(feedUrl);
        }
        // etc...
        
        
        
        // TBD:
        // ...
        
        
        
        return bean;
    }
    
    private SecondaryFetchBean enhanceSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {        
        SecondaryFetchBean bean = null;
        if(secondaryFetch instanceof SecondaryFetchBean) {
            bean = (SecondaryFetchBean) secondaryFetch;
        }
        // else ???
        
        
        // TBD:
        // ...
        
        
        return bean;
    }        
    
    private SecondaryFetchBean reviseSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {        
        SecondaryFetchBean bean = null;
        if(secondaryFetch instanceof SecondaryFetchBean) {
            bean = (SecondaryFetchBean) secondaryFetch;
        }
        // else ???
        
        
        // TBD:
        // ...
        
        
        return bean;
    }        

    // TBD: ....
    private SecondaryFetchBean processSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("TOP: processSecondaryFetch()");
        if(secondaryFetch == null) {
            return null;
        }
        SecondaryFetchBean bean = null;
        if(secondaryFetch instanceof SecondaryFetchBean) {
            bean = (SecondaryFetchBean) secondaryFetch;
        }
        // ...
        
        // TBD: ...
        // Process this secondary alone at the inital saving... ???
        //bean = (SecondaryFetchBean) FetchProcessor.getInstance().processSecondaryFetch(bean, false);
        // .....

        log.finer("BOTTOM: processSecondaryFetch()");
        return bean;
    }


    
}
