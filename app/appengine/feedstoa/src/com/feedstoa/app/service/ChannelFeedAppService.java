package com.feedstoa.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.app.feed.rss.RssFeedProcessor;
import com.feedstoa.app.util.FeedUrlUtil;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedContent;


// Updated.
public class ChannelFeedAppService extends ChannelFeedServiceImpl implements ChannelFeedService
{
    private static final Logger log = Logger.getLogger(ChannelFeedAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    // temporary
    public static final String FEED_GENERATOR_NAME = "Feed Stoa";
    // ?????
    
    
    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ChannelFeedAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ChannelFeed related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        return super.getChannelFeed(guid);
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        return super.getChannelFeed(guid, field);
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        return super.getChannelFeeds(guids);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return super.getAllChannelFeeds();
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllChannelFeedKeys(ordering, offset, count);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }



    @Override
    public String createChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("TOP: createChannelFeed()");

        // TBD:
        channelFeed = validateChannelFeed(channelFeed);
        channelFeed = enhanceChannelFeed(channelFeed);
        channelFeed = processChannelFeed(channelFeed);
        // ...

        // TBD:
//        // Permalink?
//        String permalink = channelFeed.getPermalink();
//        if(permalink == null || permalink.isEmpty()) {
//        	permalink = PermalinkUtil.generateChannelFeedViewPermalink(topUrl, channelFeed);
//        }
      // ...

        String guid = super.createChannelFeed(channelFeed);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createChannelFeed(): guid = " + guid);
        return guid;
    }

    @Override
    public ChannelFeed constructChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("TOP: constructChannelFeed()");

        // TBD:
        channelFeed = validateChannelFeed(channelFeed);
        channelFeed = enhanceChannelFeed(channelFeed);
        channelFeed = processChannelFeed(channelFeed);
        // ...
        
        // TBD:
//        // Permalink?
//        String permalink = channelFeed.getPermalink();
//        if(permalink == null || permalink.isEmpty()) {
//        	permalink = PermalinkUtil.generateChannelFeedViewPermalink(topUrl, channelFeed);
//        }
        // ...
       
                
        channelFeed = super.constructChannelFeed(channelFeed);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructChannelFeed(): channelFeed = " + channelFeed);
        return channelFeed;
    }


    @Override
    public Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("TOP: updateChannelFeed()");

        // TBD:
        channelFeed = validateChannelFeed(channelFeed);
        channelFeed = processChannelFeed(channelFeed);
        Boolean suc = super.updateChannelFeed(channelFeed);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createChannelFeed(): suc = " + suc);
        return suc;
    }
        
    @Override
    public ChannelFeed refreshChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("TOP: refreshChannelFeed()");

        // TBD:
        channelFeed = validateChannelFeed(channelFeed);
        channelFeed = processChannelFeed(channelFeed);
        channelFeed = super.refreshChannelFeed(channelFeed);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshChannelFeed(): channelFeed = " + channelFeed);
        return channelFeed;
    }

    
    
    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        return super.deleteChannelFeed(guid);
    }

    @Override
    public Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        return super.deleteChannelFeed(channelFeed);
    }

    @Override
    public Integer createChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    {
        return super.createChannelFeeds(channelFeeds);
    }

    // TBD
    //@Override
    //public Boolean updateChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    //{
    //}
    
    
    // TBD...
    private ChannelFeedBean validateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        }
        // else ???
                
        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private ChannelFeed enhanceChannelFeed(ChannelFeed channelFeed) throws BaseException
    {        
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        }
        // else ???
        
        // ???
        String generator = bean.getGenerator();
        if(generator == null || generator.isEmpty()) {
            bean.setGenerator(FEED_GENERATOR_NAME);
        }
        // ....

        // ?????
        String feedUrl = bean.getFeedUrl();
        if(feedUrl == null || feedUrl.isEmpty()) {
        	String feedServiceUrl = bean.getFeedServiceUrl();
        	if(feedServiceUrl == null || feedServiceUrl.isEmpty()) {
            	// temporary
        		feedServiceUrl = "http://www.feedstoa.com/";
        		bean.setFeedServiceUrl(feedServiceUrl);
        		// .....
        	}
        	//if(feedServiceUrl != null) {
        		feedUrl = FeedUrlUtil.generateFeedUrl(feedServiceUrl, channelFeed);
        	//}
    		if(feedUrl != null) {
    			bean.setFeedUrl(feedUrl);
    		}
        }
        
        
        // TBD:
        // ...
        
        
        return bean;
    }        

    private ChannelFeed processChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("TOP: processChannelFeed()");
        if(channelFeed == null) {
        	return null;
        }
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        }
        // ...

        // ????
    	FeedContent feedContent = RssFeedProcessor.getInstance().constructFeedContent(channelFeed);
    	if(log.isLoggable(Level.FINE)) log.fine("feedContent = " + feedContent);

    	if(feedContent != null) {
    		String guid = feedContent.getGuid();
    		bean.setFeedContent(guid);
    	}
    	// .....

        log.finer("BOTTOM: processChannelFeed()");
        return bean;
    }



}
