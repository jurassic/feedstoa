package com.feedstoa.app.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.service.AbstractServiceFactory;
import com.feedstoa.af.service.ApiConsumerService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.af.service.ServiceInfoService;
import com.feedstoa.af.service.FiveTenService;

public class AppServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(AppServiceFactory.class.getName());

    private AppServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AppServiceFactoryHolder
    {
        private static final AppServiceFactory INSTANCE = new AppServiceFactory();
    }

    // Singleton method
    public static AppServiceFactory getInstance()
    {
        return AppServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerAppService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserAppService();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestAppService();
    }

    @Override
    public SecondaryFetchService getSecondaryFetchService()
    {
        return new SecondaryFetchAppService();
    }

    @Override
    public ChannelSourceService getChannelSourceService()
    {
        return new ChannelSourceAppService();
    }

    @Override
    public ChannelFeedService getChannelFeedService()
    {
        return new ChannelFeedAppService();
    }

    @Override
    public FeedItemService getFeedItemService()
    {
        return new FeedItemAppService();
    }

    @Override
    public FeedContentService getFeedContentService()
    {
        return new FeedContentAppService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoAppService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenAppService();
    }


}
