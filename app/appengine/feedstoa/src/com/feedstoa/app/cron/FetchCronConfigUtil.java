package com.feedstoa.app.cron;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.config.Config;


public class FetchCronConfigUtil
{
    private static final Logger log = Logger.getLogger(FetchCronConfigUtil.class.getName());
    private static Random sRandom = new Random(new Date().getTime());
    
    private FetchCronConfigUtil() {}

    
    // Initialization-on-demand holder.
    private static final class DefaultFetchCronConfigsHolder
    {
        // TBD:
        private static final String CONFIG_KEY_FETCH_CRON_MODES = "feedstoaapp.fetch.cron.modes";
        // ...

        // TBD:
        // Read the channel list from the DB???
        private static Set<String> sFetchCronModes = new HashSet<String>();
        static {
            String cronModes = Config.getInstance().getString(CONFIG_KEY_FETCH_CRON_MODES, "");
            String[] modeArr = cronModes.split(",\\s*");
            if(modeArr != null && modeArr.length > 0) {
                sFetchCronModes.addAll(Arrays.asList(modeArr));            
                if(FetchCronConfigUtil.log.isLoggable(Level.INFO)) {
                    FetchCronConfigUtil.log.log(Level.INFO, "Fetch cron mode list read from the config: ");
                    for(String d : sFetchCronModes) {
                        FetchCronConfigUtil.log.log(Level.INFO, "-- mode: " + d);
                    }
                }
            } else {
                FetchCronConfigUtil.log.log(Level.INFO, "Fetch cron mode list is not found.");
            }
        }

        static Set<String> getFetchCronModeSet()
        {
            return sFetchCronModes;
        }
    }

    
    public static Set<String> getFetchCronModeSet()
    {
        return DefaultFetchCronConfigsHolder.getFetchCronModeSet();
    }    

}
