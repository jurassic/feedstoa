package com.feedstoa.app.cron;

import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.feedstoa.ws.core.StatusCode;


// Mainly, for ajax calls....
public class FetchCronServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchCronServlet.class.getName());

    
    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        //JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        if(log.isLoggable(Level.INFO)) log.info("FetchCronServlet::doGet() called.");
        
        // TBD:
        // Check the header X-AppEngine-Cron: true
        // ...
        boolean isGaeCron = false;
        String headerXGAECron = req.getHeader("X-AppEngine-Cron");
        if(headerXGAECron != null) {
            isGaeCron = Boolean.parseBoolean(headerXGAECron);
        }
        if(log.isLoggable(Level.FINE)) log.fine("isGaeCron = " + isGaeCron);

        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        if(log.isLoggable(Level.FINE)) {
            log.fine("contextPath = " + contextPath);
            log.fine("servletPath = " + servletPath);
            log.fine("pathInfo = " + pathInfo);
        }

        

        
        // TBD:
        // We need a cron to check the failed fetchRequests...
        // ....

        
        
        // TBD:
        // ...
        // fetch: FetchRequest
        // refresh: ChannelFeed
        String mode = "fetch";    // or, "refresh"
        Integer maxCount = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            if(pathInfo.contains("/")) {
                String[] parts = pathInfo.split("/", 2);
                mode = parts[0];
                try {
                    int cnt = Integer.parseInt(parts[1]);
                    if(cnt > 0) {
                        maxCount = cnt;
                    } else {
                        // ignore 
                    }
                } catch(NumberFormatException e) {
                    // ignore...
                    if(log.isLoggable(Level.INFO)) log.info("Invalid pathInfo = " + pathInfo);
                }
            } else {
                mode = pathInfo;
            }
        }
        if(maxCount == null || maxCount <= 0) {
            maxCount = FetchCronManager.DEFAULT_MAX_COUNT;            
        }
        if(log.isLoggable(Level.INFO)) {
            log.info("mode = " + mode);
            log.info("maxCount = " + maxCount);
        }

        // TBD: validate mode???

        // We process only whitelisted modes....
        Set<String> modeSet = FetchCronConfigUtil.getFetchCronModeSet();
        
        if(modeSet.contains(mode)) {
            int cnt = 0;
            if("fetch".equals(mode)) {
                cnt = FetchCronManager.getInstance().processChannelFeedFetch(maxCount);
            } else if(mode.startsWith("refresh")) {   // refresh1, refresh2, etc....
                cnt = FetchCronManager.getInstance().processChannelFeedRefresh(maxCount);
            } else if("revival".equals(mode)) {
                // "Reprocessing" (for previously failed request), etc...
                cnt = FetchCronManager.getInstance().processChannelFeedRevival(maxCount);
            } else if("retry".equals(mode)) {
                // "Reprocessing" (for requests that are stuck in "processing" status)
                cnt = FetchCronManager.getInstance().processChannelFeedRetry(maxCount);
            } else {
                // Note: Now that we are using a whitelist... 
                // this cannot happen.... (unless there is a typo in the config file...)
    
                // ???
                if(log.isLoggable(Level.WARNING)) log.warning("Unrecognized mode: " + mode);
                //cnt = FetchCronManager.getInstance().processChannelFeedRefresh(maxCount);
            }
            if(log.isLoggable(Level.INFO)) log.info("Processed " + cnt + " fetch requests at " + System.currentTimeMillis());
        } else {
            if(log.isLoggable(Level.INFO)) log.info("Not processing non-whitelisted mode: " + mode);
        }
        
        // Always return 200 ????
        // return 200 only if cnt > 0 ????
        resp.setStatus(StatusCode.OK);
    }

    
    // TBD:
    // depending on mail.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of subject, etc.
    // ...
    
    
    
}
