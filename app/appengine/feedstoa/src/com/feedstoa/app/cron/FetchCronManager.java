package com.feedstoa.app.cron;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.util.ExceptionUtil;
import com.feedstoa.app.fetch.FetchProcessor;
import com.feedstoa.app.helper.FetchRequestHelper;
import com.feedstoa.app.service.ChannelFeedAppService;
import com.feedstoa.app.service.FetchRequestAppService;
import com.feedstoa.app.service.UserAppService;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.core.GUID;


public class FetchCronManager
{
    private static final Logger log = Logger.getLogger(FetchCronManager.class.getName());   

    // TBD...
    public static final int DEFAULT_MAX_COUNT = 10;
    public static final int MAXIMUM_MAX_COUNT = 100;   // ????
    // ...

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private FetchRequestAppService fetchRequestAppService = null;
    private ChannelFeedAppService channelFeedAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private FetchRequestAppService getFetchRequestService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new FetchRequestAppService();
        }
        return fetchRequestAppService;
    }
    private ChannelFeedAppService getChannelFeedService()
    {
        if(channelFeedAppService == null) {
            channelFeedAppService = new ChannelFeedAppService();
        }
        return channelFeedAppService;
    }
    
        // etc. ...

    
    private FetchCronManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class FetchCronManagerHolder
    {
        private static final FetchCronManager INSTANCE = new FetchCronManager();
    }

    // Singleton method
    public static FetchCronManager getInstance()
    {
        return FetchCronManagerHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }
    
    
    // TBD:

    public int processChannelFeedFetch()
    {
        return processChannelFeedFetch(DEFAULT_MAX_COUNT);
    }
    public int processChannelFeedFetch(int maxCount)
    {
        if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;  // ???
        }
        // Get a list of requests to be processed...
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount);
        return processFetchRequests(keys, false, false);
    }

    public int processChannelFeedRefresh()
    {
        return processChannelFeedRefresh(DEFAULT_MAX_COUNT);
    }
    public int processChannelFeedRefresh(int maxCount)
    {
        if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;  // ???
        }
        // Get a list of requests to be processed...
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForRefreshProcessing(maxCount);
        return processFetchRequests(keys, true, false);
    }

    public int processChannelFeedRevival()
    {
        return processChannelFeedRevival(DEFAULT_MAX_COUNT);
    }
    public int processChannelFeedRevival(int maxCount)
    {
        if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;  // ???
        }
        // Get a list of requests to be processed...
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForRevivalProcessing(maxCount);
        return processFetchRequests(keys, true, true);
    }

    public int processChannelFeedRetry()
    {
        return processChannelFeedRetry(DEFAULT_MAX_COUNT);
    }
    public int processChannelFeedRetry(int maxCount)
    {
        if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;  // ???
        }
        // Get a list of requests to be processed...
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForRetryProcessing(maxCount);
        return processFetchRequests(keys, true, true);
    }

//    private int processFetchRequests(List<String> keys)
//    {
//        return processFetchRequests(keys, false);
//    }
//    // refreshing==false means first/initial processing, through fetch cron 
//    // refreshing==true means it's being processed through refresh cron
//    private int processFetchRequests(List<String> keys, boolean refreshing)
//    {
//        return processFetchRequests(keys, refreshing, false);
//    }
    private int processFetchRequests(List<String> keys, boolean refreshing, boolean reviving)
    {
        if(log.isLoggable(Level.FINER)) log.finer("Begin processing: refreshing = " + refreshing + "; reviving = " + reviving);

        // TBD::
        int size = 0;
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            size = keys.size();
            if(log.isLoggable(Level.INFO)) log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    FetchRequest fetchRequest = FetchRequestHelper.getInstance().getFetchRequest(key);
                    if(log.isLoggable(Level.FINE)) log.fine("FetchRequest retrieved for processing: fetchRequest = " + fetchRequest);

                    if(fetchRequest == null) {
                        // Can this happen????
                        if(log.isLoggable(Level.WARNING)) log.warning("Failed to fetch fetchRequest for key = " + key);
                        continue;
                    }
                    
                    // This should not be necessary...
                    String guid = fetchRequest.getGuid();
                    if(guid == null || guid.isEmpty()) {
                        // Can this happen???
                        guid = GUID.generate();
                        ((FetchRequestBean) fetchRequest).setGuid(guid);
                    }

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    // ????

                    
                    // Check the fetch status one more time here...
                    // (after the fetching the list, the fetchStatus of a fetchRequest might have changed, 
                    //              for example, to PROCESSING, by a different cron, etc....)
                    // TBD: What about FAILED and those stuck in PROCESSING for many hours/days... ???
                    Integer status = fetchRequest.getFetchStatus();
                    if(reviving == false) {
                        if(status == null || !(status.equals(FetchStatus.STATUS_SCHEDULED) || status.equals(FetchStatus.STATUS_QUEUED))) {
                            // Something's wrong....
                            // It's like another cron process is processing this at this moment...
                            if(log.isLoggable(Level.WARNING)) log.warning("Incorrect fetchStatus: " + status + " for fetchRequest = " + guid);
                            continue;
                        }
                    } else {
                        if(status == null || !(status.equals(FetchStatus.STATUS_FAILED) || status.equals(FetchStatus.STATUS_PROCESSING))) {
                            // Something's wrong....
                            if(log.isLoggable(Level.WARNING)) log.warning("Incorrect fetchStatus: " + status + " for fetchRequest = " + guid);
                            continue;
                        }
                    }
                    
                    try {
                        // Process... (ChannelFeed/FeedItems are created/updated here. But, fetchRequest will not be saved.)
                        fetchRequest = FetchProcessor.getInstance().processPageFetch(fetchRequest, refreshing);
                        if(log.isLoggable(Level.FINE)) log.fine("FetchRequest processed: fetchRequest = " + fetchRequest);
                    } catch (Exception e) {
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process fetchReuest for key = " + key, e);
                        long nextRefreshingTime = System.currentTimeMillis() + 6 * 3600 * 1000L;  // 6 hours, arbitrary. 
                        ((FetchRequestBean) fetchRequest).setResult("Fetch failed:");
                        ((FetchRequestBean) fetchRequest).setNextRefreshTime(nextRefreshingTime);            
                        ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_FAILED);
                        try {
                            String errorStr = ExceptionUtil.getStackTrace(e);
                            if(errorStr != null) {
                                errorStr = "Fetch failed: " + errorStr;
                                if(errorStr.length() > 500) {
                                    errorStr = errorStr.substring(0, 497) + "...";
                                }
                                ((FetchRequestBean) fetchRequest).setResult(errorStr);
                            }
                        } catch (Exception e1) {
                            // Ignore...
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed while setting the result of fetchReuest for key = " + key, e1);
                        }                        
                    }
                    
                    // ????
                    Integer postFetchStatus = fetchRequest.getFetchStatus();
                    if(postFetchStatus == null || postFetchStatus == FetchStatus.STATUS_PROCESSING) {
                        // ???
                        // This cannot happen.
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Invalid postFetchStatus = " + postFetchStatus);
                        ((FetchRequestBean) fetchRequest).setResult("Invalid postFetchStatus = " + postFetchStatus + ". Marking it as Failed....");
                        ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_FAILED);
                        // ???
                    }
                    
                    // ????
                    // TBD:
                    // Note: Regardless of refreshing==true or false
                    // This is always called from cron (either fetch or refresh)
                    // That means, this is always "update" (because fetchRequest has been saved before) 
                    Boolean suc = getFetchRequestService().updateFetchRequest(fetchRequest);
                    if(log.isLoggable(Level.INFO)) log.info("Fetch request processed and updated: guid = " + guid + "; suc = " + suc);
                    // ....
                    
                    // Increment only if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Retry saving ???
                    // Continue ??? Or, break out of the loop?
                    // ...
                } catch (Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unknown error while saving the updated fetch record for key = " + key, e);
                    // Retry saving ???
                    // Continue ??? Or, break out of the loop?
                    // ...
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        if(log.isLoggable(Level.FINE)) log.fine("End processing: " + counter + " requests out of " + size);
        return counter;
    }
    

}
