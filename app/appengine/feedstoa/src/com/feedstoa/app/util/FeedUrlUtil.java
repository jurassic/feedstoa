package com.feedstoa.app.util;

import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.ChannelFeed;

public class FeedUrlUtil
{
    private static final Logger log = Logger.getLogger(FeedUrlUtil.class.getName());

    // temporary
    public static final String FEED_PATH_PREFIX = "feed/";
    // temporary
    
    private static final int MIN_CODE_LENGTH = 6;
    private static final int MAX_CODE_LENGTH = 20;
    private static final int MIN_TITLE_LENGTH = 12;
    private static final int MAX_TITLE_LENGTH = 30;

    private static Random sRandom = null;
    
    // Static methods only.
    private FeedUrlUtil() {}
    
    
    // Heck!!!
    // temporary
    private static Set<String> WHITELIST = new HashSet<String>();
    static {
    	WHITELIST.add("snipblog");
    	WHITELIST.add("blogstoa");
    	WHITELIST.add("cubbyblog");
    	WHITELIST.add("aeryblog");
    	WHITELIST.add("scribblememo");
    	WHITELIST.add("tellymemo");
    	WHITELIST.add("sidememo");
    	WHITELIST.add("openmemo");
    	WHITELIST.add("openletter");
    	WHITELIST.add("betaparade");
    	WHITELIST.add("betarave");
    	WHITELIST.add("webstoa");
    	// ...
    }


    // TBD: code vs. title??? ...
    public static String generateFeedUrl(String topUrl, ChannelFeed channelFeed)
    {
        if(topUrl == null || channelFeed == null) {
            log.warning("Invalid arguments: topUrl or channelFeed is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();
        
        // [1] hostname, top level path, etc.
        sb.append(topUrl);
        if(!topUrl.endsWith("/")) {
            sb.append("/");
        }
        
        // TBD:
        // Based on feedFormat, choose different prefix?
        // ....
        sb.append(FEED_PATH_PREFIX);
        
        // [2] "Code"
        String code = channelFeed.getChannelCode();
        if(code != null && !code.isEmpty()) {
        	code = code.toLowerCase();    // Use lower case only. ???

        	boolean isWhiteListed = false;
        	if(WHITELIST.contains(code)) {
        		isWhiteListed = true;
        	} else {
        		for(String s : WHITELIST) {
        			if(code.startsWith(s)) {
        				isWhiteListed = true;
        				break;
        			}
        		}
        	}

        	if(isWhiteListed) {
        		// It's probably our services... Gets special treatment...
        		// temporary....  Eventually, should rely on authentication...
        		
        		// Just use the code.
        		sb.append(code);
        		// ...
        	} else {
        		// In general...
	        	String sanCode = sanitizeCodePhrase(code, MAX_CODE_LENGTH);   // ??? Should not be necessary to sanitize code....
	        	if(sanCode.length() < MIN_CODE_LENGTH) {
	        		sanCode += generateRandomString(MIN_CODE_LENGTH - sanCode.length());
	        	}
	            sb.append(sanCode); 
	
	            // Just to be safe, add a random string at the end...
	            String randStr = generateRandomString(2);   // 2 chars.
	            sb.append(randStr);
        	}
        } else {
            // [3] Title
            String title = channelFeed.getTitle();
            String sanTitle = sanitizeTitlePhrase(title, MAX_TITLE_LENGTH); 
            if(sanTitle.length() < MIN_TITLE_LENGTH) {
            	sanTitle += "-" + generateRandomString(MIN_TITLE_LENGTH - sanTitle.length()-1);
            }
            sb.append(sanTitle);

            // Just to be safe, add a random string at the end...
            String randStr = generateRandomString(3);   // 3 chars.
            sb.append("-").append(randStr);
        }
        
        // ...

        // TBD:
        // Add ".xml" or ".rss" at the end ???
        // (maybe, based on channelFeed.getFormat() ... ???)
        

        String feedUrl = sb.toString();
        if(log.isLoggable(Level.INFO)) log.info("Feed URL with length, " + sb.length() + ", created: " + feedUrl);
        return feedUrl;
    }

    
    // TBD: 
    // Combine sanitizeCodePhrase() and sanitizeTitlePhrase() into one function
    // ....

    private static String sanitizeCodePhrase(String phrase, int cutoffLen)
    {
        if(phrase == null) {
            return "";
        }
        phrase = phrase.toLowerCase();  // Use lower case only. ???
        phrase = phrase.replaceAll("[^a-z0-9]", " ");
        phrase = phrase.trim();
        if(phrase.length() == 0) {
            return "";
        }
        
        String[] words = phrase.split("\\s+");
        int totLen = 0;
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<words.length && totLen<=cutoffLen; i++) {
            String word = words[i];
            sb.append(word);
            totLen += word.length();
            if(i<words.length-1 && totLen<=cutoffLen-1) {
                sb.append("/");   // ???
            }
        }

        return sb.toString();
    }

    private static String sanitizeTitlePhrase(String phrase, int cutoffLen)
    {
        if(phrase == null) {
            return "";
        }
        phrase = phrase.toLowerCase();  // Use lower case only. ???
        phrase = phrase.replaceAll("[^a-z0-9]", " ");
        phrase = phrase.trim();
        if(phrase.length() == 0) {
            return "";
        }
        
        String[] words = phrase.split("\\s+");
        int totLen = 0;
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<words.length && totLen<=cutoffLen; i++) {
            String word = words[i];
            sb.append(word);
            totLen += word.length();
            if(i<words.length-1 && totLen<=cutoffLen-1) {
                sb.append("-");  // ???
            }
        }

        return sb.toString();
    }

    private static String generateRandomString(int len)
    {
        if(sRandom == null) {
            sRandom = new Random(new Date().getTime());
        }
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< len; i++) {
            sb.append(sRandom.nextInt(10));
        }
        return sb.toString();
    }

}
