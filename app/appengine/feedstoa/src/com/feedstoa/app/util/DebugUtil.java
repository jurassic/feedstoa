package com.feedstoa.app.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;


// TBD: Make it a singleton???
public class DebugUtil
{
    private static final Logger log = Logger.getLogger(DebugUtil.class.getName());

    private DebugUtil() {}

    // Lazy initialization...
    private static Boolean IS_DEVEL = null;


    // ???
    public static void initialize(String serverInfo)
    {
        if(log.isLoggable(Level.FINE)) log.fine("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            IS_DEVEL = true;
        } else {
            IS_DEVEL = false;
        }
    }

    
    // Returns true if the server is running locally.
    // Note that this value cannot change once initialized
    public static boolean isRunningOnDevel(ServletContext servletContext)
    {
        if(IS_DEVEL == null) {
            String serverInfo = servletContext.getServerInfo();
            IS_DEVEL = isRunningOnDevel(serverInfo);
        }
        return IS_DEVEL;
    }
    public static boolean isRunningOnDevel(String serverInfo)
    {
        if(IS_DEVEL == null) {
            initialize(serverInfo);
        }
        return IS_DEVEL;
    }
    public static Boolean isRunningOnDevel()
    {
        // Could be null
        return IS_DEVEL;
    }

    public static boolean isDevelFeatureEnabled(ServletContext servletContext)
    {
        if(isRunningOnDevel(servletContext) == false) {
            return false;
        } else {    // if(isRunningOnDevel(servletContext) == true) {
            return ConfigUtil.isDevelFeatureEnabled();
        }
    }
    public static boolean isDevelFeatureEnabled(String serverInfo)
    {
        if(isRunningOnDevel(serverInfo) == false) {
            return false;
        } else {    // if(isRunningOnDevel(serverInfo) == true) {
            return ConfigUtil.isDevelFeatureEnabled();
        }
    }
    public static boolean isDevelFeatureEnabled()
    {
        if(isRunningOnDevel() == null || isRunningOnDevel() == false) {
            return false;
        } else {    // if(isRunningOnDevel() == true) {
            return ConfigUtil.isDevelFeatureEnabled();
        }
    }

}
