package com.feedstoa.app.feed.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.common.RefreshStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;



// Note:
// feed package comes before service, which does before helper...
// So, cannot use App Services in this class....
public class ChannelFeedServiceHelper
{
    private static final Logger log = Logger.getLogger(ChannelFeedServiceHelper.class.getName());

    private ChannelFeedServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserServiceImpl userServiceImpl = null;
    private ChannelFeedServiceImpl fetchRequestServiceImpl = null;
    // etc...


    private UserServiceImpl getUserService()
    {
        if(userServiceImpl == null) {
            userServiceImpl = new UserServiceImpl();
        }
        return userServiceImpl;
    }   
    private ChannelFeedServiceImpl getChannelFeedService()
    {
        if(fetchRequestServiceImpl == null) {
            fetchRequestServiceImpl = new ChannelFeedServiceImpl();
        }
        return fetchRequestServiceImpl;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ChannelFeedFinderHolder
    {
        private static final ChannelFeedServiceHelper INSTANCE = new ChannelFeedServiceHelper();
    }

    // Singleton method
    public static ChannelFeedServiceHelper getInstance()
    {
        return ChannelFeedFinderHolder.INSTANCE;
    }

    
    // Key or guid...
    public ChannelFeed getChannelFeed(String guid) throws BaseException 
    {
        ChannelFeed message = null;
//        try {
            message = getChannelFeedService().getChannelFeed(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the channelFeed for guid = " + guid, e);
//        }
        return message;
    }
    

}
