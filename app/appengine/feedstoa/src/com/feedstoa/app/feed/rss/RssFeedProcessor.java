package com.feedstoa.app.feed.rss;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.app.feed.helper.ChannelFeedServiceHelper;
import com.feedstoa.app.feed.helper.FeedContentServiceHelper;
import com.feedstoa.app.feed.helper.FeedItemServiceHelper;
import com.feedstoa.app.feed.util.FeedUtil;
import com.feedstoa.app.util.ConfigUtil;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.util.CommonUtil;


public class RssFeedProcessor
{
    private static final Logger log = Logger.getLogger(RssFeedProcessor.class.getName());   
    
    // temporary
    public static int DEFAULT_FEEDITEMS = 100;
    public static int MAX_FEEDITEMS = 500;      // Is this a reasonable number????
    // ...
    
    
    private RssFeedProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class RssFeedProcessorHolder
    {
        private static final RssFeedProcessor INSTANCE = new RssFeedProcessor();
    }

    // Singleton method
    public static RssFeedProcessor getInstance()
    {
        return RssFeedProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD:
        // ...
    }

    
    
    public FeedContent constructFeedContent(String feedGuid) throws BaseException
    {
    	FeedContent feedContent = generateRssFeedContent(feedGuid);
    	if(feedContent != null) {
    		feedContent = FeedContentServiceHelper.getInstance().constructFeedContent(feedContent);
    	}
    	return feedContent;
    }
    public FeedContent constructFeedContent(ChannelFeed channelFeed) throws BaseException
    {
    	FeedContent feedContent = generateRssFeedContent(channelFeed);
    	if(feedContent != null) {
    		feedContent = FeedContentServiceHelper.getInstance().constructFeedContent(feedContent);
    	}
    	return feedContent;
    }
    public FeedContent constructFeedContent(List<FeedItem> feedItems) throws BaseException
    {
    	FeedContent feedContent = generateRssFeedContent(feedItems);
    	if(feedContent != null) {
    		feedContent = FeedContentServiceHelper.getInstance().constructFeedContent(feedContent);
    	}
    	return feedContent;
    }
    public FeedContent constructFeedContent(FeedItem feedItem) throws BaseException
    {
    	FeedContent feedContent = generateRssFeedContent(feedItem);
    	if(feedContent != null) {
    		feedContent = FeedContentServiceHelper.getInstance().constructFeedContent(feedContent);
    	}
    	return feedContent;
    }
    public FeedContent constructFeedContent(ChannelFeed channelFeed, List<FeedItem> feedItems) throws BaseException
    {
    	FeedContent feedContent = generateRssFeedContent(channelFeed, feedItems);
    	if(feedContent != null) {
    		feedContent = FeedContentServiceHelper.getInstance().constructFeedContent(feedContent);
    	}
    	return feedContent;
    }
    
    
    public FeedContent generateRssFeedContent(String feedGuid) throws BaseException
    {
    	// TBD: Find channelFeed with feedGuid....
    	ChannelFeed channelFeed = ChannelFeedServiceHelper.getInstance().getChannelFeed(feedGuid);
    	// Could be null...
    	// .....
    	return generateRssFeedContent(channelFeed, null);
    }
    public FeedContent generateRssFeedContent(ChannelFeed channelFeed) throws BaseException
    {
    	return generateRssFeedContent(channelFeed, null);
    }
    public FeedContent generateRssFeedContent(List<FeedItem> feedItems) throws BaseException
    {
    	return generateRssFeedContent(null, feedItems);
    }
    public FeedContent generateRssFeedContent(FeedItem feedItem) throws BaseException
    {
    	if(feedItem == null) {
            log.warning("feedItem is null.");
            throw new BadRequestException("feedItem is null.");
    	}
    	
    	String feedGuid = feedItem.getChannelFeed();
    	if(feedGuid == null) {
            log.warning("feedGuid is null.");
            throw new BadRequestException("feedGuid is null.");
    	}

    	// This is somewhat dubious....
    	// But, what we do is essentially, 
    	//    appending the feedItem to a list of existing feedItems associated with the same channelFeed...
    	// If only a single feedItem (with channelFeed) needs to be used, use List<FeedItem> type arg...
    	ChannelFeed channelFeed = ChannelFeedServiceHelper.getInstance().getChannelFeed(feedGuid);
    	if(channelFeed == null) {
            log.warning("channelFeed is null. feedGuid = " + feedGuid);
            throw new BadRequestException("channelFeed is null. feedGuid = " + feedGuid);
    	}
    	Integer maxCount = channelFeed.getMaxItemCount();
    	if(maxCount == null || maxCount <= 0) {   // Is zero allowed????
    		maxCount = DEFAULT_FEEDITEMS;
    	} else if(maxCount > MAX_FEEDITEMS) {
    		maxCount = MAX_FEEDITEMS;
    	}
    	List<FeedItem> feedItems = FeedItemServiceHelper.getInstance().findFeedItemsForChannelFeed(feedGuid, maxCount);
    	//if(feedItems == null) {
    	//	feedItems = new ArrayList<FeedItem>();
    	//}
        //feedItems.add(feedItem);   // feedItem becomes the last element... Not good.
    	
    	// feedItem needs to be the first element!!!
    	List<FeedItem> feedItemList = new ArrayList<FeedItem>();
    	feedItemList.add(feedItem);    
        if(feedItems != null) {
            // Note that feedItems may contain an older version of feedItem (in case this method is called during update....
            // TBD: Combine this with the logic below related to enforceUniqueItems ???
            // Otherwise, it is two repetitions of copying the feedItems when enforceUniqueItems==true....
            String id = feedItem.getId();
            if(id == null || id.isEmpty()) {
                feedItemList.addAll(feedItems);                
            } else {
                // feedGuid is the same for feedItem and all FeedItems in feedItems...
                for(FeedItem fi : feedItems) {
                    String fiId = fi.getId();
                    if(! id.equals(fiId)) {
                        feedItemList.add(fi);
                    } else {
                        // Skip
                        if(log.isLoggable(Level.INFO)) log.info("FeedItem with the same id already exists. Skipping. id = " + id);
                    }
                }
            }
        }
        return generateRssFeedContent(channelFeed, feedItemList);
        //return generateRssFeedContent(channelFeed, feedItemList, true);  // -> This makes the above loop unnecessary.. just use feedItemList.addAll(feedItems)... 
    }
    public FeedContent generateRssFeedContent(ChannelFeed channelFeed, List<FeedItem> feedItems) throws BaseException
    {
        // enforceUniqueItems==false by default, for now.....
        //boolean enforceUniqueItems = false;
        boolean enforceUniqueItems = ConfigUtil.isRssFeedItemListDedups();
        return generateRssFeedContent(channelFeed, feedItems, enforceUniqueItems);
    }
    public FeedContent generateRssFeedContent(ChannelFeed channelFeed, List<FeedItem> feedItems, boolean enforceUniqueItems) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: generateRssFeedContent() called with channelFeed = " + channelFeed + "; enforceUniqueItems = " + enforceUniqueItems);
        //if(channelFeed == null && (feedItems == null || feedItems.isEmpty() || (feedItems.size() == 1 && feedItems.get(0) == null))) {
        if(channelFeed == null && (feedItems == null || feedItems.isEmpty())) {
            // ????
            log.warning("channelFeed/feedItems is null.");
            throw new BadRequestException("channelFeed/feedItems is null.");
        }
        
        // Somewhat convoluted design....
        String feedGuid = null;
        if(channelFeed != null) {
        	feedGuid = channelFeed.getGuid();
        }
        if(feedItems != null && !feedItems.isEmpty()) {
        	// Validate
        	// ... Null feedGuid is oK, in the current implementation.... Inconsistency is the problem..
        	for(FeedItem item : feedItems) {
        		if(feedGuid == null) {
        			feedGuid = item.getChannelFeed();  // Only for the first item with non-null channelfeed, if any... 
        		} else {
        			String itemFeedGuid = item.getChannelFeed();
        			if(itemFeedGuid != null && !itemFeedGuid.equals(feedGuid)) {
        				// Error
        	            log.warning("feedItems.channelFeed are inconsistent. feedGuid = " + feedGuid + "; itemFeedGuid = " + itemFeedGuid);
        	            throw new BadRequestException("feedItems.channelFeed are inconsistent. feedGuid = " + feedGuid + "; itemFeedGuid = " + itemFeedGuid);
        			}
        		}
        	}
        }
        
        if(channelFeed == null) {
        	if(feedGuid == null) {
        		// This is an error. Cannot proceed...
                log.warning("channelFeed/feedGuid is null.");
                throw new BadRequestException("channelFeed/feedGuid is null.");
        	}
        	// Find channel feed...
        	// This non-null feedGuid is from the feedItems...
        	channelFeed = ChannelFeedServiceHelper.getInstance().getChannelFeed(feedGuid);
            if(channelFeed == null) {
        		// This is an error. Cannot proceed...
                log.warning("channelFeed is null. feedGuid = " + feedGuid);
                throw new BadRequestException("channelFeed is null. feedGuid = " + feedGuid);
            }
        }
        
        if(feedItems == null) {  // What about empty list???  --> Empty list means do not include feed items in the feed content generation...
        	// Find all items for the given feedGuid...
        	Integer maxCount = channelFeed.getMaxItemCount();
        	if(maxCount == null || maxCount <= 0) {   // Is zero allowed????
        		maxCount = DEFAULT_FEEDITEMS;
        	} else if(maxCount > MAX_FEEDITEMS) {
        		maxCount = MAX_FEEDITEMS;
        	}
        	feedItems = FeedItemServiceHelper.getInstance().findFeedItemsForChannelFeed(feedGuid, maxCount);
        	// ...
        } else {
        	// Note that the list may not be the complete/whole list of feed items for the given channelFeed....
        }
        
        if(feedItems == null) {
            // Null means error at this point...
        	// Should have been at least an empty list...
            log.warning("feedItems is null.");
            throw new BadRequestException("feedItems is null.");
        }
        
        // At this point,
        // Neither channelFeed nor feedItems can be null...
        // ....

        
        // TBD: 
        // We have to include only one per feedItem with the same channelFeed and id...
        // Commented out, for now....
        // TBD: The list should be reverse chronologically sorted for this to work... (which it is....)
        // ....
        if(enforceUniqueItems) {
            List<FeedItem> feedItemList = new ArrayList<FeedItem>();
            Map<String, FeedItem> feedItemMap = new HashMap<String, FeedItem>();  // Set suffices, but using Map for logging purposes...
            for(FeedItem fi : feedItems) {
                String key = fi.getId();     // Since channelFeed.guid is the same for all feedItems in the list, we can ignore that part.
                if(feedItemMap.containsKey(key)) {
                    // skip
                    if(log.isLoggable(Level.WARNING)) {
                        log.warning("FeedItem, " + fi.getGuid() + ", already exists in the list with the same channelFeed = " + channelFeed + " and id = " + key);
                        if(log.isLoggable(Level.INFO)) {
                            log.info("Existing/most recent feedItem = " + feedItemMap.get(key));
                        }
                    }
                } else {
                    feedItemList.add(fi);
                    feedItemMap.put(key, fi);
                }
            }
            feedItems = feedItemList;
        }
        // ....
        
        
        // TBD:
        String format = channelFeed.getFeedFormat();
        // etc...
        // ...

        // TBD:
        FeedContentBean feedContent = new FeedContentBean(); 
        feedContent.setChannelFeed(feedGuid);
        feedContent.setFeedFormat(format);
        feedContent.setFeedUrl(channelFeed.getFeedUrl());              // FeedURL is the "PK" when serving the XML content....
        feedContent.setChannelSource(channelFeed.getChannelSource());
        // ...
        long now = System.currentTimeMillis();
        feedContent.setLastBuildTime(now);
        String buildDate = FeedUtil.formatDate(now);
        feedContent.setLastBuildDate(buildDate);
        // ....
        // etc.

        String content = RssFeedUtil.generateRssContent(channelFeed, feedItems);
        if(log.isLoggable(Level.FINE)) log.fine(">>>>> RSS content = " + content);

        if(content != null) {
	        feedContent.setContent(content);
	        try {
				String hash = CommonUtil.SHA1(content);
				feedContent.setContentHash(hash);
			} catch (Exception e) {
				// ignore
				if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Failed to create hash.", e);
			}
        }
	    // ...

        log.finer("END: generateRssFeedContent()");
        return feedContent;
    }
    


}
