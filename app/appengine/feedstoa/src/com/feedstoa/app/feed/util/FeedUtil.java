package com.feedstoa.app.feed.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;


// Primarily to shield third party Feed libraries (such as Rome)
public class FeedUtil
{
    private static final Logger log = Logger.getLogger(FeedUtil.class.getName());

    // Static methods only.
    private FeedUtil() {}
    
    
    
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
        TimeZone timeZone = TimeZone.getDefault();
        //TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
        dateFormat.setTimeZone(timeZone);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static Date parseDate(String strDate)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Date date = null;
        try {
            //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
            //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
            TimeZone timeZone = TimeZone.getDefault();
            //TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
            dateFormat.setTimeZone(timeZone);
            date = dateFormat.parse(strDate);
        } catch (ParseException e1) {
            log.log(Level.WARNING, "Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            log.log(Level.WARNING, "Failed to parse Date: strDate = " + strDate, e2);
        }
        return date;
    }


    
    ///////////////////////////////////////////////////////////////////
    // TBD:
    // This added layer is to insulate third party libraries...
    ///////////////////////////////////////////////////////////////////
    
    

    // TBD
    // ....
    

}
