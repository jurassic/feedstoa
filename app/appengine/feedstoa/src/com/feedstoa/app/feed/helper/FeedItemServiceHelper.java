package com.feedstoa.app.feed.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.impl.FeedItemServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.common.RefreshStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FeedItem;



// Note:
// feed package comes before service, which does before helper...
// So, cannot use App Services in this class....
public class FeedItemServiceHelper
{
    private static final Logger log = Logger.getLogger(FeedItemServiceHelper.class.getName());

    private FeedItemServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserServiceImpl userServiceImpl = null;
    private FeedItemServiceImpl fetchRequestServiceImpl = null;
    // etc...


    private UserServiceImpl getUserService()
    {
        if(userServiceImpl == null) {
            userServiceImpl = new UserServiceImpl();
        }
        return userServiceImpl;
    }   
    private FeedItemServiceImpl getFeedItemService()
    {
        if(fetchRequestServiceImpl == null) {
            fetchRequestServiceImpl = new FeedItemServiceImpl();
        }
        return fetchRequestServiceImpl;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FeedItemFinderHolder
    {
        private static final FeedItemServiceHelper INSTANCE = new FeedItemServiceHelper();
    }

    // Singleton method
    public static FeedItemServiceHelper getInstance()
    {
        return FeedItemFinderHolder.INSTANCE;
    }

    
    // Key or guid...
    public FeedItem getFeedItem(String guid) throws BaseException 
    {
        FeedItem message = null;
//        try {
            message = getFeedItemService().getFeedItem(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the feedItem for guid = " + guid, e);
//        }
        return message;
    }
    
    public List<FeedItem> findFeedItemsForChannelFeed(String feedGuid) throws BaseException
    {
    	return findFeedItemsForChannelFeed(feedGuid, null);
    }    
    public List<FeedItem> findFeedItemsForChannelFeed(String feedGuid, Integer maxCount) throws BaseException
    {
        // TBD: 
        // We have to include only one per feedItem with the same channelFeed and id...
        // ---> Maybe this logic should be put in a higher level (e.g., in the caller)
        // ...
        List<FeedItem> channelFeeds = null;
        String filter = "channelFeed == '" + feedGuid + "'";
        // TBD: Status, etc....
        // ....
        String ordering = "createdTime desc";   // Note: For uniqueness, the list needs to be reverse chronologically sorted...
//        try {
            channelFeeds = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }

        return channelFeeds;
    }


}
