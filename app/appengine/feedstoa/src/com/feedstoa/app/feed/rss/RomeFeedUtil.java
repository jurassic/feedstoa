package com.feedstoa.app.feed.rss;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.app.feed.util.FeedUtil;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.RssItem;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.feed.synd.SyndImage;
import com.sun.syndication.feed.synd.SyndImageImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;


public class RomeFeedUtil
{
    private static final Logger log = Logger.getLogger(RomeFeedUtil.class.getName());

    // Static methods only.
    private RomeFeedUtil() {}

   
    
    ///////////////////////////////////////////////////////////////////
    // TBD:
    // This added layer is to insulate third party libraries...
    ///////////////////////////////////////////////////////////////////
    

    public static SyndEntry convertToSyndEntry(RssItem rssItem)
    {
    	if(rssItem == null) {
    		return null;
    	}

    	SyndEntryImpl syndEntry = new SyndEntryImpl();

    	String title = rssItem.getTitle();
        if(title != null) {
        	syndEntry.setTitle(title);
        }
        String link = rssItem.getLink();
        if(link != null) {
          	syndEntry.setLink(link);
        }
        String description = rssItem.getDescription();
        if(description != null) {
        	SyndContent syndContent = new SyndContentImpl();
        	syndContent.setValue(description);             // ?????
        	syndEntry.setDescription(syndContent);
        }
        String author = rssItem.getAuthor();
        if(author != null) {
        	syndEntry.setAuthor(author);
        }
        List<CategoryStruct> rssCategories = rssItem.getCategory();
        if(rssCategories != null && !rssCategories.isEmpty()) {
        	List<SyndCategory> categories = new ArrayList<SyndCategory>();
        	for(CategoryStruct a : rssCategories) {
        		SyndCategoryImpl r = new SyndCategoryImpl();
        		String domain = a.getDomain();
        		if(domain != null) {
        			r.setTaxonomyUri(domain);  // ????
        		}
        		String label = a.getLabel();
        		if(label != null) {
        			r.setName(label);          // ????
        		}
        		categories.add(r);
        	}
        	syndEntry.setCategories(categories);
        }
//        String comments = rssItem.getComments();
//        if(comments != null) {
//        	syndEntry.setComments(comments);    // ????
//        }
        EnclosureStruct enclosure = rssItem.getEnclosure();
        if(enclosure != null) {
        	SyndEnclosure syndEnclosure = new SyndEnclosureImpl();
        	String url = enclosure.getUrl();
        	if(url != null) {
        		syndEnclosure.setUrl(url);
        	}
        	String type = enclosure.getType();
        	if(type != null) {
        		syndEnclosure.setType(type);
        	}
        	Integer length = enclosure.getLength();
        	if(length != null) {
        		syndEnclosure.setLength((long) length);
        	}
        	List<SyndEnclosure> syndEnclosures = new ArrayList<SyndEnclosure>();
        	syndEnclosures.add(syndEnclosure);
        	syndEntry.setEnclosures(syndEnclosures);
        }
//        String guid = rssItem.getGuid();
//        if(guid != null) {
//        	syndEntry.setGuid(guid);    // ???
//        }
        String pubDate = rssItem.getPubDate();
        if(pubDate != null) {
        	Date publishedDate = FeedUtil.parseDate(pubDate);
        	if(publishedDate != null) {
        		syndEntry.setPublishedDate(publishedDate);
        	}
        }
        // ????
//        RssSourceStruct source = rssItem.getSource();
//        if(source != null) {
//        	SyndFeed syndFeed = new SyndFeedImpl();
//        	// ....
//        	syndEntry.setSource(syndFeed);
//        }
    	
    	return syndEntry;
    }

    public static SyndFeed convertToSyndFeed(RssChannel rssChannel)
    {
    	if(rssChannel == null) {
    		return null;
    	}

    	SyndFeedImpl syndFeed = new SyndFeedImpl();
    	syndFeed.setFeedType("rss_2.0");   // ????????

    	String title = rssChannel.getTitle();
        if(title != null) {
        	syndFeed.setTitle(title);
        }
        String link = rssChannel.getLink();
        if(link != null) {
          	syndFeed.setLink(link);
        }
        String description = rssChannel.getDescription();
        if(description != null) {
        	syndFeed.setDescription(description);
        }
        String language = rssChannel.getLanguage();
        if(language != null) {
        	syndFeed.setLanguage(language);
        }
        String copyright = rssChannel.getCopyright();
        if(copyright != null) {
        	syndFeed.setCopyright(copyright);
        }
        String managingEditor = rssChannel.getManagingEditor();
        if(managingEditor != null) {
        	syndFeed.setAuthor(managingEditor);   // ???
        }
        String webMaster = rssChannel.getWebMaster();
        if(webMaster != null) {
        	List<String> contributors = new ArrayList<String>();
        	contributors.add(webMaster);            // ???
        	syndFeed.setContributors(contributors);
        }
        String pubDate = rssChannel.getPubDate();
        if(pubDate != null) {
        	Date publishedDate = FeedUtil.parseDate(pubDate);
        	if(publishedDate != null) {
        		syndFeed.setPublishedDate(publishedDate);
        	}
        }
//        String lastBuildDate = rssChannel.getLastBuildDate();
//        if(lastBuildDate != null) {
//        	syndFeed.setLastBuildDate(lastBuildDate);   // ????
//        }
        List<CategoryStruct> rssCategories = rssChannel.getCategory();
        if(rssCategories != null && !rssCategories.isEmpty()) {
        	List<SyndCategory> categories = new ArrayList<SyndCategory>();
        	for(CategoryStruct a : rssCategories) {
        		SyndCategoryImpl r = new SyndCategoryImpl();
        		String domain = a.getDomain();
        		if(domain != null) {
        			r.setTaxonomyUri(domain);  // ????
        		}
        		String label = a.getLabel();
        		if(label != null) {
        			r.setName(label);          // ????
        		}
        		categories.add(r);
        	}
        	syndFeed.setCategories(categories);
        }
        // ????
//        String generator = rssChannel.getGenerator();
//        if(generator != null) {
//        	syndFeed.setGenerator(generator);    // ???
//        }
//        String docs = rssChannel.getDocs();
//        if(docs != null) {
//        	syndFeed.setDocs(docs);     // ???
//        }
//        RssCloudStruct cloud = rssChannel.getCloud();
//        if(cloud != null) {
//        	syndFeed.setCloud(cloud);   // ???
//        }
//        Integer ttl = rssChannel.getTtl();
//        if(ttl != null) {
//        	syndFeed.setTtl(ttl);
//        }
        ImageStruct image = rssChannel.getImage();
        if(image != null) {
        	SyndImage syndImage = new SyndImageImpl();
        	String url = image.getUrl();
        	String imgTitle = image.getTitle();
        	if(url != null && imgTitle != null) {    // ????
        		syndImage.setUrl(url);
        		syndImage.setTitle(imgTitle);

        		String imgLink = image.getLink();
            	if(imgLink != null) {
            		syndImage.setLink(imgLink);
            	}
            	syndFeed.setImage(syndImage);   // ??
        	} else {
        		// ????
        	}
        }
//        String rating = rssChannel.getRating();
//        if(rating != null) {
//        	syndFeed.setRating(rating);   // ???
//        }
//        RssTextInputStruct textInput = rssChannel.getTextInput();
//        if(textInput != null) {
//        	syndFeed.setTextInput(textInput);   // ???
//        }
//        List<RssDayStruct> skipDays = rssChannel.getSkipDays();
//        if(skipDays != null) {
//        	syndFeed.setSkipDays(skipDays);     // ???
//        }

        List<RssItem> rssItems = rssChannel.getItem();
        if(rssItems != null) {
        	List<SyndEntry> syndEntries = new ArrayList<SyndEntry>();
        	for(RssItem item : rssItems) {
        		syndEntries.add(convertToSyndEntry(item));
        	}
        	syndFeed.setEntries(syndEntries);
        }
    	
    	return syndFeed;
    }
   

    
    public static SyndEntry convertToSyndEntry(FeedItem feedItem)
    {
    	RssItem rssItem = RssFeedUtil.convertToRssItem(feedItem);
    	return convertToSyndEntry(rssItem);
    }

    public static SyndFeed convertToSyndFeed(ChannelFeed channelFeed)
    {
    	return convertToSyndFeed(channelFeed, null);
    }
    public static SyndFeed convertToSyndFeed(ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
    	RssChannel rssChannel = RssFeedUtil.convertToRssChannel(channelFeed, feedItems);
    	return convertToSyndFeed(rssChannel);
    }

    

    // channelFeed cannot be null....
    public static String generateRssContent(ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
    	if(channelFeed == null) {
    		return null;  // ???
    	}
        
        String content = null;
        try {
            SyndFeed feed = RomeFeedUtil.convertToSyndFeed(channelFeed, feedItems);
            // feed.setFeedType("rss_2.0");   // ????????

            Writer writer = new StringWriter();
            SyndFeedOutput feedOutput = new SyndFeedOutput();
			feedOutput.output(feed, writer);
	        // ????
	        writer.close();
	        
	        // ???
	        content = writer.toString();
	        
		} catch (IOException e) {
			log.log(Level.WARNING, "Could not generate RSS feed.", e);
		} catch (FeedException e) {
			log.log(Level.WARNING, "Error while generating RSS feed.", e);
		}
        
        return content;
    }


    // TBD
    // ....

    

    
}
