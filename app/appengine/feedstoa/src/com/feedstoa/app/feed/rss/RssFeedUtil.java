package com.feedstoa.app.feed.rss;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.RssChannelBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;


public class RssFeedUtil
{
    private static final Logger log = Logger.getLogger(RssFeedUtil.class.getName());

    // Static methods only.
    private RssFeedUtil() {}


   
    
    ///////////////////////////////////////////////////////////////////
    // TBD:
    // This added layer is to insulate third party libraries...
    ///////////////////////////////////////////////////////////////////
    

    // Delegate to Rome...
    public static String generateRssContent(ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
    	return RomeFeedUtil.generateRssContent(channelFeed, feedItems);
    }


    public static RssItem convertToRssItem(FeedItem feedItem)
    {
    	if(feedItem == null) {
    		return null;
    	}

    	RssItemBean rssItem = new RssItemBean();

    	String title = feedItem.getTitle();
        if(title != null) {
        	rssItem.setTitle(title);
        }
//        List<UriStruct> atomLinks = feedItem.getLink();
//        if(atomLinks != null && !atomLinks.isEmpty()) {
//            String link = atomLinks.get(0).getHref();   // First element only.
//            if(link != null) {
//            	rssItem.setLink(link);
//            }        	
//        }
        UriStruct atomLink = feedItem.getLink();
        if(atomLink != null) {
        	String href = atomLink.getHref();
        	if(href != null) {
        		rssItem.setLink(href);
        	}
        }
        String description = feedItem.getContent();
        if(description == null || description.isEmpty()) {
            description = feedItem.getSummary();  // ???
        }
        if(description != null) {
        	rssItem.setDescription(description);
        }
        UserStruct atomAuthor = feedItem.getAuthor();
        if(atomAuthor != null) {
        	String managingEditor = atomAuthor.getEmail();
        	if(managingEditor == null) {
        		managingEditor = "";      // ???
        	}
        	String name = atomAuthor.getName();
        	if(name != null && !name.isEmpty()) {
        		managingEditor += " (" + name + ")";
        	}
        	rssItem.setAuthor(managingEditor);
        }
        List<CategoryStruct> atomCategories = feedItem.getCategory();
        if(atomCategories != null && !atomCategories.isEmpty()) {
        	List<CategoryStruct> rssCategories = new ArrayList<CategoryStruct>();
        	for(CategoryStruct a : atomCategories) {
        		CategoryStructBean r = new CategoryStructBean();
        		String domain = a.getDomain();
        		if(domain != null) {
        			r.setDomain(domain);
        		}
        		String label = a.getLabel();
        		if(label != null) {
        			r.setLabel(label);
        		}
        		rssCategories.add(r);
        	}
        	rssItem.setCategory(rssCategories);
        }
        String comments = feedItem.getComments();
        if(comments != null) {
        	rssItem.setComments(comments);
        }
        EnclosureStruct enclosure = feedItem.getEnclosure();
        if(enclosure != null) {
        	rssItem.setEnclosure(enclosure);
        }
        String id = feedItem.getId();     // ???
        if(id != null) {
        	rssItem.setGuid(id);
        }
        String pubDate = feedItem.getPubDate();
        if(pubDate != null) {
        	rssItem.setPubDate(pubDate);
        }
        UriStruct source = feedItem.getSource();
        if(source != null) {
        	rssItem.setSource(source);
        }
    	
    	return rssItem;
    }

    public static RssChannel convertToRssChannel(ChannelFeed channelFeed)
    {
    	return convertToRssChannel(channelFeed, null);
    }
    public static RssChannel convertToRssChannel(ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
    	if(channelFeed == null) {
    		return null;
    	}

    	RssChannelBean rssChannel = new RssChannelBean();

    	String title = channelFeed.getTitle();
        if(title != null) {
        	rssChannel.setTitle(title);
        }
//        List<UriStruct> atomLinks = channelFeed.getLink();
//        if(atomLinks != null && !atomLinks.isEmpty()) {
//            String link = atomLinks.get(0).getHref();   // First element only.
//            if(link != null) {
//            	rssChannel.setLink(link);
//            }        	
//        }
        UriStruct atomLink = channelFeed.getLink();
        if(atomLink != null) {
        	String href = atomLink.getHref();
        	if(href != null) {
        		rssChannel.setLink(href);
        	}
        }
        String description = channelFeed.getDescription();
        if(description != null) {
        	rssChannel.setDescription(description);
        }
        String language = channelFeed.getLanguage();
        if(language != null) {
        	rssChannel.setLanguage(language);
        }
        String copyright = channelFeed.getCopyright();
        if(copyright != null) {
        	rssChannel.setCopyright(copyright);
        }
        UserStruct atomManagingEditor = channelFeed.getManagingEditor();
        if(atomManagingEditor != null) {
        	String managingEditor = atomManagingEditor.getEmail();
        	String name = atomManagingEditor.getName();
        	if(name != null && !name.isEmpty()) {
        		managingEditor += "(" + name + ")";
        	}
        	rssChannel.setManagingEditor(managingEditor);
        }
        UserStruct atomWebMaster = channelFeed.getWebMaster();
        if(atomWebMaster != null) {
        	String webMaster = atomWebMaster.getEmail();
        	String name = atomWebMaster.getName();
        	if(name != null && !name.isEmpty()) {
        		webMaster += "(" + name + ")";
        	}
        	rssChannel.setWebMaster(webMaster);
        }
        String pubDate = channelFeed.getPubDate();
        if(pubDate != null) {
        	rssChannel.setPubDate(pubDate);
        }
        String lastBuildDate = channelFeed.getLastBuildDate();
        if(lastBuildDate != null) {
        	rssChannel.setLastBuildDate(lastBuildDate);
        }
        List<CategoryStruct> atomCategories = channelFeed.getCategory();
        if(atomCategories != null && !atomCategories.isEmpty()) {
        	rssChannel.setCategory(atomCategories);
        }
        String generator = channelFeed.getGenerator();
        if(generator != null) {
        	rssChannel.setGenerator(generator);
        }
        String docs = channelFeed.getDocs();
        if(docs != null) {
        	rssChannel.setDocs(docs);
        }
        CloudStruct cloud = channelFeed.getCloud();
        if(cloud != null) {
        	rssChannel.setCloud(cloud);
        }
        Integer ttl = channelFeed.getTtl();
        if(ttl != null) {
        	rssChannel.setTtl(ttl);
        }
        ImageStruct image = channelFeed.getLogo();
        if(image != null) {
        	rssChannel.setImage(image);
        }
        String rating = channelFeed.getRating();
        if(rating != null) {
        	rssChannel.setRating(rating);
        }
        TextInputStruct textInput = channelFeed.getTextInput();
        if(textInput != null) {
        	rssChannel.setTextInput(textInput);
        }
        List<String> skipDays = channelFeed.getSkipDays();
        if(skipDays != null) {
        	rssChannel.setSkipDays(skipDays);
        }
        List<Integer> skipHours = channelFeed.getSkipHours();
        if(skipHours != null) {
        	rssChannel.setSkipHours(skipHours);
        }

        if(feedItems != null) {
            // TBD: 
            // We have to include only one per feedItem with the same channelFeed and id...
            // ---> Maybe, this should be done by the caller ????
        	List<RssItem> rssItems = new ArrayList<RssItem>();
        	for(FeedItem item : feedItems) {
        		rssItems.add(convertToRssItem(item));
        	}
        	rssChannel.setItem(rssItems);
        }
    	
    	return rssChannel;
    }
    

    // TBD
    // ....

    
    
}
