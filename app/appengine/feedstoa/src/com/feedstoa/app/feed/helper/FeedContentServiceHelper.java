package com.feedstoa.app.feed.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.impl.FeedContentServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.common.RefreshStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FeedContent;



// Note:
// feed package comes before service, which does before helper...
// So, cannot use App Services in this class....
public class FeedContentServiceHelper
{
    private static final Logger log = Logger.getLogger(FeedContentServiceHelper.class.getName());

    private FeedContentServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserServiceImpl userServiceImpl = null;
    private FeedContentServiceImpl fetchRequestServiceImpl = null;
    // etc...


    private UserServiceImpl getUserService()
    {
        if(userServiceImpl == null) {
            userServiceImpl = new UserServiceImpl();
        }
        return userServiceImpl;
    }   
    private FeedContentServiceImpl getFeedContentService()
    {
        if(fetchRequestServiceImpl == null) {
            fetchRequestServiceImpl = new FeedContentServiceImpl();
        }
        return fetchRequestServiceImpl;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FeedContentFinderHolder
    {
        private static final FeedContentServiceHelper INSTANCE = new FeedContentServiceHelper();
    }

    // Singleton method
    public static FeedContentServiceHelper getInstance()
    {
        return FeedContentFinderHolder.INSTANCE;
    }


    // Key or guid...
    public FeedContent getFeedContent(String guid) throws BaseException 
    {
        FeedContent message = null;
//        try {
            message = getFeedContentService().getFeedContent(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the feedContent for guid = " + guid, e);
//        }
        return message;
    }
    
    
    public List<FeedContent> findFeedContentsForChannelFeed(String feedGuid) throws BaseException
    {
        List<FeedContent> channelFeeds = null;
        String filter = "channelFeed == '" + feedGuid + "'";
        // TBD: BuildDate, etc...
        // ....
        String ordering = "createdTime desc";   // ???
//        try {
            channelFeeds = getFeedContentService().findFeedContents(filter, ordering, null, null, null, null, null, null);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedContent list.", e);
//        }

        return channelFeeds;
    }

    // TBD: Get the most recent FeedConent for feedGuid...
    // ...
    
    
    
    public FeedContent constructFeedContent(FeedContent feedContent) throws BaseException
    {
//    	try {
			feedContent = getFeedContentService().constructFeedContent(feedContent);
//		} catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to save feedContent.", e);
//		}
    	return feedContent;
    }

    
}
