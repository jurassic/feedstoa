package com.feedstoa.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.app.service.FetchRequestAppService;
import com.feedstoa.app.service.UserAppService;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FetchRequest;


public class FetchRequestHelper
{
    private static final Logger log = Logger.getLogger(FetchRequestHelper.class.getName());

    private FetchRequestHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private FetchRequestAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private FetchRequestAppService getFetchRequestService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new FetchRequestAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FetchRequestHelperHolder
    {
        private static final FetchRequestHelper INSTANCE = new FetchRequestHelper();
    }

    // Singleton method
    public static FetchRequestHelper getInstance()
    {
        return FetchRequestHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public FetchRequest getFetchRequest(String guid) 
    {
        FetchRequest message = null;
        try {
            message = getFetchRequestService().getFetchRequest(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
//    // ????
//    public FetchRequest getFetchRequestByKey(String key) 
//    {
//        FetchRequest message = null;
//        try {
//            // ??????
//            String guid = key;
//            message = getFetchRequestService().getFetchRequest(key);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the message for key = " + key, e);
//        }
//        return message;
//    }


    
    ///////////////////////////////////////////////////////////////////////////
    // Note: Google app engine puts so many restrictions as to what kind of query can be performed....
    //       http://code.google.com/appengine/docs/java/datastore/queries.html
    ///////////////////////////////////////////////////////////////////////////
    

    // TBD
    
    // [1] First time processing (for requests with deferred==true)
    public List<String> getFetchRequestKeysForProcessing(Integer maxCount)
    {
        List<String> keys = null;
        String filter = "fetchStatus == " + FetchStatus.STATUS_SCHEDULED;
        String ordering = null;
        try {
            keys = getFetchRequestService().findFetchRequestKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }
        return keys;
    }
    public List<FetchRequest> getFetchRequestsForProcessing(Integer maxCount)
    {
        List<FetchRequest> fetchRequests = null;
        String filter = "fetchStatus == " + FetchStatus.STATUS_SCHEDULED;
        String ordering = null;
        try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }
        return fetchRequests;
    }
 

    // [2] "Refresh" processing
    public List<String> getFetchRequestKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "fetchStatus == " + FetchStatus.STATUS_QUEUED + " && nextRefreshTime <= " + now;  // status.processing???? also, refreshInterval == -1 or > 0, etc. ???
        String ordering = "nextRefreshTime asc";
        try {
            keys = getFetchRequestService().findFetchRequestKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }
        return keys;
    }
    public List<FetchRequest> getFetchRequestsForRefreshProcessing(Integer maxCount)
    {
        List<FetchRequest> fetchRequests = null;
        long now = System.currentTimeMillis();
        String filter = "fetchStatus == " + FetchStatus.STATUS_QUEUED + " && nextRefreshTime <= " + now;  // status.processing???? also, refreshInterval == -1 or > 0, etc. ???
        String ordering = "nextRefreshTime asc";
        try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }
        return fetchRequests;
    }


    // [3] "Reprocessing" or "revival processing" for Failed or Interrupted (e.g., those stuck in "processing", etc.)
    // TBD: This is only to be done for the failed requests of non-recurring types that have not expired, etc....
    // Note: if there are multiple fetchRequest failed, only the same few fetchRequests may be reprocessed
    //       (possibly depending on how we update nextRefreshTime, etc...)
    // We need to check this, and try to avoid "starving" certain failed fetchRequests.....
    public List<String> getFetchRequestKeysForRevivalProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        long someTimeAgo = now - 12 * 60 * 60 * 1000L;   // 12 hours ago...
        // Unfortunately, this "multi-query" does not work for Key-only fetching...
        // String filter = "(fetchStatus == " + FetchStatus.STATUS_FAILED + " || fetchStatus == " + FetchStatus.STATUS_PROCESSING + ") && nextRefreshTime <= " + someTimeAgo;
        String filter = "fetchStatus == " + FetchStatus.STATUS_FAILED + " && nextRefreshTime <= " + someTimeAgo;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getFetchRequestService().findFetchRequestKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }
        return keys;
    }
    public List<FetchRequest> getFetchRequestsForRevivalProcessing(Integer maxCount)
    {
        List<FetchRequest> fetchRequests = null;
        long now = System.currentTimeMillis();
        long someTimeAgo = now - 12 * 60 * 60 * 1000L;   // 12 hours ago...
        // To be consistent with key-fetching, we will check "failed" conditions only....
        // String filter = "(fetchStatus == " + FetchStatus.STATUS_FAILED + " || fetchStatus == " + FetchStatus.STATUS_PROCESSING + ") && nextRefreshTime <= " + someTimeAgo;
        String filter = "fetchStatus == " + FetchStatus.STATUS_FAILED + " && nextRefreshTime <= " + someTimeAgo;
        String ordering = "nextRefreshTime asc";
        try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }
        return fetchRequests;
    }

    // So, separate methods for failed processing records with FetchStatus == STATUS_PROCESSING....

    public List<String> getFetchRequestKeysForRetryProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        // Note: this time delay is very important
        //   because we don't want to re-process the fetchrequests that are currently (legitmately) being processed.
        // The normal processing should take no more than 10 mins, at max,
        //   hence the delay should be longer than 10 mins...
        // (Note: We should avoid running multiple/potentially overlapping retry or revival crons...)
        long someTimeAgo = now - 1 * 60 * 60 * 1000L;   // 1 hour ago...
        String filter = "fetchStatus == " + FetchStatus.STATUS_PROCESSING + " && nextRefreshTime <= " + someTimeAgo;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getFetchRequestService().findFetchRequestKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }
        return keys;
    }
    public List<FetchRequest> getFetchRequestsForRetryProcessing(Integer maxCount)
    {
        List<FetchRequest> fetchRequests = null;
        long now = System.currentTimeMillis();
        // Same as above....
        long someTimeAgo = now - 1 * 60 * 60 * 1000L;   // 1 hour ago...
        String filter = "fetchStatus == " + FetchStatus.STATUS_PROCESSING + " && nextRefreshTime <= " + someTimeAgo;
        String ordering = "nextRefreshTime asc";
        try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }
        return fetchRequests;
    }

    
}
