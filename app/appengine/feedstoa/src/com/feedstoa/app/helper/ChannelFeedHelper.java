package com.feedstoa.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.app.service.ChannelFeedAppService;
import com.feedstoa.app.service.UserAppService;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.common.RefreshStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;


public class ChannelFeedHelper
{
    private static final Logger log = Logger.getLogger(ChannelFeedHelper.class.getName());

    private ChannelFeedHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private ChannelFeedAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private ChannelFeedAppService getChannelFeedService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new ChannelFeedAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ChannelFeedHelperHolder
    {
        private static final ChannelFeedHelper INSTANCE = new ChannelFeedHelper();
    }

    // Singleton method
    public static ChannelFeedHelper getInstance()
    {
        return ChannelFeedHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public ChannelFeed getChannelFeed(String guid) 
    {
        ChannelFeed message = null;
        try {
            message = getChannelFeedService().getChannelFeed(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getChannelFeedKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getChannelFeedService().findChannelFeedKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<ChannelFeed> getChannelFeedsForRefreshProcessing(Integer maxCount)
    {
        List<ChannelFeed> channelFeeds = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return channelFeeds;
    }


}
