package com.feedstoa.app.fetch.rss;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.RssChannelBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.app.fetch.util.ChannelFeedAndItems;
import com.feedstoa.app.fetch.util.FetchUtil;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.core.GUID;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndImage;


public class RomeFetchUtil
{
    private static final Logger log = Logger.getLogger(RomeFetchUtil.class.getName());

    // Static methods only.
    private RomeFetchUtil() {}

   
    
    ///////////////////////////////////////////////////////////////////
    // TBD:
    // This added layer is to insulate third party libraries...
    ///////////////////////////////////////////////////////////////////
    

    public static RssItem convertToRssItem(SyndEntry syndEntry)
    {
    	if(syndEntry == null) {
    		return null;
    	}

    	RssItemBean rssItem = new RssItemBean();

    	String title = syndEntry.getTitle();
        if(title != null) {
        	rssItem.setTitle(title);
        }
        String link = syndEntry.getLink();
        if(link != null) {
          	rssItem.setLink(link);
        }
        String uri = syndEntry.getUri();
        if(uri != null && !uri.isEmpty()) {
            rssItem.setGuid(uri);   // ????
        } else {
            if(link != null && !link.isEmpty()) {
                if(log.isLoggable(Level.INFO)) log.info("SyndEntry uri is null or empty. link is used instead as a guid: " + link);
                rssItem.setGuid(link);   // ????
            } else {
                String guid = GUID.generate();
                if(log.isLoggable(Level.WARNING)) log.warning("SyndEntry uri/link is null or empty. Using a random guid: " + guid);
                rssItem.setGuid(guid);  // ???
            }
        }
        
        SyndContent syndDescription = syndEntry.getDescription();
        //List<SyndContent> syndContents = syndEntry.getContents();
        if(syndDescription != null) {
            String description = syndDescription.getValue();
            if(description != null) {
                rssItem.setDescription(description);
                //rssItem.setComments(description);    // ?????  Comments is a URL to comments page...
                // TBD: how to get comments URL from syndEntry?????
                // ....
            }
        }
        String author = syndEntry.getAuthor();
        if(author != null) {
        	rssItem.setAuthor(author);
        }
        List<SyndCategory> syndCategories = syndEntry.getCategories();
        if(syndCategories != null && !syndCategories.isEmpty()) {
            List<CategoryStruct> categories = new ArrayList<CategoryStruct>();
            for(SyndCategory c : syndCategories) {
                CategoryStructBean category = new CategoryStructBean();
                String domain = c.getTaxonomyUri();  // ???
                if(domain != null) {
                    category.setDomain(domain);
                }
                String label = c.getName();
                if(label != null) {
                    category.setLabel(label);
                }
                categories.add(category);
            }
            rssItem.setCategory(categories);            
        }
        @SuppressWarnings("unchecked")
        List<SyndEnclosure> syndEnclosures = syndEntry.getEnclosures();
        if(syndEnclosures != null && !syndEnclosures.isEmpty()) {
            if(log.isLoggable(Level.WARNING)) {
                int size = syndEnclosures.size();
                if(size > 1) {
                    //log.warning("syndEnclosures list has more than one SyndEnclosure element: size " + size);
                    log.info(">>>>>>>> syndEnclosures list has more than one SyndEnclosure element: size " + size);
                }
            }
            SyndEnclosure syndEnclosure = syndEnclosures.get(0);    // TBD: If size > 1 ????
            if(syndEnclosure != null) {
                EnclosureStructBean enclosure = new EnclosureStructBean();
                String url = syndEnclosure.getUrl();
                if(url != null) {
                    enclosure.setUrl(url);
                }
                String type = syndEnclosure.getType();
                if(type != null) {
                    enclosure.setType(type);
                }
                long syndLength = syndEnclosure.getLength();
                if(log.isLoggable(Level.WARNING)) {
                    if (syndLength < Integer.MIN_VALUE || syndLength > Integer.MAX_VALUE) {
                        log.warning("syndLength is outside the integer range: syndLength = " + syndLength);
                    }
                }
                enclosure.setLength((int) syndLength);
                rssItem.setEnclosure(enclosure);
            }            
        }
        Date publishedDate = syndEntry.getPublishedDate();
        if(publishedDate != null) {
            String pubDate = FetchUtil.formatDate(publishedDate);
            if(pubDate != null) {
                rssItem.setPubDate(pubDate);
            }
        }
        // ????
        SyndFeed syndSource = syndEntry.getSource();
        if(syndSource != null) {
            String sourceHref = syndSource.getLink();   // vs. getUri() ????
            String sourceLabel = syndSource.getTitle();
            if(sourceHref != null || sourceLabel != null) {
                UriStructBean source = new UriStructBean();
                if(sourceHref != null) {
                    source.setHref(sourceHref);
                }
                if(sourceLabel != null) {
                    source.setLabel(sourceLabel);
                }
                rssItem.setSource(source);
            }
        }
        
        // etc...
        // ...
    	
    	return rssItem;
    }

    public static RssChannel convertToRssChannel(SyndFeed syndFeed)
    {
    	if(syndFeed == null) {
    		return null;
    	}
    	
    	RssChannelBean rssChannel = new RssChannelBean();

    	String feedType = syndFeed.getFeedType();
        // rssChannel.setFeedType(feedType);
    	if(! ("rss_2.0".equals(feedType) || "rss_1.0".equals(feedType))) {   // ???
    	    // ???
    	    // TBD: Bail out????
    	    //if(log.isLoggable(Level.WARNING)) log.warning("Unsupported feed type = " + feedType);
            if(log.isLoggable(Level.INFO)) log.info(">>>>>>>>>>>>>>> Unsupported feed type = " + feedType);
    	}
    	
    	String title = syndFeed.getTitle();
    	if(title != null) {
    	    rssChannel.setTitle(title);
    	}
    	

        String link = syndFeed.getLink();   // Vs. getUri() ????
        if(link != null) {
          	rssChannel.setLink(link);       // Article link...
        }
        
        // Why SyndFeed does not have ttl ?????
//        Integer ttl = syndFeed.getTtl();   // ???
//        if(ttl != null) {
//            rssChannel.setTtl(ttl);
//        }

        String description = syndFeed.getDescription();
        if(description != null) {
        	rssChannel.setDescription(description);
        }
        String language = syndFeed.getLanguage();
        if(language != null) {
        	rssChannel.setLanguage(language);
        }
        String copyright = syndFeed.getCopyright();
        if(copyright != null) {
        	rssChannel.setCopyright(copyright);
        }
        String managingEditor = syndFeed.getAuthor();
        if(managingEditor != null) {
        	rssChannel.setManagingEditor(managingEditor);  // ???
        }
        
        @SuppressWarnings("unchecked")
        // List<String> syndAuthors = syndFeed.getAuthors();  // ????
        List<String> syndContributors = syndFeed.getContributors();
        if(syndContributors != null && !syndContributors.isEmpty()) {
            String webMaster = syndContributors.get(0);    // ????? if size > 1 ????
            if(webMaster != null) {
                rssChannel.setWebMaster(webMaster);
            }
        }
        
        Date publishedDate = syndFeed.getPublishedDate();
        if(publishedDate != null) {
        	String pubDate = FetchUtil.formatDate(publishedDate);
        	if(pubDate != null) {
        		rssChannel.setPubDate(pubDate);
        	}
        }
//        String lastBuildDate = syndFeed.getLastBuildDate();
//        if(lastBuildDate != null) {
//        	rssChannel.setLastBuildDate(lastBuildDate);   // ????
//        }
        @SuppressWarnings("unchecked")
        List<SyndCategory> syndCategories = syndFeed.getCategories();
        if(syndCategories != null && !syndCategories.isEmpty()) {
            List<CategoryStruct> rssCategories = new ArrayList<CategoryStruct>();
        	for(SyndCategory c : syndCategories) {
        	    CategoryStructBean bean = new CategoryStructBean();
        		String domain = c.getTaxonomyUri();
        		if(domain != null) {
        			bean.setDomain(domain);  // ????
        		}
        		String label = c.getName();
        		if(label != null) {
        			bean.setLabel(label);          // ????
        		}
        		rssCategories.add(bean);
        	}
        	rssChannel.setCategory(rssCategories);
        }

        // ????
//        String generator = syndFeed.getGenerator();
//        if(generator != null) {
//        	rssChannel.setGenerator(generator);    // ???
//        }
//        String docs = syndFeed.getDocs();
//        if(docs != null) {
//        	rssChannel.setDocs(docs);     // ???
//        }
//        RssCloudStruct cloud = syndFeed.getCloud();
//        if(cloud != null) {
//        	rssChannel.setCloud(cloud);   // ???
//        }

        SyndImage syndImage = syndFeed.getImage();
        if(syndImage != null) {
            ImageStructBean image = new ImageStructBean();
        	String url = syndImage.getUrl();
        	String imgTitle = syndImage.getTitle();
        	if(url != null && imgTitle != null) {    // ????
        		image.setUrl(url);               // Image url
        		image.setTitle(imgTitle);

        		String imgLink = syndImage.getLink();
            	if(imgLink != null) {          
            		image.setLink(imgLink);   // a link (if image is clicked)
            	}
            	rssChannel.setImage(image);   // ??
        	} else {
        		// ????
        	}
        }
//        String rating = syndFeed.getRating();
//        if(rating != null) {
//        	rssChannel.setRating(rating);   // ???
//        }
//        RssTextInputStruct textInput = syndFeed.getTextInput();
//        if(textInput != null) {
//        	rssChannel.setTextInput(textInput);   // ???
//        }
//        List<RssDayStruct> skipDays = syndFeed.getSkipDays();
//        if(skipDays != null) {
//        	rssChannel.setSkipDays(skipDays);     // ???
//        }

        @SuppressWarnings("unchecked")
        List<SyndEntry> syndEntries = syndFeed.getEntries();
        if(syndEntries != null) {
            List<RssItem> rssItems = new ArrayList<RssItem>();
        	for(SyndEntry e : syndEntries) {
        	    rssItems.add(convertToRssItem(e));
        	}
        	rssChannel.setItem(rssItems);
        }
    	
    	return rssChannel;
    }
   

    
    // ????
    
    public static FeedItem convertToFeedItem(SyndEntry syndEntry)
    {
        return convertToFeedItem(syndEntry, null);
    }
    public static FeedItem convertToFeedItem(SyndEntry syndEntry, FeedItem feedItem)
    {
    	RssItem rssItem = convertToRssItem(syndEntry);    	
    	return RssFetchUtil.convertToFeedItem(rssItem, feedItem);
    }

    public static ChannelFeedAndItems convertToChannelFeed(SyndFeed syndFeed)
    {
        return convertToChannelFeed(syndFeed, null);
    }
    public static ChannelFeedAndItems convertToChannelFeed(SyndFeed syndFeed, ChannelFeed channelFeed)
    {
        return convertToChannelFeed(syndFeed, channelFeed, null);
    }
    public static ChannelFeedAndItems convertToChannelFeed(SyndFeed syndFeed, ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
        String feedType = null;
        if(syndFeed != null) {
             feedType = syndFeed.getFeedType();
        }

        ChannelFeedAndItems channelFeedAndItems = null;
        if("rss_2.0".equals(feedType) || "rss_1.0".equals(feedType)) {   // ???
            RssChannel rssChannel = convertToRssChannel(syndFeed);
            channelFeedAndItems = RssFetchUtil.convertToChannelFeed(rssChannel, channelFeed, feedItems);
        } else if("atom_1.0".equals(feedType)) {
            // ????
            // TBD: Implement atom conversion routines???
            // Or, just reuse Rss-related classes???
            // Or, remove this intermediate layer altogether???
            // ...
            
            // Temporary
            // Does this work???
            if(log.isLoggable(Level.INFO)) log.info(":::::::::::: Attempting to process unsupported feed type = " + feedType);
            RssChannel rssChannel = convertToRssChannel(syndFeed);
            channelFeedAndItems = RssFetchUtil.convertToChannelFeed(rssChannel, channelFeed, feedItems);
            if(log.isLoggable(Level.FINE)) {
                log.fine(":::::::::::: channelFeedAndItems = " + channelFeedAndItems);
            }
            // ...
            
        } else {
            // ???
            // TBD: Bail out????
            if(log.isLoggable(Level.WARNING)) log.warning("Unsupported feed type = " + feedType);
        }
        
        return channelFeedAndItems;
    }

    

    // TBD
    // ....

    

    
}
