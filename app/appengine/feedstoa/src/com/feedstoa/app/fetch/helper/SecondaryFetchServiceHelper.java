package com.feedstoa.app.fetch.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.impl.SecondaryFetchServiceImpl;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.SecondaryFetch;


public class SecondaryFetchServiceHelper
{
    private static final Logger log = Logger.getLogger(SecondaryFetchServiceHelper.class.getName());

    private SecondaryFetchServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserService userService = null;
    private SecondaryFetchService secondaryFetchService = null;
    // etc...


    private UserService getUserService()
    {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }   
    private SecondaryFetchService getSecondaryFetchService()
    {
        if(secondaryFetchService == null) {
            secondaryFetchService = new SecondaryFetchServiceImpl();
        }
        return secondaryFetchService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class SecondaryFetchHelperHolder
    {
        private static final SecondaryFetchServiceHelper INSTANCE = new SecondaryFetchServiceHelper();
    }

    // Singleton method
    public static SecondaryFetchServiceHelper getInstance()
    {
        return SecondaryFetchHelperHolder.INSTANCE;
    }

        
    
    // Key or guid...
    public SecondaryFetch getSecondaryFetch(String guid) throws BaseException 
    {
        SecondaryFetch secondaryFetch = null;
//        try {
            secondaryFetch = getSecondaryFetchService().getSecondaryFetch(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the secondaryFetch for guid = " + guid, e);
//        }
        return secondaryFetch;
    }

    
    public List<SecondaryFetch> findSecondaryFetchesByFetchRequest(String fetchRequest) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findSecondaryFetchesByFetchRequest() BEGIN: fetchRequest = " + fetchRequest);
    
        List<SecondaryFetch> secondaryFetches = null;
        String filter = "fetchRequest == '" + fetchRequest + "'";
        String ordering = null;
        //String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            secondaryFetches = getSecondaryFetchService().findSecondaryFetches(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve secondaryFetch list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findSecondaryFetchesByFetchRequest() END:");
        return secondaryFetches;
    }

    
    public SecondaryFetch getSecondaryFetchByChannelFeed(String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getSecondaryFetchByChannelFeed() BEGIN: channelFeed = " + channelFeed);

        SecondaryFetch secondaryFetch = null;
        String filter = "channelFeed == '" + channelFeed + "'";
        String ordering = null;
        //String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<SecondaryFetch> beans = getSecondaryFetchService().findSecondaryFetches(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                secondaryFetch = beans.get(0);
                if(log.isLoggable(Level.WARNING)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This cannot happen...
                        log.warning("More than one SecondaryFetch found for channelFeed = " + channelFeed + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve secondaryFetch list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getSecondaryFetchByChannelFeed() END: secondaryFetch = " + secondaryFetch);
        return secondaryFetch;
    }
    
    
    
    public List<SecondaryFetch> findSecondaryFetchesByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findSecondaryFetchesByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        List<SecondaryFetch> secondaryFetches = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            secondaryFetches = getSecondaryFetchService().findSecondaryFetches(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve secondaryFetch list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findSecondaryFetchesByFetchUrl() END:");
        return secondaryFetches;
    }

    public SecondaryFetch getSecondaryFetchByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getSecondaryFetchByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        SecondaryFetch secondaryFetch = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<SecondaryFetch> beans = getSecondaryFetchService().findSecondaryFetches(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                secondaryFetch = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one SecondaryFetch found for fetchUrl = " + fetchUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve secondaryFetch list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getSecondaryFetchByFetchUrl() END: secondaryFetch = " + secondaryFetch);
        return secondaryFetch;
    }

    
    public List<SecondaryFetch> findSecondaryFetchesByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findSecondaryFetchesByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        List<SecondaryFetch> secondaryFetches = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            secondaryFetches = getSecondaryFetchService().findSecondaryFetches(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve secondaryFetch list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findSecondaryFetchesByFeedUrl() END:");
        return secondaryFetches;
    }

    public SecondaryFetch getSecondaryFetchByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getSecondaryFetchByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        SecondaryFetch secondaryFetch = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<SecondaryFetch> beans = getSecondaryFetchService().findSecondaryFetches(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                secondaryFetch = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one SecondaryFetch found for feedUrl = " + feedUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve secondaryFetch list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getSecondaryFetchByFeedUrl() END: secondaryFetch = " + secondaryFetch);
        return secondaryFetch;
    }


    

}
