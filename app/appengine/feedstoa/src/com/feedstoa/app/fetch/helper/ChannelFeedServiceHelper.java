package com.feedstoa.app.fetch.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.FetchRequestServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;


public class ChannelFeedServiceHelper
{
    private static final Logger log = Logger.getLogger(ChannelFeedServiceHelper.class.getName());

    private ChannelFeedServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserService userService = null;
    private FetchRequestService fetchRequestService = null;
    private ChannelFeedService channelFeedService = null;
    // etc...


    private UserService getUserService()
    {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }   
    private FetchRequestService getFetchRequestService()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceImpl();
        }
        return fetchRequestService;
    }
    private ChannelFeedService getChannelFeedService()
    {
        if(channelFeedService == null) {
            channelFeedService = new ChannelFeedServiceImpl();
        }
        return channelFeedService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ChannelFeedHelperHolder
    {
        private static final ChannelFeedServiceHelper INSTANCE = new ChannelFeedServiceHelper();
    }

    // Singleton method
    public static ChannelFeedServiceHelper getInstance()
    {
        return ChannelFeedHelperHolder.INSTANCE;
    }

        
    
    // Key or guid...
    public ChannelFeed getChannelFeed(String guid) throws BaseException 
    {
        ChannelFeed channelFeed = null;
//        try {
            channelFeed = getChannelFeedService().getChannelFeed(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the channelFeed for guid = " + guid, e);
//        }
        return channelFeed;
    }

    
    public List<ChannelFeed> findChannelFeedsByFetchRequest(String fetchRequest) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByFetchRequest() BEGIN: fetchRequest = " + fetchRequest);

        List<ChannelFeed> channelFeeds = null;
        String filter = "fetchRequest == '" + fetchRequest + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;  // Set an upper limit, just to be safe????
//        try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByFetchRequest() END:");
        return channelFeeds;
    }

    
    public ChannelFeed getChannelFeedByFetchRequest(String fetchRequest) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByFetchRequest() BEGIN: fetchRequest = " + fetchRequest);

        ChannelFeed channelFeed = null;
        String filter = "fetchRequest == '" + fetchRequest + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<ChannelFeed> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                channelFeed = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This is possible, if FetchRequest.reuseChannel==false (or, if it was false at some point in the past)...
                        log.info("More than one ChannelFeed found for fetchRequest = " + fetchRequest + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByFetchRequest() END: channelFeed = " + channelFeed);
        return channelFeed;
    }

  
    public List<ChannelFeed> findChannelFeedsByChannelSource(String channelSource) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByChannelSource() BEGIN: channelSource = " + channelSource);

        List<ChannelFeed> channelFeeds = null;
        String filter = "channelSource == '" + channelSource + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;  // Set an upper limit, just to be safe????
//        try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByChannelSource() END:");
        return channelFeeds;
    }

    
    public ChannelFeed getChannelFeedByChannelSource(String channelSource) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByChannelSource() BEGIN: channelSource = " + channelSource);

        ChannelFeed channelFeed = null;
        String filter = "channelSource == '" + channelSource + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<ChannelFeed> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                channelFeed = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This is possible, if ChannelSource.reuseChannel==false (or, if it was false at some point in the past)...
                        log.info("More than one ChannelFeed found for channelSource = " + channelSource + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByChannelSource() END: channelFeed = " + channelFeed);
        return channelFeed;
    }

    
    // Returns the "next version" ChannelFeed of previousVersion, if any...
    public ChannelFeed getChannelFeedByPreviousChannel(String previousVersion) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByPreviousChannel() BEGIN: previousVersion = " + previousVersion);

        ChannelFeed channelFeed = null;
        String filter = "previousVersion == '" + previousVersion + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<ChannelFeed> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                channelFeed = beans.get(0);
                if(log.isLoggable(Level.WARNING)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This means more than one ChannelFeed was created during a single refresh (for FetchRequest.reuseChannel==false)
                        log.warning("More than one ChannelFeed found for previousVersion = " + previousVersion + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByPreviousChannel() END: channelFeed = " + channelFeed);
        return channelFeed;
    }

    

    public List<ChannelFeed> findChannelFeedsByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        List<ChannelFeed> channelFeeds = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByFetchUrl() END:");
        return channelFeeds;
    }

    public ChannelFeed getChannelFeedByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        ChannelFeed channelFeed = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<ChannelFeed> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                channelFeed = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one ChannelFeed found for fetchUrl = " + fetchUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByFetchUrl() END: channelFeed = " + channelFeed);
        return channelFeed;
    }

    
    public List<ChannelFeed> findChannelFeedsByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        List<ChannelFeed> channelFeeds = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByFeedUrl() END:");
        return channelFeeds;
    }

    public ChannelFeed getChannelFeedByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        ChannelFeed channelFeed = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<ChannelFeed> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                channelFeed = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one ChannelFeed found for feedUrl = " + feedUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByFeedUrl() END: channelFeed = " + channelFeed);
        return channelFeed;
    }


    // Link.href same as feedUrl ???? Maybe not, in general....  ????
    public List<ChannelFeed> findChannelFeedsByLink(String link) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByLink() BEGIN: link = " + link);
    
        List<ChannelFeed> channelFeeds = null;
        String filter = "linkhref == '" + link + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            channelFeeds = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findChannelFeedsByLink() END:");
        return channelFeeds;
    }

    public ChannelFeed getChannelFeedByLink(String link) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByLink() BEGIN: link = " + link);
    
        ChannelFeed channelFeed = null;
        String filter = "linkhref == '" + link + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<ChannelFeed> beans = getChannelFeedService().findChannelFeeds(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                channelFeed = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one ChannelFeed found for link = " + link + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve channelFeed list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeedByLink() END: channelFeed = " + channelFeed);
        return channelFeed;
    }

   

}
