package com.feedstoa.app.fetch.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.impl.FetchRequestServiceImpl;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FetchRequest;


public class FetchRequestServiceHelper
{
    private static final Logger log = Logger.getLogger(FetchRequestServiceHelper.class.getName());

    private FetchRequestServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserService userService = null;
    private FetchRequestService fetchRequestService = null;
    // etc...


    private UserService getUserService()
    {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }   
    private FetchRequestService getFetchRequestService()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceImpl();
        }
        return fetchRequestService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FetchRequestHelperHolder
    {
        private static final FetchRequestServiceHelper INSTANCE = new FetchRequestServiceHelper();
    }

    // Singleton method
    public static FetchRequestServiceHelper getInstance()
    {
        return FetchRequestHelperHolder.INSTANCE;
    }

        
    
    // Key or guid...
    public FetchRequest getFetchRequest(String guid) throws BaseException 
    {
        FetchRequest fetchRequest = null;
//        try {
            fetchRequest = getFetchRequestService().getFetchRequest(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the fetchRequest for guid = " + guid, e);
//        }
        return fetchRequest;
    }

    
    public FetchRequest getFetchRequestByChannelFeed(String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequestByChannelFeed() BEGIN: channelFeed = " + channelFeed);

        FetchRequest fetchRequest = null;
        String filter = "channelFeed == '" + channelFeed + "'";
        String ordering = null;
        //String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<FetchRequest> beans = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                fetchRequest = beans.get(0);
                if(log.isLoggable(Level.WARNING)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This cannot happen...
                        log.warning("More than one FetchRequest found for channelFeed = " + channelFeed + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetchRequest list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequestByChannelFeed() END: fetchRequest = " + fetchRequest);
        return fetchRequest;
    }
    
    
    
    public List<FetchRequest> findFetchRequestsByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFetchRequestsByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        List<FetchRequest> fetchRequests = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetchRequest list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFetchRequestsByFetchUrl() END:");
        return fetchRequests;
    }

    public FetchRequest getFetchRequestByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequestByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        FetchRequest fetchRequest = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<FetchRequest> beans = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                fetchRequest = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one FetchRequest found for fetchUrl = " + fetchUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetchRequest list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequestByFetchUrl() END: fetchRequest = " + fetchRequest);
        return fetchRequest;
    }

    
    public List<FetchRequest> findFetchRequestsByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFetchRequestsByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        List<FetchRequest> fetchRequests = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetchRequest list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFetchRequestsByFeedUrl() END:");
        return fetchRequests;
    }

    public FetchRequest getFetchRequestByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequestByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        FetchRequest fetchRequest = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<FetchRequest> beans = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                fetchRequest = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one FetchRequest found for feedUrl = " + feedUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetchRequest list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequestByFeedUrl() END: fetchRequest = " + fetchRequest);
        return fetchRequest;
    }


    

}
