package com.feedstoa.app.fetch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;


// Primarily to shield third party Feed libraries (such as Rome)
public class FetchUtil
{
    private static final Logger log = Logger.getLogger(FetchUtil.class.getName());

    // Static methods only.
    private FetchUtil() {}
    
    
    
    public static String formatDate(Date date)
    {
        if(date == null) {
            return null;  // ???
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
        //TimeZone timeZone = TimeZone.getDefault();
        ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
        //dateFormat.setTimeZone(timeZone);
        String dateStr = dateFormat.format(date);
        return dateStr;
    }

    public static String formatDate(Long time)
    {
        return formatDate(time, null);
    }
    public static String formatDate(Long time, String tzStr)
    {
        if(time == null) {
            return null;  // ???
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
        TimeZone timeZone = null;
        if(tzStr == null) {
            timeZone = TimeZone.getDefault();            
        } else {
            //timeZone = TimeZone.getTimeZone("US/Pacific");
            timeZone = TimeZone.getTimeZone(tzStr);
        }
        dateFormat.setTimeZone(timeZone);
        String dateStr = dateFormat.format(new Date(time));
        return dateStr;
    }

    public static Date parseDate(String strDate)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Date date = null;
        try {
            //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
            //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
            TimeZone timeZone = TimeZone.getDefault();
            //TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
            dateFormat.setTimeZone(timeZone);
            date = dateFormat.parse(strDate);
        } catch (ParseException e1) {
            log.log(Level.WARNING, "Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            log.log(Level.WARNING, "Failed to parse Date: strDate = " + strDate, e2);
        }
        return date;
    }

    

    // temporary
    public static boolean isEmailAddress(String email)
    {
        // temporary
        if(email != null && email.contains("@")) {
            return true;
        } else {
            return false;
        }
    }
    
    
    
    
    
    ///////////////////////////////////////////////////////////////////
    // TBD:
    // This added layer is to insulate third party libraries...
    ///////////////////////////////////////////////////////////////////
    
    

    // TBD
    // ....
    

}
