package com.feedstoa.app.fetch;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;

import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.bean.FetchBaseBean;
import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.FeedItemServiceImpl;
import com.feedstoa.af.service.impl.FetchRequestServiceImpl;
import com.feedstoa.af.service.impl.SecondaryFetchServiceImpl;
import com.feedstoa.af.util.CacheHelper;
import com.feedstoa.af.util.ExceptionUtil;
import com.feedstoa.app.cron.CronExpressionListParser;
import com.feedstoa.app.fetch.helper.ChannelFeedServiceHelper;
import com.feedstoa.app.fetch.helper.FeedItemServiceHelper;
import com.feedstoa.app.fetch.helper.SecondaryFetchServiceHelper;
import com.feedstoa.app.fetch.rss.RomeFetchUtil;
import com.feedstoa.app.fetch.util.ChannelFeedAndItems;
import com.feedstoa.app.fetch.util.FetchUtil;
import com.feedstoa.app.util.ConfigUtil;
import com.feedstoa.common.FetchStatus;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.FetchBase;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.exception.InternalServerErrorException;
import com.feedstoa.ws.exception.ServiceUnavailableException;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;


// Rome: ...
public class FetchProcessor
{
    private static final Logger log = Logger.getLogger(FetchProcessor.class.getName());   
    
    // temporary
    private static final long REFRESH_INTERVAL = 3600000 * 24 * 7;   // a week

    // Max entity size on GAE data store/memcache is 1M.  (1 char == 2 bytes)
    private static final int MAX_OUTPUT_LENGTH = 450000;  // ???

    
    // TBD:
    private FetchRequestService fetchRequestService = null;
    private SecondaryFetchService secondaryFetchService = null;
    private ChannelFeedService channelFeedService = null;
    private FeedItemService feedItemService = null;
    // ...

    
    private FetchProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class FetchProcessorHolder
    {
        private static final FetchProcessor INSTANCE = new FetchProcessor();
    }

    // Singleton method
    public static FetchProcessor getInstance()
    {
        return FetchProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD:
        // ...
    }
    
    
    // ...
    private FetchRequestService getFetchRequestService()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceImpl();
        }
        return fetchRequestService;
    }
    private SecondaryFetchService getSecondaryFetchService()
    {
        if(secondaryFetchService == null) {
            secondaryFetchService = new SecondaryFetchServiceImpl();
        }
        return secondaryFetchService;
    }
    private ChannelFeedService getChannelFeedService()
    {
        if(channelFeedService == null) {
            channelFeedService = new ChannelFeedServiceImpl();
        }
        return channelFeedService;
    }
    private FeedItemService getFeedItemService()
    {
        if(feedItemService == null) {
            feedItemService = new FeedItemServiceImpl();
        }
        return feedItemService;
    }

    
   
    
    // Note:
    // The following "cache" is used primarily to avoid processing duplicate items in the same run...
    // This does not, however, prevent the same fetchRequest (and feedItems) being processed
    //                         at the same time by multiple crons (e.g., from different instances)
    // ....

    // temporary
    // Used to store old/processed FeedItem.id in memcache...
    // Note that feedId cannot be null/empty.
    // Note that both channelFeed and fetchRequest cannot be null/empty.
    // Note: memcache maximum key length is 250 (and GAE may, not sure though, add some prefix to the key)....
    // TBD: "cache" the cacheKey ??? (e.g., just using a hash map, etc.)
    private static String getOldFeedGuidCacheKey(String feedId, String channelGuid, String fetchGuid)
    {
        final String PREFIX = "FP-FEEDGUID-";

        StringBuilder sb = new StringBuilder();
        sb.append(PREFIX);
        if(feedId != null && !feedId.isEmpty()) {
            sb.append("FI-").append(feedId);
        } else {
            // Error..
            // Should not happen...
        }
        if(channelGuid != null && !channelGuid.isEmpty()) {
            sb.append("CF-").append(channelGuid);
        } else {
            sb.append("FR-").append(fetchGuid);
        }

        String cacheKey = sb.toString();
        return cacheKey;        
    }
    private Cache getCache()
    {
        // Cache cache = CacheHelper.getQuickInstance().getCache();     // 10 mins
        Cache cache = CacheHelper.getShortInstance().getCache();  // 30 mins
        return cache;
    }
    // .....
    

    
    
    // TBD
    // Currently, feed format (FetchRequest.outputFormat) is not being used....
    // This should be used in parsing the content retrieved from the fetchUrl
    // ....
    
    public FetchRequest processPageFetch(FetchRequest fetchRequest) throws BaseException
    {
        return processPageFetch(fetchRequest, false);
    }
    public FetchRequest processPageFetch(FetchRequest fetchRequest, boolean refreshing) throws BaseException
    {
        // TBD: Default value false....
        boolean updateExistingFeedItem = ConfigUtil.isFetchUpdateFeedItems();
        return processPageFetch(fetchRequest, refreshing, updateExistingFeedItem); 
    }
    // refreshing==false means first/initial processing (either during initial creation or through fetch cron) ?????? 
    // refreshing==true means it's being processed through refresh cron
    public FetchRequest processPageFetch(FetchRequest fetchRequest, boolean refreshing, boolean updateExistingFeedItem) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processPageFetch() called with fetchRequest = " + fetchRequest + "; refreshing = " + refreshing);
        if(fetchRequest == null) {
            // ????
            log.warning("fetchRequest is null.");
            throw new BadRequestException("fetchRequest is null.");
        }
        
        
        // TBD
        // ...
        
        // Starting processing...
        // Marking it as "PROCESSING" first is necessary to reduce the chance of contention among multiple cron processes
        //       (either from two overlapping cron runs from a single instance or from two different crons from two different instances, etc...)
        // Without this, there will be much higher chance of that happening....
        // although this does not entirely eliminate that possibility....
        // ....
        Integer previousStatus = fetchRequest.getFetchStatus();
        ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_PROCESSING);
        ((FetchRequestBean) fetchRequest).setResult("");   // Reset the result (e.g., from the previous run..)
        // --> Unfortunately, this does not seem to work...
        // A lot of records remain with status == "PROCESSING"...
        // Cannot figure out why that happens....
        // Temporarily blocked....
        // Hopefully, concurrent processing of the same fetchRequest by multiple crons is not a serious problem...
        // Need to check......
//        try {
//            // Just to avoid the case where one (or a few) requests get stuck and repeatedly reprocessed
//            //      while the rest (failed during processing) requests never get a chance
//            //      (the "retry" cron selects failed requests based on "nextRefreshTime asc"),
//            // We need to move the time forward at every run.....
//            long currentTime = System.currentTimeMillis();
//            long nextCheckTime = currentTime + 30 * 60 * 1000L;   // 1/2 hour delay, arbitrary...
//            // ((FetchRequestBean) fetchRequest).setLastUpdatedTime(currentTime);    // ????   Add a new field, lastCheckedTime ????
//            ((FetchRequestBean) fetchRequest).setNextRefreshTime(nextCheckTime);
//            // Is this necessary????
//            // Note that this may be a problem since processPageFetch() may be called while the object is being created for the first time, before saved (e.g., when deferred==false)...
//            if(refreshing) {
//                // TBD:
//                // Since we are using async/delayed saving...
//                // this might be called AFTER the final result has been saved (with status=success or failed....)
//                // Is there a better way????
//                fetchRequest = getFetchRequestService().refreshFetchRequest(fetchRequest);
//                // ....
//            }
//        } catch (BaseException e) {
//            // What to do????
//            // Just continue, or bail out and try again next time????
//            log.log(Level.WARNING, "Failed to change the status of the fetchRequest to PROCESSING. Bailing out.", e);
//            throw e;
//        } catch (Exception e) {
//            // What to do????
//            // Just continue, or bail out and try again next time????
//            // TBD:
//            // Try reverting the status back to the original status, previousStatus,
//            // and try saving again???
//            // ?????
//            log.log(Level.WARNING, "Failed to change the status of the fetchRequest to PROCESSING due to unknown error. Bailing out.", e);
//            throw new InternalServerErrorException("Failed to change the status of the fetchRequest to PROCESSING due to unknown error. Bailing out.", e);
//        }
        // etc....

        
        // fetchUrl/input of the primary FetchRequest cannot be null
        String fetchUrlStr =  fetchRequest.getFetchUrl();
        URL fetchURL = null;
        try {
            fetchURL = new URL(fetchUrlStr);
//            if(feedUrl == null || feedUrl.isEmpty()) {
//                // TBD: What is feedUrl???
//                // In case of fetch, just use fetchUrl????
//                // In case of pub, use FeedStoa permalink ?????
//                String queryString = fetchURL.getQuery();
//                if(queryString != null && !queryString.isEmpty()) {
//                    int idx = fetchUrlStr.lastIndexOf(queryString);
//                    if(idx > 0) {
//                        feedUrl = fetchUrlStr.substring(0, idx-1);  // -1: Remove the trailing "?" as well.
//                    }
//                }
//                feedUrl = fetchUrlStr;
//                ((FetchRequestBean) fetchRequest).setFeedUrl(feedUrl);
//            }
        } catch (MalformedURLException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Invalid fetch url = " + fetchUrlStr, e);
            // TBD: Try one more time, with feedUrl instead (if feedUrl != fetchUrlStr)???
            
            // TBD:
            // Need to set the fetchStatus to failed????
            // --> This is done by the caller....
            throw new BadRequestException("Invalid fetch url = " + fetchUrlStr, e);
        }
        
        
        // TBD:
        // For multiple fetchUrl support...
        // Include both the primary fetchUrl and the secondary Urls.
        // The first element is FetchRequests. The rest, if any, are SecondaryFetches...
        List<FetchBase> fetchList = new ArrayList<FetchBase>();
        fetchList.add(fetchRequest);
        // ...

        // Multi fetch is relevant only when refreshing==true...
        // Because, normally,
        // FetchRequest will be created first.
        // And then, secondaryFetches may be added later using the parent FetchRequest.guid...
        if(refreshing) {
            Boolean multipleFeedEnabled = fetchRequest.isMultipleFeedEnabled();
            if(multipleFeedEnabled != null && multipleFeedEnabled.equals(Boolean.TRUE)) {
                // TBD:
                // Fetch secondaryFetches for given fetchRequest
                // and, if any found, add them to fetchList
                // ...
                
                String fetchReuqestGuid = fetchRequest.getGuid();  // fetchReuqestGuid should be valid at this point...
                List<SecondaryFetch> secondaryFetches = null;
                try {
                    secondaryFetches = SecondaryFetchServiceHelper.getInstance().findSecondaryFetchesByFetchRequest(fetchReuqestGuid);
                } catch (BaseException e) {
                    // What to do????
                    log.log(Level.WARNING, "Failed to get secondaryFetch list.", e);
                    throw e;
                } catch (Exception e) {
                    // What to do????
                    log.log(Level.WARNING, "Failed to get secondaryFetch list.", e);
                    throw new InternalServerErrorException("Failed to get secondaryFetch list due to unknown error.", e);
                }
                if(secondaryFetches != null && !secondaryFetches.isEmpty()) {
                    if(log.isLoggable(Level.FINE)) {
                        log.fine("SecondaryFetchList found for FetchRequest = " + fetchReuqestGuid);
                        for(SecondaryFetch sf : secondaryFetches) {
                            log.fine("SecondaryFetch = " + sf);
                        }
                    }
                    fetchList.addAll(secondaryFetches);
                } else {
                    log.info("FetchRequest with multipleFeedEnabled==true, but no secondaryFetches found!");
                }
                
            }
        }

        
        // TBD
        // Main loop....
        boolean sucPrimary = false;
        Throwable error = null;
        int totalProcessed = 0;
        for(int i=0; i<fetchList.size(); i++) {
            FetchBase fetch = fetchList.get(i);
            boolean suc = false;
            try {
                if(log.isLoggable(Level.FINER)) log.finer("Starting processing: i = " + i);
                suc = process(fetch, refreshing, updateExistingFeedItem);
            //} catch(BaseException e) {
            } catch(Exception e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Fetch processing failed for i = " + i);
                if(i==0) {      // TBD: Check if fetch is instanceof FetchRequest, instead ????
                    // bail out
                    error = e;
                    log.warning("Processing stopped.");
                    break;
                } else {
                    // continue
                    log.warning("Processing may continue to the next SecondaryFetch, if any.");
                    continue;
                }
            }
            if(i==0) {
                sucPrimary = suc;
            }
            if(suc == true) {
                totalProcessed++;
            }
        }
        
        
        
        // Now...
        // since, most likely, everything went smoothly (no exceptions),
        // update FetchRequest...
        
        // Note: We update the counter regardless of whether the processing was successful or not...
        Integer currentCounter = fetchRequest.getCurrentRefreshCount();
        if(currentCounter == null || currentCounter <= 0) {
            currentCounter = 1;
        } else {
            currentCounter += 1;
        }
        ((FetchRequestBean) fetchRequest).setCurrentRefreshCount(currentCounter);
        // ....

        long now =  System.currentTimeMillis();
        if(sucPrimary == true) {   // or totalProcessed > 0,  or totalProcessed == fetchList.size() ?????
            ((FetchRequestBean) fetchRequest).setLastUpdatedTime(now);
            Integer refreshInterval = fetchRequest.getRefreshInterval();
            if(refreshInterval == null || refreshInterval == 0) {
                // [1] One time request...
                ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_PROCESSED);
                ((FetchRequestBean) fetchRequest).setResult("One time fetchRequest successfully processed.");
                log.fine("One time fetchRequest successfully processed.");
            } else {
                // [2] Recurring request...
                Long refreshExpirationTime = fetchRequest.getRefreshExpirationTime();
                Integer maxRefreshCount = fetchRequest.getMaxRefreshCount();
                if((refreshExpirationTime != null && refreshExpirationTime > 0L && refreshExpirationTime <= now) || ((maxRefreshCount != null && maxRefreshCount > 0) && maxRefreshCount <= currentCounter)) {
                    // [2-a] Expired.
                    //if(nextRefreshTime != null && nextRefreshTime > 0L) {
                        ((FetchRequestBean) fetchRequest).setNextRefreshTime(0L);  // ???
                    //}
                    ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_PROCESSED);
                    ((FetchRequestBean) fetchRequest).setResult("Last processing successfully processed.");
                    if(log.isLoggable(Level.WARNING)) {
                        log.warning("Refresh fetchRequest successfully processed (last time).");
                        if(log.isLoggable(Level.INFO)) {
                            log.info("refreshExpirationTime = " + refreshExpirationTime);
                            log.info("refreshExpirationDate = " + FetchUtil.formatDate(refreshExpirationTime, "US/Pacific"));
                            log.info("currentCounter = " + currentCounter + "; maxRefreshCount = " + maxRefreshCount);
                        }
                    }
                } else {
                    Long nextRefreshTime;
                    if(refreshInterval > 0) {
                        // [2-b] Recurring based on refresh interval.
                        // Note the next refresh time is calculated based on NOW, not based on the last scheduled refresh time....
                        nextRefreshTime = now + refreshInterval * 1000L;   // TBD
                    } else {
                        if(refreshInterval == -1) {
                            // [2-c] Based on cron expressions...
                            // Again, the next refresh-time is calculated based on "now"
                            // meaning, we can skip various "fire" points as specified by the cron expressions.....
                            List<String> cronExps = fetchRequest.getRefreshExpressions();
                            String timezone = fetchRequest.getRefreshTimeZone();
                            CronExpressionListParser parser = new CronExpressionListParser(cronExps, timezone);
                            nextRefreshTime = parser.getNextCronTime(now);
                            if(nextRefreshTime == null) {   // Parse error....
                                nextRefreshTime = 0L;
                            }
                        } else {
                            // [2-c] This should not happen.
                            nextRefreshTime = 0L;   // ?????
                            if(log.isLoggable(Level.WARNING)) log.warning("Invalid refresh interval: " + refreshInterval);
                        }
                    }
                    if(log.isLoggable(Level.INFO)) {
                        log.info("nextRefreshTime set to " + nextRefreshTime);
                        if(log.isLoggable(Level.FINE)) {
                            String nextRefreshDate = FetchUtil.formatDate(nextRefreshTime);
                            log.fine("nextRefreshDate = " + nextRefreshDate);
                        }
                    }
                    if(nextRefreshTime != null && nextRefreshTime > 0L) {
                        ((FetchRequestBean) fetchRequest).setNextRefreshTime(nextRefreshTime);            
                        ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_QUEUED);
                        ((FetchRequestBean) fetchRequest).setResult("FetchRequest successfully queued.");
                    } else {
                        // ???
                        // Can this happen? If so, shouldn't we mark it as failed (so that it can be revived later) ??????
//                        log.warning("Unknown error ocurred. The first fetch request probably completed successfully, but it will not be refreshed.");
//                        ((FetchRequestBean) fetchRequest).setNextRefreshTime(0L);            
//                        ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_UNKNOWN);
                        nextRefreshTime = now + 6 * 3600 * 1000L;   // 6 hour pause....
                        log.warning("Unknown error ocurred. The first fetch request probably completed successfully, but it fails to compute the next refresh time. It will be refreshed on or after " + nextRefreshTime);
                        ((FetchRequestBean) fetchRequest).setNextRefreshTime(nextRefreshTime);            
                        ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_FAILED);
                        ((FetchRequestBean) fetchRequest).setResult("Failed to compute the next refresh time.");
                    }
                }
            }
        } else {
            // What to do?????
            log.warning("processPageFetch() failed.");
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_FAILED);  // ?????
            ((FetchRequestBean) fetchRequest).setResult("Fetch failed:");
            // Need to update nextRefreshTime in case the same failed fetchRequests are repeatedly revived
            //        and the rest are "starved"....
            long nextRefreshingTime = now + 3 * 3600 * 1000L;  // 3 hours, arbitrary. 
            ((FetchRequestBean) fetchRequest).setNextRefreshTime(nextRefreshingTime);            
            if(error != null) {
                try {
                    String errorStr = ExceptionUtil.getStackTrace(error);
                    if(errorStr != null) {
                        errorStr = "Fetch failed: " + errorStr;
                        if(errorStr.length() > 500) {
                            errorStr = errorStr.substring(0, 497) + "...";
                        }
                        ((FetchRequestBean) fetchRequest).setResult(errorStr);
                    }
                } catch (Exception e1) {
                    // Ignore...
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed while setting the result of fetchReuest.", e1);
                }
            }
        }
        
        
        
//        // TBD:
//        // Save all child secondary fetches...
//        // This is done in process().
//        if(refreshing) {
//            for(int i=0; i<fetchList.size(); i++) {
//                FetchBase fetch = fetchList.get(i);
//                if(i>0) {
//                    getSecondaryFetchService().updateSecondaryFetch((SecondaryFetch) fetch);
//                }
//            }
//        }
//        // ...
                
        
        log.finer("END: processPageFetch()");
        return fetchRequest;
    }
    
    
    // Note that fetchBase is an in-out parameter....
    private boolean process(FetchBase fetchBase, boolean refreshing, boolean updateExistingFeedItem) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("Begin process(): fetchBase = " + fetchBase + "; refreshing = " + refreshing);
 
        // Check the fetchUrl first....
        String fetchUrlStr =  fetchBase.getFetchUrl();
        String feedUrl = fetchBase.getFeedUrl();
        URL fetchURL = null;
        try {
            fetchURL = new URL(fetchUrlStr);
        } catch (MalformedURLException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Invalid fetch url = " + fetchUrlStr, e);
            // TBD: Try one more time, with feedUrl instead (if feedUrl != fetchUrlStr)???
            throw new BadRequestException("Invalid fetch url = " + fetchUrlStr, e);
        }
        
        // TBD:
        // There is this complex logic!!!
        // Make sure this is what we want.....
        //String oldChannelFeedGuid = null;
        ChannelFeed oldChannelFeed = null;
        boolean createChannelAndItems;
        if(refreshing) {
            String oldGuid = fetchBase.getChannelFeed();
            Boolean reuseChannel = fetchBase.isReuseChannel();
            if(reuseChannel != null && Boolean.FALSE.equals(reuseChannel)) {
                createChannelAndItems = true;
                //oldChannelFeedGuid = oldGuid;
            } else {  // default: reuseChannel == true
                if(oldGuid != null && !oldGuid.isEmpty()) {
                    try {
                        oldChannelFeed = ChannelFeedServiceHelper.getInstance().getChannelFeed(oldGuid);
                    //} catch(BaseException e) {
                    } catch(Exception e) {
                        // This should not normally happen. Ignore...
                        //if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to find previousChannelFeed for guid = " + oldGuid, e);
                        // For better logging...
                        // temporary
                        if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to find previousChannelFeed for guid = " + oldGuid, e);
                        // temporary
                        
                        // Let's try one more time....
                        try {
                            oldChannelFeed = ChannelFeedServiceHelper.getInstance().getChannelFeed(oldGuid);
                        //} catch(BaseException e1) {
                        } catch(Exception e1) {
                            // temporary
                            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "(Second try) Failed to find previousChannelFeed for guid = " + oldGuid, e1);
                            // temporary

                            // The problem is....
                            // For whatever reason, if it fails to retrieve the existing ChannelFeed (and if reuseChannel is set to true)...
                            //                      e.g., because the server was flakey at the time, etc...
                            // then, if we continue despite the error, this logic will go ahead and create a new ChannelFeed....
                            //       which can be a problem..... (in some use cases)....
                            // It's probably better to fail here ... ????
                            
                            // [a] ???? [a] or [b] Which is better???
                            // Darn.... if we throw an exception here.... a massive number of fetchRequests constantly fails   (presumably due to network timeout error????)
                            // So, we'll have to go with option [b]...
                            //throw new InternalServerErrorException("Failed to fetch the old ChannelFeed for fetch url = " + fetchUrlStr + " due to unknown error", e1);
                            
                            // [b] Continuing seems of an option for less maintenance...  ???
                            // If we fail here by thrown an exception, the record may become stuck at "PROCESSING", and we need "retry" cron...
                            // (in some cases, channelFeed for the given oldGuid may not exist in the DB (e.g., because it failed to store, in the first place, etc...),
                            //    in which case, evern retry cron will not help... it requires manual update...)
                            // --> Hence, it's probably preferred to not throw exception...  ???
                            // ...
                        }
                    }
                }
                if(oldChannelFeed != null) {
                    // Update
                    createChannelAndItems = false;
                    //oldChannelFeedGuid = oldGuid;
                } else {
                    // Just create a new one...
                    createChannelAndItems = true;
                }
            }
        } else {
            createChannelAndItems = true;   
        }
        
        
        // TBD
        // Currently, feed format (FetchRequest.outputFormat) is not being used....
        // This should be used in parsing the content retrieved from the fetchUrl
        // ....
        
        boolean successfullyProcessed = false;
        try {
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed syndFeed = null;
            try {
                XmlReader xmlReader1 = new XmlReader(fetchURL);
                syndFeed = input.build(xmlReader1);
//            } catch (IOException e) {
//                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
//                throw new ServiceUnavailableException("Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
//            } catch (IllegalArgumentException e) {
//                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
//                throw new ServiceUnavailableException("Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
//            } catch (FeedException e) {
//                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
//                throw new ServiceUnavailableException("Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
            } catch (Exception e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch the feed from fetch url = " + fetchUrlStr, e);

                // Try one more time...
                try {
                    XmlReader xmlReader2 = new XmlReader(fetchURL);
                    syndFeed = input.build(xmlReader2);
                } catch (Exception e1) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "(Second try) Failed to fetch the feed from fetch url = " + fetchUrlStr, e1);
                    // Bail out...
                    throw new ServiceUnavailableException("Failed to fetch the feed from fetch url = " + fetchUrlStr, e1);
                }
            }
            
            if(syndFeed != null) {
                ChannelFeedAndItems channelFeedAndItems = RomeFetchUtil.convertToChannelFeed(syndFeed, oldChannelFeed);   // Copy oldChannelFeed, if not null, to chnnelFeed first before updating the syndFeed values...
                if(channelFeedAndItems != null) {
                    ChannelFeed channelFeed = channelFeedAndItems.getChannelFeed();
                    if(channelFeed != null) {
                        String channelGuid = channelFeed.getGuid();
                        // TBD: Add/updated any additional attributes here ???
                        String fetchGuid = null;
                        if(fetchBase instanceof SecondaryFetch) {
                            // Use the parent FetchRequest's guid.
                            fetchGuid = ((SecondaryFetch) fetchBase).getFetchRequest();
                            if(fetchGuid == null || fetchGuid.isEmpty()) {
                                // this cannot happen... 
                                fetchGuid = fetchBase.getGuid();
                            }
                        } else {   // fetchBase instanceof FetchRequest
                            fetchGuid = fetchBase.getGuid();                            
                        }
                        ((ChannelFeedBean) channelFeed).setFetchRequest(fetchGuid);
                        ((ChannelFeedBean) channelFeed).setFetchUrl(fetchUrlStr);
                        ((ChannelFeedBean) channelFeed).setFeedUrl(feedUrl);
                        // ....

                        Integer maxItemCount = fetchBase.getMaxItemCount();
                        if(maxItemCount != null) {
                            ((ChannelFeedBean) channelFeed).setMaxItemCount(maxItemCount);
                        }
                        // ....
                        // etc....
                        
                        if(createChannelAndItems == false) {
                            if(oldChannelFeed != null) {  // oldChannelFeed cannot be null if createChannelAndItems == false
                                //channelGuid = oldChannelFeedGuid;
                                channelGuid = oldChannelFeed.getGuid();
                                ((ChannelFeedBean) channelFeed).setGuid(channelGuid);
                                ((ChannelFeedBean) channelFeed).setCreatedTime(oldChannelFeed.getCreatedTime());   // Hmmm... Why does this not work????
                                ((ChannelFeedBean) channelFeed).setModifiedTime(0L);  // Or, now.   ???????
                                // TBD:
                                // Copy relevant attributes from oldChannelFeed to channelFeed????
                                // ??? This is done in convertToChannelFeed() ???
                                // String prevVersion = oldChannelFeed.getPreviousVersion();
                                // if(prevVersion != null) {
                                //     ((ChannelFeedBean) channelFeed).setPreviousVersion(prevVersion);
                                // }
                                // etc...
                                // ...
                            }
                            try {
                                channelFeed = getChannelFeedService().refreshChannelFeed(channelFeed);
                            // } catch(BaseException e) {
                            } catch(Exception e) {
                                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh channelFeed for channelGuid = " + channelGuid, e);
                                // What to do???
                                // Continue?
                            }
                        } else {
                            if(oldChannelFeed != null) {
                                //((ChannelFeedBean) channelFeed).setPreviousVersion(oldChannelFeedGuid);
                                ((ChannelFeedBean) channelFeed).setPreviousVersion(oldChannelFeed.getGuid());
                            }
                            if(fetchBase instanceof FetchRequest) {
                                String feedCategory = ((FetchRequest) fetchBase).getFeedCategory();
                                if(feedCategory != null && !feedCategory.isEmpty()) {
                                    ((ChannelFeedBean) channelFeed).setFeedCategory(feedCategory);
                                }
                            }
                            try {
                                channelFeed = getChannelFeedService().constructChannelFeed(channelFeed);                            
                            // } catch(BaseException e) {
                            } catch(Exception e) {
                                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to create channelFeed for channelGuid = " + channelGuid, e);
                                // What to do???
                                // Cannot continue ... ???
                                throw new ServiceUnavailableException("Failed to create channelFeed for channelGuid = " + channelGuid, e);
                            }
                        }
                        
                        if(channelFeed != null) {
                            channelGuid = channelFeed.getGuid(); 
                            ((FetchBaseBean) fetchBase).setChannelFeed(channelGuid);
                            // PK -> FeedItem 
                            // where PK can be link.href, or id(?), etc... ??????
                            Map<String, FeedItem> oldItemMap = new HashMap<String, FeedItem>(); 
                            if(createChannelAndItems == false) {
                                List<FeedItem> oldItemList = null;
                                try {
                                    oldItemList = FeedItemServiceHelper.getInstance().findFeedItemsByChannelFeed(channelGuid);  // channelGuid == oldChannelFeedGuid if createChannelAndItems == false
                                // } catch(BaseException e) {
                                } catch(Exception e) {
                                    // What to do???
                                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get FeedItem list for channelGuid = " + channelGuid, e);
                                    // Fail here???
                                    // Just continue????
                                }
                                if(oldItemList != null && !oldItemList.isEmpty()) {
                                    for(FeedItem o : oldItemList) {
                                        // TBD: use link? or Id ????
                                        // ....
                                        UriStruct link = o.getLink();  // ?? Can this be null????
                                        String href = null;
                                        if(link != null) {
                                            href = link.getHref();
                                            if(href != null) {
                                                oldItemMap.put(href, o);
                                            }
                                        }
                                    }
                                }
                                if(log.isLoggable(Level.FINER)) {
                                    int s = oldItemMap.size();
                                    log.finer("oldItemMap for channelGuid = " + channelGuid + " => size = " + s);
                                    for(FeedItem fi : oldItemMap.values()) {
                                        log.finer("feedItem = " + fi);
                                    }
                                }
                            }
                                                        
                            
                            List<FeedItem> feedItems = channelFeedAndItems.getFeedItems();
                            if(feedItems != null && !feedItems.isEmpty()) {
                                int totalItemCount = feedItems.size();
                                int actualItemCount = 0;
                                for(FeedItem f : feedItems) {
                                    String itemGuid = f.getGuid();
                                    String feedId = f.getId();
                                    if(feedId == null || feedId.isEmpty()) {
                                        // Can this happen????
                                        if(log.isLoggable(Level.WARNING)) log.warning("feedId is null/empty. Skipping this item = " + itemGuid);
                                        continue;
                                    }
                                    // ...
                                    
                                    
                                    // Note: ...
                                    // Check if this feedItem has been processed for the last ten or 30 minutes, or whatever....
                                    // TBD: use link? or Id ????
                                    // feedId vs link.href... which is a better key ??????
                                    String oldFeedGuid = null;
                                    if(getCache() != null) {
                                        oldFeedGuid = (String) getCache().get(getOldFeedGuidCacheKey(feedId, channelGuid, fetchGuid));
                                        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "FeedItem found from the cache: feed.guid = " + oldFeedGuid);
                                    }
                                    if(oldFeedGuid == null) {
                                        // TBD: ...
                                        // Search one more time from DB ???
                                        // ????
                                        try {
                                            FeedItem existingItem = FeedItemServiceHelper.getInstance().getFeedItemById(feedId, channelGuid);
                                            if(existingItem != null) {
                                                oldFeedGuid = existingItem.getGuid();
                                            }
                                        //} catch(BaseException e) {
                                        } catch(Exception e) {
                                            // ignore... ???
                                            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Error while trying to fetch an existing FeedItem, if any, for feedId = " + feedId);
                                            // Just ignore and continue???
                                        }                                        
                                    }
                                    if(oldFeedGuid != null) {
                                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "FeedItem with feedId, " + feedId + ", already exists: oldFeedGuid = " + oldFeedGuid + " for fetchRequest = " + fetchGuid + " and channelFeed = " + channelGuid);
                                        //throw new InternalServerErrorException("FeedItem with feedId, " + feedId + ", already exists for fetchRequest = " + fetchGuid + " and channelFeed = " + channelGuid);
                                        continue;  // ???
                                    }
                                    // Note that oldFeedGuid==null does not mean it does not exist. Error may have occurred during the query....
                                    
                                    
                                    boolean fiSuc = false;
                                    try {
                                        // TBD:....
                                        ((FeedItemBean) f).setFetchRequest(fetchGuid);    // FetchRequest.guid or SecondaryFetch.fetchRequest.guid
                                        ((FeedItemBean) f).setChannelFeed(channelGuid);
                                        // etc..

                                        UriStruct link = f.getLink();  // ?? Can this be null????
                                        String href = null;
                                        if(link != null) {
                                            href = link.getHref();
                                        }
                                        if(createChannelAndItems == false && oldItemMap.containsKey(href)) {
                                            if(updateExistingFeedItem) {
                                                FeedItem oldFeedItem = oldItemMap.get(href);
                                                // Copy relevant attributes from oldFeedItem to f????
                                                // ??? This is NOT done in convertToChannelFeed()....
                                                // ... what to do ????
                                                itemGuid = oldFeedItem.getGuid();
                                                ((FeedItemBean) f).setGuid(itemGuid);
                                                ((FeedItemBean) f).setCreatedTime(oldFeedItem.getCreatedTime());
                                                String channelSource = oldFeedItem.getChannelSource(); 
                                                if(channelSource != null) {
                                                    ((FeedItemBean) f).setChannelSource(channelSource);
                                                }
                                                // etc...
                                                // ...
                                                f = getFeedItemService().refreshFeedItem(f);
                                                if(f != null) {
                                                    fiSuc = true;
                                                    if(log.isLoggable(Level.INFO)) log.info(">>> FeedItem already exists. Successfully updated the item with href = " + href);
                                                } else {
                                                    if(log.isLoggable(Level.INFO)) log.info(">>> FeedItem already exists. Failed to update the item with href = " + href);
                                                }
                                            } else {
                                                // TBD:
                                                // Does this (updateExistingFeedItem==false) make sense?
                                                // Don't we have to update some attributes of exiting feedItems such as channelFeedGuid, etc... ????
                                                //  --> Do we need to?????   Hmmm.... not sure... maybe not...
                                                // ...
                                                // fiSuc = true; ????
                                                if(log.isLoggable(Level.INFO)) log.info(">>> FeedItem already exists. Skipping the item with href = " + href);
                                            }
                                        } else {
                                            // TBD:
                                            // Check if an item with the same PK/ChannelFeed exists?
                                            // If so, that's sort of an error, in some cases (e.g., createChannelAndItems==true ???).  ?????
                                            f = getFeedItemService().constructFeedItem(f);                                        
                                            if(f != null) {
                                                fiSuc = true;
                                            }
                                        }

                                        // "Mark" it as "processed" (for both create and update)...
                                        if(f != null) {
                                            itemGuid = f.getGuid();  // Is this necessary???
                                            if(getCache() != null) {
                                                getCache().put(getOldFeedGuidCacheKey(feedId, channelGuid, fetchGuid), itemGuid);
                                            }
                                        }
                                    
                                    } catch(BaseException e) {
                                        // Ignore and move on to the next item ???? Or, bail out ????
                                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to save FeedItem: guid = " + itemGuid, e);
                                    } catch(Exception e) {
                                        // Ignore and move on to the next item ???? Or, bail out ????
                                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to save FeedItem due to unknown error: guid = " + itemGuid, e);
                                    }
                                    if(fiSuc == true) {
                                        actualItemCount++;
                                    }
                                }
                                if(log.isLoggable(Level.INFO)) log.info("FeedItems updated/created: actualItemCount = " + actualItemCount + " for totalItemCount = " + totalItemCount);
                            } else {
                                // ignore. Can this happen???
                                if(log.isLoggable(Level.WARNING)) log.warning("No feed item was found for ChannelFeed: guid = " + channelGuid);
                            }                            
                            // Success. (except for possibly we may have had some errors while saving one or more feedItems...
                            successfullyProcessed = true;
                        } else {
                            // ???
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to save ChannelFeed: guid = " + channelGuid);
                        }
                    } else {
                        // ???
                        // This cannot happen.
                        log.warning("ChannelFeed from channelFeedAndItems is null.");
                    }
                } else {
                    // ???
                    // This cannot happen.
                    log.warning("ChannelFeedAndItems is null.");
                }
            } else {
                // ???? What to do???
                log.warning("syndFeed is null.");
            }
        } catch (IllegalArgumentException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
            throw new BadRequestException("Failed to fetch the feed from fetch url = " + fetchUrlStr, e);
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch the feed from fetch url = " + fetchUrlStr + " due to unknown error", e);
            throw new InternalServerErrorException("Failed to fetch the feed from fetch url = " + fetchUrlStr + " due to unknown error", e);
        }
        
        // TBD:
        // Save all child secondary fetches...
        // Note that they are all updates.  (secondaryFeches are always created by explicit input, e.g., by the user)
        if(successfullyProcessed) {
            if(fetchBase instanceof SecondaryFetch) {
                log.info("Updating fetchBase...");
                try {
                    fetchBase = getSecondaryFetchService().refreshSecondaryFetch((SecondaryFetch) fetchBase);
                } catch (BaseException e) {
                    // What to do????
                    log.log(Level.WARNING, "Failed to fetch secondaryFetch.", e);
                    throw e;
                } catch (Exception e) {
                    // What to do????
                    log.log(Level.WARNING, "Failed to fetch secondaryFetch.", e);
                    throw new InternalServerErrorException("Failed to fetch secondaryFetch due to unknown error.", e);
                }
            } else {
                log.info("Skipping because fetchBase is not an instance of SecondaryFetch");
            }
        } else {
            // Nothing to do.
        }
        // ...
        
        if(log.isLoggable(Level.FINER)) log.finer("End process(): successfullyProcessed = " + successfullyProcessed);
        return successfullyProcessed;
    }

    
}
