package com.feedstoa.app.fetch.util;

import java.util.List;
import java.util.logging.Logger;

import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedItem;


public class ChannelFeedAndItems
{
    private static final Logger log = Logger.getLogger(ChannelFeedAndItems.class.getName());

    private final ChannelFeed mChannelFeed;
    private final List<FeedItem> mFeedItems;

    
    public ChannelFeedAndItems(ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
        mChannelFeed = channelFeed;
        mFeedItems = feedItems;
    }


    public ChannelFeed getChannelFeed()
    {
        return mChannelFeed;
    }

    public List<FeedItem> getFeedItems()
    {
        return mFeedItems;
    }


    @Override
    public String toString()
    {
        return "ChannelFeedAndItems [mChannelFeed=" + mChannelFeed
                + ", mFeedItems=" + mFeedItems + "]";
    }


}
