package com.feedstoa.app.fetch.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.af.service.impl.FeedItemServiceImpl;
import com.feedstoa.af.service.impl.FetchRequestServiceImpl;
import com.feedstoa.af.service.impl.UserServiceImpl;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.FeedItem;



// Note:
// feed package comes before service, which does before helper...
// So, cannot use App Services in this class....
public class FeedItemServiceHelper
{
    private static final Logger log = Logger.getLogger(FeedItemServiceHelper.class.getName());

    private FeedItemServiceHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserService userServiceImpl = null;
    private FetchRequestService fetchRequestService = null;
    private ChannelFeedService channelFeedService = null;
    private FeedItemService feedItemService = null;
    // etc...


    private UserService getUserService()
    {
        if(userServiceImpl == null) {
            userServiceImpl = new UserServiceImpl();
        }
        return userServiceImpl;
    }   
    private FetchRequestService getFetchRequestService()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceImpl();
        }
        return fetchRequestService;
    }
    private ChannelFeedService getChannelFeedService()
    {
        if(channelFeedService == null) {
            channelFeedService = new ChannelFeedServiceImpl();
        }
        return channelFeedService;
    }
    private FeedItemService getFeedItemService()
    {
        if(feedItemService == null) {
            feedItemService = new FeedItemServiceImpl();
        }
        return feedItemService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FeedItemFinderHolder
    {
        private static final FeedItemServiceHelper INSTANCE = new FeedItemServiceHelper();
    }

    // Singleton method
    public static FeedItemServiceHelper getInstance()
    {
        return FeedItemFinderHolder.INSTANCE;
    }

    
    // Key or guid...
    public FeedItem getFeedItem(String guid) throws BaseException 
    {
        FeedItem feedItem = null;
//        try {
            feedItem = getFeedItemService().getFeedItem(guid);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the feedItem for guid = " + guid, e);
//        }
        return feedItem;
    }
       
    
    
    public List<FeedItem> findFeedItemsByFetchRequest(String fetchRequest) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByFetchRequest() BEGIN: fetchRequest = " + fetchRequest);

        List<FeedItem> feedItems = null;
        String filter = "fetchRequest == '" + fetchRequest + "'";
        // TBD: Add some max time delta ? e.g., createdTime > now - 10 days, etc. ???
        //      otherwise, the feedItem count will monotonically increase over time.....
        String ordering = "createdTime desc";   // ???
        // Integer maxCount = null;  // Set an upper limit, just to be safe????
        Integer maxCount = 150;      // Arbitrary cutoff....
//        try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByFetchRequest() END:");
        return feedItems;
    }

    
    public FeedItem getFeedItemByFetchRequest(String fetchRequest) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByFetchRequest() BEGIN: fetchRequest = " + fetchRequest);

        FeedItem feedItem = null;
        String filter = "fetchRequest == '" + fetchRequest + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This is possible, if FetchRequest.reuseChannel==false (or, if it was false at some point in the past)...
                        log.info("More than one FeedItem found for fetchRequest = " + fetchRequest + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByFetchRequest() END: feedItem = " + feedItem);
        return feedItem;
    }

    
    public List<FeedItem> findFeedItemsByChannelFeed(String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByChannelFeed() BEGIN: channelFeed = " + channelFeed);

        List<FeedItem> feedItems = null;
        String filter = "channelFeed == '" + channelFeed + "'";
        // TBD: Add some max time delta ? e.g., createdTime > now - 10 days, etc. ???
        //      otherwise, the feedItem count will monotonically increase over time.....
        String ordering = "createdTime desc";   // ???
        // Integer maxCount = null;  // Set an upper limit, just to be safe????
        Integer maxCount = 150;      // Arbitrary cutoff....
//        try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByChannelFeed() END:");
        return feedItems;
    }

    
    public FeedItem getFeedItemByChannelFeed(String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByChannelFeed() BEGIN: channelFeed = " + channelFeed);

        FeedItem feedItem = null;
        String filter = "channelFeed == '" + channelFeed + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This is possible, if ChannelFeed.reuseChannel==false (or, if it was false at some point in the past)...
                        log.info("More than one FeedItem found for channelFeed = " + channelFeed + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByChannelFeed() END: feedItem = " + feedItem);
        return feedItem;
    }


/*
    public List<FeedItem> findFeedItemsByChannelSource(String channelSource) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByChannelSource() BEGIN: channelSource = " + channelSource);

        List<FeedItem> feedItems = null;
        String filter = "channelSource == '" + channelSource + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;  // Set an upper limit, just to be safe????
//        try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByChannelSource() END:");
        return feedItems;
    }

    
    public FeedItem getFeedItemByChannelSource(String channelSource) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByChannelSource() BEGIN: channelSource = " + channelSource);

        FeedItem feedItem = null;
        String filter = "channelSource == '" + channelSource + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//        try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // This is possible, if ChannelSource.reuseChannel==false (or, if it was false at some point in the past)...
                        log.info("More than one FeedItem found for channelSource = " + channelSource + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByChannelSource() END: feedItem = " + feedItem);
        return feedItem;
    }
*/

/*
    public List<FeedItem> findFeedItemsByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        List<FeedItem> feedItems = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByFetchUrl() END:");
        return feedItems;
    }

    public FeedItem getFeedItemByFetchUrl(String fetchUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByFetchUrl() BEGIN: fetchUrl = " + fetchUrl);
    
        FeedItem feedItem = null;
        String filter = "fetchUrl == '" + fetchUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one FeedItem found for fetchUrl = " + fetchUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByFetchUrl() END: feedItem = " + feedItem);
        return feedItem;
    }
*/
    
/*
    public List<FeedItem> findFeedItemsByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        List<FeedItem> feedItems = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByFeedUrl() END:");
        return feedItems;
    }

    public FeedItem getFeedItemByFeedUrl(String feedUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByFeedUrl() BEGIN: feedUrl = " + feedUrl);
    
        FeedItem feedItem = null;
        String filter = "feedUrl == '" + feedUrl + "'";
        String ordering = "createdTime desc";   // ???
        Integer maxCount = 2;
//      try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.INFO)) {
                    int size = beans.size();
                    if(size > 1) {
                        // Just return the most recent one....
                        log.info("More than one FeedItem found for feedUrl = " + feedUrl + ". size >= " + size);
                    }
                }
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByFeedUrl() END: feedItem = " + feedItem);
        return feedItem;
    }
*/
    
    

    // Does "id" work????
    
    public List<FeedItem> findFeedItemsById(String id) throws BaseException
    {
        return findFeedItemsById(id, null);
    }
    public List<FeedItem> findFeedItemsById(String id, String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsById() BEGIN: id = " + id + "; channelFeed = " + channelFeed);
    
        List<FeedItem> feedItems = null;
        String filter = "id == '" + id + "'";
        if(channelFeed != null && !channelFeed.isEmpty()) {
            filter += " && channelFeed == '" + channelFeed + "'";
        }
        String ordering = null;
        if(channelFeed != null && !channelFeed.isEmpty()) {
            ordering = "createdTime desc";   // ???
        }
        Integer maxCount = null;  // Set an upper limit, just to be safe????
//      try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsById() END:");
        return feedItems;
    }

    // Note there can be (normally) more than one feedItems for a given id... ({channelFeed and id} is a PK)
    public FeedItem getFeedItemById(String id) throws BaseException
    {
        return getFeedItemById(id, null);
    }
    public FeedItem getFeedItemById(String id, String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemById() BEGIN: id = " + id + "; channelFeed = " + channelFeed);
    
        FeedItem feedItem = null;
        String filter = "id == '" + id + "'";
        if(channelFeed != null && !channelFeed.isEmpty()) {
            filter += " && channelFeed == '" + channelFeed + "'";
        }
        String ordering = null;
        if(channelFeed != null && !channelFeed.isEmpty()) {
            ordering = "createdTime desc";   // ???
        }
        Integer maxCount = 2;
//      try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.WARNING)) {
                    int size = beans.size();
                    if(size > 1) {
                        if(channelFeed != null && !channelFeed.isEmpty()) {
                            // This should not happen...
                            if(log.isLoggable(Level.WARNING)) log.warning("More than one FeedItem found for id = " + id + "; channelFeed = " + channelFeed + ". size >= " + size);
                        } else {
                            // Just return the most recent one....
                            if(log.isLoggable(Level.INFO)) log.info("More than one FeedItem found for id = " + id + ". size >= " + size);                            
                        }
                    }
                }
                // TBD:
                // get a full fetch object ???
                feedItem = getFeedItem(feedItem.getGuid());
                // ...
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemById() END: feedItem = " + feedItem);
        return feedItem;
    }


    
    public List<FeedItem> findFeedItemsByLink(String link) throws BaseException
    {
        return findFeedItemsByLink(link, null);
    }
    public List<FeedItem> findFeedItemsByLink(String link, String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByLink() BEGIN: link = " + link + "; channelFeed = " + channelFeed);
    
        List<FeedItem> feedItems = null;
        String filter = "linkhref == '" + link + "'";
        if(channelFeed != null && !channelFeed.isEmpty()) {
            filter += " && channelFeed == '" + channelFeed + "'";
        }
        String ordering = null;
        if(channelFeed != null && !channelFeed.isEmpty()) {
            ordering = "createdTime desc";   // ???
        }
        Integer maxCount = null;   // Set an upper limit, just to be safe ???
//      try {
            feedItems = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("findFeedItemsByLink() END:");
        return feedItems;
    }

    public FeedItem getFeedItemByLink(String link) throws BaseException
    {
        return getFeedItemByLink(link, null);
    }
    public FeedItem getFeedItemByLink(String link, String channelFeed) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByLink() BEGIN: link = " + link + "; channelFeed = " + channelFeed);
    
        FeedItem feedItem = null;
        String filter = "linkhref == '" + link + "'";
        if(channelFeed != null && !channelFeed.isEmpty()) {
            filter += " && channelFeed == '" + channelFeed + "'";
        }
        String ordering = null;
        if(channelFeed != null && !channelFeed.isEmpty()) {
            ordering = "createdTime desc";   // ???
        }
        Integer maxCount = 2;
//      try {
            List<FeedItem> beans = getFeedItemService().findFeedItems(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null && !beans.isEmpty()) {
                feedItem = beans.get(0);
                if(log.isLoggable(Level.WARNING)) {
                    int size = beans.size();
                    if(size > 1) {
                        if(channelFeed != null && !channelFeed.isEmpty()) {
                            // This should not happen...
                            log.warning("More than one FeedItem found for link = " + link + ". size >= " + size);
                        } else {
                            // Just return the most recent one....
                            if(log.isLoggable(Level.INFO)) log.info("More than one FeedItem found for link = " + link + ". size >= " + size);                            
                        }
                    }
                }
                // TBD:
                // get a full fetch object ???
                feedItem = getFeedItem(feedItem.getGuid());
                // ...
            } else {
                // ignore...
            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve feedItem list.", e);
//        }
            
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItemByLink() END: feedItem = " + feedItem);
        return feedItem;
    }

}
