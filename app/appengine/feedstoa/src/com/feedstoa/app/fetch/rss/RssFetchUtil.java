package com.feedstoa.app.fetch.rss;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.app.fetch.util.ChannelFeedAndItems;
import com.feedstoa.app.fetch.util.FetchUtil;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.UriStruct;


public class RssFetchUtil
{
    private static final Logger log = Logger.getLogger(RssFetchUtil.class.getName());

    // Static methods only.
    private RssFetchUtil() {}


   
    
    ///////////////////////////////////////////////////////////////////
    // TBD:
    // This added layer is to insulate third party libraries...
    ///////////////////////////////////////////////////////////////////
    

    public static FeedItem convertToFeedItem(RssItem rssItem)
    {
        return convertToFeedItem(rssItem, null);
    }
    public static FeedItem convertToFeedItem(RssItem rssItem, FeedItem feedItem)
    {
    	if(rssItem == null) {
    		return null;
    	}

    	FeedItemBean feedItemBean;
    	if(feedItem == null) {
    	    feedItemBean = new FeedItemBean();
    	} else {
    	    feedItemBean = (FeedItemBean) feedItem;
    	}

    	String title = rssItem.getTitle();
        if(title != null) {
        	feedItemBean.setTitle(title);
        }
//        List<UriStruct> atomLinks = rssItem.getLink();
//        if(atomLinks != null && !atomLinks.isEmpty()) {
//            String link = atomLinks.get(0).getHref();   // First element only.
//            if(link != null) {
//            	feedItem.setLink(link);
//            }        	
//        }
        String link = rssItem.getLink();
        if(link != null) {
            UriStructBean atomLink = new UriStructBean();
            atomLink.setHref(link);
            feedItemBean.setLink(atomLink);
        }
        String description = rssItem.getDescription();
        if(description != null) {
//            String summary = description;
//            if(description.length() > 500) {
//                summary = description.substring(0, 497) + "...";
//            }
//            feedItemBean.setSummary(summary);
            feedItemBean.setContent(description);
        }
        String comments = rssItem.getComments();
        if(comments != null) {
            if(comments.length() > 500) {
                if(log.isLoggable(Level.INFO)) log.info("Comments is too long. Will be truncated to 500 chars. Original comments = " + comments);
                comments = comments.substring(0, 500);  ///
            }
            feedItemBean.setComments(comments);  // ?????  Comments is a URL to comments page...
        }

        String author = rssItem.getAuthor();
        if(author != null) {
            String email = "";
            String name = null;
            int openIdx = author.indexOf("(");
            if(openIdx >= 0) {
                email = author.substring(0, openIdx);
                int closeIdx = author.lastIndexOf(")");  // ???
                if(closeIdx > 0) {
                    name = author.substring(openIdx+1, closeIdx-1);
                } else {
                    name = author.substring(openIdx+1);
                }
            } else {
                // ????
                if(FetchUtil.isEmailAddress(author)) {
                    email = author;
                } else {
                    name = author;
                }
            }
            UserStructBean atomAuthor = new UserStructBean();
            atomAuthor.setEmail(email.trim());
            if(name != null) {
                atomAuthor.setName(name.trim());
            }
            feedItemBean.setAuthor(atomAuthor);
        }
        List<CategoryStruct> atomCategories = rssItem.getCategory();
        if(atomCategories != null && !atomCategories.isEmpty()) {
        	List<CategoryStruct> rssCategories = new ArrayList<CategoryStruct>();
        	for(CategoryStruct a : atomCategories) {
        		CategoryStructBean r = new CategoryStructBean();
        		String domain = a.getDomain();
        		if(domain != null) {
        			r.setDomain(domain);
        		}
        		String label = a.getLabel();
        		if(label != null) {
        			r.setLabel(label);
        		}
        		rssCategories.add(r);
        	}
        	feedItemBean.setCategory(rssCategories);
        }
        EnclosureStruct enclosure = rssItem.getEnclosure();
        if(enclosure != null) {
        	feedItemBean.setEnclosure(enclosure);
        }
        String guid = rssItem.getGuid();     // ???
        if(guid != null) {
        	feedItemBean.setId(guid);  // ????
        }
        String pubDate = rssItem.getPubDate();
        if(pubDate != null) {
        	feedItemBean.setPubDate(pubDate);
        }
        UriStruct source = rssItem.getSource();
        if(source != null) {
        	feedItemBean.setSource(source);
        }
    	
    	return feedItemBean;
    }

    public static ChannelFeedAndItems convertToChannelFeed(RssChannel rssChannel)
    {
        return convertToChannelFeed(rssChannel, null);
    }
    public static ChannelFeedAndItems convertToChannelFeed(RssChannel rssChannel, ChannelFeed channelFeed)
    {
        return convertToChannelFeed(rssChannel, channelFeed, null);
    }
    public static ChannelFeedAndItems convertToChannelFeed(RssChannel rssChannel, ChannelFeed channelFeed, List<FeedItem> feedItems)
    {
    	if(rssChannel == null) {
    		return null;
    	}

    	ChannelFeedBean channelFeedBean = null;
    	if(channelFeed == null) {
    	    channelFeedBean = new ChannelFeedBean();
    	} else {
    	    channelFeedBean = (ChannelFeedBean) channelFeed;   
    	}

    	String title = rssChannel.getTitle();
        if(title != null) {
        	channelFeedBean.setTitle(title);
        }
//        List<UriStruct> atomLinks = rssChannel.getLink();
//        if(atomLinks != null && !atomLinks.isEmpty()) {
//            String link = atomLinks.get(0).getHref();   // First element only.
//            if(link != null) {
//            	channelFeed.setLink(link);
//            }        	
//        }
        String link = rssChannel.getLink();
        if(link != null) {
            UriStructBean atomLink = new UriStructBean();
            atomLink.setHref(link);
            channelFeedBean.setLink(atomLink);
        }
        String description = rssChannel.getDescription();
        if(description != null) {
        	channelFeedBean.setDescription(description);
        }
        String language = rssChannel.getLanguage();
        if(language != null) {
        	channelFeedBean.setLanguage(language);
        }
        String copyright = rssChannel.getCopyright();
        if(copyright != null) {
        	channelFeedBean.setCopyright(copyright);
        }
        String managingEditor = rssChannel.getManagingEditor();
        if(managingEditor != null) {
            String email = "";
            String name = null;
            int openIdx = managingEditor.indexOf("(");
            if(openIdx >= 0) {
                email = managingEditor.substring(0, openIdx);
                int closeIdx = managingEditor.lastIndexOf(")");  // ???
                if(closeIdx > 0) {
                    name = managingEditor.substring(openIdx+1, closeIdx-1);
                } else {
                    name = managingEditor.substring(openIdx+1);
                }
            } else {
                // ????
                if(FetchUtil.isEmailAddress(managingEditor)) {
                    email = managingEditor;
                } else {
                    name = managingEditor;
                }
            }
            UserStructBean atomManagingEditor = new UserStructBean();
            atomManagingEditor.setEmail(email.trim());
            if(name != null) {
                atomManagingEditor.setName(name.trim());
            }
        	channelFeedBean.setManagingEditor(atomManagingEditor);
        }

        String webMaster = rssChannel.getWebMaster();
        if(webMaster != null) {
            String email = "";
            String name = null;
            int openIdx = webMaster.indexOf("(");
            if(openIdx >= 0) {
                email = webMaster.substring(0, openIdx);
                int closeIdx = webMaster.lastIndexOf(")");  // ???
                if(closeIdx > 0) {
                    name = webMaster.substring(openIdx+1, closeIdx-1);
                } else {
                    name = webMaster.substring(openIdx+1);
                }
            } else {
                // ????
                if(FetchUtil.isEmailAddress(webMaster)) {
                    email = webMaster;
                } else {
                    name = webMaster;
                }
            }
            UserStructBean atomWebMaster = new UserStructBean();
            atomWebMaster.setEmail(email.trim());
            if(name != null) {
                atomWebMaster.setName(name.trim());
            }
            channelFeedBean.setWebMaster(atomWebMaster);
        }
        String pubDate = rssChannel.getPubDate();
        if(pubDate != null) {
        	channelFeedBean.setPubDate(pubDate);
        }
        String lastBuildDate = rssChannel.getLastBuildDate();
        if(lastBuildDate != null) {
        	channelFeedBean.setLastBuildDate(lastBuildDate);
        }
        List<CategoryStruct> atomCategories = rssChannel.getCategory();
        if(atomCategories != null && !atomCategories.isEmpty()) {
        	channelFeedBean.setCategory(atomCategories);
        }
        String generator = rssChannel.getGenerator();
        if(generator != null) {
        	channelFeedBean.setGenerator(generator);
        }
        String docs = rssChannel.getDocs();
        if(docs != null) {
        	channelFeedBean.setDocs(docs);
        }
        CloudStruct cloud = rssChannel.getCloud();
        if(cloud != null) {
        	channelFeedBean.setCloud(cloud);
        }
        Integer ttl = rssChannel.getTtl();
        if(ttl != null) {
        	channelFeedBean.setTtl(ttl);
        }
        ImageStruct image = rssChannel.getImage();
        if(image != null) {
        	channelFeedBean.setLogo(image);
        }
        String rating = rssChannel.getRating();
        if(rating != null) {
        	channelFeedBean.setRating(rating);
        }
        TextInputStruct textInput = rssChannel.getTextInput();
        if(textInput != null) {
        	channelFeedBean.setTextInput(textInput);
        }
        List<String> skipDays = rssChannel.getSkipDays();
        if(skipDays != null) {
        	channelFeedBean.setSkipDays(skipDays);
        }
        List<Integer> skipHours = rssChannel.getSkipHours();
        if(skipHours != null) {
        	channelFeedBean.setSkipHours(skipHours);
        }

        // ??????
        
        // TBD:
        // This does not really make sense
        // we are assuming rssItems and input feedItems are sorted in some way
        // and they "match" one-to-one...
        // ....
        
        List<FeedItem> feedItemBeans = new ArrayList<FeedItem>();
        List<RssItem> rssItems = rssChannel.getItem();
        if(rssItems != null) {
            if(channelFeed != null && feedItems != null && !feedItems.isEmpty()) {
                int r = rssItems.size();
                int f = feedItems.size();
                int limit = Math.min(r, f);
                for(int i=0; i<r; i++) {
                    RssItem ri = rssItems.get(i);
                    FeedItem fi = null;
                    if(i<limit) {
                        fi = convertToFeedItem(ri, feedItems.get(i));
                    } else {
                        fi = convertToFeedItem(ri);
                    }
                    feedItemBeans.add(fi);
                }
            } else {
                for(RssItem ri : rssItems) {
                    FeedItem fi = convertToFeedItem(ri);
                    feedItemBeans.add(fi);
                }
            }
        }
    	
        ChannelFeedAndItems channelFeedAndItems = new ChannelFeedAndItems(channelFeedBean, feedItemBeans);
    	return channelFeedAndItems;
    }
    

    // TBD
    // ....

    
    
}
