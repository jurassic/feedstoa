package com.feedstoa.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.resource.exception.NotImplementedRsException;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.GeoPointStruct;
import com.feedstoa.ws.StreetAddressStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.FullNameStruct;
import com.feedstoa.ws.GaeUserStruct;
import com.feedstoa.ws.User;
import com.feedstoa.ws.search.bean.UserQueryBean;
import com.feedstoa.ws.search.gae.UserIndexBuilder;
import com.feedstoa.af.config.Config;
import com.feedstoa.af.search.gae.UserQueryHelper;


public class UserSearchManager
{
    private static final Logger log = Logger.getLogger(UserSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private UserIndexBuilder userIndexBuilder;
    private UserQueryHelper userQueryHelper;
    
    private UserSearchManager()
    {
        userIndexBuilder = new UserIndexBuilder();
        userQueryHelper = new UserQueryHelper(userIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class UserSearchManagerHolder
    {
        private static final UserSearchManager INSTANCE = new UserSearchManager();
    }

    // Singleton method
    public static UserSearchManager getInstance()
    {
        return UserSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(User user)
    {
        return userIndexBuilder.addDocument(user);
    }


    // TBD:
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        return findUserQueryBeans(queryString, ordering, offset, count, null);
    }
    // Note: strCursor is an inout param. 
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count, StringCursor strCursor)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return userQueryHelper.findUserQueryBeans(queryString, ordering, offset, count, strCursor);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

