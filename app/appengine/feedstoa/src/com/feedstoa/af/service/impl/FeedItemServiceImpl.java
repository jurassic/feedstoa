package com.feedstoa.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.af.config.Config;

import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.GeoPointStructBean;
import com.feedstoa.af.bean.StreetAddressStructBean;
import com.feedstoa.af.bean.SiteLogBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.FullNameStructBean;
import com.feedstoa.af.bean.GeoCoordinateStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.GaeUserStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CellLatitudeLongitudeBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.KeyValuePairStructBean;
import com.feedstoa.af.bean.KeyValueRelationStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.PagerStateStructBean;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.UserWebsiteStructBean;
import com.feedstoa.af.bean.HelpNoticeBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.ContactInfoStructBean;
import com.feedstoa.af.bean.AppBrandStructBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.ExternalServiceApiKeyStructBean;
import com.feedstoa.af.bean.RssChannelBean;

import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.FeedItemService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FeedItemServiceImpl implements FeedItemService
{
    private static final Logger log = Logger.getLogger(FeedItemServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "FeedItem-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("FeedItem:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public FeedItemServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // FeedItem related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FeedItem getFeedItem(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedItem(): guid = " + guid);

        FeedItemBean bean = null;
        if(getCache() != null) {
            bean = (FeedItemBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FeedItemBean) getProxyFactory().getFeedItemServiceProxy().getFeedItem(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FeedItemBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedItemBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFeedItem(String guid, String field) throws BaseException
    {
        FeedItemBean bean = null;
        if(getCache() != null) {
            bean = (FeedItemBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FeedItemBean) getProxyFactory().getFeedItemServiceProxy().getFeedItem(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FeedItemBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedItemBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("fetchUrl")) {
            return bean.getFetchUrl();
        } else if(field.equals("feedUrl")) {
            return bean.getFeedUrl();
        } else if(field.equals("channelSource")) {
            return bean.getChannelSource();
        } else if(field.equals("channelFeed")) {
            return bean.getChannelFeed();
        } else if(field.equals("feedFormat")) {
            return bean.getFeedFormat();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("link")) {
            return bean.getLink();
        } else if(field.equals("summary")) {
            return bean.getSummary();
        } else if(field.equals("content")) {
            return bean.getContent();
        } else if(field.equals("author")) {
            return bean.getAuthor();
        } else if(field.equals("contributor")) {
            return bean.getContributor();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("comments")) {
            return bean.getComments();
        } else if(field.equals("enclosure")) {
            return bean.getEnclosure();
        } else if(field.equals("id")) {
            return bean.getId();
        } else if(field.equals("pubDate")) {
            return bean.getPubDate();
        } else if(field.equals("source")) {
            return bean.getSource();
        } else if(field.equals("feedContent")) {
            return bean.getFeedContent();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("publishedTime")) {
            return bean.getPublishedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FeedItem> getFeedItems(List<String> guids) throws BaseException
    {
        log.fine("getFeedItems()");

        // TBD: Is there a better way????
        List<FeedItem> feedItems = getProxyFactory().getFeedItemServiceProxy().getFeedItems(guids);
        if(feedItems == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedItemBean list.");
        }

        log.finer("END");
        return feedItems;
    }

    @Override
    public List<FeedItem> getAllFeedItems() throws BaseException
    {
        return getAllFeedItems(null, null, null);
    }


    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedItems(ordering, offset, count, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFeedItems(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedItem> feedItems = getProxyFactory().getFeedItemServiceProxy().getAllFeedItems(ordering, offset, count, forwardCursor);
        if(feedItems == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedItemBean list.");
        }

        log.finer("END");
        return feedItems;
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedItemKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFeedItemKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFeedItemServiceProxy().getAllFeedItemKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedItemBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FeedItemBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FeedItemServiceImpl.findFeedItems(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedItem> feedItems = getProxyFactory().getFeedItemServiceProxy().findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(feedItems == null) {
            log.log(Level.WARNING, "Failed to find feedItems for the given criterion.");
        }

        log.finer("END");
        return feedItems;
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FeedItemServiceImpl.findFeedItemKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFeedItemServiceProxy().findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FeedItem keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FeedItem key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FeedItemServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getFeedItemServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFeedItem(String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        UriStructBean linkBean = null;
        if(link instanceof UriStructBean) {
            linkBean = (UriStructBean) link;
        } else if(link instanceof UriStruct) {
            linkBean = new UriStructBean(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkBean = null;   // ????
        }
        UserStructBean authorBean = null;
        if(author instanceof UserStructBean) {
            authorBean = (UserStructBean) author;
        } else if(author instanceof UserStruct) {
            authorBean = new UserStructBean(author.getUuid(), author.getName(), author.getEmail(), author.getUrl());
        } else {
            authorBean = null;   // ????
        }
        EnclosureStructBean enclosureBean = null;
        if(enclosure instanceof EnclosureStructBean) {
            enclosureBean = (EnclosureStructBean) enclosure;
        } else if(enclosure instanceof EnclosureStruct) {
            enclosureBean = new EnclosureStructBean(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            enclosureBean = null;   // ????
        }
        UriStructBean sourceBean = null;
        if(source instanceof UriStructBean) {
            sourceBean = (UriStructBean) source;
        } else if(source instanceof UriStruct) {
            sourceBean = new UriStructBean(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            sourceBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        FeedItemBean bean = new FeedItemBean(null, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, linkBean, summary, content, authorBean, contributor, category, comments, enclosureBean, id, pubDate, sourceBean, feedContent, status, note, referrerInfoBean, publishedTime, lastUpdatedTime);
        return createFeedItem(bean);
    }

    @Override
    public String createFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FeedItem bean = constructFeedItem(feedItem);
        //return bean.getGuid();

        // Param feedItem cannot be null.....
        if(feedItem == null) {
            log.log(Level.INFO, "Param feedItem is null!");
            throw new BadRequestException("Param feedItem object is null!");
        }
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        } else if(feedItem instanceof FeedItem) {
            // bean = new FeedItemBean(null, feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FeedItemBean(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createFeedItem(): Arg feedItem is of an unknown type.");
            //bean = new FeedItemBean();
            bean = new FeedItemBean(feedItem.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFeedItemServiceProxy().createFeedItem(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public FeedItem constructFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedItem cannot be null.....
        if(feedItem == null) {
            log.log(Level.INFO, "Param feedItem is null!");
            throw new BadRequestException("Param feedItem object is null!");
        }
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        } else if(feedItem instanceof FeedItem) {
            // bean = new FeedItemBean(null, feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FeedItemBean(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createFeedItem(): Arg feedItem is of an unknown type.");
            //bean = new FeedItemBean();
            bean = new FeedItemBean(feedItem.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFeedItemServiceProxy().createFeedItem(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UriStructBean linkBean = null;
        if(link instanceof UriStructBean) {
            linkBean = (UriStructBean) link;
        } else if(link instanceof UriStruct) {
            linkBean = new UriStructBean(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkBean = null;   // ????
        }
        UserStructBean authorBean = null;
        if(author instanceof UserStructBean) {
            authorBean = (UserStructBean) author;
        } else if(author instanceof UserStruct) {
            authorBean = new UserStructBean(author.getUuid(), author.getName(), author.getEmail(), author.getUrl());
        } else {
            authorBean = null;   // ????
        }
        EnclosureStructBean enclosureBean = null;
        if(enclosure instanceof EnclosureStructBean) {
            enclosureBean = (EnclosureStructBean) enclosure;
        } else if(enclosure instanceof EnclosureStruct) {
            enclosureBean = new EnclosureStructBean(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            enclosureBean = null;   // ????
        }
        UriStructBean sourceBean = null;
        if(source instanceof UriStructBean) {
            sourceBean = (UriStructBean) source;
        } else if(source instanceof UriStruct) {
            sourceBean = new UriStructBean(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            sourceBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        FeedItemBean bean = new FeedItemBean(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, linkBean, summary, content, authorBean, contributor, category, comments, enclosureBean, id, pubDate, sourceBean, feedContent, status, note, referrerInfoBean, publishedTime, lastUpdatedTime);
        return updateFeedItem(bean);
    }
        
    @Override
    public Boolean updateFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FeedItem bean = refreshFeedItem(feedItem);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param feedItem cannot be null.....
        if(feedItem == null || feedItem.getGuid() == null) {
            log.log(Level.INFO, "Param feedItem or its guid is null!");
            throw new BadRequestException("Param feedItem object or its guid is null!");
        }
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        } else {  // if(feedItem instanceof FeedItem)
            bean = new FeedItemBean(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getFeedItemServiceProxy().updateFeedItem(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public FeedItem refreshFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedItem cannot be null.....
        if(feedItem == null || feedItem.getGuid() == null) {
            log.log(Level.INFO, "Param feedItem or its guid is null!");
            throw new BadRequestException("Param feedItem object or its guid is null!");
        }
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        } else {  // if(feedItem instanceof FeedItem)
            bean = new FeedItemBean(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getFeedItemServiceProxy().updateFeedItem(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteFeedItem(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getFeedItemServiceProxy().deleteFeedItem(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            FeedItem feedItem = null;
            try {
                feedItem = getProxyFactory().getFeedItemServiceProxy().getFeedItem(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch feedItem with a key, " + guid);
                return false;
            }
            if(feedItem != null) {
                String beanGuid = feedItem.getGuid();
                Boolean suc1 = getProxyFactory().getFeedItemServiceProxy().deleteFeedItem(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("feedItem with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedItem cannot be null.....
        if(feedItem == null || feedItem.getGuid() == null) {
            log.log(Level.INFO, "Param feedItem or its guid is null!");
            throw new BadRequestException("Param feedItem object or its guid is null!");
        }
        FeedItemBean bean = null;
        if(feedItem instanceof FeedItemBean) {
            bean = (FeedItemBean) feedItem;
        } else {  // if(feedItem instanceof FeedItem)
            // ????
            log.warning("feedItem is not an instance of FeedItemBean.");
            bean = new FeedItemBean(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructBean) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructBean) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructBean) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructBean) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructBean) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getFeedItemServiceProxy().deleteFeedItem(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFeedItems(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getFeedItemServiceProxy().deleteFeedItems(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFeedItems(List<FeedItem> feedItems) throws BaseException
    {
        log.finer("BEGIN");

        if(feedItems == null) {
            log.log(Level.WARNING, "createFeedItems() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = feedItems.size();
        if(size == 0) {
            log.log(Level.WARNING, "createFeedItems() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(FeedItem feedItem : feedItems) {
            String guid = createFeedItem(feedItem);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createFeedItems() failed for at least one feedItem. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateFeedItems(List<FeedItem> feedItems) throws BaseException
    //{
    //}

}
