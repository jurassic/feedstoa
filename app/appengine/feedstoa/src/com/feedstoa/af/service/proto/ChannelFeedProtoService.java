package com.feedstoa.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.impl.ChannelFeedServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class ChannelFeedProtoService extends ChannelFeedServiceImpl implements ChannelFeedService
{
    private static final Logger log = Logger.getLogger(ChannelFeedProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ChannelFeedProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ChannelFeed related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        return super.getChannelFeed(guid);
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        return super.getChannelFeed(guid, field);
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        return super.getChannelFeeds(guids);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return super.getAllChannelFeeds();
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllChannelFeeds(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllChannelFeedKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        return super.createChannelFeed(channelFeed);
    }

    @Override
    public ChannelFeed constructChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        return super.constructChannelFeed(channelFeed);
    }


    @Override
    public Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        return super.updateChannelFeed(channelFeed);
    }
        
    @Override
    public ChannelFeed refreshChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        return super.refreshChannelFeed(channelFeed);
    }

    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        return super.deleteChannelFeed(guid);
    }

    @Override
    public Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        return super.deleteChannelFeed(channelFeed);
    }

    @Override
    public Integer createChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    {
        return super.createChannelFeeds(channelFeeds);
    }

    // TBD
    //@Override
    //public Boolean updateChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    //{
    //}

}
