package com.feedstoa.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.af.config.Config;

import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.GeoPointStructBean;
import com.feedstoa.af.bean.StreetAddressStructBean;
import com.feedstoa.af.bean.SiteLogBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.FullNameStructBean;
import com.feedstoa.af.bean.GeoCoordinateStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.GaeUserStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CellLatitudeLongitudeBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.KeyValuePairStructBean;
import com.feedstoa.af.bean.KeyValueRelationStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.PagerStateStructBean;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.UserWebsiteStructBean;
import com.feedstoa.af.bean.HelpNoticeBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.ContactInfoStructBean;
import com.feedstoa.af.bean.AppBrandStructBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.ExternalServiceApiKeyStructBean;
import com.feedstoa.af.bean.RssChannelBean;

import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.ChannelFeedService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ChannelFeedServiceImpl implements ChannelFeedService
{
    private static final Logger log = Logger.getLogger(ChannelFeedServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ChannelFeed-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ChannelFeed:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ChannelFeedServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ChannelFeed related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelFeed(): guid = " + guid);

        ChannelFeedBean bean = null;
        if(getCache() != null) {
            bean = (ChannelFeedBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ChannelFeedBean) getProxyFactory().getChannelFeedServiceProxy().getChannelFeed(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ChannelFeedBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelFeedBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        ChannelFeedBean bean = null;
        if(getCache() != null) {
            bean = (ChannelFeedBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ChannelFeedBean) getProxyFactory().getChannelFeedServiceProxy().getChannelFeed(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ChannelFeedBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelFeedBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("channelSource")) {
            return bean.getChannelSource();
        } else if(field.equals("channelCode")) {
            return bean.getChannelCode();
        } else if(field.equals("previousVersion")) {
            return bean.getPreviousVersion();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("fetchUrl")) {
            return bean.getFetchUrl();
        } else if(field.equals("feedServiceUrl")) {
            return bean.getFeedServiceUrl();
        } else if(field.equals("feedUrl")) {
            return bean.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return bean.getFeedFormat();
        } else if(field.equals("maxItemCount")) {
            return bean.getMaxItemCount();
        } else if(field.equals("feedCategory")) {
            return bean.getFeedCategory();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("subtitle")) {
            return bean.getSubtitle();
        } else if(field.equals("link")) {
            return bean.getLink();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("copyright")) {
            return bean.getCopyright();
        } else if(field.equals("managingEditor")) {
            return bean.getManagingEditor();
        } else if(field.equals("webMaster")) {
            return bean.getWebMaster();
        } else if(field.equals("contributor")) {
            return bean.getContributor();
        } else if(field.equals("pubDate")) {
            return bean.getPubDate();
        } else if(field.equals("lastBuildDate")) {
            return bean.getLastBuildDate();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("generator")) {
            return bean.getGenerator();
        } else if(field.equals("docs")) {
            return bean.getDocs();
        } else if(field.equals("cloud")) {
            return bean.getCloud();
        } else if(field.equals("ttl")) {
            return bean.getTtl();
        } else if(field.equals("logo")) {
            return bean.getLogo();
        } else if(field.equals("icon")) {
            return bean.getIcon();
        } else if(field.equals("rating")) {
            return bean.getRating();
        } else if(field.equals("textInput")) {
            return bean.getTextInput();
        } else if(field.equals("skipHours")) {
            return bean.getSkipHours();
        } else if(field.equals("skipDays")) {
            return bean.getSkipDays();
        } else if(field.equals("outputText")) {
            return bean.getOutputText();
        } else if(field.equals("outputHash")) {
            return bean.getOutputHash();
        } else if(field.equals("feedContent")) {
            return bean.getFeedContent();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("lastBuildTime")) {
            return bean.getLastBuildTime();
        } else if(field.equals("publishedTime")) {
            return bean.getPublishedTime();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        log.fine("getChannelFeeds()");

        // TBD: Is there a better way????
        List<ChannelFeed> channelFeeds = getProxyFactory().getChannelFeedServiceProxy().getChannelFeeds(guids);
        if(channelFeeds == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelFeedBean list.");
        }

        log.finer("END");
        return channelFeeds;
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return getAllChannelFeeds(null, null, null);
    }


    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllChannelFeeds(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelFeed> channelFeeds = getProxyFactory().getChannelFeedServiceProxy().getAllChannelFeeds(ordering, offset, count, forwardCursor);
        if(channelFeeds == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelFeedBean list.");
        }

        log.finer("END");
        return channelFeeds;
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllChannelFeedKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getChannelFeedServiceProxy().getAllChannelFeedKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelFeedBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ChannelFeedBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ChannelFeedServiceImpl.findChannelFeeds(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelFeed> channelFeeds = getProxyFactory().getChannelFeedServiceProxy().findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(channelFeeds == null) {
            log.log(Level.WARNING, "Failed to find channelFeeds for the given criterion.");
        }

        log.finer("END");
        return channelFeeds;
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ChannelFeedServiceImpl.findChannelFeedKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getChannelFeedServiceProxy().findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ChannelFeed keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ChannelFeed key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ChannelFeedServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getChannelFeedServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        UriStructBean linkBean = null;
        if(link instanceof UriStructBean) {
            linkBean = (UriStructBean) link;
        } else if(link instanceof UriStruct) {
            linkBean = new UriStructBean(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkBean = null;   // ????
        }
        UserStructBean managingEditorBean = null;
        if(managingEditor instanceof UserStructBean) {
            managingEditorBean = (UserStructBean) managingEditor;
        } else if(managingEditor instanceof UserStruct) {
            managingEditorBean = new UserStructBean(managingEditor.getUuid(), managingEditor.getName(), managingEditor.getEmail(), managingEditor.getUrl());
        } else {
            managingEditorBean = null;   // ????
        }
        UserStructBean webMasterBean = null;
        if(webMaster instanceof UserStructBean) {
            webMasterBean = (UserStructBean) webMaster;
        } else if(webMaster instanceof UserStruct) {
            webMasterBean = new UserStructBean(webMaster.getUuid(), webMaster.getName(), webMaster.getEmail(), webMaster.getUrl());
        } else {
            webMasterBean = null;   // ????
        }
        CloudStructBean cloudBean = null;
        if(cloud instanceof CloudStructBean) {
            cloudBean = (CloudStructBean) cloud;
        } else if(cloud instanceof CloudStruct) {
            cloudBean = new CloudStructBean(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            cloudBean = null;   // ????
        }
        ImageStructBean logoBean = null;
        if(logo instanceof ImageStructBean) {
            logoBean = (ImageStructBean) logo;
        } else if(logo instanceof ImageStruct) {
            logoBean = new ImageStructBean(logo.getUrl(), logo.getTitle(), logo.getLink(), logo.getWidth(), logo.getHeight(), logo.getDescription());
        } else {
            logoBean = null;   // ????
        }
        ImageStructBean iconBean = null;
        if(icon instanceof ImageStructBean) {
            iconBean = (ImageStructBean) icon;
        } else if(icon instanceof ImageStruct) {
            iconBean = new ImageStructBean(icon.getUrl(), icon.getTitle(), icon.getLink(), icon.getWidth(), icon.getHeight(), icon.getDescription());
        } else {
            iconBean = null;   // ????
        }
        TextInputStructBean textInputBean = null;
        if(textInput instanceof TextInputStructBean) {
            textInputBean = (TextInputStructBean) textInput;
        } else if(textInput instanceof TextInputStruct) {
            textInputBean = new TextInputStructBean(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            textInputBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        ChannelFeedBean bean = new ChannelFeedBean(null, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, linkBean, description, language, copyright, managingEditorBean, webMasterBean, contributor, pubDate, lastBuildDate, category, generator, docs, cloudBean, ttl, logoBean, iconBean, rating, textInputBean, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfoBean, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return createChannelFeed(bean);
    }

    @Override
    public String createChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ChannelFeed bean = constructChannelFeed(channelFeed);
        //return bean.getGuid();

        // Param channelFeed cannot be null.....
        if(channelFeed == null) {
            log.log(Level.INFO, "Param channelFeed is null!");
            throw new BadRequestException("Param channelFeed object is null!");
        }
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        } else if(channelFeed instanceof ChannelFeed) {
            // bean = new ChannelFeedBean(null, channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ChannelFeedBean(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createChannelFeed(): Arg channelFeed is of an unknown type.");
            //bean = new ChannelFeedBean();
            bean = new ChannelFeedBean(channelFeed.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getChannelFeedServiceProxy().createChannelFeed(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ChannelFeed constructChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelFeed cannot be null.....
        if(channelFeed == null) {
            log.log(Level.INFO, "Param channelFeed is null!");
            throw new BadRequestException("Param channelFeed object is null!");
        }
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        } else if(channelFeed instanceof ChannelFeed) {
            // bean = new ChannelFeedBean(null, channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ChannelFeedBean(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createChannelFeed(): Arg channelFeed is of an unknown type.");
            //bean = new ChannelFeedBean();
            bean = new ChannelFeedBean(channelFeed.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getChannelFeedServiceProxy().createChannelFeed(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UriStructBean linkBean = null;
        if(link instanceof UriStructBean) {
            linkBean = (UriStructBean) link;
        } else if(link instanceof UriStruct) {
            linkBean = new UriStructBean(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkBean = null;   // ????
        }
        UserStructBean managingEditorBean = null;
        if(managingEditor instanceof UserStructBean) {
            managingEditorBean = (UserStructBean) managingEditor;
        } else if(managingEditor instanceof UserStruct) {
            managingEditorBean = new UserStructBean(managingEditor.getUuid(), managingEditor.getName(), managingEditor.getEmail(), managingEditor.getUrl());
        } else {
            managingEditorBean = null;   // ????
        }
        UserStructBean webMasterBean = null;
        if(webMaster instanceof UserStructBean) {
            webMasterBean = (UserStructBean) webMaster;
        } else if(webMaster instanceof UserStruct) {
            webMasterBean = new UserStructBean(webMaster.getUuid(), webMaster.getName(), webMaster.getEmail(), webMaster.getUrl());
        } else {
            webMasterBean = null;   // ????
        }
        CloudStructBean cloudBean = null;
        if(cloud instanceof CloudStructBean) {
            cloudBean = (CloudStructBean) cloud;
        } else if(cloud instanceof CloudStruct) {
            cloudBean = new CloudStructBean(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            cloudBean = null;   // ????
        }
        ImageStructBean logoBean = null;
        if(logo instanceof ImageStructBean) {
            logoBean = (ImageStructBean) logo;
        } else if(logo instanceof ImageStruct) {
            logoBean = new ImageStructBean(logo.getUrl(), logo.getTitle(), logo.getLink(), logo.getWidth(), logo.getHeight(), logo.getDescription());
        } else {
            logoBean = null;   // ????
        }
        ImageStructBean iconBean = null;
        if(icon instanceof ImageStructBean) {
            iconBean = (ImageStructBean) icon;
        } else if(icon instanceof ImageStruct) {
            iconBean = new ImageStructBean(icon.getUrl(), icon.getTitle(), icon.getLink(), icon.getWidth(), icon.getHeight(), icon.getDescription());
        } else {
            iconBean = null;   // ????
        }
        TextInputStructBean textInputBean = null;
        if(textInput instanceof TextInputStructBean) {
            textInputBean = (TextInputStructBean) textInput;
        } else if(textInput instanceof TextInputStruct) {
            textInputBean = new TextInputStructBean(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            textInputBean = null;   // ????
        }
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        ChannelFeedBean bean = new ChannelFeedBean(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, linkBean, description, language, copyright, managingEditorBean, webMasterBean, contributor, pubDate, lastBuildDate, category, generator, docs, cloudBean, ttl, logoBean, iconBean, rating, textInputBean, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfoBean, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return updateChannelFeed(bean);
    }
        
    @Override
    public Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ChannelFeed bean = refreshChannelFeed(channelFeed);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param channelFeed cannot be null.....
        if(channelFeed == null || channelFeed.getGuid() == null) {
            log.log(Level.INFO, "Param channelFeed or its guid is null!");
            throw new BadRequestException("Param channelFeed object or its guid is null!");
        }
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        } else {  // if(channelFeed instanceof ChannelFeed)
            bean = new ChannelFeedBean(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getChannelFeedServiceProxy().updateChannelFeed(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ChannelFeed refreshChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelFeed cannot be null.....
        if(channelFeed == null || channelFeed.getGuid() == null) {
            log.log(Level.INFO, "Param channelFeed or its guid is null!");
            throw new BadRequestException("Param channelFeed object or its guid is null!");
        }
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        } else {  // if(channelFeed instanceof ChannelFeed)
            bean = new ChannelFeedBean(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getChannelFeedServiceProxy().updateChannelFeed(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getChannelFeedServiceProxy().deleteChannelFeed(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ChannelFeed channelFeed = null;
            try {
                channelFeed = getProxyFactory().getChannelFeedServiceProxy().getChannelFeed(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch channelFeed with a key, " + guid);
                return false;
            }
            if(channelFeed != null) {
                String beanGuid = channelFeed.getGuid();
                Boolean suc1 = getProxyFactory().getChannelFeedServiceProxy().deleteChannelFeed(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("channelFeed with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelFeed cannot be null.....
        if(channelFeed == null || channelFeed.getGuid() == null) {
            log.log(Level.INFO, "Param channelFeed or its guid is null!");
            throw new BadRequestException("Param channelFeed object or its guid is null!");
        }
        ChannelFeedBean bean = null;
        if(channelFeed instanceof ChannelFeedBean) {
            bean = (ChannelFeedBean) channelFeed;
        } else {  // if(channelFeed instanceof ChannelFeed)
            // ????
            log.warning("channelFeed is not an instance of ChannelFeedBean.");
            bean = new ChannelFeedBean(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructBean) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructBean) channelFeed.getManagingEditor(), (UserStructBean) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructBean) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructBean) channelFeed.getLogo(), (ImageStructBean) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructBean) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructBean) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getChannelFeedServiceProxy().deleteChannelFeed(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getChannelFeedServiceProxy().deleteChannelFeeds(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    {
        log.finer("BEGIN");

        if(channelFeeds == null) {
            log.log(Level.WARNING, "createChannelFeeds() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = channelFeeds.size();
        if(size == 0) {
            log.log(Level.WARNING, "createChannelFeeds() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ChannelFeed channelFeed : channelFeeds) {
            String guid = createChannelFeed(channelFeed);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createChannelFeeds() failed for at least one channelFeed. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException
    //{
    //}

}
