package com.feedstoa.af.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ChannelFeedService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ChannelFeed getChannelFeed(String guid) throws BaseException;
    Object getChannelFeed(String guid, String field) throws BaseException;
    List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException;
    List<ChannelFeed> getAllChannelFeeds() throws BaseException;
    /* @Deprecated */ List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException;
    List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException;
    //String createChannelFeed(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ChannelFeed?)
    String createChannelFeed(ChannelFeed channelFeed) throws BaseException;
    ChannelFeed constructChannelFeed(ChannelFeed channelFeed) throws BaseException;
    Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException;
    //Boolean updateChannelFeed(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException;
    ChannelFeed refreshChannelFeed(ChannelFeed channelFeed) throws BaseException;
    Boolean deleteChannelFeed(String guid) throws BaseException;
    Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException;
    Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException;
//    Boolean updateChannelFeeds(List<ChannelFeed> channelFeeds) throws BaseException;

}
