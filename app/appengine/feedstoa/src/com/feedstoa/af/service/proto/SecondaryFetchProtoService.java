package com.feedstoa.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.impl.SecondaryFetchServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class SecondaryFetchProtoService extends SecondaryFetchServiceImpl implements SecondaryFetchService
{
    private static final Logger log = Logger.getLogger(SecondaryFetchProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public SecondaryFetchProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // SecondaryFetch related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public SecondaryFetch getSecondaryFetch(String guid) throws BaseException
    {
        return super.getSecondaryFetch(guid);
    }

    @Override
    public Object getSecondaryFetch(String guid, String field) throws BaseException
    {
        return super.getSecondaryFetch(guid, field);
    }

    @Override
    public List<SecondaryFetch> getSecondaryFetches(List<String> guids) throws BaseException
    {
        return super.getSecondaryFetches(guids);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches() throws BaseException
    {
        return super.getAllSecondaryFetches();
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllSecondaryFetches(ordering, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllSecondaryFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllSecondaryFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllSecondaryFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        return super.createSecondaryFetch(secondaryFetch);
    }

    @Override
    public SecondaryFetch constructSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        return super.constructSecondaryFetch(secondaryFetch);
    }


    @Override
    public Boolean updateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        return super.updateSecondaryFetch(secondaryFetch);
    }
        
    @Override
    public SecondaryFetch refreshSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        return super.refreshSecondaryFetch(secondaryFetch);
    }

    @Override
    public Boolean deleteSecondaryFetch(String guid) throws BaseException
    {
        return super.deleteSecondaryFetch(guid);
    }

    @Override
    public Boolean deleteSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        return super.deleteSecondaryFetch(secondaryFetch);
    }

    @Override
    public Integer createSecondaryFetches(List<SecondaryFetch> secondaryFetches) throws BaseException
    {
        return super.createSecondaryFetches(secondaryFetches);
    }

    // TBD
    //@Override
    //public Boolean updateSecondaryFetches(List<SecondaryFetch> secondaryFetches) throws BaseException
    //{
    //}

}
