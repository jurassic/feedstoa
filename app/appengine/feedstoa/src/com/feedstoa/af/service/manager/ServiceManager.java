package com.feedstoa.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.service.AbstractServiceFactory;
import com.feedstoa.af.service.ApiConsumerService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.af.service.ServiceInfoService;
import com.feedstoa.af.service.FiveTenService;

// TBD:
// Factory? DI? (Does the current "dual" approach make sense?)
// Make it a singleton? Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

    // Reference to the Abstract factory.
    private static AbstractServiceFactory serviceFactory = ServiceFactoryManager.getServiceFactory();

    // All service getters/setters are delegated to ServiceController.
    // TBD: Use a DI framework such as Spring or Guice....
    private static ServiceController serviceController = new ServiceController(null, null, null, null, null, null, null, null, null, null);

    // Static methods only.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(serviceController.getApiConsumerService() == null) {
            serviceController.setApiConsumerService(serviceFactory.getApiConsumerService());
        }
        return serviceController.getApiConsumerService();
    }
    // Injects a ApiConsumerService instance.
	public static void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        serviceController.setApiConsumerService(apiConsumerService);
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(serviceController.getUserService() == null) {
            serviceController.setUserService(serviceFactory.getUserService());
        }
        return serviceController.getUserService();
    }
    // Injects a UserService instance.
	public static void setUserService(UserService userService) 
    {
        serviceController.setUserService(userService);
    }

    // Returns a FetchRequestService instance.
	public static FetchRequestService getFetchRequestService() 
    {
        if(serviceController.getFetchRequestService() == null) {
            serviceController.setFetchRequestService(serviceFactory.getFetchRequestService());
        }
        return serviceController.getFetchRequestService();
    }
    // Injects a FetchRequestService instance.
	public static void setFetchRequestService(FetchRequestService fetchRequestService) 
    {
        serviceController.setFetchRequestService(fetchRequestService);
    }

    // Returns a SecondaryFetchService instance.
	public static SecondaryFetchService getSecondaryFetchService() 
    {
        if(serviceController.getSecondaryFetchService() == null) {
            serviceController.setSecondaryFetchService(serviceFactory.getSecondaryFetchService());
        }
        return serviceController.getSecondaryFetchService();
    }
    // Injects a SecondaryFetchService instance.
	public static void setSecondaryFetchService(SecondaryFetchService secondaryFetchService) 
    {
        serviceController.setSecondaryFetchService(secondaryFetchService);
    }

    // Returns a ChannelSourceService instance.
	public static ChannelSourceService getChannelSourceService() 
    {
        if(serviceController.getChannelSourceService() == null) {
            serviceController.setChannelSourceService(serviceFactory.getChannelSourceService());
        }
        return serviceController.getChannelSourceService();
    }
    // Injects a ChannelSourceService instance.
	public static void setChannelSourceService(ChannelSourceService channelSourceService) 
    {
        serviceController.setChannelSourceService(channelSourceService);
    }

    // Returns a ChannelFeedService instance.
	public static ChannelFeedService getChannelFeedService() 
    {
        if(serviceController.getChannelFeedService() == null) {
            serviceController.setChannelFeedService(serviceFactory.getChannelFeedService());
        }
        return serviceController.getChannelFeedService();
    }
    // Injects a ChannelFeedService instance.
	public static void setChannelFeedService(ChannelFeedService channelFeedService) 
    {
        serviceController.setChannelFeedService(channelFeedService);
    }

    // Returns a FeedItemService instance.
	public static FeedItemService getFeedItemService() 
    {
        if(serviceController.getFeedItemService() == null) {
            serviceController.setFeedItemService(serviceFactory.getFeedItemService());
        }
        return serviceController.getFeedItemService();
    }
    // Injects a FeedItemService instance.
	public static void setFeedItemService(FeedItemService feedItemService) 
    {
        serviceController.setFeedItemService(feedItemService);
    }

    // Returns a FeedContentService instance.
	public static FeedContentService getFeedContentService() 
    {
        if(serviceController.getFeedContentService() == null) {
            serviceController.setFeedContentService(serviceFactory.getFeedContentService());
        }
        return serviceController.getFeedContentService();
    }
    // Injects a FeedContentService instance.
	public static void setFeedContentService(FeedContentService feedContentService) 
    {
        serviceController.setFeedContentService(feedContentService);
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(serviceController.getServiceInfoService() == null) {
            serviceController.setServiceInfoService(serviceFactory.getServiceInfoService());
        }
        return serviceController.getServiceInfoService();
    }
    // Injects a ServiceInfoService instance.
	public static void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        serviceController.setServiceInfoService(serviceInfoService);
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(serviceController.getFiveTenService() == null) {
            serviceController.setFiveTenService(serviceFactory.getFiveTenService());
        }
        return serviceController.getFiveTenService();
    }
    // Injects a FiveTenService instance.
	public static void setFiveTenService(FiveTenService fiveTenService) 
    {
        serviceController.setFiveTenService(fiveTenService);
    }

}
