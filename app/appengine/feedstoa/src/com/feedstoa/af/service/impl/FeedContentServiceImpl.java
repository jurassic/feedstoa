package com.feedstoa.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.af.config.Config;

import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.GeoPointStructBean;
import com.feedstoa.af.bean.StreetAddressStructBean;
import com.feedstoa.af.bean.SiteLogBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.FullNameStructBean;
import com.feedstoa.af.bean.GeoCoordinateStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.GaeUserStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CellLatitudeLongitudeBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.KeyValuePairStructBean;
import com.feedstoa.af.bean.KeyValueRelationStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.PagerStateStructBean;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.UserWebsiteStructBean;
import com.feedstoa.af.bean.HelpNoticeBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.ContactInfoStructBean;
import com.feedstoa.af.bean.AppBrandStructBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.ExternalServiceApiKeyStructBean;
import com.feedstoa.af.bean.RssChannelBean;

import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.FeedContentService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FeedContentServiceImpl implements FeedContentService
{
    private static final Logger log = Logger.getLogger(FeedContentServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "FeedContent-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("FeedContent:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public FeedContentServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // FeedContent related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FeedContent getFeedContent(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFeedContent(): guid = " + guid);

        FeedContentBean bean = null;
        if(getCache() != null) {
            bean = (FeedContentBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FeedContentBean) getProxyFactory().getFeedContentServiceProxy().getFeedContent(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FeedContentBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedContentBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFeedContent(String guid, String field) throws BaseException
    {
        FeedContentBean bean = null;
        if(getCache() != null) {
            bean = (FeedContentBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FeedContentBean) getProxyFactory().getFeedContentServiceProxy().getFeedContent(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FeedContentBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedContentBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("channelSource")) {
            return bean.getChannelSource();
        } else if(field.equals("channelFeed")) {
            return bean.getChannelFeed();
        } else if(field.equals("channelVersion")) {
            return bean.getChannelVersion();
        } else if(field.equals("feedUrl")) {
            return bean.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return bean.getFeedFormat();
        } else if(field.equals("content")) {
            return bean.getContent();
        } else if(field.equals("contentHash")) {
            return bean.getContentHash();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("lastBuildDate")) {
            return bean.getLastBuildDate();
        } else if(field.equals("lastBuildTime")) {
            return bean.getLastBuildTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FeedContent> getFeedContents(List<String> guids) throws BaseException
    {
        log.fine("getFeedContents()");

        // TBD: Is there a better way????
        List<FeedContent> feedContents = getProxyFactory().getFeedContentServiceProxy().getFeedContents(guids);
        if(feedContents == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedContentBean list.");
        }

        log.finer("END");
        return feedContents;
    }

    @Override
    public List<FeedContent> getAllFeedContents() throws BaseException
    {
        return getAllFeedContents(null, null, null);
    }


    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedContents(ordering, offset, count, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFeedContents(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedContent> feedContents = getProxyFactory().getFeedContentServiceProxy().getAllFeedContents(ordering, offset, count, forwardCursor);
        if(feedContents == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedContentBean list.");
        }

        log.finer("END");
        return feedContents;
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedContentKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFeedContentKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFeedContentServiceProxy().getAllFeedContentKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedContentBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FeedContentBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFeedContents(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FeedContentServiceImpl.findFeedContents(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedContent> feedContents = getProxyFactory().getFeedContentServiceProxy().findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(feedContents == null) {
            log.log(Level.WARNING, "Failed to find feedContents for the given criterion.");
        }

        log.finer("END");
        return feedContents;
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FeedContentServiceImpl.findFeedContentKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFeedContentServiceProxy().findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FeedContent keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FeedContent key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FeedContentServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getFeedContentServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFeedContent(String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        FeedContentBean bean = new FeedContentBean(null, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        return createFeedContent(bean);
    }

    @Override
    public String createFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FeedContent bean = constructFeedContent(feedContent);
        //return bean.getGuid();

        // Param feedContent cannot be null.....
        if(feedContent == null) {
            log.log(Level.INFO, "Param feedContent is null!");
            throw new BadRequestException("Param feedContent object is null!");
        }
        FeedContentBean bean = null;
        if(feedContent instanceof FeedContentBean) {
            bean = (FeedContentBean) feedContent;
        } else if(feedContent instanceof FeedContent) {
            // bean = new FeedContentBean(null, feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FeedContentBean(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        } else {
            log.log(Level.WARNING, "createFeedContent(): Arg feedContent is of an unknown type.");
            //bean = new FeedContentBean();
            bean = new FeedContentBean(feedContent.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFeedContentServiceProxy().createFeedContent(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public FeedContent constructFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedContent cannot be null.....
        if(feedContent == null) {
            log.log(Level.INFO, "Param feedContent is null!");
            throw new BadRequestException("Param feedContent object is null!");
        }
        FeedContentBean bean = null;
        if(feedContent instanceof FeedContentBean) {
            bean = (FeedContentBean) feedContent;
        } else if(feedContent instanceof FeedContent) {
            // bean = new FeedContentBean(null, feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FeedContentBean(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        } else {
            log.log(Level.WARNING, "createFeedContent(): Arg feedContent is of an unknown type.");
            //bean = new FeedContentBean();
            bean = new FeedContentBean(feedContent.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFeedContentServiceProxy().createFeedContent(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateFeedContent(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FeedContentBean bean = new FeedContentBean(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        return updateFeedContent(bean);
    }
        
    @Override
    public Boolean updateFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FeedContent bean = refreshFeedContent(feedContent);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param feedContent cannot be null.....
        if(feedContent == null || feedContent.getGuid() == null) {
            log.log(Level.INFO, "Param feedContent or its guid is null!");
            throw new BadRequestException("Param feedContent object or its guid is null!");
        }
        FeedContentBean bean = null;
        if(feedContent instanceof FeedContentBean) {
            bean = (FeedContentBean) feedContent;
        } else {  // if(feedContent instanceof FeedContent)
            bean = new FeedContentBean(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        }
        Boolean suc = getProxyFactory().getFeedContentServiceProxy().updateFeedContent(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public FeedContent refreshFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedContent cannot be null.....
        if(feedContent == null || feedContent.getGuid() == null) {
            log.log(Level.INFO, "Param feedContent or its guid is null!");
            throw new BadRequestException("Param feedContent object or its guid is null!");
        }
        FeedContentBean bean = null;
        if(feedContent instanceof FeedContentBean) {
            bean = (FeedContentBean) feedContent;
        } else {  // if(feedContent instanceof FeedContent)
            bean = new FeedContentBean(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        }
        Boolean suc = getProxyFactory().getFeedContentServiceProxy().updateFeedContent(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteFeedContent(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getFeedContentServiceProxy().deleteFeedContent(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            FeedContent feedContent = null;
            try {
                feedContent = getProxyFactory().getFeedContentServiceProxy().getFeedContent(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch feedContent with a key, " + guid);
                return false;
            }
            if(feedContent != null) {
                String beanGuid = feedContent.getGuid();
                Boolean suc1 = getProxyFactory().getFeedContentServiceProxy().deleteFeedContent(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("feedContent with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedContent cannot be null.....
        if(feedContent == null || feedContent.getGuid() == null) {
            log.log(Level.INFO, "Param feedContent or its guid is null!");
            throw new BadRequestException("Param feedContent object or its guid is null!");
        }
        FeedContentBean bean = null;
        if(feedContent instanceof FeedContentBean) {
            bean = (FeedContentBean) feedContent;
        } else {  // if(feedContent instanceof FeedContent)
            // ????
            log.warning("feedContent is not an instance of FeedContentBean.");
            bean = new FeedContentBean(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        }
        Boolean suc = getProxyFactory().getFeedContentServiceProxy().deleteFeedContent(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFeedContents(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getFeedContentServiceProxy().deleteFeedContents(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFeedContents(List<FeedContent> feedContents) throws BaseException
    {
        log.finer("BEGIN");

        if(feedContents == null) {
            log.log(Level.WARNING, "createFeedContents() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = feedContents.size();
        if(size == 0) {
            log.log(Level.WARNING, "createFeedContents() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(FeedContent feedContent : feedContents) {
            String guid = createFeedContent(feedContent);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createFeedContents() failed for at least one feedContent. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateFeedContents(List<FeedContent> feedContents) throws BaseException
    //{
    //}

}
