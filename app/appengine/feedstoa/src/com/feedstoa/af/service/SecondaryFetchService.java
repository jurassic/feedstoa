package com.feedstoa.af.service;

import java.util.List;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface SecondaryFetchService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    SecondaryFetch getSecondaryFetch(String guid) throws BaseException;
    Object getSecondaryFetch(String guid, String field) throws BaseException;
    List<SecondaryFetch> getSecondaryFetches(List<String> guids) throws BaseException;
    List<SecondaryFetch> getAllSecondaryFetches() throws BaseException;
    /* @Deprecated */ List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws BaseException;
    List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createSecondaryFetch(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException;
    //String createSecondaryFetch(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return SecondaryFetch?)
    String createSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException;
    SecondaryFetch constructSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException;
    Boolean updateSecondaryFetch(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException;
    //Boolean updateSecondaryFetch(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException;
    SecondaryFetch refreshSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException;
    Boolean deleteSecondaryFetch(String guid) throws BaseException;
    Boolean deleteSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException;
    Long deleteSecondaryFetches(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createSecondaryFetches(List<SecondaryFetch> secondaryFetches) throws BaseException;
//    Boolean updateSecondaryFetches(List<SecondaryFetch> secondaryFetches) throws BaseException;

}
