package com.feedstoa.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.impl.FeedItemServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class FeedItemProtoService extends FeedItemServiceImpl implements FeedItemService
{
    private static final Logger log = Logger.getLogger(FeedItemProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public FeedItemProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FeedItem related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FeedItem getFeedItem(String guid) throws BaseException
    {
        return super.getFeedItem(guid);
    }

    @Override
    public Object getFeedItem(String guid, String field) throws BaseException
    {
        return super.getFeedItem(guid, field);
    }

    @Override
    public List<FeedItem> getFeedItems(List<String> guids) throws BaseException
    {
        return super.getFeedItems(guids);
    }

    @Override
    public List<FeedItem> getAllFeedItems() throws BaseException
    {
        return super.getAllFeedItems();
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedItems(ordering, offset, count, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllFeedItems(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedItemKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllFeedItemKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createFeedItem(FeedItem feedItem) throws BaseException
    {
        return super.createFeedItem(feedItem);
    }

    @Override
    public FeedItem constructFeedItem(FeedItem feedItem) throws BaseException
    {
        return super.constructFeedItem(feedItem);
    }


    @Override
    public Boolean updateFeedItem(FeedItem feedItem) throws BaseException
    {
        return super.updateFeedItem(feedItem);
    }
        
    @Override
    public FeedItem refreshFeedItem(FeedItem feedItem) throws BaseException
    {
        return super.refreshFeedItem(feedItem);
    }

    @Override
    public Boolean deleteFeedItem(String guid) throws BaseException
    {
        return super.deleteFeedItem(guid);
    }

    @Override
    public Boolean deleteFeedItem(FeedItem feedItem) throws BaseException
    {
        return super.deleteFeedItem(feedItem);
    }

    @Override
    public Integer createFeedItems(List<FeedItem> feedItems) throws BaseException
    {
        return super.createFeedItems(feedItems);
    }

    // TBD
    //@Override
    //public Boolean updateFeedItems(List<FeedItem> feedItems) throws BaseException
    //{
    //}

}
