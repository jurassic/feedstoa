package com.feedstoa.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.af.config.Config;

import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.GeoPointStructBean;
import com.feedstoa.af.bean.StreetAddressStructBean;
import com.feedstoa.af.bean.SiteLogBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.FullNameStructBean;
import com.feedstoa.af.bean.GeoCoordinateStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.GaeUserStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CellLatitudeLongitudeBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.KeyValuePairStructBean;
import com.feedstoa.af.bean.KeyValueRelationStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.PagerStateStructBean;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.UserWebsiteStructBean;
import com.feedstoa.af.bean.HelpNoticeBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.ContactInfoStructBean;
import com.feedstoa.af.bean.AppBrandStructBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.ExternalServiceApiKeyStructBean;
import com.feedstoa.af.bean.RssChannelBean;

import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.manager.ProxyFactoryManager;
import com.feedstoa.af.service.ServiceConstants;
import com.feedstoa.af.service.ChannelSourceService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ChannelSourceServiceImpl implements ChannelSourceService
{
    private static final Logger log = Logger.getLogger(ChannelSourceServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ChannelSource-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ChannelSource:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ChannelSourceServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ChannelSource related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ChannelSource getChannelSource(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getChannelSource(): guid = " + guid);

        ChannelSourceBean bean = null;
        if(getCache() != null) {
            bean = (ChannelSourceBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ChannelSourceBean) getProxyFactory().getChannelSourceServiceProxy().getChannelSource(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ChannelSourceBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelSourceBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getChannelSource(String guid, String field) throws BaseException
    {
        ChannelSourceBean bean = null;
        if(getCache() != null) {
            bean = (ChannelSourceBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ChannelSourceBean) getProxyFactory().getChannelSourceServiceProxy().getChannelSource(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ChannelSourceBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelSourceBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("serviceName")) {
            return bean.getServiceName();
        } else if(field.equals("serviceUrl")) {
            return bean.getServiceUrl();
        } else if(field.equals("feedUrl")) {
            return bean.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return bean.getFeedFormat();
        } else if(field.equals("channelTitle")) {
            return bean.getChannelTitle();
        } else if(field.equals("channelDescription")) {
            return bean.getChannelDescription();
        } else if(field.equals("channelCategory")) {
            return bean.getChannelCategory();
        } else if(field.equals("channelUrl")) {
            return bean.getChannelUrl();
        } else if(field.equals("channelLogo")) {
            return bean.getChannelLogo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ChannelSource> getChannelSources(List<String> guids) throws BaseException
    {
        log.fine("getChannelSources()");

        // TBD: Is there a better way????
        List<ChannelSource> channelSources = getProxyFactory().getChannelSourceServiceProxy().getChannelSources(guids);
        if(channelSources == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelSourceBean list.");
        }

        log.finer("END");
        return channelSources;
    }

    @Override
    public List<ChannelSource> getAllChannelSources() throws BaseException
    {
        return getAllChannelSources(null, null, null);
    }


    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelSources(ordering, offset, count, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllChannelSources(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelSource> channelSources = getProxyFactory().getChannelSourceServiceProxy().getAllChannelSources(ordering, offset, count, forwardCursor);
        if(channelSources == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelSourceBean list.");
        }

        log.finer("END");
        return channelSources;
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelSourceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllChannelSourceKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getChannelSourceServiceProxy().getAllChannelSourceKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelSourceBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ChannelSourceBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findChannelSources(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ChannelSourceServiceImpl.findChannelSources(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelSource> channelSources = getProxyFactory().getChannelSourceServiceProxy().findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(channelSources == null) {
            log.log(Level.WARNING, "Failed to find channelSources for the given criterion.");
        }

        log.finer("END");
        return channelSources;
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ChannelSourceServiceImpl.findChannelSourceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getChannelSourceServiceProxy().findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ChannelSource keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ChannelSource key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ChannelSourceServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getChannelSourceServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createChannelSource(String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ChannelSourceBean bean = new ChannelSourceBean(null, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        return createChannelSource(bean);
    }

    @Override
    public String createChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ChannelSource bean = constructChannelSource(channelSource);
        //return bean.getGuid();

        // Param channelSource cannot be null.....
        if(channelSource == null) {
            log.log(Level.INFO, "Param channelSource is null!");
            throw new BadRequestException("Param channelSource object is null!");
        }
        ChannelSourceBean bean = null;
        if(channelSource instanceof ChannelSourceBean) {
            bean = (ChannelSourceBean) channelSource;
        } else if(channelSource instanceof ChannelSource) {
            // bean = new ChannelSourceBean(null, channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ChannelSourceBean(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        } else {
            log.log(Level.WARNING, "createChannelSource(): Arg channelSource is of an unknown type.");
            //bean = new ChannelSourceBean();
            bean = new ChannelSourceBean(channelSource.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getChannelSourceServiceProxy().createChannelSource(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ChannelSource constructChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelSource cannot be null.....
        if(channelSource == null) {
            log.log(Level.INFO, "Param channelSource is null!");
            throw new BadRequestException("Param channelSource object is null!");
        }
        ChannelSourceBean bean = null;
        if(channelSource instanceof ChannelSourceBean) {
            bean = (ChannelSourceBean) channelSource;
        } else if(channelSource instanceof ChannelSource) {
            // bean = new ChannelSourceBean(null, channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ChannelSourceBean(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        } else {
            log.log(Level.WARNING, "createChannelSource(): Arg channelSource is of an unknown type.");
            //bean = new ChannelSourceBean();
            bean = new ChannelSourceBean(channelSource.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getChannelSourceServiceProxy().createChannelSource(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateChannelSource(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ChannelSourceBean bean = new ChannelSourceBean(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        return updateChannelSource(bean);
    }
        
    @Override
    public Boolean updateChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ChannelSource bean = refreshChannelSource(channelSource);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param channelSource cannot be null.....
        if(channelSource == null || channelSource.getGuid() == null) {
            log.log(Level.INFO, "Param channelSource or its guid is null!");
            throw new BadRequestException("Param channelSource object or its guid is null!");
        }
        ChannelSourceBean bean = null;
        if(channelSource instanceof ChannelSourceBean) {
            bean = (ChannelSourceBean) channelSource;
        } else {  // if(channelSource instanceof ChannelSource)
            bean = new ChannelSourceBean(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        }
        Boolean suc = getProxyFactory().getChannelSourceServiceProxy().updateChannelSource(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ChannelSource refreshChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelSource cannot be null.....
        if(channelSource == null || channelSource.getGuid() == null) {
            log.log(Level.INFO, "Param channelSource or its guid is null!");
            throw new BadRequestException("Param channelSource object or its guid is null!");
        }
        ChannelSourceBean bean = null;
        if(channelSource instanceof ChannelSourceBean) {
            bean = (ChannelSourceBean) channelSource;
        } else {  // if(channelSource instanceof ChannelSource)
            bean = new ChannelSourceBean(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        }
        Boolean suc = getProxyFactory().getChannelSourceServiceProxy().updateChannelSource(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteChannelSource(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getChannelSourceServiceProxy().deleteChannelSource(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ChannelSource channelSource = null;
            try {
                channelSource = getProxyFactory().getChannelSourceServiceProxy().getChannelSource(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch channelSource with a key, " + guid);
                return false;
            }
            if(channelSource != null) {
                String beanGuid = channelSource.getGuid();
                Boolean suc1 = getProxyFactory().getChannelSourceServiceProxy().deleteChannelSource(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("channelSource with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelSource cannot be null.....
        if(channelSource == null || channelSource.getGuid() == null) {
            log.log(Level.INFO, "Param channelSource or its guid is null!");
            throw new BadRequestException("Param channelSource object or its guid is null!");
        }
        ChannelSourceBean bean = null;
        if(channelSource instanceof ChannelSourceBean) {
            bean = (ChannelSourceBean) channelSource;
        } else {  // if(channelSource instanceof ChannelSource)
            // ????
            log.warning("channelSource is not an instance of ChannelSourceBean.");
            bean = new ChannelSourceBean(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        }
        Boolean suc = getProxyFactory().getChannelSourceServiceProxy().deleteChannelSource(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteChannelSources(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getChannelSourceServiceProxy().deleteChannelSources(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createChannelSources(List<ChannelSource> channelSources) throws BaseException
    {
        log.finer("BEGIN");

        if(channelSources == null) {
            log.log(Level.WARNING, "createChannelSources() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = channelSources.size();
        if(size == 0) {
            log.log(Level.WARNING, "createChannelSources() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ChannelSource channelSource : channelSources) {
            String guid = createChannelSource(channelSource);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createChannelSources() failed for at least one channelSource. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateChannelSources(List<ChannelSource> channelSources) throws BaseException
    //{
    //}

}
