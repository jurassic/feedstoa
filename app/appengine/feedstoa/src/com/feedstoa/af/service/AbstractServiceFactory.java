package com.feedstoa.af.service;

public abstract class AbstractServiceFactory
{
    public abstract ApiConsumerService getApiConsumerService();
    public abstract UserService getUserService();
    public abstract FetchRequestService getFetchRequestService();
    public abstract SecondaryFetchService getSecondaryFetchService();
    public abstract ChannelSourceService getChannelSourceService();
    public abstract ChannelFeedService getChannelFeedService();
    public abstract FeedItemService getFeedItemService();
    public abstract FeedContentService getFeedContentService();
    public abstract ServiceInfoService getServiceInfoService();
    public abstract FiveTenService getFiveTenService();

}
