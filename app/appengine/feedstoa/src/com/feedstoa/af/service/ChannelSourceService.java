package com.feedstoa.af.service;

import java.util.List;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.ChannelSource;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ChannelSourceService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ChannelSource getChannelSource(String guid) throws BaseException;
    Object getChannelSource(String guid, String field) throws BaseException;
    List<ChannelSource> getChannelSources(List<String> guids) throws BaseException;
    List<ChannelSource> getAllChannelSources() throws BaseException;
    /* @Deprecated */ List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count) throws BaseException;
    List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createChannelSource(String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException;
    //String createChannelSource(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ChannelSource?)
    String createChannelSource(ChannelSource channelSource) throws BaseException;
    ChannelSource constructChannelSource(ChannelSource channelSource) throws BaseException;
    Boolean updateChannelSource(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException;
    //Boolean updateChannelSource(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateChannelSource(ChannelSource channelSource) throws BaseException;
    ChannelSource refreshChannelSource(ChannelSource channelSource) throws BaseException;
    Boolean deleteChannelSource(String guid) throws BaseException;
    Boolean deleteChannelSource(ChannelSource channelSource) throws BaseException;
    Long deleteChannelSources(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createChannelSources(List<ChannelSource> channelSources) throws BaseException;
//    Boolean updateChannelSources(List<ChannelSource> channelSources) throws BaseException;

}
