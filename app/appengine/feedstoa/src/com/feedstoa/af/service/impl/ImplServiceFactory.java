package com.feedstoa.af.service.impl;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.service.AbstractServiceFactory;
import com.feedstoa.af.service.ApiConsumerService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.af.service.ServiceInfoService;
import com.feedstoa.af.service.FiveTenService;

public class ImplServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ImplServiceFactory.class.getName());

    private ImplServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ImplServiceFactoryHolder
    {
        private static final ImplServiceFactory INSTANCE = new ImplServiceFactory();
    }

    // Singleton method
    public static ImplServiceFactory getInstance()
    {
        return ImplServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerServiceImpl();
    }

    @Override
    public UserService getUserService()
    {
        return new UserServiceImpl();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestServiceImpl();
    }

    @Override
    public SecondaryFetchService getSecondaryFetchService()
    {
        return new SecondaryFetchServiceImpl();
    }

    @Override
    public ChannelSourceService getChannelSourceService()
    {
        return new ChannelSourceServiceImpl();
    }

    @Override
    public ChannelFeedService getChannelFeedService()
    {
        return new ChannelFeedServiceImpl();
    }

    @Override
    public FeedItemService getFeedItemService()
    {
        return new FeedItemServiceImpl();
    }

    @Override
    public FeedContentService getFeedContentService()
    {
        return new FeedContentServiceImpl();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoServiceImpl();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenServiceImpl();
    }


}
