package com.feedstoa.af.service.proto;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.service.AbstractServiceFactory;
import com.feedstoa.af.service.ApiConsumerService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.af.service.ServiceInfoService;
import com.feedstoa.af.service.FiveTenService;

public class ProtoServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ProtoServiceFactory.class.getName());

    private ProtoServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ProtoServiceFactoryHolder
    {
        private static final ProtoServiceFactory INSTANCE = new ProtoServiceFactory();
    }

    // Singleton method
    public static ProtoServiceFactory getInstance()
    {
        return ProtoServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerProtoService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserProtoService();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestProtoService();
    }

    @Override
    public SecondaryFetchService getSecondaryFetchService()
    {
        return new SecondaryFetchProtoService();
    }

    @Override
    public ChannelSourceService getChannelSourceService()
    {
        return new ChannelSourceProtoService();
    }

    @Override
    public ChannelFeedService getChannelFeedService()
    {
        return new ChannelFeedProtoService();
    }

    @Override
    public FeedItemService getFeedItemService()
    {
        return new FeedItemProtoService();
    }

    @Override
    public FeedContentService getFeedContentService()
    {
        return new FeedContentProtoService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoProtoService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenProtoService();
    }


}
