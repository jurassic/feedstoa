package com.feedstoa.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.service.ApiConsumerService;
import com.feedstoa.af.service.UserService;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.ChannelSourceService;
import com.feedstoa.af.service.ChannelFeedService;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.FeedContentService;
import com.feedstoa.af.service.ServiceInfoService;
import com.feedstoa.af.service.FiveTenService;

// TBD: DI
// (For now, this class is used to make ServiceManager "DI-capable".)
public class ServiceController
{
    private static final Logger log = Logger.getLogger(ServiceController.class.getName());

    // "Injectable" services.
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private FetchRequestService fetchRequestService = null;
    private SecondaryFetchService secondaryFetchService = null;
    private ChannelSourceService channelSourceService = null;
    private ChannelFeedService channelFeedService = null;
    private FeedItemService feedItemService = null;
    private FeedContentService feedContentService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;

    // Ctor injection.
    public ServiceController(ApiConsumerService apiConsumerService, UserService userService, FetchRequestService fetchRequestService, SecondaryFetchService secondaryFetchService, ChannelSourceService channelSourceService, ChannelFeedService channelFeedService, FeedItemService feedItemService, FeedContentService feedContentService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.fetchRequestService = fetchRequestService;
        this.secondaryFetchService = secondaryFetchService;
        this.channelSourceService = channelSourceService;
        this.channelFeedService = channelFeedService;
        this.feedItemService = feedItemService;
        this.feedContentService = feedContentService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }

    // Returns a ApiConsumerService instance.
	public ApiConsumerService getApiConsumerService() 
    {
        return this.apiConsumerService;
    }
    // Setter injection for ApiConsumerService.
	public void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        this.apiConsumerService = apiConsumerService;
    }

    // Returns a UserService instance.
	public UserService getUserService() 
    {
        return this.userService;
    }
    // Setter injection for UserService.
	public void setUserService(UserService userService) 
    {
        this.userService = userService;
    }

    // Returns a FetchRequestService instance.
	public FetchRequestService getFetchRequestService() 
    {
        return this.fetchRequestService;
    }
    // Setter injection for FetchRequestService.
	public void setFetchRequestService(FetchRequestService fetchRequestService) 
    {
        this.fetchRequestService = fetchRequestService;
    }

    // Returns a SecondaryFetchService instance.
	public SecondaryFetchService getSecondaryFetchService() 
    {
        return this.secondaryFetchService;
    }
    // Setter injection for SecondaryFetchService.
	public void setSecondaryFetchService(SecondaryFetchService secondaryFetchService) 
    {
        this.secondaryFetchService = secondaryFetchService;
    }

    // Returns a ChannelSourceService instance.
	public ChannelSourceService getChannelSourceService() 
    {
        return this.channelSourceService;
    }
    // Setter injection for ChannelSourceService.
	public void setChannelSourceService(ChannelSourceService channelSourceService) 
    {
        this.channelSourceService = channelSourceService;
    }

    // Returns a ChannelFeedService instance.
	public ChannelFeedService getChannelFeedService() 
    {
        return this.channelFeedService;
    }
    // Setter injection for ChannelFeedService.
	public void setChannelFeedService(ChannelFeedService channelFeedService) 
    {
        this.channelFeedService = channelFeedService;
    }

    // Returns a FeedItemService instance.
	public FeedItemService getFeedItemService() 
    {
        return this.feedItemService;
    }
    // Setter injection for FeedItemService.
	public void setFeedItemService(FeedItemService feedItemService) 
    {
        this.feedItemService = feedItemService;
    }

    // Returns a FeedContentService instance.
	public FeedContentService getFeedContentService() 
    {
        return this.feedContentService;
    }
    // Setter injection for FeedContentService.
	public void setFeedContentService(FeedContentService feedContentService) 
    {
        this.feedContentService = feedContentService;
    }

    // Returns a ServiceInfoService instance.
	public ServiceInfoService getServiceInfoService() 
    {
        return this.serviceInfoService;
    }
    // Setter injection for ServiceInfoService.
	public void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        this.serviceInfoService = serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public FiveTenService getFiveTenService() 
    {
        return this.fiveTenService;
    }
    // Setter injection for FiveTenService.
	public void setFiveTenService(FiveTenService fiveTenService) 
    {
        this.fiveTenService = fiveTenService;
    }

}
