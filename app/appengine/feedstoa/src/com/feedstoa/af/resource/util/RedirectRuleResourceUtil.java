package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.stub.RedirectRuleStub;
import com.feedstoa.af.bean.RedirectRuleBean;


public class RedirectRuleResourceUtil
{
    private static final Logger log = Logger.getLogger(RedirectRuleResourceUtil.class.getName());

    // Static methods only.
    private RedirectRuleResourceUtil() {}

    public static RedirectRuleBean convertRedirectRuleStubToBean(RedirectRule stub)
    {
        RedirectRuleBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new RedirectRuleBean();
            bean.setRedirectType(stub.getRedirectType());
            bean.setPrecedence(stub.getPrecedence());
            bean.setSourceDomain(stub.getSourceDomain());
            bean.setSourcePath(stub.getSourcePath());
            bean.setTargetDomain(stub.getTargetDomain());
            bean.setTargetPath(stub.getTargetPath());
            bean.setNote(stub.getNote());
            bean.setStatus(stub.getStatus());
        }
        return bean;
    }

}
