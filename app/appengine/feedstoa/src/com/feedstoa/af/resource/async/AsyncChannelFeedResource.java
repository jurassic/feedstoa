package com.feedstoa.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.exception.InternalServerErrorException;
import com.feedstoa.ws.exception.NotImplementedException;
import com.feedstoa.ws.exception.RequestConflictException;
import com.feedstoa.ws.exception.RequestForbiddenException;
import com.feedstoa.ws.exception.ResourceGoneException;
import com.feedstoa.ws.exception.ResourceNotFoundException;
import com.feedstoa.ws.exception.ServiceUnavailableException;
import com.feedstoa.ws.exception.resource.BaseResourceException;
import com.feedstoa.ws.resource.exception.BadRequestRsException;
import com.feedstoa.ws.resource.exception.InternalServerErrorRsException;
import com.feedstoa.ws.resource.exception.NotImplementedRsException;
import com.feedstoa.ws.resource.exception.RequestConflictRsException;
import com.feedstoa.ws.resource.exception.RequestForbiddenRsException;
import com.feedstoa.ws.resource.exception.ResourceGoneRsException;
import com.feedstoa.ws.resource.exception.ResourceNotFoundRsException;
import com.feedstoa.ws.resource.exception.ServiceUnavailableRsException;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.stub.ChannelFeedListStub;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.proxy.ChannelFeedServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteProxyFactory;
import com.feedstoa.af.proxy.remote.RemoteChannelFeedServiceProxy;
import com.feedstoa.af.resource.ChannelFeedResource;
import com.feedstoa.af.resource.util.CloudStructResourceUtil;
import com.feedstoa.af.resource.util.CategoryStructResourceUtil;
import com.feedstoa.af.resource.util.ImageStructResourceUtil;
import com.feedstoa.af.resource.util.UriStructResourceUtil;
import com.feedstoa.af.resource.util.UserStructResourceUtil;
import com.feedstoa.af.resource.util.ReferrerInfoStructResourceUtil;
import com.feedstoa.af.resource.util.TextInputStructResourceUtil;


@Path("/_task/r/channelFeeds/")
public class AsyncChannelFeedResource extends BaseAsyncResource implements ChannelFeedResource
{
    private static final Logger log = Logger.getLogger(AsyncChannelFeedResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncChannelFeedResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getChannelFeedList(List<ChannelFeed> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findChannelFeedsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getChannelFeedAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getChannelFeed(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getChannelFeedAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getChannelFeed(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructChannelFeed(ChannelFeedStub channelFeed) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ChannelFeedBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedStub realStub = (ChannelFeedStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertChannelFeedStubToBean(realStub);
            } else {
                bean = convertChannelFeedStubToBean(channelFeed);
            }
            //bean = (ChannelFeedBean) RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().constructChannelFeed(bean);
            //channelFeed = ChannelFeedStub.convertBeanToStub(bean);
            //String guid = channelFeed.getGuid();
            // TBD: createChannelFeed() or constructChannelFeed()???  (constructChannelFeed() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().createChannelFeed(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructChannelFeed(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(channelFeed).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createChannelFeed(ChannelFeedStub channelFeed) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ChannelFeedBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedStub realStub = (ChannelFeedStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertChannelFeedStubToBean(realStub);
            } else {
                bean = convertChannelFeedStubToBean(channelFeed);
            }
            String guid = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().createChannelFeed(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createChannelFeed(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createChannelFeed(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshChannelFeed(String guid, ChannelFeedStub channelFeed) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(channelFeed == null || !guid.equals(channelFeed.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from channelFeed guid = " + channelFeed.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the channelFeed with guid = " + guid);
            }
            ChannelFeedBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedStub realStub = (ChannelFeedStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertChannelFeedStubToBean(realStub);
            } else {
                bean = convertChannelFeedStubToBean(channelFeed);
            }
            //bean = (ChannelFeedBean) RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().refreshChannelFeed(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the channelFeed with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the channelFeed with guid = " + guid);
            //}
            //channelFeed = ChannelFeedStub.convertBeanToStub(bean);
            // TBD: updateChannelFeed() or refreshChannelFeed()???  (refreshChannelFeed() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().updateChannelFeed(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the channelFeed with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the channelFeed with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshChannelFeed(): Successfully processed the request: guid = " + guid);
            return Response.ok(channelFeed).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateChannelFeed(String guid, ChannelFeedStub channelFeed) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(channelFeed == null || !guid.equals(channelFeed.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from channelFeed guid = " + channelFeed.getGuid());
                throw new RequestForbiddenRsException("Failed to update the channelFeed with guid = " + guid);
            }
            ChannelFeedBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedStub realStub = (ChannelFeedStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertChannelFeedStubToBean(realStub);
            } else {
                bean = convertChannelFeedStubToBean(channelFeed);
            }
            boolean suc = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().updateChannelFeed(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the channelFeed with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the channelFeed with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateChannelFeed(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, String link, String description, String language, String copyright, String managingEditor, String webMaster, List<String> contributor, String pubDate, String lastBuildDate, List<String> category, String generator, String docs, String cloud, Integer ttl, String logo, String icon, String rating, String textInput, List<String> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, String referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateChannelFeed(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteChannelFeed(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().deleteChannelFeed(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the channelFeed with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the channelFeed with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteChannelFeed(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteChannelFeeds(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteChannelFeeds(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().deleteChannelFeeds(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createChannelFeeds(ChannelFeedListStub channelFeeds) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createChannelFeeds(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ChannelFeedListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedListStub realStubs = (ChannelFeedListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = channelFeeds;
            }

            List<ChannelFeedStub> stubList = channelFeeds.getList();
            List<VisitorSetting> beans = new ArrayList<ChannelFeed>();
            for(ChannelFeedStub stub : stubList) {
                ChannelFeedBean bean = convertChannelFeedStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getChannelFeedServiceProxy().createChannelFeeds(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static ChannelFeedBean convertChannelFeedStubToBean(ChannelFeed stub)
    {
        ChannelFeedBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new ChannelFeedBean();
            bean.setGuid(stub.getGuid());
            bean.setUser(stub.getUser());
            bean.setChannelSource(stub.getChannelSource());
            bean.setChannelCode(stub.getChannelCode());
            bean.setPreviousVersion(stub.getPreviousVersion());
            bean.setFetchRequest(stub.getFetchRequest());
            bean.setFetchUrl(stub.getFetchUrl());
            bean.setFeedServiceUrl(stub.getFeedServiceUrl());
            bean.setFeedUrl(stub.getFeedUrl());
            bean.setFeedFormat(stub.getFeedFormat());
            bean.setMaxItemCount(stub.getMaxItemCount());
            bean.setFeedCategory(stub.getFeedCategory());
            bean.setTitle(stub.getTitle());
            bean.setSubtitle(stub.getSubtitle());
            bean.setLink(UriStructResourceUtil.convertUriStructStubToBean(stub.getLink()));
            bean.setDescription(stub.getDescription());
            bean.setLanguage(stub.getLanguage());
            bean.setCopyright(stub.getCopyright());
            bean.setManagingEditor(UserStructResourceUtil.convertUserStructStubToBean(stub.getManagingEditor()));
            bean.setWebMaster(UserStructResourceUtil.convertUserStructStubToBean(stub.getWebMaster()));
            bean.setContributor(stub.getContributor());
            bean.setPubDate(stub.getPubDate());
            bean.setLastBuildDate(stub.getLastBuildDate());
            bean.setCategory(stub.getCategory());
            bean.setGenerator(stub.getGenerator());
            bean.setDocs(stub.getDocs());
            bean.setCloud(CloudStructResourceUtil.convertCloudStructStubToBean(stub.getCloud()));
            bean.setTtl(stub.getTtl());
            bean.setLogo(ImageStructResourceUtil.convertImageStructStubToBean(stub.getLogo()));
            bean.setIcon(ImageStructResourceUtil.convertImageStructStubToBean(stub.getIcon()));
            bean.setRating(stub.getRating());
            bean.setTextInput(TextInputStructResourceUtil.convertTextInputStructStubToBean(stub.getTextInput()));
            bean.setSkipHours(stub.getSkipHours());
            bean.setSkipDays(stub.getSkipDays());
            bean.setOutputText(stub.getOutputText());
            bean.setOutputHash(stub.getOutputHash());
            bean.setFeedContent(stub.getFeedContent());
            bean.setStatus(stub.getStatus());
            bean.setNote(stub.getNote());
            bean.setReferrerInfo(ReferrerInfoStructResourceUtil.convertReferrerInfoStructStubToBean(stub.getReferrerInfo()));
            bean.setLastBuildTime(stub.getLastBuildTime());
            bean.setPublishedTime(stub.getPublishedTime());
            bean.setExpirationTime(stub.getExpirationTime());
            bean.setLastUpdatedTime(stub.getLastUpdatedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
