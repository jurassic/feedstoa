package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.af.bean.NotificationStructBean;


public class NotificationStructResourceUtil
{
    private static final Logger log = Logger.getLogger(NotificationStructResourceUtil.class.getName());

    // Static methods only.
    private NotificationStructResourceUtil() {}

    public static NotificationStructBean convertNotificationStructStubToBean(NotificationStruct stub)
    {
        NotificationStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new NotificationStructBean();
            bean.setPreferredMode(stub.getPreferredMode());
            bean.setMobileNumber(stub.getMobileNumber());
            bean.setEmailAddress(stub.getEmailAddress());
            bean.setTwitterUsername(stub.getTwitterUsername());
            bean.setFacebookId(stub.getFacebookId());
            bean.setLinkedinId(stub.getLinkedinId());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
