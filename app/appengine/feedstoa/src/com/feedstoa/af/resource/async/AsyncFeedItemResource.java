package com.feedstoa.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.exception.InternalServerErrorException;
import com.feedstoa.ws.exception.NotImplementedException;
import com.feedstoa.ws.exception.RequestConflictException;
import com.feedstoa.ws.exception.RequestForbiddenException;
import com.feedstoa.ws.exception.ResourceGoneException;
import com.feedstoa.ws.exception.ResourceNotFoundException;
import com.feedstoa.ws.exception.ServiceUnavailableException;
import com.feedstoa.ws.exception.resource.BaseResourceException;
import com.feedstoa.ws.resource.exception.BadRequestRsException;
import com.feedstoa.ws.resource.exception.InternalServerErrorRsException;
import com.feedstoa.ws.resource.exception.NotImplementedRsException;
import com.feedstoa.ws.resource.exception.RequestConflictRsException;
import com.feedstoa.ws.resource.exception.RequestForbiddenRsException;
import com.feedstoa.ws.resource.exception.ResourceGoneRsException;
import com.feedstoa.ws.resource.exception.ResourceNotFoundRsException;
import com.feedstoa.ws.resource.exception.ServiceUnavailableRsException;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.stub.FeedItemStub;
import com.feedstoa.ws.stub.FeedItemListStub;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.proxy.FeedItemServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteProxyFactory;
import com.feedstoa.af.proxy.remote.RemoteFeedItemServiceProxy;
import com.feedstoa.af.resource.FeedItemResource;
import com.feedstoa.af.resource.util.EnclosureStructResourceUtil;
import com.feedstoa.af.resource.util.CategoryStructResourceUtil;
import com.feedstoa.af.resource.util.UriStructResourceUtil;
import com.feedstoa.af.resource.util.UserStructResourceUtil;
import com.feedstoa.af.resource.util.ReferrerInfoStructResourceUtil;


@Path("/_task/r/feedItems/")
public class AsyncFeedItemResource extends BaseAsyncResource implements FeedItemResource
{
    private static final Logger log = Logger.getLogger(AsyncFeedItemResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncFeedItemResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getFeedItemList(List<FeedItem> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findFeedItemsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getFeedItemAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getFeedItem(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getFeedItemAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getFeedItem(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructFeedItem(FeedItemStub feedItem) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructFeedItem(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FeedItemBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FeedItemStub realStub = (FeedItemStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFeedItemStubToBean(realStub);
            } else {
                bean = convertFeedItemStubToBean(feedItem);
            }
            //bean = (FeedItemBean) RemoteProxyFactory.getInstance().getFeedItemServiceProxy().constructFeedItem(bean);
            //feedItem = FeedItemStub.convertBeanToStub(bean);
            //String guid = feedItem.getGuid();
            // TBD: createFeedItem() or constructFeedItem()???  (constructFeedItem() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().createFeedItem(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructFeedItem(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(feedItem).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createFeedItem(FeedItemStub feedItem) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createFeedItem(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FeedItemBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FeedItemStub realStub = (FeedItemStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFeedItemStubToBean(realStub);
            } else {
                bean = convertFeedItemStubToBean(feedItem);
            }
            String guid = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().createFeedItem(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createFeedItem(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createFeedItem(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshFeedItem(String guid, FeedItemStub feedItem) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshFeedItem(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(feedItem == null || !guid.equals(feedItem.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from feedItem guid = " + feedItem.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the feedItem with guid = " + guid);
            }
            FeedItemBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FeedItemStub realStub = (FeedItemStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFeedItemStubToBean(realStub);
            } else {
                bean = convertFeedItemStubToBean(feedItem);
            }
            //bean = (FeedItemBean) RemoteProxyFactory.getInstance().getFeedItemServiceProxy().refreshFeedItem(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the feedItem with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the feedItem with guid = " + guid);
            //}
            //feedItem = FeedItemStub.convertBeanToStub(bean);
            // TBD: updateFeedItem() or refreshFeedItem()???  (refreshFeedItem() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().updateFeedItem(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the feedItem with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the feedItem with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshFeedItem(): Successfully processed the request: guid = " + guid);
            return Response.ok(feedItem).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateFeedItem(String guid, FeedItemStub feedItem) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateFeedItem(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(feedItem == null || !guid.equals(feedItem.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from feedItem guid = " + feedItem.getGuid());
                throw new RequestForbiddenRsException("Failed to update the feedItem with guid = " + guid);
            }
            FeedItemBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FeedItemStub realStub = (FeedItemStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFeedItemStubToBean(realStub);
            } else {
                bean = convertFeedItemStubToBean(feedItem);
            }
            boolean suc = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().updateFeedItem(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the feedItem with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the feedItem with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateFeedItem(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, String link, String summary, String content, String author, List<String> contributor, List<String> category, String comments, String enclosure, String id, String pubDate, String source, String feedContent, String status, String note, String referrerInfo, Long publishedTime, Long lastUpdatedTime)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateFeedItem(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteFeedItem(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteFeedItem(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().deleteFeedItem(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the feedItem with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the feedItem with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteFeedItem(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteFeedItems(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteFeedItems(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().deleteFeedItems(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createFeedItems(FeedItemListStub feedItems) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createFeedItems(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FeedItemListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FeedItemListStub realStubs = (FeedItemListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = feedItems;
            }

            List<FeedItemStub> stubList = feedItems.getList();
            List<VisitorSetting> beans = new ArrayList<FeedItem>();
            for(FeedItemStub stub : stubList) {
                FeedItemBean bean = convertFeedItemStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getFeedItemServiceProxy().createFeedItems(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static FeedItemBean convertFeedItemStubToBean(FeedItem stub)
    {
        FeedItemBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new FeedItemBean();
            bean.setGuid(stub.getGuid());
            bean.setUser(stub.getUser());
            bean.setFetchRequest(stub.getFetchRequest());
            bean.setFetchUrl(stub.getFetchUrl());
            bean.setFeedUrl(stub.getFeedUrl());
            bean.setChannelSource(stub.getChannelSource());
            bean.setChannelFeed(stub.getChannelFeed());
            bean.setFeedFormat(stub.getFeedFormat());
            bean.setTitle(stub.getTitle());
            bean.setLink(UriStructResourceUtil.convertUriStructStubToBean(stub.getLink()));
            bean.setSummary(stub.getSummary());
            bean.setContent(stub.getContent());
            bean.setAuthor(UserStructResourceUtil.convertUserStructStubToBean(stub.getAuthor()));
            bean.setContributor(stub.getContributor());
            bean.setCategory(stub.getCategory());
            bean.setComments(stub.getComments());
            bean.setEnclosure(EnclosureStructResourceUtil.convertEnclosureStructStubToBean(stub.getEnclosure()));
            bean.setId(stub.getId());
            bean.setPubDate(stub.getPubDate());
            bean.setSource(UriStructResourceUtil.convertUriStructStubToBean(stub.getSource()));
            bean.setFeedContent(stub.getFeedContent());
            bean.setStatus(stub.getStatus());
            bean.setNote(stub.getNote());
            bean.setReferrerInfo(ReferrerInfoStructResourceUtil.convertReferrerInfoStructStubToBean(stub.getReferrerInfo()));
            bean.setPublishedTime(stub.getPublishedTime());
            bean.setLastUpdatedTime(stub.getLastUpdatedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
