package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.af.bean.UriStructBean;


public class UriStructResourceUtil
{
    private static final Logger log = Logger.getLogger(UriStructResourceUtil.class.getName());

    // Static methods only.
    private UriStructResourceUtil() {}

    public static UriStructBean convertUriStructStubToBean(UriStruct stub)
    {
        UriStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new UriStructBean();
            bean.setUuid(stub.getUuid());
            bean.setHref(stub.getHref());
            bean.setRel(stub.getRel());
            bean.setType(stub.getType());
            bean.setLabel(stub.getLabel());
        }
        return bean;
    }

}
