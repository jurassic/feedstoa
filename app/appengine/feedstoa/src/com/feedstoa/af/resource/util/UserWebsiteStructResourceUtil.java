package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.UserWebsiteStruct;
import com.feedstoa.ws.stub.UserWebsiteStructStub;
import com.feedstoa.af.bean.UserWebsiteStructBean;


public class UserWebsiteStructResourceUtil
{
    private static final Logger log = Logger.getLogger(UserWebsiteStructResourceUtil.class.getName());

    // Static methods only.
    private UserWebsiteStructResourceUtil() {}

    public static UserWebsiteStructBean convertUserWebsiteStructStubToBean(UserWebsiteStruct stub)
    {
        UserWebsiteStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new UserWebsiteStructBean();
            bean.setUuid(stub.getUuid());
            bean.setPrimarySite(stub.getPrimarySite());
            bean.setHomePage(stub.getHomePage());
            bean.setBlogSite(stub.getBlogSite());
            bean.setPortfolioSite(stub.getPortfolioSite());
            bean.setTwitterProfile(stub.getTwitterProfile());
            bean.setFacebookProfile(stub.getFacebookProfile());
            bean.setGooglePlusProfile(stub.getGooglePlusProfile());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
