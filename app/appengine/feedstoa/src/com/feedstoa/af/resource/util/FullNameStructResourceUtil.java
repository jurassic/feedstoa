package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.FullNameStruct;
import com.feedstoa.ws.stub.FullNameStructStub;
import com.feedstoa.af.bean.FullNameStructBean;


public class FullNameStructResourceUtil
{
    private static final Logger log = Logger.getLogger(FullNameStructResourceUtil.class.getName());

    // Static methods only.
    private FullNameStructResourceUtil() {}

    public static FullNameStructBean convertFullNameStructStubToBean(FullNameStruct stub)
    {
        FullNameStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new FullNameStructBean();
            bean.setUuid(stub.getUuid());
            bean.setDisplayName(stub.getDisplayName());
            bean.setLastName(stub.getLastName());
            bean.setFirstName(stub.getFirstName());
            bean.setMiddleName1(stub.getMiddleName1());
            bean.setMiddleName2(stub.getMiddleName2());
            bean.setMiddleInitial(stub.getMiddleInitial());
            bean.setSalutation(stub.getSalutation());
            bean.setSuffix(stub.getSuffix());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
