package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.stub.CloudStructStub;
import com.feedstoa.af.bean.CloudStructBean;


public class CloudStructResourceUtil
{
    private static final Logger log = Logger.getLogger(CloudStructResourceUtil.class.getName());

    // Static methods only.
    private CloudStructResourceUtil() {}

    public static CloudStructBean convertCloudStructStubToBean(CloudStruct stub)
    {
        CloudStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new CloudStructBean();
            bean.setDomain(stub.getDomain());
            bean.setPort(stub.getPort());
            bean.setPath(stub.getPath());
            bean.setRegisterProcedure(stub.getRegisterProcedure());
            bean.setProtocol(stub.getProtocol());
        }
        return bean;
    }

}
