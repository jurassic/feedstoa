package com.feedstoa.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.stub.RssItemStub;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.RssItemBean;


public class RssItemResourceUtil
{
    private static final Logger log = Logger.getLogger(RssItemResourceUtil.class.getName());

    // Static methods only.
    private RssItemResourceUtil() {}

    public static RssItemBean convertRssItemStubToBean(RssItem stub)
    {
        RssItemBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new RssItemBean();
            bean.setTitle(stub.getTitle());
            bean.setLink(stub.getLink());
            bean.setDescription(stub.getDescription());
            bean.setAuthor(stub.getAuthor());
            bean.setCategory(stub.getCategory());
            bean.setComments(stub.getComments());
            bean.setEnclosure(EnclosureStructResourceUtil.convertEnclosureStructStubToBean(stub.getEnclosure()));
            bean.setGuid(stub.getGuid());
            bean.setPubDate(stub.getPubDate());
            bean.setSource(UriStructResourceUtil.convertUriStructStubToBean(stub.getSource()));
        }
        return bean;
    }

}
