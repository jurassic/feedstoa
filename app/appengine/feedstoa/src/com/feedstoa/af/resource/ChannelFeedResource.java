package com.feedstoa.af.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.resource.BaseResourceException;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.stub.ChannelFeedListStub;

public interface ChannelFeedResource
{

    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllChannelFeeds(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllChannelFeedKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findChannelFeedKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findChannelFeeds(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({ "application/x-javascript" })
    Response findChannelFeedsAsJsonp(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

//    @GET
//    @Path("{guid : [0-9a-fA-F\\-]+}")
//    @Produces({MediaType.TEXT_HTML})
//    Response getChannelFeedAsHtml(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getChannelFeed(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({ "application/x-javascript" })
    Response getChannelFeedAsJsonp(@PathParam("guid") String guid, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Path("{guid : [0-9a-f\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getChannelFeed(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response constructChannelFeed(ChannelFeedStub channelFeed) throws BaseResourceException;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createChannelFeed(ChannelFeedStub channelFeed) throws BaseResourceException;

//    @POST
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response createChannelFeed(MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response refreshChannelFeed(@PathParam("guid") String guid, ChannelFeedStub channelFeed) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    //@Produces({MediaType.TEXT_PLAIN})    // ??? updateChannelFeed() returns 204 (No Content)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateChannelFeed(@PathParam("guid") String guid, ChannelFeedStub channelFeed) throws BaseResourceException;

    //@PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    //Response updateChannelFeed(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateChannelFeed(@PathParam("guid") String guid, @QueryParam("user") String user, @QueryParam("channelSource") String channelSource, @QueryParam("channelCode") String channelCode, @QueryParam("previousVersion") String previousVersion, @QueryParam("fetchRequest") String fetchRequest, @QueryParam("fetchUrl") String fetchUrl, @QueryParam("feedServiceUrl") String feedServiceUrl, @QueryParam("feedUrl") String feedUrl, @QueryParam("feedFormat") String feedFormat, @QueryParam("maxItemCount") Integer maxItemCount, @QueryParam("feedCategory") String feedCategory, @QueryParam("title") String title, @QueryParam("subtitle") String subtitle, @QueryParam("link") String link, @QueryParam("description") String description, @QueryParam("language") String language, @QueryParam("copyright") String copyright, @QueryParam("managingEditor") String managingEditor, @QueryParam("webMaster") String webMaster, @QueryParam("contributor") List<String> contributor, @QueryParam("pubDate") String pubDate, @QueryParam("lastBuildDate") String lastBuildDate, @QueryParam("category") List<String> category, @QueryParam("generator") String generator, @QueryParam("docs") String docs, @QueryParam("cloud") String cloud, @QueryParam("ttl") Integer ttl, @QueryParam("logo") String logo, @QueryParam("icon") String icon, @QueryParam("rating") String rating, @QueryParam("textInput") String textInput, @QueryParam("skipHours") List<String> skipHours, @QueryParam("skipDays") List<String> skipDays, @QueryParam("outputText") String outputText, @QueryParam("outputHash") String outputHash, @QueryParam("feedContent") String feedContent, @QueryParam("status") String status, @QueryParam("note") String note, @QueryParam("referrerInfo") String referrerInfo, @QueryParam("lastBuildTime") Long lastBuildTime, @QueryParam("publishedTime") Long publishedTime, @QueryParam("expirationTime") Long expirationTime, @QueryParam("lastUpdatedTime") Long lastUpdatedTime) throws BaseResourceException;

//    @POST
//    //@Path("{guid : [0-9a-fA-F\\-]+}")
//    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response updateChannelFeed(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteChannelFeed(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteChannelFeeds(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

    @POST
    @Path("bulk")
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createChannelFeeds(ChannelFeedListStub channelFeeds) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeChannelFeeds(ChannelFeedListStub channelFeeds) throws BaseResourceException;

}
