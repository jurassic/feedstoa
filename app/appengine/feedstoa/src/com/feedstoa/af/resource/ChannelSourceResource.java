package com.feedstoa.af.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.resource.BaseResourceException;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.stub.ChannelSourceStub;
import com.feedstoa.ws.stub.ChannelSourceListStub;

public interface ChannelSourceResource
{

    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllChannelSources(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllChannelSourceKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findChannelSourceKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findChannelSources(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({ "application/x-javascript" })
    Response findChannelSourcesAsJsonp(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

//    @GET
//    @Path("{guid : [0-9a-fA-F\\-]+}")
//    @Produces({MediaType.TEXT_HTML})
//    Response getChannelSourceAsHtml(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getChannelSource(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({ "application/x-javascript" })
    Response getChannelSourceAsJsonp(@PathParam("guid") String guid, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Path("{guid : [0-9a-f\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getChannelSource(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response constructChannelSource(ChannelSourceStub channelSource) throws BaseResourceException;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createChannelSource(ChannelSourceStub channelSource) throws BaseResourceException;

//    @POST
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response createChannelSource(MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response refreshChannelSource(@PathParam("guid") String guid, ChannelSourceStub channelSource) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    //@Produces({MediaType.TEXT_PLAIN})    // ??? updateChannelSource() returns 204 (No Content)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateChannelSource(@PathParam("guid") String guid, ChannelSourceStub channelSource) throws BaseResourceException;

    //@PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    //Response updateChannelSource(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateChannelSource(@PathParam("guid") String guid, @QueryParam("user") String user, @QueryParam("serviceName") String serviceName, @QueryParam("serviceUrl") String serviceUrl, @QueryParam("feedUrl") String feedUrl, @QueryParam("feedFormat") String feedFormat, @QueryParam("channelTitle") String channelTitle, @QueryParam("channelDescription") String channelDescription, @QueryParam("channelCategory") String channelCategory, @QueryParam("channelUrl") String channelUrl, @QueryParam("channelLogo") String channelLogo, @QueryParam("status") String status, @QueryParam("note") String note) throws BaseResourceException;

//    @POST
//    //@Path("{guid : [0-9a-fA-F\\-]+}")
//    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response updateChannelSource(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteChannelSource(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteChannelSources(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

    @POST
    @Path("bulk")
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createChannelSources(ChannelSourceListStub channelSources) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeChannelSources(ChannelSourceListStub channelSources) throws BaseResourceException;

}
