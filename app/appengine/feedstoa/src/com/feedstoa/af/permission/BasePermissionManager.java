package com.feedstoa.af.permission;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.permission.ApiConsumerBasePermission;
import com.feedstoa.ws.permission.UserBasePermission;
import com.feedstoa.ws.permission.FetchRequestBasePermission;
import com.feedstoa.ws.permission.SecondaryFetchBasePermission;
import com.feedstoa.ws.permission.ChannelSourceBasePermission;
import com.feedstoa.ws.permission.ChannelFeedBasePermission;
import com.feedstoa.ws.permission.FeedItemBasePermission;
import com.feedstoa.ws.permission.FeedContentBasePermission;
import com.feedstoa.ws.permission.ServiceInfoBasePermission;
import com.feedstoa.ws.permission.FiveTenBasePermission;


// TBD:
public class BasePermissionManager
{
    private static final Logger log = Logger.getLogger(BasePermissionManager.class.getName());

    private ApiConsumerBasePermission apiConsumerPermission = null;
    private UserBasePermission userPermission = null;
    private FetchRequestBasePermission fetchRequestPermission = null;
    private SecondaryFetchBasePermission secondaryFetchPermission = null;
    private ChannelSourceBasePermission channelSourcePermission = null;
    private ChannelFeedBasePermission channelFeedPermission = null;
    private FeedItemBasePermission feedItemPermission = null;
    private FeedContentBasePermission feedContentPermission = null;
    private ServiceInfoBasePermission serviceInfoPermission = null;
    private FiveTenBasePermission fiveTenPermission = null;

    // Ctor.
    public BasePermissionManager()
    {
        // TBD:
    }

	public ApiConsumerBasePermission getApiConsumerPermission() 
    {
        if(apiConsumerPermission == null) {
            apiConsumerPermission = new ApiConsumerBasePermission();
        }
        return apiConsumerPermission;
    }
	public void setApiConsumerPermission(ApiConsumerBasePermission apiConsumerPermission) 
    {
        this.apiConsumerPermission = apiConsumerPermission;
    }

	public UserBasePermission getUserPermission() 
    {
        if(userPermission == null) {
            userPermission = new UserBasePermission();
        }
        return userPermission;
    }
	public void setUserPermission(UserBasePermission userPermission) 
    {
        this.userPermission = userPermission;
    }

	public FetchRequestBasePermission getFetchRequestPermission() 
    {
        if(fetchRequestPermission == null) {
            fetchRequestPermission = new FetchRequestBasePermission();
        }
        return fetchRequestPermission;
    }
	public void setFetchRequestPermission(FetchRequestBasePermission fetchRequestPermission) 
    {
        this.fetchRequestPermission = fetchRequestPermission;
    }

	public SecondaryFetchBasePermission getSecondaryFetchPermission() 
    {
        if(secondaryFetchPermission == null) {
            secondaryFetchPermission = new SecondaryFetchBasePermission();
        }
        return secondaryFetchPermission;
    }
	public void setSecondaryFetchPermission(SecondaryFetchBasePermission secondaryFetchPermission) 
    {
        this.secondaryFetchPermission = secondaryFetchPermission;
    }

	public ChannelSourceBasePermission getChannelSourcePermission() 
    {
        if(channelSourcePermission == null) {
            channelSourcePermission = new ChannelSourceBasePermission();
        }
        return channelSourcePermission;
    }
	public void setChannelSourcePermission(ChannelSourceBasePermission channelSourcePermission) 
    {
        this.channelSourcePermission = channelSourcePermission;
    }

	public ChannelFeedBasePermission getChannelFeedPermission() 
    {
        if(channelFeedPermission == null) {
            channelFeedPermission = new ChannelFeedBasePermission();
        }
        return channelFeedPermission;
    }
	public void setChannelFeedPermission(ChannelFeedBasePermission channelFeedPermission) 
    {
        this.channelFeedPermission = channelFeedPermission;
    }

	public FeedItemBasePermission getFeedItemPermission() 
    {
        if(feedItemPermission == null) {
            feedItemPermission = new FeedItemBasePermission();
        }
        return feedItemPermission;
    }
	public void setFeedItemPermission(FeedItemBasePermission feedItemPermission) 
    {
        this.feedItemPermission = feedItemPermission;
    }

	public FeedContentBasePermission getFeedContentPermission() 
    {
        if(feedContentPermission == null) {
            feedContentPermission = new FeedContentBasePermission();
        }
        return feedContentPermission;
    }
	public void setFeedContentPermission(FeedContentBasePermission feedContentPermission) 
    {
        this.feedContentPermission = feedContentPermission;
    }

	public ServiceInfoBasePermission getServiceInfoPermission() 
    {
        if(serviceInfoPermission == null) {
            serviceInfoPermission = new ServiceInfoBasePermission();
        }
        return serviceInfoPermission;
    }
	public void setServiceInfoPermission(ServiceInfoBasePermission serviceInfoPermission) 
    {
        this.serviceInfoPermission = serviceInfoPermission;
    }

	public FiveTenBasePermission getFiveTenPermission() 
    {
        if(fiveTenPermission == null) {
            fiveTenPermission = new FiveTenBasePermission();
        }
        return fiveTenPermission;
    }
	public void setFiveTenPermission(FiveTenBasePermission fiveTenPermission) 
    {
        this.fiveTenPermission = fiveTenPermission;
    }


	public boolean isPermissionRequired(String resource, String action) 
    {
        if(resource == null || resource.isEmpty()) {
            return false;   // ???
        } else if(resource.equals("ApiConsumer")) {
            return getApiConsumerPermission().isPermissionRequired(action);
        } else if(resource.equals("User")) {
            return getUserPermission().isPermissionRequired(action);
        } else if(resource.equals("FetchRequest")) {
            return getFetchRequestPermission().isPermissionRequired(action);
        } else if(resource.equals("SecondaryFetch")) {
            return getSecondaryFetchPermission().isPermissionRequired(action);
        } else if(resource.equals("ChannelSource")) {
            return getChannelSourcePermission().isPermissionRequired(action);
        } else if(resource.equals("ChannelFeed")) {
            return getChannelFeedPermission().isPermissionRequired(action);
        } else if(resource.equals("FeedItem")) {
            return getFeedItemPermission().isPermissionRequired(action);
        } else if(resource.equals("FeedContent")) {
            return getFeedContentPermission().isPermissionRequired(action);
        } else if(resource.equals("ServiceInfo")) {
            return getServiceInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("FiveTen")) {
            return getFiveTenPermission().isPermissionRequired(action);
        } else {
            log.warning("Unrecognized resource = " + resource);
            return true;   // ???
        }
    }

    // TBD: "instance" field is currently ignored...
	public boolean isPermissionRequired(String permissionName) 
    {
        if(permissionName == null || permissionName.isEmpty()) {
            return false;   // ???
        } else if(permissionName.startsWith("ApiConsumer::")) {
            String resource = "ApiConsumer";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("User::")) {
            String resource = "User";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FetchRequest::")) {
            String resource = "FetchRequest";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("SecondaryFetch::")) {
            String resource = "SecondaryFetch";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ChannelSource::")) {
            String resource = "ChannelSource";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ChannelFeed::")) {
            String resource = "ChannelFeed";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FeedItem::")) {
            String resource = "FeedItem";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FeedContent::")) {
            String resource = "FeedContent";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ServiceInfo::")) {
            String resource = "ServiceInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FiveTen::")) {
            String resource = "FiveTen";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else {
            log.warning("Unrecognized permissionName = " + permissionName);
            return true;   // ???
        }
    }

}
