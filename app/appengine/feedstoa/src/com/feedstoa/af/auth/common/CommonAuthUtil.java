package com.feedstoa.af.auth.common;

import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.util.HashUtil;
import com.feedstoa.af.util.URLUtil;


public class CommonAuthUtil
{
    private static final Logger log = Logger.getLogger(CommonAuthUtil.class.getName());

    // TBD...
    public static final String CONFIG_KEY_CUSTOMCREDENTIAL_ENABLED = "feedstoaapp.customcredential.enabled";

    // Common URL params relevant to auth.
    public static final String PARAM_COMEBACKURL = "comebackUrl";  // To be used when auth is successful...
    public static final String PARAM_FALLBACKURL = "fallbackUrl";  // To be used when auth has failed...

    // Auth providerId. If specified, it overwrites the (global) system config values.
    // This can be used to support an auth provider different than the default value.
    // Throughout the "auth flow", a single/consistent authmode/provider should be used. 
    // Note:  authMode is a bit more general than authProvider in our usages (e.g., "openId" is an authMode, but not a provider).
    public static final String PARAM_AUTHMODE = "authMode";
    public static final String PARAM_AUTHPROVIDER = "authProvider";

    // This is a bit mask (integer) to indicate whether to re-do the app approval prompt, etc.
    public static final String PARAM_AUTHPROMPT = "authPrompt";

    // In case request/session attributes may not be suitable for passing userguid (e.g., when callback from third-party server is involved, etc.) 
    public static final String PARAM_USERGUID = "userGuid";

    // These are used to pass "extra" params during auth process.
    // Their usages are app-dependent.
    public static final String PARAM_EXTRAVAR1 = "extraVar1";
    public static final String PARAM_EXTRAVAR2 = "extraVar2";
    public static final String PARAM_EXTRAVAR3 = "extraVar3";
    // ...


    // For anti-forgery. Just use CsrfHelper.REQUEST_PARAM_CSRF_STATE.
    // public static final String PARAM_CSRFSTATE = "csrfState";

    // To indicated whether the last/most recent auth attempt was successful or not.
    // This should be typically cleared at comeback/fallback pages...
    public static final String SESSION_ATTR_AUTHSTATUS = "com.feedstoa.af.auth.common.authstatus";
    public static final String SESSION_ATTR_AUTHTOKEN = "com.feedstoa.af.auth.common.authtoken";
    public static final String SESSION_ATTR_REQUESTTOKEN_PREFIX = "com.feedstoa.af.auth.common.requesttoken.";   // for temporary storage of oauth request tokens.
    public static String getSessionKeyForRequestToken(String providerId)
    {
        // Supports only one request token per provider per user (at any given moment).
        return SESSION_ATTR_REQUESTTOKEN_PREFIX + providerId;
    }
    // ...

    // TBD...
    public static final String AUTH_SERVICE_PROVIDERID_CUSTOM = "custom";     // Internal account (e.g., username+password based auth)
    public static final String AUTH_SERVICE_PROVIDERID_FACEBOOK = "facebook";         // Facebook connect
    public static final String AUTH_SERVICE_PROVIDERID_TWITTER = "twitter";
    public static final String AUTH_SERVICE_PROVIDERID_YAHOO = "yahoo";
    public static final String AUTH_SERVICE_PROVIDERID_GOOGLE = "google";             // OpenID. vs. OAuth2?
    public static final String AUTH_SERVICE_PROVIDERID_GOOGLEOAUTH = "googleoauth";   // OAuth2. vs. Google+ signin???
    public static final String AUTH_SERVICE_PROVIDERID_GOOGLEPLUS = "googleplus";     // "Google+ Signin". OAuth2 based. "Server side flow"
    public static final String AUTH_SERVICE_PROVIDERID_GOOGLEAPPS = "googleapps";     // OpenID
    public static final String AUTH_SERVICE_PROVIDERID_GOOGLEGLASS = "googleglass";   // OAuth?? This is standard OAuth2...
    public static final String AUTH_SERVICE_PROVIDERID_AMAZON = "amazon";
    public static final String AUTH_SERVICE_PROVIDERID_AOL = "aol";
    public static final String AUTH_SERVICE_PROVIDERID_MYOPENID = "myopenid";
    public static final String AUTH_SERVICE_PROVIDERID_LIVEJOURNAL = "livejournal";
    public static final String AUTH_SERVICE_PROVIDERID_FLICKR = "flickr";
    public static final String AUTH_SERVICE_PROVIDERID_WORDPRESS = "wordpress";
    public static final String AUTH_SERVICE_PROVIDERID_BLOGGER = "blogger";
    public static final String AUTH_SERVICE_PROVIDERID_VERISIGN = "verisign";
    public static final String AUTH_SERVICE_PROVIDERID_CLAIMID = "claimid";
    public static final String AUTH_SERVICE_PROVIDERID_CLICKPASS = "clickpass";
    public static final String AUTH_SERVICE_PROVIDERID_YAMMER = "yammer";
    public static final String AUTH_SERVICE_PROVIDERID_LINKEDIN = "linkedin";
    public static final String AUTH_SERVICE_PROVIDERID_MYSPACE = "myspace";
    public static final String AUTH_SERVICE_PROVIDERID_FOURSQUARE = "foursquare";
    // Etc...
    // ...


    private CommonAuthUtil() {}

    
    // Note: We implement isNonOpenId() not isOpenId().
    // isNonOpenId() not necessary equals !isOpenId().
    public static boolean isNonOpenId(String federatedIdentity)
    {
        if(federatedIdentity == null 
            || federatedIdentity.isEmpty() 
            || AUTH_SERVICE_PROVIDERID_FACEBOOK.equals(federatedIdentity) 
            || AUTH_SERVICE_PROVIDERID_TWITTER.equals(federatedIdentity)
            || AUTH_SERVICE_PROVIDERID_GOOGLEOAUTH.equals(federatedIdentity)
            || AUTH_SERVICE_PROVIDERID_GOOGLEPLUS.equals(federatedIdentity)
            || AUTH_SERVICE_PROVIDERID_GOOGLEGLASS.equals(federatedIdentity)
            || AUTH_SERVICE_PROVIDERID_AMAZON.equals(federatedIdentity)
            // etc...
        ) {
            return true;
        } else {
            // It does not mean that the given federatedIdentity is open id...
            return false;
        }
    }
    
    // To be used for FB OAuth "state" param....
    public static String generateRandomNonce()
    {
        // temporary
        return HashUtil.generateSha1Hash(GUID.generate());
    }

//    public static String constructUrl(String topLevelUrl, String path)
//    {
//        if(topLevelUrl == null) {
//            return null;
//        }
//
//        StringBuffer sb = new StringBuffer();
//        sb.append(topLevelUrl);
//        if(path != null && !path.isEmpty()) {
//            if(topLevelUrl.endsWith("/")) {
//                if(path.startsWith("/")) {
//                    if(path.length() > 1) {
//                        sb.append(path.substring(1));
//                    }
//                } else {
//                    sb.append(path);
//                }
//            } else {
//                if(path.startsWith("/")) {
//                    sb.append(path);
//                } else {
//                    sb.append("/").append(path);
//                }
//            }
//        }
//
//        return sb.toString();
//    }

    // topLevelUrl: http://abc.com/
    // path: pqr/lmn
    // Note: we currently ignore contextPath....
    public static String constructUrl(String topLevelUrl, String servletPath)
    {
        return constructUrl(topLevelUrl, servletPath, (String) null);
    }
    public static String constructUrl(String topLevelUrl, String servletPath, String pathInfo)
    {
        return constructUrl(topLevelUrl, servletPath, pathInfo, null);
    }
    public static String constructUrl(String topLevelUrl, String servletPath, Map<String, Object> params)
    {
        return constructUrl(topLevelUrl, servletPath, null, params);
    }
    public static String constructUrl(String topLevelUrl, String servletPath, String pathInfo, Map<String, Object> params)
    {
        if(topLevelUrl == null) {
            //return null;
            topLevelUrl = "/";  // Use relative URL????
        }
        if(servletPath == null) {
            servletPath = "";
        }
        StringBuffer sb = new StringBuffer();
        sb.append(topLevelUrl);
        if(topLevelUrl.endsWith("/")) {
            if(servletPath.startsWith("/")) {
                if(servletPath.length() > 1) {
                    sb.append(servletPath.substring(1));
                }
            } else {
                if(servletPath.length() > 0) {
                    sb.append(servletPath);
                }
            }
        } else {
            if(servletPath.startsWith("/")) {
                sb.append(servletPath);
            } else {
                sb.append("/").append(servletPath);
            }
        }
        if(pathInfo != null && pathInfo.length() > 0) {
            if(servletPath.endsWith("/")) {
                if(pathInfo.startsWith("/")) {
                    if(pathInfo.length() > 1) {
                        sb.append(pathInfo.substring(1));
                    }
                } else {
                    sb.append(pathInfo);
                }
            } else {
                if(pathInfo.startsWith("/")) {
                    sb.append(pathInfo);
                } else {
                    sb.append("/").append(pathInfo);
                }
            }
        }
        String baseUrl = sb.toString();
        String url = URLUtil.buildUrl(baseUrl, params);
        return url;
    }

}
