package com.feedstoa.af.auth.googleoauth;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.logging.Level;


// Encapsulates the Google auth information.
public final class GoogleOAuthAccessToken implements Serializable
{
    private static final Logger log = Logger.getLogger(GoogleOAuthAccessToken.class.getName());
    private static final long serialVersionUID = 1L;

    // "Read-only" variables. 
    private final String accessToken;
    private final String refreshToken;
    private final long expirationTime;
    private final String tokenType;
    private final String idToken;       // What is this???

    // Copy ctor.
    public GoogleOAuthAccessToken(GoogleOAuthAccessToken oauthToken)
    {
        this( 
            (oauthToken != null) ? oauthToken.getAccessToken() : null,
            (oauthToken != null) ? oauthToken.getRefreshToken() : null,
            (oauthToken != null) ? oauthToken.getExpirationTime() : 0L,
            (oauthToken != null) ? oauthToken.getTokenType() : null,
            (oauthToken != null) ? oauthToken.getIdToken() : null
        );
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken)
    {
        this(accessToken, refreshToken, 0L);
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken, long expirationTime)
    {
        this(accessToken, refreshToken, expirationTime, null);
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken, long expirationTime, String tokenType)
    {
        this(accessToken, refreshToken, expirationTime, tokenType, null);
    }
    public GoogleOAuthAccessToken(String accessToken, String refreshToken, long expirationTime, String tokenType, String idToken)
    {
        super();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        if(expirationTime > 0L) {
            this.expirationTime = expirationTime;
        } else {
            this.expirationTime = System.currentTimeMillis();    // ????
        }
        if(tokenType != null && !tokenType.isEmpty()) {
            this.tokenType = tokenType;
        } else {
            this.tokenType = "Bearer";      // Always this value according to Google OAuth2 doc.      
        }
        this.idToken = idToken;
    }

    // The object should contain at least accessToken, otherwise it is "invalid".
    // Non-null/non-empty accessToken does not mean, the token is valid. (E.g., it might have expired.)
    // This method simply returns true if the accessToken is set.
    public boolean isValid()
    {
        return (accessToken != null && !accessToken.isEmpty());
    }

    // Returns true if refreshToken is set.
    // Again, having refreshToken does not mean it's refreshable since the token might be invalid/revoked, etc.
    // It just returns true if at least non-null/non-empty refreshToken is set.
    public boolean isRefreshable()
    {
        return (refreshToken != null && !refreshToken.isEmpty());
    }

    // Returns true if the current access token has expired according to the expiration time.
    public boolean isExpired()
    {
        long now = System.currentTimeMillis();
        if(expirationTime <= now) {
            return true;
        } else {
            return false;
        }
    }

    
    // Getters only
    
    public String getAccessToken()
    {
        return accessToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }

    public String getTokenType()
    {
        return tokenType;
    }

    public String getIdToken()
    {
        return idToken;
    }

    // For debugging purposes only...
    // Since this object contains sensitive data,
    // this should NEVER be logged.
    @Override
    public String toString()
    {
        return "GoogleOAuthAccessToken [accessToken=" + accessToken
                + ", refreshToken=" + refreshToken + ", expirationTime="
                + expirationTime + ", tokenType=" + tokenType + ", idToken="
                + idToken + "]";
    }

}
