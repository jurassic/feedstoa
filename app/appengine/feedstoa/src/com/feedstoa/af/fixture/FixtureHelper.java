package com.feedstoa.af.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.af.config.Config;
import com.feedstoa.af.bean.ApiConsumerBean;
import com.feedstoa.af.bean.UserBean;
import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.af.bean.ServiceInfoBean;
import com.feedstoa.af.bean.FiveTenBean;
import com.feedstoa.ws.stub.ApiConsumerStub;
import com.feedstoa.ws.stub.UserStub;
import com.feedstoa.ws.stub.FetchRequestStub;
import com.feedstoa.ws.stub.SecondaryFetchStub;
import com.feedstoa.ws.stub.ChannelSourceStub;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.stub.FeedItemStub;
import com.feedstoa.ws.stub.FeedContentStub;
import com.feedstoa.ws.stub.ServiceInfoStub;
import com.feedstoa.ws.stub.FiveTenStub;
import com.feedstoa.ws.stub.ApiConsumerListStub;
import com.feedstoa.ws.stub.UserListStub;
import com.feedstoa.ws.stub.FetchRequestListStub;
import com.feedstoa.ws.stub.SecondaryFetchListStub;
import com.feedstoa.ws.stub.ChannelSourceListStub;
import com.feedstoa.ws.stub.ChannelFeedListStub;
import com.feedstoa.ws.stub.FeedItemListStub;
import com.feedstoa.ws.stub.FeedContentListStub;
import com.feedstoa.ws.stub.ServiceInfoListStub;
import com.feedstoa.ws.stub.FiveTenListStub;
import com.feedstoa.af.service.manager.ServiceManager;
import com.feedstoa.af.resource.impl.UserResourceImpl;
import com.feedstoa.af.resource.impl.FetchRequestResourceImpl;
import com.feedstoa.af.resource.impl.SecondaryFetchResourceImpl;
import com.feedstoa.af.resource.impl.ChannelSourceResourceImpl;
import com.feedstoa.af.resource.impl.ChannelFeedResourceImpl;
import com.feedstoa.af.resource.impl.FeedItemResourceImpl;
import com.feedstoa.af.resource.impl.FeedContentResourceImpl;
import com.feedstoa.af.resource.impl.ServiceInfoResourceImpl;
import com.feedstoa.af.resource.impl.FiveTenResourceImpl;


// "Helper" functions...
// Get the list of fixture files,
// Read them, and
// Load the data into the datastore, etc....
// See the note in FixtureUtil...
public class FixtureHelper
{
    private static final Logger log = Logger.getLogger(FixtureHelper.class.getName());

    // ???
    private static final String CONFIG_KEY_FIXTURE_LOAD = "feedstoaapp.fixture.load";
    private static final String CONFIG_KEY_FIXTURE_DIR = "feedstoaapp.fixture.directory";
    // ...

    // Dummy var.
    protected boolean dummyFalse1 = false;
    // ...

    
    private FixtureHelper()
    {
        // ...
    }
    
    // Initialization-on-demand holder.
    private static class FixtureHelperHolder
    {
        private static final FixtureHelper INSTANCE = new FixtureHelper();
    }

    // Singleton method
    public static FixtureHelper getInstance()
    {
        return FixtureHelperHolder.INSTANCE;
    }


    public boolean isFixtureLoad()
    {
        // temporary
        return Config.getInstance().getBoolean(CONFIG_KEY_FIXTURE_LOAD, FixtureUtil.getDefaultFixtureLoad());
    }

    public String getFixtureDirName()
    {
        // temporary
        return Config.getInstance().getString(CONFIG_KEY_FIXTURE_DIR, FixtureUtil.getDefaultFixtureDir());
    }

    
    // TBD
    private List<FixtureFile> buildFixtureFileList()
    {
        List<FixtureFile> list = new ArrayList<FixtureFile>();

        String fixtureDirName = getFixtureDirName();
        File fixtureDir = new File(fixtureDirName);
        if(!fixtureDir.exists() || !fixtureDir.isDirectory()) {
            // error. what to do????
            if(log.isLoggable(Level.WARNING)) log.warning("Fixture file directory does not exist. fixtureDirName = " + fixtureDirName);
        } else {
            File[] files = fixtureDir.listFiles();
            for(File f : files) {
                if(f.isFile()) {
                    String filePath = f.getPath();  // ???
                    FixtureFile ff = new FixtureFile(filePath);
                    list.add(ff);
                } else {
                    // This should not happen. Ignore...                    
                }
            }
        }
        
        return list;
    }
    
    public int processFixtureFiles()
    {
        List<FixtureFile> list = buildFixtureFileList();
        if(log.isLoggable(Level.FINE)) {
            for(FixtureFile f : list) {
                log.fine("FixtureFile f = " + f);
            }
        }
        
        int count = 0;
        for(FixtureFile f : list) {  // list cannot be null.
            // [1] Read the file
            // [2] Convert the file content to objects
            // [3] Then, save it. (We use update/overwrite rather than create to ensure "idempotency".)
            String filePath = f.getFilePath();
            String objectType = f.getObjectType();
            String mediaType = f.getMediaType();
            
            // JSON only, for now....
            if(! FixtureUtil.MEDIA_TYPE_JSON.equals(mediaType)) {
                if(log.isLoggable(Level.WARNING)) log.warning("Currently supports only Json fixture files. Skipping filePath = " + filePath);
                continue;
            }

            BufferedReader reader = null;
            File inputFile = new File(filePath);
            if(inputFile.canRead()) {
                try {
                    reader = new BufferedReader(new FileReader(inputFile));
                    StringBuilder sb = new StringBuilder(0x1000);
                    final char[] buf = new char[0x1000];
                    int read = -1;
                    do {
                        read = reader.read(buf, 0, buf.length);
                        if (read>0) {
                            sb.append(buf, 0, read);
                        }
                    } while (read>=0);

                    if(dummyFalse1) {
                    } else if("User".equals(objectType)) {
                        UserStub stub = UserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserBean bean = UserResourceImpl.convertUserStubToBean(stub);
                        boolean suc = ServiceManager.getUserService().updateUser(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("UserList".equals(objectType)) {
                        UserListStub listStub = UserListStub.fromJsonString(sb.toString());
                        List<UserBean> beanList = UserResourceImpl.convertUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserService().updateUser(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FetchRequest".equals(objectType)) {
                        FetchRequestStub stub = FetchRequestStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FetchRequestBean bean = FetchRequestResourceImpl.convertFetchRequestStubToBean(stub);
                        boolean suc = ServiceManager.getFetchRequestService().updateFetchRequest(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("FetchRequestList".equals(objectType)) {
                        FetchRequestListStub listStub = FetchRequestListStub.fromJsonString(sb.toString());
                        List<FetchRequestBean> beanList = FetchRequestResourceImpl.convertFetchRequestListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FetchRequestBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFetchRequestService().updateFetchRequest(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("SecondaryFetch".equals(objectType)) {
                        SecondaryFetchStub stub = SecondaryFetchStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        SecondaryFetchBean bean = SecondaryFetchResourceImpl.convertSecondaryFetchStubToBean(stub);
                        boolean suc = ServiceManager.getSecondaryFetchService().updateSecondaryFetch(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("SecondaryFetchList".equals(objectType)) {
                        SecondaryFetchListStub listStub = SecondaryFetchListStub.fromJsonString(sb.toString());
                        List<SecondaryFetchBean> beanList = SecondaryFetchResourceImpl.convertSecondaryFetchListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(SecondaryFetchBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getSecondaryFetchService().updateSecondaryFetch(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ChannelSource".equals(objectType)) {
                        ChannelSourceStub stub = ChannelSourceStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ChannelSourceBean bean = ChannelSourceResourceImpl.convertChannelSourceStubToBean(stub);
                        boolean suc = ServiceManager.getChannelSourceService().updateChannelSource(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ChannelSourceList".equals(objectType)) {
                        ChannelSourceListStub listStub = ChannelSourceListStub.fromJsonString(sb.toString());
                        List<ChannelSourceBean> beanList = ChannelSourceResourceImpl.convertChannelSourceListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ChannelSourceBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getChannelSourceService().updateChannelSource(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ChannelFeed".equals(objectType)) {
                        ChannelFeedStub stub = ChannelFeedStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ChannelFeedBean bean = ChannelFeedResourceImpl.convertChannelFeedStubToBean(stub);
                        boolean suc = ServiceManager.getChannelFeedService().updateChannelFeed(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ChannelFeedList".equals(objectType)) {
                        ChannelFeedListStub listStub = ChannelFeedListStub.fromJsonString(sb.toString());
                        List<ChannelFeedBean> beanList = ChannelFeedResourceImpl.convertChannelFeedListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ChannelFeedBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getChannelFeedService().updateChannelFeed(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FeedItem".equals(objectType)) {
                        FeedItemStub stub = FeedItemStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FeedItemBean bean = FeedItemResourceImpl.convertFeedItemStubToBean(stub);
                        boolean suc = ServiceManager.getFeedItemService().updateFeedItem(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("FeedItemList".equals(objectType)) {
                        FeedItemListStub listStub = FeedItemListStub.fromJsonString(sb.toString());
                        List<FeedItemBean> beanList = FeedItemResourceImpl.convertFeedItemListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FeedItemBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFeedItemService().updateFeedItem(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FeedContent".equals(objectType)) {
                        FeedContentStub stub = FeedContentStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FeedContentBean bean = FeedContentResourceImpl.convertFeedContentStubToBean(stub);
                        boolean suc = ServiceManager.getFeedContentService().updateFeedContent(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("FeedContentList".equals(objectType)) {
                        FeedContentListStub listStub = FeedContentListStub.fromJsonString(sb.toString());
                        List<FeedContentBean> beanList = FeedContentResourceImpl.convertFeedContentListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FeedContentBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFeedContentService().updateFeedContent(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfo".equals(objectType)) {
                        ServiceInfoStub stub = ServiceInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceInfoBean bean = ServiceInfoResourceImpl.convertServiceInfoStubToBean(stub);
                        boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("ServiceInfoList".equals(objectType)) {
                        ServiceInfoListStub listStub = ServiceInfoListStub.fromJsonString(sb.toString());
                        List<ServiceInfoBean> beanList = ServiceInfoResourceImpl.convertServiceInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTen".equals(objectType)) {
                        FiveTenStub stub = FiveTenStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FiveTenBean bean = FiveTenResourceImpl.convertFiveTenStubToBean(stub);
                        boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }                        
                    } else if("FiveTenList".equals(objectType)) {
                        FiveTenListStub listStub = FiveTenListStub.fromJsonString(sb.toString());
                        List<FiveTenBean> beanList = FiveTenResourceImpl.convertFiveTenListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FiveTenBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else {
                        // This cannot happen
                    }
                } catch (FileNotFoundException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (IOException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (BaseException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process a fixture file: filePath = " + filePath, e);
                } catch (Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected error while processing a fixture file: filePath = " + filePath, e);
                } finally {
                    if(reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while closing the fixture file: filePath = " + filePath, e);
                        }
                    }
                }                
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("Skipping a fixture file because it is not readable: filePath = " + filePath);
            }
        }
                
        return count;
    }
    
}
