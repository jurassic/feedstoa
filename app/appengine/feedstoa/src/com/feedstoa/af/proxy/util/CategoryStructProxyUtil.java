package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.CategoryStruct;
// import com.feedstoa.ws.bean.CategoryStructBean;
import com.feedstoa.af.bean.CategoryStructBean;


public class CategoryStructProxyUtil
{
    private static final Logger log = Logger.getLogger(CategoryStructProxyUtil.class.getName());

    // Static methods only.
    private CategoryStructProxyUtil() {}

    public static CategoryStructBean convertServerCategoryStructBeanToAppBean(CategoryStruct serverBean)
    {
        CategoryStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new CategoryStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setDomain(serverBean.getDomain());
            bean.setLabel(serverBean.getLabel());
            bean.setTitle(serverBean.getTitle());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.CategoryStructBean convertAppCategoryStructBeanToServerBean(CategoryStruct appBean)
    {
        com.feedstoa.ws.bean.CategoryStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.CategoryStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setDomain(appBean.getDomain());
            bean.setLabel(appBean.getLabel());
            bean.setTitle(appBean.getTitle());
        }
        return bean;
    }

}
