package com.feedstoa.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.FeedContent;
// import com.feedstoa.ws.bean.FeedContentBean;
import com.feedstoa.ws.service.FeedContentService;
import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.af.proxy.FeedContentServiceProxy;


public class LocalFeedContentServiceProxy extends BaseLocalServiceProxy implements FeedContentServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFeedContentServiceProxy.class.getName());

    public LocalFeedContentServiceProxy()
    {
    }

    @Override
    public FeedContent getFeedContent(String guid) throws BaseException
    {
        FeedContent serverBean = getFeedContentService().getFeedContent(guid);
        FeedContent appBean = convertServerFeedContentBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getFeedContent(String guid, String field) throws BaseException
    {
        return getFeedContentService().getFeedContent(guid, field);       
    }

    @Override
    public List<FeedContent> getFeedContents(List<String> guids) throws BaseException
    {
        List<FeedContent> serverBeanList = getFeedContentService().getFeedContents(guids);
        List<FeedContent> appBeanList = convertServerFeedContentBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<FeedContent> getAllFeedContents() throws BaseException
    {
        return getAllFeedContents(null, null, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFeedContentService().getAllFeedContents(ordering, offset, count);
        return getAllFeedContents(ordering, offset, count, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FeedContent> serverBeanList = getFeedContentService().getAllFeedContents(ordering, offset, count, forwardCursor);
        List<FeedContent> appBeanList = convertServerFeedContentBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFeedContentService().getAllFeedContentKeys(ordering, offset, count);
        return getAllFeedContentKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFeedContentService().getAllFeedContentKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFeedContents(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFeedContentService().findFeedContents(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FeedContent> serverBeanList = getFeedContentService().findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<FeedContent> appBeanList = convertServerFeedContentBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFeedContentService().findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFeedContentService().findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFeedContentService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFeedContent(String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        return getFeedContentService().createFeedContent(user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
    }

    @Override
    public String createFeedContent(FeedContent feedContent) throws BaseException
    {
        com.feedstoa.ws.bean.FeedContentBean serverBean =  convertAppFeedContentBeanToServerBean(feedContent);
        return getFeedContentService().createFeedContent(serverBean);
    }

    @Override
    public Boolean updateFeedContent(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        return getFeedContentService().updateFeedContent(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
    }

    @Override
    public Boolean updateFeedContent(FeedContent feedContent) throws BaseException
    {
        com.feedstoa.ws.bean.FeedContentBean serverBean =  convertAppFeedContentBeanToServerBean(feedContent);
        return getFeedContentService().updateFeedContent(serverBean);
    }

    @Override
    public Boolean deleteFeedContent(String guid) throws BaseException
    {
        return getFeedContentService().deleteFeedContent(guid);
    }

    @Override
    public Boolean deleteFeedContent(FeedContent feedContent) throws BaseException
    {
        com.feedstoa.ws.bean.FeedContentBean serverBean =  convertAppFeedContentBeanToServerBean(feedContent);
        return getFeedContentService().deleteFeedContent(serverBean);
    }

    @Override
    public Long deleteFeedContents(String filter, String params, List<String> values) throws BaseException
    {
        return getFeedContentService().deleteFeedContents(filter, params, values);
    }




    public static FeedContentBean convertServerFeedContentBeanToAppBean(FeedContent serverBean)
    {
        FeedContentBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new FeedContentBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setChannelSource(serverBean.getChannelSource());
            bean.setChannelFeed(serverBean.getChannelFeed());
            bean.setChannelVersion(serverBean.getChannelVersion());
            bean.setFeedUrl(serverBean.getFeedUrl());
            bean.setFeedFormat(serverBean.getFeedFormat());
            bean.setContent(serverBean.getContent());
            bean.setContentHash(serverBean.getContentHash());
            bean.setStatus(serverBean.getStatus());
            bean.setNote(serverBean.getNote());
            bean.setLastBuildDate(serverBean.getLastBuildDate());
            bean.setLastBuildTime(serverBean.getLastBuildTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FeedContent> convertServerFeedContentBeanListToAppBeanList(List<FeedContent> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<FeedContent> beanList = new ArrayList<FeedContent>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(FeedContent sb : serverBeanList) {
                FeedContentBean bean = convertServerFeedContentBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.feedstoa.ws.bean.FeedContentBean convertAppFeedContentBeanToServerBean(FeedContent appBean)
    {
        com.feedstoa.ws.bean.FeedContentBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.FeedContentBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setChannelSource(appBean.getChannelSource());
            bean.setChannelFeed(appBean.getChannelFeed());
            bean.setChannelVersion(appBean.getChannelVersion());
            bean.setFeedUrl(appBean.getFeedUrl());
            bean.setFeedFormat(appBean.getFeedFormat());
            bean.setContent(appBean.getContent());
            bean.setContentHash(appBean.getContentHash());
            bean.setStatus(appBean.getStatus());
            bean.setNote(appBean.getNote());
            bean.setLastBuildDate(appBean.getLastBuildDate());
            bean.setLastBuildTime(appBean.getLastBuildTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FeedContent> convertAppFeedContentBeanListToServerBeanList(List<FeedContent> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<FeedContent> beanList = new ArrayList<FeedContent>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(FeedContent sb : appBeanList) {
                com.feedstoa.ws.bean.FeedContentBean bean = convertAppFeedContentBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
