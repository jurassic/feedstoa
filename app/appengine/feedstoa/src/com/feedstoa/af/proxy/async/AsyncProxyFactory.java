package com.feedstoa.af.proxy.async;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.ApiConsumerServiceProxy;
import com.feedstoa.af.proxy.UserServiceProxy;
import com.feedstoa.af.proxy.FetchRequestServiceProxy;
import com.feedstoa.af.proxy.SecondaryFetchServiceProxy;
import com.feedstoa.af.proxy.ChannelSourceServiceProxy;
import com.feedstoa.af.proxy.ChannelFeedServiceProxy;
import com.feedstoa.af.proxy.FeedItemServiceProxy;
import com.feedstoa.af.proxy.FeedContentServiceProxy;
import com.feedstoa.af.proxy.ServiceInfoServiceProxy;
import com.feedstoa.af.proxy.FiveTenServiceProxy;

public class AsyncProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(AsyncProxyFactory.class.getName());

    private AsyncProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AsyncProxyFactoryHolder
    {
        private static final AsyncProxyFactory INSTANCE = new AsyncProxyFactory();
    }

    // Singleton method
    public static AsyncProxyFactory getInstance()
    {
        return AsyncProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new AsyncApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new AsyncUserServiceProxy();
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        return new AsyncFetchRequestServiceProxy();
    }

    @Override
    public SecondaryFetchServiceProxy getSecondaryFetchServiceProxy()
    {
        return new AsyncSecondaryFetchServiceProxy();
    }

    @Override
    public ChannelSourceServiceProxy getChannelSourceServiceProxy()
    {
        return new AsyncChannelSourceServiceProxy();
    }

    @Override
    public ChannelFeedServiceProxy getChannelFeedServiceProxy()
    {
        return new AsyncChannelFeedServiceProxy();
    }

    @Override
    public FeedItemServiceProxy getFeedItemServiceProxy()
    {
        return new AsyncFeedItemServiceProxy();
    }

    @Override
    public FeedContentServiceProxy getFeedContentServiceProxy()
    {
        return new AsyncFeedContentServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new AsyncServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new AsyncFiveTenServiceProxy();
    }

}
