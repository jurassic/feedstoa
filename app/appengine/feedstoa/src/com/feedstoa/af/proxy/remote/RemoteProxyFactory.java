package com.feedstoa.af.proxy.remote;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.ApiConsumerServiceProxy;
import com.feedstoa.af.proxy.UserServiceProxy;
import com.feedstoa.af.proxy.FetchRequestServiceProxy;
import com.feedstoa.af.proxy.SecondaryFetchServiceProxy;
import com.feedstoa.af.proxy.ChannelSourceServiceProxy;
import com.feedstoa.af.proxy.ChannelFeedServiceProxy;
import com.feedstoa.af.proxy.FeedItemServiceProxy;
import com.feedstoa.af.proxy.FeedContentServiceProxy;
import com.feedstoa.af.proxy.ServiceInfoServiceProxy;
import com.feedstoa.af.proxy.FiveTenServiceProxy;

public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new RemoteApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new RemoteUserServiceProxy();
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        return new RemoteFetchRequestServiceProxy();
    }

    @Override
    public SecondaryFetchServiceProxy getSecondaryFetchServiceProxy()
    {
        return new RemoteSecondaryFetchServiceProxy();
    }

    @Override
    public ChannelSourceServiceProxy getChannelSourceServiceProxy()
    {
        return new RemoteChannelSourceServiceProxy();
    }

    @Override
    public ChannelFeedServiceProxy getChannelFeedServiceProxy()
    {
        return new RemoteChannelFeedServiceProxy();
    }

    @Override
    public FeedItemServiceProxy getFeedItemServiceProxy()
    {
        return new RemoteFeedItemServiceProxy();
    }

    @Override
    public FeedContentServiceProxy getFeedContentServiceProxy()
    {
        return new RemoteFeedContentServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new RemoteServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new RemoteFiveTenServiceProxy();
    }

}
