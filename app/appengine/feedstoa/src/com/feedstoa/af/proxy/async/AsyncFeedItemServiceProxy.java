package com.feedstoa.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.EnclosureStructStub;
import com.feedstoa.ws.stub.EnclosureStructListStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.CategoryStructListStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.stub.UriStructListStub;
import com.feedstoa.ws.stub.UserStructStub;
import com.feedstoa.ws.stub.UserStructListStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructListStub;
import com.feedstoa.ws.stub.FeedItemStub;
import com.feedstoa.ws.stub.FeedItemListStub;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.ws.service.FeedItemService;
import com.feedstoa.af.proxy.FeedItemServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteFeedItemServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncFeedItemServiceProxy extends BaseAsyncServiceProxy implements FeedItemServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncFeedItemServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteFeedItemServiceProxy remoteProxy;

    public AsyncFeedItemServiceProxy()
    {
        remoteProxy = new RemoteFeedItemServiceProxy();
    }

    @Override
    public FeedItem getFeedItem(String guid) throws BaseException
    {
        return remoteProxy.getFeedItem(guid);
    }

    @Override
    public Object getFeedItem(String guid, String field) throws BaseException
    {
        return remoteProxy.getFeedItem(guid, field);       
    }

    @Override
    public List<FeedItem> getFeedItems(List<String> guids) throws BaseException
    {
        return remoteProxy.getFeedItems(guids);
    }

    @Override
    public List<FeedItem> getAllFeedItems() throws BaseException
    {
        return getAllFeedItems(null, null, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFeedItems(ordering, offset, count);
        return getAllFeedItems(ordering, offset, count, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFeedItems(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFeedItemKeys(ordering, offset, count);
        return getAllFeedItemKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFeedItemKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFeedItems(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFeedItem(String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        FeedItemBean bean = new FeedItemBean(null, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, MarshalHelper.convertUriStructToBean(link), summary, content, MarshalHelper.convertUserStructToBean(author), contributor, category, comments, MarshalHelper.convertEnclosureStructToBean(enclosure), id, pubDate, MarshalHelper.convertUriStructToBean(source), feedContent, status, note, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), publishedTime, lastUpdatedTime);
        return createFeedItem(bean);        
    }

    @Override
    public String createFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        String guid = feedItem.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((FeedItemBean) feedItem).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateFeedItem-" + guid;
        String taskName = "RsCreateFeedItem-" + guid + "-" + (new Date()).getTime();
        FeedItemStub stub = MarshalHelper.convertFeedItemToStub(feedItem);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FeedItemStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = feedItem.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FeedItemStub dummyStub = new FeedItemStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFeedItem(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedItems/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFeedItem(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedItems/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FeedItem guid is invalid.");
        	throw new BaseException("FeedItem guid is invalid.");
        }
        FeedItemBean bean = new FeedItemBean(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, MarshalHelper.convertUriStructToBean(link), summary, content, MarshalHelper.convertUserStructToBean(author), contributor, category, comments, MarshalHelper.convertEnclosureStructToBean(enclosure), id, pubDate, MarshalHelper.convertUriStructToBean(source), feedContent, status, note, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), publishedTime, lastUpdatedTime);
        return updateFeedItem(bean);        
    }

    @Override
    public Boolean updateFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        String guid = feedItem.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FeedItem object is invalid.");
        	throw new BaseException("FeedItem object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateFeedItem-" + guid;
        String taskName = "RsUpdateFeedItem-" + guid + "-" + (new Date()).getTime();
        FeedItemStub stub = MarshalHelper.convertFeedItemToStub(feedItem);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FeedItemStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = feedItem.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FeedItemStub dummyStub = new FeedItemStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFeedItem(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedItems/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFeedItem(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedItems/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFeedItem(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteFeedItem-" + guid;
        String taskName = "RsDeleteFeedItem-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedItems/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFeedItem(FeedItem feedItem) throws BaseException
    {
        String guid = feedItem.getGuid();
        return deleteFeedItem(guid);
    }

    @Override
    public Long deleteFeedItems(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteFeedItems(filter, params, values);
    }

}
