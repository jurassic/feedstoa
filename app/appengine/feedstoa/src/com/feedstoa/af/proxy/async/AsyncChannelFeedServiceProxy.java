package com.feedstoa.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.CloudStructStub;
import com.feedstoa.ws.stub.CloudStructListStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.CategoryStructListStub;
import com.feedstoa.ws.stub.ImageStructStub;
import com.feedstoa.ws.stub.ImageStructListStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.stub.UriStructListStub;
import com.feedstoa.ws.stub.UserStructStub;
import com.feedstoa.ws.stub.UserStructListStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructListStub;
import com.feedstoa.ws.stub.TextInputStructStub;
import com.feedstoa.ws.stub.TextInputStructListStub;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.stub.ChannelFeedListStub;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.ws.service.ChannelFeedService;
import com.feedstoa.af.proxy.ChannelFeedServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteChannelFeedServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncChannelFeedServiceProxy extends BaseAsyncServiceProxy implements ChannelFeedServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncChannelFeedServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteChannelFeedServiceProxy remoteProxy;

    public AsyncChannelFeedServiceProxy()
    {
        remoteProxy = new RemoteChannelFeedServiceProxy();
    }

    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        return remoteProxy.getChannelFeed(guid);
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        return remoteProxy.getChannelFeed(guid, field);       
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        return remoteProxy.getChannelFeeds(guids);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return getAllChannelFeeds(null, null, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllChannelFeeds(ordering, offset, count);
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllChannelFeeds(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllChannelFeedKeys(ordering, offset, count);
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllChannelFeedKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        ChannelFeedBean bean = new ChannelFeedBean(null, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, MarshalHelper.convertUriStructToBean(link), description, language, copyright, MarshalHelper.convertUserStructToBean(managingEditor), MarshalHelper.convertUserStructToBean(webMaster), contributor, pubDate, lastBuildDate, category, generator, docs, MarshalHelper.convertCloudStructToBean(cloud), ttl, MarshalHelper.convertImageStructToBean(logo), MarshalHelper.convertImageStructToBean(icon), rating, MarshalHelper.convertTextInputStructToBean(textInput), skipHours, skipDays, outputText, outputHash, feedContent, status, note, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return createChannelFeed(bean);        
    }

    @Override
    public String createChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        String guid = channelFeed.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ChannelFeedBean) channelFeed).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateChannelFeed-" + guid;
        String taskName = "RsCreateChannelFeed-" + guid + "-" + (new Date()).getTime();
        ChannelFeedStub stub = MarshalHelper.convertChannelFeedToStub(channelFeed);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ChannelFeedStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = channelFeed.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ChannelFeedStub dummyStub = new ChannelFeedStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createChannelFeed(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelFeeds/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createChannelFeed(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelFeeds/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelFeed guid is invalid.");
        	throw new BaseException("ChannelFeed guid is invalid.");
        }
        ChannelFeedBean bean = new ChannelFeedBean(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, MarshalHelper.convertUriStructToBean(link), description, language, copyright, MarshalHelper.convertUserStructToBean(managingEditor), MarshalHelper.convertUserStructToBean(webMaster), contributor, pubDate, lastBuildDate, category, generator, docs, MarshalHelper.convertCloudStructToBean(cloud), ttl, MarshalHelper.convertImageStructToBean(logo), MarshalHelper.convertImageStructToBean(icon), rating, MarshalHelper.convertTextInputStructToBean(textInput), skipHours, skipDays, outputText, outputHash, feedContent, status, note, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return updateChannelFeed(bean);        
    }

    @Override
    public Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        String guid = channelFeed.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelFeed object is invalid.");
        	throw new BaseException("ChannelFeed object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateChannelFeed-" + guid;
        String taskName = "RsUpdateChannelFeed-" + guid + "-" + (new Date()).getTime();
        ChannelFeedStub stub = MarshalHelper.convertChannelFeedToStub(channelFeed);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ChannelFeedStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = channelFeed.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ChannelFeedStub dummyStub = new ChannelFeedStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateChannelFeed(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelFeeds/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateChannelFeed(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelFeeds/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteChannelFeed-" + guid;
        String taskName = "RsDeleteChannelFeed-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelFeeds/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        String guid = channelFeed.getGuid();
        return deleteChannelFeed(guid);
    }

    @Override
    public Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteChannelFeeds(filter, params, values);
    }

}
