package com.feedstoa.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.ChannelSourceStub;
import com.feedstoa.ws.stub.ChannelSourceListStub;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.ws.service.ChannelSourceService;
import com.feedstoa.af.proxy.ChannelSourceServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteChannelSourceServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncChannelSourceServiceProxy extends BaseAsyncServiceProxy implements ChannelSourceServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncChannelSourceServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteChannelSourceServiceProxy remoteProxy;

    public AsyncChannelSourceServiceProxy()
    {
        remoteProxy = new RemoteChannelSourceServiceProxy();
    }

    @Override
    public ChannelSource getChannelSource(String guid) throws BaseException
    {
        return remoteProxy.getChannelSource(guid);
    }

    @Override
    public Object getChannelSource(String guid, String field) throws BaseException
    {
        return remoteProxy.getChannelSource(guid, field);       
    }

    @Override
    public List<ChannelSource> getChannelSources(List<String> guids) throws BaseException
    {
        return remoteProxy.getChannelSources(guids);
    }

    @Override
    public List<ChannelSource> getAllChannelSources() throws BaseException
    {
        return getAllChannelSources(null, null, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllChannelSources(ordering, offset, count);
        return getAllChannelSources(ordering, offset, count, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllChannelSources(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllChannelSourceKeys(ordering, offset, count);
        return getAllChannelSourceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllChannelSourceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findChannelSources(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findChannelSources(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createChannelSource(String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        ChannelSourceBean bean = new ChannelSourceBean(null, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        return createChannelSource(bean);        
    }

    @Override
    public String createChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        String guid = channelSource.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((ChannelSourceBean) channelSource).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateChannelSource-" + guid;
        String taskName = "RsCreateChannelSource-" + guid + "-" + (new Date()).getTime();
        ChannelSourceStub stub = MarshalHelper.convertChannelSourceToStub(channelSource);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ChannelSourceStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = channelSource.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ChannelSourceStub dummyStub = new ChannelSourceStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createChannelSource(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelSources/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createChannelSource(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelSources/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateChannelSource(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelSource guid is invalid.");
        	throw new BaseException("ChannelSource guid is invalid.");
        }
        ChannelSourceBean bean = new ChannelSourceBean(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        return updateChannelSource(bean);        
    }

    @Override
    public Boolean updateChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        String guid = channelSource.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ChannelSource object is invalid.");
        	throw new BaseException("ChannelSource object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateChannelSource-" + guid;
        String taskName = "RsUpdateChannelSource-" + guid + "-" + (new Date()).getTime();
        ChannelSourceStub stub = MarshalHelper.convertChannelSourceToStub(channelSource);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(ChannelSourceStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = channelSource.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    ChannelSourceStub dummyStub = new ChannelSourceStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateChannelSource(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelSources/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateChannelSource(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelSources/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteChannelSource(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteChannelSource-" + guid;
        String taskName = "RsDeleteChannelSource-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "channelSources/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteChannelSource(ChannelSource channelSource) throws BaseException
    {
        String guid = channelSource.getGuid();
        return deleteChannelSource(guid);
    }

    @Override
    public Long deleteChannelSources(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteChannelSources(filter, params, values);
    }

}
