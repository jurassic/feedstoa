package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.ContactInfoStruct;
// import com.feedstoa.ws.bean.ContactInfoStructBean;
import com.feedstoa.af.bean.ContactInfoStructBean;


public class ContactInfoStructProxyUtil
{
    private static final Logger log = Logger.getLogger(ContactInfoStructProxyUtil.class.getName());

    // Static methods only.
    private ContactInfoStructProxyUtil() {}

    public static ContactInfoStructBean convertServerContactInfoStructBeanToAppBean(ContactInfoStruct serverBean)
    {
        ContactInfoStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ContactInfoStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setStreetAddress(serverBean.getStreetAddress());
            bean.setLocality(serverBean.getLocality());
            bean.setRegion(serverBean.getRegion());
            bean.setPostalCode(serverBean.getPostalCode());
            bean.setCountryName(serverBean.getCountryName());
            bean.setEmailAddress(serverBean.getEmailAddress());
            bean.setPhoneNumber(serverBean.getPhoneNumber());
            bean.setFaxNumber(serverBean.getFaxNumber());
            bean.setWebsite(serverBean.getWebsite());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.ContactInfoStructBean convertAppContactInfoStructBeanToServerBean(ContactInfoStruct appBean)
    {
        com.feedstoa.ws.bean.ContactInfoStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.ContactInfoStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setStreetAddress(appBean.getStreetAddress());
            bean.setLocality(appBean.getLocality());
            bean.setRegion(appBean.getRegion());
            bean.setPostalCode(appBean.getPostalCode());
            bean.setCountryName(appBean.getCountryName());
            bean.setEmailAddress(appBean.getEmailAddress());
            bean.setPhoneNumber(appBean.getPhoneNumber());
            bean.setFaxNumber(appBean.getFaxNumber());
            bean.setWebsite(appBean.getWebsite());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
