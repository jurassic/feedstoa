package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.TextInputStruct;
// import com.feedstoa.ws.bean.TextInputStructBean;
import com.feedstoa.af.bean.TextInputStructBean;


public class TextInputStructProxyUtil
{
    private static final Logger log = Logger.getLogger(TextInputStructProxyUtil.class.getName());

    // Static methods only.
    private TextInputStructProxyUtil() {}

    public static TextInputStructBean convertServerTextInputStructBeanToAppBean(TextInputStruct serverBean)
    {
        TextInputStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new TextInputStructBean();
            bean.setTitle(serverBean.getTitle());
            bean.setName(serverBean.getName());
            bean.setLink(serverBean.getLink());
            bean.setDescription(serverBean.getDescription());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.TextInputStructBean convertAppTextInputStructBeanToServerBean(TextInputStruct appBean)
    {
        com.feedstoa.ws.bean.TextInputStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.TextInputStructBean();
            bean.setTitle(appBean.getTitle());
            bean.setName(appBean.getName());
            bean.setLink(appBean.getLink());
            bean.setDescription(appBean.getDescription());
        }
        return bean;
    }

}
