package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.NotificationStruct;
// import com.feedstoa.ws.bean.NotificationStructBean;
import com.feedstoa.af.bean.NotificationStructBean;


public class NotificationStructProxyUtil
{
    private static final Logger log = Logger.getLogger(NotificationStructProxyUtil.class.getName());

    // Static methods only.
    private NotificationStructProxyUtil() {}

    public static NotificationStructBean convertServerNotificationStructBeanToAppBean(NotificationStruct serverBean)
    {
        NotificationStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new NotificationStructBean();
            bean.setPreferredMode(serverBean.getPreferredMode());
            bean.setMobileNumber(serverBean.getMobileNumber());
            bean.setEmailAddress(serverBean.getEmailAddress());
            bean.setTwitterUsername(serverBean.getTwitterUsername());
            bean.setFacebookId(serverBean.getFacebookId());
            bean.setLinkedinId(serverBean.getLinkedinId());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.NotificationStructBean convertAppNotificationStructBeanToServerBean(NotificationStruct appBean)
    {
        com.feedstoa.ws.bean.NotificationStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.NotificationStructBean();
            bean.setPreferredMode(appBean.getPreferredMode());
            bean.setMobileNumber(appBean.getMobileNumber());
            bean.setEmailAddress(appBean.getEmailAddress());
            bean.setTwitterUsername(appBean.getTwitterUsername());
            bean.setFacebookId(appBean.getFacebookId());
            bean.setLinkedinId(appBean.getLinkedinId());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
