package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.UriStruct;
// import com.feedstoa.ws.bean.UriStructBean;
import com.feedstoa.af.bean.UriStructBean;


public class UriStructProxyUtil
{
    private static final Logger log = Logger.getLogger(UriStructProxyUtil.class.getName());

    // Static methods only.
    private UriStructProxyUtil() {}

    public static UriStructBean convertServerUriStructBeanToAppBean(UriStruct serverBean)
    {
        UriStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new UriStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setHref(serverBean.getHref());
            bean.setRel(serverBean.getRel());
            bean.setType(serverBean.getType());
            bean.setLabel(serverBean.getLabel());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.UriStructBean convertAppUriStructBeanToServerBean(UriStruct appBean)
    {
        com.feedstoa.ws.bean.UriStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.UriStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setHref(appBean.getHref());
            bean.setRel(appBean.getRel());
            bean.setType(appBean.getType());
            bean.setLabel(appBean.getLabel());
        }
        return bean;
    }

}
