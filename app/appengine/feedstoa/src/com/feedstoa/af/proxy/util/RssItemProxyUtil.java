package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
// import com.feedstoa.ws.bean.RssItemBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.RssItemBean;


public class RssItemProxyUtil
{
    private static final Logger log = Logger.getLogger(RssItemProxyUtil.class.getName());

    // Static methods only.
    private RssItemProxyUtil() {}

    public static RssItemBean convertServerRssItemBeanToAppBean(RssItem serverBean)
    {
        RssItemBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new RssItemBean();
            bean.setTitle(serverBean.getTitle());
            bean.setLink(serverBean.getLink());
            bean.setDescription(serverBean.getDescription());
            bean.setAuthor(serverBean.getAuthor());
            bean.setCategory(serverBean.getCategory());
            bean.setComments(serverBean.getComments());
            bean.setEnclosure(EnclosureStructProxyUtil.convertServerEnclosureStructBeanToAppBean(serverBean.getEnclosure()));
            bean.setGuid(serverBean.getGuid());
            bean.setPubDate(serverBean.getPubDate());
            bean.setSource(UriStructProxyUtil.convertServerUriStructBeanToAppBean(serverBean.getSource()));
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.RssItemBean convertAppRssItemBeanToServerBean(RssItem appBean)
    {
        com.feedstoa.ws.bean.RssItemBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.RssItemBean();
            bean.setTitle(appBean.getTitle());
            bean.setLink(appBean.getLink());
            bean.setDescription(appBean.getDescription());
            bean.setAuthor(appBean.getAuthor());
            bean.setCategory(appBean.getCategory());
            bean.setComments(appBean.getComments());
            bean.setEnclosure(EnclosureStructProxyUtil.convertAppEnclosureStructBeanToServerBean(appBean.getEnclosure()));
            bean.setGuid(appBean.getGuid());
            bean.setPubDate(appBean.getPubDate());
            bean.setSource(UriStructProxyUtil.convertAppUriStructBeanToServerBean(appBean.getSource()));
        }
        return bean;
    }

}
