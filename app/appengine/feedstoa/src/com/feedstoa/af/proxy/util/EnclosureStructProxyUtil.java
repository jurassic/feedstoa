package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.EnclosureStruct;
// import com.feedstoa.ws.bean.EnclosureStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;


public class EnclosureStructProxyUtil
{
    private static final Logger log = Logger.getLogger(EnclosureStructProxyUtil.class.getName());

    // Static methods only.
    private EnclosureStructProxyUtil() {}

    public static EnclosureStructBean convertServerEnclosureStructBeanToAppBean(EnclosureStruct serverBean)
    {
        EnclosureStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new EnclosureStructBean();
            bean.setUrl(serverBean.getUrl());
            bean.setLength(serverBean.getLength());
            bean.setType(serverBean.getType());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.EnclosureStructBean convertAppEnclosureStructBeanToServerBean(EnclosureStruct appBean)
    {
        com.feedstoa.ws.bean.EnclosureStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.EnclosureStructBean();
            bean.setUrl(appBean.getUrl());
            bean.setLength(appBean.getLength());
            bean.setType(appBean.getType());
        }
        return bean;
    }

}
