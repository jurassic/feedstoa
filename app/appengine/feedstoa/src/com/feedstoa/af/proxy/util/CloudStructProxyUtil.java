package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.CloudStruct;
// import com.feedstoa.ws.bean.CloudStructBean;
import com.feedstoa.af.bean.CloudStructBean;


public class CloudStructProxyUtil
{
    private static final Logger log = Logger.getLogger(CloudStructProxyUtil.class.getName());

    // Static methods only.
    private CloudStructProxyUtil() {}

    public static CloudStructBean convertServerCloudStructBeanToAppBean(CloudStruct serverBean)
    {
        CloudStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new CloudStructBean();
            bean.setDomain(serverBean.getDomain());
            bean.setPort(serverBean.getPort());
            bean.setPath(serverBean.getPath());
            bean.setRegisterProcedure(serverBean.getRegisterProcedure());
            bean.setProtocol(serverBean.getProtocol());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.CloudStructBean convertAppCloudStructBeanToServerBean(CloudStruct appBean)
    {
        com.feedstoa.ws.bean.CloudStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.CloudStructBean();
            bean.setDomain(appBean.getDomain());
            bean.setPort(appBean.getPort());
            bean.setPath(appBean.getPath());
            bean.setRegisterProcedure(appBean.getRegisterProcedure());
            bean.setProtocol(appBean.getProtocol());
        }
        return bean;
    }

}
