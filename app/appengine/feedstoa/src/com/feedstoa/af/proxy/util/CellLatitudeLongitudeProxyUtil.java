package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.CellLatitudeLongitude;
// import com.feedstoa.ws.bean.CellLatitudeLongitudeBean;
import com.feedstoa.af.bean.CellLatitudeLongitudeBean;


public class CellLatitudeLongitudeProxyUtil
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeProxyUtil.class.getName());

    // Static methods only.
    private CellLatitudeLongitudeProxyUtil() {}

    public static CellLatitudeLongitudeBean convertServerCellLatitudeLongitudeBeanToAppBean(CellLatitudeLongitude serverBean)
    {
        CellLatitudeLongitudeBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new CellLatitudeLongitudeBean();
            bean.setScale(serverBean.getScale());
            bean.setLatitude(serverBean.getLatitude());
            bean.setLongitude(serverBean.getLongitude());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.CellLatitudeLongitudeBean convertAppCellLatitudeLongitudeBeanToServerBean(CellLatitudeLongitude appBean)
    {
        com.feedstoa.ws.bean.CellLatitudeLongitudeBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.CellLatitudeLongitudeBean();
            bean.setScale(appBean.getScale());
            bean.setLatitude(appBean.getLatitude());
            bean.setLongitude(appBean.getLongitude());
        }
        return bean;
    }

}
