package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
// import com.feedstoa.ws.bean.RssChannelBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.RssChannelBean;


public class RssChannelProxyUtil
{
    private static final Logger log = Logger.getLogger(RssChannelProxyUtil.class.getName());

    // Static methods only.
    private RssChannelProxyUtil() {}

    public static RssChannelBean convertServerRssChannelBeanToAppBean(RssChannel serverBean)
    {
        RssChannelBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new RssChannelBean();
            bean.setTitle(serverBean.getTitle());
            bean.setLink(serverBean.getLink());
            bean.setDescription(serverBean.getDescription());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCopyright(serverBean.getCopyright());
            bean.setManagingEditor(serverBean.getManagingEditor());
            bean.setWebMaster(serverBean.getWebMaster());
            bean.setPubDate(serverBean.getPubDate());
            bean.setLastBuildDate(serverBean.getLastBuildDate());
            bean.setCategory(serverBean.getCategory());
            bean.setGenerator(serverBean.getGenerator());
            bean.setDocs(serverBean.getDocs());
            bean.setCloud(CloudStructProxyUtil.convertServerCloudStructBeanToAppBean(serverBean.getCloud()));
            bean.setTtl(serverBean.getTtl());
            bean.setImage(ImageStructProxyUtil.convertServerImageStructBeanToAppBean(serverBean.getImage()));
            bean.setRating(serverBean.getRating());
            bean.setTextInput(TextInputStructProxyUtil.convertServerTextInputStructBeanToAppBean(serverBean.getTextInput()));
            bean.setSkipHours(serverBean.getSkipHours());
            bean.setSkipDays(serverBean.getSkipDays());
            bean.setItem(serverBean.getItem());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.RssChannelBean convertAppRssChannelBeanToServerBean(RssChannel appBean)
    {
        com.feedstoa.ws.bean.RssChannelBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.RssChannelBean();
            bean.setTitle(appBean.getTitle());
            bean.setLink(appBean.getLink());
            bean.setDescription(appBean.getDescription());
            bean.setLanguage(appBean.getLanguage());
            bean.setCopyright(appBean.getCopyright());
            bean.setManagingEditor(appBean.getManagingEditor());
            bean.setWebMaster(appBean.getWebMaster());
            bean.setPubDate(appBean.getPubDate());
            bean.setLastBuildDate(appBean.getLastBuildDate());
            bean.setCategory(appBean.getCategory());
            bean.setGenerator(appBean.getGenerator());
            bean.setDocs(appBean.getDocs());
            bean.setCloud(CloudStructProxyUtil.convertAppCloudStructBeanToServerBean(appBean.getCloud()));
            bean.setTtl(appBean.getTtl());
            bean.setImage(ImageStructProxyUtil.convertAppImageStructBeanToServerBean(appBean.getImage()));
            bean.setRating(appBean.getRating());
            bean.setTextInput(TextInputStructProxyUtil.convertAppTextInputStructBeanToServerBean(appBean.getTextInput()));
            bean.setSkipHours(appBean.getSkipHours());
            bean.setSkipDays(appBean.getSkipDays());
            bean.setItem(appBean.getItem());
        }
        return bean;
    }

}
