package com.feedstoa.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.af.proxy.AbstractProxyFactory;
import com.feedstoa.af.proxy.ApiConsumerServiceProxy;
import com.feedstoa.af.proxy.UserServiceProxy;
import com.feedstoa.af.proxy.FetchRequestServiceProxy;
import com.feedstoa.af.proxy.SecondaryFetchServiceProxy;
import com.feedstoa.af.proxy.ChannelSourceServiceProxy;
import com.feedstoa.af.proxy.ChannelFeedServiceProxy;
import com.feedstoa.af.proxy.FeedItemServiceProxy;
import com.feedstoa.af.proxy.FeedContentServiceProxy;
import com.feedstoa.af.proxy.ServiceInfoServiceProxy;
import com.feedstoa.af.proxy.FiveTenServiceProxy;
import com.feedstoa.ws.service.ApiConsumerService;
import com.feedstoa.ws.service.UserService;
import com.feedstoa.ws.service.FetchRequestService;
import com.feedstoa.ws.service.SecondaryFetchService;
import com.feedstoa.ws.service.ChannelSourceService;
import com.feedstoa.ws.service.ChannelFeedService;
import com.feedstoa.ws.service.FeedItemService;
import com.feedstoa.ws.service.FeedContentService;
import com.feedstoa.ws.service.ServiceInfoService;
import com.feedstoa.ws.service.FiveTenService;
import com.feedstoa.ws.service.impl.ApiConsumerServiceImpl;
import com.feedstoa.ws.service.impl.UserServiceImpl;
import com.feedstoa.ws.service.impl.FetchRequestServiceImpl;
import com.feedstoa.ws.service.impl.SecondaryFetchServiceImpl;
import com.feedstoa.ws.service.impl.ChannelSourceServiceImpl;
import com.feedstoa.ws.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.ws.service.impl.FeedItemServiceImpl;
import com.feedstoa.ws.service.impl.FeedContentServiceImpl;
import com.feedstoa.ws.service.impl.ServiceInfoServiceImpl;
import com.feedstoa.ws.service.impl.FiveTenServiceImpl;


// TBD: How to best inject the ws service instances?
public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    // Lazy initialization.
    private ApiConsumerServiceProxy apiConsumerServiceProxy = null;
    private UserServiceProxy userServiceProxy = null;
    private FetchRequestServiceProxy fetchRequestServiceProxy = null;
    private SecondaryFetchServiceProxy secondaryFetchServiceProxy = null;
    private ChannelSourceServiceProxy channelSourceServiceProxy = null;
    private ChannelFeedServiceProxy channelFeedServiceProxy = null;
    private FeedItemServiceProxy feedItemServiceProxy = null;
    private FeedContentServiceProxy feedContentServiceProxy = null;
    private ServiceInfoServiceProxy serviceInfoServiceProxy = null;
    private FiveTenServiceProxy fiveTenServiceProxy = null;


    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        if(apiConsumerServiceProxy == null) {
            apiConsumerServiceProxy = new LocalApiConsumerServiceProxy();
            ApiConsumerService apiConsumerService = new ApiConsumerServiceImpl();
            ((LocalApiConsumerServiceProxy) apiConsumerServiceProxy).setApiConsumerService(apiConsumerService);
        }
        return apiConsumerServiceProxy;
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        if(userServiceProxy == null) {
            userServiceProxy = new LocalUserServiceProxy();
            UserService userService = new UserServiceImpl();
            ((LocalUserServiceProxy) userServiceProxy).setUserService(userService);
        }
        return userServiceProxy;
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        if(fetchRequestServiceProxy == null) {
            fetchRequestServiceProxy = new LocalFetchRequestServiceProxy();
            FetchRequestService fetchRequestService = new FetchRequestServiceImpl();
            ((LocalFetchRequestServiceProxy) fetchRequestServiceProxy).setFetchRequestService(fetchRequestService);
        }
        return fetchRequestServiceProxy;
    }

    @Override
    public SecondaryFetchServiceProxy getSecondaryFetchServiceProxy()
    {
        if(secondaryFetchServiceProxy == null) {
            secondaryFetchServiceProxy = new LocalSecondaryFetchServiceProxy();
            SecondaryFetchService secondaryFetchService = new SecondaryFetchServiceImpl();
            ((LocalSecondaryFetchServiceProxy) secondaryFetchServiceProxy).setSecondaryFetchService(secondaryFetchService);
        }
        return secondaryFetchServiceProxy;
    }

    @Override
    public ChannelSourceServiceProxy getChannelSourceServiceProxy()
    {
        if(channelSourceServiceProxy == null) {
            channelSourceServiceProxy = new LocalChannelSourceServiceProxy();
            ChannelSourceService channelSourceService = new ChannelSourceServiceImpl();
            ((LocalChannelSourceServiceProxy) channelSourceServiceProxy).setChannelSourceService(channelSourceService);
        }
        return channelSourceServiceProxy;
    }

    @Override
    public ChannelFeedServiceProxy getChannelFeedServiceProxy()
    {
        if(channelFeedServiceProxy == null) {
            channelFeedServiceProxy = new LocalChannelFeedServiceProxy();
            ChannelFeedService channelFeedService = new ChannelFeedServiceImpl();
            ((LocalChannelFeedServiceProxy) channelFeedServiceProxy).setChannelFeedService(channelFeedService);
        }
        return channelFeedServiceProxy;
    }

    @Override
    public FeedItemServiceProxy getFeedItemServiceProxy()
    {
        if(feedItemServiceProxy == null) {
            feedItemServiceProxy = new LocalFeedItemServiceProxy();
            FeedItemService feedItemService = new FeedItemServiceImpl();
            ((LocalFeedItemServiceProxy) feedItemServiceProxy).setFeedItemService(feedItemService);
        }
        return feedItemServiceProxy;
    }

    @Override
    public FeedContentServiceProxy getFeedContentServiceProxy()
    {
        if(feedContentServiceProxy == null) {
            feedContentServiceProxy = new LocalFeedContentServiceProxy();
            FeedContentService feedContentService = new FeedContentServiceImpl();
            ((LocalFeedContentServiceProxy) feedContentServiceProxy).setFeedContentService(feedContentService);
        }
        return feedContentServiceProxy;
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoServiceProxy == null) {
            serviceInfoServiceProxy = new LocalServiceInfoServiceProxy();
            ServiceInfoService serviceInfoService = new ServiceInfoServiceImpl();
            ((LocalServiceInfoServiceProxy) serviceInfoServiceProxy).setServiceInfoService(serviceInfoService);
        }
        return serviceInfoServiceProxy;
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenServiceProxy == null) {
            fiveTenServiceProxy = new LocalFiveTenServiceProxy();
            FiveTenService fiveTenService = new FiveTenServiceImpl();
            ((LocalFiveTenServiceProxy) fiveTenServiceProxy).setFiveTenService(fiveTenService);
        }
        return fiveTenServiceProxy;
    }

}
