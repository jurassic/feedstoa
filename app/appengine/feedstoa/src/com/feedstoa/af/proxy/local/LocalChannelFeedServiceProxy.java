package com.feedstoa.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
// import com.feedstoa.ws.bean.ChannelFeedBean;
import com.feedstoa.ws.service.ChannelFeedService;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.proxy.ChannelFeedServiceProxy;
import com.feedstoa.af.proxy.util.UriStructProxyUtil;
import com.feedstoa.af.proxy.util.UserStructProxyUtil;
import com.feedstoa.af.proxy.util.UserStructProxyUtil;
import com.feedstoa.af.proxy.util.CloudStructProxyUtil;
import com.feedstoa.af.proxy.util.ImageStructProxyUtil;
import com.feedstoa.af.proxy.util.ImageStructProxyUtil;
import com.feedstoa.af.proxy.util.TextInputStructProxyUtil;
import com.feedstoa.af.proxy.util.ReferrerInfoStructProxyUtil;


public class LocalChannelFeedServiceProxy extends BaseLocalServiceProxy implements ChannelFeedServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalChannelFeedServiceProxy.class.getName());

    public LocalChannelFeedServiceProxy()
    {
    }

    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        ChannelFeed serverBean = getChannelFeedService().getChannelFeed(guid);
        ChannelFeed appBean = convertServerChannelFeedBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        return getChannelFeedService().getChannelFeed(guid, field);       
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        List<ChannelFeed> serverBeanList = getChannelFeedService().getChannelFeeds(guids);
        List<ChannelFeed> appBeanList = convertServerChannelFeedBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return getAllChannelFeeds(null, null, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getChannelFeedService().getAllChannelFeeds(ordering, offset, count);
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ChannelFeed> serverBeanList = getChannelFeedService().getAllChannelFeeds(ordering, offset, count, forwardCursor);
        List<ChannelFeed> appBeanList = convertServerChannelFeedBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getChannelFeedService().getAllChannelFeedKeys(ordering, offset, count);
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getChannelFeedService().getAllChannelFeedKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getChannelFeedService().findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ChannelFeed> serverBeanList = getChannelFeedService().findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ChannelFeed> appBeanList = convertServerChannelFeedBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getChannelFeedService().findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getChannelFeedService().findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getChannelFeedService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        return getChannelFeedService().createChannelFeed(user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
    }

    @Override
    public String createChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        com.feedstoa.ws.bean.ChannelFeedBean serverBean =  convertAppChannelFeedBeanToServerBean(channelFeed);
        return getChannelFeedService().createChannelFeed(serverBean);
    }

    @Override
    public Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        return getChannelFeedService().updateChannelFeed(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        com.feedstoa.ws.bean.ChannelFeedBean serverBean =  convertAppChannelFeedBeanToServerBean(channelFeed);
        return getChannelFeedService().updateChannelFeed(serverBean);
    }

    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        return getChannelFeedService().deleteChannelFeed(guid);
    }

    @Override
    public Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        com.feedstoa.ws.bean.ChannelFeedBean serverBean =  convertAppChannelFeedBeanToServerBean(channelFeed);
        return getChannelFeedService().deleteChannelFeed(serverBean);
    }

    @Override
    public Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException
    {
        return getChannelFeedService().deleteChannelFeeds(filter, params, values);
    }




    public static ChannelFeedBean convertServerChannelFeedBeanToAppBean(ChannelFeed serverBean)
    {
        ChannelFeedBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ChannelFeedBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setChannelSource(serverBean.getChannelSource());
            bean.setChannelCode(serverBean.getChannelCode());
            bean.setPreviousVersion(serverBean.getPreviousVersion());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setFetchUrl(serverBean.getFetchUrl());
            bean.setFeedServiceUrl(serverBean.getFeedServiceUrl());
            bean.setFeedUrl(serverBean.getFeedUrl());
            bean.setFeedFormat(serverBean.getFeedFormat());
            bean.setMaxItemCount(serverBean.getMaxItemCount());
            bean.setFeedCategory(serverBean.getFeedCategory());
            bean.setTitle(serverBean.getTitle());
            bean.setSubtitle(serverBean.getSubtitle());
            bean.setLink(UriStructProxyUtil.convertServerUriStructBeanToAppBean(serverBean.getLink()));
            bean.setDescription(serverBean.getDescription());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCopyright(serverBean.getCopyright());
            bean.setManagingEditor(UserStructProxyUtil.convertServerUserStructBeanToAppBean(serverBean.getManagingEditor()));
            bean.setWebMaster(UserStructProxyUtil.convertServerUserStructBeanToAppBean(serverBean.getWebMaster()));
            bean.setContributor(serverBean.getContributor());
            bean.setPubDate(serverBean.getPubDate());
            bean.setLastBuildDate(serverBean.getLastBuildDate());
            bean.setCategory(serverBean.getCategory());
            bean.setGenerator(serverBean.getGenerator());
            bean.setDocs(serverBean.getDocs());
            bean.setCloud(CloudStructProxyUtil.convertServerCloudStructBeanToAppBean(serverBean.getCloud()));
            bean.setTtl(serverBean.getTtl());
            bean.setLogo(ImageStructProxyUtil.convertServerImageStructBeanToAppBean(serverBean.getLogo()));
            bean.setIcon(ImageStructProxyUtil.convertServerImageStructBeanToAppBean(serverBean.getIcon()));
            bean.setRating(serverBean.getRating());
            bean.setTextInput(TextInputStructProxyUtil.convertServerTextInputStructBeanToAppBean(serverBean.getTextInput()));
            bean.setSkipHours(serverBean.getSkipHours());
            bean.setSkipDays(serverBean.getSkipDays());
            bean.setOutputText(serverBean.getOutputText());
            bean.setOutputHash(serverBean.getOutputHash());
            bean.setFeedContent(serverBean.getFeedContent());
            bean.setStatus(serverBean.getStatus());
            bean.setNote(serverBean.getNote());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setLastBuildTime(serverBean.getLastBuildTime());
            bean.setPublishedTime(serverBean.getPublishedTime());
            bean.setExpirationTime(serverBean.getExpirationTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ChannelFeed> convertServerChannelFeedBeanListToAppBeanList(List<ChannelFeed> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ChannelFeed> beanList = new ArrayList<ChannelFeed>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ChannelFeed sb : serverBeanList) {
                ChannelFeedBean bean = convertServerChannelFeedBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.feedstoa.ws.bean.ChannelFeedBean convertAppChannelFeedBeanToServerBean(ChannelFeed appBean)
    {
        com.feedstoa.ws.bean.ChannelFeedBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.ChannelFeedBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setChannelSource(appBean.getChannelSource());
            bean.setChannelCode(appBean.getChannelCode());
            bean.setPreviousVersion(appBean.getPreviousVersion());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setFetchUrl(appBean.getFetchUrl());
            bean.setFeedServiceUrl(appBean.getFeedServiceUrl());
            bean.setFeedUrl(appBean.getFeedUrl());
            bean.setFeedFormat(appBean.getFeedFormat());
            bean.setMaxItemCount(appBean.getMaxItemCount());
            bean.setFeedCategory(appBean.getFeedCategory());
            bean.setTitle(appBean.getTitle());
            bean.setSubtitle(appBean.getSubtitle());
            bean.setLink(UriStructProxyUtil.convertAppUriStructBeanToServerBean(appBean.getLink()));
            bean.setDescription(appBean.getDescription());
            bean.setLanguage(appBean.getLanguage());
            bean.setCopyright(appBean.getCopyright());
            bean.setManagingEditor(UserStructProxyUtil.convertAppUserStructBeanToServerBean(appBean.getManagingEditor()));
            bean.setWebMaster(UserStructProxyUtil.convertAppUserStructBeanToServerBean(appBean.getWebMaster()));
            bean.setContributor(appBean.getContributor());
            bean.setPubDate(appBean.getPubDate());
            bean.setLastBuildDate(appBean.getLastBuildDate());
            bean.setCategory(appBean.getCategory());
            bean.setGenerator(appBean.getGenerator());
            bean.setDocs(appBean.getDocs());
            bean.setCloud(CloudStructProxyUtil.convertAppCloudStructBeanToServerBean(appBean.getCloud()));
            bean.setTtl(appBean.getTtl());
            bean.setLogo(ImageStructProxyUtil.convertAppImageStructBeanToServerBean(appBean.getLogo()));
            bean.setIcon(ImageStructProxyUtil.convertAppImageStructBeanToServerBean(appBean.getIcon()));
            bean.setRating(appBean.getRating());
            bean.setTextInput(TextInputStructProxyUtil.convertAppTextInputStructBeanToServerBean(appBean.getTextInput()));
            bean.setSkipHours(appBean.getSkipHours());
            bean.setSkipDays(appBean.getSkipDays());
            bean.setOutputText(appBean.getOutputText());
            bean.setOutputHash(appBean.getOutputHash());
            bean.setFeedContent(appBean.getFeedContent());
            bean.setStatus(appBean.getStatus());
            bean.setNote(appBean.getNote());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setLastBuildTime(appBean.getLastBuildTime());
            bean.setPublishedTime(appBean.getPublishedTime());
            bean.setExpirationTime(appBean.getExpirationTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ChannelFeed> convertAppChannelFeedBeanListToServerBeanList(List<ChannelFeed> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ChannelFeed> beanList = new ArrayList<ChannelFeed>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ChannelFeed sb : appBeanList) {
                com.feedstoa.ws.bean.ChannelFeedBean bean = convertAppChannelFeedBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
