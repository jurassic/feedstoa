package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.UserStruct;
// import com.feedstoa.ws.bean.UserStructBean;
import com.feedstoa.af.bean.UserStructBean;


public class UserStructProxyUtil
{
    private static final Logger log = Logger.getLogger(UserStructProxyUtil.class.getName());

    // Static methods only.
    private UserStructProxyUtil() {}

    public static UserStructBean convertServerUserStructBeanToAppBean(UserStruct serverBean)
    {
        UserStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new UserStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setName(serverBean.getName());
            bean.setEmail(serverBean.getEmail());
            bean.setUrl(serverBean.getUrl());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.UserStructBean convertAppUserStructBeanToServerBean(UserStruct appBean)
    {
        com.feedstoa.ws.bean.UserStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.UserStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setName(appBean.getName());
            bean.setEmail(appBean.getEmail());
            bean.setUrl(appBean.getUrl());
        }
        return bean;
    }

}
