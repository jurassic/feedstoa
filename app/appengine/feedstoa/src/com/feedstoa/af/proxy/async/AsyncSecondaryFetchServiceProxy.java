package com.feedstoa.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.ws.stub.NotificationStructListStub;
import com.feedstoa.ws.stub.GaeAppStructStub;
import com.feedstoa.ws.stub.GaeAppStructListStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructListStub;
import com.feedstoa.ws.stub.SecondaryFetchStub;
import com.feedstoa.ws.stub.SecondaryFetchListStub;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.ws.service.SecondaryFetchService;
import com.feedstoa.af.proxy.SecondaryFetchServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteSecondaryFetchServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncSecondaryFetchServiceProxy extends BaseAsyncServiceProxy implements SecondaryFetchServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncSecondaryFetchServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteSecondaryFetchServiceProxy remoteProxy;

    public AsyncSecondaryFetchServiceProxy()
    {
        remoteProxy = new RemoteSecondaryFetchServiceProxy();
    }

    @Override
    public SecondaryFetch getSecondaryFetch(String guid) throws BaseException
    {
        return remoteProxy.getSecondaryFetch(guid);
    }

    @Override
    public Object getSecondaryFetch(String guid, String field) throws BaseException
    {
        return remoteProxy.getSecondaryFetch(guid, field);       
    }

    @Override
    public List<SecondaryFetch> getSecondaryFetches(List<String> guids) throws BaseException
    {
        return remoteProxy.getSecondaryFetches(guids);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches() throws BaseException
    {
        return getAllSecondaryFetches(null, null, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllSecondaryFetches(ordering, offset, count);
        return getAllSecondaryFetches(ordering, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllSecondaryFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllSecondaryFetchKeys(ordering, offset, count);
        return getAllSecondaryFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllSecondaryFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findSecondaryFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count);
        return findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createSecondaryFetch(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException
    {
        SecondaryFetchBean bean = new SecondaryFetchBean(null, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
        return createSecondaryFetch(bean);        
    }

    @Override
    public String createSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("BEGIN");

        String guid = secondaryFetch.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((SecondaryFetchBean) secondaryFetch).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateSecondaryFetch-" + guid;
        String taskName = "RsCreateSecondaryFetch-" + guid + "-" + (new Date()).getTime();
        SecondaryFetchStub stub = MarshalHelper.convertSecondaryFetchToStub(secondaryFetch);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(SecondaryFetchStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = secondaryFetch.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    SecondaryFetchStub dummyStub = new SecondaryFetchStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createSecondaryFetch(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "secondaryFetches/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createSecondaryFetch(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "secondaryFetches/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateSecondaryFetch(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "SecondaryFetch guid is invalid.");
        	throw new BaseException("SecondaryFetch guid is invalid.");
        }
        SecondaryFetchBean bean = new SecondaryFetchBean(guid, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
        return updateSecondaryFetch(bean);        
    }

    @Override
    public Boolean updateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("BEGIN");

        String guid = secondaryFetch.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "SecondaryFetch object is invalid.");
        	throw new BaseException("SecondaryFetch object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateSecondaryFetch-" + guid;
        String taskName = "RsUpdateSecondaryFetch-" + guid + "-" + (new Date()).getTime();
        SecondaryFetchStub stub = MarshalHelper.convertSecondaryFetchToStub(secondaryFetch);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(SecondaryFetchStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = secondaryFetch.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    SecondaryFetchStub dummyStub = new SecondaryFetchStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateSecondaryFetch(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "secondaryFetches/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateSecondaryFetch(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "secondaryFetches/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteSecondaryFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteSecondaryFetch-" + guid;
        String taskName = "RsDeleteSecondaryFetch-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "secondaryFetches/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        String guid = secondaryFetch.getGuid();
        return deleteSecondaryFetch(guid);
    }

    @Override
    public Long deleteSecondaryFetches(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteSecondaryFetches(filter, params, values);
    }

}
