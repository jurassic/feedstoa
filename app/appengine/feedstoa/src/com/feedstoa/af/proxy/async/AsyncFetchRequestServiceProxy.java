package com.feedstoa.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.ws.stub.NotificationStructListStub;
import com.feedstoa.ws.stub.GaeAppStructStub;
import com.feedstoa.ws.stub.GaeAppStructListStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructListStub;
import com.feedstoa.ws.stub.FetchRequestStub;
import com.feedstoa.ws.stub.FetchRequestListStub;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.ws.service.FetchRequestService;
import com.feedstoa.af.proxy.FetchRequestServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteFetchRequestServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncFetchRequestServiceProxy extends BaseAsyncServiceProxy implements FetchRequestServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncFetchRequestServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteFetchRequestServiceProxy remoteProxy;

    public AsyncFetchRequestServiceProxy()
    {
        remoteProxy = new RemoteFetchRequestServiceProxy();
    }

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        return remoteProxy.getFetchRequest(guid);
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return remoteProxy.getFetchRequest(guid, field);       
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        return remoteProxy.getFetchRequests(guids);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFetchRequests(ordering, offset, count);
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFetchRequests(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFetchRequestKeys(ordering, offset, count);
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFetchRequest(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException
    {
        FetchRequestBean bean = new FetchRequestBean(null, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, MarshalHelper.convertNotificationStructToBean(notificationPref), MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
        return createFetchRequest(bean);        
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        String guid = fetchRequest.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((FetchRequestBean) fetchRequest).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateFetchRequest-" + guid;
        String taskName = "RsCreateFetchRequest-" + guid + "-" + (new Date()).getTime();
        FetchRequestStub stub = MarshalHelper.convertFetchRequestToStub(fetchRequest);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FetchRequestStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = fetchRequest.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FetchRequestStub dummyStub = new FetchRequestStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFetchRequest(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "fetchRequests/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFetchRequest(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "fetchRequests/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateFetchRequest(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FetchRequest guid is invalid.");
        	throw new BaseException("FetchRequest guid is invalid.");
        }
        FetchRequestBean bean = new FetchRequestBean(guid, managerApp, appAcl, MarshalHelper.convertGaeAppStructToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, MarshalHelper.convertNotificationStructToBean(notificationPref), MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
        return updateFetchRequest(bean);        
    }

    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        String guid = fetchRequest.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FetchRequest object is invalid.");
        	throw new BaseException("FetchRequest object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateFetchRequest-" + guid;
        String taskName = "RsUpdateFetchRequest-" + guid + "-" + (new Date()).getTime();
        FetchRequestStub stub = MarshalHelper.convertFetchRequestToStub(fetchRequest);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FetchRequestStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = fetchRequest.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FetchRequestStub dummyStub = new FetchRequestStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFetchRequest(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "fetchRequests/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFetchRequest(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "fetchRequests/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteFetchRequest-" + guid;
        String taskName = "RsDeleteFetchRequest-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "fetchRequests/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        String guid = fetchRequest.getGuid();
        return deleteFetchRequest(guid);
    }

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteFetchRequests(filter, params, values);
    }

}
