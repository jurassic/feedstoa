package com.feedstoa.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
// import com.feedstoa.ws.bean.FetchRequestBean;
import com.feedstoa.ws.service.FetchRequestService;
import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.proxy.FetchRequestServiceProxy;
import com.feedstoa.af.proxy.util.GaeAppStructProxyUtil;
import com.feedstoa.af.proxy.util.NotificationStructProxyUtil;
import com.feedstoa.af.proxy.util.ReferrerInfoStructProxyUtil;


public class LocalFetchRequestServiceProxy extends BaseLocalServiceProxy implements FetchRequestServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFetchRequestServiceProxy.class.getName());

    public LocalFetchRequestServiceProxy()
    {
    }

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        FetchRequest serverBean = getFetchRequestService().getFetchRequest(guid);
        FetchRequest appBean = convertServerFetchRequestBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return getFetchRequestService().getFetchRequest(guid, field);       
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        List<FetchRequest> serverBeanList = getFetchRequestService().getFetchRequests(guids);
        List<FetchRequest> appBeanList = convertServerFetchRequestBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().getAllFetchRequests(ordering, offset, count);
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FetchRequest> serverBeanList = getFetchRequestService().getAllFetchRequests(ordering, offset, count, forwardCursor);
        List<FetchRequest> appBeanList = convertServerFetchRequestBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().getAllFetchRequestKeys(ordering, offset, count);
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FetchRequest> serverBeanList = getFetchRequestService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<FetchRequest> appBeanList = convertServerFetchRequestBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFetchRequestService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFetchRequest(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException
    {
        return getFetchRequestService().createFetchRequest(managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        com.feedstoa.ws.bean.FetchRequestBean serverBean =  convertAppFetchRequestBeanToServerBean(fetchRequest);
        return getFetchRequestService().createFetchRequest(serverBean);
    }

    @Override
    public Boolean updateFetchRequest(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException
    {
        return getFetchRequestService().updateFetchRequest(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        com.feedstoa.ws.bean.FetchRequestBean serverBean =  convertAppFetchRequestBeanToServerBean(fetchRequest);
        return getFetchRequestService().updateFetchRequest(serverBean);
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequest(guid);
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        com.feedstoa.ws.bean.FetchRequestBean serverBean =  convertAppFetchRequestBeanToServerBean(fetchRequest);
        return getFetchRequestService().deleteFetchRequest(serverBean);
    }

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequests(filter, params, values);
    }




    public static FetchRequestBean convertServerFetchRequestBeanToAppBean(FetchRequest serverBean)
    {
        FetchRequestBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new FetchRequestBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setOwnerUser(serverBean.getOwnerUser());
            bean.setUserAcl(serverBean.getUserAcl());
            bean.setUser(serverBean.getUser());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFetchUrl(serverBean.getFetchUrl());
            bean.setFeedUrl(serverBean.getFeedUrl());
            bean.setChannelFeed(serverBean.getChannelFeed());
            bean.setReuseChannel(serverBean.isReuseChannel());
            bean.setMaxItemCount(serverBean.getMaxItemCount());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
            bean.setOriginFetch(serverBean.getOriginFetch());
            bean.setOutputFormat(serverBean.getOutputFormat());
            bean.setFetchStatus(serverBean.getFetchStatus());
            bean.setResult(serverBean.getResult());
            bean.setFeedCategory(serverBean.getFeedCategory());
            bean.setMultipleFeedEnabled(serverBean.isMultipleFeedEnabled());
            bean.setDeferred(serverBean.isDeferred());
            bean.setAlert(serverBean.isAlert());
            bean.setNotificationPref(NotificationStructProxyUtil.convertServerNotificationStructBeanToAppBean(serverBean.getNotificationPref()));
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setRefreshExpressions(serverBean.getRefreshExpressions());
            bean.setRefreshTimeZone(serverBean.getRefreshTimeZone());
            bean.setCurrentRefreshCount(serverBean.getCurrentRefreshCount());
            bean.setMaxRefreshCount(serverBean.getMaxRefreshCount());
            bean.setRefreshExpirationTime(serverBean.getRefreshExpirationTime());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FetchRequest> convertServerFetchRequestBeanListToAppBeanList(List<FetchRequest> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<FetchRequest> beanList = new ArrayList<FetchRequest>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(FetchRequest sb : serverBeanList) {
                FetchRequestBean bean = convertServerFetchRequestBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.feedstoa.ws.bean.FetchRequestBean convertAppFetchRequestBeanToServerBean(FetchRequest appBean)
    {
        com.feedstoa.ws.bean.FetchRequestBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.FetchRequestBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setOwnerUser(appBean.getOwnerUser());
            bean.setUserAcl(appBean.getUserAcl());
            bean.setUser(appBean.getUser());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFetchUrl(appBean.getFetchUrl());
            bean.setFeedUrl(appBean.getFeedUrl());
            bean.setChannelFeed(appBean.getChannelFeed());
            bean.setReuseChannel(appBean.isReuseChannel());
            bean.setMaxItemCount(appBean.getMaxItemCount());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
            bean.setOriginFetch(appBean.getOriginFetch());
            bean.setOutputFormat(appBean.getOutputFormat());
            bean.setFetchStatus(appBean.getFetchStatus());
            bean.setResult(appBean.getResult());
            bean.setFeedCategory(appBean.getFeedCategory());
            bean.setMultipleFeedEnabled(appBean.isMultipleFeedEnabled());
            bean.setDeferred(appBean.isDeferred());
            bean.setAlert(appBean.isAlert());
            bean.setNotificationPref(NotificationStructProxyUtil.convertAppNotificationStructBeanToServerBean(appBean.getNotificationPref()));
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setRefreshExpressions(appBean.getRefreshExpressions());
            bean.setRefreshTimeZone(appBean.getRefreshTimeZone());
            bean.setCurrentRefreshCount(appBean.getCurrentRefreshCount());
            bean.setMaxRefreshCount(appBean.getMaxRefreshCount());
            bean.setRefreshExpirationTime(appBean.getRefreshExpirationTime());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FetchRequest> convertAppFetchRequestBeanListToServerBeanList(List<FetchRequest> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<FetchRequest> beanList = new ArrayList<FetchRequest>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(FetchRequest sb : appBeanList) {
                com.feedstoa.ws.bean.FetchRequestBean bean = convertAppFetchRequestBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
