package com.feedstoa.af.proxy;

public abstract class AbstractProxyFactory
{
    public abstract ApiConsumerServiceProxy getApiConsumerServiceProxy();
    public abstract UserServiceProxy getUserServiceProxy();
    public abstract FetchRequestServiceProxy getFetchRequestServiceProxy();
    public abstract SecondaryFetchServiceProxy getSecondaryFetchServiceProxy();
    public abstract ChannelSourceServiceProxy getChannelSourceServiceProxy();
    public abstract ChannelFeedServiceProxy getChannelFeedServiceProxy();
    public abstract FeedItemServiceProxy getFeedItemServiceProxy();
    public abstract FeedContentServiceProxy getFeedContentServiceProxy();
    public abstract ServiceInfoServiceProxy getServiceInfoServiceProxy();
    public abstract FiveTenServiceProxy getFiveTenServiceProxy();
}
