package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.KeyValuePairStruct;
// import com.feedstoa.ws.bean.KeyValuePairStructBean;
import com.feedstoa.af.bean.KeyValuePairStructBean;


public class KeyValuePairStructProxyUtil
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructProxyUtil.class.getName());

    // Static methods only.
    private KeyValuePairStructProxyUtil() {}

    public static KeyValuePairStructBean convertServerKeyValuePairStructBeanToAppBean(KeyValuePairStruct serverBean)
    {
        KeyValuePairStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new KeyValuePairStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setKey(serverBean.getKey());
            bean.setValue(serverBean.getValue());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.KeyValuePairStructBean convertAppKeyValuePairStructBeanToServerBean(KeyValuePairStruct appBean)
    {
        com.feedstoa.ws.bean.KeyValuePairStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.KeyValuePairStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setKey(appBean.getKey());
            bean.setValue(appBean.getValue());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
