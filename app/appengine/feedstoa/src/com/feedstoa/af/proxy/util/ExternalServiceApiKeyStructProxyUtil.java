package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.ExternalServiceApiKeyStruct;
// import com.feedstoa.ws.bean.ExternalServiceApiKeyStructBean;
import com.feedstoa.af.bean.ExternalServiceApiKeyStructBean;


public class ExternalServiceApiKeyStructProxyUtil
{
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructProxyUtil.class.getName());

    // Static methods only.
    private ExternalServiceApiKeyStructProxyUtil() {}

    public static ExternalServiceApiKeyStructBean convertServerExternalServiceApiKeyStructBeanToAppBean(ExternalServiceApiKeyStruct serverBean)
    {
        ExternalServiceApiKeyStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ExternalServiceApiKeyStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setService(serverBean.getService());
            bean.setKey(serverBean.getKey());
            bean.setSecret(serverBean.getSecret());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.ExternalServiceApiKeyStructBean convertAppExternalServiceApiKeyStructBeanToServerBean(ExternalServiceApiKeyStruct appBean)
    {
        com.feedstoa.ws.bean.ExternalServiceApiKeyStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.ExternalServiceApiKeyStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setService(appBean.getService());
            bean.setKey(appBean.getKey());
            bean.setSecret(appBean.getSecret());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
