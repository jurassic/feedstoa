package com.feedstoa.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.service.ApiConsumerService;
import com.feedstoa.ws.service.UserService;
import com.feedstoa.ws.service.FetchRequestService;
import com.feedstoa.ws.service.SecondaryFetchService;
import com.feedstoa.ws.service.ChannelSourceService;
import com.feedstoa.ws.service.ChannelFeedService;
import com.feedstoa.ws.service.FeedItemService;
import com.feedstoa.ws.service.FeedContentService;
import com.feedstoa.ws.service.ServiceInfoService;
import com.feedstoa.ws.service.FiveTenService;


// TBD: How to best inject the service instances?
// (The current implementation does not seem to make sense...)
public abstract class BaseLocalServiceProxy
{
    private static final Logger log = Logger.getLogger(BaseLocalServiceProxy.class.getName());

    private ApiConsumerService apiConsumerService;
    private UserService userService;
    private FetchRequestService fetchRequestService;
    private SecondaryFetchService secondaryFetchService;
    private ChannelSourceService channelSourceService;
    private ChannelFeedService channelFeedService;
    private FeedItemService feedItemService;
    private FeedContentService feedContentService;
    private ServiceInfoService serviceInfoService;
    private FiveTenService fiveTenService;

    public BaseLocalServiceProxy()
    {
        this(null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService)
    {
        this(apiConsumerService, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserService userService)
    {
        this(null, userService, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(FetchRequestService fetchRequestService)
    {
        this(null, null, fetchRequestService, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(SecondaryFetchService secondaryFetchService)
    {
        this(null, null, null, secondaryFetchService, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ChannelSourceService channelSourceService)
    {
        this(null, null, null, null, channelSourceService, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ChannelFeedService channelFeedService)
    {
        this(null, null, null, null, null, channelFeedService, null, null, null, null);
    }
    public BaseLocalServiceProxy(FeedItemService feedItemService)
    {
        this(null, null, null, null, null, null, feedItemService, null, null, null);
    }
    public BaseLocalServiceProxy(FeedContentService feedContentService)
    {
        this(null, null, null, null, null, null, null, feedContentService, null, null);
    }
    public BaseLocalServiceProxy(ServiceInfoService serviceInfoService)
    {
        this(null, null, null, null, null, null, null, null, serviceInfoService, null);
    }
    public BaseLocalServiceProxy(FiveTenService fiveTenService)
    {
        this(null, null, null, null, null, null, null, null, null, fiveTenService);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService, UserService userService, FetchRequestService fetchRequestService, SecondaryFetchService secondaryFetchService, ChannelSourceService channelSourceService, ChannelFeedService channelFeedService, FeedItemService feedItemService, FeedContentService feedContentService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.fetchRequestService = fetchRequestService;
        this.secondaryFetchService = secondaryFetchService;
        this.channelSourceService = channelSourceService;
        this.channelFeedService = channelFeedService;
        this.feedItemService = feedItemService;
        this.feedContentService = feedContentService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }
    
    // Inject dependencies.
    public void setApiConsumerService(ApiConsumerService apiConsumerService)
    {
        this.apiConsumerService = apiConsumerService;
    }
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    public void setFetchRequestService(FetchRequestService fetchRequestService)
    {
        this.fetchRequestService = fetchRequestService;
    }
    public void setSecondaryFetchService(SecondaryFetchService secondaryFetchService)
    {
        this.secondaryFetchService = secondaryFetchService;
    }
    public void setChannelSourceService(ChannelSourceService channelSourceService)
    {
        this.channelSourceService = channelSourceService;
    }
    public void setChannelFeedService(ChannelFeedService channelFeedService)
    {
        this.channelFeedService = channelFeedService;
    }
    public void setFeedItemService(FeedItemService feedItemService)
    {
        this.feedItemService = feedItemService;
    }
    public void setFeedContentService(FeedContentService feedContentService)
    {
        this.feedContentService = feedContentService;
    }
    public void setServiceInfoService(ServiceInfoService serviceInfoService)
    {
        this.serviceInfoService = serviceInfoService;
    }
    public void setFiveTenService(FiveTenService fiveTenService)
    {
        this.fiveTenService = fiveTenService;
    }
   
    // Returns a ApiConsumerService instance.
    public ApiConsumerService getApiConsumerService() 
    {
        return apiConsumerService;
    }

    // Returns a UserService instance.
    public UserService getUserService() 
    {
        return userService;
    }

    // Returns a FetchRequestService instance.
    public FetchRequestService getFetchRequestService() 
    {
        return fetchRequestService;
    }

    // Returns a SecondaryFetchService instance.
    public SecondaryFetchService getSecondaryFetchService() 
    {
        return secondaryFetchService;
    }

    // Returns a ChannelSourceService instance.
    public ChannelSourceService getChannelSourceService() 
    {
        return channelSourceService;
    }

    // Returns a ChannelFeedService instance.
    public ChannelFeedService getChannelFeedService() 
    {
        return channelFeedService;
    }

    // Returns a FeedItemService instance.
    public FeedItemService getFeedItemService() 
    {
        return feedItemService;
    }

    // Returns a FeedContentService instance.
    public FeedContentService getFeedContentService() 
    {
        return feedContentService;
    }

    // Returns a ServiceInfoService instance.
    public ServiceInfoService getServiceInfoService() 
    {
        return serviceInfoService;
    }

    // Returns a FiveTenService instance.
    public FiveTenService getFiveTenService() 
    {
        return fiveTenService;
    }

}
