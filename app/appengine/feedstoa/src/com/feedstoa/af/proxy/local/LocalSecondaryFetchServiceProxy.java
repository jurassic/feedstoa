package com.feedstoa.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
// import com.feedstoa.ws.bean.SecondaryFetchBean;
import com.feedstoa.ws.service.SecondaryFetchService;
import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.af.proxy.SecondaryFetchServiceProxy;
import com.feedstoa.af.proxy.util.GaeAppStructProxyUtil;


public class LocalSecondaryFetchServiceProxy extends BaseLocalServiceProxy implements SecondaryFetchServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalSecondaryFetchServiceProxy.class.getName());

    public LocalSecondaryFetchServiceProxy()
    {
    }

    @Override
    public SecondaryFetch getSecondaryFetch(String guid) throws BaseException
    {
        SecondaryFetch serverBean = getSecondaryFetchService().getSecondaryFetch(guid);
        SecondaryFetch appBean = convertServerSecondaryFetchBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getSecondaryFetch(String guid, String field) throws BaseException
    {
        return getSecondaryFetchService().getSecondaryFetch(guid, field);       
    }

    @Override
    public List<SecondaryFetch> getSecondaryFetches(List<String> guids) throws BaseException
    {
        List<SecondaryFetch> serverBeanList = getSecondaryFetchService().getSecondaryFetches(guids);
        List<SecondaryFetch> appBeanList = convertServerSecondaryFetchBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches() throws BaseException
    {
        return getAllSecondaryFetches(null, null, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getSecondaryFetchService().getAllSecondaryFetches(ordering, offset, count);
        return getAllSecondaryFetches(ordering, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<SecondaryFetch> serverBeanList = getSecondaryFetchService().getAllSecondaryFetches(ordering, offset, count, forwardCursor);
        List<SecondaryFetch> appBeanList = convertServerSecondaryFetchBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getSecondaryFetchService().getAllSecondaryFetchKeys(ordering, offset, count);
        return getAllSecondaryFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getSecondaryFetchService().getAllSecondaryFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findSecondaryFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getSecondaryFetchService().findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count);
        return findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<SecondaryFetch> serverBeanList = getSecondaryFetchService().findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<SecondaryFetch> appBeanList = convertServerSecondaryFetchBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getSecondaryFetchService().findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getSecondaryFetchService().findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getSecondaryFetchService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createSecondaryFetch(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException
    {
        return getSecondaryFetchService().createSecondaryFetch(managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
    }

    @Override
    public String createSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        com.feedstoa.ws.bean.SecondaryFetchBean serverBean =  convertAppSecondaryFetchBeanToServerBean(secondaryFetch);
        return getSecondaryFetchService().createSecondaryFetch(serverBean);
    }

    @Override
    public Boolean updateSecondaryFetch(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException
    {
        return getSecondaryFetchService().updateSecondaryFetch(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
    }

    @Override
    public Boolean updateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        com.feedstoa.ws.bean.SecondaryFetchBean serverBean =  convertAppSecondaryFetchBeanToServerBean(secondaryFetch);
        return getSecondaryFetchService().updateSecondaryFetch(serverBean);
    }

    @Override
    public Boolean deleteSecondaryFetch(String guid) throws BaseException
    {
        return getSecondaryFetchService().deleteSecondaryFetch(guid);
    }

    @Override
    public Boolean deleteSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        com.feedstoa.ws.bean.SecondaryFetchBean serverBean =  convertAppSecondaryFetchBeanToServerBean(secondaryFetch);
        return getSecondaryFetchService().deleteSecondaryFetch(serverBean);
    }

    @Override
    public Long deleteSecondaryFetches(String filter, String params, List<String> values) throws BaseException
    {
        return getSecondaryFetchService().deleteSecondaryFetches(filter, params, values);
    }




    public static SecondaryFetchBean convertServerSecondaryFetchBeanToAppBean(SecondaryFetch serverBean)
    {
        SecondaryFetchBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new SecondaryFetchBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setOwnerUser(serverBean.getOwnerUser());
            bean.setUserAcl(serverBean.getUserAcl());
            bean.setUser(serverBean.getUser());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFetchUrl(serverBean.getFetchUrl());
            bean.setFeedUrl(serverBean.getFeedUrl());
            bean.setChannelFeed(serverBean.getChannelFeed());
            bean.setReuseChannel(serverBean.isReuseChannel());
            bean.setMaxItemCount(serverBean.getMaxItemCount());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<SecondaryFetch> convertServerSecondaryFetchBeanListToAppBeanList(List<SecondaryFetch> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<SecondaryFetch> beanList = new ArrayList<SecondaryFetch>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(SecondaryFetch sb : serverBeanList) {
                SecondaryFetchBean bean = convertServerSecondaryFetchBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.feedstoa.ws.bean.SecondaryFetchBean convertAppSecondaryFetchBeanToServerBean(SecondaryFetch appBean)
    {
        com.feedstoa.ws.bean.SecondaryFetchBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.SecondaryFetchBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setOwnerUser(appBean.getOwnerUser());
            bean.setUserAcl(appBean.getUserAcl());
            bean.setUser(appBean.getUser());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFetchUrl(appBean.getFetchUrl());
            bean.setFeedUrl(appBean.getFeedUrl());
            bean.setChannelFeed(appBean.getChannelFeed());
            bean.setReuseChannel(appBean.isReuseChannel());
            bean.setMaxItemCount(appBean.getMaxItemCount());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<SecondaryFetch> convertAppSecondaryFetchBeanListToServerBeanList(List<SecondaryFetch> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<SecondaryFetch> beanList = new ArrayList<SecondaryFetch>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(SecondaryFetch sb : appBeanList) {
                com.feedstoa.ws.bean.SecondaryFetchBean bean = convertAppSecondaryFetchBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
