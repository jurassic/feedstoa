package com.feedstoa.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.ChannelSource;
// import com.feedstoa.ws.bean.ChannelSourceBean;
import com.feedstoa.ws.service.ChannelSourceService;
import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.af.proxy.ChannelSourceServiceProxy;


public class LocalChannelSourceServiceProxy extends BaseLocalServiceProxy implements ChannelSourceServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalChannelSourceServiceProxy.class.getName());

    public LocalChannelSourceServiceProxy()
    {
    }

    @Override
    public ChannelSource getChannelSource(String guid) throws BaseException
    {
        ChannelSource serverBean = getChannelSourceService().getChannelSource(guid);
        ChannelSource appBean = convertServerChannelSourceBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getChannelSource(String guid, String field) throws BaseException
    {
        return getChannelSourceService().getChannelSource(guid, field);       
    }

    @Override
    public List<ChannelSource> getChannelSources(List<String> guids) throws BaseException
    {
        List<ChannelSource> serverBeanList = getChannelSourceService().getChannelSources(guids);
        List<ChannelSource> appBeanList = convertServerChannelSourceBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ChannelSource> getAllChannelSources() throws BaseException
    {
        return getAllChannelSources(null, null, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getChannelSourceService().getAllChannelSources(ordering, offset, count);
        return getAllChannelSources(ordering, offset, count, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ChannelSource> serverBeanList = getChannelSourceService().getAllChannelSources(ordering, offset, count, forwardCursor);
        List<ChannelSource> appBeanList = convertServerChannelSourceBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getChannelSourceService().getAllChannelSourceKeys(ordering, offset, count);
        return getAllChannelSourceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getChannelSourceService().getAllChannelSourceKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findChannelSources(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getChannelSourceService().findChannelSources(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ChannelSource> serverBeanList = getChannelSourceService().findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ChannelSource> appBeanList = convertServerChannelSourceBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getChannelSourceService().findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getChannelSourceService().findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getChannelSourceService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createChannelSource(String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        return getChannelSourceService().createChannelSource(user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
    }

    @Override
    public String createChannelSource(ChannelSource channelSource) throws BaseException
    {
        com.feedstoa.ws.bean.ChannelSourceBean serverBean =  convertAppChannelSourceBeanToServerBean(channelSource);
        return getChannelSourceService().createChannelSource(serverBean);
    }

    @Override
    public Boolean updateChannelSource(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        return getChannelSourceService().updateChannelSource(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
    }

    @Override
    public Boolean updateChannelSource(ChannelSource channelSource) throws BaseException
    {
        com.feedstoa.ws.bean.ChannelSourceBean serverBean =  convertAppChannelSourceBeanToServerBean(channelSource);
        return getChannelSourceService().updateChannelSource(serverBean);
    }

    @Override
    public Boolean deleteChannelSource(String guid) throws BaseException
    {
        return getChannelSourceService().deleteChannelSource(guid);
    }

    @Override
    public Boolean deleteChannelSource(ChannelSource channelSource) throws BaseException
    {
        com.feedstoa.ws.bean.ChannelSourceBean serverBean =  convertAppChannelSourceBeanToServerBean(channelSource);
        return getChannelSourceService().deleteChannelSource(serverBean);
    }

    @Override
    public Long deleteChannelSources(String filter, String params, List<String> values) throws BaseException
    {
        return getChannelSourceService().deleteChannelSources(filter, params, values);
    }




    public static ChannelSourceBean convertServerChannelSourceBeanToAppBean(ChannelSource serverBean)
    {
        ChannelSourceBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ChannelSourceBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setServiceName(serverBean.getServiceName());
            bean.setServiceUrl(serverBean.getServiceUrl());
            bean.setFeedUrl(serverBean.getFeedUrl());
            bean.setFeedFormat(serverBean.getFeedFormat());
            bean.setChannelTitle(serverBean.getChannelTitle());
            bean.setChannelDescription(serverBean.getChannelDescription());
            bean.setChannelCategory(serverBean.getChannelCategory());
            bean.setChannelUrl(serverBean.getChannelUrl());
            bean.setChannelLogo(serverBean.getChannelLogo());
            bean.setStatus(serverBean.getStatus());
            bean.setNote(serverBean.getNote());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ChannelSource> convertServerChannelSourceBeanListToAppBeanList(List<ChannelSource> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ChannelSource> beanList = new ArrayList<ChannelSource>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ChannelSource sb : serverBeanList) {
                ChannelSourceBean bean = convertServerChannelSourceBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.feedstoa.ws.bean.ChannelSourceBean convertAppChannelSourceBeanToServerBean(ChannelSource appBean)
    {
        com.feedstoa.ws.bean.ChannelSourceBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.ChannelSourceBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setServiceName(appBean.getServiceName());
            bean.setServiceUrl(appBean.getServiceUrl());
            bean.setFeedUrl(appBean.getFeedUrl());
            bean.setFeedFormat(appBean.getFeedFormat());
            bean.setChannelTitle(appBean.getChannelTitle());
            bean.setChannelDescription(appBean.getChannelDescription());
            bean.setChannelCategory(appBean.getChannelCategory());
            bean.setChannelUrl(appBean.getChannelUrl());
            bean.setChannelLogo(appBean.getChannelLogo());
            bean.setStatus(appBean.getStatus());
            bean.setNote(appBean.getNote());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ChannelSource> convertAppChannelSourceBeanListToServerBeanList(List<ChannelSource> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ChannelSource> beanList = new ArrayList<ChannelSource>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ChannelSource sb : appBeanList) {
                com.feedstoa.ws.bean.ChannelSourceBean bean = convertAppChannelSourceBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
