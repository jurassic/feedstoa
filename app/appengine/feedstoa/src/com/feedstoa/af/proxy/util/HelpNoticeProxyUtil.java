package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.HelpNotice;
// import com.feedstoa.ws.bean.HelpNoticeBean;
import com.feedstoa.af.bean.HelpNoticeBean;


public class HelpNoticeProxyUtil
{
    private static final Logger log = Logger.getLogger(HelpNoticeProxyUtil.class.getName());

    // Static methods only.
    private HelpNoticeProxyUtil() {}

    public static HelpNoticeBean convertServerHelpNoticeBeanToAppBean(HelpNotice serverBean)
    {
        HelpNoticeBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new HelpNoticeBean();
            bean.setUuid(serverBean.getUuid());
            bean.setTitle(serverBean.getTitle());
            bean.setContent(serverBean.getContent());
            bean.setFormat(serverBean.getFormat());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.HelpNoticeBean convertAppHelpNoticeBeanToServerBean(HelpNotice appBean)
    {
        com.feedstoa.ws.bean.HelpNoticeBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.HelpNoticeBean();
            bean.setUuid(appBean.getUuid());
            bean.setTitle(appBean.getTitle());
            bean.setContent(appBean.getContent());
            bean.setFormat(appBean.getFormat());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
        }
        return bean;
    }

}
