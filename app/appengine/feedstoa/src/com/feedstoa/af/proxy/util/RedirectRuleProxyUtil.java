package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.RedirectRule;
// import com.feedstoa.ws.bean.RedirectRuleBean;
import com.feedstoa.af.bean.RedirectRuleBean;


public class RedirectRuleProxyUtil
{
    private static final Logger log = Logger.getLogger(RedirectRuleProxyUtil.class.getName());

    // Static methods only.
    private RedirectRuleProxyUtil() {}

    public static RedirectRuleBean convertServerRedirectRuleBeanToAppBean(RedirectRule serverBean)
    {
        RedirectRuleBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new RedirectRuleBean();
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setPrecedence(serverBean.getPrecedence());
            bean.setSourceDomain(serverBean.getSourceDomain());
            bean.setSourcePath(serverBean.getSourcePath());
            bean.setTargetDomain(serverBean.getTargetDomain());
            bean.setTargetPath(serverBean.getTargetPath());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.RedirectRuleBean convertAppRedirectRuleBeanToServerBean(RedirectRule appBean)
    {
        com.feedstoa.ws.bean.RedirectRuleBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.RedirectRuleBean();
            bean.setRedirectType(appBean.getRedirectType());
            bean.setPrecedence(appBean.getPrecedence());
            bean.setSourceDomain(appBean.getSourceDomain());
            bean.setSourcePath(appBean.getSourcePath());
            bean.setTargetDomain(appBean.getTargetDomain());
            bean.setTargetPath(appBean.getTargetPath());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
        }
        return bean;
    }

}
