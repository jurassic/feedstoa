package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.FullNameStruct;
// import com.feedstoa.ws.bean.FullNameStructBean;
import com.feedstoa.af.bean.FullNameStructBean;


public class FullNameStructProxyUtil
{
    private static final Logger log = Logger.getLogger(FullNameStructProxyUtil.class.getName());

    // Static methods only.
    private FullNameStructProxyUtil() {}

    public static FullNameStructBean convertServerFullNameStructBeanToAppBean(FullNameStruct serverBean)
    {
        FullNameStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new FullNameStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setDisplayName(serverBean.getDisplayName());
            bean.setLastName(serverBean.getLastName());
            bean.setFirstName(serverBean.getFirstName());
            bean.setMiddleName1(serverBean.getMiddleName1());
            bean.setMiddleName2(serverBean.getMiddleName2());
            bean.setMiddleInitial(serverBean.getMiddleInitial());
            bean.setSalutation(serverBean.getSalutation());
            bean.setSuffix(serverBean.getSuffix());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.FullNameStructBean convertAppFullNameStructBeanToServerBean(FullNameStruct appBean)
    {
        com.feedstoa.ws.bean.FullNameStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.FullNameStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setDisplayName(appBean.getDisplayName());
            bean.setLastName(appBean.getLastName());
            bean.setFirstName(appBean.getFirstName());
            bean.setMiddleName1(appBean.getMiddleName1());
            bean.setMiddleName2(appBean.getMiddleName2());
            bean.setMiddleInitial(appBean.getMiddleInitial());
            bean.setSalutation(appBean.getSalutation());
            bean.setSuffix(appBean.getSuffix());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
