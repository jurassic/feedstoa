package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.SiteLog;
// import com.feedstoa.ws.bean.SiteLogBean;
import com.feedstoa.af.bean.SiteLogBean;


public class SiteLogProxyUtil
{
    private static final Logger log = Logger.getLogger(SiteLogProxyUtil.class.getName());

    // Static methods only.
    private SiteLogProxyUtil() {}

    public static SiteLogBean convertServerSiteLogBeanToAppBean(SiteLog serverBean)
    {
        SiteLogBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new SiteLogBean();
            bean.setUuid(serverBean.getUuid());
            bean.setTitle(serverBean.getTitle());
            bean.setPubDate(serverBean.getPubDate());
            bean.setTag(serverBean.getTag());
            bean.setContent(serverBean.getContent());
            bean.setFormat(serverBean.getFormat());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.SiteLogBean convertAppSiteLogBeanToServerBean(SiteLog appBean)
    {
        com.feedstoa.ws.bean.SiteLogBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.SiteLogBean();
            bean.setUuid(appBean.getUuid());
            bean.setTitle(appBean.getTitle());
            bean.setPubDate(appBean.getPubDate());
            bean.setTag(appBean.getTag());
            bean.setContent(appBean.getContent());
            bean.setFormat(appBean.getFormat());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
        }
        return bean;
    }

}
