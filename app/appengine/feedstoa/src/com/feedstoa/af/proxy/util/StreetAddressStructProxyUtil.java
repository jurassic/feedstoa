package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.StreetAddressStruct;
// import com.feedstoa.ws.bean.StreetAddressStructBean;
import com.feedstoa.af.bean.StreetAddressStructBean;


public class StreetAddressStructProxyUtil
{
    private static final Logger log = Logger.getLogger(StreetAddressStructProxyUtil.class.getName());

    // Static methods only.
    private StreetAddressStructProxyUtil() {}

    public static StreetAddressStructBean convertServerStreetAddressStructBeanToAppBean(StreetAddressStruct serverBean)
    {
        StreetAddressStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new StreetAddressStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setStreet1(serverBean.getStreet1());
            bean.setStreet2(serverBean.getStreet2());
            bean.setCity(serverBean.getCity());
            bean.setCounty(serverBean.getCounty());
            bean.setPostalCode(serverBean.getPostalCode());
            bean.setState(serverBean.getState());
            bean.setProvince(serverBean.getProvince());
            bean.setCountry(serverBean.getCountry());
            bean.setCountryName(serverBean.getCountryName());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.StreetAddressStructBean convertAppStreetAddressStructBeanToServerBean(StreetAddressStruct appBean)
    {
        com.feedstoa.ws.bean.StreetAddressStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.StreetAddressStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setStreet1(appBean.getStreet1());
            bean.setStreet2(appBean.getStreet2());
            bean.setCity(appBean.getCity());
            bean.setCounty(appBean.getCounty());
            bean.setPostalCode(appBean.getPostalCode());
            bean.setState(appBean.getState());
            bean.setProvince(appBean.getProvince());
            bean.setCountry(appBean.getCountry());
            bean.setCountryName(appBean.getCountryName());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
