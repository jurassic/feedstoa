package com.feedstoa.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
// import com.feedstoa.ws.bean.FeedItemBean;
import com.feedstoa.ws.service.FeedItemService;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.proxy.FeedItemServiceProxy;
import com.feedstoa.af.proxy.util.UriStructProxyUtil;
import com.feedstoa.af.proxy.util.UserStructProxyUtil;
import com.feedstoa.af.proxy.util.EnclosureStructProxyUtil;
import com.feedstoa.af.proxy.util.UriStructProxyUtil;
import com.feedstoa.af.proxy.util.ReferrerInfoStructProxyUtil;


public class LocalFeedItemServiceProxy extends BaseLocalServiceProxy implements FeedItemServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFeedItemServiceProxy.class.getName());

    public LocalFeedItemServiceProxy()
    {
    }

    @Override
    public FeedItem getFeedItem(String guid) throws BaseException
    {
        FeedItem serverBean = getFeedItemService().getFeedItem(guid);
        FeedItem appBean = convertServerFeedItemBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getFeedItem(String guid, String field) throws BaseException
    {
        return getFeedItemService().getFeedItem(guid, field);       
    }

    @Override
    public List<FeedItem> getFeedItems(List<String> guids) throws BaseException
    {
        List<FeedItem> serverBeanList = getFeedItemService().getFeedItems(guids);
        List<FeedItem> appBeanList = convertServerFeedItemBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<FeedItem> getAllFeedItems() throws BaseException
    {
        return getAllFeedItems(null, null, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFeedItemService().getAllFeedItems(ordering, offset, count);
        return getAllFeedItems(ordering, offset, count, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FeedItem> serverBeanList = getFeedItemService().getAllFeedItems(ordering, offset, count, forwardCursor);
        List<FeedItem> appBeanList = convertServerFeedItemBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFeedItemService().getAllFeedItemKeys(ordering, offset, count);
        return getAllFeedItemKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFeedItemService().getAllFeedItemKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFeedItemService().findFeedItems(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FeedItem> serverBeanList = getFeedItemService().findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<FeedItem> appBeanList = convertServerFeedItemBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFeedItemService().findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFeedItemService().findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFeedItemService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFeedItem(String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        return getFeedItemService().createFeedItem(user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime);
    }

    @Override
    public String createFeedItem(FeedItem feedItem) throws BaseException
    {
        com.feedstoa.ws.bean.FeedItemBean serverBean =  convertAppFeedItemBeanToServerBean(feedItem);
        return getFeedItemService().createFeedItem(serverBean);
    }

    @Override
    public Boolean updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        return getFeedItemService().updateFeedItem(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateFeedItem(FeedItem feedItem) throws BaseException
    {
        com.feedstoa.ws.bean.FeedItemBean serverBean =  convertAppFeedItemBeanToServerBean(feedItem);
        return getFeedItemService().updateFeedItem(serverBean);
    }

    @Override
    public Boolean deleteFeedItem(String guid) throws BaseException
    {
        return getFeedItemService().deleteFeedItem(guid);
    }

    @Override
    public Boolean deleteFeedItem(FeedItem feedItem) throws BaseException
    {
        com.feedstoa.ws.bean.FeedItemBean serverBean =  convertAppFeedItemBeanToServerBean(feedItem);
        return getFeedItemService().deleteFeedItem(serverBean);
    }

    @Override
    public Long deleteFeedItems(String filter, String params, List<String> values) throws BaseException
    {
        return getFeedItemService().deleteFeedItems(filter, params, values);
    }




    public static FeedItemBean convertServerFeedItemBeanToAppBean(FeedItem serverBean)
    {
        FeedItemBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new FeedItemBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setFetchUrl(serverBean.getFetchUrl());
            bean.setFeedUrl(serverBean.getFeedUrl());
            bean.setChannelSource(serverBean.getChannelSource());
            bean.setChannelFeed(serverBean.getChannelFeed());
            bean.setFeedFormat(serverBean.getFeedFormat());
            bean.setTitle(serverBean.getTitle());
            bean.setLink(UriStructProxyUtil.convertServerUriStructBeanToAppBean(serverBean.getLink()));
            bean.setSummary(serverBean.getSummary());
            bean.setContent(serverBean.getContent());
            bean.setAuthor(UserStructProxyUtil.convertServerUserStructBeanToAppBean(serverBean.getAuthor()));
            bean.setContributor(serverBean.getContributor());
            bean.setCategory(serverBean.getCategory());
            bean.setComments(serverBean.getComments());
            bean.setEnclosure(EnclosureStructProxyUtil.convertServerEnclosureStructBeanToAppBean(serverBean.getEnclosure()));
            bean.setId(serverBean.getId());
            bean.setPubDate(serverBean.getPubDate());
            bean.setSource(UriStructProxyUtil.convertServerUriStructBeanToAppBean(serverBean.getSource()));
            bean.setFeedContent(serverBean.getFeedContent());
            bean.setStatus(serverBean.getStatus());
            bean.setNote(serverBean.getNote());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setPublishedTime(serverBean.getPublishedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FeedItem> convertServerFeedItemBeanListToAppBeanList(List<FeedItem> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<FeedItem> beanList = new ArrayList<FeedItem>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(FeedItem sb : serverBeanList) {
                FeedItemBean bean = convertServerFeedItemBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.feedstoa.ws.bean.FeedItemBean convertAppFeedItemBeanToServerBean(FeedItem appBean)
    {
        com.feedstoa.ws.bean.FeedItemBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.FeedItemBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setFetchUrl(appBean.getFetchUrl());
            bean.setFeedUrl(appBean.getFeedUrl());
            bean.setChannelSource(appBean.getChannelSource());
            bean.setChannelFeed(appBean.getChannelFeed());
            bean.setFeedFormat(appBean.getFeedFormat());
            bean.setTitle(appBean.getTitle());
            bean.setLink(UriStructProxyUtil.convertAppUriStructBeanToServerBean(appBean.getLink()));
            bean.setSummary(appBean.getSummary());
            bean.setContent(appBean.getContent());
            bean.setAuthor(UserStructProxyUtil.convertAppUserStructBeanToServerBean(appBean.getAuthor()));
            bean.setContributor(appBean.getContributor());
            bean.setCategory(appBean.getCategory());
            bean.setComments(appBean.getComments());
            bean.setEnclosure(EnclosureStructProxyUtil.convertAppEnclosureStructBeanToServerBean(appBean.getEnclosure()));
            bean.setId(appBean.getId());
            bean.setPubDate(appBean.getPubDate());
            bean.setSource(UriStructProxyUtil.convertAppUriStructBeanToServerBean(appBean.getSource()));
            bean.setFeedContent(appBean.getFeedContent());
            bean.setStatus(appBean.getStatus());
            bean.setNote(appBean.getNote());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setPublishedTime(appBean.getPublishedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FeedItem> convertAppFeedItemBeanListToServerBeanList(List<FeedItem> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<FeedItem> beanList = new ArrayList<FeedItem>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(FeedItem sb : appBeanList) {
                com.feedstoa.ws.bean.FeedItemBean bean = convertAppFeedItemBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
