package com.feedstoa.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.stub.ErrorStub;
import com.feedstoa.ws.stub.FeedContentStub;
import com.feedstoa.ws.stub.FeedContentListStub;
import com.feedstoa.af.util.MarshalHelper;
import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.ws.service.FeedContentService;
import com.feedstoa.af.proxy.FeedContentServiceProxy;
import com.feedstoa.af.proxy.remote.RemoteFeedContentServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncFeedContentServiceProxy extends BaseAsyncServiceProxy implements FeedContentServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncFeedContentServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteFeedContentServiceProxy remoteProxy;

    public AsyncFeedContentServiceProxy()
    {
        remoteProxy = new RemoteFeedContentServiceProxy();
    }

    @Override
    public FeedContent getFeedContent(String guid) throws BaseException
    {
        return remoteProxy.getFeedContent(guid);
    }

    @Override
    public Object getFeedContent(String guid, String field) throws BaseException
    {
        return remoteProxy.getFeedContent(guid, field);       
    }

    @Override
    public List<FeedContent> getFeedContents(List<String> guids) throws BaseException
    {
        return remoteProxy.getFeedContents(guids);
    }

    @Override
    public List<FeedContent> getAllFeedContents() throws BaseException
    {
        return getAllFeedContents(null, null, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFeedContents(ordering, offset, count);
        return getAllFeedContents(ordering, offset, count, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFeedContents(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllFeedContentKeys(ordering, offset, count);
        return getAllFeedContentKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllFeedContentKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFeedContents(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFeedContents(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFeedContent(String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        FeedContentBean bean = new FeedContentBean(null, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        return createFeedContent(bean);        
    }

    @Override
    public String createFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        String guid = feedContent.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((FeedContentBean) feedContent).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateFeedContent-" + guid;
        String taskName = "RsCreateFeedContent-" + guid + "-" + (new Date()).getTime();
        FeedContentStub stub = MarshalHelper.convertFeedContentToStub(feedContent);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FeedContentStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = feedContent.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FeedContentStub dummyStub = new FeedContentStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFeedContent(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedContents/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createFeedContent(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedContents/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateFeedContent(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FeedContent guid is invalid.");
        	throw new BaseException("FeedContent guid is invalid.");
        }
        FeedContentBean bean = new FeedContentBean(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        return updateFeedContent(bean);        
    }

    @Override
    public Boolean updateFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        String guid = feedContent.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "FeedContent object is invalid.");
        	throw new BaseException("FeedContent object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateFeedContent-" + guid;
        String taskName = "RsUpdateFeedContent-" + guid + "-" + (new Date()).getTime();
        FeedContentStub stub = MarshalHelper.convertFeedContentToStub(feedContent);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(FeedContentStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = feedContent.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    FeedContentStub dummyStub = new FeedContentStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFeedContent(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedContents/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateFeedContent(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedContents/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFeedContent(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteFeedContent-" + guid;
        String taskName = "RsDeleteFeedContent-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "feedContents/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteFeedContent(FeedContent feedContent) throws BaseException
    {
        String guid = feedContent.getGuid();
        return deleteFeedContent(guid);
    }

    @Override
    public Long deleteFeedContents(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteFeedContents(filter, params, values);
    }

}
