package com.feedstoa.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.ImageStruct;
// import com.feedstoa.ws.bean.ImageStructBean;
import com.feedstoa.af.bean.ImageStructBean;


public class ImageStructProxyUtil
{
    private static final Logger log = Logger.getLogger(ImageStructProxyUtil.class.getName());

    // Static methods only.
    private ImageStructProxyUtil() {}

    public static ImageStructBean convertServerImageStructBeanToAppBean(ImageStruct serverBean)
    {
        ImageStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ImageStructBean();
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setLink(serverBean.getLink());
            bean.setWidth(serverBean.getWidth());
            bean.setHeight(serverBean.getHeight());
            bean.setDescription(serverBean.getDescription());
        }
        return bean;
    }

    public static com.feedstoa.ws.bean.ImageStructBean convertAppImageStructBeanToServerBean(ImageStruct appBean)
    {
        com.feedstoa.ws.bean.ImageStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.feedstoa.ws.bean.ImageStructBean();
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setLink(appBean.getLink());
            bean.setWidth(appBean.getWidth());
            bean.setHeight(appBean.getHeight());
            bean.setDescription(appBean.getDescription());
        }
        return bean;
    }

}
