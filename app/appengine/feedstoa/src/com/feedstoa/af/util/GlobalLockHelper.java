package com.feedstoa.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;

import com.feedstoa.ws.ApiConsumer;
import com.feedstoa.ws.User;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.ServiceInfo;
import com.feedstoa.ws.FiveTen;


// Temporary
public final class GlobalLockHelper
{
    private static final Logger log = Logger.getLogger(GlobalLockHelper.class.getName());


    // Singleton
    private GlobalLockHelper() {}

    private static final class GlobalLockHelperHolder
    {
        private static final GlobalLockHelper INSTANCE = new GlobalLockHelper();
    }
    public static GlobalLockHelper getInstance()
    {
        return GlobalLockHelperHolder.INSTANCE;
    }


    // temporary
    // Poorman's "global locking" using a distributed cache...
    private static Cache getCache()
    {
        // 30 seconds
        return CacheHelper.getFlashInstance().getCache();
    }


    private static String getApiConsumerLockKey(ApiConsumer apiConsumer)
    {
        // Note: We assume that the arg apiConsumer is not null.
        return "ApiConsumer-Processing-Lock-" + apiConsumer.getGuid();
    }
    public String lockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            // The stored value is not important.
            String val = apiConsumer.getGuid();
            getCache().put(getApiConsumerLockKey(apiConsumer), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            String val = apiConsumer.getGuid();
            getCache().remove(getApiConsumerLockKey(apiConsumer));
            return val;
        } else {
            return null;
        }
    }
    public boolean isApiConsumerLocked(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            return getCache().containsKey(getApiConsumerLockKey(apiConsumer)); 
        } else {
            return false;
        }
    }

    private static String getUserLockKey(User user)
    {
        // Note: We assume that the arg user is not null.
        return "User-Processing-Lock-" + user.getGuid();
    }
    public String lockUser(User user)
    {
        if(getCache() != null && user != null) {
            // The stored value is not important.
            String val = user.getGuid();
            getCache().put(getUserLockKey(user), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUser(User user)
    {
        if(getCache() != null && user != null) {
            String val = user.getGuid();
            getCache().remove(getUserLockKey(user));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserLocked(User user)
    {
        if(getCache() != null && user != null) {
            return getCache().containsKey(getUserLockKey(user)); 
        } else {
            return false;
        }
    }

    private static String getFetchRequestLockKey(FetchRequest fetchRequest)
    {
        // Note: We assume that the arg fetchRequest is not null.
        return "FetchRequest-Processing-Lock-" + fetchRequest.getGuid();
    }
    public String lockFetchRequest(FetchRequest fetchRequest)
    {
        if(getCache() != null && fetchRequest != null) {
            // The stored value is not important.
            String val = fetchRequest.getGuid();
            getCache().put(getFetchRequestLockKey(fetchRequest), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFetchRequest(FetchRequest fetchRequest)
    {
        if(getCache() != null && fetchRequest != null) {
            String val = fetchRequest.getGuid();
            getCache().remove(getFetchRequestLockKey(fetchRequest));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFetchRequestLocked(FetchRequest fetchRequest)
    {
        if(getCache() != null && fetchRequest != null) {
            return getCache().containsKey(getFetchRequestLockKey(fetchRequest)); 
        } else {
            return false;
        }
    }

    private static String getSecondaryFetchLockKey(SecondaryFetch secondaryFetch)
    {
        // Note: We assume that the arg secondaryFetch is not null.
        return "SecondaryFetch-Processing-Lock-" + secondaryFetch.getGuid();
    }
    public String lockSecondaryFetch(SecondaryFetch secondaryFetch)
    {
        if(getCache() != null && secondaryFetch != null) {
            // The stored value is not important.
            String val = secondaryFetch.getGuid();
            getCache().put(getSecondaryFetchLockKey(secondaryFetch), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockSecondaryFetch(SecondaryFetch secondaryFetch)
    {
        if(getCache() != null && secondaryFetch != null) {
            String val = secondaryFetch.getGuid();
            getCache().remove(getSecondaryFetchLockKey(secondaryFetch));
            return val;
        } else {
            return null;
        }
    }
    public boolean isSecondaryFetchLocked(SecondaryFetch secondaryFetch)
    {
        if(getCache() != null && secondaryFetch != null) {
            return getCache().containsKey(getSecondaryFetchLockKey(secondaryFetch)); 
        } else {
            return false;
        }
    }

    private static String getChannelSourceLockKey(ChannelSource channelSource)
    {
        // Note: We assume that the arg channelSource is not null.
        return "ChannelSource-Processing-Lock-" + channelSource.getGuid();
    }
    public String lockChannelSource(ChannelSource channelSource)
    {
        if(getCache() != null && channelSource != null) {
            // The stored value is not important.
            String val = channelSource.getGuid();
            getCache().put(getChannelSourceLockKey(channelSource), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockChannelSource(ChannelSource channelSource)
    {
        if(getCache() != null && channelSource != null) {
            String val = channelSource.getGuid();
            getCache().remove(getChannelSourceLockKey(channelSource));
            return val;
        } else {
            return null;
        }
    }
    public boolean isChannelSourceLocked(ChannelSource channelSource)
    {
        if(getCache() != null && channelSource != null) {
            return getCache().containsKey(getChannelSourceLockKey(channelSource)); 
        } else {
            return false;
        }
    }

    private static String getChannelFeedLockKey(ChannelFeed channelFeed)
    {
        // Note: We assume that the arg channelFeed is not null.
        return "ChannelFeed-Processing-Lock-" + channelFeed.getGuid();
    }
    public String lockChannelFeed(ChannelFeed channelFeed)
    {
        if(getCache() != null && channelFeed != null) {
            // The stored value is not important.
            String val = channelFeed.getGuid();
            getCache().put(getChannelFeedLockKey(channelFeed), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockChannelFeed(ChannelFeed channelFeed)
    {
        if(getCache() != null && channelFeed != null) {
            String val = channelFeed.getGuid();
            getCache().remove(getChannelFeedLockKey(channelFeed));
            return val;
        } else {
            return null;
        }
    }
    public boolean isChannelFeedLocked(ChannelFeed channelFeed)
    {
        if(getCache() != null && channelFeed != null) {
            return getCache().containsKey(getChannelFeedLockKey(channelFeed)); 
        } else {
            return false;
        }
    }

    private static String getFeedItemLockKey(FeedItem feedItem)
    {
        // Note: We assume that the arg feedItem is not null.
        return "FeedItem-Processing-Lock-" + feedItem.getGuid();
    }
    public String lockFeedItem(FeedItem feedItem)
    {
        if(getCache() != null && feedItem != null) {
            // The stored value is not important.
            String val = feedItem.getGuid();
            getCache().put(getFeedItemLockKey(feedItem), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFeedItem(FeedItem feedItem)
    {
        if(getCache() != null && feedItem != null) {
            String val = feedItem.getGuid();
            getCache().remove(getFeedItemLockKey(feedItem));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFeedItemLocked(FeedItem feedItem)
    {
        if(getCache() != null && feedItem != null) {
            return getCache().containsKey(getFeedItemLockKey(feedItem)); 
        } else {
            return false;
        }
    }

    private static String getFeedContentLockKey(FeedContent feedContent)
    {
        // Note: We assume that the arg feedContent is not null.
        return "FeedContent-Processing-Lock-" + feedContent.getGuid();
    }
    public String lockFeedContent(FeedContent feedContent)
    {
        if(getCache() != null && feedContent != null) {
            // The stored value is not important.
            String val = feedContent.getGuid();
            getCache().put(getFeedContentLockKey(feedContent), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFeedContent(FeedContent feedContent)
    {
        if(getCache() != null && feedContent != null) {
            String val = feedContent.getGuid();
            getCache().remove(getFeedContentLockKey(feedContent));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFeedContentLocked(FeedContent feedContent)
    {
        if(getCache() != null && feedContent != null) {
            return getCache().containsKey(getFeedContentLockKey(feedContent)); 
        } else {
            return false;
        }
    }

    private static String getServiceInfoLockKey(ServiceInfo serviceInfo)
    {
        // Note: We assume that the arg serviceInfo is not null.
        return "ServiceInfo-Processing-Lock-" + serviceInfo.getGuid();
    }
    public String lockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            // The stored value is not important.
            String val = serviceInfo.getGuid();
            getCache().put(getServiceInfoLockKey(serviceInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            String val = serviceInfo.getGuid();
            getCache().remove(getServiceInfoLockKey(serviceInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isServiceInfoLocked(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            return getCache().containsKey(getServiceInfoLockKey(serviceInfo)); 
        } else {
            return false;
        }
    }

    private static String getFiveTenLockKey(FiveTen fiveTen)
    {
        // Note: We assume that the arg fiveTen is not null.
        return "FiveTen-Processing-Lock-" + fiveTen.getGuid();
    }
    public String lockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            // The stored value is not important.
            String val = fiveTen.getGuid();
            getCache().put(getFiveTenLockKey(fiveTen), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            String val = fiveTen.getGuid();
            getCache().remove(getFiveTenLockKey(fiveTen));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFiveTenLocked(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            return getCache().containsKey(getFiveTenLockKey(fiveTen)); 
        } else {
            return false;
        }
    }

 
}
