package com.feedstoa.af.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.KeyValuePairStruct;
import com.feedstoa.ws.KeyValueRelationStruct;
import com.feedstoa.ws.ExternalServiceApiKeyStruct;
import com.feedstoa.ws.GeoPointStruct;
import com.feedstoa.ws.GeoCoordinateStruct;
import com.feedstoa.ws.CellLatitudeLongitude;
import com.feedstoa.ws.StreetAddressStruct;
import com.feedstoa.ws.FullNameStruct;
import com.feedstoa.ws.UserWebsiteStruct;
import com.feedstoa.ws.ContactInfoStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.GaeUserStruct;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.PagerStateStruct;
import com.feedstoa.ws.AppBrandStruct;
import com.feedstoa.ws.ApiConsumer;
import com.feedstoa.ws.User;
import com.feedstoa.ws.FetchBase;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.HelpNotice;
import com.feedstoa.ws.SiteLog;
import com.feedstoa.ws.ServiceInfo;
import com.feedstoa.ws.FiveTen;
import com.feedstoa.ws.stub.KeyValuePairStructStub;
import com.feedstoa.ws.stub.KeyValuePairStructListStub;
import com.feedstoa.ws.stub.KeyValueRelationStructStub;
import com.feedstoa.ws.stub.KeyValueRelationStructListStub;
import com.feedstoa.ws.stub.ExternalServiceApiKeyStructStub;
import com.feedstoa.ws.stub.ExternalServiceApiKeyStructListStub;
import com.feedstoa.ws.stub.GeoPointStructStub;
import com.feedstoa.ws.stub.GeoPointStructListStub;
import com.feedstoa.ws.stub.GeoCoordinateStructStub;
import com.feedstoa.ws.stub.GeoCoordinateStructListStub;
import com.feedstoa.ws.stub.CellLatitudeLongitudeStub;
import com.feedstoa.ws.stub.CellLatitudeLongitudeListStub;
import com.feedstoa.ws.stub.StreetAddressStructStub;
import com.feedstoa.ws.stub.StreetAddressStructListStub;
import com.feedstoa.ws.stub.FullNameStructStub;
import com.feedstoa.ws.stub.FullNameStructListStub;
import com.feedstoa.ws.stub.UserWebsiteStructStub;
import com.feedstoa.ws.stub.UserWebsiteStructListStub;
import com.feedstoa.ws.stub.ContactInfoStructStub;
import com.feedstoa.ws.stub.ContactInfoStructListStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructListStub;
import com.feedstoa.ws.stub.GaeAppStructStub;
import com.feedstoa.ws.stub.GaeAppStructListStub;
import com.feedstoa.ws.stub.GaeUserStructStub;
import com.feedstoa.ws.stub.GaeUserStructListStub;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.ws.stub.NotificationStructListStub;
import com.feedstoa.ws.stub.PagerStateStructStub;
import com.feedstoa.ws.stub.PagerStateStructListStub;
import com.feedstoa.ws.stub.AppBrandStructStub;
import com.feedstoa.ws.stub.AppBrandStructListStub;
import com.feedstoa.ws.stub.ApiConsumerStub;
import com.feedstoa.ws.stub.ApiConsumerListStub;
import com.feedstoa.ws.stub.UserStub;
import com.feedstoa.ws.stub.UserListStub;
import com.feedstoa.ws.stub.FetchRequestStub;
import com.feedstoa.ws.stub.FetchRequestListStub;
import com.feedstoa.ws.stub.SecondaryFetchStub;
import com.feedstoa.ws.stub.SecondaryFetchListStub;
import com.feedstoa.ws.stub.ChannelSourceStub;
import com.feedstoa.ws.stub.ChannelSourceListStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.stub.UriStructListStub;
import com.feedstoa.ws.stub.UserStructStub;
import com.feedstoa.ws.stub.UserStructListStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.CategoryStructListStub;
import com.feedstoa.ws.stub.CloudStructStub;
import com.feedstoa.ws.stub.CloudStructListStub;
import com.feedstoa.ws.stub.ImageStructStub;
import com.feedstoa.ws.stub.ImageStructListStub;
import com.feedstoa.ws.stub.TextInputStructStub;
import com.feedstoa.ws.stub.TextInputStructListStub;
import com.feedstoa.ws.stub.EnclosureStructStub;
import com.feedstoa.ws.stub.EnclosureStructListStub;
import com.feedstoa.ws.stub.RssItemStub;
import com.feedstoa.ws.stub.RssItemListStub;
import com.feedstoa.ws.stub.RssChannelStub;
import com.feedstoa.ws.stub.RssChannelListStub;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.stub.ChannelFeedListStub;
import com.feedstoa.ws.stub.FeedItemStub;
import com.feedstoa.ws.stub.FeedItemListStub;
import com.feedstoa.ws.stub.FeedContentStub;
import com.feedstoa.ws.stub.FeedContentListStub;
import com.feedstoa.ws.stub.RedirectRuleStub;
import com.feedstoa.ws.stub.RedirectRuleListStub;
import com.feedstoa.ws.stub.HelpNoticeStub;
import com.feedstoa.ws.stub.HelpNoticeListStub;
import com.feedstoa.ws.stub.SiteLogStub;
import com.feedstoa.ws.stub.SiteLogListStub;
import com.feedstoa.ws.stub.ServiceInfoStub;
import com.feedstoa.ws.stub.ServiceInfoListStub;
import com.feedstoa.ws.stub.FiveTenStub;
import com.feedstoa.ws.stub.FiveTenListStub;
import com.feedstoa.af.bean.KeyValuePairStructBean;
import com.feedstoa.af.bean.KeyValueRelationStructBean;
import com.feedstoa.af.bean.ExternalServiceApiKeyStructBean;
import com.feedstoa.af.bean.GeoPointStructBean;
import com.feedstoa.af.bean.GeoCoordinateStructBean;
import com.feedstoa.af.bean.CellLatitudeLongitudeBean;
import com.feedstoa.af.bean.StreetAddressStructBean;
import com.feedstoa.af.bean.FullNameStructBean;
import com.feedstoa.af.bean.UserWebsiteStructBean;
import com.feedstoa.af.bean.ContactInfoStructBean;
import com.feedstoa.af.bean.ReferrerInfoStructBean;
import com.feedstoa.af.bean.GaeAppStructBean;
import com.feedstoa.af.bean.GaeUserStructBean;
import com.feedstoa.af.bean.NotificationStructBean;
import com.feedstoa.af.bean.PagerStateStructBean;
import com.feedstoa.af.bean.AppBrandStructBean;
import com.feedstoa.af.bean.ApiConsumerBean;
import com.feedstoa.af.bean.UserBean;
import com.feedstoa.af.bean.FetchBaseBean;
import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.af.bean.ChannelSourceBean;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.af.bean.TextInputStructBean;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.af.bean.RssChannelBean;
import com.feedstoa.af.bean.ChannelFeedBean;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.bean.FeedContentBean;
import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.af.bean.HelpNoticeBean;
import com.feedstoa.af.bean.SiteLogBean;
import com.feedstoa.af.bean.ServiceInfoBean;
import com.feedstoa.af.bean.FiveTenBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static KeyValuePairStructBean convertKeyValuePairStructToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValuePairStructBean();
        } else if(stub instanceof KeyValuePairStructBean) {
        	bean = (KeyValuePairStructBean) stub;
        } else {
        	bean = new KeyValuePairStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValuePairStructBean> convertKeyValuePairStructListToBeanList(List<KeyValuePairStruct> stubList)
    {
        List<KeyValuePairStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStructBean>();
            for(KeyValuePairStruct stub : stubList) {
                beanList.add(convertKeyValuePairStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub)
    {
        return convertKeyValuePairStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValuePairStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStruct>();
            for(KeyValuePairStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValuePairStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValuePairStructStub convertKeyValuePairStructToStub(KeyValuePairStruct bean)
    {
        KeyValuePairStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValuePairStructStub();
        } else if(bean instanceof KeyValuePairStructStub) {
            stub = (KeyValuePairStructStub) bean;
        } else if(bean instanceof KeyValuePairStructBean && ((KeyValuePairStructBean) bean).isWrapper()) {
            stub = ((KeyValuePairStructBean) bean).getStub();
        } else {
            stub = KeyValuePairStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeyValueRelationStructBean convertKeyValueRelationStructToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValueRelationStructBean();
        } else if(stub instanceof KeyValueRelationStructBean) {
        	bean = (KeyValueRelationStructBean) stub;
        } else {
        	bean = new KeyValueRelationStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValueRelationStructBean> convertKeyValueRelationStructListToBeanList(List<KeyValueRelationStruct> stubList)
    {
        List<KeyValueRelationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStructBean>();
            for(KeyValueRelationStruct stub : stubList) {
                beanList.add(convertKeyValueRelationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub)
    {
        return convertKeyValueRelationStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValueRelationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStruct>();
            for(KeyValueRelationStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValueRelationStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValueRelationStructStub convertKeyValueRelationStructToStub(KeyValueRelationStruct bean)
    {
        KeyValueRelationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValueRelationStructStub();
        } else if(bean instanceof KeyValueRelationStructStub) {
            stub = (KeyValueRelationStructStub) bean;
        } else if(bean instanceof KeyValueRelationStructBean && ((KeyValueRelationStructBean) bean).isWrapper()) {
            stub = ((KeyValueRelationStructBean) bean).getStub();
        } else {
            stub = KeyValueRelationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalServiceApiKeyStructBean convertExternalServiceApiKeyStructToBean(ExternalServiceApiKeyStruct stub)
    {
        ExternalServiceApiKeyStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalServiceApiKeyStructBean();
        } else if(stub instanceof ExternalServiceApiKeyStructBean) {
        	bean = (ExternalServiceApiKeyStructBean) stub;
        } else {
        	bean = new ExternalServiceApiKeyStructBean(stub);
        }
        return bean;
    }
    public static List<ExternalServiceApiKeyStructBean> convertExternalServiceApiKeyStructListToBeanList(List<ExternalServiceApiKeyStruct> stubList)
    {
        List<ExternalServiceApiKeyStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStructBean>();
            for(ExternalServiceApiKeyStruct stub : stubList) {
                beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub)
    {
        return convertExternalServiceApiKeyStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub, StringCursor forwardCursor)
    {
        List<ExternalServiceApiKeyStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStruct>();
            for(ExternalServiceApiKeyStructStub stub : listStub.getList()) {
            	beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalServiceApiKeyStructStub convertExternalServiceApiKeyStructToStub(ExternalServiceApiKeyStruct bean)
    {
        ExternalServiceApiKeyStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalServiceApiKeyStructStub();
        } else if(bean instanceof ExternalServiceApiKeyStructStub) {
            stub = (ExternalServiceApiKeyStructStub) bean;
        } else if(bean instanceof ExternalServiceApiKeyStructBean && ((ExternalServiceApiKeyStructBean) bean).isWrapper()) {
            stub = ((ExternalServiceApiKeyStructBean) bean).getStub();
        } else {
            stub = ExternalServiceApiKeyStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoPointStructBean convertGeoPointStructToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoPointStructBean();
        } else if(stub instanceof GeoPointStructBean) {
        	bean = (GeoPointStructBean) stub;
        } else {
        	bean = new GeoPointStructBean(stub);
        }
        return bean;
    }
    public static List<GeoPointStructBean> convertGeoPointStructListToBeanList(List<GeoPointStruct> stubList)
    {
        List<GeoPointStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStructBean>();
            for(GeoPointStruct stub : stubList) {
                beanList.add(convertGeoPointStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub)
    {
        return convertGeoPointStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub, StringCursor forwardCursor)
    {
        List<GeoPointStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStruct>();
            for(GeoPointStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoPointStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GeoPointStructStub convertGeoPointStructToStub(GeoPointStruct bean)
    {
        GeoPointStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoPointStructStub();
        } else if(bean instanceof GeoPointStructStub) {
            stub = (GeoPointStructStub) bean;
        } else if(bean instanceof GeoPointStructBean && ((GeoPointStructBean) bean).isWrapper()) {
            stub = ((GeoPointStructBean) bean).getStub();
        } else {
            stub = GeoPointStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoCoordinateStructBean convertGeoCoordinateStructToBean(GeoCoordinateStruct stub)
    {
        GeoCoordinateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoCoordinateStructBean();
        } else if(stub instanceof GeoCoordinateStructBean) {
        	bean = (GeoCoordinateStructBean) stub;
        } else {
        	bean = new GeoCoordinateStructBean(stub);
        }
        return bean;
    }
    public static List<GeoCoordinateStructBean> convertGeoCoordinateStructListToBeanList(List<GeoCoordinateStruct> stubList)
    {
        List<GeoCoordinateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStructBean>();
            for(GeoCoordinateStruct stub : stubList) {
                beanList.add(convertGeoCoordinateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub)
    {
        return convertGeoCoordinateStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub, StringCursor forwardCursor)
    {
        List<GeoCoordinateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStruct>();
            for(GeoCoordinateStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoCoordinateStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GeoCoordinateStructStub convertGeoCoordinateStructToStub(GeoCoordinateStruct bean)
    {
        GeoCoordinateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoCoordinateStructStub();
        } else if(bean instanceof GeoCoordinateStructStub) {
            stub = (GeoCoordinateStructStub) bean;
        } else if(bean instanceof GeoCoordinateStructBean && ((GeoCoordinateStructBean) bean).isWrapper()) {
            stub = ((GeoCoordinateStructBean) bean).getStub();
        } else {
            stub = GeoCoordinateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CellLatitudeLongitudeBean convertCellLatitudeLongitudeToBean(CellLatitudeLongitude stub)
    {
        CellLatitudeLongitudeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CellLatitudeLongitudeBean();
        } else if(stub instanceof CellLatitudeLongitudeBean) {
        	bean = (CellLatitudeLongitudeBean) stub;
        } else {
        	bean = new CellLatitudeLongitudeBean(stub);
        }
        return bean;
    }
    public static List<CellLatitudeLongitudeBean> convertCellLatitudeLongitudeListToBeanList(List<CellLatitudeLongitude> stubList)
    {
        List<CellLatitudeLongitudeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CellLatitudeLongitude>();
        } else {
            beanList = new ArrayList<CellLatitudeLongitudeBean>();
            for(CellLatitudeLongitude stub : stubList) {
                beanList.add(convertCellLatitudeLongitudeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CellLatitudeLongitude> convertCellLatitudeLongitudeListStubToBeanList(CellLatitudeLongitudeListStub listStub)
    {
        return convertCellLatitudeLongitudeListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<CellLatitudeLongitude> convertCellLatitudeLongitudeListStubToBeanList(CellLatitudeLongitudeListStub listStub, StringCursor forwardCursor)
    {
        List<CellLatitudeLongitude> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CellLatitudeLongitude>();
        } else {
            beanList = new ArrayList<CellLatitudeLongitude>();
            for(CellLatitudeLongitudeStub stub : listStub.getList()) {
            	beanList.add(convertCellLatitudeLongitudeToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static CellLatitudeLongitudeStub convertCellLatitudeLongitudeToStub(CellLatitudeLongitude bean)
    {
        CellLatitudeLongitudeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CellLatitudeLongitudeStub();
        } else if(bean instanceof CellLatitudeLongitudeStub) {
            stub = (CellLatitudeLongitudeStub) bean;
        } else if(bean instanceof CellLatitudeLongitudeBean && ((CellLatitudeLongitudeBean) bean).isWrapper()) {
            stub = ((CellLatitudeLongitudeBean) bean).getStub();
        } else {
            stub = CellLatitudeLongitudeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static StreetAddressStructBean convertStreetAddressStructToBean(StreetAddressStruct stub)
    {
        StreetAddressStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new StreetAddressStructBean();
        } else if(stub instanceof StreetAddressStructBean) {
        	bean = (StreetAddressStructBean) stub;
        } else {
        	bean = new StreetAddressStructBean(stub);
        }
        return bean;
    }
    public static List<StreetAddressStructBean> convertStreetAddressStructListToBeanList(List<StreetAddressStruct> stubList)
    {
        List<StreetAddressStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStructBean>();
            for(StreetAddressStruct stub : stubList) {
                beanList.add(convertStreetAddressStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub)
    {
        return convertStreetAddressStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub, StringCursor forwardCursor)
    {
        List<StreetAddressStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStruct>();
            for(StreetAddressStructStub stub : listStub.getList()) {
            	beanList.add(convertStreetAddressStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static StreetAddressStructStub convertStreetAddressStructToStub(StreetAddressStruct bean)
    {
        StreetAddressStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new StreetAddressStructStub();
        } else if(bean instanceof StreetAddressStructStub) {
            stub = (StreetAddressStructStub) bean;
        } else if(bean instanceof StreetAddressStructBean && ((StreetAddressStructBean) bean).isWrapper()) {
            stub = ((StreetAddressStructBean) bean).getStub();
        } else {
            stub = StreetAddressStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FullNameStructBean convertFullNameStructToBean(FullNameStruct stub)
    {
        FullNameStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FullNameStructBean();
        } else if(stub instanceof FullNameStructBean) {
        	bean = (FullNameStructBean) stub;
        } else {
        	bean = new FullNameStructBean(stub);
        }
        return bean;
    }
    public static List<FullNameStructBean> convertFullNameStructListToBeanList(List<FullNameStruct> stubList)
    {
        List<FullNameStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStructBean>();
            for(FullNameStruct stub : stubList) {
                beanList.add(convertFullNameStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub)
    {
        return convertFullNameStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub, StringCursor forwardCursor)
    {
        List<FullNameStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStruct>();
            for(FullNameStructStub stub : listStub.getList()) {
            	beanList.add(convertFullNameStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FullNameStructStub convertFullNameStructToStub(FullNameStruct bean)
    {
        FullNameStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FullNameStructStub();
        } else if(bean instanceof FullNameStructStub) {
            stub = (FullNameStructStub) bean;
        } else if(bean instanceof FullNameStructBean && ((FullNameStructBean) bean).isWrapper()) {
            stub = ((FullNameStructBean) bean).getStub();
        } else {
            stub = FullNameStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserWebsiteStructBean convertUserWebsiteStructToBean(UserWebsiteStruct stub)
    {
        UserWebsiteStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserWebsiteStructBean();
        } else if(stub instanceof UserWebsiteStructBean) {
        	bean = (UserWebsiteStructBean) stub;
        } else {
        	bean = new UserWebsiteStructBean(stub);
        }
        return bean;
    }
    public static List<UserWebsiteStructBean> convertUserWebsiteStructListToBeanList(List<UserWebsiteStruct> stubList)
    {
        List<UserWebsiteStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserWebsiteStruct>();
        } else {
            beanList = new ArrayList<UserWebsiteStructBean>();
            for(UserWebsiteStruct stub : stubList) {
                beanList.add(convertUserWebsiteStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserWebsiteStruct> convertUserWebsiteStructListStubToBeanList(UserWebsiteStructListStub listStub)
    {
        return convertUserWebsiteStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UserWebsiteStruct> convertUserWebsiteStructListStubToBeanList(UserWebsiteStructListStub listStub, StringCursor forwardCursor)
    {
        List<UserWebsiteStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserWebsiteStruct>();
        } else {
            beanList = new ArrayList<UserWebsiteStruct>();
            for(UserWebsiteStructStub stub : listStub.getList()) {
            	beanList.add(convertUserWebsiteStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserWebsiteStructStub convertUserWebsiteStructToStub(UserWebsiteStruct bean)
    {
        UserWebsiteStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserWebsiteStructStub();
        } else if(bean instanceof UserWebsiteStructStub) {
            stub = (UserWebsiteStructStub) bean;
        } else if(bean instanceof UserWebsiteStructBean && ((UserWebsiteStructBean) bean).isWrapper()) {
            stub = ((UserWebsiteStructBean) bean).getStub();
        } else {
            stub = UserWebsiteStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ContactInfoStructBean convertContactInfoStructToBean(ContactInfoStruct stub)
    {
        ContactInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ContactInfoStructBean();
        } else if(stub instanceof ContactInfoStructBean) {
        	bean = (ContactInfoStructBean) stub;
        } else {
        	bean = new ContactInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ContactInfoStructBean> convertContactInfoStructListToBeanList(List<ContactInfoStruct> stubList)
    {
        List<ContactInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ContactInfoStruct>();
        } else {
            beanList = new ArrayList<ContactInfoStructBean>();
            for(ContactInfoStruct stub : stubList) {
                beanList.add(convertContactInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ContactInfoStruct> convertContactInfoStructListStubToBeanList(ContactInfoStructListStub listStub)
    {
        return convertContactInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ContactInfoStruct> convertContactInfoStructListStubToBeanList(ContactInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<ContactInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ContactInfoStruct>();
        } else {
            beanList = new ArrayList<ContactInfoStruct>();
            for(ContactInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertContactInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ContactInfoStructStub convertContactInfoStructToStub(ContactInfoStruct bean)
    {
        ContactInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ContactInfoStructStub();
        } else if(bean instanceof ContactInfoStructStub) {
            stub = (ContactInfoStructStub) bean;
        } else if(bean instanceof ContactInfoStructBean && ((ContactInfoStructBean) bean).isWrapper()) {
            stub = ((ContactInfoStructBean) bean).getStub();
        } else {
            stub = ContactInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ReferrerInfoStructBean convertReferrerInfoStructToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ReferrerInfoStructBean();
        } else if(stub instanceof ReferrerInfoStructBean) {
        	bean = (ReferrerInfoStructBean) stub;
        } else {
        	bean = new ReferrerInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ReferrerInfoStructBean> convertReferrerInfoStructListToBeanList(List<ReferrerInfoStruct> stubList)
    {
        List<ReferrerInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStructBean>();
            for(ReferrerInfoStruct stub : stubList) {
                beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub)
    {
        return convertReferrerInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<ReferrerInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStruct>();
            for(ReferrerInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertReferrerInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ReferrerInfoStructStub convertReferrerInfoStructToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ReferrerInfoStructStub();
        } else if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else if(bean instanceof ReferrerInfoStructBean && ((ReferrerInfoStructBean) bean).isWrapper()) {
            stub = ((ReferrerInfoStructBean) bean).getStub();
        } else {
            stub = ReferrerInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeAppStructBean convertGaeAppStructToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeAppStructBean();
        } else if(stub instanceof GaeAppStructBean) {
        	bean = (GaeAppStructBean) stub;
        } else {
        	bean = new GaeAppStructBean(stub);
        }
        return bean;
    }
    public static List<GaeAppStructBean> convertGaeAppStructListToBeanList(List<GaeAppStruct> stubList)
    {
        List<GaeAppStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStructBean>();
            for(GaeAppStruct stub : stubList) {
                beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub)
    {
        return convertGaeAppStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeAppStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStruct>();
            for(GaeAppStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeAppStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeAppStructStub convertGaeAppStructToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeAppStructStub();
        } else if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else if(bean instanceof GaeAppStructBean && ((GaeAppStructBean) bean).isWrapper()) {
            stub = ((GaeAppStructBean) bean).getStub();
        } else {
            stub = GaeAppStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeUserStructBean convertGaeUserStructToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeUserStructBean();
        } else if(stub instanceof GaeUserStructBean) {
        	bean = (GaeUserStructBean) stub;
        } else {
        	bean = new GaeUserStructBean(stub);
        }
        return bean;
    }
    public static List<GaeUserStructBean> convertGaeUserStructListToBeanList(List<GaeUserStruct> stubList)
    {
        List<GaeUserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStructBean>();
            for(GaeUserStruct stub : stubList) {
                beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub)
    {
        return convertGaeUserStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeUserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStruct>();
            for(GaeUserStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeUserStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeUserStructStub convertGaeUserStructToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeUserStructStub();
        } else if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else if(bean instanceof GaeUserStructBean && ((GaeUserStructBean) bean).isWrapper()) {
            stub = ((GaeUserStructBean) bean).getStub();
        } else {
            stub = GaeUserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static NotificationStructBean convertNotificationStructToBean(NotificationStruct stub)
    {
        NotificationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new NotificationStructBean();
        } else if(stub instanceof NotificationStructBean) {
        	bean = (NotificationStructBean) stub;
        } else {
        	bean = new NotificationStructBean(stub);
        }
        return bean;
    }
    public static List<NotificationStructBean> convertNotificationStructListToBeanList(List<NotificationStruct> stubList)
    {
        List<NotificationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<NotificationStruct>();
        } else {
            beanList = new ArrayList<NotificationStructBean>();
            for(NotificationStruct stub : stubList) {
                beanList.add(convertNotificationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<NotificationStruct> convertNotificationStructListStubToBeanList(NotificationStructListStub listStub)
    {
        return convertNotificationStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<NotificationStruct> convertNotificationStructListStubToBeanList(NotificationStructListStub listStub, StringCursor forwardCursor)
    {
        List<NotificationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<NotificationStruct>();
        } else {
            beanList = new ArrayList<NotificationStruct>();
            for(NotificationStructStub stub : listStub.getList()) {
            	beanList.add(convertNotificationStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static NotificationStructStub convertNotificationStructToStub(NotificationStruct bean)
    {
        NotificationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new NotificationStructStub();
        } else if(bean instanceof NotificationStructStub) {
            stub = (NotificationStructStub) bean;
        } else if(bean instanceof NotificationStructBean && ((NotificationStructBean) bean).isWrapper()) {
            stub = ((NotificationStructBean) bean).getStub();
        } else {
            stub = NotificationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PagerStateStructBean convertPagerStateStructToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PagerStateStructBean();
        } else if(stub instanceof PagerStateStructBean) {
        	bean = (PagerStateStructBean) stub;
        } else {
        	bean = new PagerStateStructBean(stub);
        }
        return bean;
    }
    public static List<PagerStateStructBean> convertPagerStateStructListToBeanList(List<PagerStateStruct> stubList)
    {
        List<PagerStateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStructBean>();
            for(PagerStateStruct stub : stubList) {
                beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub)
    {
        return convertPagerStateStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub, StringCursor forwardCursor)
    {
        List<PagerStateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStruct>();
            for(PagerStateStructStub stub : listStub.getList()) {
            	beanList.add(convertPagerStateStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static PagerStateStructStub convertPagerStateStructToStub(PagerStateStruct bean)
    {
        PagerStateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PagerStateStructStub();
        } else if(bean instanceof PagerStateStructStub) {
            stub = (PagerStateStructStub) bean;
        } else if(bean instanceof PagerStateStructBean && ((PagerStateStructBean) bean).isWrapper()) {
            stub = ((PagerStateStructBean) bean).getStub();
        } else {
            stub = PagerStateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AppBrandStructBean convertAppBrandStructToBean(AppBrandStruct stub)
    {
        AppBrandStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AppBrandStructBean();
        } else if(stub instanceof AppBrandStructBean) {
        	bean = (AppBrandStructBean) stub;
        } else {
        	bean = new AppBrandStructBean(stub);
        }
        return bean;
    }
    public static List<AppBrandStructBean> convertAppBrandStructListToBeanList(List<AppBrandStruct> stubList)
    {
        List<AppBrandStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AppBrandStruct>();
        } else {
            beanList = new ArrayList<AppBrandStructBean>();
            for(AppBrandStruct stub : stubList) {
                beanList.add(convertAppBrandStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AppBrandStruct> convertAppBrandStructListStubToBeanList(AppBrandStructListStub listStub)
    {
        return convertAppBrandStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AppBrandStruct> convertAppBrandStructListStubToBeanList(AppBrandStructListStub listStub, StringCursor forwardCursor)
    {
        List<AppBrandStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AppBrandStruct>();
        } else {
            beanList = new ArrayList<AppBrandStruct>();
            for(AppBrandStructStub stub : listStub.getList()) {
            	beanList.add(convertAppBrandStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AppBrandStructStub convertAppBrandStructToStub(AppBrandStruct bean)
    {
        AppBrandStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AppBrandStructStub();
        } else if(bean instanceof AppBrandStructStub) {
            stub = (AppBrandStructStub) bean;
        } else if(bean instanceof AppBrandStructBean && ((AppBrandStructBean) bean).isWrapper()) {
            stub = ((AppBrandStructBean) bean).getStub();
        } else {
            stub = AppBrandStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ApiConsumerBean convertApiConsumerToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ApiConsumerBean();
        } else if(stub instanceof ApiConsumerBean) {
        	bean = (ApiConsumerBean) stub;
        } else {
        	bean = new ApiConsumerBean(stub);
        }
        return bean;
    }
    public static List<ApiConsumerBean> convertApiConsumerListToBeanList(List<ApiConsumer> stubList)
    {
        List<ApiConsumerBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumerBean>();
            for(ApiConsumer stub : stubList) {
                beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub)
    {
        return convertApiConsumerListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub, StringCursor forwardCursor)
    {
        List<ApiConsumer> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumer>();
            for(ApiConsumerStub stub : listStub.getList()) {
            	beanList.add(convertApiConsumerToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ApiConsumerStub convertApiConsumerToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ApiConsumerStub();
        } else if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else if(bean instanceof ApiConsumerBean && ((ApiConsumerBean) bean).isWrapper()) {
            stub = ((ApiConsumerBean) bean).getStub();
        } else {
            stub = ApiConsumerStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserBean convertUserToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserBean();
        } else if(stub instanceof UserBean) {
        	bean = (UserBean) stub;
        } else {
        	bean = new UserBean(stub);
        }
        return bean;
    }
    public static List<UserBean> convertUserListToBeanList(List<User> stubList)
    {
        List<UserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<UserBean>();
            for(User stub : stubList) {
                beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<User> convertUserListStubToBeanList(UserListStub listStub)
    {
        return convertUserListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<User> convertUserListStubToBeanList(UserListStub listStub, StringCursor forwardCursor)
    {
        List<User> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<User>();
            for(UserStub stub : listStub.getList()) {
            	beanList.add(convertUserToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserStub convertUserToStub(User bean)
    {
        UserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStub();
        } else if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else if(bean instanceof UserBean && ((UserBean) bean).isWrapper()) {
            stub = ((UserBean) bean).getStub();
        } else {
            stub = UserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FetchRequestBean convertFetchRequestToBean(FetchRequest stub)
    {
        FetchRequestBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FetchRequestBean();
        } else if(stub instanceof FetchRequestBean) {
        	bean = (FetchRequestBean) stub;
        } else {
        	bean = new FetchRequestBean(stub);
        }
        return bean;
    }
    public static List<FetchRequestBean> convertFetchRequestListToBeanList(List<FetchRequest> stubList)
    {
        List<FetchRequestBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FetchRequest>();
        } else {
            beanList = new ArrayList<FetchRequestBean>();
            for(FetchRequest stub : stubList) {
                beanList.add(convertFetchRequestToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FetchRequest> convertFetchRequestListStubToBeanList(FetchRequestListStub listStub)
    {
        return convertFetchRequestListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FetchRequest> convertFetchRequestListStubToBeanList(FetchRequestListStub listStub, StringCursor forwardCursor)
    {
        List<FetchRequest> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FetchRequest>();
        } else {
            beanList = new ArrayList<FetchRequest>();
            for(FetchRequestStub stub : listStub.getList()) {
            	beanList.add(convertFetchRequestToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FetchRequestStub convertFetchRequestToStub(FetchRequest bean)
    {
        FetchRequestStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FetchRequestStub();
        } else if(bean instanceof FetchRequestStub) {
            stub = (FetchRequestStub) bean;
        } else if(bean instanceof FetchRequestBean && ((FetchRequestBean) bean).isWrapper()) {
            stub = ((FetchRequestBean) bean).getStub();
        } else {
            stub = FetchRequestStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static SecondaryFetchBean convertSecondaryFetchToBean(SecondaryFetch stub)
    {
        SecondaryFetchBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new SecondaryFetchBean();
        } else if(stub instanceof SecondaryFetchBean) {
        	bean = (SecondaryFetchBean) stub;
        } else {
        	bean = new SecondaryFetchBean(stub);
        }
        return bean;
    }
    public static List<SecondaryFetchBean> convertSecondaryFetchListToBeanList(List<SecondaryFetch> stubList)
    {
        List<SecondaryFetchBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<SecondaryFetch>();
        } else {
            beanList = new ArrayList<SecondaryFetchBean>();
            for(SecondaryFetch stub : stubList) {
                beanList.add(convertSecondaryFetchToBean(stub));
            }
        }
        return beanList;
    }
    public static List<SecondaryFetch> convertSecondaryFetchListStubToBeanList(SecondaryFetchListStub listStub)
    {
        return convertSecondaryFetchListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<SecondaryFetch> convertSecondaryFetchListStubToBeanList(SecondaryFetchListStub listStub, StringCursor forwardCursor)
    {
        List<SecondaryFetch> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<SecondaryFetch>();
        } else {
            beanList = new ArrayList<SecondaryFetch>();
            for(SecondaryFetchStub stub : listStub.getList()) {
            	beanList.add(convertSecondaryFetchToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static SecondaryFetchStub convertSecondaryFetchToStub(SecondaryFetch bean)
    {
        SecondaryFetchStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new SecondaryFetchStub();
        } else if(bean instanceof SecondaryFetchStub) {
            stub = (SecondaryFetchStub) bean;
        } else if(bean instanceof SecondaryFetchBean && ((SecondaryFetchBean) bean).isWrapper()) {
            stub = ((SecondaryFetchBean) bean).getStub();
        } else {
            stub = SecondaryFetchStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ChannelSourceBean convertChannelSourceToBean(ChannelSource stub)
    {
        ChannelSourceBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ChannelSourceBean();
        } else if(stub instanceof ChannelSourceBean) {
        	bean = (ChannelSourceBean) stub;
        } else {
        	bean = new ChannelSourceBean(stub);
        }
        return bean;
    }
    public static List<ChannelSourceBean> convertChannelSourceListToBeanList(List<ChannelSource> stubList)
    {
        List<ChannelSourceBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ChannelSource>();
        } else {
            beanList = new ArrayList<ChannelSourceBean>();
            for(ChannelSource stub : stubList) {
                beanList.add(convertChannelSourceToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ChannelSource> convertChannelSourceListStubToBeanList(ChannelSourceListStub listStub)
    {
        return convertChannelSourceListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ChannelSource> convertChannelSourceListStubToBeanList(ChannelSourceListStub listStub, StringCursor forwardCursor)
    {
        List<ChannelSource> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ChannelSource>();
        } else {
            beanList = new ArrayList<ChannelSource>();
            for(ChannelSourceStub stub : listStub.getList()) {
            	beanList.add(convertChannelSourceToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ChannelSourceStub convertChannelSourceToStub(ChannelSource bean)
    {
        ChannelSourceStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ChannelSourceStub();
        } else if(bean instanceof ChannelSourceStub) {
            stub = (ChannelSourceStub) bean;
        } else if(bean instanceof ChannelSourceBean && ((ChannelSourceBean) bean).isWrapper()) {
            stub = ((ChannelSourceBean) bean).getStub();
        } else {
            stub = ChannelSourceStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UriStructBean convertUriStructToBean(UriStruct stub)
    {
        UriStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UriStructBean();
        } else if(stub instanceof UriStructBean) {
        	bean = (UriStructBean) stub;
        } else {
        	bean = new UriStructBean(stub);
        }
        return bean;
    }
    public static List<UriStructBean> convertUriStructListToBeanList(List<UriStruct> stubList)
    {
        List<UriStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UriStruct>();
        } else {
            beanList = new ArrayList<UriStructBean>();
            for(UriStruct stub : stubList) {
                beanList.add(convertUriStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UriStruct> convertUriStructListStubToBeanList(UriStructListStub listStub)
    {
        return convertUriStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UriStruct> convertUriStructListStubToBeanList(UriStructListStub listStub, StringCursor forwardCursor)
    {
        List<UriStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UriStruct>();
        } else {
            beanList = new ArrayList<UriStruct>();
            for(UriStructStub stub : listStub.getList()) {
            	beanList.add(convertUriStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UriStructStub convertUriStructToStub(UriStruct bean)
    {
        UriStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UriStructStub();
        } else if(bean instanceof UriStructStub) {
            stub = (UriStructStub) bean;
        } else if(bean instanceof UriStructBean && ((UriStructBean) bean).isWrapper()) {
            stub = ((UriStructBean) bean).getStub();
        } else {
            stub = UriStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserStructBean convertUserStructToBean(UserStruct stub)
    {
        UserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserStructBean();
        } else if(stub instanceof UserStructBean) {
        	bean = (UserStructBean) stub;
        } else {
        	bean = new UserStructBean(stub);
        }
        return bean;
    }
    public static List<UserStructBean> convertUserStructListToBeanList(List<UserStruct> stubList)
    {
        List<UserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserStruct>();
        } else {
            beanList = new ArrayList<UserStructBean>();
            for(UserStruct stub : stubList) {
                beanList.add(convertUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserStruct> convertUserStructListStubToBeanList(UserStructListStub listStub)
    {
        return convertUserStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UserStruct> convertUserStructListStubToBeanList(UserStructListStub listStub, StringCursor forwardCursor)
    {
        List<UserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserStruct>();
        } else {
            beanList = new ArrayList<UserStruct>();
            for(UserStructStub stub : listStub.getList()) {
            	beanList.add(convertUserStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserStructStub convertUserStructToStub(UserStruct bean)
    {
        UserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStructStub();
        } else if(bean instanceof UserStructStub) {
            stub = (UserStructStub) bean;
        } else if(bean instanceof UserStructBean && ((UserStructBean) bean).isWrapper()) {
            stub = ((UserStructBean) bean).getStub();
        } else {
            stub = UserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CategoryStructBean convertCategoryStructToBean(CategoryStruct stub)
    {
        CategoryStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CategoryStructBean();
        } else if(stub instanceof CategoryStructBean) {
        	bean = (CategoryStructBean) stub;
        } else {
        	bean = new CategoryStructBean(stub);
        }
        return bean;
    }
    public static List<CategoryStructBean> convertCategoryStructListToBeanList(List<CategoryStruct> stubList)
    {
        List<CategoryStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CategoryStruct>();
        } else {
            beanList = new ArrayList<CategoryStructBean>();
            for(CategoryStruct stub : stubList) {
                beanList.add(convertCategoryStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CategoryStruct> convertCategoryStructListStubToBeanList(CategoryStructListStub listStub)
    {
        return convertCategoryStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<CategoryStruct> convertCategoryStructListStubToBeanList(CategoryStructListStub listStub, StringCursor forwardCursor)
    {
        List<CategoryStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CategoryStruct>();
        } else {
            beanList = new ArrayList<CategoryStruct>();
            for(CategoryStructStub stub : listStub.getList()) {
            	beanList.add(convertCategoryStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static CategoryStructStub convertCategoryStructToStub(CategoryStruct bean)
    {
        CategoryStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CategoryStructStub();
        } else if(bean instanceof CategoryStructStub) {
            stub = (CategoryStructStub) bean;
        } else if(bean instanceof CategoryStructBean && ((CategoryStructBean) bean).isWrapper()) {
            stub = ((CategoryStructBean) bean).getStub();
        } else {
            stub = CategoryStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CloudStructBean convertCloudStructToBean(CloudStruct stub)
    {
        CloudStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CloudStructBean();
        } else if(stub instanceof CloudStructBean) {
        	bean = (CloudStructBean) stub;
        } else {
        	bean = new CloudStructBean(stub);
        }
        return bean;
    }
    public static List<CloudStructBean> convertCloudStructListToBeanList(List<CloudStruct> stubList)
    {
        List<CloudStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CloudStruct>();
        } else {
            beanList = new ArrayList<CloudStructBean>();
            for(CloudStruct stub : stubList) {
                beanList.add(convertCloudStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CloudStruct> convertCloudStructListStubToBeanList(CloudStructListStub listStub)
    {
        return convertCloudStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<CloudStruct> convertCloudStructListStubToBeanList(CloudStructListStub listStub, StringCursor forwardCursor)
    {
        List<CloudStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CloudStruct>();
        } else {
            beanList = new ArrayList<CloudStruct>();
            for(CloudStructStub stub : listStub.getList()) {
            	beanList.add(convertCloudStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static CloudStructStub convertCloudStructToStub(CloudStruct bean)
    {
        CloudStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CloudStructStub();
        } else if(bean instanceof CloudStructStub) {
            stub = (CloudStructStub) bean;
        } else if(bean instanceof CloudStructBean && ((CloudStructBean) bean).isWrapper()) {
            stub = ((CloudStructBean) bean).getStub();
        } else {
            stub = CloudStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ImageStructBean convertImageStructToBean(ImageStruct stub)
    {
        ImageStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ImageStructBean();
        } else if(stub instanceof ImageStructBean) {
        	bean = (ImageStructBean) stub;
        } else {
        	bean = new ImageStructBean(stub);
        }
        return bean;
    }
    public static List<ImageStructBean> convertImageStructListToBeanList(List<ImageStruct> stubList)
    {
        List<ImageStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ImageStruct>();
        } else {
            beanList = new ArrayList<ImageStructBean>();
            for(ImageStruct stub : stubList) {
                beanList.add(convertImageStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ImageStruct> convertImageStructListStubToBeanList(ImageStructListStub listStub)
    {
        return convertImageStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ImageStruct> convertImageStructListStubToBeanList(ImageStructListStub listStub, StringCursor forwardCursor)
    {
        List<ImageStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ImageStruct>();
        } else {
            beanList = new ArrayList<ImageStruct>();
            for(ImageStructStub stub : listStub.getList()) {
            	beanList.add(convertImageStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ImageStructStub convertImageStructToStub(ImageStruct bean)
    {
        ImageStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ImageStructStub();
        } else if(bean instanceof ImageStructStub) {
            stub = (ImageStructStub) bean;
        } else if(bean instanceof ImageStructBean && ((ImageStructBean) bean).isWrapper()) {
            stub = ((ImageStructBean) bean).getStub();
        } else {
            stub = ImageStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TextInputStructBean convertTextInputStructToBean(TextInputStruct stub)
    {
        TextInputStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TextInputStructBean();
        } else if(stub instanceof TextInputStructBean) {
        	bean = (TextInputStructBean) stub;
        } else {
        	bean = new TextInputStructBean(stub);
        }
        return bean;
    }
    public static List<TextInputStructBean> convertTextInputStructListToBeanList(List<TextInputStruct> stubList)
    {
        List<TextInputStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TextInputStruct>();
        } else {
            beanList = new ArrayList<TextInputStructBean>();
            for(TextInputStruct stub : stubList) {
                beanList.add(convertTextInputStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TextInputStruct> convertTextInputStructListStubToBeanList(TextInputStructListStub listStub)
    {
        return convertTextInputStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TextInputStruct> convertTextInputStructListStubToBeanList(TextInputStructListStub listStub, StringCursor forwardCursor)
    {
        List<TextInputStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TextInputStruct>();
        } else {
            beanList = new ArrayList<TextInputStruct>();
            for(TextInputStructStub stub : listStub.getList()) {
            	beanList.add(convertTextInputStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TextInputStructStub convertTextInputStructToStub(TextInputStruct bean)
    {
        TextInputStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TextInputStructStub();
        } else if(bean instanceof TextInputStructStub) {
            stub = (TextInputStructStub) bean;
        } else if(bean instanceof TextInputStructBean && ((TextInputStructBean) bean).isWrapper()) {
            stub = ((TextInputStructBean) bean).getStub();
        } else {
            stub = TextInputStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static EnclosureStructBean convertEnclosureStructToBean(EnclosureStruct stub)
    {
        EnclosureStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new EnclosureStructBean();
        } else if(stub instanceof EnclosureStructBean) {
        	bean = (EnclosureStructBean) stub;
        } else {
        	bean = new EnclosureStructBean(stub);
        }
        return bean;
    }
    public static List<EnclosureStructBean> convertEnclosureStructListToBeanList(List<EnclosureStruct> stubList)
    {
        List<EnclosureStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<EnclosureStruct>();
        } else {
            beanList = new ArrayList<EnclosureStructBean>();
            for(EnclosureStruct stub : stubList) {
                beanList.add(convertEnclosureStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<EnclosureStruct> convertEnclosureStructListStubToBeanList(EnclosureStructListStub listStub)
    {
        return convertEnclosureStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<EnclosureStruct> convertEnclosureStructListStubToBeanList(EnclosureStructListStub listStub, StringCursor forwardCursor)
    {
        List<EnclosureStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<EnclosureStruct>();
        } else {
            beanList = new ArrayList<EnclosureStruct>();
            for(EnclosureStructStub stub : listStub.getList()) {
            	beanList.add(convertEnclosureStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static EnclosureStructStub convertEnclosureStructToStub(EnclosureStruct bean)
    {
        EnclosureStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new EnclosureStructStub();
        } else if(bean instanceof EnclosureStructStub) {
            stub = (EnclosureStructStub) bean;
        } else if(bean instanceof EnclosureStructBean && ((EnclosureStructBean) bean).isWrapper()) {
            stub = ((EnclosureStructBean) bean).getStub();
        } else {
            stub = EnclosureStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RssItemBean convertRssItemToBean(RssItem stub)
    {
        RssItemBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RssItemBean();
        } else if(stub instanceof RssItemBean) {
        	bean = (RssItemBean) stub;
        } else {
        	bean = new RssItemBean(stub);
        }
        return bean;
    }
    public static List<RssItemBean> convertRssItemListToBeanList(List<RssItem> stubList)
    {
        List<RssItemBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RssItem>();
        } else {
            beanList = new ArrayList<RssItemBean>();
            for(RssItem stub : stubList) {
                beanList.add(convertRssItemToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RssItem> convertRssItemListStubToBeanList(RssItemListStub listStub)
    {
        return convertRssItemListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<RssItem> convertRssItemListStubToBeanList(RssItemListStub listStub, StringCursor forwardCursor)
    {
        List<RssItem> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RssItem>();
        } else {
            beanList = new ArrayList<RssItem>();
            for(RssItemStub stub : listStub.getList()) {
            	beanList.add(convertRssItemToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static RssItemStub convertRssItemToStub(RssItem bean)
    {
        RssItemStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RssItemStub();
        } else if(bean instanceof RssItemStub) {
            stub = (RssItemStub) bean;
        } else if(bean instanceof RssItemBean && ((RssItemBean) bean).isWrapper()) {
            stub = ((RssItemBean) bean).getStub();
        } else {
            stub = RssItemStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RssChannelBean convertRssChannelToBean(RssChannel stub)
    {
        RssChannelBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RssChannelBean();
        } else if(stub instanceof RssChannelBean) {
        	bean = (RssChannelBean) stub;
        } else {
        	bean = new RssChannelBean(stub);
        }
        return bean;
    }
    public static List<RssChannelBean> convertRssChannelListToBeanList(List<RssChannel> stubList)
    {
        List<RssChannelBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RssChannel>();
        } else {
            beanList = new ArrayList<RssChannelBean>();
            for(RssChannel stub : stubList) {
                beanList.add(convertRssChannelToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RssChannel> convertRssChannelListStubToBeanList(RssChannelListStub listStub)
    {
        return convertRssChannelListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<RssChannel> convertRssChannelListStubToBeanList(RssChannelListStub listStub, StringCursor forwardCursor)
    {
        List<RssChannel> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RssChannel>();
        } else {
            beanList = new ArrayList<RssChannel>();
            for(RssChannelStub stub : listStub.getList()) {
            	beanList.add(convertRssChannelToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static RssChannelStub convertRssChannelToStub(RssChannel bean)
    {
        RssChannelStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RssChannelStub();
        } else if(bean instanceof RssChannelStub) {
            stub = (RssChannelStub) bean;
        } else if(bean instanceof RssChannelBean && ((RssChannelBean) bean).isWrapper()) {
            stub = ((RssChannelBean) bean).getStub();
        } else {
            stub = RssChannelStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ChannelFeedBean convertChannelFeedToBean(ChannelFeed stub)
    {
        ChannelFeedBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ChannelFeedBean();
        } else if(stub instanceof ChannelFeedBean) {
        	bean = (ChannelFeedBean) stub;
        } else {
        	bean = new ChannelFeedBean(stub);
        }
        return bean;
    }
    public static List<ChannelFeedBean> convertChannelFeedListToBeanList(List<ChannelFeed> stubList)
    {
        List<ChannelFeedBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ChannelFeed>();
        } else {
            beanList = new ArrayList<ChannelFeedBean>();
            for(ChannelFeed stub : stubList) {
                beanList.add(convertChannelFeedToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ChannelFeed> convertChannelFeedListStubToBeanList(ChannelFeedListStub listStub)
    {
        return convertChannelFeedListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ChannelFeed> convertChannelFeedListStubToBeanList(ChannelFeedListStub listStub, StringCursor forwardCursor)
    {
        List<ChannelFeed> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ChannelFeed>();
        } else {
            beanList = new ArrayList<ChannelFeed>();
            for(ChannelFeedStub stub : listStub.getList()) {
            	beanList.add(convertChannelFeedToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ChannelFeedStub convertChannelFeedToStub(ChannelFeed bean)
    {
        ChannelFeedStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ChannelFeedStub();
        } else if(bean instanceof ChannelFeedStub) {
            stub = (ChannelFeedStub) bean;
        } else if(bean instanceof ChannelFeedBean && ((ChannelFeedBean) bean).isWrapper()) {
            stub = ((ChannelFeedBean) bean).getStub();
        } else {
            stub = ChannelFeedStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FeedItemBean convertFeedItemToBean(FeedItem stub)
    {
        FeedItemBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FeedItemBean();
        } else if(stub instanceof FeedItemBean) {
        	bean = (FeedItemBean) stub;
        } else {
        	bean = new FeedItemBean(stub);
        }
        return bean;
    }
    public static List<FeedItemBean> convertFeedItemListToBeanList(List<FeedItem> stubList)
    {
        List<FeedItemBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FeedItem>();
        } else {
            beanList = new ArrayList<FeedItemBean>();
            for(FeedItem stub : stubList) {
                beanList.add(convertFeedItemToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FeedItem> convertFeedItemListStubToBeanList(FeedItemListStub listStub)
    {
        return convertFeedItemListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FeedItem> convertFeedItemListStubToBeanList(FeedItemListStub listStub, StringCursor forwardCursor)
    {
        List<FeedItem> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FeedItem>();
        } else {
            beanList = new ArrayList<FeedItem>();
            for(FeedItemStub stub : listStub.getList()) {
            	beanList.add(convertFeedItemToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FeedItemStub convertFeedItemToStub(FeedItem bean)
    {
        FeedItemStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FeedItemStub();
        } else if(bean instanceof FeedItemStub) {
            stub = (FeedItemStub) bean;
        } else if(bean instanceof FeedItemBean && ((FeedItemBean) bean).isWrapper()) {
            stub = ((FeedItemBean) bean).getStub();
        } else {
            stub = FeedItemStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FeedContentBean convertFeedContentToBean(FeedContent stub)
    {
        FeedContentBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FeedContentBean();
        } else if(stub instanceof FeedContentBean) {
        	bean = (FeedContentBean) stub;
        } else {
        	bean = new FeedContentBean(stub);
        }
        return bean;
    }
    public static List<FeedContentBean> convertFeedContentListToBeanList(List<FeedContent> stubList)
    {
        List<FeedContentBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FeedContent>();
        } else {
            beanList = new ArrayList<FeedContentBean>();
            for(FeedContent stub : stubList) {
                beanList.add(convertFeedContentToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FeedContent> convertFeedContentListStubToBeanList(FeedContentListStub listStub)
    {
        return convertFeedContentListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FeedContent> convertFeedContentListStubToBeanList(FeedContentListStub listStub, StringCursor forwardCursor)
    {
        List<FeedContent> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FeedContent>();
        } else {
            beanList = new ArrayList<FeedContent>();
            for(FeedContentStub stub : listStub.getList()) {
            	beanList.add(convertFeedContentToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FeedContentStub convertFeedContentToStub(FeedContent bean)
    {
        FeedContentStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FeedContentStub();
        } else if(bean instanceof FeedContentStub) {
            stub = (FeedContentStub) bean;
        } else if(bean instanceof FeedContentBean && ((FeedContentBean) bean).isWrapper()) {
            stub = ((FeedContentBean) bean).getStub();
        } else {
            stub = FeedContentStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RedirectRuleBean convertRedirectRuleToBean(RedirectRule stub)
    {
        RedirectRuleBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RedirectRuleBean();
        } else if(stub instanceof RedirectRuleBean) {
        	bean = (RedirectRuleBean) stub;
        } else {
        	bean = new RedirectRuleBean(stub);
        }
        return bean;
    }
    public static List<RedirectRuleBean> convertRedirectRuleListToBeanList(List<RedirectRule> stubList)
    {
        List<RedirectRuleBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RedirectRule>();
        } else {
            beanList = new ArrayList<RedirectRuleBean>();
            for(RedirectRule stub : stubList) {
                beanList.add(convertRedirectRuleToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RedirectRule> convertRedirectRuleListStubToBeanList(RedirectRuleListStub listStub)
    {
        return convertRedirectRuleListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<RedirectRule> convertRedirectRuleListStubToBeanList(RedirectRuleListStub listStub, StringCursor forwardCursor)
    {
        List<RedirectRule> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RedirectRule>();
        } else {
            beanList = new ArrayList<RedirectRule>();
            for(RedirectRuleStub stub : listStub.getList()) {
            	beanList.add(convertRedirectRuleToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static RedirectRuleStub convertRedirectRuleToStub(RedirectRule bean)
    {
        RedirectRuleStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RedirectRuleStub();
        } else if(bean instanceof RedirectRuleStub) {
            stub = (RedirectRuleStub) bean;
        } else if(bean instanceof RedirectRuleBean && ((RedirectRuleBean) bean).isWrapper()) {
            stub = ((RedirectRuleBean) bean).getStub();
        } else {
            stub = RedirectRuleStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static HelpNoticeBean convertHelpNoticeToBean(HelpNotice stub)
    {
        HelpNoticeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new HelpNoticeBean();
        } else if(stub instanceof HelpNoticeBean) {
        	bean = (HelpNoticeBean) stub;
        } else {
        	bean = new HelpNoticeBean(stub);
        }
        return bean;
    }
    public static List<HelpNoticeBean> convertHelpNoticeListToBeanList(List<HelpNotice> stubList)
    {
        List<HelpNoticeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<HelpNotice>();
        } else {
            beanList = new ArrayList<HelpNoticeBean>();
            for(HelpNotice stub : stubList) {
                beanList.add(convertHelpNoticeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<HelpNotice> convertHelpNoticeListStubToBeanList(HelpNoticeListStub listStub)
    {
        return convertHelpNoticeListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<HelpNotice> convertHelpNoticeListStubToBeanList(HelpNoticeListStub listStub, StringCursor forwardCursor)
    {
        List<HelpNotice> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<HelpNotice>();
        } else {
            beanList = new ArrayList<HelpNotice>();
            for(HelpNoticeStub stub : listStub.getList()) {
            	beanList.add(convertHelpNoticeToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static HelpNoticeStub convertHelpNoticeToStub(HelpNotice bean)
    {
        HelpNoticeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new HelpNoticeStub();
        } else if(bean instanceof HelpNoticeStub) {
            stub = (HelpNoticeStub) bean;
        } else if(bean instanceof HelpNoticeBean && ((HelpNoticeBean) bean).isWrapper()) {
            stub = ((HelpNoticeBean) bean).getStub();
        } else {
            stub = HelpNoticeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static SiteLogBean convertSiteLogToBean(SiteLog stub)
    {
        SiteLogBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new SiteLogBean();
        } else if(stub instanceof SiteLogBean) {
        	bean = (SiteLogBean) stub;
        } else {
        	bean = new SiteLogBean(stub);
        }
        return bean;
    }
    public static List<SiteLogBean> convertSiteLogListToBeanList(List<SiteLog> stubList)
    {
        List<SiteLogBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<SiteLog>();
        } else {
            beanList = new ArrayList<SiteLogBean>();
            for(SiteLog stub : stubList) {
                beanList.add(convertSiteLogToBean(stub));
            }
        }
        return beanList;
    }
    public static List<SiteLog> convertSiteLogListStubToBeanList(SiteLogListStub listStub)
    {
        return convertSiteLogListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<SiteLog> convertSiteLogListStubToBeanList(SiteLogListStub listStub, StringCursor forwardCursor)
    {
        List<SiteLog> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<SiteLog>();
        } else {
            beanList = new ArrayList<SiteLog>();
            for(SiteLogStub stub : listStub.getList()) {
            	beanList.add(convertSiteLogToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static SiteLogStub convertSiteLogToStub(SiteLog bean)
    {
        SiteLogStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new SiteLogStub();
        } else if(bean instanceof SiteLogStub) {
            stub = (SiteLogStub) bean;
        } else if(bean instanceof SiteLogBean && ((SiteLogBean) bean).isWrapper()) {
            stub = ((SiteLogBean) bean).getStub();
        } else {
            stub = SiteLogStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceInfoBean convertServiceInfoToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceInfoBean();
        } else if(stub instanceof ServiceInfoBean) {
        	bean = (ServiceInfoBean) stub;
        } else {
        	bean = new ServiceInfoBean(stub);
        }
        return bean;
    }
    public static List<ServiceInfoBean> convertServiceInfoListToBeanList(List<ServiceInfo> stubList)
    {
        List<ServiceInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfoBean>();
            for(ServiceInfo stub : stubList) {
                beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub)
    {
        return convertServiceInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub, StringCursor forwardCursor)
    {
        List<ServiceInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : listStub.getList()) {
            	beanList.add(convertServiceInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceInfoStub convertServiceInfoToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceInfoStub();
        } else if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else if(bean instanceof ServiceInfoBean && ((ServiceInfoBean) bean).isWrapper()) {
            stub = ((ServiceInfoBean) bean).getStub();
        } else {
            stub = ServiceInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FiveTenBean convertFiveTenToBean(FiveTen stub)
    {
        FiveTenBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FiveTenBean();
        } else if(stub instanceof FiveTenBean) {
        	bean = (FiveTenBean) stub;
        } else {
        	bean = new FiveTenBean(stub);
        }
        return bean;
    }
    public static List<FiveTenBean> convertFiveTenListToBeanList(List<FiveTen> stubList)
    {
        List<FiveTenBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTenBean>();
            for(FiveTen stub : stubList) {
                beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub)
    {
        return convertFiveTenListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub, StringCursor forwardCursor)
    {
        List<FiveTen> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTen>();
            for(FiveTenStub stub : listStub.getList()) {
            	beanList.add(convertFiveTenToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FiveTenStub convertFiveTenToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FiveTenStub();
        } else if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else if(bean instanceof FiveTenBean && ((FiveTenBean) bean).isWrapper()) {
            stub = ((FiveTenBean) bean).getStub();
        } else {
            stub = FiveTenStub.convertBeanToStub(bean);
        }
        return stub;
    }

    
}
