package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.stub.TextInputStructStub;


// Wrapper class + bean combo.
public class TextInputStructBean implements TextInputStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TextInputStructBean.class.getName());

    // [1] With an embedded object.
    private TextInputStructStub stub = null;

    // [2] Or, without an embedded object.
    private String title;
    private String name;
    private String link;
    private String description;

    // Ctors.
    public TextInputStructBean()
    {
        //this((String) null);
    }
    public TextInputStructBean(String title, String name, String link, String description)
    {
        this.title = title;
        this.name = name;
        this.link = link;
        this.description = description;
    }
    public TextInputStructBean(TextInputStruct stub)
    {
        if(stub instanceof TextInputStructStub) {
            this.stub = (TextInputStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setTitle(stub.getTitle());   
            setName(stub.getName());   
            setLink(stub.getLink());   
            setDescription(stub.getDescription());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getLink()
    {
        if(getStub() != null) {
            return getStub().getLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.link;
        }
    }
    public void setLink(String link)
    {
        if(getStub() != null) {
            getStub().setLink(link);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.link = link;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public TextInputStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(TextInputStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("title = " + this.title).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("link = " + this.link).append(";");
            sb.append("description = " + this.description).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = link == null ? 0 : link.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
