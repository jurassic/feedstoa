package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.stub.UriStructStub;


// Wrapper class + bean combo.
public class UriStructBean implements UriStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UriStructBean.class.getName());

    // [1] With an embedded object.
    private UriStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String href;
    private String rel;
    private String type;
    private String label;

    // Ctors.
    public UriStructBean()
    {
        //this((String) null);
    }
    public UriStructBean(String uuid, String href, String rel, String type, String label)
    {
        this.uuid = uuid;
        this.href = href;
        this.rel = rel;
        this.type = type;
        this.label = label;
    }
    public UriStructBean(UriStruct stub)
    {
        if(stub instanceof UriStructStub) {
            this.stub = (UriStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setHref(stub.getHref());   
            setRel(stub.getRel());   
            setType(stub.getType());   
            setLabel(stub.getLabel());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getHref()
    {
        if(getStub() != null) {
            return getStub().getHref();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.href;
        }
    }
    public void setHref(String href)
    {
        if(getStub() != null) {
            getStub().setHref(href);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.href = href;
        }
    }

    public String getRel()
    {
        if(getStub() != null) {
            return getStub().getRel();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rel;
        }
    }
    public void setRel(String rel)
    {
        if(getStub() != null) {
            getStub().setRel(rel);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rel = rel;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getLabel()
    {
        if(getStub() != null) {
            return getStub().getLabel();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.label;
        }
    }
    public void setLabel(String label)
    {
        if(getStub() != null) {
            getStub().setLabel(label);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.label = label;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLabel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public UriStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UriStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("href = " + this.href).append(";");
            sb.append("rel = " + this.rel).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("label = " + this.label).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = href == null ? 0 : href.hashCode();
            _hash = 31 * _hash + delta;
            delta = rel == null ? 0 : rel.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = label == null ? 0 : label.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
