package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.stub.ImageStructStub;


// Wrapper class + bean combo.
public class ImageStructBean implements ImageStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageStructBean.class.getName());

    // [1] With an embedded object.
    private ImageStructStub stub = null;

    // [2] Or, without an embedded object.
    private String url;
    private String title;
    private String link;
    private Integer width;
    private Integer height;
    private String description;

    // Ctors.
    public ImageStructBean()
    {
        //this((String) null);
    }
    public ImageStructBean(String url, String title, String link, Integer width, Integer height, String description)
    {
        this.url = url;
        this.title = title;
        this.link = link;
        this.width = width;
        this.height = height;
        this.description = description;
    }
    public ImageStructBean(ImageStruct stub)
    {
        if(stub instanceof ImageStructStub) {
            this.stub = (ImageStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUrl(stub.getUrl());   
            setTitle(stub.getTitle());   
            setLink(stub.getLink());   
            setWidth(stub.getWidth());   
            setHeight(stub.getHeight());   
            setDescription(stub.getDescription());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getLink()
    {
        if(getStub() != null) {
            return getStub().getLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.link;
        }
    }
    public void setLink(String link)
    {
        if(getStub() != null) {
            getStub().setLink(link);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.link = link;
        }
    }

    public Integer getWidth()
    {
        if(getStub() != null) {
            return getStub().getWidth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.width;
        }
    }
    public void setWidth(Integer width)
    {
        if(getStub() != null) {
            getStub().setWidth(width);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.width = width;
        }
    }

    public Integer getHeight()
    {
        if(getStub() != null) {
            return getStub().getHeight();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.height;
        }
    }
    public void setHeight(Integer height)
    {
        if(getStub() != null) {
            getStub().setHeight(height);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.height = height;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ImageStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ImageStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("url = " + this.url).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("link = " + this.link).append(";");
            sb.append("width = " + this.width).append(";");
            sb.append("height = " + this.height).append(";");
            sb.append("description = " + this.description).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = link == null ? 0 : link.hashCode();
            _hash = 31 * _hash + delta;
            delta = width == null ? 0 : width.hashCode();
            _hash = 31 * _hash + delta;
            delta = height == null ? 0 : height.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
