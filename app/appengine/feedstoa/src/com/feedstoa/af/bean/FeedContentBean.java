package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.stub.FeedContentStub;


// Wrapper class + bean combo.
public class FeedContentBean implements FeedContent, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedContentBean.class.getName());

    // [1] With an embedded object.
    private FeedContentStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String channelSource;
    private String channelFeed;
    private String channelVersion;
    private String feedUrl;
    private String feedFormat;
    private String content;
    private String contentHash;
    private String status;
    private String note;
    private String lastBuildDate;
    private Long lastBuildTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FeedContentBean()
    {
        //this((String) null);
    }
    public FeedContentBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FeedContentBean(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime)
    {
        this(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime, null, null);
    }
    public FeedContentBean(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.channelSource = channelSource;
        this.channelFeed = channelFeed;
        this.channelVersion = channelVersion;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.content = content;
        this.contentHash = contentHash;
        this.status = status;
        this.note = note;
        this.lastBuildDate = lastBuildDate;
        this.lastBuildTime = lastBuildTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FeedContentBean(FeedContent stub)
    {
        if(stub instanceof FeedContentStub) {
            this.stub = (FeedContentStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setChannelSource(stub.getChannelSource());   
            setChannelFeed(stub.getChannelFeed());   
            setChannelVersion(stub.getChannelVersion());   
            setFeedUrl(stub.getFeedUrl());   
            setFeedFormat(stub.getFeedFormat());   
            setContent(stub.getContent());   
            setContentHash(stub.getContentHash());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setLastBuildDate(stub.getLastBuildDate());   
            setLastBuildTime(stub.getLastBuildTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getChannelSource()
    {
        if(getStub() != null) {
            return getStub().getChannelSource();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelSource;
        }
    }
    public void setChannelSource(String channelSource)
    {
        if(getStub() != null) {
            getStub().setChannelSource(channelSource);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelSource = channelSource;
        }
    }

    public String getChannelFeed()
    {
        if(getStub() != null) {
            return getStub().getChannelFeed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelFeed;
        }
    }
    public void setChannelFeed(String channelFeed)
    {
        if(getStub() != null) {
            getStub().setChannelFeed(channelFeed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelFeed = channelFeed;
        }
    }

    public String getChannelVersion()
    {
        if(getStub() != null) {
            return getStub().getChannelVersion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelVersion;
        }
    }
    public void setChannelVersion(String channelVersion)
    {
        if(getStub() != null) {
            getStub().setChannelVersion(channelVersion);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelVersion = channelVersion;
        }
    }

    public String getFeedUrl()
    {
        if(getStub() != null) {
            return getStub().getFeedUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedUrl;
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getStub() != null) {
            getStub().setFeedUrl(feedUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedUrl = feedUrl;
        }
    }

    public String getFeedFormat()
    {
        if(getStub() != null) {
            return getStub().getFeedFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedFormat;
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getStub() != null) {
            getStub().setFeedFormat(feedFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedFormat = feedFormat;
        }
    }

    public String getContent()
    {
        if(getStub() != null) {
            return getStub().getContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.content;
        }
    }
    public void setContent(String content)
    {
        if(getStub() != null) {
            getStub().setContent(content);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.content = content;
        }
    }

    public String getContentHash()
    {
        if(getStub() != null) {
            return getStub().getContentHash();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contentHash;
        }
    }
    public void setContentHash(String contentHash)
    {
        if(getStub() != null) {
            getStub().setContentHash(contentHash);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.contentHash = contentHash;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getLastBuildDate()
    {
        if(getStub() != null) {
            return getStub().getLastBuildDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastBuildDate;
        }
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        if(getStub() != null) {
            getStub().setLastBuildDate(lastBuildDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastBuildDate = lastBuildDate;
        }
    }

    public Long getLastBuildTime()
    {
        if(getStub() != null) {
            return getStub().getLastBuildTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastBuildTime;
        }
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        if(getStub() != null) {
            getStub().setLastBuildTime(lastBuildTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastBuildTime = lastBuildTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FeedContentStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FeedContentStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("channelSource = " + this.channelSource).append(";");
            sb.append("channelFeed = " + this.channelFeed).append(";");
            sb.append("channelVersion = " + this.channelVersion).append(";");
            sb.append("feedUrl = " + this.feedUrl).append(";");
            sb.append("feedFormat = " + this.feedFormat).append(";");
            sb.append("content = " + this.content).append(";");
            sb.append("contentHash = " + this.contentHash).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("lastBuildDate = " + this.lastBuildDate).append(";");
            sb.append("lastBuildTime = " + this.lastBuildTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelSource == null ? 0 : channelSource.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelFeed == null ? 0 : channelFeed.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelVersion == null ? 0 : channelVersion.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedUrl == null ? 0 : feedUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedFormat == null ? 0 : feedFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = content == null ? 0 : content.hashCode();
            _hash = 31 * _hash + delta;
            delta = contentHash == null ? 0 : contentHash.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastBuildTime == null ? 0 : lastBuildTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
