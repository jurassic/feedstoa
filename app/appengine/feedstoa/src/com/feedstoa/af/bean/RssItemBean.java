package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.stub.EnclosureStructStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.stub.RssItemStub;


// Wrapper class + bean combo.
public class RssItemBean implements RssItem, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssItemBean.class.getName());

    // [1] With an embedded object.
    private RssItemStub stub = null;

    // [2] Or, without an embedded object.
    private String title;
    private String link;
    private String description;
    private String author;
    private List<CategoryStruct> category;
    private String comments;
    private EnclosureStructBean enclosure;
    private String guid;
    private String pubDate;
    private UriStructBean source;

    // Ctors.
    public RssItemBean()
    {
        //this((String) null);
    }
    public RssItemBean(String title, String link, String description, String author, List<CategoryStruct> category, String comments, EnclosureStructBean enclosure, String guid, String pubDate, UriStructBean source)
    {
        this.title = title;
        this.link = link;
        this.description = description;
        this.author = author;
        this.category = category;
        this.comments = comments;
        this.enclosure = enclosure;
        this.guid = guid;
        this.pubDate = pubDate;
        this.source = source;
    }
    public RssItemBean(RssItem stub)
    {
        if(stub instanceof RssItemStub) {
            this.stub = (RssItemStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setTitle(stub.getTitle());   
            setLink(stub.getLink());   
            setDescription(stub.getDescription());   
            setAuthor(stub.getAuthor());   
            setCategory(stub.getCategory());   
            setComments(stub.getComments());   
            EnclosureStruct enclosure = stub.getEnclosure();
            if(enclosure instanceof EnclosureStructBean) {
                setEnclosure((EnclosureStructBean) enclosure);   
            } else {
                setEnclosure(new EnclosureStructBean(enclosure));   
            }
            setGuid(stub.getGuid());   
            setPubDate(stub.getPubDate());   
            UriStruct source = stub.getSource();
            if(source instanceof UriStructBean) {
                setSource((UriStructBean) source);   
            } else {
                setSource(new UriStructBean(source));   
            }
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getLink()
    {
        if(getStub() != null) {
            return getStub().getLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.link;
        }
    }
    public void setLink(String link)
    {
        if(getStub() != null) {
            getStub().setLink(link);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.link = link;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getAuthor()
    {
        if(getStub() != null) {
            return getStub().getAuthor();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.author;
        }
    }
    public void setAuthor(String author)
    {
        if(getStub() != null) {
            getStub().setAuthor(author);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.author = author;
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getStub() != null) {
            List<CategoryStruct> list = getStub().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructStub) {
                        elem = new CategoryStructBean(categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.category;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getStub() != null) {
                List<CategoryStruct> stub = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructStub elem = null;
                    if(categoryStruct instanceof CategoryStructStub) {
                        elem = (CategoryStructStub) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).getStub();
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructStub(categoryStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setCategory(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.category = category;  // ???

                List<CategoryStruct> beans = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructBean elem = null;
                    if(categoryStruct != null) {
                        if(categoryStruct instanceof CategoryStructBean) {
                            elem = (CategoryStructBean) categoryStruct;
                        } else {
                            elem = new CategoryStructBean(categoryStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.category = beans;
            }
        } else {
            this.category = null;
        }
    }

    public String getComments()
    {
        if(getStub() != null) {
            return getStub().getComments();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.comments;
        }
    }
    public void setComments(String comments)
    {
        if(getStub() != null) {
            getStub().setComments(comments);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.comments = comments;
        }
    }

    public EnclosureStruct getEnclosure()
    {  
        if(getStub() != null) {
            // Note the object type.
            EnclosureStruct _stub_field = getStub().getEnclosure();
            if(_stub_field == null) {
                return null;
            } else {
                return new EnclosureStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.enclosure;
        }
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setEnclosure(enclosure);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(enclosure == null) {
                this.enclosure = null;
            } else {
                if(enclosure instanceof EnclosureStructBean) {
                    this.enclosure = (EnclosureStructBean) enclosure;
                } else {
                    this.enclosure = new EnclosureStructBean(enclosure);
                }
            }
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getPubDate()
    {
        if(getStub() != null) {
            return getStub().getPubDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pubDate;
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getStub() != null) {
            getStub().setPubDate(pubDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pubDate = pubDate;
        }
    }

    public UriStruct getSource()
    {  
        if(getStub() != null) {
            // Note the object type.
            UriStruct _stub_field = getStub().getSource();
            if(_stub_field == null) {
                return null;
            } else {
                return new UriStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source;
        }
    }
    public void setSource(UriStruct source)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource(source);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source == null) {
                this.source = null;
            } else {
                if(source instanceof UriStructBean) {
                    this.source = (UriStructBean) source;
                } else {
                    this.source = new UriStructBean(source);
                }
            }
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAuthor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getComments() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEnclosure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGuid() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public RssItemStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RssItemStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("title = " + this.title).append(";");
            sb.append("link = " + this.link).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("author = " + this.author).append(";");
            sb.append("category = " + this.category).append(";");
            sb.append("comments = " + this.comments).append(";");
            sb.append("enclosure = " + this.enclosure).append(";");
            sb.append("guid = " + this.guid).append(";");
            sb.append("pubDate = " + this.pubDate).append(";");
            sb.append("source = " + this.source).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = link == null ? 0 : link.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = author == null ? 0 : author.hashCode();
            _hash = 31 * _hash + delta;
            delta = category == null ? 0 : category.hashCode();
            _hash = 31 * _hash + delta;
            delta = comments == null ? 0 : comments.hashCode();
            _hash = 31 * _hash + delta;
            delta = enclosure == null ? 0 : enclosure.hashCode();
            _hash = 31 * _hash + delta;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = pubDate == null ? 0 : pubDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = source == null ? 0 : source.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
