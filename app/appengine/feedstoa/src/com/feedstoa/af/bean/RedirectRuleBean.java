package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.stub.RedirectRuleStub;


// Wrapper class + bean combo.
public class RedirectRuleBean implements RedirectRule, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RedirectRuleBean.class.getName());

    // [1] With an embedded object.
    private RedirectRuleStub stub = null;

    // [2] Or, without an embedded object.
    private String redirectType;
    private Double precedence;
    private String sourceDomain;
    private String sourcePath;
    private String targetDomain;
    private String targetPath;
    private String note;
    private String status;

    // Ctors.
    public RedirectRuleBean()
    {
        //this((String) null);
    }
    public RedirectRuleBean(String redirectType, Double precedence, String sourceDomain, String sourcePath, String targetDomain, String targetPath, String note, String status)
    {
        this.redirectType = redirectType;
        this.precedence = precedence;
        this.sourceDomain = sourceDomain;
        this.sourcePath = sourcePath;
        this.targetDomain = targetDomain;
        this.targetPath = targetPath;
        this.note = note;
        this.status = status;
    }
    public RedirectRuleBean(RedirectRule stub)
    {
        if(stub instanceof RedirectRuleStub) {
            this.stub = (RedirectRuleStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setRedirectType(stub.getRedirectType());   
            setPrecedence(stub.getPrecedence());   
            setSourceDomain(stub.getSourceDomain());   
            setSourcePath(stub.getSourcePath());   
            setTargetDomain(stub.getTargetDomain());   
            setTargetPath(stub.getTargetPath());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getRedirectType()
    {
        if(getStub() != null) {
            return getStub().getRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectType;
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getStub() != null) {
            getStub().setRedirectType(redirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirectType = redirectType;
        }
    }

    public Double getPrecedence()
    {
        if(getStub() != null) {
            return getStub().getPrecedence();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.precedence;
        }
    }
    public void setPrecedence(Double precedence)
    {
        if(getStub() != null) {
            getStub().setPrecedence(precedence);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.precedence = precedence;
        }
    }

    public String getSourceDomain()
    {
        if(getStub() != null) {
            return getStub().getSourceDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sourceDomain;
        }
    }
    public void setSourceDomain(String sourceDomain)
    {
        if(getStub() != null) {
            getStub().setSourceDomain(sourceDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sourceDomain = sourceDomain;
        }
    }

    public String getSourcePath()
    {
        if(getStub() != null) {
            return getStub().getSourcePath();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sourcePath;
        }
    }
    public void setSourcePath(String sourcePath)
    {
        if(getStub() != null) {
            getStub().setSourcePath(sourcePath);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sourcePath = sourcePath;
        }
    }

    public String getTargetDomain()
    {
        if(getStub() != null) {
            return getStub().getTargetDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.targetDomain;
        }
    }
    public void setTargetDomain(String targetDomain)
    {
        if(getStub() != null) {
            getStub().setTargetDomain(targetDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.targetDomain = targetDomain;
        }
    }

    public String getTargetPath()
    {
        if(getStub() != null) {
            return getStub().getTargetPath();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.targetPath;
        }
    }
    public void setTargetPath(String targetPath)
    {
        if(getStub() != null) {
            getStub().setTargetPath(targetPath);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.targetPath = targetPath;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrecedence() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourceDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourcePath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public RedirectRuleStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RedirectRuleStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("redirectType = " + this.redirectType).append(";");
            sb.append("precedence = " + this.precedence).append(";");
            sb.append("sourceDomain = " + this.sourceDomain).append(";");
            sb.append("sourcePath = " + this.sourcePath).append(";");
            sb.append("targetDomain = " + this.targetDomain).append(";");
            sb.append("targetPath = " + this.targetPath).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = redirectType == null ? 0 : redirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = precedence == null ? 0 : precedence.hashCode();
            _hash = 31 * _hash + delta;
            delta = sourceDomain == null ? 0 : sourceDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = sourcePath == null ? 0 : sourcePath.hashCode();
            _hash = 31 * _hash + delta;
            delta = targetDomain == null ? 0 : targetDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = targetPath == null ? 0 : targetPath.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
