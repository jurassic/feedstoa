package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.stub.CloudStructStub;


// Wrapper class + bean combo.
public class CloudStructBean implements CloudStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CloudStructBean.class.getName());

    // [1] With an embedded object.
    private CloudStructStub stub = null;

    // [2] Or, without an embedded object.
    private String domain;
    private Integer port;
    private String path;
    private String registerProcedure;
    private String protocol;

    // Ctors.
    public CloudStructBean()
    {
        //this((String) null);
    }
    public CloudStructBean(String domain, Integer port, String path, String registerProcedure, String protocol)
    {
        this.domain = domain;
        this.port = port;
        this.path = path;
        this.registerProcedure = registerProcedure;
        this.protocol = protocol;
    }
    public CloudStructBean(CloudStruct stub)
    {
        if(stub instanceof CloudStructStub) {
            this.stub = (CloudStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setDomain(stub.getDomain());   
            setPort(stub.getPort());   
            setPath(stub.getPath());   
            setRegisterProcedure(stub.getRegisterProcedure());   
            setProtocol(stub.getProtocol());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public Integer getPort()
    {
        if(getStub() != null) {
            return getStub().getPort();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.port;
        }
    }
    public void setPort(Integer port)
    {
        if(getStub() != null) {
            getStub().setPort(port);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.port = port;
        }
    }

    public String getPath()
    {
        if(getStub() != null) {
            return getStub().getPath();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.path;
        }
    }
    public void setPath(String path)
    {
        if(getStub() != null) {
            getStub().setPath(path);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.path = path;
        }
    }

    public String getRegisterProcedure()
    {
        if(getStub() != null) {
            return getStub().getRegisterProcedure();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.registerProcedure;
        }
    }
    public void setRegisterProcedure(String registerProcedure)
    {
        if(getStub() != null) {
            getStub().setRegisterProcedure(registerProcedure);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.registerProcedure = registerProcedure;
        }
    }

    public String getProtocol()
    {
        if(getStub() != null) {
            return getStub().getProtocol();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.protocol;
        }
    }
    public void setProtocol(String protocol)
    {
        if(getStub() != null) {
            getStub().setProtocol(protocol);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.protocol = protocol;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPort() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegisterProcedure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProtocol() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public CloudStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(CloudStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("domain = " + this.domain).append(";");
            sb.append("port = " + this.port).append(";");
            sb.append("path = " + this.path).append(";");
            sb.append("registerProcedure = " + this.registerProcedure).append(";");
            sb.append("protocol = " + this.protocol).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = port == null ? 0 : port.hashCode();
            _hash = 31 * _hash + delta;
            delta = path == null ? 0 : path.hashCode();
            _hash = 31 * _hash + delta;
            delta = registerProcedure == null ? 0 : registerProcedure.hashCode();
            _hash = 31 * _hash + delta;
            delta = protocol == null ? 0 : protocol.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
