package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.stub.EnclosureStructStub;


// Wrapper class + bean combo.
public class EnclosureStructBean implements EnclosureStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(EnclosureStructBean.class.getName());

    // [1] With an embedded object.
    private EnclosureStructStub stub = null;

    // [2] Or, without an embedded object.
    private String url;
    private Integer length;
    private String type;

    // Ctors.
    public EnclosureStructBean()
    {
        //this((String) null);
    }
    public EnclosureStructBean(String url, Integer length, String type)
    {
        this.url = url;
        this.length = length;
        this.type = type;
    }
    public EnclosureStructBean(EnclosureStruct stub)
    {
        if(stub instanceof EnclosureStructStub) {
            this.stub = (EnclosureStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUrl(stub.getUrl());   
            setLength(stub.getLength());   
            setType(stub.getType());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }

    public Integer getLength()
    {
        if(getStub() != null) {
            return getStub().getLength();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.length;
        }
    }
    public void setLength(Integer length)
    {
        if(getStub() != null) {
            getStub().setLength(length);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.length = length;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLength() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public EnclosureStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(EnclosureStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("url = " + this.url).append(";");
            sb.append("length = " + this.length).append(";");
            sb.append("type = " + this.type).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            delta = length == null ? 0 : length.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
