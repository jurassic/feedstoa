package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.stub.CloudStructStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.ImageStructStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.stub.UserStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.stub.TextInputStructStub;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.stub.ChannelFeedStub;


// Wrapper class + bean combo.
public class ChannelFeedBean implements ChannelFeed, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedBean.class.getName());

    // [1] With an embedded object.
    private ChannelFeedStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String channelSource;
    private String channelCode;
    private String previousVersion;
    private String fetchRequest;
    private String fetchUrl;
    private String feedServiceUrl;
    private String feedUrl;
    private String feedFormat;
    private Integer maxItemCount;
    private String feedCategory;
    private String title;
    private String subtitle;
    private UriStructBean link;
    private String description;
    private String language;
    private String copyright;
    private UserStructBean managingEditor;
    private UserStructBean webMaster;
    private List<UserStruct> contributor;
    private String pubDate;
    private String lastBuildDate;
    private List<CategoryStruct> category;
    private String generator;
    private String docs;
    private CloudStructBean cloud;
    private Integer ttl;
    private ImageStructBean logo;
    private ImageStructBean icon;
    private String rating;
    private TextInputStructBean textInput;
    private List<Integer> skipHours;
    private List<String> skipDays;
    private String outputText;
    private String outputHash;
    private String feedContent;
    private String status;
    private String note;
    private ReferrerInfoStructBean referrerInfo;
    private Long lastBuildTime;
    private Long publishedTime;
    private Long expirationTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ChannelFeedBean()
    {
        //this((String) null);
    }
    public ChannelFeedBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ChannelFeedBean(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructBean link, String description, String language, String copyright, UserStructBean managingEditor, UserStructBean webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStructBean cloud, Integer ttl, ImageStructBean logo, ImageStructBean icon, String rating, TextInputStructBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime)
    {
        this(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime, null, null);
    }
    public ChannelFeedBean(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructBean link, String description, String language, String copyright, UserStructBean managingEditor, UserStructBean webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStructBean cloud, Integer ttl, ImageStructBean logo, ImageStructBean icon, String rating, TextInputStructBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.channelSource = channelSource;
        this.channelCode = channelCode;
        this.previousVersion = previousVersion;
        this.fetchRequest = fetchRequest;
        this.fetchUrl = fetchUrl;
        this.feedServiceUrl = feedServiceUrl;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.maxItemCount = maxItemCount;
        this.feedCategory = feedCategory;
        this.title = title;
        this.subtitle = subtitle;
        this.link = link;
        this.description = description;
        this.language = language;
        this.copyright = copyright;
        this.managingEditor = managingEditor;
        this.webMaster = webMaster;
        this.contributor = contributor;
        this.pubDate = pubDate;
        this.lastBuildDate = lastBuildDate;
        this.category = category;
        this.generator = generator;
        this.docs = docs;
        this.cloud = cloud;
        this.ttl = ttl;
        this.logo = logo;
        this.icon = icon;
        this.rating = rating;
        this.textInput = textInput;
        this.skipHours = skipHours;
        this.skipDays = skipDays;
        this.outputText = outputText;
        this.outputHash = outputHash;
        this.feedContent = feedContent;
        this.status = status;
        this.note = note;
        this.referrerInfo = referrerInfo;
        this.lastBuildTime = lastBuildTime;
        this.publishedTime = publishedTime;
        this.expirationTime = expirationTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ChannelFeedBean(ChannelFeed stub)
    {
        if(stub instanceof ChannelFeedStub) {
            this.stub = (ChannelFeedStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setChannelSource(stub.getChannelSource());   
            setChannelCode(stub.getChannelCode());   
            setPreviousVersion(stub.getPreviousVersion());   
            setFetchRequest(stub.getFetchRequest());   
            setFetchUrl(stub.getFetchUrl());   
            setFeedServiceUrl(stub.getFeedServiceUrl());   
            setFeedUrl(stub.getFeedUrl());   
            setFeedFormat(stub.getFeedFormat());   
            setMaxItemCount(stub.getMaxItemCount());   
            setFeedCategory(stub.getFeedCategory());   
            setTitle(stub.getTitle());   
            setSubtitle(stub.getSubtitle());   
            UriStruct link = stub.getLink();
            if(link instanceof UriStructBean) {
                setLink((UriStructBean) link);   
            } else {
                setLink(new UriStructBean(link));   
            }
            setDescription(stub.getDescription());   
            setLanguage(stub.getLanguage());   
            setCopyright(stub.getCopyright());   
            UserStruct managingEditor = stub.getManagingEditor();
            if(managingEditor instanceof UserStructBean) {
                setManagingEditor((UserStructBean) managingEditor);   
            } else {
                setManagingEditor(new UserStructBean(managingEditor));   
            }
            UserStruct webMaster = stub.getWebMaster();
            if(webMaster instanceof UserStructBean) {
                setWebMaster((UserStructBean) webMaster);   
            } else {
                setWebMaster(new UserStructBean(webMaster));   
            }
            setContributor(stub.getContributor());   
            setPubDate(stub.getPubDate());   
            setLastBuildDate(stub.getLastBuildDate());   
            setCategory(stub.getCategory());   
            setGenerator(stub.getGenerator());   
            setDocs(stub.getDocs());   
            CloudStruct cloud = stub.getCloud();
            if(cloud instanceof CloudStructBean) {
                setCloud((CloudStructBean) cloud);   
            } else {
                setCloud(new CloudStructBean(cloud));   
            }
            setTtl(stub.getTtl());   
            ImageStruct logo = stub.getLogo();
            if(logo instanceof ImageStructBean) {
                setLogo((ImageStructBean) logo);   
            } else {
                setLogo(new ImageStructBean(logo));   
            }
            ImageStruct icon = stub.getIcon();
            if(icon instanceof ImageStructBean) {
                setIcon((ImageStructBean) icon);   
            } else {
                setIcon(new ImageStructBean(icon));   
            }
            setRating(stub.getRating());   
            TextInputStruct textInput = stub.getTextInput();
            if(textInput instanceof TextInputStructBean) {
                setTextInput((TextInputStructBean) textInput);   
            } else {
                setTextInput(new TextInputStructBean(textInput));   
            }
            setSkipHours(stub.getSkipHours());   
            setSkipDays(stub.getSkipDays());   
            setOutputText(stub.getOutputText());   
            setOutputHash(stub.getOutputHash());   
            setFeedContent(stub.getFeedContent());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setLastBuildTime(stub.getLastBuildTime());   
            setPublishedTime(stub.getPublishedTime());   
            setExpirationTime(stub.getExpirationTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getChannelSource()
    {
        if(getStub() != null) {
            return getStub().getChannelSource();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelSource;
        }
    }
    public void setChannelSource(String channelSource)
    {
        if(getStub() != null) {
            getStub().setChannelSource(channelSource);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelSource = channelSource;
        }
    }

    public String getChannelCode()
    {
        if(getStub() != null) {
            return getStub().getChannelCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelCode;
        }
    }
    public void setChannelCode(String channelCode)
    {
        if(getStub() != null) {
            getStub().setChannelCode(channelCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelCode = channelCode;
        }
    }

    public String getPreviousVersion()
    {
        if(getStub() != null) {
            return getStub().getPreviousVersion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.previousVersion;
        }
    }
    public void setPreviousVersion(String previousVersion)
    {
        if(getStub() != null) {
            getStub().setPreviousVersion(previousVersion);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.previousVersion = previousVersion;
        }
    }

    public String getFetchRequest()
    {
        if(getStub() != null) {
            return getStub().getFetchRequest();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchRequest;
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getStub() != null) {
            getStub().setFetchRequest(fetchRequest);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchRequest = fetchRequest;
        }
    }

    public String getFetchUrl()
    {
        if(getStub() != null) {
            return getStub().getFetchUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchUrl;
        }
    }
    public void setFetchUrl(String fetchUrl)
    {
        if(getStub() != null) {
            getStub().setFetchUrl(fetchUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchUrl = fetchUrl;
        }
    }

    public String getFeedServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getFeedServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedServiceUrl;
        }
    }
    public void setFeedServiceUrl(String feedServiceUrl)
    {
        if(getStub() != null) {
            getStub().setFeedServiceUrl(feedServiceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedServiceUrl = feedServiceUrl;
        }
    }

    public String getFeedUrl()
    {
        if(getStub() != null) {
            return getStub().getFeedUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedUrl;
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getStub() != null) {
            getStub().setFeedUrl(feedUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedUrl = feedUrl;
        }
    }

    public String getFeedFormat()
    {
        if(getStub() != null) {
            return getStub().getFeedFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedFormat;
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getStub() != null) {
            getStub().setFeedFormat(feedFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedFormat = feedFormat;
        }
    }

    public Integer getMaxItemCount()
    {
        if(getStub() != null) {
            return getStub().getMaxItemCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.maxItemCount;
        }
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        if(getStub() != null) {
            getStub().setMaxItemCount(maxItemCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.maxItemCount = maxItemCount;
        }
    }

    public String getFeedCategory()
    {
        if(getStub() != null) {
            return getStub().getFeedCategory();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedCategory;
        }
    }
    public void setFeedCategory(String feedCategory)
    {
        if(getStub() != null) {
            getStub().setFeedCategory(feedCategory);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedCategory = feedCategory;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getSubtitle()
    {
        if(getStub() != null) {
            return getStub().getSubtitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.subtitle;
        }
    }
    public void setSubtitle(String subtitle)
    {
        if(getStub() != null) {
            getStub().setSubtitle(subtitle);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.subtitle = subtitle;
        }
    }

    public UriStruct getLink()
    {  
        if(getStub() != null) {
            // Note the object type.
            UriStruct _stub_field = getStub().getLink();
            if(_stub_field == null) {
                return null;
            } else {
                return new UriStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.link;
        }
    }
    public void setLink(UriStruct link)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setLink(link);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(link == null) {
                this.link = null;
            } else {
                if(link instanceof UriStructBean) {
                    this.link = (UriStructBean) link;
                } else {
                    this.link = new UriStructBean(link);
                }
            }
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getLanguage()
    {
        if(getStub() != null) {
            return getStub().getLanguage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.language;
        }
    }
    public void setLanguage(String language)
    {
        if(getStub() != null) {
            getStub().setLanguage(language);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.language = language;
        }
    }

    public String getCopyright()
    {
        if(getStub() != null) {
            return getStub().getCopyright();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.copyright;
        }
    }
    public void setCopyright(String copyright)
    {
        if(getStub() != null) {
            getStub().setCopyright(copyright);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.copyright = copyright;
        }
    }

    public UserStruct getManagingEditor()
    {  
        if(getStub() != null) {
            // Note the object type.
            UserStruct _stub_field = getStub().getManagingEditor();
            if(_stub_field == null) {
                return null;
            } else {
                return new UserStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managingEditor;
        }
    }
    public void setManagingEditor(UserStruct managingEditor)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setManagingEditor(managingEditor);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(managingEditor == null) {
                this.managingEditor = null;
            } else {
                if(managingEditor instanceof UserStructBean) {
                    this.managingEditor = (UserStructBean) managingEditor;
                } else {
                    this.managingEditor = new UserStructBean(managingEditor);
                }
            }
        }
    }

    public UserStruct getWebMaster()
    {  
        if(getStub() != null) {
            // Note the object type.
            UserStruct _stub_field = getStub().getWebMaster();
            if(_stub_field == null) {
                return null;
            } else {
                return new UserStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.webMaster;
        }
    }
    public void setWebMaster(UserStruct webMaster)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setWebMaster(webMaster);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(webMaster == null) {
                this.webMaster = null;
            } else {
                if(webMaster instanceof UserStructBean) {
                    this.webMaster = (UserStructBean) webMaster;
                } else {
                    this.webMaster = new UserStructBean(webMaster);
                }
            }
        }
    }

    public List<UserStruct> getContributor()
    {
        if(getStub() != null) {
            List<UserStruct> list = getStub().getContributor();
            if(list != null) {
                List<UserStruct> bean = new ArrayList<UserStruct>();
                for(UserStruct userStruct : list) {
                    UserStructBean elem = null;
                    if(userStruct instanceof UserStructBean) {
                        elem = (UserStructBean) userStruct;
                    } else if(userStruct instanceof UserStructStub) {
                        elem = new UserStructBean(userStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contributor;
        }
    }
    public void setContributor(List<UserStruct> contributor)
    {
        if(contributor != null) {
            if(getStub() != null) {
                List<UserStruct> stub = new ArrayList<UserStruct>();
                for(UserStruct userStruct : contributor) {
                    UserStructStub elem = null;
                    if(userStruct instanceof UserStructStub) {
                        elem = (UserStructStub) userStruct;
                    } else if(userStruct instanceof UserStructBean) {
                        elem = ((UserStructBean) userStruct).getStub();
                    } else if(userStruct instanceof UserStruct) {
                        elem = new UserStructStub(userStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setContributor(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.contributor = contributor;  // ???

                List<UserStruct> beans = new ArrayList<UserStruct>();
                for(UserStruct userStruct : contributor) {
                    UserStructBean elem = null;
                    if(userStruct != null) {
                        if(userStruct instanceof UserStructBean) {
                            elem = (UserStructBean) userStruct;
                        } else {
                            elem = new UserStructBean(userStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.contributor = beans;
            }
        } else {
            this.contributor = null;
        }
    }

    public String getPubDate()
    {
        if(getStub() != null) {
            return getStub().getPubDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pubDate;
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getStub() != null) {
            getStub().setPubDate(pubDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pubDate = pubDate;
        }
    }

    public String getLastBuildDate()
    {
        if(getStub() != null) {
            return getStub().getLastBuildDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastBuildDate;
        }
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        if(getStub() != null) {
            getStub().setLastBuildDate(lastBuildDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastBuildDate = lastBuildDate;
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getStub() != null) {
            List<CategoryStruct> list = getStub().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructStub) {
                        elem = new CategoryStructBean(categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.category;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getStub() != null) {
                List<CategoryStruct> stub = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructStub elem = null;
                    if(categoryStruct instanceof CategoryStructStub) {
                        elem = (CategoryStructStub) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).getStub();
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructStub(categoryStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setCategory(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.category = category;  // ???

                List<CategoryStruct> beans = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructBean elem = null;
                    if(categoryStruct != null) {
                        if(categoryStruct instanceof CategoryStructBean) {
                            elem = (CategoryStructBean) categoryStruct;
                        } else {
                            elem = new CategoryStructBean(categoryStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.category = beans;
            }
        } else {
            this.category = null;
        }
    }

    public String getGenerator()
    {
        if(getStub() != null) {
            return getStub().getGenerator();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.generator;
        }
    }
    public void setGenerator(String generator)
    {
        if(getStub() != null) {
            getStub().setGenerator(generator);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.generator = generator;
        }
    }

    public String getDocs()
    {
        if(getStub() != null) {
            return getStub().getDocs();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.docs;
        }
    }
    public void setDocs(String docs)
    {
        if(getStub() != null) {
            getStub().setDocs(docs);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.docs = docs;
        }
    }

    public CloudStruct getCloud()
    {  
        if(getStub() != null) {
            // Note the object type.
            CloudStruct _stub_field = getStub().getCloud();
            if(_stub_field == null) {
                return null;
            } else {
                return new CloudStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.cloud;
        }
    }
    public void setCloud(CloudStruct cloud)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setCloud(cloud);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(cloud == null) {
                this.cloud = null;
            } else {
                if(cloud instanceof CloudStructBean) {
                    this.cloud = (CloudStructBean) cloud;
                } else {
                    this.cloud = new CloudStructBean(cloud);
                }
            }
        }
    }

    public Integer getTtl()
    {
        if(getStub() != null) {
            return getStub().getTtl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ttl;
        }
    }
    public void setTtl(Integer ttl)
    {
        if(getStub() != null) {
            getStub().setTtl(ttl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ttl = ttl;
        }
    }

    public ImageStruct getLogo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ImageStruct _stub_field = getStub().getLogo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ImageStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.logo;
        }
    }
    public void setLogo(ImageStruct logo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setLogo(logo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(logo == null) {
                this.logo = null;
            } else {
                if(logo instanceof ImageStructBean) {
                    this.logo = (ImageStructBean) logo;
                } else {
                    this.logo = new ImageStructBean(logo);
                }
            }
        }
    }

    public ImageStruct getIcon()
    {  
        if(getStub() != null) {
            // Note the object type.
            ImageStruct _stub_field = getStub().getIcon();
            if(_stub_field == null) {
                return null;
            } else {
                return new ImageStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.icon;
        }
    }
    public void setIcon(ImageStruct icon)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setIcon(icon);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(icon == null) {
                this.icon = null;
            } else {
                if(icon instanceof ImageStructBean) {
                    this.icon = (ImageStructBean) icon;
                } else {
                    this.icon = new ImageStructBean(icon);
                }
            }
        }
    }

    public String getRating()
    {
        if(getStub() != null) {
            return getStub().getRating();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rating;
        }
    }
    public void setRating(String rating)
    {
        if(getStub() != null) {
            getStub().setRating(rating);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rating = rating;
        }
    }

    public TextInputStruct getTextInput()
    {  
        if(getStub() != null) {
            // Note the object type.
            TextInputStruct _stub_field = getStub().getTextInput();
            if(_stub_field == null) {
                return null;
            } else {
                return new TextInputStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.textInput;
        }
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setTextInput(textInput);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(textInput == null) {
                this.textInput = null;
            } else {
                if(textInput instanceof TextInputStructBean) {
                    this.textInput = (TextInputStructBean) textInput;
                } else {
                    this.textInput = new TextInputStructBean(textInput);
                }
            }
        }
    }

    public List<Integer> getSkipHours()
    {
        if(getStub() != null) {
            return getStub().getSkipHours();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.skipHours;
        }
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        if(getStub() != null) {
            getStub().setSkipHours(skipHours);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.skipHours = skipHours;
        }
    }

    public List<String> getSkipDays()
    {
        if(getStub() != null) {
            return getStub().getSkipDays();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.skipDays;
        }
    }
    public void setSkipDays(List<String> skipDays)
    {
        if(getStub() != null) {
            getStub().setSkipDays(skipDays);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.skipDays = skipDays;
        }
    }

    public String getOutputText()
    {
        if(getStub() != null) {
            return getStub().getOutputText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputText;
        }
    }
    public void setOutputText(String outputText)
    {
        if(getStub() != null) {
            getStub().setOutputText(outputText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputText = outputText;
        }
    }

    public String getOutputHash()
    {
        if(getStub() != null) {
            return getStub().getOutputHash();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputHash;
        }
    }
    public void setOutputHash(String outputHash)
    {
        if(getStub() != null) {
            getStub().setOutputHash(outputHash);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputHash = outputHash;
        }
    }

    public String getFeedContent()
    {
        if(getStub() != null) {
            return getStub().getFeedContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedContent;
        }
    }
    public void setFeedContent(String feedContent)
    {
        if(getStub() != null) {
            getStub().setFeedContent(feedContent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedContent = feedContent;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public Long getLastBuildTime()
    {
        if(getStub() != null) {
            return getStub().getLastBuildTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastBuildTime;
        }
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        if(getStub() != null) {
            getStub().setLastBuildTime(lastBuildTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastBuildTime = lastBuildTime;
        }
    }

    public Long getPublishedTime()
    {
        if(getStub() != null) {
            return getStub().getPublishedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.publishedTime;
        }
    }
    public void setPublishedTime(Long publishedTime)
    {
        if(getStub() != null) {
            getStub().setPublishedTime(publishedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.publishedTime = publishedTime;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getStub() != null) {
            return getStub().getLastUpdatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastUpdatedTime;
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getStub() != null) {
            getStub().setLastUpdatedTime(lastUpdatedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastUpdatedTime = lastUpdatedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ChannelFeedStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ChannelFeedStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("channelSource = " + this.channelSource).append(";");
            sb.append("channelCode = " + this.channelCode).append(";");
            sb.append("previousVersion = " + this.previousVersion).append(";");
            sb.append("fetchRequest = " + this.fetchRequest).append(";");
            sb.append("fetchUrl = " + this.fetchUrl).append(";");
            sb.append("feedServiceUrl = " + this.feedServiceUrl).append(";");
            sb.append("feedUrl = " + this.feedUrl).append(";");
            sb.append("feedFormat = " + this.feedFormat).append(";");
            sb.append("maxItemCount = " + this.maxItemCount).append(";");
            sb.append("feedCategory = " + this.feedCategory).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("subtitle = " + this.subtitle).append(";");
            sb.append("link = " + this.link).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("language = " + this.language).append(";");
            sb.append("copyright = " + this.copyright).append(";");
            sb.append("managingEditor = " + this.managingEditor).append(";");
            sb.append("webMaster = " + this.webMaster).append(";");
            sb.append("contributor = " + this.contributor).append(";");
            sb.append("pubDate = " + this.pubDate).append(";");
            sb.append("lastBuildDate = " + this.lastBuildDate).append(";");
            sb.append("category = " + this.category).append(";");
            sb.append("generator = " + this.generator).append(";");
            sb.append("docs = " + this.docs).append(";");
            sb.append("cloud = " + this.cloud).append(";");
            sb.append("ttl = " + this.ttl).append(";");
            sb.append("logo = " + this.logo).append(";");
            sb.append("icon = " + this.icon).append(";");
            sb.append("rating = " + this.rating).append(";");
            sb.append("textInput = " + this.textInput).append(";");
            sb.append("skipHours = " + this.skipHours).append(";");
            sb.append("skipDays = " + this.skipDays).append(";");
            sb.append("outputText = " + this.outputText).append(";");
            sb.append("outputHash = " + this.outputHash).append(";");
            sb.append("feedContent = " + this.feedContent).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("lastBuildTime = " + this.lastBuildTime).append(";");
            sb.append("publishedTime = " + this.publishedTime).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelSource == null ? 0 : channelSource.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelCode == null ? 0 : channelCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = previousVersion == null ? 0 : previousVersion.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedServiceUrl == null ? 0 : feedServiceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedUrl == null ? 0 : feedUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedFormat == null ? 0 : feedFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = maxItemCount == null ? 0 : maxItemCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedCategory == null ? 0 : feedCategory.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = subtitle == null ? 0 : subtitle.hashCode();
            _hash = 31 * _hash + delta;
            delta = link == null ? 0 : link.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = language == null ? 0 : language.hashCode();
            _hash = 31 * _hash + delta;
            delta = copyright == null ? 0 : copyright.hashCode();
            _hash = 31 * _hash + delta;
            delta = managingEditor == null ? 0 : managingEditor.hashCode();
            _hash = 31 * _hash + delta;
            delta = webMaster == null ? 0 : webMaster.hashCode();
            _hash = 31 * _hash + delta;
            delta = contributor == null ? 0 : contributor.hashCode();
            _hash = 31 * _hash + delta;
            delta = pubDate == null ? 0 : pubDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = category == null ? 0 : category.hashCode();
            _hash = 31 * _hash + delta;
            delta = generator == null ? 0 : generator.hashCode();
            _hash = 31 * _hash + delta;
            delta = docs == null ? 0 : docs.hashCode();
            _hash = 31 * _hash + delta;
            delta = cloud == null ? 0 : cloud.hashCode();
            _hash = 31 * _hash + delta;
            delta = ttl == null ? 0 : ttl.hashCode();
            _hash = 31 * _hash + delta;
            delta = logo == null ? 0 : logo.hashCode();
            _hash = 31 * _hash + delta;
            delta = icon == null ? 0 : icon.hashCode();
            _hash = 31 * _hash + delta;
            delta = rating == null ? 0 : rating.hashCode();
            _hash = 31 * _hash + delta;
            delta = textInput == null ? 0 : textInput.hashCode();
            _hash = 31 * _hash + delta;
            delta = skipHours == null ? 0 : skipHours.hashCode();
            _hash = 31 * _hash + delta;
            delta = skipDays == null ? 0 : skipDays.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputText == null ? 0 : outputText.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputHash == null ? 0 : outputHash.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedContent == null ? 0 : feedContent.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastBuildTime == null ? 0 : lastBuildTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = publishedTime == null ? 0 : publishedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
