package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.ws.stub.GaeAppStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.stub.FetchBaseStub;
import com.feedstoa.ws.stub.FetchRequestStub;


// Wrapper class + bean combo.
public class FetchRequestBean extends FetchBaseBean implements FetchRequest, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestBean.class.getName());


    // [2] Or, without an embedded object.
    private String originFetch;
    private String outputFormat;
    private Integer fetchStatus;
    private String result;
    private String feedCategory;
    private Boolean multipleFeedEnabled;
    private Boolean deferred;
    private Boolean alert;
    private NotificationStructBean notificationPref;
    private ReferrerInfoStructBean referrerInfo;
    private Integer refreshInterval;
    private List<String> refreshExpressions;
    private String refreshTimeZone;
    private Integer currentRefreshCount;
    private Integer maxRefreshCount;
    private Long refreshExpirationTime;
    private Long nextRefreshTime;
    private Long lastUpdatedTime;

    // Ctors.
    public FetchRequestBean()
    {
        //this((String) null);
    }
    public FetchRequestBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchRequestBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructBean notificationPref, ReferrerInfoStructBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime, null, null);
    }
    public FetchRequestBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructBean notificationPref, ReferrerInfoStructBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, createdTime, modifiedTime);

        this.originFetch = originFetch;
        this.outputFormat = outputFormat;
        this.fetchStatus = fetchStatus;
        this.result = result;
        this.feedCategory = feedCategory;
        this.multipleFeedEnabled = multipleFeedEnabled;
        this.deferred = deferred;
        this.alert = alert;
        this.notificationPref = notificationPref;
        this.referrerInfo = referrerInfo;
        this.refreshInterval = refreshInterval;
        this.refreshExpressions = refreshExpressions;
        this.refreshTimeZone = refreshTimeZone;
        this.currentRefreshCount = currentRefreshCount;
        this.maxRefreshCount = maxRefreshCount;
        this.refreshExpirationTime = refreshExpirationTime;
        this.nextRefreshTime = nextRefreshTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }
    public FetchRequestBean(FetchRequest stub)
    {
        if(stub instanceof FetchRequestStub) {
            super.setStub((FetchBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setUser(stub.getUser());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFetchUrl(stub.getFetchUrl());   
            setFeedUrl(stub.getFeedUrl());   
            setChannelFeed(stub.getChannelFeed());   
            setReuseChannel(stub.isReuseChannel());   
            setMaxItemCount(stub.getMaxItemCount());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setOriginFetch(stub.getOriginFetch());   
            setOutputFormat(stub.getOutputFormat());   
            setFetchStatus(stub.getFetchStatus());   
            setResult(stub.getResult());   
            setFeedCategory(stub.getFeedCategory());   
            setMultipleFeedEnabled(stub.isMultipleFeedEnabled());   
            setDeferred(stub.isDeferred());   
            setAlert(stub.isAlert());   
            NotificationStruct notificationPref = stub.getNotificationPref();
            if(notificationPref instanceof NotificationStructBean) {
                setNotificationPref((NotificationStructBean) notificationPref);   
            } else {
                setNotificationPref(new NotificationStructBean(notificationPref));   
            }
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setRefreshInterval(stub.getRefreshInterval());   
            setRefreshExpressions(stub.getRefreshExpressions());   
            setRefreshTimeZone(stub.getRefreshTimeZone());   
            setCurrentRefreshCount(stub.getCurrentRefreshCount());   
            setMaxRefreshCount(stub.getMaxRefreshCount());   
            setRefreshExpirationTime(stub.getRefreshExpirationTime());   
            setNextRefreshTime(stub.getNextRefreshTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getManagerApp()
    {
        return super.getManagerApp();
    }
    public void setManagerApp(String managerApp)
    {
        super.setManagerApp(managerApp);
    }

    public Long getAppAcl()
    {
        return super.getAppAcl();
    }
    public void setAppAcl(Long appAcl)
    {
        super.setAppAcl(appAcl);
    }

    public GaeAppStruct getGaeApp()
    {
        return super.getGaeApp();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        super.setGaeApp(gaeApp);
    }

    public String getOwnerUser()
    {
        return super.getOwnerUser();
    }
    public void setOwnerUser(String ownerUser)
    {
        super.setOwnerUser(ownerUser);
    }

    public Long getUserAcl()
    {
        return super.getUserAcl();
    }
    public void setUserAcl(Long userAcl)
    {
        super.setUserAcl(userAcl);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getFetchUrl()
    {
        return super.getFetchUrl();
    }
    public void setFetchUrl(String fetchUrl)
    {
        super.setFetchUrl(fetchUrl);
    }

    public String getFeedUrl()
    {
        return super.getFeedUrl();
    }
    public void setFeedUrl(String feedUrl)
    {
        super.setFeedUrl(feedUrl);
    }

    public String getChannelFeed()
    {
        return super.getChannelFeed();
    }
    public void setChannelFeed(String channelFeed)
    {
        super.setChannelFeed(channelFeed);
    }

    public Boolean isReuseChannel()
    {
        return super.isReuseChannel();
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        super.setReuseChannel(reuseChannel);
    }

    public Integer getMaxItemCount()
    {
        return super.getMaxItemCount();
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        super.setMaxItemCount(maxItemCount);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getOriginFetch()
    {
        if(getStub() != null) {
            return getStub().getOriginFetch();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.originFetch;
        }
    }
    public void setOriginFetch(String originFetch)
    {
        if(getStub() != null) {
            getStub().setOriginFetch(originFetch);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.originFetch = originFetch;
        }
    }

    public String getOutputFormat()
    {
        if(getStub() != null) {
            return getStub().getOutputFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.outputFormat;
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getStub() != null) {
            getStub().setOutputFormat(outputFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.outputFormat = outputFormat;
        }
    }

    public Integer getFetchStatus()
    {
        if(getStub() != null) {
            return getStub().getFetchStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchStatus;
        }
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        if(getStub() != null) {
            getStub().setFetchStatus(fetchStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchStatus = fetchStatus;
        }
    }

    public String getResult()
    {
        if(getStub() != null) {
            return getStub().getResult();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.result;
        }
    }
    public void setResult(String result)
    {
        if(getStub() != null) {
            getStub().setResult(result);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.result = result;
        }
    }

    public String getFeedCategory()
    {
        if(getStub() != null) {
            return getStub().getFeedCategory();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedCategory;
        }
    }
    public void setFeedCategory(String feedCategory)
    {
        if(getStub() != null) {
            getStub().setFeedCategory(feedCategory);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedCategory = feedCategory;
        }
    }

    public Boolean isMultipleFeedEnabled()
    {
        if(getStub() != null) {
            return getStub().isMultipleFeedEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.multipleFeedEnabled;
        }
    }
    public void setMultipleFeedEnabled(Boolean multipleFeedEnabled)
    {
        if(getStub() != null) {
            getStub().setMultipleFeedEnabled(multipleFeedEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.multipleFeedEnabled = multipleFeedEnabled;
        }
    }

    public Boolean isDeferred()
    {
        if(getStub() != null) {
            return getStub().isDeferred();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.deferred;
        }
    }
    public void setDeferred(Boolean deferred)
    {
        if(getStub() != null) {
            getStub().setDeferred(deferred);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.deferred = deferred;
        }
    }

    public Boolean isAlert()
    {
        if(getStub() != null) {
            return getStub().isAlert();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.alert;
        }
    }
    public void setAlert(Boolean alert)
    {
        if(getStub() != null) {
            getStub().setAlert(alert);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.alert = alert;
        }
    }

    public NotificationStruct getNotificationPref()
    {  
        if(getStub() != null) {
            // Note the object type.
            NotificationStruct _stub_field = getStub().getNotificationPref();
            if(_stub_field == null) {
                return null;
            } else {
                return new NotificationStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.notificationPref;
        }
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setNotificationPref(notificationPref);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(notificationPref == null) {
                this.notificationPref = null;
            } else {
                if(notificationPref instanceof NotificationStructBean) {
                    this.notificationPref = (NotificationStructBean) notificationPref;
                } else {
                    this.notificationPref = new NotificationStructBean(notificationPref);
                }
            }
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public Integer getRefreshInterval()
    {
        if(getStub() != null) {
            return getStub().getRefreshInterval();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshInterval;
        }
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        if(getStub() != null) {
            getStub().setRefreshInterval(refreshInterval);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshInterval = refreshInterval;
        }
    }

    public List<String> getRefreshExpressions()
    {
        if(getStub() != null) {
            return getStub().getRefreshExpressions();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshExpressions;
        }
    }
    public void setRefreshExpressions(List<String> refreshExpressions)
    {
        if(getStub() != null) {
            getStub().setRefreshExpressions(refreshExpressions);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshExpressions = refreshExpressions;
        }
    }

    public String getRefreshTimeZone()
    {
        if(getStub() != null) {
            return getStub().getRefreshTimeZone();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshTimeZone;
        }
    }
    public void setRefreshTimeZone(String refreshTimeZone)
    {
        if(getStub() != null) {
            getStub().setRefreshTimeZone(refreshTimeZone);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshTimeZone = refreshTimeZone;
        }
    }

    public Integer getCurrentRefreshCount()
    {
        if(getStub() != null) {
            return getStub().getCurrentRefreshCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.currentRefreshCount;
        }
    }
    public void setCurrentRefreshCount(Integer currentRefreshCount)
    {
        if(getStub() != null) {
            getStub().setCurrentRefreshCount(currentRefreshCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.currentRefreshCount = currentRefreshCount;
        }
    }

    public Integer getMaxRefreshCount()
    {
        if(getStub() != null) {
            return getStub().getMaxRefreshCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.maxRefreshCount;
        }
    }
    public void setMaxRefreshCount(Integer maxRefreshCount)
    {
        if(getStub() != null) {
            getStub().setMaxRefreshCount(maxRefreshCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.maxRefreshCount = maxRefreshCount;
        }
    }

    public Long getRefreshExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getRefreshExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshExpirationTime;
        }
    }
    public void setRefreshExpirationTime(Long refreshExpirationTime)
    {
        if(getStub() != null) {
            getStub().setRefreshExpirationTime(refreshExpirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshExpirationTime = refreshExpirationTime;
        }
    }

    public Long getNextRefreshTime()
    {
        if(getStub() != null) {
            return getStub().getNextRefreshTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextRefreshTime;
        }
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        if(getStub() != null) {
            getStub().setNextRefreshTime(nextRefreshTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextRefreshTime = nextRefreshTime;
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getStub() != null) {
            return getStub().getLastUpdatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastUpdatedTime;
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getStub() != null) {
            getStub().setLastUpdatedTime(lastUpdatedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastUpdatedTime = lastUpdatedTime;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FetchRequestStub getStub()
    {
        return (FetchRequestStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("originFetch = " + this.originFetch).append(";");
            sb.append("outputFormat = " + this.outputFormat).append(";");
            sb.append("fetchStatus = " + this.fetchStatus).append(";");
            sb.append("result = " + this.result).append(";");
            sb.append("feedCategory = " + this.feedCategory).append(";");
            sb.append("multipleFeedEnabled = " + this.multipleFeedEnabled).append(";");
            sb.append("deferred = " + this.deferred).append(";");
            sb.append("alert = " + this.alert).append(";");
            sb.append("notificationPref = " + this.notificationPref).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("refreshInterval = " + this.refreshInterval).append(";");
            sb.append("refreshExpressions = " + this.refreshExpressions).append(";");
            sb.append("refreshTimeZone = " + this.refreshTimeZone).append(";");
            sb.append("currentRefreshCount = " + this.currentRefreshCount).append(";");
            sb.append("maxRefreshCount = " + this.maxRefreshCount).append(";");
            sb.append("refreshExpirationTime = " + this.refreshExpirationTime).append(";");
            sb.append("nextRefreshTime = " + this.nextRefreshTime).append(";");
            sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = originFetch == null ? 0 : originFetch.hashCode();
            _hash = 31 * _hash + delta;
            delta = outputFormat == null ? 0 : outputFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchStatus == null ? 0 : fetchStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = result == null ? 0 : result.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedCategory == null ? 0 : feedCategory.hashCode();
            _hash = 31 * _hash + delta;
            delta = multipleFeedEnabled == null ? 0 : multipleFeedEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = deferred == null ? 0 : deferred.hashCode();
            _hash = 31 * _hash + delta;
            delta = alert == null ? 0 : alert.hashCode();
            _hash = 31 * _hash + delta;
            delta = notificationPref == null ? 0 : notificationPref.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshExpressions == null ? 0 : refreshExpressions.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshTimeZone == null ? 0 : refreshTimeZone.hashCode();
            _hash = 31 * _hash + delta;
            delta = currentRefreshCount == null ? 0 : currentRefreshCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = maxRefreshCount == null ? 0 : maxRefreshCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshExpirationTime == null ? 0 : refreshExpirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextRefreshTime == null ? 0 : nextRefreshTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
