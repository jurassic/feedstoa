package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.stub.EnclosureStructStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.UriStructStub;
import com.feedstoa.ws.stub.UserStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.stub.FeedItemStub;


// Wrapper class + bean combo.
public class FeedItemBean implements FeedItem, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedItemBean.class.getName());

    // [1] With an embedded object.
    private FeedItemStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String fetchRequest;
    private String fetchUrl;
    private String feedUrl;
    private String channelSource;
    private String channelFeed;
    private String feedFormat;
    private String title;
    private UriStructBean link;
    private String summary;
    private String content;
    private UserStructBean author;
    private List<UserStruct> contributor;
    private List<CategoryStruct> category;
    private String comments;
    private EnclosureStructBean enclosure;
    private String id;
    private String pubDate;
    private UriStructBean source;
    private String feedContent;
    private String status;
    private String note;
    private ReferrerInfoStructBean referrerInfo;
    private Long publishedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FeedItemBean()
    {
        //this((String) null);
    }
    public FeedItemBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FeedItemBean(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructBean link, String summary, String content, UserStructBean author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStructBean enclosure, String id, String pubDate, UriStructBean source, String feedContent, String status, String note, ReferrerInfoStructBean referrerInfo, Long publishedTime, Long lastUpdatedTime)
    {
        this(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime, null, null);
    }
    public FeedItemBean(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructBean link, String summary, String content, UserStructBean author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStructBean enclosure, String id, String pubDate, UriStructBean source, String feedContent, String status, String note, ReferrerInfoStructBean referrerInfo, Long publishedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.fetchRequest = fetchRequest;
        this.fetchUrl = fetchUrl;
        this.feedUrl = feedUrl;
        this.channelSource = channelSource;
        this.channelFeed = channelFeed;
        this.feedFormat = feedFormat;
        this.title = title;
        this.link = link;
        this.summary = summary;
        this.content = content;
        this.author = author;
        this.contributor = contributor;
        this.category = category;
        this.comments = comments;
        this.enclosure = enclosure;
        this.id = id;
        this.pubDate = pubDate;
        this.source = source;
        this.feedContent = feedContent;
        this.status = status;
        this.note = note;
        this.referrerInfo = referrerInfo;
        this.publishedTime = publishedTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FeedItemBean(FeedItem stub)
    {
        if(stub instanceof FeedItemStub) {
            this.stub = (FeedItemStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setFetchRequest(stub.getFetchRequest());   
            setFetchUrl(stub.getFetchUrl());   
            setFeedUrl(stub.getFeedUrl());   
            setChannelSource(stub.getChannelSource());   
            setChannelFeed(stub.getChannelFeed());   
            setFeedFormat(stub.getFeedFormat());   
            setTitle(stub.getTitle());   
            UriStruct link = stub.getLink();
            if(link instanceof UriStructBean) {
                setLink((UriStructBean) link);   
            } else {
                setLink(new UriStructBean(link));   
            }
            setSummary(stub.getSummary());   
            setContent(stub.getContent());   
            UserStruct author = stub.getAuthor();
            if(author instanceof UserStructBean) {
                setAuthor((UserStructBean) author);   
            } else {
                setAuthor(new UserStructBean(author));   
            }
            setContributor(stub.getContributor());   
            setCategory(stub.getCategory());   
            setComments(stub.getComments());   
            EnclosureStruct enclosure = stub.getEnclosure();
            if(enclosure instanceof EnclosureStructBean) {
                setEnclosure((EnclosureStructBean) enclosure);   
            } else {
                setEnclosure(new EnclosureStructBean(enclosure));   
            }
            setId(stub.getId());   
            setPubDate(stub.getPubDate());   
            UriStruct source = stub.getSource();
            if(source instanceof UriStructBean) {
                setSource((UriStructBean) source);   
            } else {
                setSource(new UriStructBean(source));   
            }
            setFeedContent(stub.getFeedContent());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setPublishedTime(stub.getPublishedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getFetchRequest()
    {
        if(getStub() != null) {
            return getStub().getFetchRequest();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchRequest;
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getStub() != null) {
            getStub().setFetchRequest(fetchRequest);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchRequest = fetchRequest;
        }
    }

    public String getFetchUrl()
    {
        if(getStub() != null) {
            return getStub().getFetchUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchUrl;
        }
    }
    public void setFetchUrl(String fetchUrl)
    {
        if(getStub() != null) {
            getStub().setFetchUrl(fetchUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchUrl = fetchUrl;
        }
    }

    public String getFeedUrl()
    {
        if(getStub() != null) {
            return getStub().getFeedUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedUrl;
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getStub() != null) {
            getStub().setFeedUrl(feedUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedUrl = feedUrl;
        }
    }

    public String getChannelSource()
    {
        if(getStub() != null) {
            return getStub().getChannelSource();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelSource;
        }
    }
    public void setChannelSource(String channelSource)
    {
        if(getStub() != null) {
            getStub().setChannelSource(channelSource);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelSource = channelSource;
        }
    }

    public String getChannelFeed()
    {
        if(getStub() != null) {
            return getStub().getChannelFeed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelFeed;
        }
    }
    public void setChannelFeed(String channelFeed)
    {
        if(getStub() != null) {
            getStub().setChannelFeed(channelFeed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelFeed = channelFeed;
        }
    }

    public String getFeedFormat()
    {
        if(getStub() != null) {
            return getStub().getFeedFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedFormat;
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getStub() != null) {
            getStub().setFeedFormat(feedFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedFormat = feedFormat;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public UriStruct getLink()
    {  
        if(getStub() != null) {
            // Note the object type.
            UriStruct _stub_field = getStub().getLink();
            if(_stub_field == null) {
                return null;
            } else {
                return new UriStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.link;
        }
    }
    public void setLink(UriStruct link)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setLink(link);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(link == null) {
                this.link = null;
            } else {
                if(link instanceof UriStructBean) {
                    this.link = (UriStructBean) link;
                } else {
                    this.link = new UriStructBean(link);
                }
            }
        }
    }

    public String getSummary()
    {
        if(getStub() != null) {
            return getStub().getSummary();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.summary;
        }
    }
    public void setSummary(String summary)
    {
        if(getStub() != null) {
            getStub().setSummary(summary);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.summary = summary;
        }
    }

    public String getContent()
    {
        if(getStub() != null) {
            return getStub().getContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.content;
        }
    }
    public void setContent(String content)
    {
        if(getStub() != null) {
            getStub().setContent(content);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.content = content;
        }
    }

    public UserStruct getAuthor()
    {  
        if(getStub() != null) {
            // Note the object type.
            UserStruct _stub_field = getStub().getAuthor();
            if(_stub_field == null) {
                return null;
            } else {
                return new UserStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.author;
        }
    }
    public void setAuthor(UserStruct author)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setAuthor(author);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(author == null) {
                this.author = null;
            } else {
                if(author instanceof UserStructBean) {
                    this.author = (UserStructBean) author;
                } else {
                    this.author = new UserStructBean(author);
                }
            }
        }
    }

    public List<UserStruct> getContributor()
    {
        if(getStub() != null) {
            List<UserStruct> list = getStub().getContributor();
            if(list != null) {
                List<UserStruct> bean = new ArrayList<UserStruct>();
                for(UserStruct userStruct : list) {
                    UserStructBean elem = null;
                    if(userStruct instanceof UserStructBean) {
                        elem = (UserStructBean) userStruct;
                    } else if(userStruct instanceof UserStructStub) {
                        elem = new UserStructBean(userStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contributor;
        }
    }
    public void setContributor(List<UserStruct> contributor)
    {
        if(contributor != null) {
            if(getStub() != null) {
                List<UserStruct> stub = new ArrayList<UserStruct>();
                for(UserStruct userStruct : contributor) {
                    UserStructStub elem = null;
                    if(userStruct instanceof UserStructStub) {
                        elem = (UserStructStub) userStruct;
                    } else if(userStruct instanceof UserStructBean) {
                        elem = ((UserStructBean) userStruct).getStub();
                    } else if(userStruct instanceof UserStruct) {
                        elem = new UserStructStub(userStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setContributor(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.contributor = contributor;  // ???

                List<UserStruct> beans = new ArrayList<UserStruct>();
                for(UserStruct userStruct : contributor) {
                    UserStructBean elem = null;
                    if(userStruct != null) {
                        if(userStruct instanceof UserStructBean) {
                            elem = (UserStructBean) userStruct;
                        } else {
                            elem = new UserStructBean(userStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.contributor = beans;
            }
        } else {
            this.contributor = null;
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getStub() != null) {
            List<CategoryStruct> list = getStub().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructStub) {
                        elem = new CategoryStructBean(categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.category;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getStub() != null) {
                List<CategoryStruct> stub = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructStub elem = null;
                    if(categoryStruct instanceof CategoryStructStub) {
                        elem = (CategoryStructStub) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).getStub();
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructStub(categoryStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setCategory(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.category = category;  // ???

                List<CategoryStruct> beans = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructBean elem = null;
                    if(categoryStruct != null) {
                        if(categoryStruct instanceof CategoryStructBean) {
                            elem = (CategoryStructBean) categoryStruct;
                        } else {
                            elem = new CategoryStructBean(categoryStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.category = beans;
            }
        } else {
            this.category = null;
        }
    }

    public String getComments()
    {
        if(getStub() != null) {
            return getStub().getComments();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.comments;
        }
    }
    public void setComments(String comments)
    {
        if(getStub() != null) {
            getStub().setComments(comments);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.comments = comments;
        }
    }

    public EnclosureStruct getEnclosure()
    {  
        if(getStub() != null) {
            // Note the object type.
            EnclosureStruct _stub_field = getStub().getEnclosure();
            if(_stub_field == null) {
                return null;
            } else {
                return new EnclosureStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.enclosure;
        }
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setEnclosure(enclosure);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(enclosure == null) {
                this.enclosure = null;
            } else {
                if(enclosure instanceof EnclosureStructBean) {
                    this.enclosure = (EnclosureStructBean) enclosure;
                } else {
                    this.enclosure = new EnclosureStructBean(enclosure);
                }
            }
        }
    }

    public String getId()
    {
        if(getStub() != null) {
            return getStub().getId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.id;
        }
    }
    public void setId(String id)
    {
        if(getStub() != null) {
            getStub().setId(id);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.id = id;
        }
    }

    public String getPubDate()
    {
        if(getStub() != null) {
            return getStub().getPubDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pubDate;
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getStub() != null) {
            getStub().setPubDate(pubDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pubDate = pubDate;
        }
    }

    public UriStruct getSource()
    {  
        if(getStub() != null) {
            // Note the object type.
            UriStruct _stub_field = getStub().getSource();
            if(_stub_field == null) {
                return null;
            } else {
                return new UriStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source;
        }
    }
    public void setSource(UriStruct source)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource(source);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source == null) {
                this.source = null;
            } else {
                if(source instanceof UriStructBean) {
                    this.source = (UriStructBean) source;
                } else {
                    this.source = new UriStructBean(source);
                }
            }
        }
    }

    public String getFeedContent()
    {
        if(getStub() != null) {
            return getStub().getFeedContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedContent;
        }
    }
    public void setFeedContent(String feedContent)
    {
        if(getStub() != null) {
            getStub().setFeedContent(feedContent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedContent = feedContent;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public Long getPublishedTime()
    {
        if(getStub() != null) {
            return getStub().getPublishedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.publishedTime;
        }
    }
    public void setPublishedTime(Long publishedTime)
    {
        if(getStub() != null) {
            getStub().setPublishedTime(publishedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.publishedTime = publishedTime;
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getStub() != null) {
            return getStub().getLastUpdatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastUpdatedTime;
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getStub() != null) {
            getStub().setLastUpdatedTime(lastUpdatedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastUpdatedTime = lastUpdatedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FeedItemStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FeedItemStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("fetchRequest = " + this.fetchRequest).append(";");
            sb.append("fetchUrl = " + this.fetchUrl).append(";");
            sb.append("feedUrl = " + this.feedUrl).append(";");
            sb.append("channelSource = " + this.channelSource).append(";");
            sb.append("channelFeed = " + this.channelFeed).append(";");
            sb.append("feedFormat = " + this.feedFormat).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("link = " + this.link).append(";");
            sb.append("summary = " + this.summary).append(";");
            sb.append("content = " + this.content).append(";");
            sb.append("author = " + this.author).append(";");
            sb.append("contributor = " + this.contributor).append(";");
            sb.append("category = " + this.category).append(";");
            sb.append("comments = " + this.comments).append(";");
            sb.append("enclosure = " + this.enclosure).append(";");
            sb.append("id = " + this.id).append(";");
            sb.append("pubDate = " + this.pubDate).append(";");
            sb.append("source = " + this.source).append(";");
            sb.append("feedContent = " + this.feedContent).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("publishedTime = " + this.publishedTime).append(";");
            sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedUrl == null ? 0 : feedUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelSource == null ? 0 : channelSource.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelFeed == null ? 0 : channelFeed.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedFormat == null ? 0 : feedFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = link == null ? 0 : link.hashCode();
            _hash = 31 * _hash + delta;
            delta = summary == null ? 0 : summary.hashCode();
            _hash = 31 * _hash + delta;
            delta = content == null ? 0 : content.hashCode();
            _hash = 31 * _hash + delta;
            delta = author == null ? 0 : author.hashCode();
            _hash = 31 * _hash + delta;
            delta = contributor == null ? 0 : contributor.hashCode();
            _hash = 31 * _hash + delta;
            delta = category == null ? 0 : category.hashCode();
            _hash = 31 * _hash + delta;
            delta = comments == null ? 0 : comments.hashCode();
            _hash = 31 * _hash + delta;
            delta = enclosure == null ? 0 : enclosure.hashCode();
            _hash = 31 * _hash + delta;
            delta = id == null ? 0 : id.hashCode();
            _hash = 31 * _hash + delta;
            delta = pubDate == null ? 0 : pubDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = source == null ? 0 : source.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedContent == null ? 0 : feedContent.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = publishedTime == null ? 0 : publishedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
