package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.ws.stub.GaeAppStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.FetchBase;
import com.feedstoa.ws.stub.FetchBaseStub;


// Wrapper class + bean combo.
public abstract class FetchBaseBean implements FetchBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchBaseBean.class.getName());

    // [1] With an embedded object.
    private FetchBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String managerApp;
    private Long appAcl;
    private GaeAppStructBean gaeApp;
    private String ownerUser;
    private Long userAcl;
    private String user;
    private String title;
    private String description;
    private String fetchUrl;
    private String feedUrl;
    private String channelFeed;
    private Boolean reuseChannel;
    private Integer maxItemCount;
    private String note;
    private String status;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FetchBaseBean()
    {
        //this((String) null);
    }
    public FetchBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchBaseBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, null, null);
    }
    public FetchBaseBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.managerApp = managerApp;
        this.appAcl = appAcl;
        this.gaeApp = gaeApp;
        this.ownerUser = ownerUser;
        this.userAcl = userAcl;
        this.user = user;
        this.title = title;
        this.description = description;
        this.fetchUrl = fetchUrl;
        this.feedUrl = feedUrl;
        this.channelFeed = channelFeed;
        this.reuseChannel = reuseChannel;
        this.maxItemCount = maxItemCount;
        this.note = note;
        this.status = status;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FetchBaseBean(FetchBase stub)
    {
        if(stub instanceof FetchBaseStub) {
            this.stub = (FetchBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setUser(stub.getUser());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFetchUrl(stub.getFetchUrl());   
            setFeedUrl(stub.getFeedUrl());   
            setChannelFeed(stub.getChannelFeed());   
            setReuseChannel(stub.isReuseChannel());   
            setMaxItemCount(stub.getMaxItemCount());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getManagerApp()
    {
        if(getStub() != null) {
            return getStub().getManagerApp();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managerApp;
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getStub() != null) {
            getStub().setManagerApp(managerApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managerApp = managerApp;
        }
    }

    public Long getAppAcl()
    {
        if(getStub() != null) {
            return getStub().getAppAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appAcl;
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getStub() != null) {
            getStub().setAppAcl(appAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.appAcl = appAcl;
        }
    }

    public GaeAppStruct getGaeApp()
    {  
        if(getStub() != null) {
            // Note the object type.
            GaeAppStruct _stub_field = getStub().getGaeApp();
            if(_stub_field == null) {
                return null;
            } else {
                return new GaeAppStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gaeApp;
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGaeApp(gaeApp);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(gaeApp == null) {
                this.gaeApp = null;
            } else {
                if(gaeApp instanceof GaeAppStructBean) {
                    this.gaeApp = (GaeAppStructBean) gaeApp;
                } else {
                    this.gaeApp = new GaeAppStructBean(gaeApp);
                }
            }
        }
    }

    public String getOwnerUser()
    {
        if(getStub() != null) {
            return getStub().getOwnerUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ownerUser;
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getStub() != null) {
            getStub().setOwnerUser(ownerUser);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ownerUser = ownerUser;
        }
    }

    public Long getUserAcl()
    {
        if(getStub() != null) {
            return getStub().getUserAcl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAcl;
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getStub() != null) {
            getStub().setUserAcl(userAcl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAcl = userAcl;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getFetchUrl()
    {
        if(getStub() != null) {
            return getStub().getFetchUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchUrl;
        }
    }
    public void setFetchUrl(String fetchUrl)
    {
        if(getStub() != null) {
            getStub().setFetchUrl(fetchUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchUrl = fetchUrl;
        }
    }

    public String getFeedUrl()
    {
        if(getStub() != null) {
            return getStub().getFeedUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedUrl;
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getStub() != null) {
            getStub().setFeedUrl(feedUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedUrl = feedUrl;
        }
    }

    public String getChannelFeed()
    {
        if(getStub() != null) {
            return getStub().getChannelFeed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelFeed;
        }
    }
    public void setChannelFeed(String channelFeed)
    {
        if(getStub() != null) {
            getStub().setChannelFeed(channelFeed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelFeed = channelFeed;
        }
    }

    public Boolean isReuseChannel()
    {
        if(getStub() != null) {
            return getStub().isReuseChannel();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.reuseChannel;
        }
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        if(getStub() != null) {
            getStub().setReuseChannel(reuseChannel);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.reuseChannel = reuseChannel;
        }
    }

    public Integer getMaxItemCount()
    {
        if(getStub() != null) {
            return getStub().getMaxItemCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.maxItemCount;
        }
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        if(getStub() != null) {
            getStub().setMaxItemCount(maxItemCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.maxItemCount = maxItemCount;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FetchBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FetchBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("managerApp = " + this.managerApp).append(";");
            sb.append("appAcl = " + this.appAcl).append(";");
            sb.append("gaeApp = " + this.gaeApp).append(";");
            sb.append("ownerUser = " + this.ownerUser).append(";");
            sb.append("userAcl = " + this.userAcl).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("fetchUrl = " + this.fetchUrl).append(";");
            sb.append("feedUrl = " + this.feedUrl).append(";");
            sb.append("channelFeed = " + this.channelFeed).append(";");
            sb.append("reuseChannel = " + this.reuseChannel).append(";");
            sb.append("maxItemCount = " + this.maxItemCount).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = managerApp == null ? 0 : managerApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = appAcl == null ? 0 : appAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = gaeApp == null ? 0 : gaeApp.hashCode();
            _hash = 31 * _hash + delta;
            delta = ownerUser == null ? 0 : ownerUser.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAcl == null ? 0 : userAcl.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedUrl == null ? 0 : feedUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelFeed == null ? 0 : channelFeed.hashCode();
            _hash = 31 * _hash + delta;
            delta = reuseChannel == null ? 0 : reuseChannel.hashCode();
            _hash = 31 * _hash + delta;
            delta = maxItemCount == null ? 0 : maxItemCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
