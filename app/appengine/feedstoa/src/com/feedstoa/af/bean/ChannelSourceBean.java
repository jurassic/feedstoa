package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.stub.ChannelSourceStub;


// Wrapper class + bean combo.
public class ChannelSourceBean implements ChannelSource, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelSourceBean.class.getName());

    // [1] With an embedded object.
    private ChannelSourceStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String serviceName;
    private String serviceUrl;
    private String feedUrl;
    private String feedFormat;
    private String channelTitle;
    private String channelDescription;
    private String channelCategory;
    private String channelUrl;
    private String channelLogo;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ChannelSourceBean()
    {
        //this((String) null);
    }
    public ChannelSourceBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ChannelSourceBean(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note)
    {
        this(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note, null, null);
    }
    public ChannelSourceBean(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.channelTitle = channelTitle;
        this.channelDescription = channelDescription;
        this.channelCategory = channelCategory;
        this.channelUrl = channelUrl;
        this.channelLogo = channelLogo;
        this.status = status;
        this.note = note;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ChannelSourceBean(ChannelSource stub)
    {
        if(stub instanceof ChannelSourceStub) {
            this.stub = (ChannelSourceStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setServiceName(stub.getServiceName());   
            setServiceUrl(stub.getServiceUrl());   
            setFeedUrl(stub.getFeedUrl());   
            setFeedFormat(stub.getFeedFormat());   
            setChannelTitle(stub.getChannelTitle());   
            setChannelDescription(stub.getChannelDescription());   
            setChannelCategory(stub.getChannelCategory());   
            setChannelUrl(stub.getChannelUrl());   
            setChannelLogo(stub.getChannelLogo());   
            setStatus(stub.getStatus());   
            setNote(stub.getNote());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getServiceName()
    {
        if(getStub() != null) {
            return getStub().getServiceName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceName;
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getStub() != null) {
            getStub().setServiceName(serviceName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceName = serviceName;
        }
    }

    public String getServiceUrl()
    {
        if(getStub() != null) {
            return getStub().getServiceUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.serviceUrl;
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getStub() != null) {
            getStub().setServiceUrl(serviceUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.serviceUrl = serviceUrl;
        }
    }

    public String getFeedUrl()
    {
        if(getStub() != null) {
            return getStub().getFeedUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedUrl;
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getStub() != null) {
            getStub().setFeedUrl(feedUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedUrl = feedUrl;
        }
    }

    public String getFeedFormat()
    {
        if(getStub() != null) {
            return getStub().getFeedFormat();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.feedFormat;
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getStub() != null) {
            getStub().setFeedFormat(feedFormat);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.feedFormat = feedFormat;
        }
    }

    public String getChannelTitle()
    {
        if(getStub() != null) {
            return getStub().getChannelTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelTitle;
        }
    }
    public void setChannelTitle(String channelTitle)
    {
        if(getStub() != null) {
            getStub().setChannelTitle(channelTitle);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelTitle = channelTitle;
        }
    }

    public String getChannelDescription()
    {
        if(getStub() != null) {
            return getStub().getChannelDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelDescription;
        }
    }
    public void setChannelDescription(String channelDescription)
    {
        if(getStub() != null) {
            getStub().setChannelDescription(channelDescription);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelDescription = channelDescription;
        }
    }

    public String getChannelCategory()
    {
        if(getStub() != null) {
            return getStub().getChannelCategory();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelCategory;
        }
    }
    public void setChannelCategory(String channelCategory)
    {
        if(getStub() != null) {
            getStub().setChannelCategory(channelCategory);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelCategory = channelCategory;
        }
    }

    public String getChannelUrl()
    {
        if(getStub() != null) {
            return getStub().getChannelUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelUrl;
        }
    }
    public void setChannelUrl(String channelUrl)
    {
        if(getStub() != null) {
            getStub().setChannelUrl(channelUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelUrl = channelUrl;
        }
    }

    public String getChannelLogo()
    {
        if(getStub() != null) {
            return getStub().getChannelLogo();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.channelLogo;
        }
    }
    public void setChannelLogo(String channelLogo)
    {
        if(getStub() != null) {
            getStub().setChannelLogo(channelLogo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.channelLogo = channelLogo;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ChannelSourceStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ChannelSourceStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("serviceName = " + this.serviceName).append(";");
            sb.append("serviceUrl = " + this.serviceUrl).append(";");
            sb.append("feedUrl = " + this.feedUrl).append(";");
            sb.append("feedFormat = " + this.feedFormat).append(";");
            sb.append("channelTitle = " + this.channelTitle).append(";");
            sb.append("channelDescription = " + this.channelDescription).append(";");
            sb.append("channelCategory = " + this.channelCategory).append(";");
            sb.append("channelUrl = " + this.channelUrl).append(";");
            sb.append("channelLogo = " + this.channelLogo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceName == null ? 0 : serviceName.hashCode();
            _hash = 31 * _hash + delta;
            delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedUrl == null ? 0 : feedUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = feedFormat == null ? 0 : feedFormat.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelTitle == null ? 0 : channelTitle.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelDescription == null ? 0 : channelDescription.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelCategory == null ? 0 : channelCategory.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelUrl == null ? 0 : channelUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = channelLogo == null ? 0 : channelLogo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
