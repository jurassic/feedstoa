package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.stub.CloudStructStub;
import com.feedstoa.ws.stub.CategoryStructStub;
import com.feedstoa.ws.stub.ImageStructStub;
import com.feedstoa.ws.stub.TextInputStructStub;
import com.feedstoa.ws.stub.RssItemStub;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.stub.RssChannelStub;


// Wrapper class + bean combo.
public class RssChannelBean implements RssChannel, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssChannelBean.class.getName());

    // [1] With an embedded object.
    private RssChannelStub stub = null;

    // [2] Or, without an embedded object.
    private String title;
    private String link;
    private String description;
    private String language;
    private String copyright;
    private String managingEditor;
    private String webMaster;
    private String pubDate;
    private String lastBuildDate;
    private List<CategoryStruct> category;
    private String generator;
    private String docs;
    private CloudStructBean cloud;
    private Integer ttl;
    private ImageStructBean image;
    private String rating;
    private TextInputStructBean textInput;
    private List<Integer> skipHours;
    private List<String> skipDays;
    private List<RssItem> item;

    // Ctors.
    public RssChannelBean()
    {
        //this((String) null);
    }
    public RssChannelBean(String title, String link, String description, String language, String copyright, String managingEditor, String webMaster, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStructBean cloud, Integer ttl, ImageStructBean image, String rating, TextInputStructBean textInput, List<Integer> skipHours, List<String> skipDays, List<RssItem> item)
    {
        this.title = title;
        this.link = link;
        this.description = description;
        this.language = language;
        this.copyright = copyright;
        this.managingEditor = managingEditor;
        this.webMaster = webMaster;
        this.pubDate = pubDate;
        this.lastBuildDate = lastBuildDate;
        this.category = category;
        this.generator = generator;
        this.docs = docs;
        this.cloud = cloud;
        this.ttl = ttl;
        this.image = image;
        this.rating = rating;
        this.textInput = textInput;
        this.skipHours = skipHours;
        this.skipDays = skipDays;
        this.item = item;
    }
    public RssChannelBean(RssChannel stub)
    {
        if(stub instanceof RssChannelStub) {
            this.stub = (RssChannelStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setTitle(stub.getTitle());   
            setLink(stub.getLink());   
            setDescription(stub.getDescription());   
            setLanguage(stub.getLanguage());   
            setCopyright(stub.getCopyright());   
            setManagingEditor(stub.getManagingEditor());   
            setWebMaster(stub.getWebMaster());   
            setPubDate(stub.getPubDate());   
            setLastBuildDate(stub.getLastBuildDate());   
            setCategory(stub.getCategory());   
            setGenerator(stub.getGenerator());   
            setDocs(stub.getDocs());   
            CloudStruct cloud = stub.getCloud();
            if(cloud instanceof CloudStructBean) {
                setCloud((CloudStructBean) cloud);   
            } else {
                setCloud(new CloudStructBean(cloud));   
            }
            setTtl(stub.getTtl());   
            ImageStruct image = stub.getImage();
            if(image instanceof ImageStructBean) {
                setImage((ImageStructBean) image);   
            } else {
                setImage(new ImageStructBean(image));   
            }
            setRating(stub.getRating());   
            TextInputStruct textInput = stub.getTextInput();
            if(textInput instanceof TextInputStructBean) {
                setTextInput((TextInputStructBean) textInput);   
            } else {
                setTextInput(new TextInputStructBean(textInput));   
            }
            setSkipHours(stub.getSkipHours());   
            setSkipDays(stub.getSkipDays());   
            setItem(stub.getItem());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getLink()
    {
        if(getStub() != null) {
            return getStub().getLink();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.link;
        }
    }
    public void setLink(String link)
    {
        if(getStub() != null) {
            getStub().setLink(link);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.link = link;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public String getLanguage()
    {
        if(getStub() != null) {
            return getStub().getLanguage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.language;
        }
    }
    public void setLanguage(String language)
    {
        if(getStub() != null) {
            getStub().setLanguage(language);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.language = language;
        }
    }

    public String getCopyright()
    {
        if(getStub() != null) {
            return getStub().getCopyright();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.copyright;
        }
    }
    public void setCopyright(String copyright)
    {
        if(getStub() != null) {
            getStub().setCopyright(copyright);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.copyright = copyright;
        }
    }

    public String getManagingEditor()
    {
        if(getStub() != null) {
            return getStub().getManagingEditor();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.managingEditor;
        }
    }
    public void setManagingEditor(String managingEditor)
    {
        if(getStub() != null) {
            getStub().setManagingEditor(managingEditor);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.managingEditor = managingEditor;
        }
    }

    public String getWebMaster()
    {
        if(getStub() != null) {
            return getStub().getWebMaster();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.webMaster;
        }
    }
    public void setWebMaster(String webMaster)
    {
        if(getStub() != null) {
            getStub().setWebMaster(webMaster);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.webMaster = webMaster;
        }
    }

    public String getPubDate()
    {
        if(getStub() != null) {
            return getStub().getPubDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pubDate;
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getStub() != null) {
            getStub().setPubDate(pubDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pubDate = pubDate;
        }
    }

    public String getLastBuildDate()
    {
        if(getStub() != null) {
            return getStub().getLastBuildDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastBuildDate;
        }
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        if(getStub() != null) {
            getStub().setLastBuildDate(lastBuildDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastBuildDate = lastBuildDate;
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getStub() != null) {
            List<CategoryStruct> list = getStub().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructStub) {
                        elem = new CategoryStructBean(categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.category;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getStub() != null) {
                List<CategoryStruct> stub = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructStub elem = null;
                    if(categoryStruct instanceof CategoryStructStub) {
                        elem = (CategoryStructStub) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).getStub();
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructStub(categoryStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setCategory(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.category = category;  // ???

                List<CategoryStruct> beans = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructBean elem = null;
                    if(categoryStruct != null) {
                        if(categoryStruct instanceof CategoryStructBean) {
                            elem = (CategoryStructBean) categoryStruct;
                        } else {
                            elem = new CategoryStructBean(categoryStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.category = beans;
            }
        } else {
            this.category = null;
        }
    }

    public String getGenerator()
    {
        if(getStub() != null) {
            return getStub().getGenerator();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.generator;
        }
    }
    public void setGenerator(String generator)
    {
        if(getStub() != null) {
            getStub().setGenerator(generator);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.generator = generator;
        }
    }

    public String getDocs()
    {
        if(getStub() != null) {
            return getStub().getDocs();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.docs;
        }
    }
    public void setDocs(String docs)
    {
        if(getStub() != null) {
            getStub().setDocs(docs);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.docs = docs;
        }
    }

    public CloudStruct getCloud()
    {  
        if(getStub() != null) {
            // Note the object type.
            CloudStruct _stub_field = getStub().getCloud();
            if(_stub_field == null) {
                return null;
            } else {
                return new CloudStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.cloud;
        }
    }
    public void setCloud(CloudStruct cloud)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setCloud(cloud);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(cloud == null) {
                this.cloud = null;
            } else {
                if(cloud instanceof CloudStructBean) {
                    this.cloud = (CloudStructBean) cloud;
                } else {
                    this.cloud = new CloudStructBean(cloud);
                }
            }
        }
    }

    public Integer getTtl()
    {
        if(getStub() != null) {
            return getStub().getTtl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ttl;
        }
    }
    public void setTtl(Integer ttl)
    {
        if(getStub() != null) {
            getStub().setTtl(ttl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ttl = ttl;
        }
    }

    public ImageStruct getImage()
    {  
        if(getStub() != null) {
            // Note the object type.
            ImageStruct _stub_field = getStub().getImage();
            if(_stub_field == null) {
                return null;
            } else {
                return new ImageStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image;
        }
    }
    public void setImage(ImageStruct image)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setImage(image);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(image == null) {
                this.image = null;
            } else {
                if(image instanceof ImageStructBean) {
                    this.image = (ImageStructBean) image;
                } else {
                    this.image = new ImageStructBean(image);
                }
            }
        }
    }

    public String getRating()
    {
        if(getStub() != null) {
            return getStub().getRating();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rating;
        }
    }
    public void setRating(String rating)
    {
        if(getStub() != null) {
            getStub().setRating(rating);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rating = rating;
        }
    }

    public TextInputStruct getTextInput()
    {  
        if(getStub() != null) {
            // Note the object type.
            TextInputStruct _stub_field = getStub().getTextInput();
            if(_stub_field == null) {
                return null;
            } else {
                return new TextInputStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.textInput;
        }
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setTextInput(textInput);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(textInput == null) {
                this.textInput = null;
            } else {
                if(textInput instanceof TextInputStructBean) {
                    this.textInput = (TextInputStructBean) textInput;
                } else {
                    this.textInput = new TextInputStructBean(textInput);
                }
            }
        }
    }

    public List<Integer> getSkipHours()
    {
        if(getStub() != null) {
            return getStub().getSkipHours();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.skipHours;
        }
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        if(getStub() != null) {
            getStub().setSkipHours(skipHours);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.skipHours = skipHours;
        }
    }

    public List<String> getSkipDays()
    {
        if(getStub() != null) {
            return getStub().getSkipDays();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.skipDays;
        }
    }
    public void setSkipDays(List<String> skipDays)
    {
        if(getStub() != null) {
            getStub().setSkipDays(skipDays);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.skipDays = skipDays;
        }
    }

    public List<RssItem> getItem()
    {
        if(getStub() != null) {
            List<RssItem> list = getStub().getItem();
            if(list != null) {
                List<RssItem> bean = new ArrayList<RssItem>();
                for(RssItem rssItem : list) {
                    RssItemBean elem = null;
                    if(rssItem instanceof RssItemBean) {
                        elem = (RssItemBean) rssItem;
                    } else if(rssItem instanceof RssItemStub) {
                        elem = new RssItemBean(rssItem);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.item;
        }
    }
    public void setItem(List<RssItem> item)
    {
        if(item != null) {
            if(getStub() != null) {
                List<RssItem> stub = new ArrayList<RssItem>();
                for(RssItem rssItem : item) {
                    RssItemStub elem = null;
                    if(rssItem instanceof RssItemStub) {
                        elem = (RssItemStub) rssItem;
                    } else if(rssItem instanceof RssItemBean) {
                        elem = ((RssItemBean) rssItem).getStub();
                    } else if(rssItem instanceof RssItem) {
                        elem = new RssItemStub(rssItem);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setItem(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.item = item;  // ???

                List<RssItem> beans = new ArrayList<RssItem>();
                for(RssItem rssItem : item) {
                    RssItemBean elem = null;
                    if(rssItem != null) {
                        if(rssItem instanceof RssItemBean) {
                            elem = (RssItemBean) rssItem;
                        } else {
                            elem = new RssItemBean(rssItem);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.item = beans;
            }
        } else {
            this.item = null;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCopyright() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getManagingEditor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebMaster() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastBuildDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGenerator() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDocs() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCloud() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTtl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRating() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTextInput() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipHours() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipDays() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getItem() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public RssChannelStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RssChannelStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("title = " + this.title).append(";");
            sb.append("link = " + this.link).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("language = " + this.language).append(";");
            sb.append("copyright = " + this.copyright).append(";");
            sb.append("managingEditor = " + this.managingEditor).append(";");
            sb.append("webMaster = " + this.webMaster).append(";");
            sb.append("pubDate = " + this.pubDate).append(";");
            sb.append("lastBuildDate = " + this.lastBuildDate).append(";");
            sb.append("category = " + this.category).append(";");
            sb.append("generator = " + this.generator).append(";");
            sb.append("docs = " + this.docs).append(";");
            sb.append("cloud = " + this.cloud).append(";");
            sb.append("ttl = " + this.ttl).append(";");
            sb.append("image = " + this.image).append(";");
            sb.append("rating = " + this.rating).append(";");
            sb.append("textInput = " + this.textInput).append(";");
            sb.append("skipHours = " + this.skipHours).append(";");
            sb.append("skipDays = " + this.skipDays).append(";");
            sb.append("item = " + this.item).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = link == null ? 0 : link.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = language == null ? 0 : language.hashCode();
            _hash = 31 * _hash + delta;
            delta = copyright == null ? 0 : copyright.hashCode();
            _hash = 31 * _hash + delta;
            delta = managingEditor == null ? 0 : managingEditor.hashCode();
            _hash = 31 * _hash + delta;
            delta = webMaster == null ? 0 : webMaster.hashCode();
            _hash = 31 * _hash + delta;
            delta = pubDate == null ? 0 : pubDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = category == null ? 0 : category.hashCode();
            _hash = 31 * _hash + delta;
            delta = generator == null ? 0 : generator.hashCode();
            _hash = 31 * _hash + delta;
            delta = docs == null ? 0 : docs.hashCode();
            _hash = 31 * _hash + delta;
            delta = cloud == null ? 0 : cloud.hashCode();
            _hash = 31 * _hash + delta;
            delta = ttl == null ? 0 : ttl.hashCode();
            _hash = 31 * _hash + delta;
            delta = image == null ? 0 : image.hashCode();
            _hash = 31 * _hash + delta;
            delta = rating == null ? 0 : rating.hashCode();
            _hash = 31 * _hash + delta;
            delta = textInput == null ? 0 : textInput.hashCode();
            _hash = 31 * _hash + delta;
            delta = skipHours == null ? 0 : skipHours.hashCode();
            _hash = 31 * _hash + delta;
            delta = skipDays == null ? 0 : skipDays.hashCode();
            _hash = 31 * _hash + delta;
            delta = item == null ? 0 : item.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
