package com.feedstoa.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.stub.NotificationStructStub;
import com.feedstoa.ws.stub.GaeAppStructStub;
import com.feedstoa.ws.stub.ReferrerInfoStructStub;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.stub.FetchBaseStub;
import com.feedstoa.ws.stub.SecondaryFetchStub;


// Wrapper class + bean combo.
public class SecondaryFetchBean extends FetchBaseBean implements SecondaryFetch, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SecondaryFetchBean.class.getName());


    // [2] Or, without an embedded object.
    private String fetchRequest;

    // Ctors.
    public SecondaryFetchBean()
    {
        //this((String) null);
    }
    public SecondaryFetchBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public SecondaryFetchBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest, null, null);
    }
    public SecondaryFetchBean(String guid, String managerApp, Long appAcl, GaeAppStructBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, createdTime, modifiedTime);

        this.fetchRequest = fetchRequest;
    }
    public SecondaryFetchBean(SecondaryFetch stub)
    {
        if(stub instanceof SecondaryFetchStub) {
            super.setStub((FetchBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setManagerApp(stub.getManagerApp());   
            setAppAcl(stub.getAppAcl());   
            GaeAppStruct gaeApp = stub.getGaeApp();
            if(gaeApp instanceof GaeAppStructBean) {
                setGaeApp((GaeAppStructBean) gaeApp);   
            } else {
                setGaeApp(new GaeAppStructBean(gaeApp));   
            }
            setOwnerUser(stub.getOwnerUser());   
            setUserAcl(stub.getUserAcl());   
            setUser(stub.getUser());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFetchUrl(stub.getFetchUrl());   
            setFeedUrl(stub.getFeedUrl());   
            setChannelFeed(stub.getChannelFeed());   
            setReuseChannel(stub.isReuseChannel());   
            setMaxItemCount(stub.getMaxItemCount());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setFetchRequest(stub.getFetchRequest());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getManagerApp()
    {
        return super.getManagerApp();
    }
    public void setManagerApp(String managerApp)
    {
        super.setManagerApp(managerApp);
    }

    public Long getAppAcl()
    {
        return super.getAppAcl();
    }
    public void setAppAcl(Long appAcl)
    {
        super.setAppAcl(appAcl);
    }

    public GaeAppStruct getGaeApp()
    {
        return super.getGaeApp();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        super.setGaeApp(gaeApp);
    }

    public String getOwnerUser()
    {
        return super.getOwnerUser();
    }
    public void setOwnerUser(String ownerUser)
    {
        super.setOwnerUser(ownerUser);
    }

    public Long getUserAcl()
    {
        return super.getUserAcl();
    }
    public void setUserAcl(Long userAcl)
    {
        super.setUserAcl(userAcl);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public String getFetchUrl()
    {
        return super.getFetchUrl();
    }
    public void setFetchUrl(String fetchUrl)
    {
        super.setFetchUrl(fetchUrl);
    }

    public String getFeedUrl()
    {
        return super.getFeedUrl();
    }
    public void setFeedUrl(String feedUrl)
    {
        super.setFeedUrl(feedUrl);
    }

    public String getChannelFeed()
    {
        return super.getChannelFeed();
    }
    public void setChannelFeed(String channelFeed)
    {
        super.setChannelFeed(channelFeed);
    }

    public Boolean isReuseChannel()
    {
        return super.isReuseChannel();
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        super.setReuseChannel(reuseChannel);
    }

    public Integer getMaxItemCount()
    {
        return super.getMaxItemCount();
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        super.setMaxItemCount(maxItemCount);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public String getFetchRequest()
    {
        if(getStub() != null) {
            return getStub().getFetchRequest();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchRequest;
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getStub() != null) {
            getStub().setFetchRequest(fetchRequest);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchRequest = fetchRequest;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public SecondaryFetchStub getStub()
    {
        return (SecondaryFetchStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("fetchRequest = " + this.fetchRequest).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
