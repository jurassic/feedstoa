package com.feedstoa.cert.af;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseOAuthConsumerRegistry.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;

    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD:
        consumerSecretMap.put("1235a708-9154-459d-bc0c-e6fc3c7f6d4a", "a5cbd96d-5898-400d-9ddb-5ff33c435f99");  // FeedStoaApp
        consumerSecretMap.put("21f2ba6e-8726-430a-8d92-d82b81d586d8", "48cc3eb9-e3c5-4caa-970e-ebf7da1107a4");  // FeedStoaApp + FeedStoaWeb
        consumerSecretMap.put("a701ea1b-e300-48c2-8941-0bd714cfb5e5", "3e4e369d-4489-4da5-99da-1a362a3357f0");  // FeedStoaApp + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
