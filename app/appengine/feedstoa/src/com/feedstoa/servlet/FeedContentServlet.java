package com.feedstoa.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.feedstoa.af.core.JerseyClient;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.FeedContentJsBean;
import com.feedstoa.helper.FeedContentHelper;
import com.feedstoa.wa.service.FeedContentWebService;
import com.feedstoa.wa.service.UserWebService;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StatusCode;


// Mainly, for ajax calls....
// Payload: { "feedContent": feedContentBean }
public class FeedContentServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedContentServlet.class.getName());
    
    // Arbitrary cutoff. Note: 500 is the max length for String type...
    //private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // temporary
    private static final String QUERY_PARAM_TARGET_URL = "targetUrl";
    private static final String QUERY_PARAM_FETCH = "fetch";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private FeedContentWebService feedContentWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private FeedContentWebService getFeedContentService()
    {
        if(feedContentWebService == null) {
            feedContentWebService = new FeedContentWebService();
        }
        return feedContentWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
        
        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) {
            log.fine("requestUrl = " + requestUrl);
            log.fine("contextPath = " + contextPath);
            log.fine("servletPath = " + servletPath);
            log.fine("pathInfo = " + pathInfo);
            log.fine("queryString = " + queryString);
        }

        String targetUrl = null;
        String[] targetUrls = req.getParameterValues(QUERY_PARAM_TARGET_URL);
        if(targetUrls != null && targetUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            targetUrl = URLDecoder.decode(targetUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the targetUrl???
            // ....
        } else {
            // error...
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("targetUrl = " + targetUrl);
        
        boolean fetch = true;   // default value.
        String[] fetchStrs = req.getParameterValues(QUERY_PARAM_FETCH);
        if(fetchStrs != null && fetchStrs.length > 0) {
            if(fetchStrs[0].equals("0") || fetchStrs[0].equalsIgnoreCase("false")) {  // temporary
                fetch = false;
            }
            // else ignore.
        }
        if(log.isLoggable(Level.FINE)) log.fine("fetch = " + fetch);
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        
        
        // ????
        // TBD....
        FeedContentJsBean feedContent = FeedContentHelper.getInstance().getFeedContentByFeedUrl(targetUrl);
        if(feedContent == null) {
            if(fetch == true) {
                feedContent = new FeedContentJsBean();
                String guid = GUID.generate();
                feedContent.setGuid(guid);
//                feedContent.setTargetUrl(targetUrl);
                try {
                    URL url = new URL(targetUrl);  // ???
                    String query = url.getQuery();
                    String pageUrl = targetUrl;
                    if(query != null) {
                        int qidx = targetUrl.indexOf("?");
                        if(qidx > 0) {
                            pageUrl = targetUrl.substring(0, qidx);
                        }
                    }
//                    feedContent.setPageUrl(pageUrl);
//                    if(query != null && !query.isEmpty()) {
//                        List<String> params = QueryStringUtil.parseQueryString(query);
//                        feedContent.setQueryString(query); 
//                        feedContent.setQueryParams(params);                    
//                    }
                } catch(MalformedURLException e) {
                    log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                    // ignore and continue???? or, bail out???
                    resp.setStatus(StatusCode.BAD_REQUEST);
                    return;
                }
//                feedContent.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  /// ???
                
                FeedContent bean = FeedContentWebService.convertFeedContentJsBeanToBean(feedContent);
//                try {
//                    bean = FetchProcessor.getInstance().processPageFetch(bean);
//                    feedContent = FeedContentWebService.convertFeedContentToJsBean(bean);
//                } catch (BaseException e) {
//                    log.log(Level.WARNING, "Failed to fetch the page: targetUrl = " + targetUrl, e);
//                    feedContent = null;
//                }
                
                if(feedContent != null) {
                    try {
                        feedContent = getFeedContentService().constructFeedContent(feedContent);
                    } catch (WebException e) {
                        log.log(Level.WARNING, "Failed to save the feedContent with targetUrl = " + targetUrl, e);
                        feedContent = null;
                    }
                } 
            } else {
                // ???
            }
        }
        
        if(feedContent != null) {
            String jsonStr = feedContent.toJsonString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        
        // TBD: ...
        
        
        
        
        log.info("doGet(): BOTTOM");
    }

    
    
    // Note:
    // Currently, only GET is "officially" supported...
    // ...
    
    
    // TBD:
    // depending on feedContent.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Method not allowed.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Method not allowed.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
