package com.feedstoa.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.feedstoa.af.core.JerseyClient;
import com.feedstoa.app.fetch.FetchProcessor;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.ChannelFeedJsBean;
import com.feedstoa.fe.bean.FeedItemJsBean;
import com.feedstoa.helper.ChannelFeedHelper;
import com.feedstoa.helper.URLHelper;
import com.feedstoa.util.JsonBeanUtil;
import com.feedstoa.wa.service.ChannelFeedWebService;
import com.feedstoa.wa.service.FeedItemWebService;
import com.feedstoa.wa.service.UserWebService;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StatusCode;


// Mainly, for ajax calls....
// Payload: { "channelFeed": channelFeedBean, "feedItem": feedItemBean }
// Due to the way we implemented FeedContent generation...
// normally, only a single object can be added to the payload...
public class ChannelFeedServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedServlet.class.getName());
    
    // Arbitrary cutoff. Note: 500 is the max length for String type...
    //private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // temporary
    private static final String QUERY_PARAM_TARGET_URL = "targetUrl";
    private static final String QUERY_PARAM_FETCH = "fetch";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private ChannelFeedWebService channelFeedWebService = null;
    private FeedItemWebService feedItemWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ChannelFeedWebService getChannelFeedService()
    {
        if(channelFeedWebService == null) {
            channelFeedWebService = new ChannelFeedWebService();
        }
        return channelFeedWebService;
    }
    private FeedItemWebService getFeedItemService()
    {
        if(feedItemWebService == null) {
            feedItemWebService = new FeedItemWebService();
        }
        return feedItemWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
        
        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String targetUrl = null;
        String[] targetUrls = req.getParameterValues(QUERY_PARAM_TARGET_URL);
        if(targetUrls != null && targetUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            targetUrl = URLDecoder.decode(targetUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the targetUrl???
            // ....
        } else {
            // error...
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        log.fine("targetUrl = " + targetUrl);
        
        boolean fetch = true;   // default value.
        String[] fetchStrs = req.getParameterValues(QUERY_PARAM_FETCH);
        if(fetchStrs != null && fetchStrs.length > 0) {
            if(fetchStrs[0].equals("0") || fetchStrs[0].equalsIgnoreCase("false")) {  // temporary
                fetch = false;
            }
            // else ignore.
        }
        log.fine("fetch = " + fetch);
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        
        
        // ????
        // TBD....
        ChannelFeedJsBean channelFeed = ChannelFeedHelper.getInstance().getChannelFeedByTargetUrl(targetUrl);
        if(channelFeed == null) {
            if(fetch == true) {
                channelFeed = new ChannelFeedJsBean();
                String guid = GUID.generate();
                channelFeed.setGuid(guid);
//                channelFeed.setTargetUrl(targetUrl);
                try {
                    URL url = new URL(targetUrl);  // ???
                    String query = url.getQuery();
                    String pageUrl = targetUrl;
                    if(query != null) {
                        int qidx = targetUrl.indexOf("?");
                        if(qidx > 0) {
                            pageUrl = targetUrl.substring(0, qidx);
                        }
                    }
//                    channelFeed.setPageUrl(pageUrl);
//                    if(query != null && !query.isEmpty()) {
//                        List<String> params = QueryStringUtil.parseQueryString(query);
//                        channelFeed.setQueryString(query); 
//                        channelFeed.setQueryParams(params);                    
//                    }
                } catch(MalformedURLException e) {
                    log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                    // ignore and continue???? or, bail out???
                    resp.setStatus(StatusCode.BAD_REQUEST);
                    return;
                }
//                channelFeed.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  /// ???
                
                // TBD:
//                ChannelFeed bean = ChannelFeedWebService.convertChannelFeedJsBeanToBean(channelFeed);
//                try {
//                    bean = FetchProcessor.getInstance().processPageFetch(bean);
//                    channelFeed = ChannelFeedWebService.convertChannelFeedToJsBean(bean);
//                } catch (BaseException e) {
//                    log.log(Level.WARNING, "Failed to fetch the page: targetUrl = " + targetUrl, e);
//                    channelFeed = null;
//                }
                
                if(channelFeed != null) {
                    try {
                        channelFeed = getChannelFeedService().constructChannelFeed(channelFeed);
                    } catch (WebException e) {
                        log.log(Level.WARNING, "Failed to save the channelFeed with targetUrl = " + targetUrl, e);
                        channelFeed = null;
                    }
                } 
            } else {
                // ???
            }
        }
        
        if(channelFeed != null) {
            String jsonStr = channelFeed.toJsonString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        
        // TBD: ...
        
/*      
        FeedItemJsBean feedItem = FeedItemHelper.getInstance().getFeedItemByTargetUrl(targetUrl);
        if(feedItem == null) {
            if(fetch == true) {
                feedItem = new FeedItemJsBean();
                String guid = GUID.generate();
                feedItem.setGuid(guid);
//                feedItem.setTargetUrl(targetUrl);
                try {
                    URL url = new URL(targetUrl);  // ???
                    String query = url.getQuery();
                    String pageUrl = targetUrl;
                    if(query != null) {
                        int qidx = targetUrl.indexOf("?");
                        if(qidx > 0) {
                            pageUrl = targetUrl.substring(0, qidx);
                        }
                    }
//                    feedItem.setPageUrl(pageUrl);
//                    if(query != null && !query.isEmpty()) {
//                        List<String> params = QueryStringUtil.parseQueryString(query);
//                        feedItem.setQueryString(query); 
//                        feedItem.setQueryParams(params);                    
//                    }
                } catch(MalformedURLException e) {
                    log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                    // ignore and continue???? or, bail out???
                    resp.setStatus(StatusCode.BAD_REQUEST);
                    return;
                }
//                feedItem.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  /// ???
                
                FeedItem bean = FeedItemWebService.convertFeedItemJsBeanToBean(feedItem);
                try {
                    bean = FetchProcessor.getInstance().processPageFetch(bean);
                    feedItem = FeedItemWebService.convertFeedItemToJsBean(bean);
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Failed to fetch the page: targetUrl = " + targetUrl, e);
                    feedItem = null;
                }
                
                if(feedItem != null) {
                    try {
                        feedItem = getFeedItemService().constructFeedItem(feedItem);
                    } catch (WebException e) {
                        log.log(Level.WARNING, "Failed to save the feedItem with targetUrl = " + targetUrl, e);
                        feedItem = null;
                    }
                } 
            } else {
                // ???
            }
        }
        
        if(feedItem != null) {
            String jsonStr = feedItem.toJsonString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
*/
        
        
        
        
        log.info("doGet(): BOTTOM");
    }

    
    
    // Note:
    // Currently, only GET is "officially" supported...
    // ...
    
    
    // TBD:
    // depending on channelFeed.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");

        // TBD: Check Accept header. Etc...
        // For now, both input and output are application/json.
        ChannelFeedJsBean outChannelFeedBean = null;
        FeedItemJsBean outFeedItemBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();

            ChannelFeedJsBean inChannelFeedBean = null;
            FeedItemJsBean inFeedItemBean = null;
            Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
            for(String key : jsonObjectMap.keySet()) {
                String json = jsonObjectMap.get(key);
                log.info("key = " + key + "; json = " + json);
                
                if(key.equals("channelFeed")) {
                    inChannelFeedBean = ChannelFeedJsBean.fromJsonString(json);
                } else if(key.equals("feedItem")) {
                    inFeedItemBean = FeedItemJsBean.fromJsonString(json);
                } else {
                    // TBD:
                    // This should not happen, for now.
                    log.warning("Unregconized key in the json input string. key = " + key);
                }
            }

            // TBD: Make sure neither is null???
            // Or, just process what is inputted....
            
            // temporary!!!
            if(inChannelFeedBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // ????
                // temporary....
                // etc. ...
                // ...
            	
            	// temporary
            	String serviceUrl = inChannelFeedBean.getFeedServiceUrl();
            	if(serviceUrl == null || serviceUrl.isEmpty()) {
            		serviceUrl = URLHelper.getInstance().getTopLevelURLFromRequest(req);
            		inChannelFeedBean.setFeedServiceUrl(serviceUrl);
            	}
            	// ....
            	

                outChannelFeedBean = getChannelFeedService().constructChannelFeed(inChannelFeedBean);
            }
            
            // temporary!!!
            if(inFeedItemBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // ????
                // temporary....
                // etc. ...
                // ...

                outFeedItemBean = getFeedItemService().constructFeedItem(inFeedItemBean);
            }

            // ...

        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to save channelFeed.", e);
        }
        
        // temporary
        boolean suc = false;
        if(outChannelFeedBean != null) {
            suc = true;
        }

        // Response.
        if(suc == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            //resp.setHeader("Cache-Control", "no-cache");    // ?????

            // Location header???
            //String guid = outChannelFeedBean.getGuid();
            
            // Construct output
            Map<String, String> jsonMap = new HashMap<String, String>();
            if(outChannelFeedBean != null) {
                jsonMap.put("channelFeed", outChannelFeedBean.toJsonString());
            }
            if(outFeedItemBean != null) {
                jsonMap.put("feedItem", outFeedItemBean.toJsonString());
            }
            String output = JsonBeanUtil.generateJsonObjectString(jsonMap);

            PrintWriter out = resp.getWriter();
            out.write(output);
        } else {
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
        }

        log.info("doPost(): BOTTOM");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPut(): TOP");

        // TBD: Check Accept header. Etc...

        String pathInfo = req.getPathInfo();
        String guid = URLHelper.getInstance().getGuidFromPathInfo(pathInfo);        

        // guid should not be null!!!
        if(guid != null && !guid.isEmpty()) {
            Boolean suc = null;
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                
                ChannelFeedJsBean inChannelFeedBean = null;
                FeedItemJsBean inFeedItemBean = null;
                Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
                for(String key : jsonObjectMap.keySet()) {
                    String json = jsonObjectMap.get(key);
                    log.info("key = " + key + "; json = " + json);
                    
                    if(key.equals("channelFeed")) {
                        inChannelFeedBean = ChannelFeedJsBean.fromJsonString(json);
                    } else if(key.equals("feedItem")) {
                        inFeedItemBean = FeedItemJsBean.fromJsonString(json);
                    } else {
                        // TBD:
                        // This should not happen, for now.
                        log.warning("Unregconized key in the json input string. key = " + key);
                    }
                }

                // TBD: Make sure neither is null???
                // Or, just process what is inputted....

                Boolean sucChannelFeed = null;
                if(inChannelFeedBean != null) {
                    String beanGuid = inChannelFeedBean.getGuid();
                    if(guid.equals(beanGuid)) {
                        inChannelFeedBean.setModifiedTime(System.currentTimeMillis());
                        sucChannelFeed = getChannelFeedService().updateChannelFeed(inChannelFeedBean);
                        log.finer("sucChannelFeed = " + sucChannelFeed);
                    } else {
                        log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                        // ???
                    }
                }

                Boolean sucFeedItem = null;
                if(inFeedItemBean != null) {
                    String beanGuid = inFeedItemBean.getGuid();
                    if(guid.equals(beanGuid)) {
                        inFeedItemBean.setModifiedTime(System.currentTimeMillis());
                        sucFeedItem = getFeedItemService().updateFeedItem(inFeedItemBean);
                        log.finer("sucFeedItem = " + sucFeedItem);
                    } else {
                        log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                        // ???
                    }
                }

                // TBD: ...
                if((sucChannelFeed==null || Boolean.TRUE.equals(sucChannelFeed)) && (sucFeedItem==null || Boolean.TRUE.equals(sucFeedItem))) {  // ????
                    suc = Boolean.TRUE;
                }
                // ...

            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(Boolean.TRUE.equals(suc)) {
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            // ???
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }

        log.info("doPut(): BOTTOM");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
