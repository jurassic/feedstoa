package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.UserWebsiteStruct;
import com.feedstoa.af.bean.UserWebsiteStructBean;
import com.feedstoa.fe.bean.UserWebsiteStructJsBean;


public class UserWebsiteStructWebUtil
{
    private static final Logger log = Logger.getLogger(UserWebsiteStructWebUtil.class.getName());

    // Static methods only.
    private UserWebsiteStructWebUtil() {}
    

    public static UserWebsiteStructJsBean convertUserWebsiteStructToJsBean(UserWebsiteStruct userWebsiteStruct)
    {
        UserWebsiteStructJsBean jsBean = null;
        if(userWebsiteStruct != null) {
            jsBean = new UserWebsiteStructJsBean();
            jsBean.setUuid(userWebsiteStruct.getUuid());
            jsBean.setPrimarySite(userWebsiteStruct.getPrimarySite());
            jsBean.setHomePage(userWebsiteStruct.getHomePage());
            jsBean.setBlogSite(userWebsiteStruct.getBlogSite());
            jsBean.setPortfolioSite(userWebsiteStruct.getPortfolioSite());
            jsBean.setTwitterProfile(userWebsiteStruct.getTwitterProfile());
            jsBean.setFacebookProfile(userWebsiteStruct.getFacebookProfile());
            jsBean.setGooglePlusProfile(userWebsiteStruct.getGooglePlusProfile());
            jsBean.setNote(userWebsiteStruct.getNote());
        }
        return jsBean;
    }

    public static UserWebsiteStruct convertUserWebsiteStructJsBeanToBean(UserWebsiteStructJsBean jsBean)
    {
        UserWebsiteStructBean userWebsiteStruct = null;
        if(jsBean != null) {
            userWebsiteStruct = new UserWebsiteStructBean();
            userWebsiteStruct.setUuid(jsBean.getUuid());
            userWebsiteStruct.setPrimarySite(jsBean.getPrimarySite());
            userWebsiteStruct.setHomePage(jsBean.getHomePage());
            userWebsiteStruct.setBlogSite(jsBean.getBlogSite());
            userWebsiteStruct.setPortfolioSite(jsBean.getPortfolioSite());
            userWebsiteStruct.setTwitterProfile(jsBean.getTwitterProfile());
            userWebsiteStruct.setFacebookProfile(jsBean.getFacebookProfile());
            userWebsiteStruct.setGooglePlusProfile(jsBean.getGooglePlusProfile());
            userWebsiteStruct.setNote(jsBean.getNote());
        }
        return userWebsiteStruct;
    }

}
