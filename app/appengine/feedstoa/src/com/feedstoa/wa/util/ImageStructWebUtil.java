package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.ImageStruct;
import com.feedstoa.af.bean.ImageStructBean;
import com.feedstoa.fe.bean.ImageStructJsBean;


public class ImageStructWebUtil
{
    private static final Logger log = Logger.getLogger(ImageStructWebUtil.class.getName());

    // Static methods only.
    private ImageStructWebUtil() {}
    

    public static ImageStructJsBean convertImageStructToJsBean(ImageStruct imageStruct)
    {
        ImageStructJsBean jsBean = null;
        if(imageStruct != null) {
            jsBean = new ImageStructJsBean();
            jsBean.setUrl(imageStruct.getUrl());
            jsBean.setTitle(imageStruct.getTitle());
            jsBean.setLink(imageStruct.getLink());
            jsBean.setWidth(imageStruct.getWidth());
            jsBean.setHeight(imageStruct.getHeight());
            jsBean.setDescription(imageStruct.getDescription());
        }
        return jsBean;
    }

    public static ImageStruct convertImageStructJsBeanToBean(ImageStructJsBean jsBean)
    {
        ImageStructBean imageStruct = null;
        if(jsBean != null) {
            imageStruct = new ImageStructBean();
            imageStruct.setUrl(jsBean.getUrl());
            imageStruct.setTitle(jsBean.getTitle());
            imageStruct.setLink(jsBean.getLink());
            imageStruct.setWidth(jsBean.getWidth());
            imageStruct.setHeight(jsBean.getHeight());
            imageStruct.setDescription(jsBean.getDescription());
        }
        return imageStruct;
    }

}
