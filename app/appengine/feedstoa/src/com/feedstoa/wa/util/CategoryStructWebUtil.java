package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.af.bean.CategoryStructBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;


public class CategoryStructWebUtil
{
    private static final Logger log = Logger.getLogger(CategoryStructWebUtil.class.getName());

    // Static methods only.
    private CategoryStructWebUtil() {}
    

    public static CategoryStructJsBean convertCategoryStructToJsBean(CategoryStruct categoryStruct)
    {
        CategoryStructJsBean jsBean = null;
        if(categoryStruct != null) {
            jsBean = new CategoryStructJsBean();
            jsBean.setUuid(categoryStruct.getUuid());
            jsBean.setDomain(categoryStruct.getDomain());
            jsBean.setLabel(categoryStruct.getLabel());
            jsBean.setTitle(categoryStruct.getTitle());
        }
        return jsBean;
    }

    public static CategoryStruct convertCategoryStructJsBeanToBean(CategoryStructJsBean jsBean)
    {
        CategoryStructBean categoryStruct = null;
        if(jsBean != null) {
            categoryStruct = new CategoryStructBean();
            categoryStruct.setUuid(jsBean.getUuid());
            categoryStruct.setDomain(jsBean.getDomain());
            categoryStruct.setLabel(jsBean.getLabel());
            categoryStruct.setTitle(jsBean.getTitle());
        }
        return categoryStruct;
    }

}
