package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.RedirectRule;
import com.feedstoa.af.bean.RedirectRuleBean;
import com.feedstoa.fe.bean.RedirectRuleJsBean;


public class RedirectRuleWebUtil
{
    private static final Logger log = Logger.getLogger(RedirectRuleWebUtil.class.getName());

    // Static methods only.
    private RedirectRuleWebUtil() {}
    

    public static RedirectRuleJsBean convertRedirectRuleToJsBean(RedirectRule redirectRule)
    {
        RedirectRuleJsBean jsBean = null;
        if(redirectRule != null) {
            jsBean = new RedirectRuleJsBean();
            jsBean.setRedirectType(redirectRule.getRedirectType());
            jsBean.setPrecedence(redirectRule.getPrecedence());
            jsBean.setSourceDomain(redirectRule.getSourceDomain());
            jsBean.setSourcePath(redirectRule.getSourcePath());
            jsBean.setTargetDomain(redirectRule.getTargetDomain());
            jsBean.setTargetPath(redirectRule.getTargetPath());
            jsBean.setNote(redirectRule.getNote());
            jsBean.setStatus(redirectRule.getStatus());
        }
        return jsBean;
    }

    public static RedirectRule convertRedirectRuleJsBeanToBean(RedirectRuleJsBean jsBean)
    {
        RedirectRuleBean redirectRule = null;
        if(jsBean != null) {
            redirectRule = new RedirectRuleBean();
            redirectRule.setRedirectType(jsBean.getRedirectType());
            redirectRule.setPrecedence(jsBean.getPrecedence());
            redirectRule.setSourceDomain(jsBean.getSourceDomain());
            redirectRule.setSourcePath(jsBean.getSourcePath());
            redirectRule.setTargetDomain(jsBean.getTargetDomain());
            redirectRule.setTargetPath(jsBean.getTargetPath());
            redirectRule.setNote(jsBean.getNote());
            redirectRule.setStatus(jsBean.getStatus());
        }
        return redirectRule;
    }

}
