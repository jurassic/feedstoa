package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.af.bean.RssItemBean;
import com.feedstoa.fe.bean.EnclosureStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.RssItemJsBean;


public class RssItemWebUtil
{
    private static final Logger log = Logger.getLogger(RssItemWebUtil.class.getName());

    // Static methods only.
    private RssItemWebUtil() {}
    

    public static RssItemJsBean convertRssItemToJsBean(RssItem rssItem)
    {
        RssItemJsBean jsBean = null;
        if(rssItem != null) {
            jsBean = new RssItemJsBean();
            jsBean.setTitle(rssItem.getTitle());
            jsBean.setLink(rssItem.getLink());
            jsBean.setDescription(rssItem.getDescription());
            jsBean.setAuthor(rssItem.getAuthor());
            List<CategoryStructJsBean> categoryJsBeans = new ArrayList<CategoryStructJsBean>();
            List<CategoryStruct> categoryBeans = rssItem.getCategory();
            if(categoryBeans != null) {
                for(CategoryStruct category : categoryBeans) {
                    CategoryStructJsBean jB = CategoryStructWebUtil.convertCategoryStructToJsBean(category);
                    categoryJsBeans.add(jB); 
                }
            }
            jsBean.setCategory(categoryJsBeans);
            jsBean.setComments(rssItem.getComments());
            jsBean.setEnclosure(EnclosureStructWebUtil.convertEnclosureStructToJsBean(rssItem.getEnclosure()));
            jsBean.setGuid(rssItem.getGuid());
            jsBean.setPubDate(rssItem.getPubDate());
            jsBean.setSource(UriStructWebUtil.convertUriStructToJsBean(rssItem.getSource()));
        }
        return jsBean;
    }

    public static RssItem convertRssItemJsBeanToBean(RssItemJsBean jsBean)
    {
        RssItemBean rssItem = null;
        if(jsBean != null) {
            rssItem = new RssItemBean();
            rssItem.setTitle(jsBean.getTitle());
            rssItem.setLink(jsBean.getLink());
            rssItem.setDescription(jsBean.getDescription());
            rssItem.setAuthor(jsBean.getAuthor());
            List<CategoryStruct> categoryBeans = new ArrayList<CategoryStruct>();
            List<CategoryStructJsBean> categoryJsBeans = jsBean.getCategory();
            if(categoryJsBeans != null) {
                for(CategoryStructJsBean category : categoryJsBeans) {
                    CategoryStruct b = CategoryStructWebUtil.convertCategoryStructJsBeanToBean(category);
                    categoryBeans.add(b); 
                }
            }
            rssItem.setCategory(categoryBeans);
            rssItem.setComments(jsBean.getComments());
            rssItem.setEnclosure(EnclosureStructWebUtil.convertEnclosureStructJsBeanToBean(jsBean.getEnclosure()));

            rssItem.setGuid(jsBean.getGuid());
            rssItem.setPubDate(jsBean.getPubDate());
            rssItem.setSource(UriStructWebUtil.convertUriStructJsBeanToBean(jsBean.getSource()));

        }
        return rssItem;
    }

}
