package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.UserStruct;
import com.feedstoa.af.bean.UserStructBean;
import com.feedstoa.fe.bean.UserStructJsBean;


public class UserStructWebUtil
{
    private static final Logger log = Logger.getLogger(UserStructWebUtil.class.getName());

    // Static methods only.
    private UserStructWebUtil() {}
    

    public static UserStructJsBean convertUserStructToJsBean(UserStruct userStruct)
    {
        UserStructJsBean jsBean = null;
        if(userStruct != null) {
            jsBean = new UserStructJsBean();
            jsBean.setUuid(userStruct.getUuid());
            jsBean.setName(userStruct.getName());
            jsBean.setEmail(userStruct.getEmail());
            jsBean.setUrl(userStruct.getUrl());
        }
        return jsBean;
    }

    public static UserStruct convertUserStructJsBeanToBean(UserStructJsBean jsBean)
    {
        UserStructBean userStruct = null;
        if(jsBean != null) {
            userStruct = new UserStructBean();
            userStruct.setUuid(jsBean.getUuid());
            userStruct.setName(jsBean.getName());
            userStruct.setEmail(jsBean.getEmail());
            userStruct.setUrl(jsBean.getUrl());
        }
        return userStruct;
    }

}
