package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.af.bean.RssChannelBean;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.ImageStructJsBean;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.fe.bean.RssItemJsBean;
import com.feedstoa.fe.bean.RssChannelJsBean;


public class RssChannelWebUtil
{
    private static final Logger log = Logger.getLogger(RssChannelWebUtil.class.getName());

    // Static methods only.
    private RssChannelWebUtil() {}
    

    public static RssChannelJsBean convertRssChannelToJsBean(RssChannel rssChannel)
    {
        RssChannelJsBean jsBean = null;
        if(rssChannel != null) {
            jsBean = new RssChannelJsBean();
            jsBean.setTitle(rssChannel.getTitle());
            jsBean.setLink(rssChannel.getLink());
            jsBean.setDescription(rssChannel.getDescription());
            jsBean.setLanguage(rssChannel.getLanguage());
            jsBean.setCopyright(rssChannel.getCopyright());
            jsBean.setManagingEditor(rssChannel.getManagingEditor());
            jsBean.setWebMaster(rssChannel.getWebMaster());
            jsBean.setPubDate(rssChannel.getPubDate());
            jsBean.setLastBuildDate(rssChannel.getLastBuildDate());
            List<CategoryStructJsBean> categoryJsBeans = new ArrayList<CategoryStructJsBean>();
            List<CategoryStruct> categoryBeans = rssChannel.getCategory();
            if(categoryBeans != null) {
                for(CategoryStruct category : categoryBeans) {
                    CategoryStructJsBean jB = CategoryStructWebUtil.convertCategoryStructToJsBean(category);
                    categoryJsBeans.add(jB); 
                }
            }
            jsBean.setCategory(categoryJsBeans);
            jsBean.setGenerator(rssChannel.getGenerator());
            jsBean.setDocs(rssChannel.getDocs());
            jsBean.setCloud(CloudStructWebUtil.convertCloudStructToJsBean(rssChannel.getCloud()));
            jsBean.setTtl(rssChannel.getTtl());
            jsBean.setImage(ImageStructWebUtil.convertImageStructToJsBean(rssChannel.getImage()));
            jsBean.setRating(rssChannel.getRating());
            jsBean.setTextInput(TextInputStructWebUtil.convertTextInputStructToJsBean(rssChannel.getTextInput()));
            jsBean.setSkipHours(rssChannel.getSkipHours());
            jsBean.setSkipDays(rssChannel.getSkipDays());
            List<RssItemJsBean> itemJsBeans = new ArrayList<RssItemJsBean>();
            List<RssItem> itemBeans = rssChannel.getItem();
            if(itemBeans != null) {
                for(RssItem item : itemBeans) {
                    RssItemJsBean jB = RssItemWebUtil.convertRssItemToJsBean(item);
                    itemJsBeans.add(jB); 
                }
            }
            jsBean.setItem(itemJsBeans);
        }
        return jsBean;
    }

    public static RssChannel convertRssChannelJsBeanToBean(RssChannelJsBean jsBean)
    {
        RssChannelBean rssChannel = null;
        if(jsBean != null) {
            rssChannel = new RssChannelBean();
            rssChannel.setTitle(jsBean.getTitle());
            rssChannel.setLink(jsBean.getLink());
            rssChannel.setDescription(jsBean.getDescription());
            rssChannel.setLanguage(jsBean.getLanguage());
            rssChannel.setCopyright(jsBean.getCopyright());
            rssChannel.setManagingEditor(jsBean.getManagingEditor());
            rssChannel.setWebMaster(jsBean.getWebMaster());
            rssChannel.setPubDate(jsBean.getPubDate());
            rssChannel.setLastBuildDate(jsBean.getLastBuildDate());
            List<CategoryStruct> categoryBeans = new ArrayList<CategoryStruct>();
            List<CategoryStructJsBean> categoryJsBeans = jsBean.getCategory();
            if(categoryJsBeans != null) {
                for(CategoryStructJsBean category : categoryJsBeans) {
                    CategoryStruct b = CategoryStructWebUtil.convertCategoryStructJsBeanToBean(category);
                    categoryBeans.add(b); 
                }
            }
            rssChannel.setCategory(categoryBeans);
            rssChannel.setGenerator(jsBean.getGenerator());
            rssChannel.setDocs(jsBean.getDocs());
            rssChannel.setCloud(CloudStructWebUtil.convertCloudStructJsBeanToBean(jsBean.getCloud()));

            rssChannel.setTtl(jsBean.getTtl());
            rssChannel.setImage(ImageStructWebUtil.convertImageStructJsBeanToBean(jsBean.getImage()));

            rssChannel.setRating(jsBean.getRating());
            rssChannel.setTextInput(TextInputStructWebUtil.convertTextInputStructJsBeanToBean(jsBean.getTextInput()));

            rssChannel.setSkipHours(jsBean.getSkipHours());
            rssChannel.setSkipDays(jsBean.getSkipDays());
            List<RssItem> itemBeans = new ArrayList<RssItem>();
            List<RssItemJsBean> itemJsBeans = jsBean.getItem();
            if(itemJsBeans != null) {
                for(RssItemJsBean item : itemJsBeans) {
                    RssItem b = RssItemWebUtil.convertRssItemJsBeanToBean(item);
                    itemBeans.add(b); 
                }
            }
            rssChannel.setItem(itemBeans);
        }
        return rssChannel;
    }

}
