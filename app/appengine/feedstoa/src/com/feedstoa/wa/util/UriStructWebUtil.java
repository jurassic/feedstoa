package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.UriStruct;
import com.feedstoa.af.bean.UriStructBean;
import com.feedstoa.fe.bean.UriStructJsBean;


public class UriStructWebUtil
{
    private static final Logger log = Logger.getLogger(UriStructWebUtil.class.getName());

    // Static methods only.
    private UriStructWebUtil() {}
    

    public static UriStructJsBean convertUriStructToJsBean(UriStruct uriStruct)
    {
        UriStructJsBean jsBean = null;
        if(uriStruct != null) {
            jsBean = new UriStructJsBean();
            jsBean.setUuid(uriStruct.getUuid());
            jsBean.setHref(uriStruct.getHref());
            jsBean.setRel(uriStruct.getRel());
            jsBean.setType(uriStruct.getType());
            jsBean.setLabel(uriStruct.getLabel());
        }
        return jsBean;
    }

    public static UriStruct convertUriStructJsBeanToBean(UriStructJsBean jsBean)
    {
        UriStructBean uriStruct = null;
        if(jsBean != null) {
            uriStruct = new UriStructBean();
            uriStruct.setUuid(jsBean.getUuid());
            uriStruct.setHref(jsBean.getHref());
            uriStruct.setRel(jsBean.getRel());
            uriStruct.setType(jsBean.getType());
            uriStruct.setLabel(jsBean.getLabel());
        }
        return uriStruct;
    }

}
