package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.af.bean.CloudStructBean;
import com.feedstoa.fe.bean.CloudStructJsBean;


public class CloudStructWebUtil
{
    private static final Logger log = Logger.getLogger(CloudStructWebUtil.class.getName());

    // Static methods only.
    private CloudStructWebUtil() {}
    

    public static CloudStructJsBean convertCloudStructToJsBean(CloudStruct cloudStruct)
    {
        CloudStructJsBean jsBean = null;
        if(cloudStruct != null) {
            jsBean = new CloudStructJsBean();
            jsBean.setDomain(cloudStruct.getDomain());
            jsBean.setPort(cloudStruct.getPort());
            jsBean.setPath(cloudStruct.getPath());
            jsBean.setRegisterProcedure(cloudStruct.getRegisterProcedure());
            jsBean.setProtocol(cloudStruct.getProtocol());
        }
        return jsBean;
    }

    public static CloudStruct convertCloudStructJsBeanToBean(CloudStructJsBean jsBean)
    {
        CloudStructBean cloudStruct = null;
        if(jsBean != null) {
            cloudStruct = new CloudStructBean();
            cloudStruct.setDomain(jsBean.getDomain());
            cloudStruct.setPort(jsBean.getPort());
            cloudStruct.setPath(jsBean.getPath());
            cloudStruct.setRegisterProcedure(jsBean.getRegisterProcedure());
            cloudStruct.setProtocol(jsBean.getProtocol());
        }
        return cloudStruct;
    }

}
