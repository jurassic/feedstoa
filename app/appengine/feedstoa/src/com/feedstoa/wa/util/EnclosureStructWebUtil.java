package com.feedstoa.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.af.bean.EnclosureStructBean;
import com.feedstoa.fe.bean.EnclosureStructJsBean;


public class EnclosureStructWebUtil
{
    private static final Logger log = Logger.getLogger(EnclosureStructWebUtil.class.getName());

    // Static methods only.
    private EnclosureStructWebUtil() {}
    

    public static EnclosureStructJsBean convertEnclosureStructToJsBean(EnclosureStruct enclosureStruct)
    {
        EnclosureStructJsBean jsBean = null;
        if(enclosureStruct != null) {
            jsBean = new EnclosureStructJsBean();
            jsBean.setUrl(enclosureStruct.getUrl());
            jsBean.setLength(enclosureStruct.getLength());
            jsBean.setType(enclosureStruct.getType());
        }
        return jsBean;
    }

    public static EnclosureStruct convertEnclosureStructJsBeanToBean(EnclosureStructJsBean jsBean)
    {
        EnclosureStructBean enclosureStruct = null;
        if(jsBean != null) {
            enclosureStruct = new EnclosureStructBean();
            enclosureStruct.setUrl(jsBean.getUrl());
            enclosureStruct.setLength(jsBean.getLength());
            enclosureStruct.setType(jsBean.getType());
        }
        return enclosureStruct;
    }

}
