package com.feedstoa.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.af.bean.SecondaryFetchBean;
import com.feedstoa.af.service.SecondaryFetchService;
import com.feedstoa.af.service.manager.ServiceManager;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.NotificationStructJsBean;
import com.feedstoa.fe.bean.GaeAppStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.SecondaryFetchJsBean;
import com.feedstoa.wa.util.NotificationStructWebUtil;
import com.feedstoa.wa.util.GaeAppStructWebUtil;
import com.feedstoa.wa.util.ReferrerInfoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SecondaryFetchWebService // implements SecondaryFetchService
{
    private static final Logger log = Logger.getLogger(SecondaryFetchWebService.class.getName());
     
    // Af service interface.
    private SecondaryFetchService mService = null;

    public SecondaryFetchWebService()
    {
        this(ServiceManager.getSecondaryFetchService());
    }
    public SecondaryFetchWebService(SecondaryFetchService service)
    {
        mService = service;
    }
    
    private SecondaryFetchService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getSecondaryFetchService();
        }
        return mService;
    }
    
    
    public SecondaryFetchJsBean getSecondaryFetch(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            SecondaryFetch secondaryFetch = getService().getSecondaryFetch(guid);
            SecondaryFetchJsBean bean = convertSecondaryFetchToJsBean(secondaryFetch);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getSecondaryFetch(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getSecondaryFetch(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SecondaryFetchJsBean> getSecondaryFetches(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SecondaryFetchJsBean> jsBeans = new ArrayList<SecondaryFetchJsBean>();
            List<SecondaryFetch> secondaryFetches = getService().getSecondaryFetches(guids);
            if(secondaryFetches != null) {
                for(SecondaryFetch secondaryFetch : secondaryFetches) {
                    jsBeans.add(convertSecondaryFetchToJsBean(secondaryFetch));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SecondaryFetchJsBean> getAllSecondaryFetches() throws WebException
    {
        return getAllSecondaryFetches(null, null, null);
    }

    // @Deprecated
    public List<SecondaryFetchJsBean> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllSecondaryFetches(ordering, offset, count, null);
    }

    public List<SecondaryFetchJsBean> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SecondaryFetchJsBean> jsBeans = new ArrayList<SecondaryFetchJsBean>();
            List<SecondaryFetch> secondaryFetches = getService().getAllSecondaryFetches(ordering, offset, count, forwardCursor);
            if(secondaryFetches != null) {
                for(SecondaryFetch secondaryFetch : secondaryFetches) {
                    jsBeans.add(convertSecondaryFetchToJsBean(secondaryFetch));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllSecondaryFetchKeys(ordering, offset, count, null);
    }

    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllSecondaryFetchKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<SecondaryFetchJsBean> findSecondaryFetches(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findSecondaryFetches(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<SecondaryFetchJsBean> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<SecondaryFetchJsBean> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SecondaryFetchJsBean> jsBeans = new ArrayList<SecondaryFetchJsBean>();
            List<SecondaryFetch> secondaryFetches = getService().findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(secondaryFetches != null) {
                for(SecondaryFetch secondaryFetch : secondaryFetches) {
                    jsBeans.add(convertSecondaryFetchToJsBean(secondaryFetch));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createSecondaryFetch(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws WebException
    {
        try {
            return getService().createSecondaryFetch(managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createSecondaryFetch(String jsonStr) throws WebException
    {
        return createSecondaryFetch(SecondaryFetchJsBean.fromJsonString(jsonStr));
    }

    public String createSecondaryFetch(SecondaryFetchJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SecondaryFetch secondaryFetch = convertSecondaryFetchJsBeanToBean(jsBean);
            return getService().createSecondaryFetch(secondaryFetch);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public SecondaryFetchJsBean constructSecondaryFetch(String jsonStr) throws WebException
    {
        return constructSecondaryFetch(SecondaryFetchJsBean.fromJsonString(jsonStr));
    }

    public SecondaryFetchJsBean constructSecondaryFetch(SecondaryFetchJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SecondaryFetch secondaryFetch = convertSecondaryFetchJsBeanToBean(jsBean);
            secondaryFetch = getService().constructSecondaryFetch(secondaryFetch);
            jsBean = convertSecondaryFetchToJsBean(secondaryFetch);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateSecondaryFetch(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws WebException
    {
        try {
            return getService().updateSecondaryFetch(guid, managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateSecondaryFetch(String jsonStr) throws WebException
    {
        return updateSecondaryFetch(SecondaryFetchJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateSecondaryFetch(SecondaryFetchJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SecondaryFetch secondaryFetch = convertSecondaryFetchJsBeanToBean(jsBean);
            return getService().updateSecondaryFetch(secondaryFetch);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public SecondaryFetchJsBean refreshSecondaryFetch(String jsonStr) throws WebException
    {
        return refreshSecondaryFetch(SecondaryFetchJsBean.fromJsonString(jsonStr));
    }

    public SecondaryFetchJsBean refreshSecondaryFetch(SecondaryFetchJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SecondaryFetch secondaryFetch = convertSecondaryFetchJsBeanToBean(jsBean);
            secondaryFetch = getService().refreshSecondaryFetch(secondaryFetch);
            jsBean = convertSecondaryFetchToJsBean(secondaryFetch);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteSecondaryFetch(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteSecondaryFetch(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteSecondaryFetch(SecondaryFetchJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            SecondaryFetch secondaryFetch = convertSecondaryFetchJsBeanToBean(jsBean);
            return getService().deleteSecondaryFetch(secondaryFetch);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteSecondaryFetches(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteSecondaryFetches(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static SecondaryFetchJsBean convertSecondaryFetchToJsBean(SecondaryFetch secondaryFetch)
    {
        SecondaryFetchJsBean jsBean = null;
        if(secondaryFetch != null) {
            jsBean = new SecondaryFetchJsBean();
            jsBean.setGuid(secondaryFetch.getGuid());
            jsBean.setManagerApp(secondaryFetch.getManagerApp());
            jsBean.setAppAcl(secondaryFetch.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructToJsBean(secondaryFetch.getGaeApp()));
            jsBean.setOwnerUser(secondaryFetch.getOwnerUser());
            jsBean.setUserAcl(secondaryFetch.getUserAcl());
            jsBean.setUser(secondaryFetch.getUser());
            jsBean.setTitle(secondaryFetch.getTitle());
            jsBean.setDescription(secondaryFetch.getDescription());
            jsBean.setFetchUrl(secondaryFetch.getFetchUrl());
            jsBean.setFeedUrl(secondaryFetch.getFeedUrl());
            jsBean.setChannelFeed(secondaryFetch.getChannelFeed());
            jsBean.setReuseChannel(secondaryFetch.isReuseChannel());
            jsBean.setMaxItemCount(secondaryFetch.getMaxItemCount());
            jsBean.setNote(secondaryFetch.getNote());
            jsBean.setStatus(secondaryFetch.getStatus());
            jsBean.setFetchRequest(secondaryFetch.getFetchRequest());
            jsBean.setCreatedTime(secondaryFetch.getCreatedTime());
            jsBean.setModifiedTime(secondaryFetch.getModifiedTime());
        }
        return jsBean;
    }

    public static SecondaryFetch convertSecondaryFetchJsBeanToBean(SecondaryFetchJsBean jsBean)
    {
        SecondaryFetchBean secondaryFetch = null;
        if(jsBean != null) {
            secondaryFetch = new SecondaryFetchBean();
            secondaryFetch.setGuid(jsBean.getGuid());
            secondaryFetch.setManagerApp(jsBean.getManagerApp());
            secondaryFetch.setAppAcl(jsBean.getAppAcl());
            secondaryFetch.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            secondaryFetch.setOwnerUser(jsBean.getOwnerUser());
            secondaryFetch.setUserAcl(jsBean.getUserAcl());
            secondaryFetch.setUser(jsBean.getUser());
            secondaryFetch.setTitle(jsBean.getTitle());
            secondaryFetch.setDescription(jsBean.getDescription());
            secondaryFetch.setFetchUrl(jsBean.getFetchUrl());
            secondaryFetch.setFeedUrl(jsBean.getFeedUrl());
            secondaryFetch.setChannelFeed(jsBean.getChannelFeed());
            secondaryFetch.setReuseChannel(jsBean.isReuseChannel());
            secondaryFetch.setMaxItemCount(jsBean.getMaxItemCount());
            secondaryFetch.setNote(jsBean.getNote());
            secondaryFetch.setStatus(jsBean.getStatus());
            secondaryFetch.setFetchRequest(jsBean.getFetchRequest());
            secondaryFetch.setCreatedTime(jsBean.getCreatedTime());
            secondaryFetch.setModifiedTime(jsBean.getModifiedTime());
        }
        return secondaryFetch;
    }

}
