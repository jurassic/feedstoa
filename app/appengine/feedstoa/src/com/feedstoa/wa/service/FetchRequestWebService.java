package com.feedstoa.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.af.bean.FetchRequestBean;
import com.feedstoa.af.service.FetchRequestService;
import com.feedstoa.af.service.manager.ServiceManager;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.NotificationStructJsBean;
import com.feedstoa.fe.bean.GaeAppStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.FetchRequestJsBean;
import com.feedstoa.wa.util.NotificationStructWebUtil;
import com.feedstoa.wa.util.GaeAppStructWebUtil;
import com.feedstoa.wa.util.ReferrerInfoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FetchRequestWebService // implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestWebService.class.getName());
     
    // Af service interface.
    private FetchRequestService mService = null;

    public FetchRequestWebService()
    {
        this(ServiceManager.getFetchRequestService());
    }
    public FetchRequestWebService(FetchRequestService service)
    {
        mService = service;
    }
    
    private FetchRequestService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getFetchRequestService();
        }
        return mService;
    }
    
    
    public FetchRequestJsBean getFetchRequest(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            FetchRequest fetchRequest = getService().getFetchRequest(guid);
            FetchRequestJsBean bean = convertFetchRequestToJsBean(fetchRequest);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getFetchRequest(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getFetchRequest(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FetchRequestJsBean> getFetchRequests(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FetchRequestJsBean> jsBeans = new ArrayList<FetchRequestJsBean>();
            List<FetchRequest> fetchRequests = getService().getFetchRequests(guids);
            if(fetchRequests != null) {
                for(FetchRequest fetchRequest : fetchRequests) {
                    jsBeans.add(convertFetchRequestToJsBean(fetchRequest));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FetchRequestJsBean> getAllFetchRequests() throws WebException
    {
        return getAllFetchRequests(null, null, null);
    }

    // @Deprecated
    public List<FetchRequestJsBean> getAllFetchRequests(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    public List<FetchRequestJsBean> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FetchRequestJsBean> jsBeans = new ArrayList<FetchRequestJsBean>();
            List<FetchRequest> fetchRequests = getService().getAllFetchRequests(ordering, offset, count, forwardCursor);
            if(fetchRequests != null) {
                for(FetchRequest fetchRequest : fetchRequests) {
                    jsBeans.add(convertFetchRequestToJsBean(fetchRequest));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<FetchRequestJsBean> findFetchRequests(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<FetchRequestJsBean> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<FetchRequestJsBean> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FetchRequestJsBean> jsBeans = new ArrayList<FetchRequestJsBean>();
            List<FetchRequest> fetchRequests = getService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(fetchRequests != null) {
                for(FetchRequest fetchRequest : fetchRequests) {
                    jsBeans.add(convertFetchRequestToJsBean(fetchRequest));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFetchRequest(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().createFetchRequest(managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, NotificationStructWebUtil.convertNotificationStructJsBeanToBean(notificationPref), ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFetchRequest(String jsonStr) throws WebException
    {
        return createFetchRequest(FetchRequestJsBean.fromJsonString(jsonStr));
    }

    public String createFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            return getService().createFetchRequest(fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FetchRequestJsBean constructFetchRequest(String jsonStr) throws WebException
    {
        return constructFetchRequest(FetchRequestJsBean.fromJsonString(jsonStr));
    }

    public FetchRequestJsBean constructFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            fetchRequest = getService().constructFetchRequest(fetchRequest);
            jsBean = convertFetchRequestToJsBean(fetchRequest);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateFetchRequest(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, ReferrerInfoStructJsBean referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().updateFetchRequest(guid, managerApp, appAcl, GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(gaeApp), ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, NotificationStructWebUtil.convertNotificationStructJsBeanToBean(notificationPref), ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateFetchRequest(String jsonStr) throws WebException
    {
        return updateFetchRequest(FetchRequestJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            return getService().updateFetchRequest(fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FetchRequestJsBean refreshFetchRequest(String jsonStr) throws WebException
    {
        return refreshFetchRequest(FetchRequestJsBean.fromJsonString(jsonStr));
    }

    public FetchRequestJsBean refreshFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            fetchRequest = getService().refreshFetchRequest(fetchRequest);
            jsBean = convertFetchRequestToJsBean(fetchRequest);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFetchRequest(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteFetchRequest(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            return getService().deleteFetchRequest(fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteFetchRequests(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteFetchRequests(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static FetchRequestJsBean convertFetchRequestToJsBean(FetchRequest fetchRequest)
    {
        FetchRequestJsBean jsBean = null;
        if(fetchRequest != null) {
            jsBean = new FetchRequestJsBean();
            jsBean.setGuid(fetchRequest.getGuid());
            jsBean.setManagerApp(fetchRequest.getManagerApp());
            jsBean.setAppAcl(fetchRequest.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructToJsBean(fetchRequest.getGaeApp()));
            jsBean.setOwnerUser(fetchRequest.getOwnerUser());
            jsBean.setUserAcl(fetchRequest.getUserAcl());
            jsBean.setUser(fetchRequest.getUser());
            jsBean.setTitle(fetchRequest.getTitle());
            jsBean.setDescription(fetchRequest.getDescription());
            jsBean.setFetchUrl(fetchRequest.getFetchUrl());
            jsBean.setFeedUrl(fetchRequest.getFeedUrl());
            jsBean.setChannelFeed(fetchRequest.getChannelFeed());
            jsBean.setReuseChannel(fetchRequest.isReuseChannel());
            jsBean.setMaxItemCount(fetchRequest.getMaxItemCount());
            jsBean.setNote(fetchRequest.getNote());
            jsBean.setStatus(fetchRequest.getStatus());
            jsBean.setOriginFetch(fetchRequest.getOriginFetch());
            jsBean.setOutputFormat(fetchRequest.getOutputFormat());
            jsBean.setFetchStatus(fetchRequest.getFetchStatus());
            jsBean.setResult(fetchRequest.getResult());
            jsBean.setFeedCategory(fetchRequest.getFeedCategory());
            jsBean.setMultipleFeedEnabled(fetchRequest.isMultipleFeedEnabled());
            jsBean.setDeferred(fetchRequest.isDeferred());
            jsBean.setAlert(fetchRequest.isAlert());
            jsBean.setNotificationPref(NotificationStructWebUtil.convertNotificationStructToJsBean(fetchRequest.getNotificationPref()));
            jsBean.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructToJsBean(fetchRequest.getReferrerInfo()));
            jsBean.setRefreshInterval(fetchRequest.getRefreshInterval());
            jsBean.setRefreshExpressions(fetchRequest.getRefreshExpressions());
            jsBean.setRefreshTimeZone(fetchRequest.getRefreshTimeZone());
            jsBean.setCurrentRefreshCount(fetchRequest.getCurrentRefreshCount());
            jsBean.setMaxRefreshCount(fetchRequest.getMaxRefreshCount());
            jsBean.setRefreshExpirationTime(fetchRequest.getRefreshExpirationTime());
            jsBean.setNextRefreshTime(fetchRequest.getNextRefreshTime());
            jsBean.setLastUpdatedTime(fetchRequest.getLastUpdatedTime());
            jsBean.setCreatedTime(fetchRequest.getCreatedTime());
            jsBean.setModifiedTime(fetchRequest.getModifiedTime());
        }
        return jsBean;
    }

    public static FetchRequest convertFetchRequestJsBeanToBean(FetchRequestJsBean jsBean)
    {
        FetchRequestBean fetchRequest = null;
        if(jsBean != null) {
            fetchRequest = new FetchRequestBean();
            fetchRequest.setGuid(jsBean.getGuid());
            fetchRequest.setManagerApp(jsBean.getManagerApp());
            fetchRequest.setAppAcl(jsBean.getAppAcl());
            fetchRequest.setGaeApp(GaeAppStructWebUtil.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            fetchRequest.setOwnerUser(jsBean.getOwnerUser());
            fetchRequest.setUserAcl(jsBean.getUserAcl());
            fetchRequest.setUser(jsBean.getUser());
            fetchRequest.setTitle(jsBean.getTitle());
            fetchRequest.setDescription(jsBean.getDescription());
            fetchRequest.setFetchUrl(jsBean.getFetchUrl());
            fetchRequest.setFeedUrl(jsBean.getFeedUrl());
            fetchRequest.setChannelFeed(jsBean.getChannelFeed());
            fetchRequest.setReuseChannel(jsBean.isReuseChannel());
            fetchRequest.setMaxItemCount(jsBean.getMaxItemCount());
            fetchRequest.setNote(jsBean.getNote());
            fetchRequest.setStatus(jsBean.getStatus());
            fetchRequest.setOriginFetch(jsBean.getOriginFetch());
            fetchRequest.setOutputFormat(jsBean.getOutputFormat());
            fetchRequest.setFetchStatus(jsBean.getFetchStatus());
            fetchRequest.setResult(jsBean.getResult());
            fetchRequest.setFeedCategory(jsBean.getFeedCategory());
            fetchRequest.setMultipleFeedEnabled(jsBean.isMultipleFeedEnabled());
            fetchRequest.setDeferred(jsBean.isDeferred());
            fetchRequest.setAlert(jsBean.isAlert());
            fetchRequest.setNotificationPref(NotificationStructWebUtil.convertNotificationStructJsBeanToBean(jsBean.getNotificationPref()));
            fetchRequest.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            fetchRequest.setRefreshInterval(jsBean.getRefreshInterval());
            fetchRequest.setRefreshExpressions(jsBean.getRefreshExpressions());
            fetchRequest.setRefreshTimeZone(jsBean.getRefreshTimeZone());
            fetchRequest.setCurrentRefreshCount(jsBean.getCurrentRefreshCount());
            fetchRequest.setMaxRefreshCount(jsBean.getMaxRefreshCount());
            fetchRequest.setRefreshExpirationTime(jsBean.getRefreshExpirationTime());
            fetchRequest.setNextRefreshTime(jsBean.getNextRefreshTime());
            fetchRequest.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            fetchRequest.setCreatedTime(jsBean.getCreatedTime());
            fetchRequest.setModifiedTime(jsBean.getModifiedTime());
        }
        return fetchRequest;
    }

}
