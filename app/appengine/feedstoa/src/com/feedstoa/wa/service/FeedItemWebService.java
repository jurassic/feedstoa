package com.feedstoa.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.af.bean.FeedItemBean;
import com.feedstoa.af.service.FeedItemService;
import com.feedstoa.af.service.manager.ServiceManager;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.EnclosureStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.FeedItemJsBean;
import com.feedstoa.wa.util.EnclosureStructWebUtil;
import com.feedstoa.wa.util.CategoryStructWebUtil;
import com.feedstoa.wa.util.UriStructWebUtil;
import com.feedstoa.wa.util.UserStructWebUtil;
import com.feedstoa.wa.util.ReferrerInfoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FeedItemWebService // implements FeedItemService
{
    private static final Logger log = Logger.getLogger(FeedItemWebService.class.getName());
     
    // Af service interface.
    private FeedItemService mService = null;

    public FeedItemWebService()
    {
        this(ServiceManager.getFeedItemService());
    }
    public FeedItemWebService(FeedItemService service)
    {
        mService = service;
    }
    
    private FeedItemService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getFeedItemService();
        }
        return mService;
    }
    
    
    public FeedItemJsBean getFeedItem(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            FeedItem feedItem = getService().getFeedItem(guid);
            FeedItemJsBean bean = convertFeedItemToJsBean(feedItem);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getFeedItem(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getFeedItem(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FeedItemJsBean> getFeedItems(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FeedItemJsBean> jsBeans = new ArrayList<FeedItemJsBean>();
            List<FeedItem> feedItems = getService().getFeedItems(guids);
            if(feedItems != null) {
                for(FeedItem feedItem : feedItems) {
                    jsBeans.add(convertFeedItemToJsBean(feedItem));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FeedItemJsBean> getAllFeedItems() throws WebException
    {
        return getAllFeedItems(null, null, null);
    }

    // @Deprecated
    public List<FeedItemJsBean> getAllFeedItems(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFeedItems(ordering, offset, count, null);
    }

    public List<FeedItemJsBean> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FeedItemJsBean> jsBeans = new ArrayList<FeedItemJsBean>();
            List<FeedItem> feedItems = getService().getAllFeedItems(ordering, offset, count, forwardCursor);
            if(feedItems != null) {
                for(FeedItem feedItem : feedItems) {
                    jsBeans.add(convertFeedItemToJsBean(feedItem));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFeedItemKeys(ordering, offset, count, null);
    }

    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllFeedItemKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<FeedItemJsBean> findFeedItems(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findFeedItems(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<FeedItemJsBean> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<FeedItemJsBean> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FeedItemJsBean> jsBeans = new ArrayList<FeedItemJsBean>();
            List<FeedItem> feedItems = getService().findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(feedItems != null) {
                for(FeedItem feedItem : feedItems) {
                    jsBeans.add(convertFeedItemToJsBean(feedItem));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFeedItem(String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructJsBean link, String summary, String content, UserStructJsBean author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStructJsBean enclosure, String id, String pubDate, UriStructJsBean source, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long publishedTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().createFeedItem(user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, UriStructWebUtil.convertUriStructJsBeanToBean(link), summary, content, UserStructWebUtil.convertUserStructJsBeanToBean(author), contributor, category, comments, EnclosureStructWebUtil.convertEnclosureStructJsBeanToBean(enclosure), id, pubDate, UriStructWebUtil.convertUriStructJsBeanToBean(source), feedContent, status, note, ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), publishedTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFeedItem(String jsonStr) throws WebException
    {
        return createFeedItem(FeedItemJsBean.fromJsonString(jsonStr));
    }

    public String createFeedItem(FeedItemJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FeedItem feedItem = convertFeedItemJsBeanToBean(jsBean);
            return getService().createFeedItem(feedItem);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FeedItemJsBean constructFeedItem(String jsonStr) throws WebException
    {
        return constructFeedItem(FeedItemJsBean.fromJsonString(jsonStr));
    }

    public FeedItemJsBean constructFeedItem(FeedItemJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FeedItem feedItem = convertFeedItemJsBeanToBean(jsBean);
            feedItem = getService().constructFeedItem(feedItem);
            jsBean = convertFeedItemToJsBean(feedItem);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStructJsBean link, String summary, String content, UserStructJsBean author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStructJsBean enclosure, String id, String pubDate, UriStructJsBean source, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long publishedTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().updateFeedItem(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, UriStructWebUtil.convertUriStructJsBeanToBean(link), summary, content, UserStructWebUtil.convertUserStructJsBeanToBean(author), contributor, category, comments, EnclosureStructWebUtil.convertEnclosureStructJsBeanToBean(enclosure), id, pubDate, UriStructWebUtil.convertUriStructJsBeanToBean(source), feedContent, status, note, ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(referrerInfo), publishedTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateFeedItem(String jsonStr) throws WebException
    {
        return updateFeedItem(FeedItemJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateFeedItem(FeedItemJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FeedItem feedItem = convertFeedItemJsBeanToBean(jsBean);
            return getService().updateFeedItem(feedItem);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FeedItemJsBean refreshFeedItem(String jsonStr) throws WebException
    {
        return refreshFeedItem(FeedItemJsBean.fromJsonString(jsonStr));
    }

    public FeedItemJsBean refreshFeedItem(FeedItemJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FeedItem feedItem = convertFeedItemJsBeanToBean(jsBean);
            feedItem = getService().refreshFeedItem(feedItem);
            jsBean = convertFeedItemToJsBean(feedItem);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFeedItem(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteFeedItem(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFeedItem(FeedItemJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            FeedItem feedItem = convertFeedItemJsBeanToBean(jsBean);
            return getService().deleteFeedItem(feedItem);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteFeedItems(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteFeedItems(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static FeedItemJsBean convertFeedItemToJsBean(FeedItem feedItem)
    {
        FeedItemJsBean jsBean = null;
        if(feedItem != null) {
            jsBean = new FeedItemJsBean();
            jsBean.setGuid(feedItem.getGuid());
            jsBean.setUser(feedItem.getUser());
            jsBean.setFetchRequest(feedItem.getFetchRequest());
            jsBean.setFetchUrl(feedItem.getFetchUrl());
            jsBean.setFeedUrl(feedItem.getFeedUrl());
            jsBean.setChannelSource(feedItem.getChannelSource());
            jsBean.setChannelFeed(feedItem.getChannelFeed());
            jsBean.setFeedFormat(feedItem.getFeedFormat());
            jsBean.setTitle(feedItem.getTitle());
            jsBean.setLink(UriStructWebUtil.convertUriStructToJsBean(feedItem.getLink()));
            jsBean.setSummary(feedItem.getSummary());
            jsBean.setContent(feedItem.getContent());
            jsBean.setAuthor(UserStructWebUtil.convertUserStructToJsBean(feedItem.getAuthor()));
            List<UserStructJsBean> contributorJsBeans = new ArrayList<UserStructJsBean>();
            List<UserStruct> contributorBeans = feedItem.getContributor();
            if(contributorBeans != null) {
                for(UserStruct contributor : contributorBeans) {
                    UserStructJsBean jB = UserStructWebUtil.convertUserStructToJsBean(contributor);
                    contributorJsBeans.add(jB); 
                }
            }
            jsBean.setContributor(contributorJsBeans);
            List<CategoryStructJsBean> categoryJsBeans = new ArrayList<CategoryStructJsBean>();
            List<CategoryStruct> categoryBeans = feedItem.getCategory();
            if(categoryBeans != null) {
                for(CategoryStruct category : categoryBeans) {
                    CategoryStructJsBean jB = CategoryStructWebUtil.convertCategoryStructToJsBean(category);
                    categoryJsBeans.add(jB); 
                }
            }
            jsBean.setCategory(categoryJsBeans);
            jsBean.setComments(feedItem.getComments());
            jsBean.setEnclosure(EnclosureStructWebUtil.convertEnclosureStructToJsBean(feedItem.getEnclosure()));
            jsBean.setId(feedItem.getId());
            jsBean.setPubDate(feedItem.getPubDate());
            jsBean.setSource(UriStructWebUtil.convertUriStructToJsBean(feedItem.getSource()));
            jsBean.setFeedContent(feedItem.getFeedContent());
            jsBean.setStatus(feedItem.getStatus());
            jsBean.setNote(feedItem.getNote());
            jsBean.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructToJsBean(feedItem.getReferrerInfo()));
            jsBean.setPublishedTime(feedItem.getPublishedTime());
            jsBean.setLastUpdatedTime(feedItem.getLastUpdatedTime());
            jsBean.setCreatedTime(feedItem.getCreatedTime());
            jsBean.setModifiedTime(feedItem.getModifiedTime());
        }
        return jsBean;
    }

    public static FeedItem convertFeedItemJsBeanToBean(FeedItemJsBean jsBean)
    {
        FeedItemBean feedItem = null;
        if(jsBean != null) {
            feedItem = new FeedItemBean();
            feedItem.setGuid(jsBean.getGuid());
            feedItem.setUser(jsBean.getUser());
            feedItem.setFetchRequest(jsBean.getFetchRequest());
            feedItem.setFetchUrl(jsBean.getFetchUrl());
            feedItem.setFeedUrl(jsBean.getFeedUrl());
            feedItem.setChannelSource(jsBean.getChannelSource());
            feedItem.setChannelFeed(jsBean.getChannelFeed());
            feedItem.setFeedFormat(jsBean.getFeedFormat());
            feedItem.setTitle(jsBean.getTitle());
            feedItem.setLink(UriStructWebUtil.convertUriStructJsBeanToBean(jsBean.getLink()));
            feedItem.setSummary(jsBean.getSummary());
            feedItem.setContent(jsBean.getContent());
            feedItem.setAuthor(UserStructWebUtil.convertUserStructJsBeanToBean(jsBean.getAuthor()));
            List<UserStruct> contributorBeans = new ArrayList<UserStruct>();
            List<UserStructJsBean> contributorJsBeans = jsBean.getContributor();
            if(contributorJsBeans != null) {
                for(UserStructJsBean contributor : contributorJsBeans) {
                    UserStruct b = UserStructWebUtil.convertUserStructJsBeanToBean(contributor);
                    contributorBeans.add(b); 
                }
            }
            feedItem.setContributor(contributorBeans);
            List<CategoryStruct> categoryBeans = new ArrayList<CategoryStruct>();
            List<CategoryStructJsBean> categoryJsBeans = jsBean.getCategory();
            if(categoryJsBeans != null) {
                for(CategoryStructJsBean category : categoryJsBeans) {
                    CategoryStruct b = CategoryStructWebUtil.convertCategoryStructJsBeanToBean(category);
                    categoryBeans.add(b); 
                }
            }
            feedItem.setCategory(categoryBeans);
            feedItem.setComments(jsBean.getComments());
            feedItem.setEnclosure(EnclosureStructWebUtil.convertEnclosureStructJsBeanToBean(jsBean.getEnclosure()));
            feedItem.setId(jsBean.getId());
            feedItem.setPubDate(jsBean.getPubDate());
            feedItem.setSource(UriStructWebUtil.convertUriStructJsBeanToBean(jsBean.getSource()));
            feedItem.setFeedContent(jsBean.getFeedContent());
            feedItem.setStatus(jsBean.getStatus());
            feedItem.setNote(jsBean.getNote());
            feedItem.setReferrerInfo(ReferrerInfoStructWebUtil.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            feedItem.setPublishedTime(jsBean.getPublishedTime());
            feedItem.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            feedItem.setCreatedTime(jsBean.getCreatedTime());
            feedItem.setModifiedTime(jsBean.getModifiedTime());
        }
        return feedItem;
    }

}
