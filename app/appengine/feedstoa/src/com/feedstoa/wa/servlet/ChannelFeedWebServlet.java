package com.feedstoa.wa.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.ArrayList;

//import com.feedstoa.ws.ChannelFeed;
//import com.feedstoa.ws.CloudStruct;
//import com.feedstoa.ws.CategoryStruct;
//import com.feedstoa.ws.ImageStruct;
//import com.feedstoa.ws.UriStruct;
//import com.feedstoa.ws.UserStruct;
//import com.feedstoa.ws.ReferrerInfoStruct;
//import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.core.StatusCode;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.af.util.CorsHelper;
import com.feedstoa.fe.Validateable;
import com.feedstoa.fe.WebException;
import com.feedstoa.fe.bean.ChannelFeedJsBean;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.ImageStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.wa.service.ChannelFeedWebService;


public class ChannelFeedWebServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedWebServlet.class.getName());
    private static final String FORMBEAN_CLASS_NAME = "com.feedstoa.form.bean.ChannelFeedFormBean";


    // TBD: Is this safe for concurrent calls???
    private ChannelFeedWebService mService = null;
    private ChannelFeedWebService getService()
    {
        if(mService == null) {
            mService = new ChannelFeedWebService();
        }
        return mService;
    }

    private Class<?> formBeanClass = null;
    private void loadFormBeanClass()
    {
        try {
            formBeanClass = Class.forName(FORMBEAN_CLASS_NAME);
        } catch(Exception e) {
            // ignore
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, FORMBEAN_CLASS_NAME + " does not exist.", e);
        }
    }


    @Override
    public void init() throws ServletException
    {
        super.init();
        loadFormBeanClass();
    }

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        loadFormBeanClass();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doGet() called.");

        // TBD: Check Accept header. Etc...
        String guid = null;
        String pathInfo = req.getPathInfo();
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }
        if(guid != null && !guid.isEmpty()) {
            ChannelFeedJsBean bean = null;
            try {

	            // TBD:
	            if(CorsHelper.getInstance().useAllowOrigin()) {
	                String sameOriginType = CorsHelper.getInstance().getDefaultAllowOrigin();
	                if(CorsHelper.isValid(sameOriginType)) {
	                    if(CorsHelper.AC_ALLOW_ORIGIN_ALL.equals(sameOriginType)) {
	                        resp.setHeader("Access-Control-Allow-Origin", "*");
	                    } else {
	                        String reqOrigin = req.getHeader("Origin");
	                        if(reqOrigin != null && !reqOrigin.isEmpty()) {
	                            // TBD: Validate the URL?
	                            resp.setHeader("Access-Control-Allow-Origin", reqOrigin);
	                        } else {
	                            // ignore
	                        }
	                    }
	                } else {
	                    // ignore
	                }
	            }

                bean = getService().getChannelFeed(guid);
                resp.setContentType("application/json;charset=UTF-8");
                // resp.setHeader("Cache-Control", "no-cache");     // ?????
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
                PrintWriter out = resp.getWriter();
                out.write(bean.toJsonString());
            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
                resp.setStatus(StatusCode.NOT_FOUND);  // ???
            }
        } else {
            // If guid is not set, fetch the list.

            String filter = req.getParameter("filter");
            String ordering = req.getParameter("ordering");
            Long offset = null;
            String strOffset = req.getParameter("offset");
            if(strOffset != null) {
                try {
                    offset = Long.parseLong(strOffset);
                } catch(Exception e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
            Integer count = null;
            String strCount = req.getParameter("count");
            if(strCount != null) {
                try {
                    count = Integer.parseInt(strCount);
                } catch(Exception e) {
                    // ignore
                    if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
            
            List<ChannelFeedJsBean> beans = null;
            try {
                beans = getService().findChannelFeeds(filter, ordering, null, null, null, null, offset, count);
            } catch (WebException e) {
                log.log(Level.WARNING, "Failed to get ChannelFeedJsBean list.", e);
            }
            
            if(beans != null) {

	            // TBD:
	            if(CorsHelper.getInstance().useAllowOrigin()) {
	                String sameOriginType = CorsHelper.getInstance().getDefaultAllowOrigin();
	                if(CorsHelper.isValid(sameOriginType)) {
	                    if(CorsHelper.AC_ALLOW_ORIGIN_ALL.equals(sameOriginType)) {
	                        resp.setHeader("Access-Control-Allow-Origin", "*");
	                    } else {
	                        String reqOrigin = req.getHeader("Origin");
	                        if(reqOrigin != null && !reqOrigin.isEmpty()) {
	                            // TBD: Validate the URL?
	                            resp.setHeader("Access-Control-Allow-Origin", reqOrigin);
	                        } else {
	                            // ignore
	                        }
	                    }
	                } else {
	                    // ignore
	                }
	            }

                resp.setContentType("application/json;charset=UTF-8");
                // resp.setHeader("Cache-Control", "no-cache");     // ?????
                resp.setStatus(StatusCode.OK);

                StringBuilder sb = new StringBuilder();
                sb.append("[");
                for(int i=0; i<beans.size(); i++) {
                    ChannelFeedJsBean b = beans.get(i);
                    sb.append(b.toJsonString());
                    if(i < beans.size()-1) {
                        sb.append(",");
                    }
                }
                sb.append("]");
                String jsonStr = sb.toString(); 
                PrintWriter out = resp.getWriter();
                out.write(jsonStr);                
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doPost() called.");

        // TBD: Check Accept header. Etc...
        boolean validated = true;
        boolean succeeded = false;
        ChannelFeedJsBean outBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();
            ChannelFeedJsBean inBean = null;
            if(formBeanClass != null) {
                try {
                    inBean = (ChannelFeedJsBean) formBeanClass.getMethod("fromJsonString", String.class).invoke(null, jsonStr);
                    //inBean = (ChannelFeedJsBean) formBeanClass.getConstructor(String.class).newInstance(jsonStr);
                } catch(Exception e1) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, FORMBEAN_CLASS_NAME + ".fromJsonString() failed.", e1);
                }
            }
            if(inBean == null) {
                inBean = ChannelFeedJsBean.fromJsonString(jsonStr);                
            }
            if(inBean instanceof Validateable) {
                validated = ((Validateable) inBean).validate();
            }
            if(validated == true) {
                try {
                    outBean = getService().constructChannelFeed(inBean);
                    // String guid = getService().createChannelFeed(inBean);
                    succeeded = true;
                } catch (WebException e1) {
                    log.log(Level.WARNING, "Server error.", e1);
                }
            }
            if(outBean == null) {
                // ???
                outBean = inBean;
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error.", e);
        }
        
        if(succeeded == true) {
            
            // TBD:
            if(CorsHelper.getInstance().useAllowOrigin()) {
                String sameOriginType = CorsHelper.getInstance().getDefaultAllowOrigin();
                if(CorsHelper.isValid(sameOriginType)) {
                    if(CorsHelper.AC_ALLOW_ORIGIN_ALL.equals(sameOriginType)) {
                        resp.setHeader("Access-Control-Allow-Origin", "*");
                    } else {
                        String reqOrigin = req.getHeader("Origin");
                        if(reqOrigin != null && !reqOrigin.isEmpty()) {
                            // TBD: Validate the URL?
                            resp.setHeader("Access-Control-Allow-Origin", reqOrigin);
                        } else {
                            // ignore
                        }
                    }
                } else {
                    // ignore
                }
            }
            
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            // resp.setHeader("Cache-Control", "no-cache");     // ?????

            // Location header???
            //String guid = outBean.getGuid();

            PrintWriter out = resp.getWriter();
            out.write(outBean.toJsonString());
        } else {
            if(outBean == null) {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            } else {
                if(validated == false) {
                    resp.setStatus(StatusCode.BAD_REQUEST);
                } else {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ???
                    if(outBean instanceof Validateable) {
                        ((Validateable) outBean).addError(Validateable.FIELD_BEANWIDE, "Server error.");
                    }
                }
                PrintWriter out = resp.getWriter();
                out.write(outBean.toJsonString());
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doPut() called.");

        // TBD: Check Accept header. Etc...
        String guid = null;
        String pathInfo = req.getPathInfo();
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }
        // TBD: guid should not be null!!!

        boolean validated = true;
        boolean succeeded = false;
        ChannelFeedJsBean outBean = null;
        if(guid != null && !guid.isEmpty()) {
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                ChannelFeedJsBean inBean = null;
                if(formBeanClass != null) {
                    try {
                        inBean = (ChannelFeedJsBean) formBeanClass.getMethod("fromJsonString", String.class).invoke(null, jsonStr);
                        //inBean = (ChannelFeedJsBean) formBeanClass.getConstructor(String.class).newInstance(jsonStr);
                    } catch(Exception e1) {
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, FORMBEAN_CLASS_NAME + ".fromJsonString() failed.", e1);
                    }
                }
                if(inBean == null) {
                    inBean = ChannelFeedJsBean.fromJsonString(jsonStr);                
                }
                String beanGuid = inBean.getGuid();
                if(guid.equals(beanGuid)) {
                    if(inBean instanceof Validateable) {
                        validated = ((Validateable) inBean).validate();
                    }
                } else {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                    validated = false;
                    if(inBean instanceof Validateable) {
                        ((Validateable) inBean).addError("guid", "Inconsistent input.");
                    }
                }
                if(validated == true) {
                    try {
                        outBean = getService().refreshChannelFeed(inBean);
                        // Boolean suc = getService().updateChannelFeed(inBean);
                        succeeded = true;
                    } catch (WebException e1) {
                        log.log(Level.WARNING, "Server error.", e1);
                    }
                }
                if(outBean == null) {
                    // ???
                    outBean = inBean;
                }
            } catch (Exception e) {
                log.log(Level.WARNING, "Unknown error.", e);
            }
        } else {
            // ???
            log.warning("Required arg, guid, is missing.");
        }

        if(succeeded == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            // resp.setHeader("Cache-Control", "no-cache");     // ?????

            // Location header???
            //String guid = outBean.getGuid();

            PrintWriter out = resp.getWriter();
            out.write(outBean.toJsonString());
        } else {
            if(outBean == null) {
                if(guid == null || guid.isEmpty()) {
                    resp.setStatus(StatusCode.BAD_REQUEST);  // ???
                } else {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
                }
            } else {
                if(validated == false) {
                    resp.setStatus(StatusCode.BAD_REQUEST);
                } else {
                    resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ???
                    if(outBean instanceof Validateable) {
                        ((Validateable) outBean).addError(Validateable.FIELD_BEANWIDE, "Server error.");
                    }
                }
                PrintWriter out = resp.getWriter();
                out.write(outBean.toJsonString());
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.finer("doDelete() called.");

        // TBD: Check Accept header. Etc...
        String guid = null;
        String pathInfo = req.getPathInfo();
        if(pathInfo == null || pathInfo.isEmpty()) {
            // Get list???
            // For now, use the query param. ???
            guid = req.getParameter("guid");
        } else {
            if(pathInfo.startsWith("/")) {
                guid = pathInfo.substring(1);
            } else {
                guid = pathInfo;
            }
        }
        // TBD: guid should not be null!!!

        if(guid != null && !guid.isEmpty()) {
            // TBD:
            Boolean suc = null;
            try {
                suc = getService().deleteChannelFeed(guid);
            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(suc != null && suc.equals(Boolean.TRUE)) {
                resp.setStatus(StatusCode.OK);
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            log.warning("Required arg, guid, is missing.");
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
        }
    }
    
    
}
