package com.feedstoa.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.fe.Validateable;
import com.feedstoa.fe.core.StringEscapeUtil;
import com.feedstoa.fe.bean.CloudStructJsBean;
import com.feedstoa.fe.bean.CategoryStructJsBean;
import com.feedstoa.fe.bean.ImageStructJsBean;
import com.feedstoa.fe.bean.UriStructJsBean;
import com.feedstoa.fe.bean.UserStructJsBean;
import com.feedstoa.fe.bean.ReferrerInfoStructJsBean;
import com.feedstoa.fe.bean.TextInputStructJsBean;
import com.feedstoa.fe.bean.ChannelFeedJsBean;


// Place holder...
public class ChannelFeedFormBean extends ChannelFeedJsBean implements Serializable, Cloneable, Validateable  //, ChannelFeed
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public ChannelFeedFormBean()
    {
        super();
    }
    public ChannelFeedFormBean(String guid)
    {
       super(guid);
    }
    public ChannelFeedFormBean(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructJsBean link, String description, String language, String copyright, UserStructJsBean managingEditor, UserStructJsBean webMaster, List<UserStructJsBean> contributor, String pubDate, String lastBuildDate, List<CategoryStructJsBean> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean logo, ImageStructJsBean icon, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime)
    {
        super(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
    }
    public ChannelFeedFormBean(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStructJsBean link, String description, String language, String copyright, UserStructJsBean managingEditor, UserStructJsBean webMaster, List<UserStructJsBean> contributor, String pubDate, String lastBuildDate, List<CategoryStructJsBean> category, String generator, String docs, CloudStructJsBean cloud, Integer ttl, ImageStructJsBean logo, ImageStructJsBean icon, String rating, TextInputStructJsBean textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStructJsBean referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime, createdTime, modifiedTime);
    }
    public ChannelFeedFormBean(ChannelFeedJsBean bean)
    {
        super(bean);
    }

    public static ChannelFeedFormBean fromJsonString(String jsonStr)
    {
        ChannelFeedFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, ChannelFeedFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getUser() == null) {
//            addError("user", "user is null");
//            allOK = false;
//        }
//        // TBD
//        if(getChannelSource() == null) {
//            addError("channelSource", "channelSource is null");
//            allOK = false;
//        }
//        // TBD
//        if(getChannelCode() == null) {
//            addError("channelCode", "channelCode is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPreviousVersion() == null) {
//            addError("previousVersion", "previousVersion is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFetchRequest() == null) {
//            addError("fetchRequest", "fetchRequest is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFetchUrl() == null) {
//            addError("fetchUrl", "fetchUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedServiceUrl() == null) {
//            addError("feedServiceUrl", "feedServiceUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedUrl() == null) {
//            addError("feedUrl", "feedUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedFormat() == null) {
//            addError("feedFormat", "feedFormat is null");
//            allOK = false;
//        }
//        // TBD
//        if(getMaxItemCount() == null) {
//            addError("maxItemCount", "maxItemCount is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedCategory() == null) {
//            addError("feedCategory", "feedCategory is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTitle() == null) {
//            addError("title", "title is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSubtitle() == null) {
//            addError("subtitle", "subtitle is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLink() == null) {
//            addError("link", "link is null");
//            allOK = false;
//        } else {
//            UriStructJsBean link = getLink();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getDescription() == null) {
//            addError("description", "description is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLanguage() == null) {
//            addError("language", "language is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCopyright() == null) {
//            addError("copyright", "copyright is null");
//            allOK = false;
//        }
//        // TBD
//        if(getManagingEditor() == null) {
//            addError("managingEditor", "managingEditor is null");
//            allOK = false;
//        } else {
//            UserStructJsBean managingEditor = getManagingEditor();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getWebMaster() == null) {
//            addError("webMaster", "webMaster is null");
//            allOK = false;
//        } else {
//            UserStructJsBean webMaster = getWebMaster();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getContributor() == null) {
//            addError("contributor", "contributor is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPubDate() == null) {
//            addError("pubDate", "pubDate is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastBuildDate() == null) {
//            addError("lastBuildDate", "lastBuildDate is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCategory() == null) {
//            addError("category", "category is null");
//            allOK = false;
//        }
//        // TBD
//        if(getGenerator() == null) {
//            addError("generator", "generator is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDocs() == null) {
//            addError("docs", "docs is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCloud() == null) {
//            addError("cloud", "cloud is null");
//            allOK = false;
//        } else {
//            CloudStructJsBean cloud = getCloud();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getTtl() == null) {
//            addError("ttl", "ttl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLogo() == null) {
//            addError("logo", "logo is null");
//            allOK = false;
//        } else {
//            ImageStructJsBean logo = getLogo();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getIcon() == null) {
//            addError("icon", "icon is null");
//            allOK = false;
//        } else {
//            ImageStructJsBean icon = getIcon();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getRating() == null) {
//            addError("rating", "rating is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTextInput() == null) {
//            addError("textInput", "textInput is null");
//            allOK = false;
//        } else {
//            TextInputStructJsBean textInput = getTextInput();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getSkipHours() == null) {
//            addError("skipHours", "skipHours is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSkipDays() == null) {
//            addError("skipDays", "skipDays is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputText() == null) {
//            addError("outputText", "outputText is null");
//            allOK = false;
//        }
//        // TBD
//        if(getOutputHash() == null) {
//            addError("outputHash", "outputHash is null");
//            allOK = false;
//        }
//        // TBD
//        if(getFeedContent() == null) {
//            addError("feedContent", "feedContent is null");
//            allOK = false;
//        }
//        // TBD
//        if(getStatus() == null) {
//            addError("status", "status is null");
//            allOK = false;
//        }
//        // TBD
//        if(getNote() == null) {
//            addError("note", "note is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReferrerInfo() == null) {
//            addError("referrerInfo", "referrerInfo is null");
//            allOK = false;
//        } else {
//            ReferrerInfoStructJsBean referrerInfo = getReferrerInfo();
//            // ...
//            // allOK = false;
//        }
//        // TBD
//        if(getLastBuildTime() == null) {
//            addError("lastBuildTime", "lastBuildTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getPublishedTime() == null) {
//            addError("publishedTime", "publishedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getExpirationTime() == null) {
//            addError("expirationTime", "expirationTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastUpdatedTime() == null) {
//            addError("lastUpdatedTime", "lastUpdatedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ChannelFeedFormBean cloned = new ChannelFeedFormBean((ChannelFeedJsBean) super.clone());
        return cloned;
    }

}
