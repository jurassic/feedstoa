package com.feedstoa;

import java.io.IOException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class FeedStoaAppServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
}
