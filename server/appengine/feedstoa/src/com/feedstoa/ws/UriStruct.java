package com.feedstoa.ws;



public interface UriStruct 
{
    String  getUuid();
    String  getHref();
    String  getRel();
    String  getType();
    String  getLabel();
    boolean isEmpty();
}
