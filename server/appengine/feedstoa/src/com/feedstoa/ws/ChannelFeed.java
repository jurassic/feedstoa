package com.feedstoa.ws;

import java.util.List;
import java.util.ArrayList;


public interface ChannelFeed 
{
    String  getGuid();
    String  getUser();
    String  getChannelSource();
    String  getChannelCode();
    String  getPreviousVersion();
    String  getFetchRequest();
    String  getFetchUrl();
    String  getFeedServiceUrl();
    String  getFeedUrl();
    String  getFeedFormat();
    Integer  getMaxItemCount();
    String  getFeedCategory();
    String  getTitle();
    String  getSubtitle();
    UriStruct  getLink();
    String  getDescription();
    String  getLanguage();
    String  getCopyright();
    UserStruct  getManagingEditor();
    UserStruct  getWebMaster();
    List<UserStruct>  getContributor();
    String  getPubDate();
    String  getLastBuildDate();
    List<CategoryStruct>  getCategory();
    String  getGenerator();
    String  getDocs();
    CloudStruct  getCloud();
    Integer  getTtl();
    ImageStruct  getLogo();
    ImageStruct  getIcon();
    String  getRating();
    TextInputStruct  getTextInput();
    List<Integer>  getSkipHours();
    List<String>  getSkipDays();
    String  getOutputText();
    String  getOutputHash();
    String  getFeedContent();
    String  getStatus();
    String  getNote();
    ReferrerInfoStruct  getReferrerInfo();
    Long  getLastBuildTime();
    Long  getPublishedTime();
    Long  getExpirationTime();
    Long  getLastUpdatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
