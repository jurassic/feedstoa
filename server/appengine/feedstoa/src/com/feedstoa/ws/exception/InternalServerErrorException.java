package com.feedstoa.ws.exception;

import com.feedstoa.ws.BaseException;


public class InternalServerErrorException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public InternalServerErrorException() 
    {
        super();
    }
    public InternalServerErrorException(String message) 
    {
        super(message);
    }
    public InternalServerErrorException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public InternalServerErrorException(Throwable cause) 
    {
        super(cause);
    }

}
