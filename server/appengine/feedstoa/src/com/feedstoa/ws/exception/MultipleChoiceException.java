package com.feedstoa.ws.exception;

import com.feedstoa.ws.BaseException;


public class MultipleChoiceException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public MultipleChoiceException() 
    {
        super();
    }
    public MultipleChoiceException(String message) 
    {
        super(message);
    }
   public MultipleChoiceException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public MultipleChoiceException(Throwable cause) 
    {
        super(cause);
    }

}
