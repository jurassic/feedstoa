package com.feedstoa.ws;



public interface FetchBase 
{
    String  getGuid();
    String  getManagerApp();
    Long  getAppAcl();
    GaeAppStruct  getGaeApp();
    String  getOwnerUser();
    Long  getUserAcl();
    String  getUser();
    String  getTitle();
    String  getDescription();
    String  getFetchUrl();
    String  getFeedUrl();
    String  getChannelFeed();
    Boolean  isReuseChannel();
    Integer  getMaxItemCount();
    String  getNote();
    String  getStatus();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
