package com.feedstoa.ws;

import java.util.List;
import java.util.ArrayList;


public interface FeedItem 
{
    String  getGuid();
    String  getUser();
    String  getFetchRequest();
    String  getFetchUrl();
    String  getFeedUrl();
    String  getChannelSource();
    String  getChannelFeed();
    String  getFeedFormat();
    String  getTitle();
    UriStruct  getLink();
    String  getSummary();
    String  getContent();
    UserStruct  getAuthor();
    List<UserStruct>  getContributor();
    List<CategoryStruct>  getCategory();
    String  getComments();
    EnclosureStruct  getEnclosure();
    String  getId();
    String  getPubDate();
    UriStruct  getSource();
    String  getFeedContent();
    String  getStatus();
    String  getNote();
    ReferrerInfoStruct  getReferrerInfo();
    Long  getPublishedTime();
    Long  getLastUpdatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
