package com.feedstoa.ws.dao;

import java.util.List;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.data.FeedContentDataObject;


// TBD: Add offset/count to getAllFeedContents() and findFeedContents(), etc.
public interface FeedContentDAO
{
    FeedContentDataObject getFeedContent(String guid) throws BaseException;
    List<FeedContentDataObject> getFeedContents(List<String> guids) throws BaseException;
    List<FeedContentDataObject> getAllFeedContents() throws BaseException;
    /* @Deprecated */ List<FeedContentDataObject> getAllFeedContents(String ordering, Long offset, Integer count) throws BaseException;
    List<FeedContentDataObject> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FeedContentDataObject> findFeedContents(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FeedContentDataObject> findFeedContents(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<FeedContentDataObject> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FeedContentDataObject> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createFeedContent(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FeedContentDataObject?)
    String createFeedContent(FeedContentDataObject feedContent) throws BaseException;          // Returns Guid.  (Return FeedContentDataObject?)
    //Boolean updateFeedContent(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFeedContent(FeedContentDataObject feedContent) throws BaseException;
    Boolean deleteFeedContent(String guid) throws BaseException;
    Boolean deleteFeedContent(FeedContentDataObject feedContent) throws BaseException;
    Long deleteFeedContents(String filter, String params, List<String> values) throws BaseException;
}
