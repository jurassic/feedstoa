package com.feedstoa.ws.dao;

import java.util.List;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.data.ChannelSourceDataObject;


// TBD: Add offset/count to getAllChannelSources() and findChannelSources(), etc.
public interface ChannelSourceDAO
{
    ChannelSourceDataObject getChannelSource(String guid) throws BaseException;
    List<ChannelSourceDataObject> getChannelSources(List<String> guids) throws BaseException;
    List<ChannelSourceDataObject> getAllChannelSources() throws BaseException;
    /* @Deprecated */ List<ChannelSourceDataObject> getAllChannelSources(String ordering, Long offset, Integer count) throws BaseException;
    List<ChannelSourceDataObject> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ChannelSourceDataObject> findChannelSources(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ChannelSourceDataObject> findChannelSources(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<ChannelSourceDataObject> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ChannelSourceDataObject> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createChannelSource(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ChannelSourceDataObject?)
    String createChannelSource(ChannelSourceDataObject channelSource) throws BaseException;          // Returns Guid.  (Return ChannelSourceDataObject?)
    //Boolean updateChannelSource(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateChannelSource(ChannelSourceDataObject channelSource) throws BaseException;
    Boolean deleteChannelSource(String guid) throws BaseException;
    Boolean deleteChannelSource(ChannelSourceDataObject channelSource) throws BaseException;
    Long deleteChannelSources(String filter, String params, List<String> values) throws BaseException;
}
