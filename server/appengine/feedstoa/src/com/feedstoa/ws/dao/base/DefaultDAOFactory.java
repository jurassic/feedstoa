package com.feedstoa.ws.dao.base;

import java.util.logging.Logger;

import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.dao.ApiConsumerDAO;
import com.feedstoa.ws.dao.UserDAO;
import com.feedstoa.ws.dao.FetchRequestDAO;
import com.feedstoa.ws.dao.SecondaryFetchDAO;
import com.feedstoa.ws.dao.ChannelSourceDAO;
import com.feedstoa.ws.dao.ChannelFeedDAO;
import com.feedstoa.ws.dao.FeedItemDAO;
import com.feedstoa.ws.dao.FeedContentDAO;
import com.feedstoa.ws.dao.ServiceInfoDAO;
import com.feedstoa.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new DefaultApiConsumerDAO();
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new DefaultUserDAO();
    }

    @Override
    public FetchRequestDAO getFetchRequestDAO()
    {
        return new DefaultFetchRequestDAO();
    }

    @Override
    public SecondaryFetchDAO getSecondaryFetchDAO()
    {
        return new DefaultSecondaryFetchDAO();
    }

    @Override
    public ChannelSourceDAO getChannelSourceDAO()
    {
        return new DefaultChannelSourceDAO();
    }

    @Override
    public ChannelFeedDAO getChannelFeedDAO()
    {
        return new DefaultChannelFeedDAO();
    }

    @Override
    public FeedItemDAO getFeedItemDAO()
    {
        return new DefaultFeedItemDAO();
    }

    @Override
    public FeedContentDAO getFeedContentDAO()
    {
        return new DefaultFeedContentDAO();
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new DefaultServiceInfoDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
