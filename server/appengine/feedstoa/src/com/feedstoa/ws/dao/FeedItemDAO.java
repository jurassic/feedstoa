package com.feedstoa.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.data.FeedItemDataObject;


// TBD: Add offset/count to getAllFeedItems() and findFeedItems(), etc.
public interface FeedItemDAO
{
    FeedItemDataObject getFeedItem(String guid) throws BaseException;
    List<FeedItemDataObject> getFeedItems(List<String> guids) throws BaseException;
    List<FeedItemDataObject> getAllFeedItems() throws BaseException;
    /* @Deprecated */ List<FeedItemDataObject> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException;
    List<FeedItemDataObject> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FeedItemDataObject> findFeedItems(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FeedItemDataObject> findFeedItems(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<FeedItemDataObject> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FeedItemDataObject> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createFeedItem(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FeedItemDataObject?)
    String createFeedItem(FeedItemDataObject feedItem) throws BaseException;          // Returns Guid.  (Return FeedItemDataObject?)
    //Boolean updateFeedItem(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFeedItem(FeedItemDataObject feedItem) throws BaseException;
    Boolean deleteFeedItem(String guid) throws BaseException;
    Boolean deleteFeedItem(FeedItemDataObject feedItem) throws BaseException;
    Long deleteFeedItems(String filter, String params, List<String> values) throws BaseException;
}
