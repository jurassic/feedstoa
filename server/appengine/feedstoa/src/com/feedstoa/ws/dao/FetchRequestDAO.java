package com.feedstoa.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.data.FetchRequestDataObject;


// TBD: Add offset/count to getAllFetchRequests() and findFetchRequests(), etc.
public interface FetchRequestDAO
{
    FetchRequestDataObject getFetchRequest(String guid) throws BaseException;
    List<FetchRequestDataObject> getFetchRequests(List<String> guids) throws BaseException;
    List<FetchRequestDataObject> getAllFetchRequests() throws BaseException;
    /* @Deprecated */ List<FetchRequestDataObject> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException;
    List<FetchRequestDataObject> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createFetchRequest(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FetchRequestDataObject?)
    String createFetchRequest(FetchRequestDataObject fetchRequest) throws BaseException;          // Returns Guid.  (Return FetchRequestDataObject?)
    //Boolean updateFetchRequest(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFetchRequest(FetchRequestDataObject fetchRequest) throws BaseException;
    Boolean deleteFetchRequest(String guid) throws BaseException;
    Boolean deleteFetchRequest(FetchRequestDataObject fetchRequest) throws BaseException;
    Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException;
}
