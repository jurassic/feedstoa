package com.feedstoa.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract ApiConsumerDAO getApiConsumerDAO();
    public abstract UserDAO getUserDAO();
    public abstract FetchRequestDAO getFetchRequestDAO();
    public abstract SecondaryFetchDAO getSecondaryFetchDAO();
    public abstract ChannelSourceDAO getChannelSourceDAO();
    public abstract ChannelFeedDAO getChannelFeedDAO();
    public abstract FeedItemDAO getFeedItemDAO();
    public abstract FeedContentDAO getFeedContentDAO();
    public abstract ServiceInfoDAO getServiceInfoDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
