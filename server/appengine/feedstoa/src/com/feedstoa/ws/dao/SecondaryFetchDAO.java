package com.feedstoa.ws.dao;

import java.util.List;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.data.SecondaryFetchDataObject;


// TBD: Add offset/count to getAllSecondaryFetches() and findSecondaryFetches(), etc.
public interface SecondaryFetchDAO
{
    SecondaryFetchDataObject getSecondaryFetch(String guid) throws BaseException;
    List<SecondaryFetchDataObject> getSecondaryFetches(List<String> guids) throws BaseException;
    List<SecondaryFetchDataObject> getAllSecondaryFetches() throws BaseException;
    /* @Deprecated */ List<SecondaryFetchDataObject> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws BaseException;
    List<SecondaryFetchDataObject> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<SecondaryFetchDataObject> findSecondaryFetches(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<SecondaryFetchDataObject> findSecondaryFetches(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<SecondaryFetchDataObject> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<SecondaryFetchDataObject> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createSecondaryFetch(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return SecondaryFetchDataObject?)
    String createSecondaryFetch(SecondaryFetchDataObject secondaryFetch) throws BaseException;          // Returns Guid.  (Return SecondaryFetchDataObject?)
    //Boolean updateSecondaryFetch(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateSecondaryFetch(SecondaryFetchDataObject secondaryFetch) throws BaseException;
    Boolean deleteSecondaryFetch(String guid) throws BaseException;
    Boolean deleteSecondaryFetch(SecondaryFetchDataObject secondaryFetch) throws BaseException;
    Long deleteSecondaryFetches(String filter, String params, List<String> values) throws BaseException;
}
