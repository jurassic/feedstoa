package com.feedstoa.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.data.ChannelFeedDataObject;


// TBD: Add offset/count to getAllChannelFeeds() and findChannelFeeds(), etc.
public interface ChannelFeedDAO
{
    ChannelFeedDataObject getChannelFeed(String guid) throws BaseException;
    List<ChannelFeedDataObject> getChannelFeeds(List<String> guids) throws BaseException;
    List<ChannelFeedDataObject> getAllChannelFeeds() throws BaseException;
    /* @Deprecated */ List<ChannelFeedDataObject> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException;
    List<ChannelFeedDataObject> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ChannelFeedDataObject> findChannelFeeds(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ChannelFeedDataObject> findChannelFeeds(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<ChannelFeedDataObject> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ChannelFeedDataObject> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createChannelFeed(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ChannelFeedDataObject?)
    String createChannelFeed(ChannelFeedDataObject channelFeed) throws BaseException;          // Returns Guid.  (Return ChannelFeedDataObject?)
    //Boolean updateChannelFeed(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateChannelFeed(ChannelFeedDataObject channelFeed) throws BaseException;
    Boolean deleteChannelFeed(String guid) throws BaseException;
    Boolean deleteChannelFeed(ChannelFeedDataObject channelFeed) throws BaseException;
    Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException;
}
