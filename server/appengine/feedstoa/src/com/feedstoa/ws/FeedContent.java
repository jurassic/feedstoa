package com.feedstoa.ws;



public interface FeedContent 
{
    String  getGuid();
    String  getUser();
    String  getChannelSource();
    String  getChannelFeed();
    String  getChannelVersion();
    String  getFeedUrl();
    String  getFeedFormat();
    String  getContent();
    String  getContentHash();
    String  getStatus();
    String  getNote();
    String  getLastBuildDate();
    Long  getLastBuildTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
