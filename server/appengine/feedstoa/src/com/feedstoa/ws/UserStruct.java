package com.feedstoa.ws;



public interface UserStruct 
{
    String  getUuid();
    String  getName();
    String  getEmail();
    String  getUrl();
    boolean isEmpty();
}
