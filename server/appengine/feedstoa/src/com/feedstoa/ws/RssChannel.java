package com.feedstoa.ws;

import java.util.List;
import java.util.ArrayList;


public interface RssChannel 
{
    String  getTitle();
    String  getLink();
    String  getDescription();
    String  getLanguage();
    String  getCopyright();
    String  getManagingEditor();
    String  getWebMaster();
    String  getPubDate();
    String  getLastBuildDate();
    List<CategoryStruct>  getCategory();
    String  getGenerator();
    String  getDocs();
    CloudStruct  getCloud();
    Integer  getTtl();
    ImageStruct  getImage();
    String  getRating();
    TextInputStruct  getTextInput();
    List<Integer>  getSkipHours();
    List<String>  getSkipDays();
    List<RssItem>  getItem();
    boolean isEmpty();
}
