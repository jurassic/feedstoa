package com.feedstoa.ws;



public interface CategoryStruct 
{
    String  getUuid();
    String  getDomain();
    String  getLabel();
    String  getTitle();
    boolean isEmpty();
}
