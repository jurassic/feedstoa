package com.feedstoa.ws;



public interface RedirectRule 
{
    String  getRedirectType();
    Double  getPrecedence();
    String  getSourceDomain();
    String  getSourcePath();
    String  getTargetDomain();
    String  getTargetPath();
    String  getNote();
    String  getStatus();
    boolean isEmpty();
}
