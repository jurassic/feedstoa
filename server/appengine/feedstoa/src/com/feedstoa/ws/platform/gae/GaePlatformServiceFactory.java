package com.feedstoa.ws.platform.gae;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.platform.PlatformServiceFactory;
import com.feedstoa.ws.platform.AppIdentityPlatformService;
import com.feedstoa.ws.platform.ConfigPlatformService;
import com.feedstoa.ws.platform.UserPlatformService;
import com.feedstoa.ws.platform.OAuthPlatformService;
import com.feedstoa.ws.platform.MemcachePlatformService;
import com.feedstoa.ws.platform.MailPlatformService;
import com.feedstoa.ws.platform.MessagingPlatformService;


public class GaePlatformServiceFactory extends PlatformServiceFactory
{
    private static final Logger log = Logger.getLogger(GaePlatformServiceFactory.class.getName());

    private GaePlatformServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class GaePlatformServiceFactoryHolder
    {
        private static final GaePlatformServiceFactory INSTANCE = new GaePlatformServiceFactory();
    }

    // Singleton method
    public static GaePlatformServiceFactory getInstance()
    {
        return GaePlatformServiceFactoryHolder.INSTANCE;
    }


    // Platform Services

    public AppIdentityPlatformService getAppIdentityPlatformService()
    {
        return GaeAppIdentityPlatformService.getInstance();
    }

    public ConfigPlatformService getConfigPlatformService()
    {
        return new GaeConfigPlatformService();
    }

    public UserPlatformService getUserPlatformService()
    {
        return GaeUserPlatformService.getInstance();
    }

    public OAuthPlatformService getOAuthPlatformService()
    {
        return GaeOAuthPlatformService.getInstance();
    }

    public MemcachePlatformService getMemcachePlatformService()
    {
        return new GaeMemcachePlatformService();
    }

    public MailPlatformService getMailPlatformService()
    {
        return new GaeMailPlatformService();
    }

    public MessagingPlatformService getMessagingPlatformService()
    {
        return new GaeMessagingPlatformService();
    }


}
