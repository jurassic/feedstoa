package com.feedstoa.ws;



public interface ChannelSource 
{
    String  getGuid();
    String  getUser();
    String  getServiceName();
    String  getServiceUrl();
    String  getFeedUrl();
    String  getFeedFormat();
    String  getChannelTitle();
    String  getChannelDescription();
    String  getChannelCategory();
    String  getChannelUrl();
    String  getChannelLogo();
    String  getStatus();
    String  getNote();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
