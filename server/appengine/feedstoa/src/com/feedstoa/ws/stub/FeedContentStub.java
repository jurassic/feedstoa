package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "feedContent")
@XmlType(propOrder = {"guid", "user", "channelSource", "channelFeed", "channelVersion", "feedUrl", "feedFormat", "content", "contentHash", "status", "note", "lastBuildDate", "lastBuildTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedContentStub implements FeedContent, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedContentStub.class.getName());

    private String guid;
    private String user;
    private String channelSource;
    private String channelFeed;
    private String channelVersion;
    private String feedUrl;
    private String feedFormat;
    private String content;
    private String contentHash;
    private String status;
    private String note;
    private String lastBuildDate;
    private Long lastBuildTime;
    private Long createdTime;
    private Long modifiedTime;

    public FeedContentStub()
    {
        this(null);
    }
    public FeedContentStub(FeedContent bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.channelSource = bean.getChannelSource();
            this.channelFeed = bean.getChannelFeed();
            this.channelVersion = bean.getChannelVersion();
            this.feedUrl = bean.getFeedUrl();
            this.feedFormat = bean.getFeedFormat();
            this.content = bean.getContent();
            this.contentHash = bean.getContentHash();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.lastBuildDate = bean.getLastBuildDate();
            this.lastBuildTime = bean.getLastBuildTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    @XmlElement
    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    @XmlElement
    public String getChannelVersion()
    {
        return this.channelVersion;
    }
    public void setChannelVersion(String channelVersion)
    {
        this.channelVersion = channelVersion;
    }

    @XmlElement
    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    @XmlElement
    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    @XmlElement
    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    @XmlElement
    public String getContentHash()
    {
        return this.contentHash;
    }
    public void setContentHash(String contentHash)
    {
        this.contentHash = contentHash;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    @XmlElement
    public Long getLastBuildTime()
    {
        return this.lastBuildTime;
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        this.lastBuildTime = lastBuildTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("channelSource", this.channelSource);
        dataMap.put("channelFeed", this.channelFeed);
        dataMap.put("channelVersion", this.channelVersion);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("content", this.content);
        dataMap.put("contentHash", this.contentHash);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("lastBuildDate", this.lastBuildDate);
        dataMap.put("lastBuildTime", this.lastBuildTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelSource == null ? 0 : channelSource.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelFeed == null ? 0 : channelFeed.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelVersion == null ? 0 : channelVersion.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentHash == null ? 0 : contentHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildTime == null ? 0 : lastBuildTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static FeedContentStub convertBeanToStub(FeedContent bean)
    {
        FeedContentStub stub = null;
        if(bean instanceof FeedContentStub) {
            stub = (FeedContentStub) bean;
        } else {
            if(bean != null) {
                stub = new FeedContentStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FeedContentStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of FeedContentStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FeedContentStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FeedContentStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FeedContentStub object as a string.", e);
        }
        
        return null;
    }
    public static FeedContentStub fromJsonString(String jsonStr)
    {
        try {
            FeedContentStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FeedContentStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedContentStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedContentStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedContentStub object.", e);
        }
        
        return null;
    }

}
