package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "channelSources")
@XmlType(propOrder = {"channelSource", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelSourceListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelSourceListStub.class.getName());

    private List<ChannelSourceStub> channelSources = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ChannelSourceListStub()
    {
        this(new ArrayList<ChannelSourceStub>());
    }
    public ChannelSourceListStub(List<ChannelSourceStub> channelSources)
    {
        this(channelSources, null);
    }
    public ChannelSourceListStub(List<ChannelSourceStub> channelSources, String forwardCursor)
    {
        this.channelSources = channelSources;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(channelSources == null) {
            return true;
        } else {
            return channelSources.isEmpty();
        }
    }
    public int getSize()
    {
        if(channelSources == null) {
            return 0;
        } else {
            return channelSources.size();
        }
    }


    @XmlElement(name = "channelSource")
    public List<ChannelSourceStub> getChannelSource()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ChannelSourceStub> getList()
    {
        return channelSources;
    }
    public void setList(List<ChannelSourceStub> channelSources)
    {
        this.channelSources = channelSources;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ChannelSourceStub> it = this.channelSources.iterator();
        while(it.hasNext()) {
            ChannelSourceStub channelSource = it.next();
            sb.append(channelSource.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ChannelSourceListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ChannelSourceListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ChannelSourceListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ChannelSourceListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ChannelSourceListStub object as a string.", e);
        }
        
        return null;
    }
    public static ChannelSourceListStub fromJsonString(String jsonStr)
    {
        try {
            ChannelSourceListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ChannelSourceListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelSourceListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelSourceListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelSourceListStub object.", e);
        }
        
        return null;
    }

}
