package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "rssChannels")
@XmlType(propOrder = {"rssChannel", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RssChannelListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssChannelListStub.class.getName());

    private List<RssChannelStub> rssChannels = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public RssChannelListStub()
    {
        this(new ArrayList<RssChannelStub>());
    }
    public RssChannelListStub(List<RssChannelStub> rssChannels)
    {
        this(rssChannels, null);
    }
    public RssChannelListStub(List<RssChannelStub> rssChannels, String forwardCursor)
    {
        this.rssChannels = rssChannels;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(rssChannels == null) {
            return true;
        } else {
            return rssChannels.isEmpty();
        }
    }
    public int getSize()
    {
        if(rssChannels == null) {
            return 0;
        } else {
            return rssChannels.size();
        }
    }


    @XmlElement(name = "rssChannel")
    public List<RssChannelStub> getRssChannel()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RssChannelStub> getList()
    {
        return rssChannels;
    }
    public void setList(List<RssChannelStub> rssChannels)
    {
        this.rssChannels = rssChannels;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<RssChannelStub> it = this.rssChannels.iterator();
        while(it.hasNext()) {
            RssChannelStub rssChannel = it.next();
            sb.append(rssChannel.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RssChannelListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RssChannelListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RssChannelListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RssChannelListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RssChannelListStub object as a string.", e);
        }
        
        return null;
    }
    public static RssChannelListStub fromJsonString(String jsonStr)
    {
        try {
            RssChannelListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RssChannelListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RssChannelListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RssChannelListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RssChannelListStub object.", e);
        }
        
        return null;
    }

}
