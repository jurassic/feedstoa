package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.GeoCoordinateStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "geoCoordinateStructs")
@XmlType(propOrder = {"geoCoordinateStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoCoordinateStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GeoCoordinateStructListStub.class.getName());

    private List<GeoCoordinateStructStub> geoCoordinateStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public GeoCoordinateStructListStub()
    {
        this(new ArrayList<GeoCoordinateStructStub>());
    }
    public GeoCoordinateStructListStub(List<GeoCoordinateStructStub> geoCoordinateStructs)
    {
        this(geoCoordinateStructs, null);
    }
    public GeoCoordinateStructListStub(List<GeoCoordinateStructStub> geoCoordinateStructs, String forwardCursor)
    {
        this.geoCoordinateStructs = geoCoordinateStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(geoCoordinateStructs == null) {
            return true;
        } else {
            return geoCoordinateStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(geoCoordinateStructs == null) {
            return 0;
        } else {
            return geoCoordinateStructs.size();
        }
    }


    @XmlElement(name = "geoCoordinateStruct")
    public List<GeoCoordinateStructStub> getGeoCoordinateStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<GeoCoordinateStructStub> getList()
    {
        return geoCoordinateStructs;
    }
    public void setList(List<GeoCoordinateStructStub> geoCoordinateStructs)
    {
        this.geoCoordinateStructs = geoCoordinateStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<GeoCoordinateStructStub> it = this.geoCoordinateStructs.iterator();
        while(it.hasNext()) {
            GeoCoordinateStructStub geoCoordinateStruct = it.next();
            sb.append(geoCoordinateStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static GeoCoordinateStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of GeoCoordinateStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write GeoCoordinateStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write GeoCoordinateStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write GeoCoordinateStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static GeoCoordinateStructListStub fromJsonString(String jsonStr)
    {
        try {
            GeoCoordinateStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, GeoCoordinateStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoCoordinateStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoCoordinateStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into GeoCoordinateStructListStub object.", e);
        }
        
        return null;
    }

}
