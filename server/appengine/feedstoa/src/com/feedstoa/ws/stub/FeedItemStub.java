package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "feedItem")
@XmlType(propOrder = {"guid", "user", "fetchRequest", "fetchUrl", "feedUrl", "channelSource", "channelFeed", "feedFormat", "title", "linkStub", "summary", "content", "authorStub", "contributorStub", "categoryStub", "comments", "enclosureStub", "id", "pubDate", "sourceStub", "feedContent", "status", "note", "referrerInfoStub", "publishedTime", "lastUpdatedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedItemStub implements FeedItem, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedItemStub.class.getName());

    private String guid;
    private String user;
    private String fetchRequest;
    private String fetchUrl;
    private String feedUrl;
    private String channelSource;
    private String channelFeed;
    private String feedFormat;
    private String title;
    private UriStructStub link;
    private String summary;
    private String content;
    private UserStructStub author;
    private List<UserStructStub> contributor;
    private List<CategoryStructStub> category;
    private String comments;
    private EnclosureStructStub enclosure;
    private String id;
    private String pubDate;
    private UriStructStub source;
    private String feedContent;
    private String status;
    private String note;
    private ReferrerInfoStructStub referrerInfo;
    private Long publishedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    public FeedItemStub()
    {
        this(null);
    }
    public FeedItemStub(FeedItem bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.fetchRequest = bean.getFetchRequest();
            this.fetchUrl = bean.getFetchUrl();
            this.feedUrl = bean.getFeedUrl();
            this.channelSource = bean.getChannelSource();
            this.channelFeed = bean.getChannelFeed();
            this.feedFormat = bean.getFeedFormat();
            this.title = bean.getTitle();
            this.link = UriStructStub.convertBeanToStub(bean.getLink());
            this.summary = bean.getSummary();
            this.content = bean.getContent();
            this.author = UserStructStub.convertBeanToStub(bean.getAuthor());
            List<UserStructStub> _contributorStubs = null;
            if(bean.getContributor() != null) {
                _contributorStubs = new ArrayList<UserStructStub>();
                for(UserStruct b : bean.getContributor()) {
                    _contributorStubs.add(UserStructStub.convertBeanToStub(b));
                }
            }
            this.contributor = _contributorStubs;
            List<CategoryStructStub> _categoryStubs = null;
            if(bean.getCategory() != null) {
                _categoryStubs = new ArrayList<CategoryStructStub>();
                for(CategoryStruct b : bean.getCategory()) {
                    _categoryStubs.add(CategoryStructStub.convertBeanToStub(b));
                }
            }
            this.category = _categoryStubs;
            this.comments = bean.getComments();
            this.enclosure = EnclosureStructStub.convertBeanToStub(bean.getEnclosure());
            this.id = bean.getId();
            this.pubDate = bean.getPubDate();
            this.source = UriStructStub.convertBeanToStub(bean.getSource());
            this.feedContent = bean.getFeedContent();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.publishedTime = bean.getPublishedTime();
            this.lastUpdatedTime = bean.getLastUpdatedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    @XmlElement
    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    @XmlElement
    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    @XmlElement
    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    @XmlElement
    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    @XmlElement
    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    @XmlElement
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlElement(name = "link")
    @JsonIgnore
    public UriStructStub getLinkStub()
    {
        return this.link;
    }
    public void setLinkStub(UriStructStub link)
    {
        this.link = link;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UriStructStub.class)
    public UriStruct getLink()
    {  
        return getLinkStub();
    }
    public void setLink(UriStruct link)
    {
        if((link == null) || (link instanceof UriStructStub)) {
            setLinkStub((UriStructStub) link);
        } else {
            // TBD
            setLinkStub(UriStructStub.convertBeanToStub(link));
        }
    }

    @XmlElement
    public String getSummary()
    {
        return this.summary;
    }
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    @XmlElement
    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    @XmlElement(name = "author")
    @JsonIgnore
    public UserStructStub getAuthorStub()
    {
        return this.author;
    }
    public void setAuthorStub(UserStructStub author)
    {
        this.author = author;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UserStructStub.class)
    public UserStruct getAuthor()
    {  
        return getAuthorStub();
    }
    public void setAuthor(UserStruct author)
    {
        if((author == null) || (author instanceof UserStructStub)) {
            setAuthorStub((UserStructStub) author);
        } else {
            // TBD
            setAuthorStub(UserStructStub.convertBeanToStub(author));
        }
    }

    @XmlElement(name = "contributor")
    @JsonIgnore
    public List<UserStructStub> getContributorStub()
    {
        return this.contributor;
    }
    public void setContributorStub(List<UserStructStub> contributor)
    {
        this.contributor = contributor;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=UserStructStub.class)
    public List<UserStruct> getContributor()
    {  
        return (List<UserStruct>) ((List<?>) getContributorStub());
    }
    public void setContributor(List<UserStruct> contributor)
    {
        // TBD
        //if(contributor instanceof List<UserStructStub>) {
            setContributorStub((List<UserStructStub>) ((List<?>) contributor));
        //} else {
        //    // TBD
        //    List<UserStructStub> _UserStructList = new ArrayList<UserStructStub>(); // ???
        //    for(UserStruct b : contributor) {
        //        _UserStructList.add(UserStructStub.convertBeanToStub(contributor));
        //    }
        //    setContributorStub(_UserStructList);
        //}
    }

    @XmlElement(name = "category")
    @JsonIgnore
    public List<CategoryStructStub> getCategoryStub()
    {
        return this.category;
    }
    public void setCategoryStub(List<CategoryStructStub> category)
    {
        this.category = category;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=CategoryStructStub.class)
    public List<CategoryStruct> getCategory()
    {  
        return (List<CategoryStruct>) ((List<?>) getCategoryStub());
    }
    public void setCategory(List<CategoryStruct> category)
    {
        // TBD
        //if(category instanceof List<CategoryStructStub>) {
            setCategoryStub((List<CategoryStructStub>) ((List<?>) category));
        //} else {
        //    // TBD
        //    List<CategoryStructStub> _CategoryStructList = new ArrayList<CategoryStructStub>(); // ???
        //    for(CategoryStruct b : category) {
        //        _CategoryStructList.add(CategoryStructStub.convertBeanToStub(category));
        //    }
        //    setCategoryStub(_CategoryStructList);
        //}
    }

    @XmlElement
    public String getComments()
    {
        return this.comments;
    }
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    @XmlElement(name = "enclosure")
    @JsonIgnore
    public EnclosureStructStub getEnclosureStub()
    {
        return this.enclosure;
    }
    public void setEnclosureStub(EnclosureStructStub enclosure)
    {
        this.enclosure = enclosure;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=EnclosureStructStub.class)
    public EnclosureStruct getEnclosure()
    {  
        return getEnclosureStub();
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if((enclosure == null) || (enclosure instanceof EnclosureStructStub)) {
            setEnclosureStub((EnclosureStructStub) enclosure);
        } else {
            // TBD
            setEnclosureStub(EnclosureStructStub.convertBeanToStub(enclosure));
        }
    }

    @XmlElement
    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    @XmlElement
    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    @XmlElement(name = "source")
    @JsonIgnore
    public UriStructStub getSourceStub()
    {
        return this.source;
    }
    public void setSourceStub(UriStructStub source)
    {
        this.source = source;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UriStructStub.class)
    public UriStruct getSource()
    {  
        return getSourceStub();
    }
    public void setSource(UriStruct source)
    {
        if((source == null) || (source instanceof UriStructStub)) {
            setSourceStub((UriStructStub) source);
        } else {
            // TBD
            setSourceStub(UriStructStub.convertBeanToStub(source));
        }
    }

    @XmlElement
    public String getFeedContent()
    {
        return this.feedContent;
    }
    public void setFeedContent(String feedContent)
    {
        this.feedContent = feedContent;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public Long getPublishedTime()
    {
        return this.publishedTime;
    }
    public void setPublishedTime(Long publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("fetchRequest", this.fetchRequest);
        dataMap.put("fetchUrl", this.fetchUrl);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("channelSource", this.channelSource);
        dataMap.put("channelFeed", this.channelFeed);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("title", this.title);
        dataMap.put("link", this.link);
        dataMap.put("summary", this.summary);
        dataMap.put("content", this.content);
        dataMap.put("author", this.author);
        dataMap.put("contributor", this.contributor);
        dataMap.put("category", this.category);
        dataMap.put("comments", this.comments);
        dataMap.put("enclosure", this.enclosure);
        dataMap.put("id", this.id);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("source", this.source);
        dataMap.put("feedContent", this.feedContent);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("publishedTime", this.publishedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelSource == null ? 0 : channelSource.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelFeed == null ? 0 : channelFeed.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = summary == null ? 0 : summary.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = author == null ? 0 : author.hashCode();
        _hash = 31 * _hash + delta;
        delta = contributor == null ? 0 : contributor.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = comments == null ? 0 : comments.hashCode();
        _hash = 31 * _hash + delta;
        delta = enclosure == null ? 0 : enclosure.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedContent == null ? 0 : feedContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = publishedTime == null ? 0 : publishedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static FeedItemStub convertBeanToStub(FeedItem bean)
    {
        FeedItemStub stub = null;
        if(bean instanceof FeedItemStub) {
            stub = (FeedItemStub) bean;
        } else {
            if(bean != null) {
                stub = new FeedItemStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FeedItemStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of FeedItemStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FeedItemStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FeedItemStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FeedItemStub object as a string.", e);
        }
        
        return null;
    }
    public static FeedItemStub fromJsonString(String jsonStr)
    {
        try {
            FeedItemStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FeedItemStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedItemStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedItemStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedItemStub object.", e);
        }
        
        return null;
    }

}
