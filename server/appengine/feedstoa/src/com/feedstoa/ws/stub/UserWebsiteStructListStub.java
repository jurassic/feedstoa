package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.UserWebsiteStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "userWebsiteStructs")
@XmlType(propOrder = {"userWebsiteStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWebsiteStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserWebsiteStructListStub.class.getName());

    private List<UserWebsiteStructStub> userWebsiteStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public UserWebsiteStructListStub()
    {
        this(new ArrayList<UserWebsiteStructStub>());
    }
    public UserWebsiteStructListStub(List<UserWebsiteStructStub> userWebsiteStructs)
    {
        this(userWebsiteStructs, null);
    }
    public UserWebsiteStructListStub(List<UserWebsiteStructStub> userWebsiteStructs, String forwardCursor)
    {
        this.userWebsiteStructs = userWebsiteStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(userWebsiteStructs == null) {
            return true;
        } else {
            return userWebsiteStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(userWebsiteStructs == null) {
            return 0;
        } else {
            return userWebsiteStructs.size();
        }
    }


    @XmlElement(name = "userWebsiteStruct")
    public List<UserWebsiteStructStub> getUserWebsiteStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserWebsiteStructStub> getList()
    {
        return userWebsiteStructs;
    }
    public void setList(List<UserWebsiteStructStub> userWebsiteStructs)
    {
        this.userWebsiteStructs = userWebsiteStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<UserWebsiteStructStub> it = this.userWebsiteStructs.iterator();
        while(it.hasNext()) {
            UserWebsiteStructStub userWebsiteStruct = it.next();
            sb.append(userWebsiteStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserWebsiteStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserWebsiteStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserWebsiteStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserWebsiteStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserWebsiteStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserWebsiteStructListStub fromJsonString(String jsonStr)
    {
        try {
            UserWebsiteStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserWebsiteStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserWebsiteStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserWebsiteStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserWebsiteStructListStub object.", e);
        }
        
        return null;
    }

}
