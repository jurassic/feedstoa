package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "channelFeed")
@XmlType(propOrder = {"guid", "user", "channelSource", "channelCode", "previousVersion", "fetchRequest", "fetchUrl", "feedServiceUrl", "feedUrl", "feedFormat", "maxItemCount", "feedCategory", "title", "subtitle", "linkStub", "description", "language", "copyright", "managingEditorStub", "webMasterStub", "contributorStub", "pubDate", "lastBuildDate", "categoryStub", "generator", "docs", "cloudStub", "ttl", "logoStub", "iconStub", "rating", "textInputStub", "skipHours", "skipDays", "outputText", "outputHash", "feedContent", "status", "note", "referrerInfoStub", "lastBuildTime", "publishedTime", "expirationTime", "lastUpdatedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelFeedStub implements ChannelFeed, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedStub.class.getName());

    private String guid;
    private String user;
    private String channelSource;
    private String channelCode;
    private String previousVersion;
    private String fetchRequest;
    private String fetchUrl;
    private String feedServiceUrl;
    private String feedUrl;
    private String feedFormat;
    private Integer maxItemCount;
    private String feedCategory;
    private String title;
    private String subtitle;
    private UriStructStub link;
    private String description;
    private String language;
    private String copyright;
    private UserStructStub managingEditor;
    private UserStructStub webMaster;
    private List<UserStructStub> contributor;
    private String pubDate;
    private String lastBuildDate;
    private List<CategoryStructStub> category;
    private String generator;
    private String docs;
    private CloudStructStub cloud;
    private Integer ttl;
    private ImageStructStub logo;
    private ImageStructStub icon;
    private String rating;
    private TextInputStructStub textInput;
    private List<Integer> skipHours;
    private List<String> skipDays;
    private String outputText;
    private String outputHash;
    private String feedContent;
    private String status;
    private String note;
    private ReferrerInfoStructStub referrerInfo;
    private Long lastBuildTime;
    private Long publishedTime;
    private Long expirationTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    public ChannelFeedStub()
    {
        this(null);
    }
    public ChannelFeedStub(ChannelFeed bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.channelSource = bean.getChannelSource();
            this.channelCode = bean.getChannelCode();
            this.previousVersion = bean.getPreviousVersion();
            this.fetchRequest = bean.getFetchRequest();
            this.fetchUrl = bean.getFetchUrl();
            this.feedServiceUrl = bean.getFeedServiceUrl();
            this.feedUrl = bean.getFeedUrl();
            this.feedFormat = bean.getFeedFormat();
            this.maxItemCount = bean.getMaxItemCount();
            this.feedCategory = bean.getFeedCategory();
            this.title = bean.getTitle();
            this.subtitle = bean.getSubtitle();
            this.link = UriStructStub.convertBeanToStub(bean.getLink());
            this.description = bean.getDescription();
            this.language = bean.getLanguage();
            this.copyright = bean.getCopyright();
            this.managingEditor = UserStructStub.convertBeanToStub(bean.getManagingEditor());
            this.webMaster = UserStructStub.convertBeanToStub(bean.getWebMaster());
            List<UserStructStub> _contributorStubs = null;
            if(bean.getContributor() != null) {
                _contributorStubs = new ArrayList<UserStructStub>();
                for(UserStruct b : bean.getContributor()) {
                    _contributorStubs.add(UserStructStub.convertBeanToStub(b));
                }
            }
            this.contributor = _contributorStubs;
            this.pubDate = bean.getPubDate();
            this.lastBuildDate = bean.getLastBuildDate();
            List<CategoryStructStub> _categoryStubs = null;
            if(bean.getCategory() != null) {
                _categoryStubs = new ArrayList<CategoryStructStub>();
                for(CategoryStruct b : bean.getCategory()) {
                    _categoryStubs.add(CategoryStructStub.convertBeanToStub(b));
                }
            }
            this.category = _categoryStubs;
            this.generator = bean.getGenerator();
            this.docs = bean.getDocs();
            this.cloud = CloudStructStub.convertBeanToStub(bean.getCloud());
            this.ttl = bean.getTtl();
            this.logo = ImageStructStub.convertBeanToStub(bean.getLogo());
            this.icon = ImageStructStub.convertBeanToStub(bean.getIcon());
            this.rating = bean.getRating();
            this.textInput = TextInputStructStub.convertBeanToStub(bean.getTextInput());
            this.skipHours = bean.getSkipHours();
            this.skipDays = bean.getSkipDays();
            this.outputText = bean.getOutputText();
            this.outputHash = bean.getOutputHash();
            this.feedContent = bean.getFeedContent();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.lastBuildTime = bean.getLastBuildTime();
            this.publishedTime = bean.getPublishedTime();
            this.expirationTime = bean.getExpirationTime();
            this.lastUpdatedTime = bean.getLastUpdatedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    @XmlElement
    public String getChannelCode()
    {
        return this.channelCode;
    }
    public void setChannelCode(String channelCode)
    {
        this.channelCode = channelCode;
    }

    @XmlElement
    public String getPreviousVersion()
    {
        return this.previousVersion;
    }
    public void setPreviousVersion(String previousVersion)
    {
        this.previousVersion = previousVersion;
    }

    @XmlElement
    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    @XmlElement
    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    @XmlElement
    public String getFeedServiceUrl()
    {
        return this.feedServiceUrl;
    }
    public void setFeedServiceUrl(String feedServiceUrl)
    {
        this.feedServiceUrl = feedServiceUrl;
    }

    @XmlElement
    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    @XmlElement
    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    @XmlElement
    public Integer getMaxItemCount()
    {
        return this.maxItemCount;
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        this.maxItemCount = maxItemCount;
    }

    @XmlElement
    public String getFeedCategory()
    {
        return this.feedCategory;
    }
    public void setFeedCategory(String feedCategory)
    {
        this.feedCategory = feedCategory;
    }

    @XmlElement
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlElement
    public String getSubtitle()
    {
        return this.subtitle;
    }
    public void setSubtitle(String subtitle)
    {
        this.subtitle = subtitle;
    }

    @XmlElement(name = "link")
    @JsonIgnore
    public UriStructStub getLinkStub()
    {
        return this.link;
    }
    public void setLinkStub(UriStructStub link)
    {
        this.link = link;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UriStructStub.class)
    public UriStruct getLink()
    {  
        return getLinkStub();
    }
    public void setLink(UriStruct link)
    {
        if((link == null) || (link instanceof UriStructStub)) {
            setLinkStub((UriStructStub) link);
        } else {
            // TBD
            setLinkStub(UriStructStub.convertBeanToStub(link));
        }
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    @XmlElement
    public String getCopyright()
    {
        return this.copyright;
    }
    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    @XmlElement(name = "managingEditor")
    @JsonIgnore
    public UserStructStub getManagingEditorStub()
    {
        return this.managingEditor;
    }
    public void setManagingEditorStub(UserStructStub managingEditor)
    {
        this.managingEditor = managingEditor;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UserStructStub.class)
    public UserStruct getManagingEditor()
    {  
        return getManagingEditorStub();
    }
    public void setManagingEditor(UserStruct managingEditor)
    {
        if((managingEditor == null) || (managingEditor instanceof UserStructStub)) {
            setManagingEditorStub((UserStructStub) managingEditor);
        } else {
            // TBD
            setManagingEditorStub(UserStructStub.convertBeanToStub(managingEditor));
        }
    }

    @XmlElement(name = "webMaster")
    @JsonIgnore
    public UserStructStub getWebMasterStub()
    {
        return this.webMaster;
    }
    public void setWebMasterStub(UserStructStub webMaster)
    {
        this.webMaster = webMaster;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UserStructStub.class)
    public UserStruct getWebMaster()
    {  
        return getWebMasterStub();
    }
    public void setWebMaster(UserStruct webMaster)
    {
        if((webMaster == null) || (webMaster instanceof UserStructStub)) {
            setWebMasterStub((UserStructStub) webMaster);
        } else {
            // TBD
            setWebMasterStub(UserStructStub.convertBeanToStub(webMaster));
        }
    }

    @XmlElement(name = "contributor")
    @JsonIgnore
    public List<UserStructStub> getContributorStub()
    {
        return this.contributor;
    }
    public void setContributorStub(List<UserStructStub> contributor)
    {
        this.contributor = contributor;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=UserStructStub.class)
    public List<UserStruct> getContributor()
    {  
        return (List<UserStruct>) ((List<?>) getContributorStub());
    }
    public void setContributor(List<UserStruct> contributor)
    {
        // TBD
        //if(contributor instanceof List<UserStructStub>) {
            setContributorStub((List<UserStructStub>) ((List<?>) contributor));
        //} else {
        //    // TBD
        //    List<UserStructStub> _UserStructList = new ArrayList<UserStructStub>(); // ???
        //    for(UserStruct b : contributor) {
        //        _UserStructList.add(UserStructStub.convertBeanToStub(contributor));
        //    }
        //    setContributorStub(_UserStructList);
        //}
    }

    @XmlElement
    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    @XmlElement
    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    @XmlElement(name = "category")
    @JsonIgnore
    public List<CategoryStructStub> getCategoryStub()
    {
        return this.category;
    }
    public void setCategoryStub(List<CategoryStructStub> category)
    {
        this.category = category;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=CategoryStructStub.class)
    public List<CategoryStruct> getCategory()
    {  
        return (List<CategoryStruct>) ((List<?>) getCategoryStub());
    }
    public void setCategory(List<CategoryStruct> category)
    {
        // TBD
        //if(category instanceof List<CategoryStructStub>) {
            setCategoryStub((List<CategoryStructStub>) ((List<?>) category));
        //} else {
        //    // TBD
        //    List<CategoryStructStub> _CategoryStructList = new ArrayList<CategoryStructStub>(); // ???
        //    for(CategoryStruct b : category) {
        //        _CategoryStructList.add(CategoryStructStub.convertBeanToStub(category));
        //    }
        //    setCategoryStub(_CategoryStructList);
        //}
    }

    @XmlElement
    public String getGenerator()
    {
        return this.generator;
    }
    public void setGenerator(String generator)
    {
        this.generator = generator;
    }

    @XmlElement
    public String getDocs()
    {
        return this.docs;
    }
    public void setDocs(String docs)
    {
        this.docs = docs;
    }

    @XmlElement(name = "cloud")
    @JsonIgnore
    public CloudStructStub getCloudStub()
    {
        return this.cloud;
    }
    public void setCloudStub(CloudStructStub cloud)
    {
        this.cloud = cloud;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=CloudStructStub.class)
    public CloudStruct getCloud()
    {  
        return getCloudStub();
    }
    public void setCloud(CloudStruct cloud)
    {
        if((cloud == null) || (cloud instanceof CloudStructStub)) {
            setCloudStub((CloudStructStub) cloud);
        } else {
            // TBD
            setCloudStub(CloudStructStub.convertBeanToStub(cloud));
        }
    }

    @XmlElement
    public Integer getTtl()
    {
        return this.ttl;
    }
    public void setTtl(Integer ttl)
    {
        this.ttl = ttl;
    }

    @XmlElement(name = "logo")
    @JsonIgnore
    public ImageStructStub getLogoStub()
    {
        return this.logo;
    }
    public void setLogoStub(ImageStructStub logo)
    {
        this.logo = logo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ImageStructStub.class)
    public ImageStruct getLogo()
    {  
        return getLogoStub();
    }
    public void setLogo(ImageStruct logo)
    {
        if((logo == null) || (logo instanceof ImageStructStub)) {
            setLogoStub((ImageStructStub) logo);
        } else {
            // TBD
            setLogoStub(ImageStructStub.convertBeanToStub(logo));
        }
    }

    @XmlElement(name = "icon")
    @JsonIgnore
    public ImageStructStub getIconStub()
    {
        return this.icon;
    }
    public void setIconStub(ImageStructStub icon)
    {
        this.icon = icon;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ImageStructStub.class)
    public ImageStruct getIcon()
    {  
        return getIconStub();
    }
    public void setIcon(ImageStruct icon)
    {
        if((icon == null) || (icon instanceof ImageStructStub)) {
            setIconStub((ImageStructStub) icon);
        } else {
            // TBD
            setIconStub(ImageStructStub.convertBeanToStub(icon));
        }
    }

    @XmlElement
    public String getRating()
    {
        return this.rating;
    }
    public void setRating(String rating)
    {
        this.rating = rating;
    }

    @XmlElement(name = "textInput")
    @JsonIgnore
    public TextInputStructStub getTextInputStub()
    {
        return this.textInput;
    }
    public void setTextInputStub(TextInputStructStub textInput)
    {
        this.textInput = textInput;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TextInputStructStub.class)
    public TextInputStruct getTextInput()
    {  
        return getTextInputStub();
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if((textInput == null) || (textInput instanceof TextInputStructStub)) {
            setTextInputStub((TextInputStructStub) textInput);
        } else {
            // TBD
            setTextInputStub(TextInputStructStub.convertBeanToStub(textInput));
        }
    }

    @XmlElement
    public List<Integer> getSkipHours()
    {
        return this.skipHours;
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        this.skipHours = skipHours;
    }

    @XmlElement
    public List<String> getSkipDays()
    {
        return this.skipDays;
    }
    public void setSkipDays(List<String> skipDays)
    {
        this.skipDays = skipDays;
    }

    @XmlElement
    public String getOutputText()
    {
        return this.outputText;
    }
    public void setOutputText(String outputText)
    {
        this.outputText = outputText;
    }

    @XmlElement
    public String getOutputHash()
    {
        return this.outputHash;
    }
    public void setOutputHash(String outputHash)
    {
        this.outputHash = outputHash;
    }

    @XmlElement
    public String getFeedContent()
    {
        return this.feedContent;
    }
    public void setFeedContent(String feedContent)
    {
        this.feedContent = feedContent;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public Long getLastBuildTime()
    {
        return this.lastBuildTime;
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        this.lastBuildTime = lastBuildTime;
    }

    @XmlElement
    public Long getPublishedTime()
    {
        return this.publishedTime;
    }
    public void setPublishedTime(Long publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("channelSource", this.channelSource);
        dataMap.put("channelCode", this.channelCode);
        dataMap.put("previousVersion", this.previousVersion);
        dataMap.put("fetchRequest", this.fetchRequest);
        dataMap.put("fetchUrl", this.fetchUrl);
        dataMap.put("feedServiceUrl", this.feedServiceUrl);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("maxItemCount", this.maxItemCount);
        dataMap.put("feedCategory", this.feedCategory);
        dataMap.put("title", this.title);
        dataMap.put("subtitle", this.subtitle);
        dataMap.put("link", this.link);
        dataMap.put("description", this.description);
        dataMap.put("language", this.language);
        dataMap.put("copyright", this.copyright);
        dataMap.put("managingEditor", this.managingEditor);
        dataMap.put("webMaster", this.webMaster);
        dataMap.put("contributor", this.contributor);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("lastBuildDate", this.lastBuildDate);
        dataMap.put("category", this.category);
        dataMap.put("generator", this.generator);
        dataMap.put("docs", this.docs);
        dataMap.put("cloud", this.cloud);
        dataMap.put("ttl", this.ttl);
        dataMap.put("logo", this.logo);
        dataMap.put("icon", this.icon);
        dataMap.put("rating", this.rating);
        dataMap.put("textInput", this.textInput);
        dataMap.put("skipHours", this.skipHours);
        dataMap.put("skipDays", this.skipDays);
        dataMap.put("outputText", this.outputText);
        dataMap.put("outputHash", this.outputHash);
        dataMap.put("feedContent", this.feedContent);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("lastBuildTime", this.lastBuildTime);
        dataMap.put("publishedTime", this.publishedTime);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelSource == null ? 0 : channelSource.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelCode == null ? 0 : channelCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousVersion == null ? 0 : previousVersion.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedServiceUrl == null ? 0 : feedServiceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxItemCount == null ? 0 : maxItemCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedCategory == null ? 0 : feedCategory.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = subtitle == null ? 0 : subtitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = copyright == null ? 0 : copyright.hashCode();
        _hash = 31 * _hash + delta;
        delta = managingEditor == null ? 0 : managingEditor.hashCode();
        _hash = 31 * _hash + delta;
        delta = webMaster == null ? 0 : webMaster.hashCode();
        _hash = 31 * _hash + delta;
        delta = contributor == null ? 0 : contributor.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = generator == null ? 0 : generator.hashCode();
        _hash = 31 * _hash + delta;
        delta = docs == null ? 0 : docs.hashCode();
        _hash = 31 * _hash + delta;
        delta = cloud == null ? 0 : cloud.hashCode();
        _hash = 31 * _hash + delta;
        delta = ttl == null ? 0 : ttl.hashCode();
        _hash = 31 * _hash + delta;
        delta = logo == null ? 0 : logo.hashCode();
        _hash = 31 * _hash + delta;
        delta = icon == null ? 0 : icon.hashCode();
        _hash = 31 * _hash + delta;
        delta = rating == null ? 0 : rating.hashCode();
        _hash = 31 * _hash + delta;
        delta = textInput == null ? 0 : textInput.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipHours == null ? 0 : skipHours.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipDays == null ? 0 : skipDays.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputText == null ? 0 : outputText.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputHash == null ? 0 : outputHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedContent == null ? 0 : feedContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildTime == null ? 0 : lastBuildTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = publishedTime == null ? 0 : publishedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ChannelFeedStub convertBeanToStub(ChannelFeed bean)
    {
        ChannelFeedStub stub = null;
        if(bean instanceof ChannelFeedStub) {
            stub = (ChannelFeedStub) bean;
        } else {
            if(bean != null) {
                stub = new ChannelFeedStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ChannelFeedStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ChannelFeedStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ChannelFeedStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ChannelFeedStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ChannelFeedStub object as a string.", e);
        }
        
        return null;
    }
    public static ChannelFeedStub fromJsonString(String jsonStr)
    {
        try {
            ChannelFeedStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ChannelFeedStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelFeedStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelFeedStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelFeedStub object.", e);
        }
        
        return null;
    }

}
