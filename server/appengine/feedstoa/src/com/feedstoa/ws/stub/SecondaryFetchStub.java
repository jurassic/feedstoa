package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "secondaryFetch")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "ownerUser", "userAcl", "user", "title", "description", "fetchUrl", "feedUrl", "channelFeed", "reuseChannel", "maxItemCount", "note", "status", "fetchRequest", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SecondaryFetchStub extends FetchBaseStub implements SecondaryFetch, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SecondaryFetchStub.class.getName());

    private String fetchRequest;

    public SecondaryFetchStub()
    {
        this(null);
    }
    public SecondaryFetchStub(SecondaryFetch bean)
    {
        super(bean);
        if(bean != null) {
            this.fetchRequest = bean.getFetchRequest();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getManagerApp()
    {
        return super.getManagerApp();
    }
    public void setManagerApp(String managerApp)
    {
        super.setManagerApp(managerApp);
    }

    @XmlElement
    public Long getAppAcl()
    {
        return super.getAppAcl();
    }
    public void setAppAcl(Long appAcl)
    {
        super.setAppAcl(appAcl);
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return super.getGaeAppStub();
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        super.setGaeAppStub(gaeApp);
    }
    @XmlTransient
    @JsonDeserialize(as=GaeAppStructStub.class)
    public GaeAppStruct getGaeApp()
    {  
        return super.getGaeApp();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        super.setGaeApp(gaeApp);
    }

    @XmlElement
    public String getOwnerUser()
    {
        return super.getOwnerUser();
    }
    public void setOwnerUser(String ownerUser)
    {
        super.setOwnerUser(ownerUser);
    }

    @XmlElement
    public Long getUserAcl()
    {
        return super.getUserAcl();
    }
    public void setUserAcl(Long userAcl)
    {
        super.setUserAcl(userAcl);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getFetchUrl()
    {
        return super.getFetchUrl();
    }
    public void setFetchUrl(String fetchUrl)
    {
        super.setFetchUrl(fetchUrl);
    }

    @XmlElement
    public String getFeedUrl()
    {
        return super.getFeedUrl();
    }
    public void setFeedUrl(String feedUrl)
    {
        super.setFeedUrl(feedUrl);
    }

    @XmlElement
    public String getChannelFeed()
    {
        return super.getChannelFeed();
    }
    public void setChannelFeed(String channelFeed)
    {
        super.setChannelFeed(channelFeed);
    }

    @XmlElement
    public Boolean isReuseChannel()
    {
        return super.isReuseChannel();
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        super.setReuseChannel(reuseChannel);
    }

    @XmlElement
    public Integer getMaxItemCount()
    {
        return super.getMaxItemCount();
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        super.setMaxItemCount(maxItemCount);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("fetchRequest", this.fetchRequest);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static SecondaryFetchStub convertBeanToStub(SecondaryFetch bean)
    {
        SecondaryFetchStub stub = null;
        if(bean instanceof SecondaryFetchStub) {
            stub = (SecondaryFetchStub) bean;
        } else {
            if(bean != null) {
                stub = new SecondaryFetchStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static SecondaryFetchStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of SecondaryFetchStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write SecondaryFetchStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write SecondaryFetchStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write SecondaryFetchStub object as a string.", e);
        }
        
        return null;
    }
    public static SecondaryFetchStub fromJsonString(String jsonStr)
    {
        try {
            SecondaryFetchStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, SecondaryFetchStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into SecondaryFetchStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into SecondaryFetchStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into SecondaryFetchStub object.", e);
        }
        
        return null;
    }

}
