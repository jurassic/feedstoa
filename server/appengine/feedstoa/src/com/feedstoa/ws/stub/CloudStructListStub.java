package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "cloudStructs")
@XmlType(propOrder = {"cloudStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CloudStructListStub.class.getName());

    private List<CloudStructStub> cloudStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public CloudStructListStub()
    {
        this(new ArrayList<CloudStructStub>());
    }
    public CloudStructListStub(List<CloudStructStub> cloudStructs)
    {
        this(cloudStructs, null);
    }
    public CloudStructListStub(List<CloudStructStub> cloudStructs, String forwardCursor)
    {
        this.cloudStructs = cloudStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(cloudStructs == null) {
            return true;
        } else {
            return cloudStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(cloudStructs == null) {
            return 0;
        } else {
            return cloudStructs.size();
        }
    }


    @XmlElement(name = "cloudStruct")
    public List<CloudStructStub> getCloudStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CloudStructStub> getList()
    {
        return cloudStructs;
    }
    public void setList(List<CloudStructStub> cloudStructs)
    {
        this.cloudStructs = cloudStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<CloudStructStub> it = this.cloudStructs.iterator();
        while(it.hasNext()) {
            CloudStructStub cloudStruct = it.next();
            sb.append(cloudStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CloudStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of CloudStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CloudStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CloudStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CloudStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static CloudStructListStub fromJsonString(String jsonStr)
    {
        try {
            CloudStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CloudStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CloudStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CloudStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CloudStructListStub object.", e);
        }
        
        return null;
    }

}
