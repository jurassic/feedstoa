package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "secondaryFetches")
@XmlType(propOrder = {"secondaryFetch", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SecondaryFetchListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SecondaryFetchListStub.class.getName());

    private List<SecondaryFetchStub> secondaryFetches = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public SecondaryFetchListStub()
    {
        this(new ArrayList<SecondaryFetchStub>());
    }
    public SecondaryFetchListStub(List<SecondaryFetchStub> secondaryFetches)
    {
        this(secondaryFetches, null);
    }
    public SecondaryFetchListStub(List<SecondaryFetchStub> secondaryFetches, String forwardCursor)
    {
        this.secondaryFetches = secondaryFetches;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(secondaryFetches == null) {
            return true;
        } else {
            return secondaryFetches.isEmpty();
        }
    }
    public int getSize()
    {
        if(secondaryFetches == null) {
            return 0;
        } else {
            return secondaryFetches.size();
        }
    }


    @XmlElement(name = "secondaryFetch")
    public List<SecondaryFetchStub> getSecondaryFetch()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<SecondaryFetchStub> getList()
    {
        return secondaryFetches;
    }
    public void setList(List<SecondaryFetchStub> secondaryFetches)
    {
        this.secondaryFetches = secondaryFetches;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<SecondaryFetchStub> it = this.secondaryFetches.iterator();
        while(it.hasNext()) {
            SecondaryFetchStub secondaryFetch = it.next();
            sb.append(secondaryFetch.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static SecondaryFetchListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of SecondaryFetchListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write SecondaryFetchListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write SecondaryFetchListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write SecondaryFetchListStub object as a string.", e);
        }
        
        return null;
    }
    public static SecondaryFetchListStub fromJsonString(String jsonStr)
    {
        try {
            SecondaryFetchListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, SecondaryFetchListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into SecondaryFetchListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into SecondaryFetchListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into SecondaryFetchListStub object.", e);
        }
        
        return null;
    }

}
