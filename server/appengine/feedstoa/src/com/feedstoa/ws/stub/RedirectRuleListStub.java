package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "redirectRules")
@XmlType(propOrder = {"redirectRule", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RedirectRuleListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RedirectRuleListStub.class.getName());

    private List<RedirectRuleStub> redirectRules = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public RedirectRuleListStub()
    {
        this(new ArrayList<RedirectRuleStub>());
    }
    public RedirectRuleListStub(List<RedirectRuleStub> redirectRules)
    {
        this(redirectRules, null);
    }
    public RedirectRuleListStub(List<RedirectRuleStub> redirectRules, String forwardCursor)
    {
        this.redirectRules = redirectRules;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(redirectRules == null) {
            return true;
        } else {
            return redirectRules.isEmpty();
        }
    }
    public int getSize()
    {
        if(redirectRules == null) {
            return 0;
        } else {
            return redirectRules.size();
        }
    }


    @XmlElement(name = "redirectRule")
    public List<RedirectRuleStub> getRedirectRule()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RedirectRuleStub> getList()
    {
        return redirectRules;
    }
    public void setList(List<RedirectRuleStub> redirectRules)
    {
        this.redirectRules = redirectRules;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<RedirectRuleStub> it = this.redirectRules.iterator();
        while(it.hasNext()) {
            RedirectRuleStub redirectRule = it.next();
            sb.append(redirectRule.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RedirectRuleListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RedirectRuleListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RedirectRuleListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RedirectRuleListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RedirectRuleListStub object as a string.", e);
        }
        
        return null;
    }
    public static RedirectRuleListStub fromJsonString(String jsonStr)
    {
        try {
            RedirectRuleListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RedirectRuleListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RedirectRuleListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RedirectRuleListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RedirectRuleListStub object.", e);
        }
        
        return null;
    }

}
