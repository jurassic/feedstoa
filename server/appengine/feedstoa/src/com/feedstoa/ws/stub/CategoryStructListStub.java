package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "categoryStructs")
@XmlType(propOrder = {"categoryStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CategoryStructListStub.class.getName());

    private List<CategoryStructStub> categoryStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public CategoryStructListStub()
    {
        this(new ArrayList<CategoryStructStub>());
    }
    public CategoryStructListStub(List<CategoryStructStub> categoryStructs)
    {
        this(categoryStructs, null);
    }
    public CategoryStructListStub(List<CategoryStructStub> categoryStructs, String forwardCursor)
    {
        this.categoryStructs = categoryStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(categoryStructs == null) {
            return true;
        } else {
            return categoryStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(categoryStructs == null) {
            return 0;
        } else {
            return categoryStructs.size();
        }
    }


    @XmlElement(name = "categoryStruct")
    public List<CategoryStructStub> getCategoryStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CategoryStructStub> getList()
    {
        return categoryStructs;
    }
    public void setList(List<CategoryStructStub> categoryStructs)
    {
        this.categoryStructs = categoryStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<CategoryStructStub> it = this.categoryStructs.iterator();
        while(it.hasNext()) {
            CategoryStructStub categoryStruct = it.next();
            sb.append(categoryStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CategoryStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of CategoryStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CategoryStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CategoryStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CategoryStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static CategoryStructListStub fromJsonString(String jsonStr)
    {
        try {
            CategoryStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CategoryStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CategoryStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CategoryStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CategoryStructListStub object.", e);
        }
        
        return null;
    }

}
