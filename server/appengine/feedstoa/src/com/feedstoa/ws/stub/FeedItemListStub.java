package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "feedItems")
@XmlType(propOrder = {"feedItem", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedItemListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedItemListStub.class.getName());

    private List<FeedItemStub> feedItems = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public FeedItemListStub()
    {
        this(new ArrayList<FeedItemStub>());
    }
    public FeedItemListStub(List<FeedItemStub> feedItems)
    {
        this(feedItems, null);
    }
    public FeedItemListStub(List<FeedItemStub> feedItems, String forwardCursor)
    {
        this.feedItems = feedItems;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(feedItems == null) {
            return true;
        } else {
            return feedItems.isEmpty();
        }
    }
    public int getSize()
    {
        if(feedItems == null) {
            return 0;
        } else {
            return feedItems.size();
        }
    }


    @XmlElement(name = "feedItem")
    public List<FeedItemStub> getFeedItem()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<FeedItemStub> getList()
    {
        return feedItems;
    }
    public void setList(List<FeedItemStub> feedItems)
    {
        this.feedItems = feedItems;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<FeedItemStub> it = this.feedItems.iterator();
        while(it.hasNext()) {
            FeedItemStub feedItem = it.next();
            sb.append(feedItem.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FeedItemListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FeedItemListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FeedItemListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FeedItemListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FeedItemListStub object as a string.", e);
        }
        
        return null;
    }
    public static FeedItemListStub fromJsonString(String jsonStr)
    {
        try {
            FeedItemListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FeedItemListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedItemListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedItemListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedItemListStub object.", e);
        }
        
        return null;
    }

}
