package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "rssItems")
@XmlType(propOrder = {"rssItem", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RssItemListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssItemListStub.class.getName());

    private List<RssItemStub> rssItems = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public RssItemListStub()
    {
        this(new ArrayList<RssItemStub>());
    }
    public RssItemListStub(List<RssItemStub> rssItems)
    {
        this(rssItems, null);
    }
    public RssItemListStub(List<RssItemStub> rssItems, String forwardCursor)
    {
        this.rssItems = rssItems;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(rssItems == null) {
            return true;
        } else {
            return rssItems.isEmpty();
        }
    }
    public int getSize()
    {
        if(rssItems == null) {
            return 0;
        } else {
            return rssItems.size();
        }
    }


    @XmlElement(name = "rssItem")
    public List<RssItemStub> getRssItem()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RssItemStub> getList()
    {
        return rssItems;
    }
    public void setList(List<RssItemStub> rssItems)
    {
        this.rssItems = rssItems;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<RssItemStub> it = this.rssItems.iterator();
        while(it.hasNext()) {
            RssItemStub rssItem = it.next();
            sb.append(rssItem.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RssItemListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RssItemListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RssItemListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RssItemListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RssItemListStub object as a string.", e);
        }
        
        return null;
    }
    public static RssItemListStub fromJsonString(String jsonStr)
    {
        try {
            RssItemListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RssItemListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RssItemListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RssItemListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RssItemListStub object.", e);
        }
        
        return null;
    }

}
