package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "rssItem")
@XmlType(propOrder = {"title", "link", "description", "author", "categoryStub", "comments", "enclosureStub", "guid", "pubDate", "sourceStub"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RssItemStub implements RssItem, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssItemStub.class.getName());

    private String title;
    private String link;
    private String description;
    private String author;
    private List<CategoryStructStub> category;
    private String comments;
    private EnclosureStructStub enclosure;
    private String guid;
    private String pubDate;
    private UriStructStub source;

    public RssItemStub()
    {
        this(null);
    }
    public RssItemStub(RssItem bean)
    {
        if(bean != null) {
            this.title = bean.getTitle();
            this.link = bean.getLink();
            this.description = bean.getDescription();
            this.author = bean.getAuthor();
            List<CategoryStructStub> _categoryStubs = null;
            if(bean.getCategory() != null) {
                _categoryStubs = new ArrayList<CategoryStructStub>();
                for(CategoryStruct b : bean.getCategory()) {
                    _categoryStubs.add(CategoryStructStub.convertBeanToStub(b));
                }
            }
            this.category = _categoryStubs;
            this.comments = bean.getComments();
            this.enclosure = EnclosureStructStub.convertBeanToStub(bean.getEnclosure());
            this.guid = bean.getGuid();
            this.pubDate = bean.getPubDate();
            this.source = UriStructStub.convertBeanToStub(bean.getSource());
        }
    }


    @XmlElement
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlElement
    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getAuthor()
    {
        return this.author;
    }
    public void setAuthor(String author)
    {
        this.author = author;
    }

    @XmlElement(name = "category")
    @JsonIgnore
    public List<CategoryStructStub> getCategoryStub()
    {
        return this.category;
    }
    public void setCategoryStub(List<CategoryStructStub> category)
    {
        this.category = category;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=CategoryStructStub.class)
    public List<CategoryStruct> getCategory()
    {  
        return (List<CategoryStruct>) ((List<?>) getCategoryStub());
    }
    public void setCategory(List<CategoryStruct> category)
    {
        // TBD
        //if(category instanceof List<CategoryStructStub>) {
            setCategoryStub((List<CategoryStructStub>) ((List<?>) category));
        //} else {
        //    // TBD
        //    List<CategoryStructStub> _CategoryStructList = new ArrayList<CategoryStructStub>(); // ???
        //    for(CategoryStruct b : category) {
        //        _CategoryStructList.add(CategoryStructStub.convertBeanToStub(category));
        //    }
        //    setCategoryStub(_CategoryStructList);
        //}
    }

    @XmlElement
    public String getComments()
    {
        return this.comments;
    }
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    @XmlElement(name = "enclosure")
    @JsonIgnore
    public EnclosureStructStub getEnclosureStub()
    {
        return this.enclosure;
    }
    public void setEnclosureStub(EnclosureStructStub enclosure)
    {
        this.enclosure = enclosure;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=EnclosureStructStub.class)
    public EnclosureStruct getEnclosure()
    {  
        return getEnclosureStub();
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if((enclosure == null) || (enclosure instanceof EnclosureStructStub)) {
            setEnclosureStub((EnclosureStructStub) enclosure);
        } else {
            // TBD
            setEnclosureStub(EnclosureStructStub.convertBeanToStub(enclosure));
        }
    }

    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    @XmlElement(name = "source")
    @JsonIgnore
    public UriStructStub getSourceStub()
    {
        return this.source;
    }
    public void setSourceStub(UriStructStub source)
    {
        this.source = source;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=UriStructStub.class)
    public UriStruct getSource()
    {  
        return getSourceStub();
    }
    public void setSource(UriStruct source)
    {
        if((source == null) || (source instanceof UriStructStub)) {
            setSourceStub((UriStructStub) source);
        } else {
            // TBD
            setSourceStub(UriStructStub.convertBeanToStub(source));
        }
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAuthor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getComments() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEnclosure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGuid() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("title", this.title);
        dataMap.put("link", this.link);
        dataMap.put("description", this.description);
        dataMap.put("author", this.author);
        dataMap.put("category", this.category);
        dataMap.put("comments", this.comments);
        dataMap.put("enclosure", this.enclosure);
        dataMap.put("guid", this.guid);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("source", this.source);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = author == null ? 0 : author.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = comments == null ? 0 : comments.hashCode();
        _hash = 31 * _hash + delta;
        delta = enclosure == null ? 0 : enclosure.hashCode();
        _hash = 31 * _hash + delta;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static RssItemStub convertBeanToStub(RssItem bean)
    {
        RssItemStub stub = null;
        if(bean instanceof RssItemStub) {
            stub = (RssItemStub) bean;
        } else {
            if(bean != null) {
                stub = new RssItemStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RssItemStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of RssItemStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RssItemStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RssItemStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RssItemStub object as a string.", e);
        }
        
        return null;
    }
    public static RssItemStub fromJsonString(String jsonStr)
    {
        try {
            RssItemStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RssItemStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RssItemStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RssItemStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RssItemStub object.", e);
        }
        
        return null;
    }

}
