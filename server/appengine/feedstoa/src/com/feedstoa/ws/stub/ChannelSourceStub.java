package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "channelSource")
@XmlType(propOrder = {"guid", "user", "serviceName", "serviceUrl", "feedUrl", "feedFormat", "channelTitle", "channelDescription", "channelCategory", "channelUrl", "channelLogo", "status", "note", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelSourceStub implements ChannelSource, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelSourceStub.class.getName());

    private String guid;
    private String user;
    private String serviceName;
    private String serviceUrl;
    private String feedUrl;
    private String feedFormat;
    private String channelTitle;
    private String channelDescription;
    private String channelCategory;
    private String channelUrl;
    private String channelLogo;
    private String status;
    private String note;
    private Long createdTime;
    private Long modifiedTime;

    public ChannelSourceStub()
    {
        this(null);
    }
    public ChannelSourceStub(ChannelSource bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.serviceName = bean.getServiceName();
            this.serviceUrl = bean.getServiceUrl();
            this.feedUrl = bean.getFeedUrl();
            this.feedFormat = bean.getFeedFormat();
            this.channelTitle = bean.getChannelTitle();
            this.channelDescription = bean.getChannelDescription();
            this.channelCategory = bean.getChannelCategory();
            this.channelUrl = bean.getChannelUrl();
            this.channelLogo = bean.getChannelLogo();
            this.status = bean.getStatus();
            this.note = bean.getNote();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    @XmlElement
    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    @XmlElement
    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    @XmlElement
    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    @XmlElement
    public String getChannelTitle()
    {
        return this.channelTitle;
    }
    public void setChannelTitle(String channelTitle)
    {
        this.channelTitle = channelTitle;
    }

    @XmlElement
    public String getChannelDescription()
    {
        return this.channelDescription;
    }
    public void setChannelDescription(String channelDescription)
    {
        this.channelDescription = channelDescription;
    }

    @XmlElement
    public String getChannelCategory()
    {
        return this.channelCategory;
    }
    public void setChannelCategory(String channelCategory)
    {
        this.channelCategory = channelCategory;
    }

    @XmlElement
    public String getChannelUrl()
    {
        return this.channelUrl;
    }
    public void setChannelUrl(String channelUrl)
    {
        this.channelUrl = channelUrl;
    }

    @XmlElement
    public String getChannelLogo()
    {
        return this.channelLogo;
    }
    public void setChannelLogo(String channelLogo)
    {
        this.channelLogo = channelLogo;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("serviceName", this.serviceName);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("channelTitle", this.channelTitle);
        dataMap.put("channelDescription", this.channelDescription);
        dataMap.put("channelCategory", this.channelCategory);
        dataMap.put("channelUrl", this.channelUrl);
        dataMap.put("channelLogo", this.channelLogo);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelTitle == null ? 0 : channelTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelDescription == null ? 0 : channelDescription.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelCategory == null ? 0 : channelCategory.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelUrl == null ? 0 : channelUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelLogo == null ? 0 : channelLogo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ChannelSourceStub convertBeanToStub(ChannelSource bean)
    {
        ChannelSourceStub stub = null;
        if(bean instanceof ChannelSourceStub) {
            stub = (ChannelSourceStub) bean;
        } else {
            if(bean != null) {
                stub = new ChannelSourceStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ChannelSourceStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of ChannelSourceStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ChannelSourceStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ChannelSourceStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ChannelSourceStub object as a string.", e);
        }
        
        return null;
    }
    public static ChannelSourceStub fromJsonString(String jsonStr)
    {
        try {
            ChannelSourceStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ChannelSourceStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelSourceStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelSourceStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelSourceStub object.", e);
        }
        
        return null;
    }

}
