package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "fetchRequest")
@XmlType(propOrder = {"guid", "managerApp", "appAcl", "gaeAppStub", "ownerUser", "userAcl", "user", "title", "description", "fetchUrl", "feedUrl", "channelFeed", "reuseChannel", "maxItemCount", "note", "status", "originFetch", "outputFormat", "fetchStatus", "result", "feedCategory", "multipleFeedEnabled", "deferred", "alert", "notificationPrefStub", "referrerInfoStub", "refreshInterval", "refreshExpressions", "refreshTimeZone", "currentRefreshCount", "maxRefreshCount", "refreshExpirationTime", "nextRefreshTime", "lastUpdatedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchRequestStub extends FetchBaseStub implements FetchRequest, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestStub.class.getName());

    private String originFetch;
    private String outputFormat;
    private Integer fetchStatus;
    private String result;
    private String feedCategory;
    private Boolean multipleFeedEnabled;
    private Boolean deferred;
    private Boolean alert;
    private NotificationStructStub notificationPref;
    private ReferrerInfoStructStub referrerInfo;
    private Integer refreshInterval;
    private List<String> refreshExpressions;
    private String refreshTimeZone;
    private Integer currentRefreshCount;
    private Integer maxRefreshCount;
    private Long refreshExpirationTime;
    private Long nextRefreshTime;
    private Long lastUpdatedTime;

    public FetchRequestStub()
    {
        this(null);
    }
    public FetchRequestStub(FetchRequest bean)
    {
        super(bean);
        if(bean != null) {
            this.originFetch = bean.getOriginFetch();
            this.outputFormat = bean.getOutputFormat();
            this.fetchStatus = bean.getFetchStatus();
            this.result = bean.getResult();
            this.feedCategory = bean.getFeedCategory();
            this.multipleFeedEnabled = bean.isMultipleFeedEnabled();
            this.deferred = bean.isDeferred();
            this.alert = bean.isAlert();
            this.notificationPref = NotificationStructStub.convertBeanToStub(bean.getNotificationPref());
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.refreshInterval = bean.getRefreshInterval();
            this.refreshExpressions = bean.getRefreshExpressions();
            this.refreshTimeZone = bean.getRefreshTimeZone();
            this.currentRefreshCount = bean.getCurrentRefreshCount();
            this.maxRefreshCount = bean.getMaxRefreshCount();
            this.refreshExpirationTime = bean.getRefreshExpirationTime();
            this.nextRefreshTime = bean.getNextRefreshTime();
            this.lastUpdatedTime = bean.getLastUpdatedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getManagerApp()
    {
        return super.getManagerApp();
    }
    public void setManagerApp(String managerApp)
    {
        super.setManagerApp(managerApp);
    }

    @XmlElement
    public Long getAppAcl()
    {
        return super.getAppAcl();
    }
    public void setAppAcl(Long appAcl)
    {
        super.setAppAcl(appAcl);
    }

    @XmlElement(name = "gaeApp")
    @JsonIgnore
    public GaeAppStructStub getGaeAppStub()
    {
        return super.getGaeAppStub();
    }
    public void setGaeAppStub(GaeAppStructStub gaeApp)
    {
        super.setGaeAppStub(gaeApp);
    }
    @XmlTransient
    @JsonDeserialize(as=GaeAppStructStub.class)
    public GaeAppStruct getGaeApp()
    {  
        return super.getGaeApp();
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        super.setGaeApp(gaeApp);
    }

    @XmlElement
    public String getOwnerUser()
    {
        return super.getOwnerUser();
    }
    public void setOwnerUser(String ownerUser)
    {
        super.setOwnerUser(ownerUser);
    }

    @XmlElement
    public Long getUserAcl()
    {
        return super.getUserAcl();
    }
    public void setUserAcl(Long userAcl)
    {
        super.setUserAcl(userAcl);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public String getFetchUrl()
    {
        return super.getFetchUrl();
    }
    public void setFetchUrl(String fetchUrl)
    {
        super.setFetchUrl(fetchUrl);
    }

    @XmlElement
    public String getFeedUrl()
    {
        return super.getFeedUrl();
    }
    public void setFeedUrl(String feedUrl)
    {
        super.setFeedUrl(feedUrl);
    }

    @XmlElement
    public String getChannelFeed()
    {
        return super.getChannelFeed();
    }
    public void setChannelFeed(String channelFeed)
    {
        super.setChannelFeed(channelFeed);
    }

    @XmlElement
    public Boolean isReuseChannel()
    {
        return super.isReuseChannel();
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        super.setReuseChannel(reuseChannel);
    }

    @XmlElement
    public Integer getMaxItemCount()
    {
        return super.getMaxItemCount();
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        super.setMaxItemCount(maxItemCount);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public String getOriginFetch()
    {
        return this.originFetch;
    }
    public void setOriginFetch(String originFetch)
    {
        this.originFetch = originFetch;
    }

    @XmlElement
    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    @XmlElement
    public Integer getFetchStatus()
    {
        return this.fetchStatus;
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        this.fetchStatus = fetchStatus;
    }

    @XmlElement
    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    @XmlElement
    public String getFeedCategory()
    {
        return this.feedCategory;
    }
    public void setFeedCategory(String feedCategory)
    {
        this.feedCategory = feedCategory;
    }

    @XmlElement
    public Boolean isMultipleFeedEnabled()
    {
        return this.multipleFeedEnabled;
    }
    public void setMultipleFeedEnabled(Boolean multipleFeedEnabled)
    {
        this.multipleFeedEnabled = multipleFeedEnabled;
    }

    @XmlElement
    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    @XmlElement
    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    @XmlElement(name = "notificationPref")
    @JsonIgnore
    public NotificationStructStub getNotificationPrefStub()
    {
        return this.notificationPref;
    }
    public void setNotificationPrefStub(NotificationStructStub notificationPref)
    {
        this.notificationPref = notificationPref;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=NotificationStructStub.class)
    public NotificationStruct getNotificationPref()
    {  
        return getNotificationPrefStub();
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if((notificationPref == null) || (notificationPref instanceof NotificationStructStub)) {
            setNotificationPrefStub((NotificationStructStub) notificationPref);
        } else {
            // TBD
            setNotificationPrefStub(NotificationStructStub.convertBeanToStub(notificationPref));
        }
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public Integer getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    @XmlElement
    public List<String> getRefreshExpressions()
    {
        return this.refreshExpressions;
    }
    public void setRefreshExpressions(List<String> refreshExpressions)
    {
        this.refreshExpressions = refreshExpressions;
    }

    @XmlElement
    public String getRefreshTimeZone()
    {
        return this.refreshTimeZone;
    }
    public void setRefreshTimeZone(String refreshTimeZone)
    {
        this.refreshTimeZone = refreshTimeZone;
    }

    @XmlElement
    public Integer getCurrentRefreshCount()
    {
        return this.currentRefreshCount;
    }
    public void setCurrentRefreshCount(Integer currentRefreshCount)
    {
        this.currentRefreshCount = currentRefreshCount;
    }

    @XmlElement
    public Integer getMaxRefreshCount()
    {
        return this.maxRefreshCount;
    }
    public void setMaxRefreshCount(Integer maxRefreshCount)
    {
        this.maxRefreshCount = maxRefreshCount;
    }

    @XmlElement
    public Long getRefreshExpirationTime()
    {
        return this.refreshExpirationTime;
    }
    public void setRefreshExpirationTime(Long refreshExpirationTime)
    {
        this.refreshExpirationTime = refreshExpirationTime;
    }

    @XmlElement
    public Long getNextRefreshTime()
    {
        return this.nextRefreshTime;
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        this.nextRefreshTime = nextRefreshTime;
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("originFetch", this.originFetch);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("fetchStatus", this.fetchStatus);
        dataMap.put("result", this.result);
        dataMap.put("feedCategory", this.feedCategory);
        dataMap.put("multipleFeedEnabled", this.multipleFeedEnabled);
        dataMap.put("deferred", this.deferred);
        dataMap.put("alert", this.alert);
        dataMap.put("notificationPref", this.notificationPref);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("refreshInterval", this.refreshInterval);
        dataMap.put("refreshExpressions", this.refreshExpressions);
        dataMap.put("refreshTimeZone", this.refreshTimeZone);
        dataMap.put("currentRefreshCount", this.currentRefreshCount);
        dataMap.put("maxRefreshCount", this.maxRefreshCount);
        dataMap.put("refreshExpirationTime", this.refreshExpirationTime);
        dataMap.put("nextRefreshTime", this.nextRefreshTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = originFetch == null ? 0 : originFetch.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchStatus == null ? 0 : fetchStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedCategory == null ? 0 : feedCategory.hashCode();
        _hash = 31 * _hash + delta;
        delta = multipleFeedEnabled == null ? 0 : multipleFeedEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = deferred == null ? 0 : deferred.hashCode();
        _hash = 31 * _hash + delta;
        delta = alert == null ? 0 : alert.hashCode();
        _hash = 31 * _hash + delta;
        delta = notificationPref == null ? 0 : notificationPref.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshExpressions == null ? 0 : refreshExpressions.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshTimeZone == null ? 0 : refreshTimeZone.hashCode();
        _hash = 31 * _hash + delta;
        delta = currentRefreshCount == null ? 0 : currentRefreshCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxRefreshCount == null ? 0 : maxRefreshCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshExpirationTime == null ? 0 : refreshExpirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextRefreshTime == null ? 0 : nextRefreshTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static FetchRequestStub convertBeanToStub(FetchRequest bean)
    {
        FetchRequestStub stub = null;
        if(bean instanceof FetchRequestStub) {
            stub = (FetchRequestStub) bean;
        } else {
            if(bean != null) {
                stub = new FetchRequestStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FetchRequestStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of FetchRequestStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestStub object as a string.", e);
        }
        
        return null;
    }
    public static FetchRequestStub fromJsonString(String jsonStr)
    {
        try {
            FetchRequestStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FetchRequestStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestStub object.", e);
        }
        
        return null;
    }

}
