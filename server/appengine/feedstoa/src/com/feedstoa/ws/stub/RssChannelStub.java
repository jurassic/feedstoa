package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "rssChannel")
@XmlType(propOrder = {"title", "link", "description", "language", "copyright", "managingEditor", "webMaster", "pubDate", "lastBuildDate", "categoryStub", "generator", "docs", "cloudStub", "ttl", "imageStub", "rating", "textInputStub", "skipHours", "skipDays", "itemStub"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RssChannelStub implements RssChannel, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RssChannelStub.class.getName());

    private String title;
    private String link;
    private String description;
    private String language;
    private String copyright;
    private String managingEditor;
    private String webMaster;
    private String pubDate;
    private String lastBuildDate;
    private List<CategoryStructStub> category;
    private String generator;
    private String docs;
    private CloudStructStub cloud;
    private Integer ttl;
    private ImageStructStub image;
    private String rating;
    private TextInputStructStub textInput;
    private List<Integer> skipHours;
    private List<String> skipDays;
    private List<RssItemStub> item;

    public RssChannelStub()
    {
        this(null);
    }
    public RssChannelStub(RssChannel bean)
    {
        if(bean != null) {
            this.title = bean.getTitle();
            this.link = bean.getLink();
            this.description = bean.getDescription();
            this.language = bean.getLanguage();
            this.copyright = bean.getCopyright();
            this.managingEditor = bean.getManagingEditor();
            this.webMaster = bean.getWebMaster();
            this.pubDate = bean.getPubDate();
            this.lastBuildDate = bean.getLastBuildDate();
            List<CategoryStructStub> _categoryStubs = null;
            if(bean.getCategory() != null) {
                _categoryStubs = new ArrayList<CategoryStructStub>();
                for(CategoryStruct b : bean.getCategory()) {
                    _categoryStubs.add(CategoryStructStub.convertBeanToStub(b));
                }
            }
            this.category = _categoryStubs;
            this.generator = bean.getGenerator();
            this.docs = bean.getDocs();
            this.cloud = CloudStructStub.convertBeanToStub(bean.getCloud());
            this.ttl = bean.getTtl();
            this.image = ImageStructStub.convertBeanToStub(bean.getImage());
            this.rating = bean.getRating();
            this.textInput = TextInputStructStub.convertBeanToStub(bean.getTextInput());
            this.skipHours = bean.getSkipHours();
            this.skipDays = bean.getSkipDays();
            List<RssItemStub> _itemStubs = null;
            if(bean.getItem() != null) {
                _itemStubs = new ArrayList<RssItemStub>();
                for(RssItem b : bean.getItem()) {
                    _itemStubs.add(RssItemStub.convertBeanToStub(b));
                }
            }
            this.item = _itemStubs;
        }
    }


    @XmlElement
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlElement
    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
    }

    @XmlElement
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlElement
    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    @XmlElement
    public String getCopyright()
    {
        return this.copyright;
    }
    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    @XmlElement
    public String getManagingEditor()
    {
        return this.managingEditor;
    }
    public void setManagingEditor(String managingEditor)
    {
        this.managingEditor = managingEditor;
    }

    @XmlElement
    public String getWebMaster()
    {
        return this.webMaster;
    }
    public void setWebMaster(String webMaster)
    {
        this.webMaster = webMaster;
    }

    @XmlElement
    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    @XmlElement
    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    @XmlElement(name = "category")
    @JsonIgnore
    public List<CategoryStructStub> getCategoryStub()
    {
        return this.category;
    }
    public void setCategoryStub(List<CategoryStructStub> category)
    {
        this.category = category;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=CategoryStructStub.class)
    public List<CategoryStruct> getCategory()
    {  
        return (List<CategoryStruct>) ((List<?>) getCategoryStub());
    }
    public void setCategory(List<CategoryStruct> category)
    {
        // TBD
        //if(category instanceof List<CategoryStructStub>) {
            setCategoryStub((List<CategoryStructStub>) ((List<?>) category));
        //} else {
        //    // TBD
        //    List<CategoryStructStub> _CategoryStructList = new ArrayList<CategoryStructStub>(); // ???
        //    for(CategoryStruct b : category) {
        //        _CategoryStructList.add(CategoryStructStub.convertBeanToStub(category));
        //    }
        //    setCategoryStub(_CategoryStructList);
        //}
    }

    @XmlElement
    public String getGenerator()
    {
        return this.generator;
    }
    public void setGenerator(String generator)
    {
        this.generator = generator;
    }

    @XmlElement
    public String getDocs()
    {
        return this.docs;
    }
    public void setDocs(String docs)
    {
        this.docs = docs;
    }

    @XmlElement(name = "cloud")
    @JsonIgnore
    public CloudStructStub getCloudStub()
    {
        return this.cloud;
    }
    public void setCloudStub(CloudStructStub cloud)
    {
        this.cloud = cloud;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=CloudStructStub.class)
    public CloudStruct getCloud()
    {  
        return getCloudStub();
    }
    public void setCloud(CloudStruct cloud)
    {
        if((cloud == null) || (cloud instanceof CloudStructStub)) {
            setCloudStub((CloudStructStub) cloud);
        } else {
            // TBD
            setCloudStub(CloudStructStub.convertBeanToStub(cloud));
        }
    }

    @XmlElement
    public Integer getTtl()
    {
        return this.ttl;
    }
    public void setTtl(Integer ttl)
    {
        this.ttl = ttl;
    }

    @XmlElement(name = "image")
    @JsonIgnore
    public ImageStructStub getImageStub()
    {
        return this.image;
    }
    public void setImageStub(ImageStructStub image)
    {
        this.image = image;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ImageStructStub.class)
    public ImageStruct getImage()
    {  
        return getImageStub();
    }
    public void setImage(ImageStruct image)
    {
        if((image == null) || (image instanceof ImageStructStub)) {
            setImageStub((ImageStructStub) image);
        } else {
            // TBD
            setImageStub(ImageStructStub.convertBeanToStub(image));
        }
    }

    @XmlElement
    public String getRating()
    {
        return this.rating;
    }
    public void setRating(String rating)
    {
        this.rating = rating;
    }

    @XmlElement(name = "textInput")
    @JsonIgnore
    public TextInputStructStub getTextInputStub()
    {
        return this.textInput;
    }
    public void setTextInputStub(TextInputStructStub textInput)
    {
        this.textInput = textInput;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TextInputStructStub.class)
    public TextInputStruct getTextInput()
    {  
        return getTextInputStub();
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if((textInput == null) || (textInput instanceof TextInputStructStub)) {
            setTextInputStub((TextInputStructStub) textInput);
        } else {
            // TBD
            setTextInputStub(TextInputStructStub.convertBeanToStub(textInput));
        }
    }

    @XmlElement
    public List<Integer> getSkipHours()
    {
        return this.skipHours;
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        this.skipHours = skipHours;
    }

    @XmlElement
    public List<String> getSkipDays()
    {
        return this.skipDays;
    }
    public void setSkipDays(List<String> skipDays)
    {
        this.skipDays = skipDays;
    }

    @XmlElement(name = "item")
    @JsonIgnore
    public List<RssItemStub> getItemStub()
    {
        return this.item;
    }
    public void setItemStub(List<RssItemStub> item)
    {
        this.item = item;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=RssItemStub.class)
    public List<RssItem> getItem()
    {  
        return (List<RssItem>) ((List<?>) getItemStub());
    }
    public void setItem(List<RssItem> item)
    {
        // TBD
        //if(item instanceof List<RssItemStub>) {
            setItemStub((List<RssItemStub>) ((List<?>) item));
        //} else {
        //    // TBD
        //    List<RssItemStub> _RssItemList = new ArrayList<RssItemStub>(); // ???
        //    for(RssItem b : item) {
        //        _RssItemList.add(RssItemStub.convertBeanToStub(item));
        //    }
        //    setItemStub(_RssItemList);
        //}
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCopyright() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getManagingEditor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebMaster() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastBuildDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGenerator() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDocs() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCloud() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTtl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRating() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTextInput() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipHours() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipDays() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getItem() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("title", this.title);
        dataMap.put("link", this.link);
        dataMap.put("description", this.description);
        dataMap.put("language", this.language);
        dataMap.put("copyright", this.copyright);
        dataMap.put("managingEditor", this.managingEditor);
        dataMap.put("webMaster", this.webMaster);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("lastBuildDate", this.lastBuildDate);
        dataMap.put("category", this.category);
        dataMap.put("generator", this.generator);
        dataMap.put("docs", this.docs);
        dataMap.put("cloud", this.cloud);
        dataMap.put("ttl", this.ttl);
        dataMap.put("image", this.image);
        dataMap.put("rating", this.rating);
        dataMap.put("textInput", this.textInput);
        dataMap.put("skipHours", this.skipHours);
        dataMap.put("skipDays", this.skipDays);
        dataMap.put("item", this.item);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = copyright == null ? 0 : copyright.hashCode();
        _hash = 31 * _hash + delta;
        delta = managingEditor == null ? 0 : managingEditor.hashCode();
        _hash = 31 * _hash + delta;
        delta = webMaster == null ? 0 : webMaster.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = generator == null ? 0 : generator.hashCode();
        _hash = 31 * _hash + delta;
        delta = docs == null ? 0 : docs.hashCode();
        _hash = 31 * _hash + delta;
        delta = cloud == null ? 0 : cloud.hashCode();
        _hash = 31 * _hash + delta;
        delta = ttl == null ? 0 : ttl.hashCode();
        _hash = 31 * _hash + delta;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = rating == null ? 0 : rating.hashCode();
        _hash = 31 * _hash + delta;
        delta = textInput == null ? 0 : textInput.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipHours == null ? 0 : skipHours.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipDays == null ? 0 : skipDays.hashCode();
        _hash = 31 * _hash + delta;
        delta = item == null ? 0 : item.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static RssChannelStub convertBeanToStub(RssChannel bean)
    {
        RssChannelStub stub = null;
        if(bean instanceof RssChannelStub) {
            stub = (RssChannelStub) bean;
        } else {
            if(bean != null) {
                stub = new RssChannelStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RssChannelStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of RssChannelStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RssChannelStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RssChannelStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RssChannelStub object as a string.", e);
        }
        
        return null;
    }
    public static RssChannelStub fromJsonString(String jsonStr)
    {
        try {
            RssChannelStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RssChannelStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RssChannelStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RssChannelStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RssChannelStub object.", e);
        }
        
        return null;
    }

}
