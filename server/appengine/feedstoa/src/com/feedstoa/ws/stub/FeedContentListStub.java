package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "feedContents")
@XmlType(propOrder = {"feedContent", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedContentListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedContentListStub.class.getName());

    private List<FeedContentStub> feedContents = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public FeedContentListStub()
    {
        this(new ArrayList<FeedContentStub>());
    }
    public FeedContentListStub(List<FeedContentStub> feedContents)
    {
        this(feedContents, null);
    }
    public FeedContentListStub(List<FeedContentStub> feedContents, String forwardCursor)
    {
        this.feedContents = feedContents;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(feedContents == null) {
            return true;
        } else {
            return feedContents.isEmpty();
        }
    }
    public int getSize()
    {
        if(feedContents == null) {
            return 0;
        } else {
            return feedContents.size();
        }
    }


    @XmlElement(name = "feedContent")
    public List<FeedContentStub> getFeedContent()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<FeedContentStub> getList()
    {
        return feedContents;
    }
    public void setList(List<FeedContentStub> feedContents)
    {
        this.feedContents = feedContents;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<FeedContentStub> it = this.feedContents.iterator();
        while(it.hasNext()) {
            FeedContentStub feedContent = it.next();
            sb.append(feedContent.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FeedContentListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FeedContentListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FeedContentListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FeedContentListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FeedContentListStub object as a string.", e);
        }
        
        return null;
    }
    public static FeedContentListStub fromJsonString(String jsonStr)
    {
        try {
            FeedContentListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FeedContentListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedContentListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedContentListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FeedContentListStub object.", e);
        }
        
        return null;
    }

}
