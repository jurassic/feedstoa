package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "channelFeeds")
@XmlType(propOrder = {"channelFeed", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelFeedListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelFeedListStub.class.getName());

    private List<ChannelFeedStub> channelFeeds = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ChannelFeedListStub()
    {
        this(new ArrayList<ChannelFeedStub>());
    }
    public ChannelFeedListStub(List<ChannelFeedStub> channelFeeds)
    {
        this(channelFeeds, null);
    }
    public ChannelFeedListStub(List<ChannelFeedStub> channelFeeds, String forwardCursor)
    {
        this.channelFeeds = channelFeeds;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(channelFeeds == null) {
            return true;
        } else {
            return channelFeeds.isEmpty();
        }
    }
    public int getSize()
    {
        if(channelFeeds == null) {
            return 0;
        } else {
            return channelFeeds.size();
        }
    }


    @XmlElement(name = "channelFeed")
    public List<ChannelFeedStub> getChannelFeed()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ChannelFeedStub> getList()
    {
        return channelFeeds;
    }
    public void setList(List<ChannelFeedStub> channelFeeds)
    {
        this.channelFeeds = channelFeeds;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ChannelFeedStub> it = this.channelFeeds.iterator();
        while(it.hasNext()) {
            ChannelFeedStub channelFeed = it.next();
            sb.append(channelFeed.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ChannelFeedListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ChannelFeedListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ChannelFeedListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ChannelFeedListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ChannelFeedListStub object as a string.", e);
        }
        
        return null;
    }
    public static ChannelFeedListStub fromJsonString(String jsonStr)
    {
        try {
            ChannelFeedListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ChannelFeedListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelFeedListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelFeedListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ChannelFeedListStub object.", e);
        }
        
        return null;
    }

}
