package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "redirectRule")
@XmlType(propOrder = {"redirectType", "precedence", "sourceDomain", "sourcePath", "targetDomain", "targetPath", "note", "status"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RedirectRuleStub implements RedirectRule, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RedirectRuleStub.class.getName());

    private String redirectType;
    private Double precedence;
    private String sourceDomain;
    private String sourcePath;
    private String targetDomain;
    private String targetPath;
    private String note;
    private String status;

    public RedirectRuleStub()
    {
        this(null);
    }
    public RedirectRuleStub(RedirectRule bean)
    {
        if(bean != null) {
            this.redirectType = bean.getRedirectType();
            this.precedence = bean.getPrecedence();
            this.sourceDomain = bean.getSourceDomain();
            this.sourcePath = bean.getSourcePath();
            this.targetDomain = bean.getTargetDomain();
            this.targetPath = bean.getTargetPath();
            this.note = bean.getNote();
            this.status = bean.getStatus();
        }
    }


    @XmlElement
    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    @XmlElement
    public Double getPrecedence()
    {
        return this.precedence;
    }
    public void setPrecedence(Double precedence)
    {
        this.precedence = precedence;
    }

    @XmlElement
    public String getSourceDomain()
    {
        return this.sourceDomain;
    }
    public void setSourceDomain(String sourceDomain)
    {
        this.sourceDomain = sourceDomain;
    }

    @XmlElement
    public String getSourcePath()
    {
        return this.sourcePath;
    }
    public void setSourcePath(String sourcePath)
    {
        this.sourcePath = sourcePath;
    }

    @XmlElement
    public String getTargetDomain()
    {
        return this.targetDomain;
    }
    public void setTargetDomain(String targetDomain)
    {
        this.targetDomain = targetDomain;
    }

    @XmlElement
    public String getTargetPath()
    {
        return this.targetPath;
    }
    public void setTargetPath(String targetPath)
    {
        this.targetPath = targetPath;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrecedence() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourceDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourcePath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("redirectType", this.redirectType);
        dataMap.put("precedence", this.precedence);
        dataMap.put("sourceDomain", this.sourceDomain);
        dataMap.put("sourcePath", this.sourcePath);
        dataMap.put("targetDomain", this.targetDomain);
        dataMap.put("targetPath", this.targetPath);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = precedence == null ? 0 : precedence.hashCode();
        _hash = 31 * _hash + delta;
        delta = sourceDomain == null ? 0 : sourceDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = sourcePath == null ? 0 : sourcePath.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetDomain == null ? 0 : targetDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetPath == null ? 0 : targetPath.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static RedirectRuleStub convertBeanToStub(RedirectRule bean)
    {
        RedirectRuleStub stub = null;
        if(bean instanceof RedirectRuleStub) {
            stub = (RedirectRuleStub) bean;
        } else {
            if(bean != null) {
                stub = new RedirectRuleStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RedirectRuleStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of RedirectRuleStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RedirectRuleStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RedirectRuleStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RedirectRuleStub object as a string.", e);
        }
        
        return null;
    }
    public static RedirectRuleStub fromJsonString(String jsonStr)
    {
        try {
            RedirectRuleStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RedirectRuleStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RedirectRuleStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RedirectRuleStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RedirectRuleStub object.", e);
        }
        
        return null;
    }

}
