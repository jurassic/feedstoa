package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.SiteLog;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "siteLogs")
@XmlType(propOrder = {"siteLog", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteLogListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SiteLogListStub.class.getName());

    private List<SiteLogStub> siteLogs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public SiteLogListStub()
    {
        this(new ArrayList<SiteLogStub>());
    }
    public SiteLogListStub(List<SiteLogStub> siteLogs)
    {
        this(siteLogs, null);
    }
    public SiteLogListStub(List<SiteLogStub> siteLogs, String forwardCursor)
    {
        this.siteLogs = siteLogs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(siteLogs == null) {
            return true;
        } else {
            return siteLogs.isEmpty();
        }
    }
    public int getSize()
    {
        if(siteLogs == null) {
            return 0;
        } else {
            return siteLogs.size();
        }
    }


    @XmlElement(name = "siteLog")
    public List<SiteLogStub> getSiteLog()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<SiteLogStub> getList()
    {
        return siteLogs;
    }
    public void setList(List<SiteLogStub> siteLogs)
    {
        this.siteLogs = siteLogs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<SiteLogStub> it = this.siteLogs.iterator();
        while(it.hasNext()) {
            SiteLogStub siteLog = it.next();
            sb.append(siteLog.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static SiteLogListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of SiteLogListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write SiteLogListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write SiteLogListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write SiteLogListStub object as a string.", e);
        }
        
        return null;
    }
    public static SiteLogListStub fromJsonString(String jsonStr)
    {
        try {
            SiteLogListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, SiteLogListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into SiteLogListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into SiteLogListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into SiteLogListStub object.", e);
        }
        
        return null;
    }

}
