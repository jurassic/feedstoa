package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "textInputStructs")
@XmlType(propOrder = {"textInputStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextInputStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TextInputStructListStub.class.getName());

    private List<TextInputStructStub> textInputStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TextInputStructListStub()
    {
        this(new ArrayList<TextInputStructStub>());
    }
    public TextInputStructListStub(List<TextInputStructStub> textInputStructs)
    {
        this(textInputStructs, null);
    }
    public TextInputStructListStub(List<TextInputStructStub> textInputStructs, String forwardCursor)
    {
        this.textInputStructs = textInputStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(textInputStructs == null) {
            return true;
        } else {
            return textInputStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(textInputStructs == null) {
            return 0;
        } else {
            return textInputStructs.size();
        }
    }


    @XmlElement(name = "textInputStruct")
    public List<TextInputStructStub> getTextInputStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TextInputStructStub> getList()
    {
        return textInputStructs;
    }
    public void setList(List<TextInputStructStub> textInputStructs)
    {
        this.textInputStructs = textInputStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TextInputStructStub> it = this.textInputStructs.iterator();
        while(it.hasNext()) {
            TextInputStructStub textInputStruct = it.next();
            sb.append(textInputStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TextInputStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TextInputStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TextInputStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TextInputStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TextInputStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static TextInputStructListStub fromJsonString(String jsonStr)
    {
        try {
            TextInputStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TextInputStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TextInputStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TextInputStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TextInputStructListStub object.", e);
        }
        
        return null;
    }

}
