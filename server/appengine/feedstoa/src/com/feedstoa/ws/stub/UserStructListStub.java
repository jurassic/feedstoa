package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "userStructs")
@XmlType(propOrder = {"userStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserStructListStub.class.getName());

    private List<UserStructStub> userStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public UserStructListStub()
    {
        this(new ArrayList<UserStructStub>());
    }
    public UserStructListStub(List<UserStructStub> userStructs)
    {
        this(userStructs, null);
    }
    public UserStructListStub(List<UserStructStub> userStructs, String forwardCursor)
    {
        this.userStructs = userStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(userStructs == null) {
            return true;
        } else {
            return userStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(userStructs == null) {
            return 0;
        } else {
            return userStructs.size();
        }
    }


    @XmlElement(name = "userStruct")
    public List<UserStructStub> getUserStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserStructStub> getList()
    {
        return userStructs;
    }
    public void setList(List<UserStructStub> userStructs)
    {
        this.userStructs = userStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<UserStructStub> it = this.userStructs.iterator();
        while(it.hasNext()) {
            UserStructStub userStruct = it.next();
            sb.append(userStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserStructListStub fromJsonString(String jsonStr)
    {
        try {
            UserStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserStructListStub object.", e);
        }
        
        return null;
    }

}
