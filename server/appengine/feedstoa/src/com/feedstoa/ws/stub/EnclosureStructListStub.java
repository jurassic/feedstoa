package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "enclosureStructs")
@XmlType(propOrder = {"enclosureStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnclosureStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(EnclosureStructListStub.class.getName());

    private List<EnclosureStructStub> enclosureStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public EnclosureStructListStub()
    {
        this(new ArrayList<EnclosureStructStub>());
    }
    public EnclosureStructListStub(List<EnclosureStructStub> enclosureStructs)
    {
        this(enclosureStructs, null);
    }
    public EnclosureStructListStub(List<EnclosureStructStub> enclosureStructs, String forwardCursor)
    {
        this.enclosureStructs = enclosureStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(enclosureStructs == null) {
            return true;
        } else {
            return enclosureStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(enclosureStructs == null) {
            return 0;
        } else {
            return enclosureStructs.size();
        }
    }


    @XmlElement(name = "enclosureStruct")
    public List<EnclosureStructStub> getEnclosureStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<EnclosureStructStub> getList()
    {
        return enclosureStructs;
    }
    public void setList(List<EnclosureStructStub> enclosureStructs)
    {
        this.enclosureStructs = enclosureStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<EnclosureStructStub> it = this.enclosureStructs.iterator();
        while(it.hasNext()) {
            EnclosureStructStub enclosureStruct = it.next();
            sb.append(enclosureStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static EnclosureStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of EnclosureStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write EnclosureStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write EnclosureStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write EnclosureStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static EnclosureStructListStub fromJsonString(String jsonStr)
    {
        try {
            EnclosureStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, EnclosureStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into EnclosureStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into EnclosureStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into EnclosureStructListStub object.", e);
        }
        
        return null;
    }

}
