package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "cloudStruct")
@XmlType(propOrder = {"domain", "port", "path", "registerProcedure", "protocol"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudStructStub implements CloudStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CloudStructStub.class.getName());

    private String domain;
    private Integer port;
    private String path;
    private String registerProcedure;
    private String protocol;

    public CloudStructStub()
    {
        this(null);
    }
    public CloudStructStub(CloudStruct bean)
    {
        if(bean != null) {
            this.domain = bean.getDomain();
            this.port = bean.getPort();
            this.path = bean.getPath();
            this.registerProcedure = bean.getRegisterProcedure();
            this.protocol = bean.getProtocol();
        }
    }


    @XmlElement
    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    @XmlElement
    public Integer getPort()
    {
        return this.port;
    }
    public void setPort(Integer port)
    {
        this.port = port;
    }

    @XmlElement
    public String getPath()
    {
        return this.path;
    }
    public void setPath(String path)
    {
        this.path = path;
    }

    @XmlElement
    public String getRegisterProcedure()
    {
        return this.registerProcedure;
    }
    public void setRegisterProcedure(String registerProcedure)
    {
        this.registerProcedure = registerProcedure;
    }

    @XmlElement
    public String getProtocol()
    {
        return this.protocol;
    }
    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPort() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegisterProcedure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProtocol() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("domain", this.domain);
        dataMap.put("port", this.port);
        dataMap.put("path", this.path);
        dataMap.put("registerProcedure", this.registerProcedure);
        dataMap.put("protocol", this.protocol);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = port == null ? 0 : port.hashCode();
        _hash = 31 * _hash + delta;
        delta = path == null ? 0 : path.hashCode();
        _hash = 31 * _hash + delta;
        delta = registerProcedure == null ? 0 : registerProcedure.hashCode();
        _hash = 31 * _hash + delta;
        delta = protocol == null ? 0 : protocol.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static CloudStructStub convertBeanToStub(CloudStruct bean)
    {
        CloudStructStub stub = null;
        if(bean instanceof CloudStructStub) {
            stub = (CloudStructStub) bean;
        } else {
            if(bean != null) {
                stub = new CloudStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CloudStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of CloudStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CloudStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CloudStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CloudStructStub object as a string.", e);
        }
        
        return null;
    }
    public static CloudStructStub fromJsonString(String jsonStr)
    {
        try {
            CloudStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CloudStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CloudStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CloudStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CloudStructStub object.", e);
        }
        
        return null;
    }

}
