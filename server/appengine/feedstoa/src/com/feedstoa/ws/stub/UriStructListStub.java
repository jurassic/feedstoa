package com.feedstoa.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.util.JsonUtil;


@XmlRootElement(name = "uriStructs")
@XmlType(propOrder = {"uriStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UriStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UriStructListStub.class.getName());

    private List<UriStructStub> uriStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public UriStructListStub()
    {
        this(new ArrayList<UriStructStub>());
    }
    public UriStructListStub(List<UriStructStub> uriStructs)
    {
        this(uriStructs, null);
    }
    public UriStructListStub(List<UriStructStub> uriStructs, String forwardCursor)
    {
        this.uriStructs = uriStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(uriStructs == null) {
            return true;
        } else {
            return uriStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(uriStructs == null) {
            return 0;
        } else {
            return uriStructs.size();
        }
    }


    @XmlElement(name = "uriStruct")
    public List<UriStructStub> getUriStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UriStructStub> getList()
    {
        return uriStructs;
    }
    public void setList(List<UriStructStub> uriStructs)
    {
        this.uriStructs = uriStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<UriStructStub> it = this.uriStructs.iterator();
        while(it.hasNext()) {
            UriStructStub uriStruct = it.next();
            sb.append(uriStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UriStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UriStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UriStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UriStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UriStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static UriStructListStub fromJsonString(String jsonStr)
    {
        try {
            UriStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UriStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UriStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UriStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UriStructListStub object.", e);
        }
        
        return null;
    }

}
