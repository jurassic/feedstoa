package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.bean.TextInputStructBean;
import com.feedstoa.ws.stub.TextInputStructStub;


public class TextInputStructResourceUtil
{
    private static final Logger log = Logger.getLogger(TextInputStructResourceUtil.class.getName());

    // Static methods only.
    private TextInputStructResourceUtil() {}

    public static TextInputStructBean convertTextInputStructStubToBean(TextInputStruct stub)
    {
        TextInputStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null TextInputStructBean is returned.");
        } else {
            bean = new TextInputStructBean();
            bean.setTitle(stub.getTitle());
            bean.setName(stub.getName());
            bean.setLink(stub.getLink());
            bean.setDescription(stub.getDescription());
        }
        return bean;
    }

}
