package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.SiteLog;
import com.feedstoa.ws.bean.SiteLogBean;
import com.feedstoa.ws.stub.SiteLogStub;


public class SiteLogResourceUtil
{
    private static final Logger log = Logger.getLogger(SiteLogResourceUtil.class.getName());

    // Static methods only.
    private SiteLogResourceUtil() {}

    public static SiteLogBean convertSiteLogStubToBean(SiteLog stub)
    {
        SiteLogBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null SiteLogBean is returned.");
        } else {
            bean = new SiteLogBean();
            bean.setUuid(stub.getUuid());
            bean.setTitle(stub.getTitle());
            bean.setPubDate(stub.getPubDate());
            bean.setTag(stub.getTag());
            bean.setContent(stub.getContent());
            bean.setFormat(stub.getFormat());
            bean.setNote(stub.getNote());
            bean.setStatus(stub.getStatus());
        }
        return bean;
    }

}
