package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.bean.UserStructBean;
import com.feedstoa.ws.stub.UserStructStub;


public class UserStructResourceUtil
{
    private static final Logger log = Logger.getLogger(UserStructResourceUtil.class.getName());

    // Static methods only.
    private UserStructResourceUtil() {}

    public static UserStructBean convertUserStructStubToBean(UserStruct stub)
    {
        UserStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null UserStructBean is returned.");
        } else {
            bean = new UserStructBean();
            bean.setUuid(stub.getUuid());
            bean.setName(stub.getName());
            bean.setEmail(stub.getEmail());
            bean.setUrl(stub.getUrl());
        }
        return bean;
    }

}
