package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.CellLatitudeLongitude;
import com.feedstoa.ws.bean.CellLatitudeLongitudeBean;
import com.feedstoa.ws.stub.CellLatitudeLongitudeStub;


public class CellLatitudeLongitudeResourceUtil
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeResourceUtil.class.getName());

    // Static methods only.
    private CellLatitudeLongitudeResourceUtil() {}

    public static CellLatitudeLongitudeBean convertCellLatitudeLongitudeStubToBean(CellLatitudeLongitude stub)
    {
        CellLatitudeLongitudeBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null CellLatitudeLongitudeBean is returned.");
        } else {
            bean = new CellLatitudeLongitudeBean();
            bean.setScale(stub.getScale());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
        }
        return bean;
    }

}
