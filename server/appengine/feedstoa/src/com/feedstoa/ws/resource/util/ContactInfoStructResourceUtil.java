package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.ContactInfoStruct;
import com.feedstoa.ws.bean.ContactInfoStructBean;
import com.feedstoa.ws.stub.ContactInfoStructStub;


public class ContactInfoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ContactInfoStructResourceUtil.class.getName());

    // Static methods only.
    private ContactInfoStructResourceUtil() {}

    public static ContactInfoStructBean convertContactInfoStructStubToBean(ContactInfoStruct stub)
    {
        ContactInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ContactInfoStructBean is returned.");
        } else {
            bean = new ContactInfoStructBean();
            bean.setUuid(stub.getUuid());
            bean.setStreetAddress(stub.getStreetAddress());
            bean.setLocality(stub.getLocality());
            bean.setRegion(stub.getRegion());
            bean.setPostalCode(stub.getPostalCode());
            bean.setCountryName(stub.getCountryName());
            bean.setEmailAddress(stub.getEmailAddress());
            bean.setPhoneNumber(stub.getPhoneNumber());
            bean.setFaxNumber(stub.getFaxNumber());
            bean.setWebsite(stub.getWebsite());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
