package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.bean.EnclosureStructBean;
import com.feedstoa.ws.stub.EnclosureStructStub;


public class EnclosureStructResourceUtil
{
    private static final Logger log = Logger.getLogger(EnclosureStructResourceUtil.class.getName());

    // Static methods only.
    private EnclosureStructResourceUtil() {}

    public static EnclosureStructBean convertEnclosureStructStubToBean(EnclosureStruct stub)
    {
        EnclosureStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null EnclosureStructBean is returned.");
        } else {
            bean = new EnclosureStructBean();
            bean.setUrl(stub.getUrl());
            bean.setLength(stub.getLength());
            bean.setType(stub.getType());
        }
        return bean;
    }

}
