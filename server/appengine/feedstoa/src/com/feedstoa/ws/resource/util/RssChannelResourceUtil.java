package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.bean.RssChannelBean;
import com.feedstoa.ws.stub.RssChannelStub;


public class RssChannelResourceUtil
{
    private static final Logger log = Logger.getLogger(RssChannelResourceUtil.class.getName());

    // Static methods only.
    private RssChannelResourceUtil() {}

    public static RssChannelBean convertRssChannelStubToBean(RssChannel stub)
    {
        RssChannelBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null RssChannelBean is returned.");
        } else {
            bean = new RssChannelBean();
            bean.setTitle(stub.getTitle());
            bean.setLink(stub.getLink());
            bean.setDescription(stub.getDescription());
            bean.setLanguage(stub.getLanguage());
            bean.setCopyright(stub.getCopyright());
            bean.setManagingEditor(stub.getManagingEditor());
            bean.setWebMaster(stub.getWebMaster());
            bean.setPubDate(stub.getPubDate());
            bean.setLastBuildDate(stub.getLastBuildDate());
            bean.setCategory(stub.getCategory());
            bean.setGenerator(stub.getGenerator());
            bean.setDocs(stub.getDocs());
            bean.setCloud(CloudStructResourceUtil.convertCloudStructStubToBean(stub.getCloud()));
            bean.setTtl(stub.getTtl());
            bean.setImage(ImageStructResourceUtil.convertImageStructStubToBean(stub.getImage()));
            bean.setRating(stub.getRating());
            bean.setTextInput(TextInputStructResourceUtil.convertTextInputStructStubToBean(stub.getTextInput()));
            bean.setSkipHours(stub.getSkipHours());
            bean.setSkipDays(stub.getSkipDays());
            bean.setItem(stub.getItem());
        }
        return bean;
    }

}
