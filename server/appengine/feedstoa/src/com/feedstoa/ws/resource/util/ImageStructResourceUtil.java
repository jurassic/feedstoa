package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.bean.ImageStructBean;
import com.feedstoa.ws.stub.ImageStructStub;


public class ImageStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ImageStructResourceUtil.class.getName());

    // Static methods only.
    private ImageStructResourceUtil() {}

    public static ImageStructBean convertImageStructStubToBean(ImageStruct stub)
    {
        ImageStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ImageStructBean is returned.");
        } else {
            bean = new ImageStructBean();
            bean.setUrl(stub.getUrl());
            bean.setTitle(stub.getTitle());
            bean.setLink(stub.getLink());
            bean.setWidth(stub.getWidth());
            bean.setHeight(stub.getHeight());
            bean.setDescription(stub.getDescription());
        }
        return bean;
    }

}
