package com.feedstoa.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.feedstoa.ws.service.ApiConsumerService;
import com.feedstoa.ws.service.impl.ApiConsumerServiceImpl;
import com.feedstoa.ws.service.UserService;
import com.feedstoa.ws.service.impl.UserServiceImpl;
import com.feedstoa.ws.service.FetchRequestService;
import com.feedstoa.ws.service.impl.FetchRequestServiceImpl;
import com.feedstoa.ws.service.SecondaryFetchService;
import com.feedstoa.ws.service.impl.SecondaryFetchServiceImpl;
import com.feedstoa.ws.service.ChannelSourceService;
import com.feedstoa.ws.service.impl.ChannelSourceServiceImpl;
import com.feedstoa.ws.service.ChannelFeedService;
import com.feedstoa.ws.service.impl.ChannelFeedServiceImpl;
import com.feedstoa.ws.service.FeedItemService;
import com.feedstoa.ws.service.impl.FeedItemServiceImpl;
import com.feedstoa.ws.service.FeedContentService;
import com.feedstoa.ws.service.impl.FeedContentServiceImpl;
import com.feedstoa.ws.service.ServiceInfoService;
import com.feedstoa.ws.service.impl.ServiceInfoServiceImpl;
import com.feedstoa.ws.service.FiveTenService;
import com.feedstoa.ws.service.impl.FiveTenServiceImpl;


// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static ApiConsumerService apiConsumerService = null;
	private static UserService userService = null;
	private static FetchRequestService fetchRequestService = null;
	private static SecondaryFetchService secondaryFetchService = null;
	private static ChannelSourceService channelSourceService = null;
	private static ChannelFeedService channelFeedService = null;
	private static FeedItemService feedItemService = null;
	private static FeedContentService feedContentService = null;
	private static ServiceInfoService serviceInfoService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(ServiceManager.apiConsumerService == null) {
            ServiceManager.apiConsumerService = new ApiConsumerServiceImpl();
        }
        return ServiceManager.apiConsumerService;
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(ServiceManager.userService == null) {
            ServiceManager.userService = new UserServiceImpl();
        }
        return ServiceManager.userService;
    }

    // Returns a FetchRequestService instance.
	public static FetchRequestService getFetchRequestService() 
    {
        if(ServiceManager.fetchRequestService == null) {
            ServiceManager.fetchRequestService = new FetchRequestServiceImpl();
        }
        return ServiceManager.fetchRequestService;
    }

    // Returns a SecondaryFetchService instance.
	public static SecondaryFetchService getSecondaryFetchService() 
    {
        if(ServiceManager.secondaryFetchService == null) {
            ServiceManager.secondaryFetchService = new SecondaryFetchServiceImpl();
        }
        return ServiceManager.secondaryFetchService;
    }

    // Returns a ChannelSourceService instance.
	public static ChannelSourceService getChannelSourceService() 
    {
        if(ServiceManager.channelSourceService == null) {
            ServiceManager.channelSourceService = new ChannelSourceServiceImpl();
        }
        return ServiceManager.channelSourceService;
    }

    // Returns a ChannelFeedService instance.
	public static ChannelFeedService getChannelFeedService() 
    {
        if(ServiceManager.channelFeedService == null) {
            ServiceManager.channelFeedService = new ChannelFeedServiceImpl();
        }
        return ServiceManager.channelFeedService;
    }

    // Returns a FeedItemService instance.
	public static FeedItemService getFeedItemService() 
    {
        if(ServiceManager.feedItemService == null) {
            ServiceManager.feedItemService = new FeedItemServiceImpl();
        }
        return ServiceManager.feedItemService;
    }

    // Returns a FeedContentService instance.
	public static FeedContentService getFeedContentService() 
    {
        if(ServiceManager.feedContentService == null) {
            ServiceManager.feedContentService = new FeedContentServiceImpl();
        }
        return ServiceManager.feedContentService;
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(ServiceManager.serviceInfoService == null) {
            ServiceManager.serviceInfoService = new ServiceInfoServiceImpl();
        }
        return ServiceManager.serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
