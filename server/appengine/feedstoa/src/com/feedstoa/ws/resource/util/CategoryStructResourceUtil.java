package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.bean.CategoryStructBean;
import com.feedstoa.ws.stub.CategoryStructStub;


public class CategoryStructResourceUtil
{
    private static final Logger log = Logger.getLogger(CategoryStructResourceUtil.class.getName());

    // Static methods only.
    private CategoryStructResourceUtil() {}

    public static CategoryStructBean convertCategoryStructStubToBean(CategoryStruct stub)
    {
        CategoryStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null CategoryStructBean is returned.");
        } else {
            bean = new CategoryStructBean();
            bean.setUuid(stub.getUuid());
            bean.setDomain(stub.getDomain());
            bean.setLabel(stub.getLabel());
            bean.setTitle(stub.getTitle());
        }
        return bean;
    }

}
