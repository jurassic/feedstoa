package com.feedstoa.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.GeoPointStruct;
import com.feedstoa.ws.bean.GeoPointStructBean;
import com.feedstoa.ws.stub.GeoPointStructStub;


public class GeoPointStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GeoPointStructResourceUtil.class.getName());

    // Static methods only.
    private GeoPointStructResourceUtil() {}

    public static GeoPointStructBean convertGeoPointStructStubToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null GeoPointStructBean is returned.");
        } else {
            bean = new GeoPointStructBean();
            bean.setUuid(stub.getUuid());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
            bean.setAltitude(stub.getAltitude());
            bean.setSensorUsed(stub.isSensorUsed());
        }
        return bean;
    }

}
