package com.feedstoa.ws.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.CommonConstants;
import com.feedstoa.ws.core.GUID;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.exception.InternalServerErrorException;
import com.feedstoa.ws.exception.NotImplementedException;
import com.feedstoa.ws.exception.RequestConflictException;
import com.feedstoa.ws.exception.RequestForbiddenException;
import com.feedstoa.ws.exception.DataStoreException;
import com.feedstoa.ws.exception.ResourceGoneException;
import com.feedstoa.ws.exception.ResourceNotFoundException;
import com.feedstoa.ws.exception.ResourceAlreadyPresentException;
import com.feedstoa.ws.exception.ServiceUnavailableException;
import com.feedstoa.ws.exception.resource.BaseResourceException;
import com.feedstoa.ws.resource.exception.BadRequestRsException;
import com.feedstoa.ws.resource.exception.InternalServerErrorRsException;
import com.feedstoa.ws.resource.exception.NotImplementedRsException;
import com.feedstoa.ws.resource.exception.RequestConflictRsException;
import com.feedstoa.ws.resource.exception.RequestForbiddenRsException;
import com.feedstoa.ws.resource.exception.DataStoreRsException;
import com.feedstoa.ws.resource.exception.ResourceGoneRsException;
import com.feedstoa.ws.resource.exception.ResourceNotFoundRsException;
import com.feedstoa.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.feedstoa.ws.resource.exception.ServiceUnavailableRsException;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.bean.ChannelFeedBean;
import com.feedstoa.ws.stub.ChannelFeedListStub;
import com.feedstoa.ws.stub.ChannelFeedStub;
import com.feedstoa.ws.resource.ServiceManager;
import com.feedstoa.ws.resource.ChannelFeedResource;
import com.feedstoa.ws.resource.util.CloudStructResourceUtil;
import com.feedstoa.ws.resource.util.CategoryStructResourceUtil;
import com.feedstoa.ws.resource.util.ImageStructResourceUtil;
import com.feedstoa.ws.resource.util.UriStructResourceUtil;
import com.feedstoa.ws.resource.util.UserStructResourceUtil;
import com.feedstoa.ws.resource.util.ReferrerInfoStructResourceUtil;
import com.feedstoa.ws.resource.util.TextInputStructResourceUtil;


@Path("/_task/w/channelFeeds/")
public class AsyncChannelFeedResource extends BaseAsyncResource implements ChannelFeedResource
{
    private static final Logger log = Logger.getLogger(AsyncChannelFeedResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncChannelFeedResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getChannelFeedList(List<ChannelFeed> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getChannelFeedKeys(List<String> guids) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getChannelFeed(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getChannelFeed(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response createChannelFeed(ChannelFeedStub channelFeed) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ChannelFeedBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedStub realStub = (ChannelFeedStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertChannelFeedStubToBean(realStub);
            } else {
                bean = convertChannelFeedStubToBean(channelFeed);
            }
            String guid = ServiceManager.getChannelFeedService().createChannelFeed(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createChannelFeed(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateChannelFeed(String guid, ChannelFeedStub channelFeed) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(channelFeed == null || !guid.equals(channelFeed.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from channelFeed guid = " + channelFeed.getGuid());
                throw new RequestForbiddenException("Failed to update the channelFeed with guid = " + guid);
            }
            ChannelFeedBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ChannelFeedStub realStub = (ChannelFeedStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertChannelFeedStubToBean(realStub);
            } else {
                bean = convertChannelFeedStubToBean(channelFeed);
            }
            boolean suc = ServiceManager.getChannelFeedService().updateChannelFeed(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the channelFeed with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the channelFeed with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateChannelFeed(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, String link, String description, String language, String copyright, String managingEditor, String webMaster, List<String> contributor, String pubDate, String lastBuildDate, List<String> category, String generator, String docs, String cloud, Integer ttl, String logo, String icon, String rating, String textInput, List<String> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, String referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteChannelFeed(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteChannelFeed(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getChannelFeedService().deleteChannelFeed(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the channelFeed with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the channelFeed with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteChannelFeed(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteChannelFeeds(String filter, String params, List<String> values) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    public static ChannelFeedBean convertChannelFeedStubToBean(ChannelFeed stub)
    {
        ChannelFeedBean bean = new ChannelFeedBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setUser(stub.getUser());
            bean.setChannelSource(stub.getChannelSource());
            bean.setChannelCode(stub.getChannelCode());
            bean.setPreviousVersion(stub.getPreviousVersion());
            bean.setFetchRequest(stub.getFetchRequest());
            bean.setFetchUrl(stub.getFetchUrl());
            bean.setFeedServiceUrl(stub.getFeedServiceUrl());
            bean.setFeedUrl(stub.getFeedUrl());
            bean.setFeedFormat(stub.getFeedFormat());
            bean.setMaxItemCount(stub.getMaxItemCount());
            bean.setFeedCategory(stub.getFeedCategory());
            bean.setTitle(stub.getTitle());
            bean.setSubtitle(stub.getSubtitle());
            bean.setLink(UriStructResourceUtil.convertUriStructStubToBean(stub.getLink()));
            bean.setDescription(stub.getDescription());
            bean.setLanguage(stub.getLanguage());
            bean.setCopyright(stub.getCopyright());
            bean.setManagingEditor(UserStructResourceUtil.convertUserStructStubToBean(stub.getManagingEditor()));
            bean.setWebMaster(UserStructResourceUtil.convertUserStructStubToBean(stub.getWebMaster()));
            bean.setContributor(stub.getContributor());
            bean.setPubDate(stub.getPubDate());
            bean.setLastBuildDate(stub.getLastBuildDate());
            bean.setCategory(stub.getCategory());
            bean.setGenerator(stub.getGenerator());
            bean.setDocs(stub.getDocs());
            bean.setCloud(CloudStructResourceUtil.convertCloudStructStubToBean(stub.getCloud()));
            bean.setTtl(stub.getTtl());
            bean.setLogo(ImageStructResourceUtil.convertImageStructStubToBean(stub.getLogo()));
            bean.setIcon(ImageStructResourceUtil.convertImageStructStubToBean(stub.getIcon()));
            bean.setRating(stub.getRating());
            bean.setTextInput(TextInputStructResourceUtil.convertTextInputStructStubToBean(stub.getTextInput()));
            bean.setSkipHours(stub.getSkipHours());
            bean.setSkipDays(stub.getSkipDays());
            bean.setOutputText(stub.getOutputText());
            bean.setOutputHash(stub.getOutputHash());
            bean.setFeedContent(stub.getFeedContent());
            bean.setStatus(stub.getStatus());
            bean.setNote(stub.getNote());
            bean.setReferrerInfo(ReferrerInfoStructResourceUtil.convertReferrerInfoStructStubToBean(stub.getReferrerInfo()));
            bean.setLastBuildTime(stub.getLastBuildTime());
            bean.setPublishedTime(stub.getPublishedTime());
            bean.setExpirationTime(stub.getExpirationTime());
            bean.setLastUpdatedTime(stub.getLastUpdatedTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
