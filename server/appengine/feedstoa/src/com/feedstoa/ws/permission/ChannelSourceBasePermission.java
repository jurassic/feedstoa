package com.feedstoa.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ChannelSourceBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ChannelSourceBasePermission.class.getName());

    public ChannelSourceBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "ChannelSource::" + action;
    }


}
