package com.feedstoa.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class FeedContentBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FeedContentBasePermission.class.getName());

    public FeedContentBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "FeedContent::" + action;
    }


}
