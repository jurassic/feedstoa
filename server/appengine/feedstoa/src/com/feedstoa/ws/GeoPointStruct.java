package com.feedstoa.ws;



public interface GeoPointStruct 
{
    String  getUuid();
    Double  getLatitude();
    Double  getLongitude();
    Double  getAltitude();
    Boolean  isSensorUsed();
    boolean isEmpty();
}
