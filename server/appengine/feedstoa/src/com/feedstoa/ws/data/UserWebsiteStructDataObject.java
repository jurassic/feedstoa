package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.UserWebsiteStruct;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class UserWebsiteStructDataObject implements UserWebsiteStruct, Serializable
{
    private static final Logger log = Logger.getLogger(UserWebsiteStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _userwebsitestruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String primarySite;

    @Persistent(defaultFetchGroup = "true")
    private String homePage;

    @Persistent(defaultFetchGroup = "true")
    private String blogSite;

    @Persistent(defaultFetchGroup = "true")
    private String portfolioSite;

    @Persistent(defaultFetchGroup = "true")
    private String twitterProfile;

    @Persistent(defaultFetchGroup = "true")
    private String facebookProfile;

    @Persistent(defaultFetchGroup = "true")
    private String googlePlusProfile;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public UserWebsiteStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null);
    }
    public UserWebsiteStructDataObject(String uuid, String primarySite, String homePage, String blogSite, String portfolioSite, String twitterProfile, String facebookProfile, String googlePlusProfile, String note)
    {
        setUuid(uuid);
        setPrimarySite(primarySite);
        setHomePage(homePage);
        setBlogSite(blogSite);
        setPortfolioSite(portfolioSite);
        setTwitterProfile(twitterProfile);
        setFacebookProfile(facebookProfile);
        setGooglePlusProfile(googlePlusProfile);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_userwebsitestruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _userwebsitestruct_encoded_key = KeyFactory.createKeyString(UserWebsiteStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _userwebsitestruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _userwebsitestruct_encoded_key = KeyFactory.createKeyString(UserWebsiteStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getPrimarySite()
    {
        return this.primarySite;
    }
    public void setPrimarySite(String primarySite)
    {
        this.primarySite = primarySite;
        if(this.primarySite != null) {
            resetEncodedKey();
        }
    }

    public String getHomePage()
    {
        return this.homePage;
    }
    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
        if(this.homePage != null) {
            resetEncodedKey();
        }
    }

    public String getBlogSite()
    {
        return this.blogSite;
    }
    public void setBlogSite(String blogSite)
    {
        this.blogSite = blogSite;
        if(this.blogSite != null) {
            resetEncodedKey();
        }
    }

    public String getPortfolioSite()
    {
        return this.portfolioSite;
    }
    public void setPortfolioSite(String portfolioSite)
    {
        this.portfolioSite = portfolioSite;
        if(this.portfolioSite != null) {
            resetEncodedKey();
        }
    }

    public String getTwitterProfile()
    {
        return this.twitterProfile;
    }
    public void setTwitterProfile(String twitterProfile)
    {
        this.twitterProfile = twitterProfile;
        if(this.twitterProfile != null) {
            resetEncodedKey();
        }
    }

    public String getFacebookProfile()
    {
        return this.facebookProfile;
    }
    public void setFacebookProfile(String facebookProfile)
    {
        this.facebookProfile = facebookProfile;
        if(this.facebookProfile != null) {
            resetEncodedKey();
        }
    }

    public String getGooglePlusProfile()
    {
        return this.googlePlusProfile;
    }
    public void setGooglePlusProfile(String googlePlusProfile)
    {
        this.googlePlusProfile = googlePlusProfile;
        if(this.googlePlusProfile != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPrimarySite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHomePage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getBlogSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPortfolioSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGooglePlusProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("primarySite", this.primarySite);
        dataMap.put("homePage", this.homePage);
        dataMap.put("blogSite", this.blogSite);
        dataMap.put("portfolioSite", this.portfolioSite);
        dataMap.put("twitterProfile", this.twitterProfile);
        dataMap.put("facebookProfile", this.facebookProfile);
        dataMap.put("googlePlusProfile", this.googlePlusProfile);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserWebsiteStruct thatObj = (UserWebsiteStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.primarySite == null && thatObj.getPrimarySite() != null)
            || (this.primarySite != null && thatObj.getPrimarySite() == null)
            || !this.primarySite.equals(thatObj.getPrimarySite()) ) {
            return false;
        }
        if( (this.homePage == null && thatObj.getHomePage() != null)
            || (this.homePage != null && thatObj.getHomePage() == null)
            || !this.homePage.equals(thatObj.getHomePage()) ) {
            return false;
        }
        if( (this.blogSite == null && thatObj.getBlogSite() != null)
            || (this.blogSite != null && thatObj.getBlogSite() == null)
            || !this.blogSite.equals(thatObj.getBlogSite()) ) {
            return false;
        }
        if( (this.portfolioSite == null && thatObj.getPortfolioSite() != null)
            || (this.portfolioSite != null && thatObj.getPortfolioSite() == null)
            || !this.portfolioSite.equals(thatObj.getPortfolioSite()) ) {
            return false;
        }
        if( (this.twitterProfile == null && thatObj.getTwitterProfile() != null)
            || (this.twitterProfile != null && thatObj.getTwitterProfile() == null)
            || !this.twitterProfile.equals(thatObj.getTwitterProfile()) ) {
            return false;
        }
        if( (this.facebookProfile == null && thatObj.getFacebookProfile() != null)
            || (this.facebookProfile != null && thatObj.getFacebookProfile() == null)
            || !this.facebookProfile.equals(thatObj.getFacebookProfile()) ) {
            return false;
        }
        if( (this.googlePlusProfile == null && thatObj.getGooglePlusProfile() != null)
            || (this.googlePlusProfile != null && thatObj.getGooglePlusProfile() == null)
            || !this.googlePlusProfile.equals(thatObj.getGooglePlusProfile()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = primarySite == null ? 0 : primarySite.hashCode();
        _hash = 31 * _hash + delta;
        delta = homePage == null ? 0 : homePage.hashCode();
        _hash = 31 * _hash + delta;
        delta = blogSite == null ? 0 : blogSite.hashCode();
        _hash = 31 * _hash + delta;
        delta = portfolioSite == null ? 0 : portfolioSite.hashCode();
        _hash = 31 * _hash + delta;
        delta = twitterProfile == null ? 0 : twitterProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = facebookProfile == null ? 0 : facebookProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = googlePlusProfile == null ? 0 : googlePlusProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
