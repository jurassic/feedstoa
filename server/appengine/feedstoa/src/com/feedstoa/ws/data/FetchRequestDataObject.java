package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class FetchRequestDataObject extends FetchBaseDataObject implements FetchRequest
{
    private static final Logger log = Logger.getLogger(FetchRequestDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FetchRequestDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FetchRequestDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String originFetch;

    @Persistent(defaultFetchGroup = "true")
    private String outputFormat;

    @Persistent(defaultFetchGroup = "true")
    private Integer fetchStatus;

    @Persistent(defaultFetchGroup = "true")
    private String result;

    @Persistent(defaultFetchGroup = "true")
    private String feedCategory;

    @Persistent(defaultFetchGroup = "true")
    private Boolean multipleFeedEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean deferred;

    @Persistent(defaultFetchGroup = "true")
    private Boolean alert;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="preferredMode", columns=@Column(name="notificationPrefpreferredMode")),
        @Persistent(name="mobileNumber", columns=@Column(name="notificationPrefmobileNumber")),
        @Persistent(name="emailAddress", columns=@Column(name="notificationPrefemailAddress")),
        @Persistent(name="twitterUsername", columns=@Column(name="notificationPreftwitterUsername")),
        @Persistent(name="facebookId", columns=@Column(name="notificationPreffacebookId")),
        @Persistent(name="linkedinId", columns=@Column(name="notificationPreflinkedinId")),
        @Persistent(name="note", columns=@Column(name="notificationPrefnote")),
    })
    private NotificationStructDataObject notificationPref;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private Integer refreshInterval;

    @Persistent(defaultFetchGroup = "true")
    private List<String> refreshExpressions;

    @Persistent(defaultFetchGroup = "true")
    private String refreshTimeZone;

    @Persistent(defaultFetchGroup = "true")
    private Integer currentRefreshCount;

    @Persistent(defaultFetchGroup = "true")
    private Integer maxRefreshCount;

    @Persistent(defaultFetchGroup = "true")
    private Long refreshExpirationTime;

    @Persistent(defaultFetchGroup = "true")
    private Long nextRefreshTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastUpdatedTime;

    public FetchRequestDataObject()
    {
        this(null);
    }
    public FetchRequestDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchRequestDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime)
    {
        this(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPref, referrerInfo, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime, null, null);
    }
    public FetchRequestDataObject(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, managerApp, appAcl, gaeApp, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, createdTime, modifiedTime);
        this.originFetch = originFetch;
        this.outputFormat = outputFormat;
        this.fetchStatus = fetchStatus;
        this.result = result;
        this.feedCategory = feedCategory;
        this.multipleFeedEnabled = multipleFeedEnabled;
        this.deferred = deferred;
        this.alert = alert;
        if(notificationPref != null) {
            this.notificationPref = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            this.notificationPref = null;
        }
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.refreshInterval = refreshInterval;
        this.refreshExpressions = refreshExpressions;  // ???
        this.refreshTimeZone = refreshTimeZone;
        this.currentRefreshCount = currentRefreshCount;
        this.maxRefreshCount = maxRefreshCount;
        this.refreshExpirationTime = refreshExpirationTime;
        this.nextRefreshTime = nextRefreshTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

//    @Override
//    protected Key createKey()
//    {
//        return FetchRequestDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FetchRequestDataObject.composeKey(getGuid());
    }

    public String getOriginFetch()
    {
        return this.originFetch;
    }
    public void setOriginFetch(String originFetch)
    {
        this.originFetch = originFetch;
    }

    public String getOutputFormat()
    {
        return this.outputFormat;
    }
    public void setOutputFormat(String outputFormat)
    {
        this.outputFormat = outputFormat;
    }

    public Integer getFetchStatus()
    {
        return this.fetchStatus;
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        this.fetchStatus = fetchStatus;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getFeedCategory()
    {
        return this.feedCategory;
    }
    public void setFeedCategory(String feedCategory)
    {
        this.feedCategory = feedCategory;
    }

    public Boolean isMultipleFeedEnabled()
    {
        return this.multipleFeedEnabled;
    }
    public void setMultipleFeedEnabled(Boolean multipleFeedEnabled)
    {
        this.multipleFeedEnabled = multipleFeedEnabled;
    }

    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    public NotificationStruct getNotificationPref()
    {
        return this.notificationPref;
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(notificationPref == null) {
            this.notificationPref = null;
            log.log(Level.INFO, "FetchRequestDataObject.setNotificationPref(NotificationStruct notificationPref): Arg notificationPref is null.");            
        } else if(notificationPref instanceof NotificationStructDataObject) {
            this.notificationPref = (NotificationStructDataObject) notificationPref;
        } else if(notificationPref instanceof NotificationStruct) {
            this.notificationPref = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            this.notificationPref = new NotificationStructDataObject();   // ????
            log.log(Level.WARNING, "FetchRequestDataObject.setNotificationPref(NotificationStruct notificationPref): Arg notificationPref is of an invalid type.");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "FetchRequestDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "FetchRequestDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public Integer getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    public List<String> getRefreshExpressions()
    {
        return this.refreshExpressions;   // ???
    }
    public void setRefreshExpressions(List<String> refreshExpressions)
    {
        this.refreshExpressions = refreshExpressions;
    }

    public String getRefreshTimeZone()
    {
        return this.refreshTimeZone;
    }
    public void setRefreshTimeZone(String refreshTimeZone)
    {
        this.refreshTimeZone = refreshTimeZone;
    }

    public Integer getCurrentRefreshCount()
    {
        return this.currentRefreshCount;
    }
    public void setCurrentRefreshCount(Integer currentRefreshCount)
    {
        this.currentRefreshCount = currentRefreshCount;
    }

    public Integer getMaxRefreshCount()
    {
        return this.maxRefreshCount;
    }
    public void setMaxRefreshCount(Integer maxRefreshCount)
    {
        this.maxRefreshCount = maxRefreshCount;
    }

    public Long getRefreshExpirationTime()
    {
        return this.refreshExpirationTime;
    }
    public void setRefreshExpirationTime(Long refreshExpirationTime)
    {
        this.refreshExpirationTime = refreshExpirationTime;
    }

    public Long getNextRefreshTime()
    {
        return this.nextRefreshTime;
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        this.nextRefreshTime = nextRefreshTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("originFetch", this.originFetch);
        dataMap.put("outputFormat", this.outputFormat);
        dataMap.put("fetchStatus", this.fetchStatus);
        dataMap.put("result", this.result);
        dataMap.put("feedCategory", this.feedCategory);
        dataMap.put("multipleFeedEnabled", this.multipleFeedEnabled);
        dataMap.put("deferred", this.deferred);
        dataMap.put("alert", this.alert);
        dataMap.put("notificationPref", this.notificationPref);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("refreshInterval", this.refreshInterval);
        dataMap.put("refreshExpressions", this.refreshExpressions);
        dataMap.put("refreshTimeZone", this.refreshTimeZone);
        dataMap.put("currentRefreshCount", this.currentRefreshCount);
        dataMap.put("maxRefreshCount", this.maxRefreshCount);
        dataMap.put("refreshExpirationTime", this.refreshExpirationTime);
        dataMap.put("nextRefreshTime", this.nextRefreshTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FetchRequest thatObj = (FetchRequest) obj;
        if( (this.originFetch == null && thatObj.getOriginFetch() != null)
            || (this.originFetch != null && thatObj.getOriginFetch() == null)
            || !this.originFetch.equals(thatObj.getOriginFetch()) ) {
            return false;
        }
        if( (this.outputFormat == null && thatObj.getOutputFormat() != null)
            || (this.outputFormat != null && thatObj.getOutputFormat() == null)
            || !this.outputFormat.equals(thatObj.getOutputFormat()) ) {
            return false;
        }
        if( (this.fetchStatus == null && thatObj.getFetchStatus() != null)
            || (this.fetchStatus != null && thatObj.getFetchStatus() == null)
            || !this.fetchStatus.equals(thatObj.getFetchStatus()) ) {
            return false;
        }
        if( (this.result == null && thatObj.getResult() != null)
            || (this.result != null && thatObj.getResult() == null)
            || !this.result.equals(thatObj.getResult()) ) {
            return false;
        }
        if( (this.feedCategory == null && thatObj.getFeedCategory() != null)
            || (this.feedCategory != null && thatObj.getFeedCategory() == null)
            || !this.feedCategory.equals(thatObj.getFeedCategory()) ) {
            return false;
        }
        if( (this.multipleFeedEnabled == null && thatObj.isMultipleFeedEnabled() != null)
            || (this.multipleFeedEnabled != null && thatObj.isMultipleFeedEnabled() == null)
            || !this.multipleFeedEnabled.equals(thatObj.isMultipleFeedEnabled()) ) {
            return false;
        }
        if( (this.deferred == null && thatObj.isDeferred() != null)
            || (this.deferred != null && thatObj.isDeferred() == null)
            || !this.deferred.equals(thatObj.isDeferred()) ) {
            return false;
        }
        if( (this.alert == null && thatObj.isAlert() != null)
            || (this.alert != null && thatObj.isAlert() == null)
            || !this.alert.equals(thatObj.isAlert()) ) {
            return false;
        }
        if( (this.notificationPref == null && thatObj.getNotificationPref() != null)
            || (this.notificationPref != null && thatObj.getNotificationPref() == null)
            || !this.notificationPref.equals(thatObj.getNotificationPref()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.refreshInterval == null && thatObj.getRefreshInterval() != null)
            || (this.refreshInterval != null && thatObj.getRefreshInterval() == null)
            || !this.refreshInterval.equals(thatObj.getRefreshInterval()) ) {
            return false;
        }
        if( (this.refreshExpressions == null && thatObj.getRefreshExpressions() != null)
            || (this.refreshExpressions != null && thatObj.getRefreshExpressions() == null)
            || !this.refreshExpressions.equals(thatObj.getRefreshExpressions()) ) {
            return false;
        }
        if( (this.refreshTimeZone == null && thatObj.getRefreshTimeZone() != null)
            || (this.refreshTimeZone != null && thatObj.getRefreshTimeZone() == null)
            || !this.refreshTimeZone.equals(thatObj.getRefreshTimeZone()) ) {
            return false;
        }
        if( (this.currentRefreshCount == null && thatObj.getCurrentRefreshCount() != null)
            || (this.currentRefreshCount != null && thatObj.getCurrentRefreshCount() == null)
            || !this.currentRefreshCount.equals(thatObj.getCurrentRefreshCount()) ) {
            return false;
        }
        if( (this.maxRefreshCount == null && thatObj.getMaxRefreshCount() != null)
            || (this.maxRefreshCount != null && thatObj.getMaxRefreshCount() == null)
            || !this.maxRefreshCount.equals(thatObj.getMaxRefreshCount()) ) {
            return false;
        }
        if( (this.refreshExpirationTime == null && thatObj.getRefreshExpirationTime() != null)
            || (this.refreshExpirationTime != null && thatObj.getRefreshExpirationTime() == null)
            || !this.refreshExpirationTime.equals(thatObj.getRefreshExpirationTime()) ) {
            return false;
        }
        if( (this.nextRefreshTime == null && thatObj.getNextRefreshTime() != null)
            || (this.nextRefreshTime != null && thatObj.getNextRefreshTime() == null)
            || !this.nextRefreshTime.equals(thatObj.getNextRefreshTime()) ) {
            return false;
        }
        if( (this.lastUpdatedTime == null && thatObj.getLastUpdatedTime() != null)
            || (this.lastUpdatedTime != null && thatObj.getLastUpdatedTime() == null)
            || !this.lastUpdatedTime.equals(thatObj.getLastUpdatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = originFetch == null ? 0 : originFetch.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputFormat == null ? 0 : outputFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchStatus == null ? 0 : fetchStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedCategory == null ? 0 : feedCategory.hashCode();
        _hash = 31 * _hash + delta;
        delta = multipleFeedEnabled == null ? 0 : multipleFeedEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = deferred == null ? 0 : deferred.hashCode();
        _hash = 31 * _hash + delta;
        delta = alert == null ? 0 : alert.hashCode();
        _hash = 31 * _hash + delta;
        delta = notificationPref == null ? 0 : notificationPref.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshExpressions == null ? 0 : refreshExpressions.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshTimeZone == null ? 0 : refreshTimeZone.hashCode();
        _hash = 31 * _hash + delta;
        delta = currentRefreshCount == null ? 0 : currentRefreshCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxRefreshCount == null ? 0 : maxRefreshCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshExpirationTime == null ? 0 : refreshExpirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextRefreshTime == null ? 0 : nextRefreshTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
