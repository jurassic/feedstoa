package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class RedirectRuleDataObject implements RedirectRule, Serializable
{
    private static final Logger log = Logger.getLogger(RedirectRuleDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _redirectrule_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String redirectType;

    @Persistent(defaultFetchGroup = "true")
    private Double precedence;

    @Persistent(defaultFetchGroup = "true")
    private String sourceDomain;

    @Persistent(defaultFetchGroup = "true")
    private String sourcePath;

    @Persistent(defaultFetchGroup = "true")
    private String targetDomain;

    @Persistent(defaultFetchGroup = "true")
    private String targetPath;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    public RedirectRuleDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null);
    }
    public RedirectRuleDataObject(String redirectType, Double precedence, String sourceDomain, String sourcePath, String targetDomain, String targetPath, String note, String status)
    {
        setRedirectType(redirectType);
        setPrecedence(precedence);
        setSourceDomain(sourceDomain);
        setSourcePath(sourcePath);
        setTargetDomain(targetDomain);
        setTargetPath(targetPath);
        setNote(note);
        setStatus(status);
    }

    private void resetEncodedKey()
    {
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
        if(this.redirectType != null) {
            resetEncodedKey();
        }
    }

    public Double getPrecedence()
    {
        return this.precedence;
    }
    public void setPrecedence(Double precedence)
    {
        this.precedence = precedence;
        if(this.precedence != null) {
            resetEncodedKey();
        }
    }

    public String getSourceDomain()
    {
        return this.sourceDomain;
    }
    public void setSourceDomain(String sourceDomain)
    {
        this.sourceDomain = sourceDomain;
        if(this.sourceDomain != null) {
            resetEncodedKey();
        }
    }

    public String getSourcePath()
    {
        return this.sourcePath;
    }
    public void setSourcePath(String sourcePath)
    {
        this.sourcePath = sourcePath;
        if(this.sourcePath != null) {
            resetEncodedKey();
        }
    }

    public String getTargetDomain()
    {
        return this.targetDomain;
    }
    public void setTargetDomain(String targetDomain)
    {
        this.targetDomain = targetDomain;
        if(this.targetDomain != null) {
            resetEncodedKey();
        }
    }

    public String getTargetPath()
    {
        return this.targetPath;
    }
    public void setTargetPath(String targetPath)
    {
        this.targetPath = targetPath;
        if(this.targetPath != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
        if(this.status != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrecedence() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourceDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourcePath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("redirectType", this.redirectType);
        dataMap.put("precedence", this.precedence);
        dataMap.put("sourceDomain", this.sourceDomain);
        dataMap.put("sourcePath", this.sourcePath);
        dataMap.put("targetDomain", this.targetDomain);
        dataMap.put("targetPath", this.targetPath);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RedirectRule thatObj = (RedirectRule) obj;
        if( (this.redirectType == null && thatObj.getRedirectType() != null)
            || (this.redirectType != null && thatObj.getRedirectType() == null)
            || !this.redirectType.equals(thatObj.getRedirectType()) ) {
            return false;
        }
        if( (this.precedence == null && thatObj.getPrecedence() != null)
            || (this.precedence != null && thatObj.getPrecedence() == null)
            || !this.precedence.equals(thatObj.getPrecedence()) ) {
            return false;
        }
        if( (this.sourceDomain == null && thatObj.getSourceDomain() != null)
            || (this.sourceDomain != null && thatObj.getSourceDomain() == null)
            || !this.sourceDomain.equals(thatObj.getSourceDomain()) ) {
            return false;
        }
        if( (this.sourcePath == null && thatObj.getSourcePath() != null)
            || (this.sourcePath != null && thatObj.getSourcePath() == null)
            || !this.sourcePath.equals(thatObj.getSourcePath()) ) {
            return false;
        }
        if( (this.targetDomain == null && thatObj.getTargetDomain() != null)
            || (this.targetDomain != null && thatObj.getTargetDomain() == null)
            || !this.targetDomain.equals(thatObj.getTargetDomain()) ) {
            return false;
        }
        if( (this.targetPath == null && thatObj.getTargetPath() != null)
            || (this.targetPath != null && thatObj.getTargetPath() == null)
            || !this.targetPath.equals(thatObj.getTargetPath()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = precedence == null ? 0 : precedence.hashCode();
        _hash = 31 * _hash + delta;
        delta = sourceDomain == null ? 0 : sourceDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = sourcePath == null ? 0 : sourcePath.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetDomain == null ? 0 : targetDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetPath == null ? 0 : targetPath.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
