package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class RssChannelDataObject implements RssChannel, Serializable
{
    private static final Logger log = Logger.getLogger(RssChannelDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _rsschannel_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String link;

    @Persistent(defaultFetchGroup = "true")
    private Text description;

    @Persistent(defaultFetchGroup = "true")
    private String language;

    @Persistent(defaultFetchGroup = "true")
    private String copyright;

    @Persistent(defaultFetchGroup = "true")
    private String managingEditor;

    @Persistent(defaultFetchGroup = "true")
    private String webMaster;

    @Persistent(defaultFetchGroup = "true")
    private String pubDate;

    @Persistent(defaultFetchGroup = "true")
    private String lastBuildDate;

    @Persistent(defaultFetchGroup = "false")
    private List<CategoryStructDataObject> category;

    @Persistent(defaultFetchGroup = "true")
    private String generator;

    @Persistent(defaultFetchGroup = "true")
    private String docs;

    @Persistent(defaultFetchGroup = "true")
    private CloudStructDataObject cloud;

    @Persistent(defaultFetchGroup = "true")
    private Integer ttl;

    @Persistent(defaultFetchGroup = "true")
    private ImageStructDataObject image;

    @Persistent(defaultFetchGroup = "true")
    private String rating;

    @Persistent(defaultFetchGroup = "true")
    private TextInputStructDataObject textInput;

    @Persistent(defaultFetchGroup = "false")
    private List<Integer> skipHours;

    @Persistent(defaultFetchGroup = "false")
    private List<String> skipDays;

    @Persistent(defaultFetchGroup = "false")
    private List<RssItemDataObject> item;

    public RssChannelDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public RssChannelDataObject(String title, String link, String description, String language, String copyright, String managingEditor, String webMaster, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct image, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, List<RssItem> item)
    {
        setTitle(title);
        setLink(link);
        setDescription(description);
        setLanguage(language);
        setCopyright(copyright);
        setManagingEditor(managingEditor);
        setWebMaster(webMaster);
        setPubDate(pubDate);
        setLastBuildDate(lastBuildDate);
        setCategory(category);
        setGenerator(generator);
        setDocs(docs);
        if(cloud != null) {
            this.cloud = new CloudStructDataObject(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            this.cloud = null;
        }
        setTtl(ttl);
        if(image != null) {
            this.image = new ImageStructDataObject(image.getUrl(), image.getTitle(), image.getLink(), image.getWidth(), image.getHeight(), image.getDescription());
        } else {
            this.image = null;
        }
        setRating(rating);
        if(textInput != null) {
            this.textInput = new TextInputStructDataObject(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            this.textInput = null;
        }
        this.skipHours = skipHours;  // ???
        this.skipDays = skipDays;  // ???
        setItem(item);
    }

    private void resetEncodedKey()
    {
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
        if(this.title != null) {
            resetEncodedKey();
        }
    }

    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
        if(this.link != null) {
            resetEncodedKey();
        }
    }

    public String getDescription()
    {
        if(this.description == null) {
            return null;
        }    
        return this.description.getValue();
    }
    public void setDescription(String description)
    {
        if(description == null) {
            this.description = null;
        } else {
            this.description = new Text(description);
        }
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
        if(this.language != null) {
            resetEncodedKey();
        }
    }

    public String getCopyright()
    {
        return this.copyright;
    }
    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
        if(this.copyright != null) {
            resetEncodedKey();
        }
    }

    public String getManagingEditor()
    {
        return this.managingEditor;
    }
    public void setManagingEditor(String managingEditor)
    {
        this.managingEditor = managingEditor;
        if(this.managingEditor != null) {
            resetEncodedKey();
        }
    }

    public String getWebMaster()
    {
        return this.webMaster;
    }
    public void setWebMaster(String webMaster)
    {
        this.webMaster = webMaster;
        if(this.webMaster != null) {
            resetEncodedKey();
        }
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
        if(this.pubDate != null) {
            resetEncodedKey();
        }
    }

    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
        if(this.lastBuildDate != null) {
            resetEncodedKey();
        }
    }

    @SuppressWarnings("unchecked")
    public List<CategoryStruct> getCategory()
    {         
        return (List<CategoryStruct>) ((List<?>) this.category);
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            List<CategoryStructDataObject> dataObj = new ArrayList<CategoryStructDataObject>();
            for(CategoryStruct categoryStruct : category) {
                CategoryStructDataObject elem = null;
                if(categoryStruct instanceof CategoryStructDataObject) {
                    elem = (CategoryStructDataObject) categoryStruct;
                } else if(categoryStruct instanceof CategoryStruct) {
                    elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.category = dataObj;
        } else {
            this.category = null;
        }
    }

    public String getGenerator()
    {
        return this.generator;
    }
    public void setGenerator(String generator)
    {
        this.generator = generator;
        if(this.generator != null) {
            resetEncodedKey();
        }
    }

    public String getDocs()
    {
        return this.docs;
    }
    public void setDocs(String docs)
    {
        this.docs = docs;
        if(this.docs != null) {
            resetEncodedKey();
        }
    }

    public CloudStruct getCloud()
    {
        return this.cloud;
    }
    public void setCloud(CloudStruct cloud)
    {
        if(cloud == null) {
            this.cloud = null;
            log.log(Level.INFO, "RssChannelDataObject.setCloud(CloudStruct cloud): Arg cloud is null.");            
        } else if(cloud instanceof CloudStructDataObject) {
            this.cloud = (CloudStructDataObject) cloud;
        } else if(cloud instanceof CloudStruct) {
            this.cloud = new CloudStructDataObject(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            this.cloud = new CloudStructDataObject();   // ????
            log.log(Level.WARNING, "RssChannelDataObject.setCloud(CloudStruct cloud): Arg cloud is of an invalid type.");
        }
    }

    public Integer getTtl()
    {
        return this.ttl;
    }
    public void setTtl(Integer ttl)
    {
        this.ttl = ttl;
        if(this.ttl != null) {
            resetEncodedKey();
        }
    }

    public ImageStruct getImage()
    {
        return this.image;
    }
    public void setImage(ImageStruct image)
    {
        if(image == null) {
            this.image = null;
            log.log(Level.INFO, "RssChannelDataObject.setImage(ImageStruct image): Arg image is null.");            
        } else if(image instanceof ImageStructDataObject) {
            this.image = (ImageStructDataObject) image;
        } else if(image instanceof ImageStruct) {
            this.image = new ImageStructDataObject(image.getUrl(), image.getTitle(), image.getLink(), image.getWidth(), image.getHeight(), image.getDescription());
        } else {
            this.image = new ImageStructDataObject();   // ????
            log.log(Level.WARNING, "RssChannelDataObject.setImage(ImageStruct image): Arg image is of an invalid type.");
        }
    }

    public String getRating()
    {
        return this.rating;
    }
    public void setRating(String rating)
    {
        this.rating = rating;
        if(this.rating != null) {
            resetEncodedKey();
        }
    }

    public TextInputStruct getTextInput()
    {
        return this.textInput;
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if(textInput == null) {
            this.textInput = null;
            log.log(Level.INFO, "RssChannelDataObject.setTextInput(TextInputStruct textInput): Arg textInput is null.");            
        } else if(textInput instanceof TextInputStructDataObject) {
            this.textInput = (TextInputStructDataObject) textInput;
        } else if(textInput instanceof TextInputStruct) {
            this.textInput = new TextInputStructDataObject(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            this.textInput = new TextInputStructDataObject();   // ????
            log.log(Level.WARNING, "RssChannelDataObject.setTextInput(TextInputStruct textInput): Arg textInput is of an invalid type.");
        }
    }

    public List<Integer> getSkipHours()
    {
        return this.skipHours;   // ???
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        this.skipHours = skipHours;
    }

    public List<String> getSkipDays()
    {
        return this.skipDays;   // ???
    }
    public void setSkipDays(List<String> skipDays)
    {
        this.skipDays = skipDays;
    }

    @SuppressWarnings("unchecked")
    public List<RssItem> getItem()
    {         
        return (List<RssItem>) ((List<?>) this.item);
    }
    public void setItem(List<RssItem> item)
    {
        if(item != null) {
            List<RssItemDataObject> dataObj = new ArrayList<RssItemDataObject>();
            for(RssItem rssItem : item) {
                RssItemDataObject elem = null;
                if(rssItem instanceof RssItemDataObject) {
                    elem = (RssItemDataObject) rssItem;
                } else if(rssItem instanceof RssItem) {
                    elem = new RssItemDataObject(rssItem.getTitle(), rssItem.getLink(), rssItem.getDescription(), rssItem.getAuthor(), rssItem.getCategory(), rssItem.getComments(), rssItem.getEnclosure(), rssItem.getGuid(), rssItem.getPubDate(), rssItem.getSource());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.item = dataObj;
        } else {
            this.item = null;
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCopyright() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getManagingEditor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebMaster() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastBuildDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGenerator() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDocs() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCloud() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTtl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRating() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTextInput() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipHours() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipDays() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getItem() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("title", this.title);
        dataMap.put("link", this.link);
        dataMap.put("description", this.description);
        dataMap.put("language", this.language);
        dataMap.put("copyright", this.copyright);
        dataMap.put("managingEditor", this.managingEditor);
        dataMap.put("webMaster", this.webMaster);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("lastBuildDate", this.lastBuildDate);
        dataMap.put("category", this.category);
        dataMap.put("generator", this.generator);
        dataMap.put("docs", this.docs);
        dataMap.put("cloud", this.cloud);
        dataMap.put("ttl", this.ttl);
        dataMap.put("image", this.image);
        dataMap.put("rating", this.rating);
        dataMap.put("textInput", this.textInput);
        dataMap.put("skipHours", this.skipHours);
        dataMap.put("skipDays", this.skipDays);
        dataMap.put("item", this.item);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RssChannel thatObj = (RssChannel) obj;
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.link == null && thatObj.getLink() != null)
            || (this.link != null && thatObj.getLink() == null)
            || !this.link.equals(thatObj.getLink()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.language == null && thatObj.getLanguage() != null)
            || (this.language != null && thatObj.getLanguage() == null)
            || !this.language.equals(thatObj.getLanguage()) ) {
            return false;
        }
        if( (this.copyright == null && thatObj.getCopyright() != null)
            || (this.copyright != null && thatObj.getCopyright() == null)
            || !this.copyright.equals(thatObj.getCopyright()) ) {
            return false;
        }
        if( (this.managingEditor == null && thatObj.getManagingEditor() != null)
            || (this.managingEditor != null && thatObj.getManagingEditor() == null)
            || !this.managingEditor.equals(thatObj.getManagingEditor()) ) {
            return false;
        }
        if( (this.webMaster == null && thatObj.getWebMaster() != null)
            || (this.webMaster != null && thatObj.getWebMaster() == null)
            || !this.webMaster.equals(thatObj.getWebMaster()) ) {
            return false;
        }
        if( (this.pubDate == null && thatObj.getPubDate() != null)
            || (this.pubDate != null && thatObj.getPubDate() == null)
            || !this.pubDate.equals(thatObj.getPubDate()) ) {
            return false;
        }
        if( (this.lastBuildDate == null && thatObj.getLastBuildDate() != null)
            || (this.lastBuildDate != null && thatObj.getLastBuildDate() == null)
            || !this.lastBuildDate.equals(thatObj.getLastBuildDate()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.generator == null && thatObj.getGenerator() != null)
            || (this.generator != null && thatObj.getGenerator() == null)
            || !this.generator.equals(thatObj.getGenerator()) ) {
            return false;
        }
        if( (this.docs == null && thatObj.getDocs() != null)
            || (this.docs != null && thatObj.getDocs() == null)
            || !this.docs.equals(thatObj.getDocs()) ) {
            return false;
        }
        if( (this.cloud == null && thatObj.getCloud() != null)
            || (this.cloud != null && thatObj.getCloud() == null)
            || !this.cloud.equals(thatObj.getCloud()) ) {
            return false;
        }
        if( (this.ttl == null && thatObj.getTtl() != null)
            || (this.ttl != null && thatObj.getTtl() == null)
            || !this.ttl.equals(thatObj.getTtl()) ) {
            return false;
        }
        if( (this.image == null && thatObj.getImage() != null)
            || (this.image != null && thatObj.getImage() == null)
            || !this.image.equals(thatObj.getImage()) ) {
            return false;
        }
        if( (this.rating == null && thatObj.getRating() != null)
            || (this.rating != null && thatObj.getRating() == null)
            || !this.rating.equals(thatObj.getRating()) ) {
            return false;
        }
        if( (this.textInput == null && thatObj.getTextInput() != null)
            || (this.textInput != null && thatObj.getTextInput() == null)
            || !this.textInput.equals(thatObj.getTextInput()) ) {
            return false;
        }
        if( (this.skipHours == null && thatObj.getSkipHours() != null)
            || (this.skipHours != null && thatObj.getSkipHours() == null)
            || !this.skipHours.equals(thatObj.getSkipHours()) ) {
            return false;
        }
        if( (this.skipDays == null && thatObj.getSkipDays() != null)
            || (this.skipDays != null && thatObj.getSkipDays() == null)
            || !this.skipDays.equals(thatObj.getSkipDays()) ) {
            return false;
        }
        if( (this.item == null && thatObj.getItem() != null)
            || (this.item != null && thatObj.getItem() == null)
            || !this.item.equals(thatObj.getItem()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = copyright == null ? 0 : copyright.hashCode();
        _hash = 31 * _hash + delta;
        delta = managingEditor == null ? 0 : managingEditor.hashCode();
        _hash = 31 * _hash + delta;
        delta = webMaster == null ? 0 : webMaster.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = generator == null ? 0 : generator.hashCode();
        _hash = 31 * _hash + delta;
        delta = docs == null ? 0 : docs.hashCode();
        _hash = 31 * _hash + delta;
        delta = cloud == null ? 0 : cloud.hashCode();
        _hash = 31 * _hash + delta;
        delta = ttl == null ? 0 : ttl.hashCode();
        _hash = 31 * _hash + delta;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = rating == null ? 0 : rating.hashCode();
        _hash = 31 * _hash + delta;
        delta = textInput == null ? 0 : textInput.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipHours == null ? 0 : skipHours.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipDays == null ? 0 : skipDays.hashCode();
        _hash = 31 * _hash + delta;
        delta = item == null ? 0 : item.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
