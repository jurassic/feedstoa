package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class UserStructDataObject implements UserStruct, Serializable
{
    private static final Logger log = Logger.getLogger(UserStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _userstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String name;

    @Persistent(defaultFetchGroup = "true")
    private String email;

    @Persistent(defaultFetchGroup = "true")
    private String url;

    public UserStructDataObject()
    {
        // ???
        // this(null, null, null, null);
    }
    public UserStructDataObject(String uuid, String name, String email, String url)
    {
        setUuid(uuid);
        setName(name);
        setEmail(email);
        setUrl(url);
    }

    private void resetEncodedKey()
    {
        if(_userstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _userstruct_encoded_key = KeyFactory.createKeyString(UserStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _userstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _userstruct_encoded_key = KeyFactory.createKeyString(UserStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
        if(this.name != null) {
            resetEncodedKey();
        }
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;
        if(this.email != null) {
            resetEncodedKey();
        }
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
        if(this.url != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmail() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("name", this.name);
        dataMap.put("email", this.email);
        dataMap.put("url", this.url);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UserStruct thatObj = (UserStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.name == null && thatObj.getName() != null)
            || (this.name != null && thatObj.getName() == null)
            || !this.name.equals(thatObj.getName()) ) {
            return false;
        }
        if( (this.email == null && thatObj.getEmail() != null)
            || (this.email != null && thatObj.getEmail() == null)
            || !this.email.equals(thatObj.getEmail()) ) {
            return false;
        }
        if( (this.url == null && thatObj.getUrl() != null)
            || (this.url != null && thatObj.getUrl() == null)
            || !this.url.equals(thatObj.getUrl()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = email == null ? 0 : email.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
