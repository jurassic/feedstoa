package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class CloudStructDataObject implements CloudStruct, Serializable
{
    private static final Logger log = Logger.getLogger(CloudStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _cloudstruct_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private Integer port;

    @Persistent(defaultFetchGroup = "true")
    private String path;

    @Persistent(defaultFetchGroup = "true")
    private String registerProcedure;

    @Persistent(defaultFetchGroup = "true")
    private String protocol;

    public CloudStructDataObject()
    {
        // ???
        // this(null, null, null, null, null);
    }
    public CloudStructDataObject(String domain, Integer port, String path, String registerProcedure, String protocol)
    {
        setDomain(domain);
        setPort(port);
        setPath(path);
        setRegisterProcedure(registerProcedure);
        setProtocol(protocol);
    }

    private void resetEncodedKey()
    {
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
        if(this.domain != null) {
            resetEncodedKey();
        }
    }

    public Integer getPort()
    {
        return this.port;
    }
    public void setPort(Integer port)
    {
        this.port = port;
        if(this.port != null) {
            resetEncodedKey();
        }
    }

    public String getPath()
    {
        return this.path;
    }
    public void setPath(String path)
    {
        this.path = path;
        if(this.path != null) {
            resetEncodedKey();
        }
    }

    public String getRegisterProcedure()
    {
        return this.registerProcedure;
    }
    public void setRegisterProcedure(String registerProcedure)
    {
        this.registerProcedure = registerProcedure;
        if(this.registerProcedure != null) {
            resetEncodedKey();
        }
    }

    public String getProtocol()
    {
        return this.protocol;
    }
    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
        if(this.protocol != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPort() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegisterProcedure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProtocol() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("domain", this.domain);
        dataMap.put("port", this.port);
        dataMap.put("path", this.path);
        dataMap.put("registerProcedure", this.registerProcedure);
        dataMap.put("protocol", this.protocol);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        CloudStruct thatObj = (CloudStruct) obj;
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.port == null && thatObj.getPort() != null)
            || (this.port != null && thatObj.getPort() == null)
            || !this.port.equals(thatObj.getPort()) ) {
            return false;
        }
        if( (this.path == null && thatObj.getPath() != null)
            || (this.path != null && thatObj.getPath() == null)
            || !this.path.equals(thatObj.getPath()) ) {
            return false;
        }
        if( (this.registerProcedure == null && thatObj.getRegisterProcedure() != null)
            || (this.registerProcedure != null && thatObj.getRegisterProcedure() == null)
            || !this.registerProcedure.equals(thatObj.getRegisterProcedure()) ) {
            return false;
        }
        if( (this.protocol == null && thatObj.getProtocol() != null)
            || (this.protocol != null && thatObj.getProtocol() == null)
            || !this.protocol.equals(thatObj.getProtocol()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = port == null ? 0 : port.hashCode();
        _hash = 31 * _hash + delta;
        delta = path == null ? 0 : path.hashCode();
        _hash = 31 * _hash + delta;
        delta = registerProcedure == null ? 0 : registerProcedure.hashCode();
        _hash = 31 * _hash + delta;
        delta = protocol == null ? 0 : protocol.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
