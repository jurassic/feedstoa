package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class RssItemDataObject implements RssItem, Serializable
{
    private static final Logger log = Logger.getLogger(RssItemDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _rssitem_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String link;

    @Persistent(defaultFetchGroup = "true")
    private Text description;

    @Persistent(defaultFetchGroup = "true")
    private String author;

    @Persistent(defaultFetchGroup = "false")
    private List<CategoryStructDataObject> category;

    @Persistent(defaultFetchGroup = "true")
    private String comments;

    @Persistent(defaultFetchGroup = "true")
    private EnclosureStructDataObject enclosure;

    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String pubDate;

    @Persistent(defaultFetchGroup = "true")
    private UriStructDataObject source;

    public RssItemDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null);
    }
    public RssItemDataObject(String title, String link, String description, String author, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String guid, String pubDate, UriStruct source)
    {
        setTitle(title);
        setLink(link);
        setDescription(description);
        setAuthor(author);
        setCategory(category);
        setComments(comments);
        if(enclosure != null) {
            this.enclosure = new EnclosureStructDataObject(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            this.enclosure = null;
        }
        setGuid(guid);
        setPubDate(pubDate);
        if(source != null) {
            this.source = new UriStructDataObject(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            this.source = null;
        }
    }

    private void resetEncodedKey()
    {
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
        if(this.title != null) {
            resetEncodedKey();
        }
    }

    public String getLink()
    {
        return this.link;
    }
    public void setLink(String link)
    {
        this.link = link;
        if(this.link != null) {
            resetEncodedKey();
        }
    }

    public String getDescription()
    {
        if(this.description == null) {
            return null;
        }    
        return this.description.getValue();
    }
    public void setDescription(String description)
    {
        if(description == null) {
            this.description = null;
        } else {
            this.description = new Text(description);
        }
    }

    public String getAuthor()
    {
        return this.author;
    }
    public void setAuthor(String author)
    {
        this.author = author;
        if(this.author != null) {
            resetEncodedKey();
        }
    }

    @SuppressWarnings("unchecked")
    public List<CategoryStruct> getCategory()
    {         
        return (List<CategoryStruct>) ((List<?>) this.category);
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            List<CategoryStructDataObject> dataObj = new ArrayList<CategoryStructDataObject>();
            for(CategoryStruct categoryStruct : category) {
                CategoryStructDataObject elem = null;
                if(categoryStruct instanceof CategoryStructDataObject) {
                    elem = (CategoryStructDataObject) categoryStruct;
                } else if(categoryStruct instanceof CategoryStruct) {
                    elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.category = dataObj;
        } else {
            this.category = null;
        }
    }

    public String getComments()
    {
        return this.comments;
    }
    public void setComments(String comments)
    {
        this.comments = comments;
        if(this.comments != null) {
            resetEncodedKey();
        }
    }

    public EnclosureStruct getEnclosure()
    {
        return this.enclosure;
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if(enclosure == null) {
            this.enclosure = null;
            log.log(Level.INFO, "RssItemDataObject.setEnclosure(EnclosureStruct enclosure): Arg enclosure is null.");            
        } else if(enclosure instanceof EnclosureStructDataObject) {
            this.enclosure = (EnclosureStructDataObject) enclosure;
        } else if(enclosure instanceof EnclosureStruct) {
            this.enclosure = new EnclosureStructDataObject(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            this.enclosure = new EnclosureStructDataObject();   // ????
            log.log(Level.WARNING, "RssItemDataObject.setEnclosure(EnclosureStruct enclosure): Arg enclosure is of an invalid type.");
        }
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
        if(this.guid != null) {
            resetEncodedKey();
        }
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
        if(this.pubDate != null) {
            resetEncodedKey();
        }
    }

    public UriStruct getSource()
    {
        return this.source;
    }
    public void setSource(UriStruct source)
    {
        if(source == null) {
            this.source = null;
            log.log(Level.INFO, "RssItemDataObject.setSource(UriStruct source): Arg source is null.");            
        } else if(source instanceof UriStructDataObject) {
            this.source = (UriStructDataObject) source;
        } else if(source instanceof UriStruct) {
            this.source = new UriStructDataObject(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            this.source = new UriStructDataObject();   // ????
            log.log(Level.WARNING, "RssItemDataObject.setSource(UriStruct source): Arg source is of an invalid type.");
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAuthor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getComments() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEnclosure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGuid() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("title", this.title);
        dataMap.put("link", this.link);
        dataMap.put("description", this.description);
        dataMap.put("author", this.author);
        dataMap.put("category", this.category);
        dataMap.put("comments", this.comments);
        dataMap.put("enclosure", this.enclosure);
        dataMap.put("guid", this.guid);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("source", this.source);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RssItem thatObj = (RssItem) obj;
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.link == null && thatObj.getLink() != null)
            || (this.link != null && thatObj.getLink() == null)
            || !this.link.equals(thatObj.getLink()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.author == null && thatObj.getAuthor() != null)
            || (this.author != null && thatObj.getAuthor() == null)
            || !this.author.equals(thatObj.getAuthor()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.comments == null && thatObj.getComments() != null)
            || (this.comments != null && thatObj.getComments() == null)
            || !this.comments.equals(thatObj.getComments()) ) {
            return false;
        }
        if( (this.enclosure == null && thatObj.getEnclosure() != null)
            || (this.enclosure != null && thatObj.getEnclosure() == null)
            || !this.enclosure.equals(thatObj.getEnclosure()) ) {
            return false;
        }
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.pubDate == null && thatObj.getPubDate() != null)
            || (this.pubDate != null && thatObj.getPubDate() == null)
            || !this.pubDate.equals(thatObj.getPubDate()) ) {
            return false;
        }
        if( (this.source == null && thatObj.getSource() != null)
            || (this.source != null && thatObj.getSource() == null)
            || !this.source.equals(thatObj.getSource()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = author == null ? 0 : author.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = comments == null ? 0 : comments.hashCode();
        _hash = 31 * _hash + delta;
        delta = enclosure == null ? 0 : enclosure.hashCode();
        _hash = 31 * _hash + delta;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
