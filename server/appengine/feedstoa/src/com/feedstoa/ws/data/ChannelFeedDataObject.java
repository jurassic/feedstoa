package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ChannelFeedDataObject extends KeyedDataObject implements ChannelFeed
{
    private static final Logger log = Logger.getLogger(ChannelFeedDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ChannelFeedDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ChannelFeedDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String channelSource;

    @Persistent(defaultFetchGroup = "true")
    private String channelCode;

    @Persistent(defaultFetchGroup = "true")
    private String previousVersion;

    @Persistent(defaultFetchGroup = "true")
    private String fetchRequest;

    @Persistent(defaultFetchGroup = "true")
    private String fetchUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedServiceUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedFormat;

    @Persistent(defaultFetchGroup = "true")
    private Integer maxItemCount;

    @Persistent(defaultFetchGroup = "true")
    private String feedCategory;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String subtitle;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="linkuuid")),
        @Persistent(name="href", columns=@Column(name="linkhref")),
        @Persistent(name="rel", columns=@Column(name="linkrel")),
        @Persistent(name="type", columns=@Column(name="linktype")),
        @Persistent(name="label", columns=@Column(name="linklabel")),
    })
    private UriStructDataObject link;

    @Persistent(defaultFetchGroup = "true")
    private Text description;

    @Persistent(defaultFetchGroup = "true")
    private String language;

    @Persistent(defaultFetchGroup = "true")
    private String copyright;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="managingEditoruuid")),
        @Persistent(name="name", columns=@Column(name="managingEditorname")),
        @Persistent(name="email", columns=@Column(name="managingEditoremail")),
        @Persistent(name="url", columns=@Column(name="managingEditorurl")),
    })
    private UserStructDataObject managingEditor;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="webMasteruuid")),
        @Persistent(name="name", columns=@Column(name="webMastername")),
        @Persistent(name="email", columns=@Column(name="webMasteremail")),
        @Persistent(name="url", columns=@Column(name="webMasterurl")),
    })
    private UserStructDataObject webMaster;

    @Persistent(defaultFetchGroup = "false")
    private List<UserStructDataObject> contributor;

    @Persistent(defaultFetchGroup = "true")
    private String pubDate;

    @Persistent(defaultFetchGroup = "true")
    private String lastBuildDate;

    @Persistent(defaultFetchGroup = "false")
    private List<CategoryStructDataObject> category;

    @Persistent(defaultFetchGroup = "true")
    private String generator;

    @Persistent(defaultFetchGroup = "true")
    private String docs;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="domain", columns=@Column(name="clouddomain")),
        @Persistent(name="port", columns=@Column(name="cloudport")),
        @Persistent(name="path", columns=@Column(name="cloudpath")),
        @Persistent(name="registerProcedure", columns=@Column(name="cloudregisterProcedure")),
        @Persistent(name="protocol", columns=@Column(name="cloudprotocol")),
    })
    private CloudStructDataObject cloud;

    @Persistent(defaultFetchGroup = "true")
    private Integer ttl;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="url", columns=@Column(name="logourl")),
        @Persistent(name="title", columns=@Column(name="logotitle")),
        @Persistent(name="link", columns=@Column(name="logolink")),
        @Persistent(name="width", columns=@Column(name="logowidth")),
        @Persistent(name="height", columns=@Column(name="logoheight")),
        @Persistent(name="description", columns=@Column(name="logodescription")),
    })
    private ImageStructDataObject logo;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="url", columns=@Column(name="iconurl")),
        @Persistent(name="title", columns=@Column(name="icontitle")),
        @Persistent(name="link", columns=@Column(name="iconlink")),
        @Persistent(name="width", columns=@Column(name="iconwidth")),
        @Persistent(name="height", columns=@Column(name="iconheight")),
        @Persistent(name="description", columns=@Column(name="icondescription")),
    })
    private ImageStructDataObject icon;

    @Persistent(defaultFetchGroup = "true")
    private String rating;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="title", columns=@Column(name="textInputtitle")),
        @Persistent(name="name", columns=@Column(name="textInputname")),
        @Persistent(name="link", columns=@Column(name="textInputlink")),
        @Persistent(name="description", columns=@Column(name="textInputdescription")),
    })
    private TextInputStructDataObject textInput;

    @Persistent(defaultFetchGroup = "true")
    private List<Integer> skipHours;

    @Persistent(defaultFetchGroup = "true")
    private List<String> skipDays;

    @Persistent(defaultFetchGroup = "false")
    private Text outputText;

    @Persistent(defaultFetchGroup = "true")
    private String outputHash;

    @Persistent(defaultFetchGroup = "true")
    private String feedContent;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private Long lastBuildTime;

    @Persistent(defaultFetchGroup = "true")
    private Long publishedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastUpdatedTime;

    public ChannelFeedDataObject()
    {
        this(null);
    }
    public ChannelFeedDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ChannelFeedDataObject(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime)
    {
        this(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, link, description, language, copyright, managingEditor, webMaster, contributor, pubDate, lastBuildDate, category, generator, docs, cloud, ttl, logo, icon, rating, textInput, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfo, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime, null, null);
    }
    public ChannelFeedDataObject(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.channelSource = channelSource;
        this.channelCode = channelCode;
        this.previousVersion = previousVersion;
        this.fetchRequest = fetchRequest;
        this.fetchUrl = fetchUrl;
        this.feedServiceUrl = feedServiceUrl;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.maxItemCount = maxItemCount;
        this.feedCategory = feedCategory;
        this.title = title;
        this.subtitle = subtitle;
        if(link != null) {
            this.link = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            this.link = null;
        }
        setDescription(description);
        this.language = language;
        this.copyright = copyright;
        if(managingEditor != null) {
            this.managingEditor = new UserStructDataObject(managingEditor.getUuid(), managingEditor.getName(), managingEditor.getEmail(), managingEditor.getUrl());
        } else {
            this.managingEditor = null;
        }
        if(webMaster != null) {
            this.webMaster = new UserStructDataObject(webMaster.getUuid(), webMaster.getName(), webMaster.getEmail(), webMaster.getUrl());
        } else {
            this.webMaster = null;
        }
        setContributor(contributor);
        this.pubDate = pubDate;
        this.lastBuildDate = lastBuildDate;
        setCategory(category);
        this.generator = generator;
        this.docs = docs;
        if(cloud != null) {
            this.cloud = new CloudStructDataObject(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            this.cloud = null;
        }
        this.ttl = ttl;
        if(logo != null) {
            this.logo = new ImageStructDataObject(logo.getUrl(), logo.getTitle(), logo.getLink(), logo.getWidth(), logo.getHeight(), logo.getDescription());
        } else {
            this.logo = null;
        }
        if(icon != null) {
            this.icon = new ImageStructDataObject(icon.getUrl(), icon.getTitle(), icon.getLink(), icon.getWidth(), icon.getHeight(), icon.getDescription());
        } else {
            this.icon = null;
        }
        this.rating = rating;
        if(textInput != null) {
            this.textInput = new TextInputStructDataObject(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            this.textInput = null;
        }
        this.skipHours = skipHours;  // ???
        this.skipDays = skipDays;  // ???
        setOutputText(outputText);
        this.outputHash = outputHash;
        this.feedContent = feedContent;
        this.status = status;
        this.note = note;
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.lastBuildTime = lastBuildTime;
        this.publishedTime = publishedTime;
        this.expirationTime = expirationTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ChannelFeedDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ChannelFeedDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    public String getChannelCode()
    {
        return this.channelCode;
    }
    public void setChannelCode(String channelCode)
    {
        this.channelCode = channelCode;
    }

    public String getPreviousVersion()
    {
        return this.previousVersion;
    }
    public void setPreviousVersion(String previousVersion)
    {
        this.previousVersion = previousVersion;
    }

    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    public String getFeedServiceUrl()
    {
        return this.feedServiceUrl;
    }
    public void setFeedServiceUrl(String feedServiceUrl)
    {
        this.feedServiceUrl = feedServiceUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public Integer getMaxItemCount()
    {
        return this.maxItemCount;
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        this.maxItemCount = maxItemCount;
    }

    public String getFeedCategory()
    {
        return this.feedCategory;
    }
    public void setFeedCategory(String feedCategory)
    {
        this.feedCategory = feedCategory;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSubtitle()
    {
        return this.subtitle;
    }
    public void setSubtitle(String subtitle)
    {
        this.subtitle = subtitle;
    }

    public UriStruct getLink()
    {
        return this.link;
    }
    public void setLink(UriStruct link)
    {
        if(link == null) {
            this.link = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setLink(UriStruct link): Arg link is null.");            
        } else if(link instanceof UriStructDataObject) {
            this.link = (UriStructDataObject) link;
        } else if(link instanceof UriStruct) {
            this.link = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            this.link = new UriStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setLink(UriStruct link): Arg link is of an invalid type.");
        }
    }

    public String getDescription()
    {
        if(this.description == null) {
            return null;
        }    
        return this.description.getValue();
    }
    public void setDescription(String description)
    {
        if(description == null) {
            this.description = null;
        } else {
            this.description = new Text(description);
        }
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getCopyright()
    {
        return this.copyright;
    }
    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    public UserStruct getManagingEditor()
    {
        return this.managingEditor;
    }
    public void setManagingEditor(UserStruct managingEditor)
    {
        if(managingEditor == null) {
            this.managingEditor = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setManagingEditor(UserStruct managingEditor): Arg managingEditor is null.");            
        } else if(managingEditor instanceof UserStructDataObject) {
            this.managingEditor = (UserStructDataObject) managingEditor;
        } else if(managingEditor instanceof UserStruct) {
            this.managingEditor = new UserStructDataObject(managingEditor.getUuid(), managingEditor.getName(), managingEditor.getEmail(), managingEditor.getUrl());
        } else {
            this.managingEditor = new UserStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setManagingEditor(UserStruct managingEditor): Arg managingEditor is of an invalid type.");
        }
    }

    public UserStruct getWebMaster()
    {
        return this.webMaster;
    }
    public void setWebMaster(UserStruct webMaster)
    {
        if(webMaster == null) {
            this.webMaster = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setWebMaster(UserStruct webMaster): Arg webMaster is null.");            
        } else if(webMaster instanceof UserStructDataObject) {
            this.webMaster = (UserStructDataObject) webMaster;
        } else if(webMaster instanceof UserStruct) {
            this.webMaster = new UserStructDataObject(webMaster.getUuid(), webMaster.getName(), webMaster.getEmail(), webMaster.getUrl());
        } else {
            this.webMaster = new UserStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setWebMaster(UserStruct webMaster): Arg webMaster is of an invalid type.");
        }
    }

    @SuppressWarnings("unchecked")
    public List<UserStruct> getContributor()
    {         
        return (List<UserStruct>) ((List<?>) this.contributor);
    }
    public void setContributor(List<UserStruct> contributor)
    {
        if(contributor != null) {
            List<UserStructDataObject> dataObj = new ArrayList<UserStructDataObject>();
            for(UserStruct userStruct : contributor) {
                UserStructDataObject elem = null;
                if(userStruct instanceof UserStructDataObject) {
                    elem = (UserStructDataObject) userStruct;
                } else if(userStruct instanceof UserStruct) {
                    elem = new UserStructDataObject(userStruct.getUuid(), userStruct.getName(), userStruct.getEmail(), userStruct.getUrl());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.contributor = dataObj;
        } else {
            this.contributor = null;
        }
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    @SuppressWarnings("unchecked")
    public List<CategoryStruct> getCategory()
    {         
        return (List<CategoryStruct>) ((List<?>) this.category);
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            List<CategoryStructDataObject> dataObj = new ArrayList<CategoryStructDataObject>();
            for(CategoryStruct categoryStruct : category) {
                CategoryStructDataObject elem = null;
                if(categoryStruct instanceof CategoryStructDataObject) {
                    elem = (CategoryStructDataObject) categoryStruct;
                } else if(categoryStruct instanceof CategoryStruct) {
                    elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.category = dataObj;
        } else {
            this.category = null;
        }
    }

    public String getGenerator()
    {
        return this.generator;
    }
    public void setGenerator(String generator)
    {
        this.generator = generator;
    }

    public String getDocs()
    {
        return this.docs;
    }
    public void setDocs(String docs)
    {
        this.docs = docs;
    }

    public CloudStruct getCloud()
    {
        return this.cloud;
    }
    public void setCloud(CloudStruct cloud)
    {
        if(cloud == null) {
            this.cloud = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setCloud(CloudStruct cloud): Arg cloud is null.");            
        } else if(cloud instanceof CloudStructDataObject) {
            this.cloud = (CloudStructDataObject) cloud;
        } else if(cloud instanceof CloudStruct) {
            this.cloud = new CloudStructDataObject(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            this.cloud = new CloudStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setCloud(CloudStruct cloud): Arg cloud is of an invalid type.");
        }
    }

    public Integer getTtl()
    {
        return this.ttl;
    }
    public void setTtl(Integer ttl)
    {
        this.ttl = ttl;
    }

    public ImageStruct getLogo()
    {
        return this.logo;
    }
    public void setLogo(ImageStruct logo)
    {
        if(logo == null) {
            this.logo = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setLogo(ImageStruct logo): Arg logo is null.");            
        } else if(logo instanceof ImageStructDataObject) {
            this.logo = (ImageStructDataObject) logo;
        } else if(logo instanceof ImageStruct) {
            this.logo = new ImageStructDataObject(logo.getUrl(), logo.getTitle(), logo.getLink(), logo.getWidth(), logo.getHeight(), logo.getDescription());
        } else {
            this.logo = new ImageStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setLogo(ImageStruct logo): Arg logo is of an invalid type.");
        }
    }

    public ImageStruct getIcon()
    {
        return this.icon;
    }
    public void setIcon(ImageStruct icon)
    {
        if(icon == null) {
            this.icon = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setIcon(ImageStruct icon): Arg icon is null.");            
        } else if(icon instanceof ImageStructDataObject) {
            this.icon = (ImageStructDataObject) icon;
        } else if(icon instanceof ImageStruct) {
            this.icon = new ImageStructDataObject(icon.getUrl(), icon.getTitle(), icon.getLink(), icon.getWidth(), icon.getHeight(), icon.getDescription());
        } else {
            this.icon = new ImageStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setIcon(ImageStruct icon): Arg icon is of an invalid type.");
        }
    }

    public String getRating()
    {
        return this.rating;
    }
    public void setRating(String rating)
    {
        this.rating = rating;
    }

    public TextInputStruct getTextInput()
    {
        return this.textInput;
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if(textInput == null) {
            this.textInput = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setTextInput(TextInputStruct textInput): Arg textInput is null.");            
        } else if(textInput instanceof TextInputStructDataObject) {
            this.textInput = (TextInputStructDataObject) textInput;
        } else if(textInput instanceof TextInputStruct) {
            this.textInput = new TextInputStructDataObject(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            this.textInput = new TextInputStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setTextInput(TextInputStruct textInput): Arg textInput is of an invalid type.");
        }
    }

    public List<Integer> getSkipHours()
    {
        return this.skipHours;   // ???
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        this.skipHours = skipHours;
    }

    public List<String> getSkipDays()
    {
        return this.skipDays;   // ???
    }
    public void setSkipDays(List<String> skipDays)
    {
        this.skipDays = skipDays;
    }

    public String getOutputText()
    {
        if(this.outputText == null) {
            return null;
        }    
        return this.outputText.getValue();
    }
    public void setOutputText(String outputText)
    {
        if(outputText == null) {
            this.outputText = null;
        } else {
            this.outputText = new Text(outputText);
        }
    }

    public String getOutputHash()
    {
        return this.outputHash;
    }
    public void setOutputHash(String outputHash)
    {
        this.outputHash = outputHash;
    }

    public String getFeedContent()
    {
        return this.feedContent;
    }
    public void setFeedContent(String feedContent)
    {
        this.feedContent = feedContent;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "ChannelFeedDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "ChannelFeedDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public Long getLastBuildTime()
    {
        return this.lastBuildTime;
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        this.lastBuildTime = lastBuildTime;
    }

    public Long getPublishedTime()
    {
        return this.publishedTime;
    }
    public void setPublishedTime(Long publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("channelSource", this.channelSource);
        dataMap.put("channelCode", this.channelCode);
        dataMap.put("previousVersion", this.previousVersion);
        dataMap.put("fetchRequest", this.fetchRequest);
        dataMap.put("fetchUrl", this.fetchUrl);
        dataMap.put("feedServiceUrl", this.feedServiceUrl);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("maxItemCount", this.maxItemCount);
        dataMap.put("feedCategory", this.feedCategory);
        dataMap.put("title", this.title);
        dataMap.put("subtitle", this.subtitle);
        dataMap.put("link", this.link);
        dataMap.put("description", this.description);
        dataMap.put("language", this.language);
        dataMap.put("copyright", this.copyright);
        dataMap.put("managingEditor", this.managingEditor);
        dataMap.put("webMaster", this.webMaster);
        dataMap.put("contributor", this.contributor);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("lastBuildDate", this.lastBuildDate);
        dataMap.put("category", this.category);
        dataMap.put("generator", this.generator);
        dataMap.put("docs", this.docs);
        dataMap.put("cloud", this.cloud);
        dataMap.put("ttl", this.ttl);
        dataMap.put("logo", this.logo);
        dataMap.put("icon", this.icon);
        dataMap.put("rating", this.rating);
        dataMap.put("textInput", this.textInput);
        dataMap.put("skipHours", this.skipHours);
        dataMap.put("skipDays", this.skipDays);
        dataMap.put("outputText", this.outputText);
        dataMap.put("outputHash", this.outputHash);
        dataMap.put("feedContent", this.feedContent);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("lastBuildTime", this.lastBuildTime);
        dataMap.put("publishedTime", this.publishedTime);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ChannelFeed thatObj = (ChannelFeed) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.channelSource == null && thatObj.getChannelSource() != null)
            || (this.channelSource != null && thatObj.getChannelSource() == null)
            || !this.channelSource.equals(thatObj.getChannelSource()) ) {
            return false;
        }
        if( (this.channelCode == null && thatObj.getChannelCode() != null)
            || (this.channelCode != null && thatObj.getChannelCode() == null)
            || !this.channelCode.equals(thatObj.getChannelCode()) ) {
            return false;
        }
        if( (this.previousVersion == null && thatObj.getPreviousVersion() != null)
            || (this.previousVersion != null && thatObj.getPreviousVersion() == null)
            || !this.previousVersion.equals(thatObj.getPreviousVersion()) ) {
            return false;
        }
        if( (this.fetchRequest == null && thatObj.getFetchRequest() != null)
            || (this.fetchRequest != null && thatObj.getFetchRequest() == null)
            || !this.fetchRequest.equals(thatObj.getFetchRequest()) ) {
            return false;
        }
        if( (this.fetchUrl == null && thatObj.getFetchUrl() != null)
            || (this.fetchUrl != null && thatObj.getFetchUrl() == null)
            || !this.fetchUrl.equals(thatObj.getFetchUrl()) ) {
            return false;
        }
        if( (this.feedServiceUrl == null && thatObj.getFeedServiceUrl() != null)
            || (this.feedServiceUrl != null && thatObj.getFeedServiceUrl() == null)
            || !this.feedServiceUrl.equals(thatObj.getFeedServiceUrl()) ) {
            return false;
        }
        if( (this.feedUrl == null && thatObj.getFeedUrl() != null)
            || (this.feedUrl != null && thatObj.getFeedUrl() == null)
            || !this.feedUrl.equals(thatObj.getFeedUrl()) ) {
            return false;
        }
        if( (this.feedFormat == null && thatObj.getFeedFormat() != null)
            || (this.feedFormat != null && thatObj.getFeedFormat() == null)
            || !this.feedFormat.equals(thatObj.getFeedFormat()) ) {
            return false;
        }
        if( (this.maxItemCount == null && thatObj.getMaxItemCount() != null)
            || (this.maxItemCount != null && thatObj.getMaxItemCount() == null)
            || !this.maxItemCount.equals(thatObj.getMaxItemCount()) ) {
            return false;
        }
        if( (this.feedCategory == null && thatObj.getFeedCategory() != null)
            || (this.feedCategory != null && thatObj.getFeedCategory() == null)
            || !this.feedCategory.equals(thatObj.getFeedCategory()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.subtitle == null && thatObj.getSubtitle() != null)
            || (this.subtitle != null && thatObj.getSubtitle() == null)
            || !this.subtitle.equals(thatObj.getSubtitle()) ) {
            return false;
        }
        if( (this.link == null && thatObj.getLink() != null)
            || (this.link != null && thatObj.getLink() == null)
            || !this.link.equals(thatObj.getLink()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.language == null && thatObj.getLanguage() != null)
            || (this.language != null && thatObj.getLanguage() == null)
            || !this.language.equals(thatObj.getLanguage()) ) {
            return false;
        }
        if( (this.copyright == null && thatObj.getCopyright() != null)
            || (this.copyright != null && thatObj.getCopyright() == null)
            || !this.copyright.equals(thatObj.getCopyright()) ) {
            return false;
        }
        if( (this.managingEditor == null && thatObj.getManagingEditor() != null)
            || (this.managingEditor != null && thatObj.getManagingEditor() == null)
            || !this.managingEditor.equals(thatObj.getManagingEditor()) ) {
            return false;
        }
        if( (this.webMaster == null && thatObj.getWebMaster() != null)
            || (this.webMaster != null && thatObj.getWebMaster() == null)
            || !this.webMaster.equals(thatObj.getWebMaster()) ) {
            return false;
        }
        if( (this.contributor == null && thatObj.getContributor() != null)
            || (this.contributor != null && thatObj.getContributor() == null)
            || !this.contributor.equals(thatObj.getContributor()) ) {
            return false;
        }
        if( (this.pubDate == null && thatObj.getPubDate() != null)
            || (this.pubDate != null && thatObj.getPubDate() == null)
            || !this.pubDate.equals(thatObj.getPubDate()) ) {
            return false;
        }
        if( (this.lastBuildDate == null && thatObj.getLastBuildDate() != null)
            || (this.lastBuildDate != null && thatObj.getLastBuildDate() == null)
            || !this.lastBuildDate.equals(thatObj.getLastBuildDate()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.generator == null && thatObj.getGenerator() != null)
            || (this.generator != null && thatObj.getGenerator() == null)
            || !this.generator.equals(thatObj.getGenerator()) ) {
            return false;
        }
        if( (this.docs == null && thatObj.getDocs() != null)
            || (this.docs != null && thatObj.getDocs() == null)
            || !this.docs.equals(thatObj.getDocs()) ) {
            return false;
        }
        if( (this.cloud == null && thatObj.getCloud() != null)
            || (this.cloud != null && thatObj.getCloud() == null)
            || !this.cloud.equals(thatObj.getCloud()) ) {
            return false;
        }
        if( (this.ttl == null && thatObj.getTtl() != null)
            || (this.ttl != null && thatObj.getTtl() == null)
            || !this.ttl.equals(thatObj.getTtl()) ) {
            return false;
        }
        if( (this.logo == null && thatObj.getLogo() != null)
            || (this.logo != null && thatObj.getLogo() == null)
            || !this.logo.equals(thatObj.getLogo()) ) {
            return false;
        }
        if( (this.icon == null && thatObj.getIcon() != null)
            || (this.icon != null && thatObj.getIcon() == null)
            || !this.icon.equals(thatObj.getIcon()) ) {
            return false;
        }
        if( (this.rating == null && thatObj.getRating() != null)
            || (this.rating != null && thatObj.getRating() == null)
            || !this.rating.equals(thatObj.getRating()) ) {
            return false;
        }
        if( (this.textInput == null && thatObj.getTextInput() != null)
            || (this.textInput != null && thatObj.getTextInput() == null)
            || !this.textInput.equals(thatObj.getTextInput()) ) {
            return false;
        }
        if( (this.skipHours == null && thatObj.getSkipHours() != null)
            || (this.skipHours != null && thatObj.getSkipHours() == null)
            || !this.skipHours.equals(thatObj.getSkipHours()) ) {
            return false;
        }
        if( (this.skipDays == null && thatObj.getSkipDays() != null)
            || (this.skipDays != null && thatObj.getSkipDays() == null)
            || !this.skipDays.equals(thatObj.getSkipDays()) ) {
            return false;
        }
        if( (this.outputText == null && thatObj.getOutputText() != null)
            || (this.outputText != null && thatObj.getOutputText() == null)
            || !this.outputText.equals(thatObj.getOutputText()) ) {
            return false;
        }
        if( (this.outputHash == null && thatObj.getOutputHash() != null)
            || (this.outputHash != null && thatObj.getOutputHash() == null)
            || !this.outputHash.equals(thatObj.getOutputHash()) ) {
            return false;
        }
        if( (this.feedContent == null && thatObj.getFeedContent() != null)
            || (this.feedContent != null && thatObj.getFeedContent() == null)
            || !this.feedContent.equals(thatObj.getFeedContent()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.lastBuildTime == null && thatObj.getLastBuildTime() != null)
            || (this.lastBuildTime != null && thatObj.getLastBuildTime() == null)
            || !this.lastBuildTime.equals(thatObj.getLastBuildTime()) ) {
            return false;
        }
        if( (this.publishedTime == null && thatObj.getPublishedTime() != null)
            || (this.publishedTime != null && thatObj.getPublishedTime() == null)
            || !this.publishedTime.equals(thatObj.getPublishedTime()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }
        if( (this.lastUpdatedTime == null && thatObj.getLastUpdatedTime() != null)
            || (this.lastUpdatedTime != null && thatObj.getLastUpdatedTime() == null)
            || !this.lastUpdatedTime.equals(thatObj.getLastUpdatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelSource == null ? 0 : channelSource.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelCode == null ? 0 : channelCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = previousVersion == null ? 0 : previousVersion.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedServiceUrl == null ? 0 : feedServiceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = maxItemCount == null ? 0 : maxItemCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedCategory == null ? 0 : feedCategory.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = subtitle == null ? 0 : subtitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = copyright == null ? 0 : copyright.hashCode();
        _hash = 31 * _hash + delta;
        delta = managingEditor == null ? 0 : managingEditor.hashCode();
        _hash = 31 * _hash + delta;
        delta = webMaster == null ? 0 : webMaster.hashCode();
        _hash = 31 * _hash + delta;
        delta = contributor == null ? 0 : contributor.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = generator == null ? 0 : generator.hashCode();
        _hash = 31 * _hash + delta;
        delta = docs == null ? 0 : docs.hashCode();
        _hash = 31 * _hash + delta;
        delta = cloud == null ? 0 : cloud.hashCode();
        _hash = 31 * _hash + delta;
        delta = ttl == null ? 0 : ttl.hashCode();
        _hash = 31 * _hash + delta;
        delta = logo == null ? 0 : logo.hashCode();
        _hash = 31 * _hash + delta;
        delta = icon == null ? 0 : icon.hashCode();
        _hash = 31 * _hash + delta;
        delta = rating == null ? 0 : rating.hashCode();
        _hash = 31 * _hash + delta;
        delta = textInput == null ? 0 : textInput.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipHours == null ? 0 : skipHours.hashCode();
        _hash = 31 * _hash + delta;
        delta = skipDays == null ? 0 : skipDays.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputText == null ? 0 : outputText.hashCode();
        _hash = 31 * _hash + delta;
        delta = outputHash == null ? 0 : outputHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedContent == null ? 0 : feedContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildTime == null ? 0 : lastBuildTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = publishedTime == null ? 0 : publishedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
