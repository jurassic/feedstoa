package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class FeedContentDataObject extends KeyedDataObject implements FeedContent
{
    private static final Logger log = Logger.getLogger(FeedContentDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FeedContentDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FeedContentDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String channelSource;

    @Persistent(defaultFetchGroup = "true")
    private String channelFeed;

    @Persistent(defaultFetchGroup = "true")
    private String channelVersion;

    @Persistent(defaultFetchGroup = "true")
    private String feedUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedFormat;

    @Persistent(defaultFetchGroup = "false")
    private Text content;

    @Persistent(defaultFetchGroup = "true")
    private String contentHash;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String lastBuildDate;

    @Persistent(defaultFetchGroup = "true")
    private Long lastBuildTime;

    public FeedContentDataObject()
    {
        this(null);
    }
    public FeedContentDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FeedContentDataObject(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime)
    {
        this(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime, null, null);
    }
    public FeedContentDataObject(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.channelSource = channelSource;
        this.channelFeed = channelFeed;
        this.channelVersion = channelVersion;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        setContent(content);
        this.contentHash = contentHash;
        this.status = status;
        this.note = note;
        this.lastBuildDate = lastBuildDate;
        this.lastBuildTime = lastBuildTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return FeedContentDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FeedContentDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    public String getChannelVersion()
    {
        return this.channelVersion;
    }
    public void setChannelVersion(String channelVersion)
    {
        this.channelVersion = channelVersion;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public String getContent()
    {
        if(this.content == null) {
            return null;
        }    
        return this.content.getValue();
    }
    public void setContent(String content)
    {
        if(content == null) {
            this.content = null;
        } else {
            this.content = new Text(content);
        }
    }

    public String getContentHash()
    {
        return this.contentHash;
    }
    public void setContentHash(String contentHash)
    {
        this.contentHash = contentHash;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getLastBuildDate()
    {
        return this.lastBuildDate;
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        this.lastBuildDate = lastBuildDate;
    }

    public Long getLastBuildTime()
    {
        return this.lastBuildTime;
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        this.lastBuildTime = lastBuildTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("channelSource", this.channelSource);
        dataMap.put("channelFeed", this.channelFeed);
        dataMap.put("channelVersion", this.channelVersion);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("content", this.content);
        dataMap.put("contentHash", this.contentHash);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("lastBuildDate", this.lastBuildDate);
        dataMap.put("lastBuildTime", this.lastBuildTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FeedContent thatObj = (FeedContent) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.channelSource == null && thatObj.getChannelSource() != null)
            || (this.channelSource != null && thatObj.getChannelSource() == null)
            || !this.channelSource.equals(thatObj.getChannelSource()) ) {
            return false;
        }
        if( (this.channelFeed == null && thatObj.getChannelFeed() != null)
            || (this.channelFeed != null && thatObj.getChannelFeed() == null)
            || !this.channelFeed.equals(thatObj.getChannelFeed()) ) {
            return false;
        }
        if( (this.channelVersion == null && thatObj.getChannelVersion() != null)
            || (this.channelVersion != null && thatObj.getChannelVersion() == null)
            || !this.channelVersion.equals(thatObj.getChannelVersion()) ) {
            return false;
        }
        if( (this.feedUrl == null && thatObj.getFeedUrl() != null)
            || (this.feedUrl != null && thatObj.getFeedUrl() == null)
            || !this.feedUrl.equals(thatObj.getFeedUrl()) ) {
            return false;
        }
        if( (this.feedFormat == null && thatObj.getFeedFormat() != null)
            || (this.feedFormat != null && thatObj.getFeedFormat() == null)
            || !this.feedFormat.equals(thatObj.getFeedFormat()) ) {
            return false;
        }
        if( (this.content == null && thatObj.getContent() != null)
            || (this.content != null && thatObj.getContent() == null)
            || !this.content.equals(thatObj.getContent()) ) {
            return false;
        }
        if( (this.contentHash == null && thatObj.getContentHash() != null)
            || (this.contentHash != null && thatObj.getContentHash() == null)
            || !this.contentHash.equals(thatObj.getContentHash()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.lastBuildDate == null && thatObj.getLastBuildDate() != null)
            || (this.lastBuildDate != null && thatObj.getLastBuildDate() == null)
            || !this.lastBuildDate.equals(thatObj.getLastBuildDate()) ) {
            return false;
        }
        if( (this.lastBuildTime == null && thatObj.getLastBuildTime() != null)
            || (this.lastBuildTime != null && thatObj.getLastBuildTime() == null)
            || !this.lastBuildTime.equals(thatObj.getLastBuildTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelSource == null ? 0 : channelSource.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelFeed == null ? 0 : channelFeed.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelVersion == null ? 0 : channelVersion.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentHash == null ? 0 : contentHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildDate == null ? 0 : lastBuildDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastBuildTime == null ? 0 : lastBuildTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
