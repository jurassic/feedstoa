package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class ChannelSourceDataObject extends KeyedDataObject implements ChannelSource
{
    private static final Logger log = Logger.getLogger(ChannelSourceDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ChannelSourceDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ChannelSourceDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String serviceName;

    @Persistent(defaultFetchGroup = "true")
    private String serviceUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedFormat;

    @Persistent(defaultFetchGroup = "true")
    private String channelTitle;

    @Persistent(defaultFetchGroup = "false")
    private Text channelDescription;

    @Persistent(defaultFetchGroup = "true")
    private String channelCategory;

    @Persistent(defaultFetchGroup = "true")
    private String channelUrl;

    @Persistent(defaultFetchGroup = "true")
    private String channelLogo;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public ChannelSourceDataObject()
    {
        this(null);
    }
    public ChannelSourceDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ChannelSourceDataObject(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note)
    {
        this(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note, null, null);
    }
    public ChannelSourceDataObject(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.serviceName = serviceName;
        this.serviceUrl = serviceUrl;
        this.feedUrl = feedUrl;
        this.feedFormat = feedFormat;
        this.channelTitle = channelTitle;
        setChannelDescription(channelDescription);
        this.channelCategory = channelCategory;
        this.channelUrl = channelUrl;
        this.channelLogo = channelLogo;
        this.status = status;
        this.note = note;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ChannelSourceDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ChannelSourceDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public String getChannelTitle()
    {
        return this.channelTitle;
    }
    public void setChannelTitle(String channelTitle)
    {
        this.channelTitle = channelTitle;
    }

    public String getChannelDescription()
    {
        if(this.channelDescription == null) {
            return null;
        }    
        return this.channelDescription.getValue();
    }
    public void setChannelDescription(String channelDescription)
    {
        if(channelDescription == null) {
            this.channelDescription = null;
        } else {
            this.channelDescription = new Text(channelDescription);
        }
    }

    public String getChannelCategory()
    {
        return this.channelCategory;
    }
    public void setChannelCategory(String channelCategory)
    {
        this.channelCategory = channelCategory;
    }

    public String getChannelUrl()
    {
        return this.channelUrl;
    }
    public void setChannelUrl(String channelUrl)
    {
        this.channelUrl = channelUrl;
    }

    public String getChannelLogo()
    {
        return this.channelLogo;
    }
    public void setChannelLogo(String channelLogo)
    {
        this.channelLogo = channelLogo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("serviceName", this.serviceName);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("channelTitle", this.channelTitle);
        dataMap.put("channelDescription", this.channelDescription);
        dataMap.put("channelCategory", this.channelCategory);
        dataMap.put("channelUrl", this.channelUrl);
        dataMap.put("channelLogo", this.channelLogo);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ChannelSource thatObj = (ChannelSource) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.serviceName == null && thatObj.getServiceName() != null)
            || (this.serviceName != null && thatObj.getServiceName() == null)
            || !this.serviceName.equals(thatObj.getServiceName()) ) {
            return false;
        }
        if( (this.serviceUrl == null && thatObj.getServiceUrl() != null)
            || (this.serviceUrl != null && thatObj.getServiceUrl() == null)
            || !this.serviceUrl.equals(thatObj.getServiceUrl()) ) {
            return false;
        }
        if( (this.feedUrl == null && thatObj.getFeedUrl() != null)
            || (this.feedUrl != null && thatObj.getFeedUrl() == null)
            || !this.feedUrl.equals(thatObj.getFeedUrl()) ) {
            return false;
        }
        if( (this.feedFormat == null && thatObj.getFeedFormat() != null)
            || (this.feedFormat != null && thatObj.getFeedFormat() == null)
            || !this.feedFormat.equals(thatObj.getFeedFormat()) ) {
            return false;
        }
        if( (this.channelTitle == null && thatObj.getChannelTitle() != null)
            || (this.channelTitle != null && thatObj.getChannelTitle() == null)
            || !this.channelTitle.equals(thatObj.getChannelTitle()) ) {
            return false;
        }
        if( (this.channelDescription == null && thatObj.getChannelDescription() != null)
            || (this.channelDescription != null && thatObj.getChannelDescription() == null)
            || !this.channelDescription.equals(thatObj.getChannelDescription()) ) {
            return false;
        }
        if( (this.channelCategory == null && thatObj.getChannelCategory() != null)
            || (this.channelCategory != null && thatObj.getChannelCategory() == null)
            || !this.channelCategory.equals(thatObj.getChannelCategory()) ) {
            return false;
        }
        if( (this.channelUrl == null && thatObj.getChannelUrl() != null)
            || (this.channelUrl != null && thatObj.getChannelUrl() == null)
            || !this.channelUrl.equals(thatObj.getChannelUrl()) ) {
            return false;
        }
        if( (this.channelLogo == null && thatObj.getChannelLogo() != null)
            || (this.channelLogo != null && thatObj.getChannelLogo() == null)
            || !this.channelLogo.equals(thatObj.getChannelLogo()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelTitle == null ? 0 : channelTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelDescription == null ? 0 : channelDescription.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelCategory == null ? 0 : channelCategory.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelUrl == null ? 0 : channelUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelLogo == null ? 0 : channelLogo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
