package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class NotificationStructDataObject implements NotificationStruct, Serializable
{
    private static final Logger log = Logger.getLogger(NotificationStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _notificationstruct_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String preferredMode;

    @Persistent(defaultFetchGroup = "true")
    private String mobileNumber;

    @Persistent(defaultFetchGroup = "true")
    private String emailAddress;

    @Persistent(defaultFetchGroup = "true")
    private String twitterUsername;

    @Persistent(defaultFetchGroup = "true")
    private Long facebookId;

    @Persistent(defaultFetchGroup = "true")
    private String linkedinId;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public NotificationStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null);
    }
    public NotificationStructDataObject(String preferredMode, String mobileNumber, String emailAddress, String twitterUsername, Long facebookId, String linkedinId, String note)
    {
        setPreferredMode(preferredMode);
        setMobileNumber(mobileNumber);
        setEmailAddress(emailAddress);
        setTwitterUsername(twitterUsername);
        setFacebookId(facebookId);
        setLinkedinId(linkedinId);
        setNote(note);
    }

    private void resetEncodedKey()
    {
    }

    public String getPreferredMode()
    {
        return this.preferredMode;
    }
    public void setPreferredMode(String preferredMode)
    {
        this.preferredMode = preferredMode;
        if(this.preferredMode != null) {
            resetEncodedKey();
        }
    }

    public String getMobileNumber()
    {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
        if(this.mobileNumber != null) {
            resetEncodedKey();
        }
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
        if(this.emailAddress != null) {
            resetEncodedKey();
        }
    }

    public String getTwitterUsername()
    {
        return this.twitterUsername;
    }
    public void setTwitterUsername(String twitterUsername)
    {
        this.twitterUsername = twitterUsername;
        if(this.twitterUsername != null) {
            resetEncodedKey();
        }
    }

    public Long getFacebookId()
    {
        return this.facebookId;
    }
    public void setFacebookId(Long facebookId)
    {
        this.facebookId = facebookId;
        if(this.facebookId != null) {
            resetEncodedKey();
        }
    }

    public String getLinkedinId()
    {
        return this.linkedinId;
    }
    public void setLinkedinId(String linkedinId)
    {
        this.linkedinId = linkedinId;
        if(this.linkedinId != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPreferredMode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMobileNumber() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEmailAddress() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterUsername() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLinkedinId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("preferredMode", this.preferredMode);
        dataMap.put("mobileNumber", this.mobileNumber);
        dataMap.put("emailAddress", this.emailAddress);
        dataMap.put("twitterUsername", this.twitterUsername);
        dataMap.put("facebookId", this.facebookId);
        dataMap.put("linkedinId", this.linkedinId);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        NotificationStruct thatObj = (NotificationStruct) obj;
        if( (this.preferredMode == null && thatObj.getPreferredMode() != null)
            || (this.preferredMode != null && thatObj.getPreferredMode() == null)
            || !this.preferredMode.equals(thatObj.getPreferredMode()) ) {
            return false;
        }
        if( (this.mobileNumber == null && thatObj.getMobileNumber() != null)
            || (this.mobileNumber != null && thatObj.getMobileNumber() == null)
            || !this.mobileNumber.equals(thatObj.getMobileNumber()) ) {
            return false;
        }
        if( (this.emailAddress == null && thatObj.getEmailAddress() != null)
            || (this.emailAddress != null && thatObj.getEmailAddress() == null)
            || !this.emailAddress.equals(thatObj.getEmailAddress()) ) {
            return false;
        }
        if( (this.twitterUsername == null && thatObj.getTwitterUsername() != null)
            || (this.twitterUsername != null && thatObj.getTwitterUsername() == null)
            || !this.twitterUsername.equals(thatObj.getTwitterUsername()) ) {
            return false;
        }
        if( (this.facebookId == null && thatObj.getFacebookId() != null)
            || (this.facebookId != null && thatObj.getFacebookId() == null)
            || !this.facebookId.equals(thatObj.getFacebookId()) ) {
            return false;
        }
        if( (this.linkedinId == null && thatObj.getLinkedinId() != null)
            || (this.linkedinId != null && thatObj.getLinkedinId() == null)
            || !this.linkedinId.equals(thatObj.getLinkedinId()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = preferredMode == null ? 0 : preferredMode.hashCode();
        _hash = 31 * _hash + delta;
        delta = mobileNumber == null ? 0 : mobileNumber.hashCode();
        _hash = 31 * _hash + delta;
        delta = emailAddress == null ? 0 : emailAddress.hashCode();
        _hash = 31 * _hash + delta;
        delta = twitterUsername == null ? 0 : twitterUsername.hashCode();
        _hash = 31 * _hash + delta;
        delta = facebookId == null ? 0 : facebookId.hashCode();
        _hash = 31 * _hash + delta;
        delta = linkedinId == null ? 0 : linkedinId.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
