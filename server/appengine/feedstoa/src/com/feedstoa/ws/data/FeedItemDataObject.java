package com.feedstoa.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.util.CommonUtil;
import com.feedstoa.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class FeedItemDataObject extends KeyedDataObject implements FeedItem
{
    private static final Logger log = Logger.getLogger(FeedItemDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FeedItemDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FeedItemDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String fetchRequest;

    @Persistent(defaultFetchGroup = "true")
    private String fetchUrl;

    @Persistent(defaultFetchGroup = "true")
    private String feedUrl;

    @Persistent(defaultFetchGroup = "true")
    private String channelSource;

    @Persistent(defaultFetchGroup = "true")
    private String channelFeed;

    @Persistent(defaultFetchGroup = "true")
    private String feedFormat;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="linkuuid")),
        @Persistent(name="href", columns=@Column(name="linkhref")),
        @Persistent(name="rel", columns=@Column(name="linkrel")),
        @Persistent(name="type", columns=@Column(name="linktype")),
        @Persistent(name="label", columns=@Column(name="linklabel")),
    })
    private UriStructDataObject link;

    @Persistent(defaultFetchGroup = "true")
    private String summary;

    @Persistent(defaultFetchGroup = "false")
    private Text content;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="authoruuid")),
        @Persistent(name="name", columns=@Column(name="authorname")),
        @Persistent(name="email", columns=@Column(name="authoremail")),
        @Persistent(name="url", columns=@Column(name="authorurl")),
    })
    private UserStructDataObject author;

    @Persistent(defaultFetchGroup = "false")
    private List<UserStructDataObject> contributor;

    @Persistent(defaultFetchGroup = "false")
    private List<CategoryStructDataObject> category;

    @Persistent(defaultFetchGroup = "true")
    private String comments;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="url", columns=@Column(name="enclosureurl")),
        @Persistent(name="length", columns=@Column(name="enclosurelength")),
        @Persistent(name="type", columns=@Column(name="enclosuretype")),
    })
    private EnclosureStructDataObject enclosure;

    @Persistent(defaultFetchGroup = "true")
    private String id;

    @Persistent(defaultFetchGroup = "true")
    private String pubDate;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="uuid", columns=@Column(name="sourceuuid")),
        @Persistent(name="href", columns=@Column(name="sourcehref")),
        @Persistent(name="rel", columns=@Column(name="sourcerel")),
        @Persistent(name="type", columns=@Column(name="sourcetype")),
        @Persistent(name="label", columns=@Column(name="sourcelabel")),
    })
    private UriStructDataObject source;

    @Persistent(defaultFetchGroup = "true")
    private String feedContent;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private Long publishedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastUpdatedTime;

    public FeedItemDataObject()
    {
        this(null);
    }
    public FeedItemDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FeedItemDataObject(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime)
    {
        this(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, link, summary, content, author, contributor, category, comments, enclosure, id, pubDate, source, feedContent, status, note, referrerInfo, publishedTime, lastUpdatedTime, null, null);
    }
    public FeedItemDataObject(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.fetchRequest = fetchRequest;
        this.fetchUrl = fetchUrl;
        this.feedUrl = feedUrl;
        this.channelSource = channelSource;
        this.channelFeed = channelFeed;
        this.feedFormat = feedFormat;
        this.title = title;
        if(link != null) {
            this.link = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            this.link = null;
        }
        this.summary = summary;
        setContent(content);
        if(author != null) {
            this.author = new UserStructDataObject(author.getUuid(), author.getName(), author.getEmail(), author.getUrl());
        } else {
            this.author = null;
        }
        setContributor(contributor);
        setCategory(category);
        this.comments = comments;
        if(enclosure != null) {
            this.enclosure = new EnclosureStructDataObject(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            this.enclosure = null;
        }
        this.id = id;
        this.pubDate = pubDate;
        if(source != null) {
            this.source = new UriStructDataObject(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            this.source = null;
        }
        this.feedContent = feedContent;
        this.status = status;
        this.note = note;
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.publishedTime = publishedTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return FeedItemDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FeedItemDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    public String getFetchUrl()
    {
        return this.fetchUrl;
    }
    public void setFetchUrl(String fetchUrl)
    {
        this.fetchUrl = fetchUrl;
    }

    public String getFeedUrl()
    {
        return this.feedUrl;
    }
    public void setFeedUrl(String feedUrl)
    {
        this.feedUrl = feedUrl;
    }

    public String getChannelSource()
    {
        return this.channelSource;
    }
    public void setChannelSource(String channelSource)
    {
        this.channelSource = channelSource;
    }

    public String getChannelFeed()
    {
        return this.channelFeed;
    }
    public void setChannelFeed(String channelFeed)
    {
        this.channelFeed = channelFeed;
    }

    public String getFeedFormat()
    {
        return this.feedFormat;
    }
    public void setFeedFormat(String feedFormat)
    {
        this.feedFormat = feedFormat;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public UriStruct getLink()
    {
        return this.link;
    }
    public void setLink(UriStruct link)
    {
        if(link == null) {
            this.link = null;
            log.log(Level.INFO, "FeedItemDataObject.setLink(UriStruct link): Arg link is null.");            
        } else if(link instanceof UriStructDataObject) {
            this.link = (UriStructDataObject) link;
        } else if(link instanceof UriStruct) {
            this.link = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            this.link = new UriStructDataObject();   // ????
            log.log(Level.WARNING, "FeedItemDataObject.setLink(UriStruct link): Arg link is of an invalid type.");
        }
    }

    public String getSummary()
    {
        return this.summary;
    }
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getContent()
    {
        if(this.content == null) {
            return null;
        }    
        return this.content.getValue();
    }
    public void setContent(String content)
    {
        if(content == null) {
            this.content = null;
        } else {
            this.content = new Text(content);
        }
    }

    public UserStruct getAuthor()
    {
        return this.author;
    }
    public void setAuthor(UserStruct author)
    {
        if(author == null) {
            this.author = null;
            log.log(Level.INFO, "FeedItemDataObject.setAuthor(UserStruct author): Arg author is null.");            
        } else if(author instanceof UserStructDataObject) {
            this.author = (UserStructDataObject) author;
        } else if(author instanceof UserStruct) {
            this.author = new UserStructDataObject(author.getUuid(), author.getName(), author.getEmail(), author.getUrl());
        } else {
            this.author = new UserStructDataObject();   // ????
            log.log(Level.WARNING, "FeedItemDataObject.setAuthor(UserStruct author): Arg author is of an invalid type.");
        }
    }

    @SuppressWarnings("unchecked")
    public List<UserStruct> getContributor()
    {         
        return (List<UserStruct>) ((List<?>) this.contributor);
    }
    public void setContributor(List<UserStruct> contributor)
    {
        if(contributor != null) {
            List<UserStructDataObject> dataObj = new ArrayList<UserStructDataObject>();
            for(UserStruct userStruct : contributor) {
                UserStructDataObject elem = null;
                if(userStruct instanceof UserStructDataObject) {
                    elem = (UserStructDataObject) userStruct;
                } else if(userStruct instanceof UserStruct) {
                    elem = new UserStructDataObject(userStruct.getUuid(), userStruct.getName(), userStruct.getEmail(), userStruct.getUrl());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.contributor = dataObj;
        } else {
            this.contributor = null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CategoryStruct> getCategory()
    {         
        return (List<CategoryStruct>) ((List<?>) this.category);
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            List<CategoryStructDataObject> dataObj = new ArrayList<CategoryStructDataObject>();
            for(CategoryStruct categoryStruct : category) {
                CategoryStructDataObject elem = null;
                if(categoryStruct instanceof CategoryStructDataObject) {
                    elem = (CategoryStructDataObject) categoryStruct;
                } else if(categoryStruct instanceof CategoryStruct) {
                    elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.category = dataObj;
        } else {
            this.category = null;
        }
    }

    public String getComments()
    {
        return this.comments;
    }
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public EnclosureStruct getEnclosure()
    {
        return this.enclosure;
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if(enclosure == null) {
            this.enclosure = null;
            log.log(Level.INFO, "FeedItemDataObject.setEnclosure(EnclosureStruct enclosure): Arg enclosure is null.");            
        } else if(enclosure instanceof EnclosureStructDataObject) {
            this.enclosure = (EnclosureStructDataObject) enclosure;
        } else if(enclosure instanceof EnclosureStruct) {
            this.enclosure = new EnclosureStructDataObject(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            this.enclosure = new EnclosureStructDataObject();   // ????
            log.log(Level.WARNING, "FeedItemDataObject.setEnclosure(EnclosureStruct enclosure): Arg enclosure is of an invalid type.");
        }
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getPubDate()
    {
        return this.pubDate;
    }
    public void setPubDate(String pubDate)
    {
        this.pubDate = pubDate;
    }

    public UriStruct getSource()
    {
        return this.source;
    }
    public void setSource(UriStruct source)
    {
        if(source == null) {
            this.source = null;
            log.log(Level.INFO, "FeedItemDataObject.setSource(UriStruct source): Arg source is null.");            
        } else if(source instanceof UriStructDataObject) {
            this.source = (UriStructDataObject) source;
        } else if(source instanceof UriStruct) {
            this.source = new UriStructDataObject(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            this.source = new UriStructDataObject();   // ????
            log.log(Level.WARNING, "FeedItemDataObject.setSource(UriStruct source): Arg source is of an invalid type.");
        }
    }

    public String getFeedContent()
    {
        return this.feedContent;
    }
    public void setFeedContent(String feedContent)
    {
        this.feedContent = feedContent;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "FeedItemDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "FeedItemDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public Long getPublishedTime()
    {
        return this.publishedTime;
    }
    public void setPublishedTime(Long publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("fetchRequest", this.fetchRequest);
        dataMap.put("fetchUrl", this.fetchUrl);
        dataMap.put("feedUrl", this.feedUrl);
        dataMap.put("channelSource", this.channelSource);
        dataMap.put("channelFeed", this.channelFeed);
        dataMap.put("feedFormat", this.feedFormat);
        dataMap.put("title", this.title);
        dataMap.put("link", this.link);
        dataMap.put("summary", this.summary);
        dataMap.put("content", this.content);
        dataMap.put("author", this.author);
        dataMap.put("contributor", this.contributor);
        dataMap.put("category", this.category);
        dataMap.put("comments", this.comments);
        dataMap.put("enclosure", this.enclosure);
        dataMap.put("id", this.id);
        dataMap.put("pubDate", this.pubDate);
        dataMap.put("source", this.source);
        dataMap.put("feedContent", this.feedContent);
        dataMap.put("status", this.status);
        dataMap.put("note", this.note);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("publishedTime", this.publishedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FeedItem thatObj = (FeedItem) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.fetchRequest == null && thatObj.getFetchRequest() != null)
            || (this.fetchRequest != null && thatObj.getFetchRequest() == null)
            || !this.fetchRequest.equals(thatObj.getFetchRequest()) ) {
            return false;
        }
        if( (this.fetchUrl == null && thatObj.getFetchUrl() != null)
            || (this.fetchUrl != null && thatObj.getFetchUrl() == null)
            || !this.fetchUrl.equals(thatObj.getFetchUrl()) ) {
            return false;
        }
        if( (this.feedUrl == null && thatObj.getFeedUrl() != null)
            || (this.feedUrl != null && thatObj.getFeedUrl() == null)
            || !this.feedUrl.equals(thatObj.getFeedUrl()) ) {
            return false;
        }
        if( (this.channelSource == null && thatObj.getChannelSource() != null)
            || (this.channelSource != null && thatObj.getChannelSource() == null)
            || !this.channelSource.equals(thatObj.getChannelSource()) ) {
            return false;
        }
        if( (this.channelFeed == null && thatObj.getChannelFeed() != null)
            || (this.channelFeed != null && thatObj.getChannelFeed() == null)
            || !this.channelFeed.equals(thatObj.getChannelFeed()) ) {
            return false;
        }
        if( (this.feedFormat == null && thatObj.getFeedFormat() != null)
            || (this.feedFormat != null && thatObj.getFeedFormat() == null)
            || !this.feedFormat.equals(thatObj.getFeedFormat()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.link == null && thatObj.getLink() != null)
            || (this.link != null && thatObj.getLink() == null)
            || !this.link.equals(thatObj.getLink()) ) {
            return false;
        }
        if( (this.summary == null && thatObj.getSummary() != null)
            || (this.summary != null && thatObj.getSummary() == null)
            || !this.summary.equals(thatObj.getSummary()) ) {
            return false;
        }
        if( (this.content == null && thatObj.getContent() != null)
            || (this.content != null && thatObj.getContent() == null)
            || !this.content.equals(thatObj.getContent()) ) {
            return false;
        }
        if( (this.author == null && thatObj.getAuthor() != null)
            || (this.author != null && thatObj.getAuthor() == null)
            || !this.author.equals(thatObj.getAuthor()) ) {
            return false;
        }
        if( (this.contributor == null && thatObj.getContributor() != null)
            || (this.contributor != null && thatObj.getContributor() == null)
            || !this.contributor.equals(thatObj.getContributor()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.comments == null && thatObj.getComments() != null)
            || (this.comments != null && thatObj.getComments() == null)
            || !this.comments.equals(thatObj.getComments()) ) {
            return false;
        }
        if( (this.enclosure == null && thatObj.getEnclosure() != null)
            || (this.enclosure != null && thatObj.getEnclosure() == null)
            || !this.enclosure.equals(thatObj.getEnclosure()) ) {
            return false;
        }
        if( (this.id == null && thatObj.getId() != null)
            || (this.id != null && thatObj.getId() == null)
            || !this.id.equals(thatObj.getId()) ) {
            return false;
        }
        if( (this.pubDate == null && thatObj.getPubDate() != null)
            || (this.pubDate != null && thatObj.getPubDate() == null)
            || !this.pubDate.equals(thatObj.getPubDate()) ) {
            return false;
        }
        if( (this.source == null && thatObj.getSource() != null)
            || (this.source != null && thatObj.getSource() == null)
            || !this.source.equals(thatObj.getSource()) ) {
            return false;
        }
        if( (this.feedContent == null && thatObj.getFeedContent() != null)
            || (this.feedContent != null && thatObj.getFeedContent() == null)
            || !this.feedContent.equals(thatObj.getFeedContent()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.publishedTime == null && thatObj.getPublishedTime() != null)
            || (this.publishedTime != null && thatObj.getPublishedTime() == null)
            || !this.publishedTime.equals(thatObj.getPublishedTime()) ) {
            return false;
        }
        if( (this.lastUpdatedTime == null && thatObj.getLastUpdatedTime() != null)
            || (this.lastUpdatedTime != null && thatObj.getLastUpdatedTime() == null)
            || !this.lastUpdatedTime.equals(thatObj.getLastUpdatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchUrl == null ? 0 : fetchUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedUrl == null ? 0 : feedUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelSource == null ? 0 : channelSource.hashCode();
        _hash = 31 * _hash + delta;
        delta = channelFeed == null ? 0 : channelFeed.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedFormat == null ? 0 : feedFormat.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = link == null ? 0 : link.hashCode();
        _hash = 31 * _hash + delta;
        delta = summary == null ? 0 : summary.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = author == null ? 0 : author.hashCode();
        _hash = 31 * _hash + delta;
        delta = contributor == null ? 0 : contributor.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = comments == null ? 0 : comments.hashCode();
        _hash = 31 * _hash + delta;
        delta = enclosure == null ? 0 : enclosure.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = pubDate == null ? 0 : pubDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        delta = feedContent == null ? 0 : feedContent.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = publishedTime == null ? 0 : publishedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
