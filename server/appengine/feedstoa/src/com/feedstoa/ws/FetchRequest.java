package com.feedstoa.ws;

import java.util.List;
import java.util.ArrayList;


public interface FetchRequest extends FetchBase
{
    String  getOriginFetch();
    String  getOutputFormat();
    Integer  getFetchStatus();
    String  getResult();
    String  getFeedCategory();
    Boolean  isMultipleFeedEnabled();
    Boolean  isDeferred();
    Boolean  isAlert();
    NotificationStruct  getNotificationPref();
    ReferrerInfoStruct  getReferrerInfo();
    Integer  getRefreshInterval();
    List<String>  getRefreshExpressions();
    String  getRefreshTimeZone();
    Integer  getCurrentRefreshCount();
    Integer  getMaxRefreshCount();
    Long  getRefreshExpirationTime();
    Long  getNextRefreshTime();
    Long  getLastUpdatedTime();
}
