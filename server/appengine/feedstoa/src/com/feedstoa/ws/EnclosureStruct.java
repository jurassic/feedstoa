package com.feedstoa.ws;



public interface EnclosureStruct 
{
    String  getUrl();
    Integer  getLength();
    String  getType();
    boolean isEmpty();
}
