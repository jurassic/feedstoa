package com.feedstoa.ws;

import java.util.List;
import java.util.ArrayList;


public interface RssItem 
{
    String  getTitle();
    String  getLink();
    String  getDescription();
    String  getAuthor();
    List<CategoryStruct>  getCategory();
    String  getComments();
    EnclosureStruct  getEnclosure();
    String  getGuid();
    String  getPubDate();
    UriStruct  getSource();
    boolean isEmpty();
}
