package com.feedstoa.ws;



public interface CloudStruct 
{
    String  getDomain();
    Integer  getPort();
    String  getPath();
    String  getRegisterProcedure();
    String  getProtocol();
    boolean isEmpty();
}
