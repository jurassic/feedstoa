package com.feedstoa.ws;



public interface KeyValueRelationStruct extends KeyValuePairStruct
{
    String  getRelation();
    boolean isEmpty();
}
