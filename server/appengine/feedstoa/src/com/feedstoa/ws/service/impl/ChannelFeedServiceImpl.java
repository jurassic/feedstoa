package com.feedstoa.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.bean.CloudStructBean;
import com.feedstoa.ws.bean.CategoryStructBean;
import com.feedstoa.ws.bean.ImageStructBean;
import com.feedstoa.ws.bean.UriStructBean;
import com.feedstoa.ws.bean.UserStructBean;
import com.feedstoa.ws.bean.ReferrerInfoStructBean;
import com.feedstoa.ws.bean.TextInputStructBean;
import com.feedstoa.ws.bean.ChannelFeedBean;
import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.data.CloudStructDataObject;
import com.feedstoa.ws.data.CategoryStructDataObject;
import com.feedstoa.ws.data.ImageStructDataObject;
import com.feedstoa.ws.data.UriStructDataObject;
import com.feedstoa.ws.data.UserStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.TextInputStructDataObject;
import com.feedstoa.ws.data.ChannelFeedDataObject;
import com.feedstoa.ws.service.DAOFactoryManager;
import com.feedstoa.ws.service.ChannelFeedService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ChannelFeedServiceImpl implements ChannelFeedService
{
    private static final Logger log = Logger.getLogger(ChannelFeedServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ChannelFeed related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ChannelFeed getChannelFeed(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ChannelFeedDataObject dataObj = getDAOFactory().getChannelFeedDAO().getChannelFeed(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelFeedDataObject for guid = " + guid);
            return null;  // ????
        }
        ChannelFeedBean bean = new ChannelFeedBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getChannelFeed(String guid, String field) throws BaseException
    {
        ChannelFeedDataObject dataObj = getDAOFactory().getChannelFeedDAO().getChannelFeed(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelFeedDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("channelSource")) {
            return dataObj.getChannelSource();
        } else if(field.equals("channelCode")) {
            return dataObj.getChannelCode();
        } else if(field.equals("previousVersion")) {
            return dataObj.getPreviousVersion();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("fetchUrl")) {
            return dataObj.getFetchUrl();
        } else if(field.equals("feedServiceUrl")) {
            return dataObj.getFeedServiceUrl();
        } else if(field.equals("feedUrl")) {
            return dataObj.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return dataObj.getFeedFormat();
        } else if(field.equals("maxItemCount")) {
            return dataObj.getMaxItemCount();
        } else if(field.equals("feedCategory")) {
            return dataObj.getFeedCategory();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("subtitle")) {
            return dataObj.getSubtitle();
        } else if(field.equals("link")) {
            return dataObj.getLink();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("copyright")) {
            return dataObj.getCopyright();
        } else if(field.equals("managingEditor")) {
            return dataObj.getManagingEditor();
        } else if(field.equals("webMaster")) {
            return dataObj.getWebMaster();
        } else if(field.equals("contributor")) {
            return dataObj.getContributor();
        } else if(field.equals("pubDate")) {
            return dataObj.getPubDate();
        } else if(field.equals("lastBuildDate")) {
            return dataObj.getLastBuildDate();
        } else if(field.equals("category")) {
            return dataObj.getCategory();
        } else if(field.equals("generator")) {
            return dataObj.getGenerator();
        } else if(field.equals("docs")) {
            return dataObj.getDocs();
        } else if(field.equals("cloud")) {
            return dataObj.getCloud();
        } else if(field.equals("ttl")) {
            return dataObj.getTtl();
        } else if(field.equals("logo")) {
            return dataObj.getLogo();
        } else if(field.equals("icon")) {
            return dataObj.getIcon();
        } else if(field.equals("rating")) {
            return dataObj.getRating();
        } else if(field.equals("textInput")) {
            return dataObj.getTextInput();
        } else if(field.equals("skipHours")) {
            return dataObj.getSkipHours();
        } else if(field.equals("skipDays")) {
            return dataObj.getSkipDays();
        } else if(field.equals("outputText")) {
            return dataObj.getOutputText();
        } else if(field.equals("outputHash")) {
            return dataObj.getOutputHash();
        } else if(field.equals("feedContent")) {
            return dataObj.getFeedContent();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("lastBuildTime")) {
            return dataObj.getLastBuildTime();
        } else if(field.equals("publishedTime")) {
            return dataObj.getPublishedTime();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ChannelFeed> getChannelFeeds(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ChannelFeed> list = new ArrayList<ChannelFeed>();
        List<ChannelFeedDataObject> dataObjs = getDAOFactory().getChannelFeedDAO().getChannelFeeds(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelFeedDataObject list.");
        } else {
            Iterator<ChannelFeedDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ChannelFeedDataObject dataObj = (ChannelFeedDataObject) it.next();
                list.add(new ChannelFeedBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds() throws BaseException
    {
        return getAllChannelFeeds(null, null, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeeds(ordering, offset, count, null);
    }

    @Override
    public List<ChannelFeed> getAllChannelFeeds(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllChannelFeeds(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelFeed> list = new ArrayList<ChannelFeed>();
        List<ChannelFeedDataObject> dataObjs = getDAOFactory().getChannelFeedDAO().getAllChannelFeeds(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelFeedDataObject list.");
        } else {
            Iterator<ChannelFeedDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ChannelFeedDataObject dataObj = (ChannelFeedDataObject) it.next();
                list.add(new ChannelFeedBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelFeedKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelFeedKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllChannelFeedKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getChannelFeedDAO().getAllChannelFeedKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelFeed key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ChannelFeed> findChannelFeeds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ChannelFeedServiceImpl.findChannelFeeds(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelFeed> list = new ArrayList<ChannelFeed>();
        List<ChannelFeedDataObject> dataObjs = getDAOFactory().getChannelFeedDAO().findChannelFeeds(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find channelFeeds for the given criterion.");
        } else {
            Iterator<ChannelFeedDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ChannelFeedDataObject dataObj = (ChannelFeedDataObject) it.next();
                list.add(new ChannelFeedBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelFeedKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ChannelFeedServiceImpl.findChannelFeedKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getChannelFeedDAO().findChannelFeedKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ChannelFeed keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ChannelFeedServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getChannelFeedDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createChannelFeed(String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        UriStructDataObject linkDobj = null;
        if(link instanceof UriStructBean) {
            linkDobj = ((UriStructBean) link).toDataObject();
        } else if(link instanceof UriStruct) {
            linkDobj = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkDobj = null;   // ????
        }
        UserStructDataObject managingEditorDobj = null;
        if(managingEditor instanceof UserStructBean) {
            managingEditorDobj = ((UserStructBean) managingEditor).toDataObject();
        } else if(managingEditor instanceof UserStruct) {
            managingEditorDobj = new UserStructDataObject(managingEditor.getUuid(), managingEditor.getName(), managingEditor.getEmail(), managingEditor.getUrl());
        } else {
            managingEditorDobj = null;   // ????
        }
        UserStructDataObject webMasterDobj = null;
        if(webMaster instanceof UserStructBean) {
            webMasterDobj = ((UserStructBean) webMaster).toDataObject();
        } else if(webMaster instanceof UserStruct) {
            webMasterDobj = new UserStructDataObject(webMaster.getUuid(), webMaster.getName(), webMaster.getEmail(), webMaster.getUrl());
        } else {
            webMasterDobj = null;   // ????
        }
        CloudStructDataObject cloudDobj = null;
        if(cloud instanceof CloudStructBean) {
            cloudDobj = ((CloudStructBean) cloud).toDataObject();
        } else if(cloud instanceof CloudStruct) {
            cloudDobj = new CloudStructDataObject(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            cloudDobj = null;   // ????
        }
        ImageStructDataObject logoDobj = null;
        if(logo instanceof ImageStructBean) {
            logoDobj = ((ImageStructBean) logo).toDataObject();
        } else if(logo instanceof ImageStruct) {
            logoDobj = new ImageStructDataObject(logo.getUrl(), logo.getTitle(), logo.getLink(), logo.getWidth(), logo.getHeight(), logo.getDescription());
        } else {
            logoDobj = null;   // ????
        }
        ImageStructDataObject iconDobj = null;
        if(icon instanceof ImageStructBean) {
            iconDobj = ((ImageStructBean) icon).toDataObject();
        } else if(icon instanceof ImageStruct) {
            iconDobj = new ImageStructDataObject(icon.getUrl(), icon.getTitle(), icon.getLink(), icon.getWidth(), icon.getHeight(), icon.getDescription());
        } else {
            iconDobj = null;   // ????
        }
        TextInputStructDataObject textInputDobj = null;
        if(textInput instanceof TextInputStructBean) {
            textInputDobj = ((TextInputStructBean) textInput).toDataObject();
        } else if(textInput instanceof TextInputStruct) {
            textInputDobj = new TextInputStructDataObject(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            textInputDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        ChannelFeedDataObject dataObj = new ChannelFeedDataObject(null, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, linkDobj, description, language, copyright, managingEditorDobj, webMasterDobj, contributor, pubDate, lastBuildDate, category, generator, docs, cloudDobj, ttl, logoDobj, iconDobj, rating, textInputDobj, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfoDobj, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return createChannelFeed(dataObj);
    }

    @Override
    public String createChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelFeed cannot be null.....
        if(channelFeed == null) {
            log.log(Level.INFO, "Param channelFeed is null!");
            throw new BadRequestException("Param channelFeed object is null!");
        }
        ChannelFeedDataObject dataObj = null;
        if(channelFeed instanceof ChannelFeedDataObject) {
            dataObj = (ChannelFeedDataObject) channelFeed;
        } else if(channelFeed instanceof ChannelFeedBean) {
            dataObj = ((ChannelFeedBean) channelFeed).toDataObject();
        } else {  // if(channelFeed instanceof ChannelFeed)
            //dataObj = new ChannelFeedDataObject(null, channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructDataObject) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructDataObject) channelFeed.getManagingEditor(), (UserStructDataObject) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructDataObject) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructDataObject) channelFeed.getLogo(), (ImageStructDataObject) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructDataObject) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructDataObject) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ChannelFeedDataObject(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), (UriStructDataObject) channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), (UserStructDataObject) channelFeed.getManagingEditor(), (UserStructDataObject) channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), (CloudStructDataObject) channelFeed.getCloud(), channelFeed.getTtl(), (ImageStructDataObject) channelFeed.getLogo(), (ImageStructDataObject) channelFeed.getIcon(), channelFeed.getRating(), (TextInputStructDataObject) channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), (ReferrerInfoStructDataObject) channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        }
        String guid = getDAOFactory().getChannelFeedDAO().createChannelFeed(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateChannelFeed(String guid, String user, String channelSource, String channelCode, String previousVersion, String fetchRequest, String fetchUrl, String feedServiceUrl, String feedUrl, String feedFormat, Integer maxItemCount, String feedCategory, String title, String subtitle, UriStruct link, String description, String language, String copyright, UserStruct managingEditor, UserStruct webMaster, List<UserStruct> contributor, String pubDate, String lastBuildDate, List<CategoryStruct> category, String generator, String docs, CloudStruct cloud, Integer ttl, ImageStruct logo, ImageStruct icon, String rating, TextInputStruct textInput, List<Integer> skipHours, List<String> skipDays, String outputText, String outputHash, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long lastBuildTime, Long publishedTime, Long expirationTime, Long lastUpdatedTime) throws BaseException
    {
        UriStructDataObject linkDobj = null;
        if(link instanceof UriStructBean) {
            linkDobj = ((UriStructBean) link).toDataObject();            
        } else if(link instanceof UriStruct) {
            linkDobj = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkDobj = null;   // ????
        }
        UserStructDataObject managingEditorDobj = null;
        if(managingEditor instanceof UserStructBean) {
            managingEditorDobj = ((UserStructBean) managingEditor).toDataObject();            
        } else if(managingEditor instanceof UserStruct) {
            managingEditorDobj = new UserStructDataObject(managingEditor.getUuid(), managingEditor.getName(), managingEditor.getEmail(), managingEditor.getUrl());
        } else {
            managingEditorDobj = null;   // ????
        }
        UserStructDataObject webMasterDobj = null;
        if(webMaster instanceof UserStructBean) {
            webMasterDobj = ((UserStructBean) webMaster).toDataObject();            
        } else if(webMaster instanceof UserStruct) {
            webMasterDobj = new UserStructDataObject(webMaster.getUuid(), webMaster.getName(), webMaster.getEmail(), webMaster.getUrl());
        } else {
            webMasterDobj = null;   // ????
        }
        CloudStructDataObject cloudDobj = null;
        if(cloud instanceof CloudStructBean) {
            cloudDobj = ((CloudStructBean) cloud).toDataObject();            
        } else if(cloud instanceof CloudStruct) {
            cloudDobj = new CloudStructDataObject(cloud.getDomain(), cloud.getPort(), cloud.getPath(), cloud.getRegisterProcedure(), cloud.getProtocol());
        } else {
            cloudDobj = null;   // ????
        }
        ImageStructDataObject logoDobj = null;
        if(logo instanceof ImageStructBean) {
            logoDobj = ((ImageStructBean) logo).toDataObject();            
        } else if(logo instanceof ImageStruct) {
            logoDobj = new ImageStructDataObject(logo.getUrl(), logo.getTitle(), logo.getLink(), logo.getWidth(), logo.getHeight(), logo.getDescription());
        } else {
            logoDobj = null;   // ????
        }
        ImageStructDataObject iconDobj = null;
        if(icon instanceof ImageStructBean) {
            iconDobj = ((ImageStructBean) icon).toDataObject();            
        } else if(icon instanceof ImageStruct) {
            iconDobj = new ImageStructDataObject(icon.getUrl(), icon.getTitle(), icon.getLink(), icon.getWidth(), icon.getHeight(), icon.getDescription());
        } else {
            iconDobj = null;   // ????
        }
        TextInputStructDataObject textInputDobj = null;
        if(textInput instanceof TextInputStructBean) {
            textInputDobj = ((TextInputStructBean) textInput).toDataObject();            
        } else if(textInput instanceof TextInputStruct) {
            textInputDobj = new TextInputStructDataObject(textInput.getTitle(), textInput.getName(), textInput.getLink(), textInput.getDescription());
        } else {
            textInputDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ChannelFeedDataObject dataObj = new ChannelFeedDataObject(guid, user, channelSource, channelCode, previousVersion, fetchRequest, fetchUrl, feedServiceUrl, feedUrl, feedFormat, maxItemCount, feedCategory, title, subtitle, linkDobj, description, language, copyright, managingEditorDobj, webMasterDobj, contributor, pubDate, lastBuildDate, category, generator, docs, cloudDobj, ttl, logoDobj, iconDobj, rating, textInputDobj, skipHours, skipDays, outputText, outputHash, feedContent, status, note, referrerInfoDobj, lastBuildTime, publishedTime, expirationTime, lastUpdatedTime);
        return updateChannelFeed(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelFeed cannot be null.....
        if(channelFeed == null || channelFeed.getGuid() == null) {
            log.log(Level.INFO, "Param channelFeed or its guid is null!");
            throw new BadRequestException("Param channelFeed object or its guid is null!");
        }
        ChannelFeedDataObject dataObj = null;
        if(channelFeed instanceof ChannelFeedDataObject) {
            dataObj = (ChannelFeedDataObject) channelFeed;
        } else if(channelFeed instanceof ChannelFeedBean) {
            dataObj = ((ChannelFeedBean) channelFeed).toDataObject();
        } else {  // if(channelFeed instanceof ChannelFeed)
            dataObj = new ChannelFeedDataObject(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), channelFeed.getManagingEditor(), channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), channelFeed.getCloud(), channelFeed.getTtl(), channelFeed.getLogo(), channelFeed.getIcon(), channelFeed.getRating(), channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getChannelFeedDAO().updateChannelFeed(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteChannelFeed(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getChannelFeedDAO().deleteChannelFeed(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteChannelFeed(ChannelFeed channelFeed) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelFeed cannot be null.....
        if(channelFeed == null || channelFeed.getGuid() == null) {
            log.log(Level.INFO, "Param channelFeed or its guid is null!");
            throw new BadRequestException("Param channelFeed object or its guid is null!");
        }
        ChannelFeedDataObject dataObj = null;
        if(channelFeed instanceof ChannelFeedDataObject) {
            dataObj = (ChannelFeedDataObject) channelFeed;
        } else if(channelFeed instanceof ChannelFeedBean) {
            dataObj = ((ChannelFeedBean) channelFeed).toDataObject();
        } else {  // if(channelFeed instanceof ChannelFeed)
            dataObj = new ChannelFeedDataObject(channelFeed.getGuid(), channelFeed.getUser(), channelFeed.getChannelSource(), channelFeed.getChannelCode(), channelFeed.getPreviousVersion(), channelFeed.getFetchRequest(), channelFeed.getFetchUrl(), channelFeed.getFeedServiceUrl(), channelFeed.getFeedUrl(), channelFeed.getFeedFormat(), channelFeed.getMaxItemCount(), channelFeed.getFeedCategory(), channelFeed.getTitle(), channelFeed.getSubtitle(), channelFeed.getLink(), channelFeed.getDescription(), channelFeed.getLanguage(), channelFeed.getCopyright(), channelFeed.getManagingEditor(), channelFeed.getWebMaster(), channelFeed.getContributor(), channelFeed.getPubDate(), channelFeed.getLastBuildDate(), channelFeed.getCategory(), channelFeed.getGenerator(), channelFeed.getDocs(), channelFeed.getCloud(), channelFeed.getTtl(), channelFeed.getLogo(), channelFeed.getIcon(), channelFeed.getRating(), channelFeed.getTextInput(), channelFeed.getSkipHours(), channelFeed.getSkipDays(), channelFeed.getOutputText(), channelFeed.getOutputHash(), channelFeed.getFeedContent(), channelFeed.getStatus(), channelFeed.getNote(), channelFeed.getReferrerInfo(), channelFeed.getLastBuildTime(), channelFeed.getPublishedTime(), channelFeed.getExpirationTime(), channelFeed.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getChannelFeedDAO().deleteChannelFeed(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteChannelFeeds(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getChannelFeedDAO().deleteChannelFeeds(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
