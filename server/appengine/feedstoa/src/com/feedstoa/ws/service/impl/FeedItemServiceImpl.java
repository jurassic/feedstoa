package com.feedstoa.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.bean.EnclosureStructBean;
import com.feedstoa.ws.bean.CategoryStructBean;
import com.feedstoa.ws.bean.UriStructBean;
import com.feedstoa.ws.bean.UserStructBean;
import com.feedstoa.ws.bean.ReferrerInfoStructBean;
import com.feedstoa.ws.bean.FeedItemBean;
import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.data.EnclosureStructDataObject;
import com.feedstoa.ws.data.CategoryStructDataObject;
import com.feedstoa.ws.data.UriStructDataObject;
import com.feedstoa.ws.data.UserStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.FeedItemDataObject;
import com.feedstoa.ws.service.DAOFactoryManager;
import com.feedstoa.ws.service.FeedItemService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FeedItemServiceImpl implements FeedItemService
{
    private static final Logger log = Logger.getLogger(FeedItemServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // FeedItem related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FeedItem getFeedItem(String guid) throws BaseException
    {
        log.finer("BEGIN");

        FeedItemDataObject dataObj = getDAOFactory().getFeedItemDAO().getFeedItem(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedItemDataObject for guid = " + guid);
            return null;  // ????
        }
        FeedItemBean bean = new FeedItemBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFeedItem(String guid, String field) throws BaseException
    {
        FeedItemDataObject dataObj = getDAOFactory().getFeedItemDAO().getFeedItem(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedItemDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("fetchUrl")) {
            return dataObj.getFetchUrl();
        } else if(field.equals("feedUrl")) {
            return dataObj.getFeedUrl();
        } else if(field.equals("channelSource")) {
            return dataObj.getChannelSource();
        } else if(field.equals("channelFeed")) {
            return dataObj.getChannelFeed();
        } else if(field.equals("feedFormat")) {
            return dataObj.getFeedFormat();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("link")) {
            return dataObj.getLink();
        } else if(field.equals("summary")) {
            return dataObj.getSummary();
        } else if(field.equals("content")) {
            return dataObj.getContent();
        } else if(field.equals("author")) {
            return dataObj.getAuthor();
        } else if(field.equals("contributor")) {
            return dataObj.getContributor();
        } else if(field.equals("category")) {
            return dataObj.getCategory();
        } else if(field.equals("comments")) {
            return dataObj.getComments();
        } else if(field.equals("enclosure")) {
            return dataObj.getEnclosure();
        } else if(field.equals("id")) {
            return dataObj.getId();
        } else if(field.equals("pubDate")) {
            return dataObj.getPubDate();
        } else if(field.equals("source")) {
            return dataObj.getSource();
        } else if(field.equals("feedContent")) {
            return dataObj.getFeedContent();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("publishedTime")) {
            return dataObj.getPublishedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FeedItem> getFeedItems(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<FeedItem> list = new ArrayList<FeedItem>();
        List<FeedItemDataObject> dataObjs = getDAOFactory().getFeedItemDAO().getFeedItems(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedItemDataObject list.");
        } else {
            Iterator<FeedItemDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FeedItemDataObject dataObj = (FeedItemDataObject) it.next();
                list.add(new FeedItemBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<FeedItem> getAllFeedItems() throws BaseException
    {
        return getAllFeedItems(null, null, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedItems(ordering, offset, count, null);
    }

    @Override
    public List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFeedItems(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedItem> list = new ArrayList<FeedItem>();
        List<FeedItemDataObject> dataObjs = getDAOFactory().getFeedItemDAO().getAllFeedItems(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedItemDataObject list.");
        } else {
            Iterator<FeedItemDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FeedItemDataObject dataObj = (FeedItemDataObject) it.next();
                list.add(new FeedItemBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedItemKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllFeedItemKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFeedItemDAO().getAllFeedItemKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedItem key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FeedItemServiceImpl.findFeedItems(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedItem> list = new ArrayList<FeedItem>();
        List<FeedItemDataObject> dataObjs = getDAOFactory().getFeedItemDAO().findFeedItems(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find feedItems for the given criterion.");
        } else {
            Iterator<FeedItemDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FeedItemDataObject dataObj = (FeedItemDataObject) it.next();
                list.add(new FeedItemBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FeedItemServiceImpl.findFeedItemKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFeedItemDAO().findFeedItemKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FeedItem keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FeedItemServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getFeedItemDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createFeedItem(String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        UriStructDataObject linkDobj = null;
        if(link instanceof UriStructBean) {
            linkDobj = ((UriStructBean) link).toDataObject();
        } else if(link instanceof UriStruct) {
            linkDobj = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkDobj = null;   // ????
        }
        UserStructDataObject authorDobj = null;
        if(author instanceof UserStructBean) {
            authorDobj = ((UserStructBean) author).toDataObject();
        } else if(author instanceof UserStruct) {
            authorDobj = new UserStructDataObject(author.getUuid(), author.getName(), author.getEmail(), author.getUrl());
        } else {
            authorDobj = null;   // ????
        }
        EnclosureStructDataObject enclosureDobj = null;
        if(enclosure instanceof EnclosureStructBean) {
            enclosureDobj = ((EnclosureStructBean) enclosure).toDataObject();
        } else if(enclosure instanceof EnclosureStruct) {
            enclosureDobj = new EnclosureStructDataObject(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            enclosureDobj = null;   // ????
        }
        UriStructDataObject sourceDobj = null;
        if(source instanceof UriStructBean) {
            sourceDobj = ((UriStructBean) source).toDataObject();
        } else if(source instanceof UriStruct) {
            sourceDobj = new UriStructDataObject(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            sourceDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        FeedItemDataObject dataObj = new FeedItemDataObject(null, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, linkDobj, summary, content, authorDobj, contributor, category, comments, enclosureDobj, id, pubDate, sourceDobj, feedContent, status, note, referrerInfoDobj, publishedTime, lastUpdatedTime);
        return createFeedItem(dataObj);
    }

    @Override
    public String createFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedItem cannot be null.....
        if(feedItem == null) {
            log.log(Level.INFO, "Param feedItem is null!");
            throw new BadRequestException("Param feedItem object is null!");
        }
        FeedItemDataObject dataObj = null;
        if(feedItem instanceof FeedItemDataObject) {
            dataObj = (FeedItemDataObject) feedItem;
        } else if(feedItem instanceof FeedItemBean) {
            dataObj = ((FeedItemBean) feedItem).toDataObject();
        } else {  // if(feedItem instanceof FeedItem)
            //dataObj = new FeedItemDataObject(null, feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructDataObject) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructDataObject) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructDataObject) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructDataObject) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructDataObject) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new FeedItemDataObject(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), (UriStructDataObject) feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), (UserStructDataObject) feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), (EnclosureStructDataObject) feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), (UriStructDataObject) feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), (ReferrerInfoStructDataObject) feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        }
        String guid = getDAOFactory().getFeedItemDAO().createFeedItem(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException
    {
        UriStructDataObject linkDobj = null;
        if(link instanceof UriStructBean) {
            linkDobj = ((UriStructBean) link).toDataObject();            
        } else if(link instanceof UriStruct) {
            linkDobj = new UriStructDataObject(link.getUuid(), link.getHref(), link.getRel(), link.getType(), link.getLabel());
        } else {
            linkDobj = null;   // ????
        }
        UserStructDataObject authorDobj = null;
        if(author instanceof UserStructBean) {
            authorDobj = ((UserStructBean) author).toDataObject();            
        } else if(author instanceof UserStruct) {
            authorDobj = new UserStructDataObject(author.getUuid(), author.getName(), author.getEmail(), author.getUrl());
        } else {
            authorDobj = null;   // ????
        }
        EnclosureStructDataObject enclosureDobj = null;
        if(enclosure instanceof EnclosureStructBean) {
            enclosureDobj = ((EnclosureStructBean) enclosure).toDataObject();            
        } else if(enclosure instanceof EnclosureStruct) {
            enclosureDobj = new EnclosureStructDataObject(enclosure.getUrl(), enclosure.getLength(), enclosure.getType());
        } else {
            enclosureDobj = null;   // ????
        }
        UriStructDataObject sourceDobj = null;
        if(source instanceof UriStructBean) {
            sourceDobj = ((UriStructBean) source).toDataObject();            
        } else if(source instanceof UriStruct) {
            sourceDobj = new UriStructDataObject(source.getUuid(), source.getHref(), source.getRel(), source.getType(), source.getLabel());
        } else {
            sourceDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FeedItemDataObject dataObj = new FeedItemDataObject(guid, user, fetchRequest, fetchUrl, feedUrl, channelSource, channelFeed, feedFormat, title, linkDobj, summary, content, authorDobj, contributor, category, comments, enclosureDobj, id, pubDate, sourceDobj, feedContent, status, note, referrerInfoDobj, publishedTime, lastUpdatedTime);
        return updateFeedItem(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedItem cannot be null.....
        if(feedItem == null || feedItem.getGuid() == null) {
            log.log(Level.INFO, "Param feedItem or its guid is null!");
            throw new BadRequestException("Param feedItem object or its guid is null!");
        }
        FeedItemDataObject dataObj = null;
        if(feedItem instanceof FeedItemDataObject) {
            dataObj = (FeedItemDataObject) feedItem;
        } else if(feedItem instanceof FeedItemBean) {
            dataObj = ((FeedItemBean) feedItem).toDataObject();
        } else {  // if(feedItem instanceof FeedItem)
            dataObj = new FeedItemDataObject(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getFeedItemDAO().updateFeedItem(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteFeedItem(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getFeedItemDAO().deleteFeedItem(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteFeedItem(FeedItem feedItem) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedItem cannot be null.....
        if(feedItem == null || feedItem.getGuid() == null) {
            log.log(Level.INFO, "Param feedItem or its guid is null!");
            throw new BadRequestException("Param feedItem object or its guid is null!");
        }
        FeedItemDataObject dataObj = null;
        if(feedItem instanceof FeedItemDataObject) {
            dataObj = (FeedItemDataObject) feedItem;
        } else if(feedItem instanceof FeedItemBean) {
            dataObj = ((FeedItemBean) feedItem).toDataObject();
        } else {  // if(feedItem instanceof FeedItem)
            dataObj = new FeedItemDataObject(feedItem.getGuid(), feedItem.getUser(), feedItem.getFetchRequest(), feedItem.getFetchUrl(), feedItem.getFeedUrl(), feedItem.getChannelSource(), feedItem.getChannelFeed(), feedItem.getFeedFormat(), feedItem.getTitle(), feedItem.getLink(), feedItem.getSummary(), feedItem.getContent(), feedItem.getAuthor(), feedItem.getContributor(), feedItem.getCategory(), feedItem.getComments(), feedItem.getEnclosure(), feedItem.getId(), feedItem.getPubDate(), feedItem.getSource(), feedItem.getFeedContent(), feedItem.getStatus(), feedItem.getNote(), feedItem.getReferrerInfo(), feedItem.getPublishedTime(), feedItem.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getFeedItemDAO().deleteFeedItem(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteFeedItems(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getFeedItemDAO().deleteFeedItems(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
