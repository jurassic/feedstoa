package com.feedstoa.ws.service;

import java.util.List;

import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface FeedContentService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    FeedContent getFeedContent(String guid) throws BaseException;
    Object getFeedContent(String guid, String field) throws BaseException;
    List<FeedContent> getFeedContents(List<String> guids) throws BaseException;
    List<FeedContent> getAllFeedContents() throws BaseException;
    /* @Deprecated */ List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count) throws BaseException;
    List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createFeedContent(String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException;
    //String createFeedContent(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FeedContent?)
    String createFeedContent(FeedContent feedContent) throws BaseException;          // Returns Guid.  (Return FeedContent?)
    Boolean updateFeedContent(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException;
    //Boolean updateFeedContent(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFeedContent(FeedContent feedContent) throws BaseException;
    Boolean deleteFeedContent(String guid) throws BaseException;
    Boolean deleteFeedContent(FeedContent feedContent) throws BaseException;
    Long deleteFeedContents(String filter, String params, List<String> values) throws BaseException;

//    Integer createFeedContents(List<FeedContent> feedContents) throws BaseException;
//    Boolean updateeFeedContents(List<FeedContent> feedContents) throws BaseException;

}
