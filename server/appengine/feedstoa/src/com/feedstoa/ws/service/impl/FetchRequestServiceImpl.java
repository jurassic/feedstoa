package com.feedstoa.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.bean.NotificationStructBean;
import com.feedstoa.ws.bean.GaeAppStructBean;
import com.feedstoa.ws.bean.ReferrerInfoStructBean;
import com.feedstoa.ws.bean.FetchRequestBean;
import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.data.NotificationStructDataObject;
import com.feedstoa.ws.data.GaeAppStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.FetchRequestDataObject;
import com.feedstoa.ws.service.DAOFactoryManager;
import com.feedstoa.ws.service.FetchRequestService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FetchRequestServiceImpl implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");

        FetchRequestDataObject dataObj = getDAOFactory().getFetchRequestDAO().getFetchRequest(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject for guid = " + guid);
            return null;  // ????
        }
        FetchRequestBean bean = new FetchRequestBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        FetchRequestDataObject dataObj = getDAOFactory().getFetchRequestDAO().getFetchRequest(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fetchUrl")) {
            return dataObj.getFetchUrl();
        } else if(field.equals("feedUrl")) {
            return dataObj.getFeedUrl();
        } else if(field.equals("channelFeed")) {
            return dataObj.getChannelFeed();
        } else if(field.equals("reuseChannel")) {
            return dataObj.isReuseChannel();
        } else if(field.equals("maxItemCount")) {
            return dataObj.getMaxItemCount();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("originFetch")) {
            return dataObj.getOriginFetch();
        } else if(field.equals("outputFormat")) {
            return dataObj.getOutputFormat();
        } else if(field.equals("fetchStatus")) {
            return dataObj.getFetchStatus();
        } else if(field.equals("result")) {
            return dataObj.getResult();
        } else if(field.equals("feedCategory")) {
            return dataObj.getFeedCategory();
        } else if(field.equals("multipleFeedEnabled")) {
            return dataObj.isMultipleFeedEnabled();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("alert")) {
            return dataObj.isAlert();
        } else if(field.equals("notificationPref")) {
            return dataObj.getNotificationPref();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("refreshExpressions")) {
            return dataObj.getRefreshExpressions();
        } else if(field.equals("refreshTimeZone")) {
            return dataObj.getRefreshTimeZone();
        } else if(field.equals("currentRefreshCount")) {
            return dataObj.getCurrentRefreshCount();
        } else if(field.equals("maxRefreshCount")) {
            return dataObj.getMaxRefreshCount();
        } else if(field.equals("refreshExpirationTime")) {
            return dataObj.getRefreshExpirationTime();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<FetchRequest> list = new ArrayList<FetchRequest>();
        List<FetchRequestDataObject> dataObjs = getDAOFactory().getFetchRequestDAO().getFetchRequests(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject list.");
        } else {
            Iterator<FetchRequestDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FetchRequestDataObject dataObj = (FetchRequestDataObject) it.next();
                list.add(new FetchRequestBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequests(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FetchRequest> list = new ArrayList<FetchRequest>();
        List<FetchRequestDataObject> dataObjs = getDAOFactory().getFetchRequestDAO().getAllFetchRequests(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject list.");
        } else {
            Iterator<FetchRequestDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FetchRequestDataObject dataObj = (FetchRequestDataObject) it.next();
                list.add(new FetchRequestBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllFetchRequestKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFetchRequestDAO().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequest key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FetchRequestServiceImpl.findFetchRequests(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FetchRequest> list = new ArrayList<FetchRequest>();
        List<FetchRequestDataObject> dataObjs = getDAOFactory().getFetchRequestDAO().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find fetchRequests for the given criterion.");
        } else {
            Iterator<FetchRequestDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FetchRequestDataObject dataObj = (FetchRequestDataObject) it.next();
                list.add(new FetchRequestBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FetchRequestServiceImpl.findFetchRequestKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFetchRequestDAO().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FetchRequest keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FetchRequestServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getFetchRequestDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createFetchRequest(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        NotificationStructDataObject notificationPrefDobj = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefDobj = ((NotificationStructBean) notificationPref).toDataObject();
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefDobj = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            notificationPrefDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        
        FetchRequestDataObject dataObj = new FetchRequestDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPrefDobj, referrerInfoDobj, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
        return createFetchRequest(dataObj);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null) {
            log.log(Level.INFO, "Param fetchRequest is null!");
            throw new BadRequestException("Param fetchRequest object is null!");
        }
        FetchRequestDataObject dataObj = null;
        if(fetchRequest instanceof FetchRequestDataObject) {
            dataObj = (FetchRequestDataObject) fetchRequest;
        } else if(fetchRequest instanceof FetchRequestBean) {
            dataObj = ((FetchRequestBean) fetchRequest).toDataObject();
        } else {  // if(fetchRequest instanceof FetchRequest)
            //dataObj = new FetchRequestDataObject(null, fetchRequest.getManagerApp(), fetchRequest.getAppAcl(), (GaeAppStructDataObject) fetchRequest.getGaeApp(), fetchRequest.getOwnerUser(), fetchRequest.getUserAcl(), fetchRequest.getUser(), fetchRequest.getTitle(), fetchRequest.getDescription(), fetchRequest.getFetchUrl(), fetchRequest.getFeedUrl(), fetchRequest.getChannelFeed(), fetchRequest.isReuseChannel(), fetchRequest.getMaxItemCount(), fetchRequest.getNote(), fetchRequest.getStatus(), fetchRequest.getOriginFetch(), fetchRequest.getOutputFormat(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getFeedCategory(), fetchRequest.isMultipleFeedEnabled(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructDataObject) fetchRequest.getNotificationPref(), (ReferrerInfoStructDataObject) fetchRequest.getReferrerInfo(), fetchRequest.getRefreshInterval(), fetchRequest.getRefreshExpressions(), fetchRequest.getRefreshTimeZone(), fetchRequest.getCurrentRefreshCount(), fetchRequest.getMaxRefreshCount(), fetchRequest.getRefreshExpirationTime(), fetchRequest.getNextRefreshTime(), fetchRequest.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new FetchRequestDataObject(fetchRequest.getGuid(), fetchRequest.getManagerApp(), fetchRequest.getAppAcl(), (GaeAppStructDataObject) fetchRequest.getGaeApp(), fetchRequest.getOwnerUser(), fetchRequest.getUserAcl(), fetchRequest.getUser(), fetchRequest.getTitle(), fetchRequest.getDescription(), fetchRequest.getFetchUrl(), fetchRequest.getFeedUrl(), fetchRequest.getChannelFeed(), fetchRequest.isReuseChannel(), fetchRequest.getMaxItemCount(), fetchRequest.getNote(), fetchRequest.getStatus(), fetchRequest.getOriginFetch(), fetchRequest.getOutputFormat(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getFeedCategory(), fetchRequest.isMultipleFeedEnabled(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructDataObject) fetchRequest.getNotificationPref(), (ReferrerInfoStructDataObject) fetchRequest.getReferrerInfo(), fetchRequest.getRefreshInterval(), fetchRequest.getRefreshExpressions(), fetchRequest.getRefreshTimeZone(), fetchRequest.getCurrentRefreshCount(), fetchRequest.getMaxRefreshCount(), fetchRequest.getRefreshExpirationTime(), fetchRequest.getNextRefreshTime(), fetchRequest.getLastUpdatedTime());
        }
        String guid = getDAOFactory().getFetchRequestDAO().createFetchRequest(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFetchRequest(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        NotificationStructDataObject notificationPrefDobj = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefDobj = ((NotificationStructBean) notificationPref).toDataObject();            
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefDobj = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            notificationPrefDobj = null;   // ????
        }
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FetchRequestDataObject dataObj = new FetchRequestDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, originFetch, outputFormat, fetchStatus, result, feedCategory, multipleFeedEnabled, deferred, alert, notificationPrefDobj, referrerInfoDobj, refreshInterval, refreshExpressions, refreshTimeZone, currentRefreshCount, maxRefreshCount, refreshExpirationTime, nextRefreshTime, lastUpdatedTime);
        return updateFetchRequest(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestDataObject dataObj = null;
        if(fetchRequest instanceof FetchRequestDataObject) {
            dataObj = (FetchRequestDataObject) fetchRequest;
        } else if(fetchRequest instanceof FetchRequestBean) {
            dataObj = ((FetchRequestBean) fetchRequest).toDataObject();
        } else {  // if(fetchRequest instanceof FetchRequest)
            dataObj = new FetchRequestDataObject(fetchRequest.getGuid(), fetchRequest.getManagerApp(), fetchRequest.getAppAcl(), fetchRequest.getGaeApp(), fetchRequest.getOwnerUser(), fetchRequest.getUserAcl(), fetchRequest.getUser(), fetchRequest.getTitle(), fetchRequest.getDescription(), fetchRequest.getFetchUrl(), fetchRequest.getFeedUrl(), fetchRequest.getChannelFeed(), fetchRequest.isReuseChannel(), fetchRequest.getMaxItemCount(), fetchRequest.getNote(), fetchRequest.getStatus(), fetchRequest.getOriginFetch(), fetchRequest.getOutputFormat(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getFeedCategory(), fetchRequest.isMultipleFeedEnabled(), fetchRequest.isDeferred(), fetchRequest.isAlert(), fetchRequest.getNotificationPref(), fetchRequest.getReferrerInfo(), fetchRequest.getRefreshInterval(), fetchRequest.getRefreshExpressions(), fetchRequest.getRefreshTimeZone(), fetchRequest.getCurrentRefreshCount(), fetchRequest.getMaxRefreshCount(), fetchRequest.getRefreshExpirationTime(), fetchRequest.getNextRefreshTime(), fetchRequest.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getFetchRequestDAO().updateFetchRequest(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getFetchRequestDAO().deleteFetchRequest(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestDataObject dataObj = null;
        if(fetchRequest instanceof FetchRequestDataObject) {
            dataObj = (FetchRequestDataObject) fetchRequest;
        } else if(fetchRequest instanceof FetchRequestBean) {
            dataObj = ((FetchRequestBean) fetchRequest).toDataObject();
        } else {  // if(fetchRequest instanceof FetchRequest)
            dataObj = new FetchRequestDataObject(fetchRequest.getGuid(), fetchRequest.getManagerApp(), fetchRequest.getAppAcl(), fetchRequest.getGaeApp(), fetchRequest.getOwnerUser(), fetchRequest.getUserAcl(), fetchRequest.getUser(), fetchRequest.getTitle(), fetchRequest.getDescription(), fetchRequest.getFetchUrl(), fetchRequest.getFeedUrl(), fetchRequest.getChannelFeed(), fetchRequest.isReuseChannel(), fetchRequest.getMaxItemCount(), fetchRequest.getNote(), fetchRequest.getStatus(), fetchRequest.getOriginFetch(), fetchRequest.getOutputFormat(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getFeedCategory(), fetchRequest.isMultipleFeedEnabled(), fetchRequest.isDeferred(), fetchRequest.isAlert(), fetchRequest.getNotificationPref(), fetchRequest.getReferrerInfo(), fetchRequest.getRefreshInterval(), fetchRequest.getRefreshExpressions(), fetchRequest.getRefreshTimeZone(), fetchRequest.getCurrentRefreshCount(), fetchRequest.getMaxRefreshCount(), fetchRequest.getRefreshExpirationTime(), fetchRequest.getNextRefreshTime(), fetchRequest.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getFetchRequestDAO().deleteFetchRequest(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getFetchRequestDAO().deleteFetchRequests(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
