package com.feedstoa.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.bean.FeedContentBean;
import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.data.FeedContentDataObject;
import com.feedstoa.ws.service.DAOFactoryManager;
import com.feedstoa.ws.service.FeedContentService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FeedContentServiceImpl implements FeedContentService
{
    private static final Logger log = Logger.getLogger(FeedContentServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // FeedContent related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FeedContent getFeedContent(String guid) throws BaseException
    {
        log.finer("BEGIN");

        FeedContentDataObject dataObj = getDAOFactory().getFeedContentDAO().getFeedContent(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedContentDataObject for guid = " + guid);
            return null;  // ????
        }
        FeedContentBean bean = new FeedContentBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFeedContent(String guid, String field) throws BaseException
    {
        FeedContentDataObject dataObj = getDAOFactory().getFeedContentDAO().getFeedContent(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FeedContentDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("channelSource")) {
            return dataObj.getChannelSource();
        } else if(field.equals("channelFeed")) {
            return dataObj.getChannelFeed();
        } else if(field.equals("channelVersion")) {
            return dataObj.getChannelVersion();
        } else if(field.equals("feedUrl")) {
            return dataObj.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return dataObj.getFeedFormat();
        } else if(field.equals("content")) {
            return dataObj.getContent();
        } else if(field.equals("contentHash")) {
            return dataObj.getContentHash();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("lastBuildDate")) {
            return dataObj.getLastBuildDate();
        } else if(field.equals("lastBuildTime")) {
            return dataObj.getLastBuildTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FeedContent> getFeedContents(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<FeedContent> list = new ArrayList<FeedContent>();
        List<FeedContentDataObject> dataObjs = getDAOFactory().getFeedContentDAO().getFeedContents(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedContentDataObject list.");
        } else {
            Iterator<FeedContentDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FeedContentDataObject dataObj = (FeedContentDataObject) it.next();
                list.add(new FeedContentBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<FeedContent> getAllFeedContents() throws BaseException
    {
        return getAllFeedContents(null, null, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedContents(ordering, offset, count, null);
    }

    @Override
    public List<FeedContent> getAllFeedContents(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFeedContents(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedContent> list = new ArrayList<FeedContent>();
        List<FeedContentDataObject> dataObjs = getDAOFactory().getFeedContentDAO().getAllFeedContents(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedContentDataObject list.");
        } else {
            Iterator<FeedContentDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FeedContentDataObject dataObj = (FeedContentDataObject) it.next();
                list.add(new FeedContentBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFeedContentKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFeedContentKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllFeedContentKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFeedContentDAO().getAllFeedContentKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FeedContent key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFeedContents(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<FeedContent> findFeedContents(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FeedContentServiceImpl.findFeedContents(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FeedContent> list = new ArrayList<FeedContent>();
        List<FeedContentDataObject> dataObjs = getDAOFactory().getFeedContentDAO().findFeedContents(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find feedContents for the given criterion.");
        } else {
            Iterator<FeedContentDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FeedContentDataObject dataObj = (FeedContentDataObject) it.next();
                list.add(new FeedContentBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFeedContentKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FeedContentServiceImpl.findFeedContentKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFeedContentDAO().findFeedContentKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FeedContent keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FeedContentServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getFeedContentDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createFeedContent(String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        FeedContentDataObject dataObj = new FeedContentDataObject(null, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        return createFeedContent(dataObj);
    }

    @Override
    public String createFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedContent cannot be null.....
        if(feedContent == null) {
            log.log(Level.INFO, "Param feedContent is null!");
            throw new BadRequestException("Param feedContent object is null!");
        }
        FeedContentDataObject dataObj = null;
        if(feedContent instanceof FeedContentDataObject) {
            dataObj = (FeedContentDataObject) feedContent;
        } else if(feedContent instanceof FeedContentBean) {
            dataObj = ((FeedContentBean) feedContent).toDataObject();
        } else {  // if(feedContent instanceof FeedContent)
            //dataObj = new FeedContentDataObject(null, feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new FeedContentDataObject(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        }
        String guid = getDAOFactory().getFeedContentDAO().createFeedContent(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFeedContent(String guid, String user, String channelSource, String channelFeed, String channelVersion, String feedUrl, String feedFormat, String content, String contentHash, String status, String note, String lastBuildDate, Long lastBuildTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FeedContentDataObject dataObj = new FeedContentDataObject(guid, user, channelSource, channelFeed, channelVersion, feedUrl, feedFormat, content, contentHash, status, note, lastBuildDate, lastBuildTime);
        return updateFeedContent(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedContent cannot be null.....
        if(feedContent == null || feedContent.getGuid() == null) {
            log.log(Level.INFO, "Param feedContent or its guid is null!");
            throw new BadRequestException("Param feedContent object or its guid is null!");
        }
        FeedContentDataObject dataObj = null;
        if(feedContent instanceof FeedContentDataObject) {
            dataObj = (FeedContentDataObject) feedContent;
        } else if(feedContent instanceof FeedContentBean) {
            dataObj = ((FeedContentBean) feedContent).toDataObject();
        } else {  // if(feedContent instanceof FeedContent)
            dataObj = new FeedContentDataObject(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        }
        Boolean suc = getDAOFactory().getFeedContentDAO().updateFeedContent(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteFeedContent(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getFeedContentDAO().deleteFeedContent(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteFeedContent(FeedContent feedContent) throws BaseException
    {
        log.finer("BEGIN");

        // Param feedContent cannot be null.....
        if(feedContent == null || feedContent.getGuid() == null) {
            log.log(Level.INFO, "Param feedContent or its guid is null!");
            throw new BadRequestException("Param feedContent object or its guid is null!");
        }
        FeedContentDataObject dataObj = null;
        if(feedContent instanceof FeedContentDataObject) {
            dataObj = (FeedContentDataObject) feedContent;
        } else if(feedContent instanceof FeedContentBean) {
            dataObj = ((FeedContentBean) feedContent).toDataObject();
        } else {  // if(feedContent instanceof FeedContent)
            dataObj = new FeedContentDataObject(feedContent.getGuid(), feedContent.getUser(), feedContent.getChannelSource(), feedContent.getChannelFeed(), feedContent.getChannelVersion(), feedContent.getFeedUrl(), feedContent.getFeedFormat(), feedContent.getContent(), feedContent.getContentHash(), feedContent.getStatus(), feedContent.getNote(), feedContent.getLastBuildDate(), feedContent.getLastBuildTime());
        }
        Boolean suc = getDAOFactory().getFeedContentDAO().deleteFeedContent(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteFeedContents(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getFeedContentDAO().deleteFeedContents(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
