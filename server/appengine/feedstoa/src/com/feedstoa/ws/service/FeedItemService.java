package com.feedstoa.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface FeedItemService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    FeedItem getFeedItem(String guid) throws BaseException;
    Object getFeedItem(String guid, String field) throws BaseException;
    List<FeedItem> getFeedItems(List<String> guids) throws BaseException;
    List<FeedItem> getAllFeedItems() throws BaseException;
    /* @Deprecated */ List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count) throws BaseException;
    List<FeedItem> getAllFeedItems(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFeedItemKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FeedItem> findFeedItems(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFeedItemKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createFeedItem(String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException;
    //String createFeedItem(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FeedItem?)
    String createFeedItem(FeedItem feedItem) throws BaseException;          // Returns Guid.  (Return FeedItem?)
    Boolean updateFeedItem(String guid, String user, String fetchRequest, String fetchUrl, String feedUrl, String channelSource, String channelFeed, String feedFormat, String title, UriStruct link, String summary, String content, UserStruct author, List<UserStruct> contributor, List<CategoryStruct> category, String comments, EnclosureStruct enclosure, String id, String pubDate, UriStruct source, String feedContent, String status, String note, ReferrerInfoStruct referrerInfo, Long publishedTime, Long lastUpdatedTime) throws BaseException;
    //Boolean updateFeedItem(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFeedItem(FeedItem feedItem) throws BaseException;
    Boolean deleteFeedItem(String guid) throws BaseException;
    Boolean deleteFeedItem(FeedItem feedItem) throws BaseException;
    Long deleteFeedItems(String filter, String params, List<String> values) throws BaseException;

//    Integer createFeedItems(List<FeedItem> feedItems) throws BaseException;
//    Boolean updateeFeedItems(List<FeedItem> feedItems) throws BaseException;

}
