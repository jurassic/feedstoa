package com.feedstoa.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface FetchRequestService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    FetchRequest getFetchRequest(String guid) throws BaseException;
    Object getFetchRequest(String guid, String field) throws BaseException;
    List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException;
    List<FetchRequest> getAllFetchRequests() throws BaseException;
    /* @Deprecated */ List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException;
    List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createFetchRequest(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException;
    //String createFetchRequest(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FetchRequest?)
    String createFetchRequest(FetchRequest fetchRequest) throws BaseException;          // Returns Guid.  (Return FetchRequest?)
    Boolean updateFetchRequest(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String originFetch, String outputFormat, Integer fetchStatus, String result, String feedCategory, Boolean multipleFeedEnabled, Boolean deferred, Boolean alert, NotificationStruct notificationPref, ReferrerInfoStruct referrerInfo, Integer refreshInterval, List<String> refreshExpressions, String refreshTimeZone, Integer currentRefreshCount, Integer maxRefreshCount, Long refreshExpirationTime, Long nextRefreshTime, Long lastUpdatedTime) throws BaseException;
    //Boolean updateFetchRequest(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException;
    Boolean deleteFetchRequest(String guid) throws BaseException;
    Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException;
    Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException;

//    Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException;
//    Boolean updateeFetchRequests(List<FetchRequest> fetchRequests) throws BaseException;

}
