package com.feedstoa.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.bean.NotificationStructBean;
import com.feedstoa.ws.bean.GaeAppStructBean;
import com.feedstoa.ws.bean.ReferrerInfoStructBean;
import com.feedstoa.ws.bean.SecondaryFetchBean;
import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.data.NotificationStructDataObject;
import com.feedstoa.ws.data.GaeAppStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.SecondaryFetchDataObject;
import com.feedstoa.ws.service.DAOFactoryManager;
import com.feedstoa.ws.service.SecondaryFetchService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SecondaryFetchServiceImpl implements SecondaryFetchService
{
    private static final Logger log = Logger.getLogger(SecondaryFetchServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // SecondaryFetch related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public SecondaryFetch getSecondaryFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");

        SecondaryFetchDataObject dataObj = getDAOFactory().getSecondaryFetchDAO().getSecondaryFetch(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SecondaryFetchDataObject for guid = " + guid);
            return null;  // ????
        }
        SecondaryFetchBean bean = new SecondaryFetchBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getSecondaryFetch(String guid, String field) throws BaseException
    {
        SecondaryFetchDataObject dataObj = getDAOFactory().getSecondaryFetchDAO().getSecondaryFetch(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve SecondaryFetchDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("managerApp")) {
            return dataObj.getManagerApp();
        } else if(field.equals("appAcl")) {
            return dataObj.getAppAcl();
        } else if(field.equals("gaeApp")) {
            return dataObj.getGaeApp();
        } else if(field.equals("ownerUser")) {
            return dataObj.getOwnerUser();
        } else if(field.equals("userAcl")) {
            return dataObj.getUserAcl();
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fetchUrl")) {
            return dataObj.getFetchUrl();
        } else if(field.equals("feedUrl")) {
            return dataObj.getFeedUrl();
        } else if(field.equals("channelFeed")) {
            return dataObj.getChannelFeed();
        } else if(field.equals("reuseChannel")) {
            return dataObj.isReuseChannel();
        } else if(field.equals("maxItemCount")) {
            return dataObj.getMaxItemCount();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<SecondaryFetch> getSecondaryFetches(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<SecondaryFetch> list = new ArrayList<SecondaryFetch>();
        List<SecondaryFetchDataObject> dataObjs = getDAOFactory().getSecondaryFetchDAO().getSecondaryFetches(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve SecondaryFetchDataObject list.");
        } else {
            Iterator<SecondaryFetchDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SecondaryFetchDataObject dataObj = (SecondaryFetchDataObject) it.next();
                list.add(new SecondaryFetchBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches() throws BaseException
    {
        return getAllSecondaryFetches(null, null, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllSecondaryFetches(ordering, offset, count, null);
    }

    @Override
    public List<SecondaryFetch> getAllSecondaryFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllSecondaryFetches(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<SecondaryFetch> list = new ArrayList<SecondaryFetch>();
        List<SecondaryFetchDataObject> dataObjs = getDAOFactory().getSecondaryFetchDAO().getAllSecondaryFetches(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve SecondaryFetchDataObject list.");
        } else {
            Iterator<SecondaryFetchDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SecondaryFetchDataObject dataObj = (SecondaryFetchDataObject) it.next();
                list.add(new SecondaryFetchBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllSecondaryFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllSecondaryFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllSecondaryFetchKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getSecondaryFetchDAO().getAllSecondaryFetchKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve SecondaryFetch key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findSecondaryFetches(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<SecondaryFetch> findSecondaryFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SecondaryFetchServiceImpl.findSecondaryFetches(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<SecondaryFetch> list = new ArrayList<SecondaryFetch>();
        List<SecondaryFetchDataObject> dataObjs = getDAOFactory().getSecondaryFetchDAO().findSecondaryFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find secondaryFetches for the given criterion.");
        } else {
            Iterator<SecondaryFetchDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                SecondaryFetchDataObject dataObj = (SecondaryFetchDataObject) it.next();
                list.add(new SecondaryFetchBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findSecondaryFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SecondaryFetchServiceImpl.findSecondaryFetchKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getSecondaryFetchDAO().findSecondaryFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find SecondaryFetch keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("SecondaryFetchServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getSecondaryFetchDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createSecondaryFetch(String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }
        
        SecondaryFetchDataObject dataObj = new SecondaryFetchDataObject(null, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
        return createSecondaryFetch(dataObj);
    }

    @Override
    public String createSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param secondaryFetch cannot be null.....
        if(secondaryFetch == null) {
            log.log(Level.INFO, "Param secondaryFetch is null!");
            throw new BadRequestException("Param secondaryFetch object is null!");
        }
        SecondaryFetchDataObject dataObj = null;
        if(secondaryFetch instanceof SecondaryFetchDataObject) {
            dataObj = (SecondaryFetchDataObject) secondaryFetch;
        } else if(secondaryFetch instanceof SecondaryFetchBean) {
            dataObj = ((SecondaryFetchBean) secondaryFetch).toDataObject();
        } else {  // if(secondaryFetch instanceof SecondaryFetch)
            //dataObj = new SecondaryFetchDataObject(null, secondaryFetch.getManagerApp(), secondaryFetch.getAppAcl(), (GaeAppStructDataObject) secondaryFetch.getGaeApp(), secondaryFetch.getOwnerUser(), secondaryFetch.getUserAcl(), secondaryFetch.getUser(), secondaryFetch.getTitle(), secondaryFetch.getDescription(), secondaryFetch.getFetchUrl(), secondaryFetch.getFeedUrl(), secondaryFetch.getChannelFeed(), secondaryFetch.isReuseChannel(), secondaryFetch.getMaxItemCount(), secondaryFetch.getNote(), secondaryFetch.getStatus(), secondaryFetch.getFetchRequest());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new SecondaryFetchDataObject(secondaryFetch.getGuid(), secondaryFetch.getManagerApp(), secondaryFetch.getAppAcl(), (GaeAppStructDataObject) secondaryFetch.getGaeApp(), secondaryFetch.getOwnerUser(), secondaryFetch.getUserAcl(), secondaryFetch.getUser(), secondaryFetch.getTitle(), secondaryFetch.getDescription(), secondaryFetch.getFetchUrl(), secondaryFetch.getFeedUrl(), secondaryFetch.getChannelFeed(), secondaryFetch.isReuseChannel(), secondaryFetch.getMaxItemCount(), secondaryFetch.getNote(), secondaryFetch.getStatus(), secondaryFetch.getFetchRequest());
        }
        String guid = getDAOFactory().getSecondaryFetchDAO().createSecondaryFetch(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateSecondaryFetch(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String ownerUser, Long userAcl, String user, String title, String description, String fetchUrl, String feedUrl, String channelFeed, Boolean reuseChannel, Integer maxItemCount, String note, String status, String fetchRequest) throws BaseException
    {
        GaeAppStructDataObject gaeAppDobj = null;
        if(gaeApp instanceof GaeAppStructBean) {
            gaeAppDobj = ((GaeAppStructBean) gaeApp).toDataObject();            
        } else if(gaeApp instanceof GaeAppStruct) {
            gaeAppDobj = new GaeAppStructDataObject(gaeApp.getGroupId(), gaeApp.getAppId(), gaeApp.getAppDomain(), gaeApp.getNamespace(), gaeApp.getAcl(), gaeApp.getNote());
        } else {
            gaeAppDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        SecondaryFetchDataObject dataObj = new SecondaryFetchDataObject(guid, managerApp, appAcl, gaeAppDobj, ownerUser, userAcl, user, title, description, fetchUrl, feedUrl, channelFeed, reuseChannel, maxItemCount, note, status, fetchRequest);
        return updateSecondaryFetch(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param secondaryFetch cannot be null.....
        if(secondaryFetch == null || secondaryFetch.getGuid() == null) {
            log.log(Level.INFO, "Param secondaryFetch or its guid is null!");
            throw new BadRequestException("Param secondaryFetch object or its guid is null!");
        }
        SecondaryFetchDataObject dataObj = null;
        if(secondaryFetch instanceof SecondaryFetchDataObject) {
            dataObj = (SecondaryFetchDataObject) secondaryFetch;
        } else if(secondaryFetch instanceof SecondaryFetchBean) {
            dataObj = ((SecondaryFetchBean) secondaryFetch).toDataObject();
        } else {  // if(secondaryFetch instanceof SecondaryFetch)
            dataObj = new SecondaryFetchDataObject(secondaryFetch.getGuid(), secondaryFetch.getManagerApp(), secondaryFetch.getAppAcl(), secondaryFetch.getGaeApp(), secondaryFetch.getOwnerUser(), secondaryFetch.getUserAcl(), secondaryFetch.getUser(), secondaryFetch.getTitle(), secondaryFetch.getDescription(), secondaryFetch.getFetchUrl(), secondaryFetch.getFeedUrl(), secondaryFetch.getChannelFeed(), secondaryFetch.isReuseChannel(), secondaryFetch.getMaxItemCount(), secondaryFetch.getNote(), secondaryFetch.getStatus(), secondaryFetch.getFetchRequest());
        }
        Boolean suc = getDAOFactory().getSecondaryFetchDAO().updateSecondaryFetch(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteSecondaryFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getSecondaryFetchDAO().deleteSecondaryFetch(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteSecondaryFetch(SecondaryFetch secondaryFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param secondaryFetch cannot be null.....
        if(secondaryFetch == null || secondaryFetch.getGuid() == null) {
            log.log(Level.INFO, "Param secondaryFetch or its guid is null!");
            throw new BadRequestException("Param secondaryFetch object or its guid is null!");
        }
        SecondaryFetchDataObject dataObj = null;
        if(secondaryFetch instanceof SecondaryFetchDataObject) {
            dataObj = (SecondaryFetchDataObject) secondaryFetch;
        } else if(secondaryFetch instanceof SecondaryFetchBean) {
            dataObj = ((SecondaryFetchBean) secondaryFetch).toDataObject();
        } else {  // if(secondaryFetch instanceof SecondaryFetch)
            dataObj = new SecondaryFetchDataObject(secondaryFetch.getGuid(), secondaryFetch.getManagerApp(), secondaryFetch.getAppAcl(), secondaryFetch.getGaeApp(), secondaryFetch.getOwnerUser(), secondaryFetch.getUserAcl(), secondaryFetch.getUser(), secondaryFetch.getTitle(), secondaryFetch.getDescription(), secondaryFetch.getFetchUrl(), secondaryFetch.getFeedUrl(), secondaryFetch.getChannelFeed(), secondaryFetch.isReuseChannel(), secondaryFetch.getMaxItemCount(), secondaryFetch.getNote(), secondaryFetch.getStatus(), secondaryFetch.getFetchRequest());
        }
        Boolean suc = getDAOFactory().getSecondaryFetchDAO().deleteSecondaryFetch(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteSecondaryFetches(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getSecondaryFetchDAO().deleteSecondaryFetches(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
