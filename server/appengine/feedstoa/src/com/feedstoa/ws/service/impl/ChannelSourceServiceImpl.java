package com.feedstoa.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.BaseException;
import com.feedstoa.ws.core.StringCursor;
import com.feedstoa.ws.exception.BadRequestException;
import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.bean.ChannelSourceBean;
import com.feedstoa.ws.dao.DAOFactory;
import com.feedstoa.ws.data.ChannelSourceDataObject;
import com.feedstoa.ws.service.DAOFactoryManager;
import com.feedstoa.ws.service.ChannelSourceService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ChannelSourceServiceImpl implements ChannelSourceService
{
    private static final Logger log = Logger.getLogger(ChannelSourceServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ChannelSource related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ChannelSource getChannelSource(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ChannelSourceDataObject dataObj = getDAOFactory().getChannelSourceDAO().getChannelSource(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelSourceDataObject for guid = " + guid);
            return null;  // ????
        }
        ChannelSourceBean bean = new ChannelSourceBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getChannelSource(String guid, String field) throws BaseException
    {
        ChannelSourceDataObject dataObj = getDAOFactory().getChannelSourceDAO().getChannelSource(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ChannelSourceDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("serviceName")) {
            return dataObj.getServiceName();
        } else if(field.equals("serviceUrl")) {
            return dataObj.getServiceUrl();
        } else if(field.equals("feedUrl")) {
            return dataObj.getFeedUrl();
        } else if(field.equals("feedFormat")) {
            return dataObj.getFeedFormat();
        } else if(field.equals("channelTitle")) {
            return dataObj.getChannelTitle();
        } else if(field.equals("channelDescription")) {
            return dataObj.getChannelDescription();
        } else if(field.equals("channelCategory")) {
            return dataObj.getChannelCategory();
        } else if(field.equals("channelUrl")) {
            return dataObj.getChannelUrl();
        } else if(field.equals("channelLogo")) {
            return dataObj.getChannelLogo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ChannelSource> getChannelSources(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ChannelSource> list = new ArrayList<ChannelSource>();
        List<ChannelSourceDataObject> dataObjs = getDAOFactory().getChannelSourceDAO().getChannelSources(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelSourceDataObject list.");
        } else {
            Iterator<ChannelSourceDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ChannelSourceDataObject dataObj = (ChannelSourceDataObject) it.next();
                list.add(new ChannelSourceBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ChannelSource> getAllChannelSources() throws BaseException
    {
        return getAllChannelSources(null, null, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelSources(ordering, offset, count, null);
    }

    @Override
    public List<ChannelSource> getAllChannelSources(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllChannelSources(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelSource> list = new ArrayList<ChannelSource>();
        List<ChannelSourceDataObject> dataObjs = getDAOFactory().getChannelSourceDAO().getAllChannelSources(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelSourceDataObject list.");
        } else {
            Iterator<ChannelSourceDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ChannelSourceDataObject dataObj = (ChannelSourceDataObject) it.next();
                list.add(new ChannelSourceBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllChannelSourceKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllChannelSourceKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllChannelSourceKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getChannelSourceDAO().getAllChannelSourceKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ChannelSource key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findChannelSources(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ChannelSource> findChannelSources(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ChannelSourceServiceImpl.findChannelSources(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ChannelSource> list = new ArrayList<ChannelSource>();
        List<ChannelSourceDataObject> dataObjs = getDAOFactory().getChannelSourceDAO().findChannelSources(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find channelSources for the given criterion.");
        } else {
            Iterator<ChannelSourceDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ChannelSourceDataObject dataObj = (ChannelSourceDataObject) it.next();
                list.add(new ChannelSourceBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findChannelSourceKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ChannelSourceServiceImpl.findChannelSourceKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getChannelSourceDAO().findChannelSourceKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ChannelSource keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ChannelSourceServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getChannelSourceDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createChannelSource(String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        ChannelSourceDataObject dataObj = new ChannelSourceDataObject(null, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        return createChannelSource(dataObj);
    }

    @Override
    public String createChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelSource cannot be null.....
        if(channelSource == null) {
            log.log(Level.INFO, "Param channelSource is null!");
            throw new BadRequestException("Param channelSource object is null!");
        }
        ChannelSourceDataObject dataObj = null;
        if(channelSource instanceof ChannelSourceDataObject) {
            dataObj = (ChannelSourceDataObject) channelSource;
        } else if(channelSource instanceof ChannelSourceBean) {
            dataObj = ((ChannelSourceBean) channelSource).toDataObject();
        } else {  // if(channelSource instanceof ChannelSource)
            //dataObj = new ChannelSourceDataObject(null, channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ChannelSourceDataObject(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        }
        String guid = getDAOFactory().getChannelSourceDAO().createChannelSource(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateChannelSource(String guid, String user, String serviceName, String serviceUrl, String feedUrl, String feedFormat, String channelTitle, String channelDescription, String channelCategory, String channelUrl, String channelLogo, String status, String note) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ChannelSourceDataObject dataObj = new ChannelSourceDataObject(guid, user, serviceName, serviceUrl, feedUrl, feedFormat, channelTitle, channelDescription, channelCategory, channelUrl, channelLogo, status, note);
        return updateChannelSource(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelSource cannot be null.....
        if(channelSource == null || channelSource.getGuid() == null) {
            log.log(Level.INFO, "Param channelSource or its guid is null!");
            throw new BadRequestException("Param channelSource object or its guid is null!");
        }
        ChannelSourceDataObject dataObj = null;
        if(channelSource instanceof ChannelSourceDataObject) {
            dataObj = (ChannelSourceDataObject) channelSource;
        } else if(channelSource instanceof ChannelSourceBean) {
            dataObj = ((ChannelSourceBean) channelSource).toDataObject();
        } else {  // if(channelSource instanceof ChannelSource)
            dataObj = new ChannelSourceDataObject(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        }
        Boolean suc = getDAOFactory().getChannelSourceDAO().updateChannelSource(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteChannelSource(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getChannelSourceDAO().deleteChannelSource(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteChannelSource(ChannelSource channelSource) throws BaseException
    {
        log.finer("BEGIN");

        // Param channelSource cannot be null.....
        if(channelSource == null || channelSource.getGuid() == null) {
            log.log(Level.INFO, "Param channelSource or its guid is null!");
            throw new BadRequestException("Param channelSource object or its guid is null!");
        }
        ChannelSourceDataObject dataObj = null;
        if(channelSource instanceof ChannelSourceDataObject) {
            dataObj = (ChannelSourceDataObject) channelSource;
        } else if(channelSource instanceof ChannelSourceBean) {
            dataObj = ((ChannelSourceBean) channelSource).toDataObject();
        } else {  // if(channelSource instanceof ChannelSource)
            dataObj = new ChannelSourceDataObject(channelSource.getGuid(), channelSource.getUser(), channelSource.getServiceName(), channelSource.getServiceUrl(), channelSource.getFeedUrl(), channelSource.getFeedFormat(), channelSource.getChannelTitle(), channelSource.getChannelDescription(), channelSource.getChannelCategory(), channelSource.getChannelUrl(), channelSource.getChannelLogo(), channelSource.getStatus(), channelSource.getNote());
        }
        Boolean suc = getDAOFactory().getChannelSourceDAO().deleteChannelSource(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteChannelSources(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getChannelSourceDAO().deleteChannelSources(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
