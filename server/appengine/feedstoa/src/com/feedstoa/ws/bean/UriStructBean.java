package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.data.UriStructDataObject;

public class UriStructBean implements UriStruct
{
    private static final Logger log = Logger.getLogger(UriStructBean.class.getName());

    // Embedded data object.
    private UriStructDataObject dobj = null;

    public UriStructBean()
    {
        this(new UriStructDataObject());
    }
    public UriStructBean(UriStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public UriStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
        }
    }

    public String getHref()
    {
        if(getDataObject() != null) {
            return getDataObject().getHref();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHref(String href)
    {
        if(getDataObject() != null) {
            getDataObject().setHref(href);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
        }
    }

    public String getRel()
    {
        if(getDataObject() != null) {
            return getDataObject().getRel();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRel(String rel)
    {
        if(getDataObject() != null) {
            getDataObject().setRel(rel);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
        }
    }

    public String getLabel()
    {
        if(getDataObject() != null) {
            return getDataObject().getLabel();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLabel(String label)
    {
        if(getDataObject() != null) {
            getDataObject().setLabel(label);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UriStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLabel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public UriStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
