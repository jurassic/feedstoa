package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchBase;
import com.feedstoa.ws.data.NotificationStructDataObject;
import com.feedstoa.ws.data.GaeAppStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.FetchBaseDataObject;

public abstract class FetchBaseBean extends BeanBase implements FetchBase
{
    private static final Logger log = Logger.getLogger(FetchBaseBean.class.getName());

    public FetchBaseBean()
    {
        super();
    }

    @Override
    public abstract FetchBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getManagerApp()
    {
        if(getDataObject() != null) {
            return getDataObject().getManagerApp();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setManagerApp(String managerApp)
    {
        if(getDataObject() != null) {
            getDataObject().setManagerApp(managerApp);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public Long getAppAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAppAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppAcl(Long appAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setAppAcl(appAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public GaeAppStruct getGaeApp()
    {
        if(getDataObject() != null) {
            GaeAppStruct _field = getDataObject().getGaeApp();
            if(_field == null) {
                log.log(Level.INFO, "gaeApp is null.");
                return null;
            } else {
                return new GaeAppStructBean((GaeAppStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setGaeApp(GaeAppStruct gaeApp)
    {
        if(getDataObject() != null) {
            getDataObject().setGaeApp(
                (gaeApp instanceof GaeAppStructBean) ?
                ((GaeAppStructBean) gaeApp).toDataObject() :
                ((gaeApp instanceof GaeAppStructDataObject) ? gaeApp : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getOwnerUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getOwnerUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setOwnerUser(String ownerUser)
    {
        if(getDataObject() != null) {
            getDataObject().setOwnerUser(ownerUser);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public Long getUserAcl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserAcl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserAcl(Long userAcl)
    {
        if(getDataObject() != null) {
            getDataObject().setUserAcl(userAcl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getFetchUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchUrl(String fetchUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchUrl(fetchUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getFeedUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedUrl(feedUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getChannelFeed()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelFeed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelFeed(String channelFeed)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelFeed(channelFeed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public Boolean isReuseChannel()
    {
        if(getDataObject() != null) {
            return getDataObject().isReuseChannel();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setReuseChannel(Boolean reuseChannel)
    {
        if(getDataObject() != null) {
            getDataObject().setReuseChannel(reuseChannel);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public Integer getMaxItemCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxItemCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxItemCount(maxItemCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
