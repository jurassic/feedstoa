package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.data.EnclosureStructDataObject;
import com.feedstoa.ws.data.CategoryStructDataObject;
import com.feedstoa.ws.data.UriStructDataObject;
import com.feedstoa.ws.data.RssItemDataObject;

public class RssItemBean implements RssItem
{
    private static final Logger log = Logger.getLogger(RssItemBean.class.getName());

    // Embedded data object.
    private RssItemDataObject dobj = null;

    public RssItemBean()
    {
        this(new RssItemDataObject());
    }
    public RssItemBean(RssItemDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public RssItemDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public String getLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setLink(String link)
    {
        if(getDataObject() != null) {
            getDataObject().setLink(link);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public String getAuthor()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthor(String author)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthor(author);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getDataObject() != null) {
            List<CategoryStruct> list = getDataObject().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = new CategoryStructBean((CategoryStructDataObject) categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getDataObject() != null) {
                List<CategoryStruct> dataObj = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructDataObject elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).toDataObject();
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = (CategoryStructDataObject) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setCategory(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setCategory(category);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            }
        }
    }

    public String getComments()
    {
        if(getDataObject() != null) {
            return getDataObject().getComments();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setComments(String comments)
    {
        if(getDataObject() != null) {
            getDataObject().setComments(comments);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public EnclosureStruct getEnclosure()
    {
        if(getDataObject() != null) {
            EnclosureStruct _field = getDataObject().getEnclosure();
            if(_field == null) {
                log.log(Level.INFO, "enclosure is null.");
                return null;
            } else {
                return new EnclosureStructBean((EnclosureStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if(getDataObject() != null) {
            getDataObject().setEnclosure(
                (enclosure instanceof EnclosureStructBean) ?
                ((EnclosureStructBean) enclosure).toDataObject() :
                ((enclosure instanceof EnclosureStructDataObject) ? enclosure : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public String getPubDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getPubDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getDataObject() != null) {
            getDataObject().setPubDate(pubDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }

    public UriStruct getSource()
    {
        if(getDataObject() != null) {
            UriStruct _field = getDataObject().getSource();
            if(_field == null) {
                log.log(Level.INFO, "source is null.");
                return null;
            } else {
                return new UriStructBean((UriStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource(UriStruct source)
    {
        if(getDataObject() != null) {
            getDataObject().setSource(
                (source instanceof UriStructBean) ?
                ((UriStructBean) source).toDataObject() :
                ((source instanceof UriStructDataObject) ? source : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssItemDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAuthor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getComments() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEnclosure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGuid() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public RssItemDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
