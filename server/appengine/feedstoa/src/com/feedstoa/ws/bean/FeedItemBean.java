package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FeedItem;
import com.feedstoa.ws.data.EnclosureStructDataObject;
import com.feedstoa.ws.data.CategoryStructDataObject;
import com.feedstoa.ws.data.UriStructDataObject;
import com.feedstoa.ws.data.UserStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.FeedItemDataObject;

public class FeedItemBean extends BeanBase implements FeedItem
{
    private static final Logger log = Logger.getLogger(FeedItemBean.class.getName());

    // Embedded data object.
    private FeedItemDataObject dobj = null;

    public FeedItemBean()
    {
        this(new FeedItemDataObject());
    }
    public FeedItemBean(String guid)
    {
        this(new FeedItemDataObject(guid));
    }
    public FeedItemBean(FeedItemDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public FeedItemDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getFetchRequest()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchRequest();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchRequest(fetchRequest);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getFetchUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchUrl(String fetchUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchUrl(fetchUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getFeedUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedUrl(feedUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getChannelSource()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelSource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelSource(String channelSource)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelSource(channelSource);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getChannelFeed()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelFeed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelFeed(String channelFeed)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelFeed(channelFeed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getFeedFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedFormat(feedFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public UriStruct getLink()
    {
        if(getDataObject() != null) {
            UriStruct _field = getDataObject().getLink();
            if(_field == null) {
                log.log(Level.INFO, "link is null.");
                return null;
            } else {
                return new UriStructBean((UriStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setLink(UriStruct link)
    {
        if(getDataObject() != null) {
            getDataObject().setLink(
                (link instanceof UriStructBean) ?
                ((UriStructBean) link).toDataObject() :
                ((link instanceof UriStructDataObject) ? link : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getSummary()
    {
        if(getDataObject() != null) {
            return getDataObject().getSummary();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setSummary(String summary)
    {
        if(getDataObject() != null) {
            getDataObject().setSummary(summary);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setContent(String content)
    {
        if(getDataObject() != null) {
            getDataObject().setContent(content);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public UserStruct getAuthor()
    {
        if(getDataObject() != null) {
            UserStruct _field = getDataObject().getAuthor();
            if(_field == null) {
                log.log(Level.INFO, "author is null.");
                return null;
            } else {
                return new UserStructBean((UserStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthor(UserStruct author)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthor(
                (author instanceof UserStructBean) ?
                ((UserStructBean) author).toDataObject() :
                ((author instanceof UserStructDataObject) ? author : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public List<UserStruct> getContributor()
    {
        if(getDataObject() != null) {
            List<UserStruct> list = getDataObject().getContributor();
            if(list != null) {
                List<UserStruct> bean = new ArrayList<UserStruct>();
                for(UserStruct userStruct : list) {
                    UserStructBean elem = null;
                    if(userStruct instanceof UserStructBean) {
                        elem = (UserStructBean) userStruct;
                    } else if(userStruct instanceof UserStructDataObject) {
                        elem = new UserStructBean((UserStructDataObject) userStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;
        }
    }
    public void setContributor(List<UserStruct> contributor)
    {
        if(contributor != null) {
            if(getDataObject() != null) {
                List<UserStruct> dataObj = new ArrayList<UserStruct>();
                for(UserStruct userStruct : contributor) {
                    UserStructDataObject elem = null;
                    if(userStruct instanceof UserStructBean) {
                        elem = ((UserStructBean) userStruct).toDataObject();
                    } else if(userStruct instanceof UserStructDataObject) {
                        elem = (UserStructDataObject) userStruct;
                    } else if(userStruct instanceof UserStruct) {
                        elem = new UserStructDataObject(userStruct.getUuid(), userStruct.getName(), userStruct.getEmail(), userStruct.getUrl());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setContributor(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setContributor(contributor);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            }
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getDataObject() != null) {
            List<CategoryStruct> list = getDataObject().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = new CategoryStructBean((CategoryStructDataObject) categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getDataObject() != null) {
                List<CategoryStruct> dataObj = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructDataObject elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).toDataObject();
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = (CategoryStructDataObject) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setCategory(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setCategory(category);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            }
        }
    }

    public String getComments()
    {
        if(getDataObject() != null) {
            return getDataObject().getComments();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setComments(String comments)
    {
        if(getDataObject() != null) {
            getDataObject().setComments(comments);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public EnclosureStruct getEnclosure()
    {
        if(getDataObject() != null) {
            EnclosureStruct _field = getDataObject().getEnclosure();
            if(_field == null) {
                log.log(Level.INFO, "enclosure is null.");
                return null;
            } else {
                return new EnclosureStructBean((EnclosureStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setEnclosure(EnclosureStruct enclosure)
    {
        if(getDataObject() != null) {
            getDataObject().setEnclosure(
                (enclosure instanceof EnclosureStructBean) ?
                ((EnclosureStructBean) enclosure).toDataObject() :
                ((enclosure instanceof EnclosureStructDataObject) ? enclosure : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getId()
    {
        if(getDataObject() != null) {
            return getDataObject().getId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setId(String id)
    {
        if(getDataObject() != null) {
            getDataObject().setId(id);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getPubDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getPubDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getDataObject() != null) {
            getDataObject().setPubDate(pubDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public UriStruct getSource()
    {
        if(getDataObject() != null) {
            UriStruct _field = getDataObject().getSource();
            if(_field == null) {
                log.log(Level.INFO, "source is null.");
                return null;
            } else {
                return new UriStructBean((UriStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource(UriStruct source)
    {
        if(getDataObject() != null) {
            getDataObject().setSource(
                (source instanceof UriStructBean) ?
                ((UriStructBean) source).toDataObject() :
                ((source instanceof UriStructDataObject) ? source : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getFeedContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedContent(String feedContent)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedContent(feedContent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public Long getPublishedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getPublishedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setPublishedTime(Long publishedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setPublishedTime(publishedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastUpdatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastUpdatedTime(lastUpdatedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedItemDataObject is null!");
        }
    }


    // TBD
    public FeedItemDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
