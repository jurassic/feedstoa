package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.data.ImageStructDataObject;

public class ImageStructBean implements ImageStruct
{
    private static final Logger log = Logger.getLogger(ImageStructBean.class.getName());

    // Embedded data object.
    private ImageStructDataObject dobj = null;

    public ImageStructBean()
    {
        this(new ImageStructDataObject());
    }
    public ImageStructBean(ImageStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ImageStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLink(String link)
    {
        if(getDataObject() != null) {
            getDataObject().setLink(link);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public Integer getWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setWidth(Integer width)
    {
        if(getDataObject() != null) {
            getDataObject().setWidth(width);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public Integer getHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHeight(Integer height)
    {
        if(getDataObject() != null) {
            getDataObject().setHeight(height);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ImageStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
