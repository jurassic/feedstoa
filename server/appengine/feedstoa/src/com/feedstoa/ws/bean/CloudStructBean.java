package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.data.CloudStructDataObject;

public class CloudStructBean implements CloudStruct
{
    private static final Logger log = Logger.getLogger(CloudStructBean.class.getName());

    // Embedded data object.
    private CloudStructDataObject dobj = null;

    public CloudStructBean()
    {
        this(new CloudStructDataObject());
    }
    public CloudStructBean(CloudStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public CloudStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
        }
    }

    public Integer getPort()
    {
        if(getDataObject() != null) {
            return getDataObject().getPort();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPort(Integer port)
    {
        if(getDataObject() != null) {
            getDataObject().setPort(port);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
        }
    }

    public String getPath()
    {
        if(getDataObject() != null) {
            return getDataObject().getPath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPath(String path)
    {
        if(getDataObject() != null) {
            getDataObject().setPath(path);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
        }
    }

    public String getRegisterProcedure()
    {
        if(getDataObject() != null) {
            return getDataObject().getRegisterProcedure();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRegisterProcedure(String registerProcedure)
    {
        if(getDataObject() != null) {
            getDataObject().setRegisterProcedure(registerProcedure);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
        }
    }

    public String getProtocol()
    {
        if(getDataObject() != null) {
            return getDataObject().getProtocol();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setProtocol(String protocol)
    {
        if(getDataObject() != null) {
            getDataObject().setProtocol(protocol);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CloudStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPort() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRegisterProcedure() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProtocol() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public CloudStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
