package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.RssItem;
import com.feedstoa.ws.RssChannel;
import com.feedstoa.ws.data.CloudStructDataObject;
import com.feedstoa.ws.data.CategoryStructDataObject;
import com.feedstoa.ws.data.ImageStructDataObject;
import com.feedstoa.ws.data.TextInputStructDataObject;
import com.feedstoa.ws.data.RssItemDataObject;
import com.feedstoa.ws.data.RssChannelDataObject;

public class RssChannelBean implements RssChannel
{
    private static final Logger log = Logger.getLogger(RssChannelBean.class.getName());

    // Embedded data object.
    private RssChannelDataObject dobj = null;

    public RssChannelBean()
    {
        this(new RssChannelDataObject());
    }
    public RssChannelBean(RssChannelDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public RssChannelDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getLink()
    {
        if(getDataObject() != null) {
            return getDataObject().getLink();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setLink(String link)
    {
        if(getDataObject() != null) {
            getDataObject().setLink(link);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getLanguage()
    {
        if(getDataObject() != null) {
            return getDataObject().getLanguage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setLanguage(String language)
    {
        if(getDataObject() != null) {
            getDataObject().setLanguage(language);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getCopyright()
    {
        if(getDataObject() != null) {
            return getDataObject().getCopyright();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setCopyright(String copyright)
    {
        if(getDataObject() != null) {
            getDataObject().setCopyright(copyright);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getManagingEditor()
    {
        if(getDataObject() != null) {
            return getDataObject().getManagingEditor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setManagingEditor(String managingEditor)
    {
        if(getDataObject() != null) {
            getDataObject().setManagingEditor(managingEditor);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getWebMaster()
    {
        if(getDataObject() != null) {
            return getDataObject().getWebMaster();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setWebMaster(String webMaster)
    {
        if(getDataObject() != null) {
            getDataObject().setWebMaster(webMaster);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getPubDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getPubDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getDataObject() != null) {
            getDataObject().setPubDate(pubDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getLastBuildDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastBuildDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        if(getDataObject() != null) {
            getDataObject().setLastBuildDate(lastBuildDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getDataObject() != null) {
            List<CategoryStruct> list = getDataObject().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = new CategoryStructBean((CategoryStructDataObject) categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getDataObject() != null) {
                List<CategoryStruct> dataObj = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructDataObject elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).toDataObject();
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = (CategoryStructDataObject) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setCategory(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setCategory(category);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            }
        }
    }

    public String getGenerator()
    {
        if(getDataObject() != null) {
            return getDataObject().getGenerator();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setGenerator(String generator)
    {
        if(getDataObject() != null) {
            getDataObject().setGenerator(generator);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getDocs()
    {
        if(getDataObject() != null) {
            return getDataObject().getDocs();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setDocs(String docs)
    {
        if(getDataObject() != null) {
            getDataObject().setDocs(docs);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public CloudStruct getCloud()
    {
        if(getDataObject() != null) {
            CloudStruct _field = getDataObject().getCloud();
            if(_field == null) {
                log.log(Level.INFO, "cloud is null.");
                return null;
            } else {
                return new CloudStructBean((CloudStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setCloud(CloudStruct cloud)
    {
        if(getDataObject() != null) {
            getDataObject().setCloud(
                (cloud instanceof CloudStructBean) ?
                ((CloudStructBean) cloud).toDataObject() :
                ((cloud instanceof CloudStructDataObject) ? cloud : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public Integer getTtl()
    {
        if(getDataObject() != null) {
            return getDataObject().getTtl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setTtl(Integer ttl)
    {
        if(getDataObject() != null) {
            getDataObject().setTtl(ttl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public ImageStruct getImage()
    {
        if(getDataObject() != null) {
            ImageStruct _field = getDataObject().getImage();
            if(_field == null) {
                log.log(Level.INFO, "image is null.");
                return null;
            } else {
                return new ImageStructBean((ImageStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setImage(ImageStruct image)
    {
        if(getDataObject() != null) {
            getDataObject().setImage(
                (image instanceof ImageStructBean) ?
                ((ImageStructBean) image).toDataObject() :
                ((image instanceof ImageStructDataObject) ? image : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public String getRating()
    {
        if(getDataObject() != null) {
            return getDataObject().getRating();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setRating(String rating)
    {
        if(getDataObject() != null) {
            getDataObject().setRating(rating);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public TextInputStruct getTextInput()
    {
        if(getDataObject() != null) {
            TextInputStruct _field = getDataObject().getTextInput();
            if(_field == null) {
                log.log(Level.INFO, "textInput is null.");
                return null;
            } else {
                return new TextInputStructBean((TextInputStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if(getDataObject() != null) {
            getDataObject().setTextInput(
                (textInput instanceof TextInputStructBean) ?
                ((TextInputStructBean) textInput).toDataObject() :
                ((textInput instanceof TextInputStructDataObject) ? textInput : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public List<Integer> getSkipHours()
    {
        if(getDataObject() != null) {
            return getDataObject().getSkipHours();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        if(getDataObject() != null) {
            getDataObject().setSkipHours(skipHours);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public List<String> getSkipDays()
    {
        if(getDataObject() != null) {
            return getDataObject().getSkipDays();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;   // ???
        }
    }
    public void setSkipDays(List<String> skipDays)
    {
        if(getDataObject() != null) {
            getDataObject().setSkipDays(skipDays);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
        }
    }

    public List<RssItem> getItem()
    {
        if(getDataObject() != null) {
            List<RssItem> list = getDataObject().getItem();
            if(list != null) {
                List<RssItem> bean = new ArrayList<RssItem>();
                for(RssItem rssItem : list) {
                    RssItemBean elem = null;
                    if(rssItem instanceof RssItemBean) {
                        elem = (RssItemBean) rssItem;
                    } else if(rssItem instanceof RssItemDataObject) {
                        elem = new RssItemBean((RssItemDataObject) rssItem);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            return null;
        }
    }
    public void setItem(List<RssItem> item)
    {
        if(item != null) {
            if(getDataObject() != null) {
                List<RssItem> dataObj = new ArrayList<RssItem>();
                for(RssItem rssItem : item) {
                    RssItemDataObject elem = null;
                    if(rssItem instanceof RssItemBean) {
                        elem = ((RssItemBean) rssItem).toDataObject();
                    } else if(rssItem instanceof RssItemDataObject) {
                        elem = (RssItemDataObject) rssItem;
                    } else if(rssItem instanceof RssItem) {
                        elem = new RssItemDataObject(rssItem.getTitle(), rssItem.getLink(), rssItem.getDescription(), rssItem.getAuthor(), rssItem.getCategory(), rssItem.getComments(), rssItem.getEnclosure(), rssItem.getGuid(), rssItem.getPubDate(), rssItem.getSource());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setItem(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setItem(item);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            }
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RssChannelDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLink() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDescription() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLanguage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCopyright() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getManagingEditor() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWebMaster() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPubDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLastBuildDate() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCategory() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGenerator() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDocs() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getCloud() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTtl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRating() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTextInput() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipHours() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSkipDays() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getItem() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public RssChannelDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
