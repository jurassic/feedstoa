package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.FetchRequest;
import com.feedstoa.ws.data.NotificationStructDataObject;
import com.feedstoa.ws.data.GaeAppStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.FetchRequestDataObject;

public class FetchRequestBean extends FetchBaseBean implements FetchRequest
{
    private static final Logger log = Logger.getLogger(FetchRequestBean.class.getName());

    // Embedded data object.
    private FetchRequestDataObject dobj = null;

    public FetchRequestBean()
    {
        this(new FetchRequestDataObject());
    }
    public FetchRequestBean(String guid)
    {
        this(new FetchRequestDataObject(guid));
    }
    public FetchRequestBean(FetchRequestDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public FetchRequestDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getOriginFetch()
    {
        if(getDataObject() != null) {
            return getDataObject().getOriginFetch();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setOriginFetch(String originFetch)
    {
        if(getDataObject() != null) {
            getDataObject().setOriginFetch(originFetch);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getOutputFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputFormat(String outputFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputFormat(outputFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getFetchStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchStatus(fetchStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getResult()
    {
        if(getDataObject() != null) {
            return getDataObject().getResult();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setResult(String result)
    {
        if(getDataObject() != null) {
            getDataObject().setResult(result);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getFeedCategory()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedCategory();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedCategory(String feedCategory)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedCategory(feedCategory);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Boolean isMultipleFeedEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isMultipleFeedEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setMultipleFeedEnabled(Boolean multipleFeedEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setMultipleFeedEnabled(multipleFeedEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Boolean isDeferred()
    {
        if(getDataObject() != null) {
            return getDataObject().isDeferred();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setDeferred(Boolean deferred)
    {
        if(getDataObject() != null) {
            getDataObject().setDeferred(deferred);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Boolean isAlert()
    {
        if(getDataObject() != null) {
            return getDataObject().isAlert();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setAlert(Boolean alert)
    {
        if(getDataObject() != null) {
            getDataObject().setAlert(alert);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public NotificationStruct getNotificationPref()
    {
        if(getDataObject() != null) {
            NotificationStruct _field = getDataObject().getNotificationPref();
            if(_field == null) {
                log.log(Level.INFO, "notificationPref is null.");
                return null;
            } else {
                return new NotificationStructBean((NotificationStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(getDataObject() != null) {
            getDataObject().setNotificationPref(
                (notificationPref instanceof NotificationStructBean) ?
                ((NotificationStructBean) notificationPref).toDataObject() :
                ((notificationPref instanceof NotificationStructDataObject) ? notificationPref : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getRefreshInterval()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshInterval();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshInterval(refreshInterval);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public List<String> getRefreshExpressions()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshExpressions();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshExpressions(List<String> refreshExpressions)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshExpressions(refreshExpressions);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getRefreshTimeZone()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshTimeZone();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshTimeZone(String refreshTimeZone)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshTimeZone(refreshTimeZone);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getCurrentRefreshCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getCurrentRefreshCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setCurrentRefreshCount(Integer currentRefreshCount)
    {
        if(getDataObject() != null) {
            getDataObject().setCurrentRefreshCount(currentRefreshCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getMaxRefreshCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxRefreshCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxRefreshCount(Integer maxRefreshCount)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxRefreshCount(maxRefreshCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Long getRefreshExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshExpirationTime(Long refreshExpirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshExpirationTime(refreshExpirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Long getNextRefreshTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getNextRefreshTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        if(getDataObject() != null) {
            getDataObject().setNextRefreshTime(nextRefreshTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastUpdatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastUpdatedTime(lastUpdatedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }


    // TBD
    public FetchRequestDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
