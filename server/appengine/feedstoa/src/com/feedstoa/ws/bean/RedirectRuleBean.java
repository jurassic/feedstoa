package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.RedirectRule;
import com.feedstoa.ws.data.RedirectRuleDataObject;

public class RedirectRuleBean implements RedirectRule
{
    private static final Logger log = Logger.getLogger(RedirectRuleBean.class.getName());

    // Embedded data object.
    private RedirectRuleDataObject dobj = null;

    public RedirectRuleBean()
    {
        this(new RedirectRuleDataObject());
    }
    public RedirectRuleBean(RedirectRuleDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public RedirectRuleDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirectType(redirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public Double getPrecedence()
    {
        if(getDataObject() != null) {
            return getDataObject().getPrecedence();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setPrecedence(Double precedence)
    {
        if(getDataObject() != null) {
            getDataObject().setPrecedence(precedence);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public String getSourceDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getSourceDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setSourceDomain(String sourceDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setSourceDomain(sourceDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public String getSourcePath()
    {
        if(getDataObject() != null) {
            return getDataObject().getSourcePath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setSourcePath(String sourcePath)
    {
        if(getDataObject() != null) {
            getDataObject().setSourcePath(sourcePath);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public String getTargetDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getTargetDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setTargetDomain(String targetDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setTargetDomain(targetDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public String getTargetPath()
    {
        if(getDataObject() != null) {
            return getDataObject().getTargetPath();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setTargetPath(String targetPath)
    {
        if(getDataObject() != null) {
            getDataObject().setTargetPath(targetPath);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RedirectRuleDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getRedirectType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPrecedence() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourceDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSourcePath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetDomain() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTargetPath() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getStatus() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public RedirectRuleDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
