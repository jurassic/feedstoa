package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.ChannelSource;
import com.feedstoa.ws.data.ChannelSourceDataObject;

public class ChannelSourceBean extends BeanBase implements ChannelSource
{
    private static final Logger log = Logger.getLogger(ChannelSourceBean.class.getName());

    // Embedded data object.
    private ChannelSourceDataObject dobj = null;

    public ChannelSourceBean()
    {
        this(new ChannelSourceDataObject());
    }
    public ChannelSourceBean(String guid)
    {
        this(new ChannelSourceDataObject(guid));
    }
    public ChannelSourceBean(ChannelSourceDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ChannelSourceDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getServiceName()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceName(String serviceName)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceName(serviceName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setServiceUrl(String serviceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setServiceUrl(serviceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getFeedUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedUrl(feedUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getFeedFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedFormat(feedFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getChannelTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelTitle(String channelTitle)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelTitle(channelTitle);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getChannelDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelDescription(String channelDescription)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelDescription(channelDescription);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getChannelCategory()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelCategory();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelCategory(String channelCategory)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelCategory(channelCategory);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getChannelUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelUrl(String channelUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelUrl(channelUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getChannelLogo()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelLogo();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelLogo(String channelLogo)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelLogo(channelLogo);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelSourceDataObject is null!");
        }
    }


    // TBD
    public ChannelSourceDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
