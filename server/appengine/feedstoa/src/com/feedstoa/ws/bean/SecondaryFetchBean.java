package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.NotificationStruct;
import com.feedstoa.ws.GaeAppStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.SecondaryFetch;
import com.feedstoa.ws.data.NotificationStructDataObject;
import com.feedstoa.ws.data.GaeAppStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.SecondaryFetchDataObject;

public class SecondaryFetchBean extends FetchBaseBean implements SecondaryFetch
{
    private static final Logger log = Logger.getLogger(SecondaryFetchBean.class.getName());

    // Embedded data object.
    private SecondaryFetchDataObject dobj = null;

    public SecondaryFetchBean()
    {
        this(new SecondaryFetchDataObject());
    }
    public SecondaryFetchBean(String guid)
    {
        this(new SecondaryFetchDataObject(guid));
    }
    public SecondaryFetchBean(SecondaryFetchDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public SecondaryFetchDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getFetchRequest()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchRequest();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SecondaryFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchRequest(fetchRequest);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded SecondaryFetchDataObject is null!");
        }
    }


    // TBD
    public SecondaryFetchDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
