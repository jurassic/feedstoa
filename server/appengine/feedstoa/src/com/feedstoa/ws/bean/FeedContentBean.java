package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.FeedContent;
import com.feedstoa.ws.data.FeedContentDataObject;

public class FeedContentBean extends BeanBase implements FeedContent
{
    private static final Logger log = Logger.getLogger(FeedContentBean.class.getName());

    // Embedded data object.
    private FeedContentDataObject dobj = null;

    public FeedContentBean()
    {
        this(new FeedContentDataObject());
    }
    public FeedContentBean(String guid)
    {
        this(new FeedContentDataObject(guid));
    }
    public FeedContentBean(FeedContentDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public FeedContentDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getChannelSource()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelSource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelSource(String channelSource)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelSource(channelSource);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getChannelFeed()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelFeed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelFeed(String channelFeed)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelFeed(channelFeed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getChannelVersion()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelVersion();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelVersion(String channelVersion)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelVersion(channelVersion);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getFeedUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedUrl(feedUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getFeedFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedFormat(feedFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setContent(String content)
    {
        if(getDataObject() != null) {
            getDataObject().setContent(content);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getContentHash()
    {
        if(getDataObject() != null) {
            return getDataObject().getContentHash();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setContentHash(String contentHash)
    {
        if(getDataObject() != null) {
            getDataObject().setContentHash(contentHash);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public String getLastBuildDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastBuildDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        if(getDataObject() != null) {
            getDataObject().setLastBuildDate(lastBuildDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }

    public Long getLastBuildTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastBuildTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastBuildTime(lastBuildTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FeedContentDataObject is null!");
        }
    }


    // TBD
    public FeedContentDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
