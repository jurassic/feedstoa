package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.feedstoa.ws.CloudStruct;
import com.feedstoa.ws.CategoryStruct;
import com.feedstoa.ws.ImageStruct;
import com.feedstoa.ws.UriStruct;
import com.feedstoa.ws.UserStruct;
import com.feedstoa.ws.ReferrerInfoStruct;
import com.feedstoa.ws.TextInputStruct;
import com.feedstoa.ws.ChannelFeed;
import com.feedstoa.ws.data.CloudStructDataObject;
import com.feedstoa.ws.data.CategoryStructDataObject;
import com.feedstoa.ws.data.ImageStructDataObject;
import com.feedstoa.ws.data.UriStructDataObject;
import com.feedstoa.ws.data.UserStructDataObject;
import com.feedstoa.ws.data.ReferrerInfoStructDataObject;
import com.feedstoa.ws.data.TextInputStructDataObject;
import com.feedstoa.ws.data.ChannelFeedDataObject;

public class ChannelFeedBean extends BeanBase implements ChannelFeed
{
    private static final Logger log = Logger.getLogger(ChannelFeedBean.class.getName());

    // Embedded data object.
    private ChannelFeedDataObject dobj = null;

    public ChannelFeedBean()
    {
        this(new ChannelFeedDataObject());
    }
    public ChannelFeedBean(String guid)
    {
        this(new ChannelFeedDataObject(guid));
    }
    public ChannelFeedBean(ChannelFeedDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ChannelFeedDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getChannelSource()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelSource();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelSource(String channelSource)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelSource(channelSource);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getChannelCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getChannelCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setChannelCode(String channelCode)
    {
        if(getDataObject() != null) {
            getDataObject().setChannelCode(channelCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getPreviousVersion()
    {
        if(getDataObject() != null) {
            return getDataObject().getPreviousVersion();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreviousVersion(String previousVersion)
    {
        if(getDataObject() != null) {
            getDataObject().setPreviousVersion(previousVersion);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFetchRequest()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchRequest();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchRequest(fetchRequest);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFetchUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchUrl(String fetchUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchUrl(fetchUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFeedServiceUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedServiceUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedServiceUrl(String feedServiceUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedServiceUrl(feedServiceUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFeedUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedUrl(String feedUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedUrl(feedUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFeedFormat()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedFormat();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedFormat(String feedFormat)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedFormat(feedFormat);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public Integer getMaxItemCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getMaxItemCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setMaxItemCount(Integer maxItemCount)
    {
        if(getDataObject() != null) {
            getDataObject().setMaxItemCount(maxItemCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFeedCategory()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedCategory();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedCategory(String feedCategory)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedCategory(feedCategory);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getSubtitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getSubtitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setSubtitle(String subtitle)
    {
        if(getDataObject() != null) {
            getDataObject().setSubtitle(subtitle);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public UriStruct getLink()
    {
        if(getDataObject() != null) {
            UriStruct _field = getDataObject().getLink();
            if(_field == null) {
                log.log(Level.INFO, "link is null.");
                return null;
            } else {
                return new UriStructBean((UriStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setLink(UriStruct link)
    {
        if(getDataObject() != null) {
            getDataObject().setLink(
                (link instanceof UriStructBean) ?
                ((UriStructBean) link).toDataObject() :
                ((link instanceof UriStructDataObject) ? link : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getLanguage()
    {
        if(getDataObject() != null) {
            return getDataObject().getLanguage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setLanguage(String language)
    {
        if(getDataObject() != null) {
            getDataObject().setLanguage(language);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getCopyright()
    {
        if(getDataObject() != null) {
            return getDataObject().getCopyright();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setCopyright(String copyright)
    {
        if(getDataObject() != null) {
            getDataObject().setCopyright(copyright);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public UserStruct getManagingEditor()
    {
        if(getDataObject() != null) {
            UserStruct _field = getDataObject().getManagingEditor();
            if(_field == null) {
                log.log(Level.INFO, "managingEditor is null.");
                return null;
            } else {
                return new UserStructBean((UserStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setManagingEditor(UserStruct managingEditor)
    {
        if(getDataObject() != null) {
            getDataObject().setManagingEditor(
                (managingEditor instanceof UserStructBean) ?
                ((UserStructBean) managingEditor).toDataObject() :
                ((managingEditor instanceof UserStructDataObject) ? managingEditor : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public UserStruct getWebMaster()
    {
        if(getDataObject() != null) {
            UserStruct _field = getDataObject().getWebMaster();
            if(_field == null) {
                log.log(Level.INFO, "webMaster is null.");
                return null;
            } else {
                return new UserStructBean((UserStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setWebMaster(UserStruct webMaster)
    {
        if(getDataObject() != null) {
            getDataObject().setWebMaster(
                (webMaster instanceof UserStructBean) ?
                ((UserStructBean) webMaster).toDataObject() :
                ((webMaster instanceof UserStructDataObject) ? webMaster : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public List<UserStruct> getContributor()
    {
        if(getDataObject() != null) {
            List<UserStruct> list = getDataObject().getContributor();
            if(list != null) {
                List<UserStruct> bean = new ArrayList<UserStruct>();
                for(UserStruct userStruct : list) {
                    UserStructBean elem = null;
                    if(userStruct instanceof UserStructBean) {
                        elem = (UserStructBean) userStruct;
                    } else if(userStruct instanceof UserStructDataObject) {
                        elem = new UserStructBean((UserStructDataObject) userStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;
        }
    }
    public void setContributor(List<UserStruct> contributor)
    {
        if(contributor != null) {
            if(getDataObject() != null) {
                List<UserStruct> dataObj = new ArrayList<UserStruct>();
                for(UserStruct userStruct : contributor) {
                    UserStructDataObject elem = null;
                    if(userStruct instanceof UserStructBean) {
                        elem = ((UserStructBean) userStruct).toDataObject();
                    } else if(userStruct instanceof UserStructDataObject) {
                        elem = (UserStructDataObject) userStruct;
                    } else if(userStruct instanceof UserStruct) {
                        elem = new UserStructDataObject(userStruct.getUuid(), userStruct.getName(), userStruct.getEmail(), userStruct.getUrl());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setContributor(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setContributor(contributor);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            }
        }
    }

    public String getPubDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getPubDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setPubDate(String pubDate)
    {
        if(getDataObject() != null) {
            getDataObject().setPubDate(pubDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getLastBuildDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastBuildDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastBuildDate(String lastBuildDate)
    {
        if(getDataObject() != null) {
            getDataObject().setLastBuildDate(lastBuildDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public List<CategoryStruct> getCategory()
    {
        if(getDataObject() != null) {
            List<CategoryStruct> list = getDataObject().getCategory();
            if(list != null) {
                List<CategoryStruct> bean = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : list) {
                    CategoryStructBean elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = (CategoryStructBean) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = new CategoryStructBean((CategoryStructDataObject) categoryStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;
        }
    }
    public void setCategory(List<CategoryStruct> category)
    {
        if(category != null) {
            if(getDataObject() != null) {
                List<CategoryStruct> dataObj = new ArrayList<CategoryStruct>();
                for(CategoryStruct categoryStruct : category) {
                    CategoryStructDataObject elem = null;
                    if(categoryStruct instanceof CategoryStructBean) {
                        elem = ((CategoryStructBean) categoryStruct).toDataObject();
                    } else if(categoryStruct instanceof CategoryStructDataObject) {
                        elem = (CategoryStructDataObject) categoryStruct;
                    } else if(categoryStruct instanceof CategoryStruct) {
                        elem = new CategoryStructDataObject(categoryStruct.getUuid(), categoryStruct.getDomain(), categoryStruct.getLabel(), categoryStruct.getTitle());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setCategory(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setCategory(category);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            }
        }
    }

    public String getGenerator()
    {
        if(getDataObject() != null) {
            return getDataObject().getGenerator();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setGenerator(String generator)
    {
        if(getDataObject() != null) {
            getDataObject().setGenerator(generator);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getDocs()
    {
        if(getDataObject() != null) {
            return getDataObject().getDocs();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setDocs(String docs)
    {
        if(getDataObject() != null) {
            getDataObject().setDocs(docs);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public CloudStruct getCloud()
    {
        if(getDataObject() != null) {
            CloudStruct _field = getDataObject().getCloud();
            if(_field == null) {
                log.log(Level.INFO, "cloud is null.");
                return null;
            } else {
                return new CloudStructBean((CloudStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setCloud(CloudStruct cloud)
    {
        if(getDataObject() != null) {
            getDataObject().setCloud(
                (cloud instanceof CloudStructBean) ?
                ((CloudStructBean) cloud).toDataObject() :
                ((cloud instanceof CloudStructDataObject) ? cloud : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public Integer getTtl()
    {
        if(getDataObject() != null) {
            return getDataObject().getTtl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setTtl(Integer ttl)
    {
        if(getDataObject() != null) {
            getDataObject().setTtl(ttl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public ImageStruct getLogo()
    {
        if(getDataObject() != null) {
            ImageStruct _field = getDataObject().getLogo();
            if(_field == null) {
                log.log(Level.INFO, "logo is null.");
                return null;
            } else {
                return new ImageStructBean((ImageStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setLogo(ImageStruct logo)
    {
        if(getDataObject() != null) {
            getDataObject().setLogo(
                (logo instanceof ImageStructBean) ?
                ((ImageStructBean) logo).toDataObject() :
                ((logo instanceof ImageStructDataObject) ? logo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public ImageStruct getIcon()
    {
        if(getDataObject() != null) {
            ImageStruct _field = getDataObject().getIcon();
            if(_field == null) {
                log.log(Level.INFO, "icon is null.");
                return null;
            } else {
                return new ImageStructBean((ImageStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setIcon(ImageStruct icon)
    {
        if(getDataObject() != null) {
            getDataObject().setIcon(
                (icon instanceof ImageStructBean) ?
                ((ImageStructBean) icon).toDataObject() :
                ((icon instanceof ImageStructDataObject) ? icon : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getRating()
    {
        if(getDataObject() != null) {
            return getDataObject().getRating();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setRating(String rating)
    {
        if(getDataObject() != null) {
            getDataObject().setRating(rating);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public TextInputStruct getTextInput()
    {
        if(getDataObject() != null) {
            TextInputStruct _field = getDataObject().getTextInput();
            if(_field == null) {
                log.log(Level.INFO, "textInput is null.");
                return null;
            } else {
                return new TextInputStructBean((TextInputStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setTextInput(TextInputStruct textInput)
    {
        if(getDataObject() != null) {
            getDataObject().setTextInput(
                (textInput instanceof TextInputStructBean) ?
                ((TextInputStructBean) textInput).toDataObject() :
                ((textInput instanceof TextInputStructDataObject) ? textInput : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public List<Integer> getSkipHours()
    {
        if(getDataObject() != null) {
            return getDataObject().getSkipHours();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setSkipHours(List<Integer> skipHours)
    {
        if(getDataObject() != null) {
            getDataObject().setSkipHours(skipHours);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public List<String> getSkipDays()
    {
        if(getDataObject() != null) {
            return getDataObject().getSkipDays();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setSkipDays(List<String> skipDays)
    {
        if(getDataObject() != null) {
            getDataObject().setSkipDays(skipDays);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getOutputText()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputText(String outputText)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputText(outputText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getOutputHash()
    {
        if(getDataObject() != null) {
            return getDataObject().getOutputHash();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setOutputHash(String outputHash)
    {
        if(getDataObject() != null) {
            getDataObject().setOutputHash(outputHash);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getFeedContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getFeedContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setFeedContent(String feedContent)
    {
        if(getDataObject() != null) {
            getDataObject().setFeedContent(feedContent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public Long getLastBuildTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastBuildTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastBuildTime(Long lastBuildTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastBuildTime(lastBuildTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public Long getPublishedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getPublishedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setPublishedTime(Long publishedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setPublishedTime(publishedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastUpdatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastUpdatedTime(lastUpdatedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ChannelFeedDataObject is null!");
        }
    }


    // TBD
    public ChannelFeedDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
