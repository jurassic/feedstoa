package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.UserWebsiteStruct;
import com.feedstoa.ws.data.UserWebsiteStructDataObject;

public class UserWebsiteStructBean implements UserWebsiteStruct
{
    private static final Logger log = Logger.getLogger(UserWebsiteStructBean.class.getName());

    // Embedded data object.
    private UserWebsiteStructDataObject dobj = null;

    public UserWebsiteStructBean()
    {
        this(new UserWebsiteStructDataObject());
    }
    public UserWebsiteStructBean(UserWebsiteStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public UserWebsiteStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getPrimarySite()
    {
        if(getDataObject() != null) {
            return getDataObject().getPrimarySite();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPrimarySite(String primarySite)
    {
        if(getDataObject() != null) {
            getDataObject().setPrimarySite(primarySite);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getHomePage()
    {
        if(getDataObject() != null) {
            return getDataObject().getHomePage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHomePage(String homePage)
    {
        if(getDataObject() != null) {
            getDataObject().setHomePage(homePage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getBlogSite()
    {
        if(getDataObject() != null) {
            return getDataObject().getBlogSite();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setBlogSite(String blogSite)
    {
        if(getDataObject() != null) {
            getDataObject().setBlogSite(blogSite);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getPortfolioSite()
    {
        if(getDataObject() != null) {
            return getDataObject().getPortfolioSite();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPortfolioSite(String portfolioSite)
    {
        if(getDataObject() != null) {
            getDataObject().setPortfolioSite(portfolioSite);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getTwitterProfile()
    {
        if(getDataObject() != null) {
            return getDataObject().getTwitterProfile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setTwitterProfile(String twitterProfile)
    {
        if(getDataObject() != null) {
            getDataObject().setTwitterProfile(twitterProfile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getFacebookProfile()
    {
        if(getDataObject() != null) {
            return getDataObject().getFacebookProfile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setFacebookProfile(String facebookProfile)
    {
        if(getDataObject() != null) {
            getDataObject().setFacebookProfile(facebookProfile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getGooglePlusProfile()
    {
        if(getDataObject() != null) {
            return getDataObject().getGooglePlusProfile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setGooglePlusProfile(String googlePlusProfile)
    {
        if(getDataObject() != null) {
            getDataObject().setGooglePlusProfile(googlePlusProfile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UserWebsiteStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPrimarySite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHomePage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getBlogSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPortfolioSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGooglePlusProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public UserWebsiteStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
