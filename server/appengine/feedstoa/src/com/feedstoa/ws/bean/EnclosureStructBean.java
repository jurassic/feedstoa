package com.feedstoa.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.feedstoa.ws.EnclosureStruct;
import com.feedstoa.ws.data.EnclosureStructDataObject;

public class EnclosureStructBean implements EnclosureStruct
{
    private static final Logger log = Logger.getLogger(EnclosureStructBean.class.getName());

    // Embedded data object.
    private EnclosureStructDataObject dobj = null;

    public EnclosureStructBean()
    {
        this(new EnclosureStructDataObject());
    }
    public EnclosureStructBean(EnclosureStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public EnclosureStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
        }
    }

    public Integer getLength()
    {
        if(getDataObject() != null) {
            return getDataObject().getLength();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLength(Integer length)
    {
        if(getDataObject() != null) {
            getDataObject().setLength(length);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EnclosureStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLength() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public EnclosureStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
