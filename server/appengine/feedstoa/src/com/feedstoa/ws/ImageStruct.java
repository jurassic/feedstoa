package com.feedstoa.ws;



public interface ImageStruct 
{
    String  getUrl();
    String  getTitle();
    String  getLink();
    Integer  getWidth();
    Integer  getHeight();
    String  getDescription();
    boolean isEmpty();
}
