package com.feedstoa.ws;



public interface TextInputStruct 
{
    String  getTitle();
    String  getName();
    String  getLink();
    String  getDescription();
    boolean isEmpty();
}
