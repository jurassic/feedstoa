package com.feedstoa.cert.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseConsumerRegistry.class.getName());

    // Consumer key-secret map.
    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
    private Map<String, String> consumerSecretMap;
    
    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD...
        consumerSecretMap.put("19ea846d-9d2b-40c4-9886-a073c266f093", "afdf0130-b273-4531-830f-696e2f1aaec9");  // FeedStoa + FeedStoaApp
        consumerSecretMap.put("c85ba22c-f49c-422c-83bf-9d73717b0f1c", "f585c179-d420-458e-96f8-4d3ab1ef9789");  // FeedStoa + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
